﻿using UnityEngine;
using System.Collections.Generic;

namespace Bini.Utils {
	// Stores per-scene hints for pools
	[MonoBehaviourSingleton(OnlyActive=true, AutoCreate=true)]
	public class PoolManager : MonoBehaviourSingleton<PoolManager> {
		public string[] Hints;

		Hint global_hint;
		Hint default_hint;
		Dictionary<string, Hint> hints_map = null;

		public struct Hint {
			public int Capacity; public bool CapacityIsSet;
			public float Decay; public bool DecayIsSet;
			public bool Clear; public bool ClearIsSet;

			public void Parse(string s) {
				if (string.IsNullOrEmpty(s)) return;
				var args = s.Split(',');
				for (int i = 0; i < args.Length; i++) {
					var toks = args[i].Split('=');
					string name = toks[0].Trim().ToLower();
					string val = (toks.Length > 1) ? toks[1].Trim() : null;
					switch (name) {
					case "capacity": CapacityIsSet = int.TryParse(val, out Capacity); break;
					case "decay": DecayIsSet = float.TryParse(val, out Decay); break;
					case "clear": ClearIsSet = bool.TryParse(val, out Clear); break;
					}
				}
			}

			public override string ToString() {
				string capacity_str = (CapacityIsSet ? "Capacity="+Capacity : "");
				string decay_str = (DecayIsSet ? "Decay="+Decay : "");
				string clear_str = (ClearIsSet ? "Clear="+Clear : "");
				string s = capacity_str;
				if (decay_str.Length != 0) {
					if (s.Length != 0) s += ", ";
					s += decay_str;
				}
				if (clear_str.Length != 0) {
					if (s.Length != 0) s += ", ";
					s += clear_str;
				}
				return "Hints{"+s+"}";
			}
		}

		Hint ParseHint(string s, out string name) {
			name = null;
			s = s.Trim();
			if (string.IsNullOrEmpty(s)) return new Hint();
			var toks = s.Split(':');
			if (toks.Length == 1) {
				//global_hint.Parse(toks[0].Trim());
				//default_hint = global_hint;
				default_hint.Parse(toks[0].Trim());
				return default_hint;
			} else {
				name = toks[0].Trim();
				if (string.IsNullOrEmpty(name)) {
					default_hint.Parse(toks[1]);
					return default_hint;
				} else {
					var hint = default_hint;
					hint.Parse(toks[1]);
					hints_map[name] = hint;
					return hint;
				}
			}
		}
		void ParseHints() {
			string name;
			for (int i = 0; i < Hints.Length; i++) {
				ParseHint(Hints[i], out name);
			}
		}
		void InitHintsMap() {
			if (Hints != null) {
				hints_map = new Dictionary<string, Hint>(Hints.Length);
				ParseHints();
			} else {
				hints_map = new Dictionary<string, Hint>();
			}
		}

		public void AddHint(string s) {
			if (hints_map == null) InitHintsMap();
			string name;
			var hint = ParseHint(s, out name);
			foreach (var pool in Pools) {
				if (pool.name == name) ApplyHint(pool, hint);
			}
		}

		public Hint GetHint(string name) {
			if (hints_map == null) InitHintsMap();
			if (!string.IsNullOrEmpty(name)) {
				Hint hint;
				if (hints_map.TryGetValue(name, out hint)) return hint;
			}
			return global_hint;
		}

		public void ApplyHint(Pool pool) {
			ApplyHint(pool, GetHint(pool.name));
		}
		public void ApplyHint(Pool pool, Hint hint) {
			if (hint.DecayIsSet) pool.decay = hint.Decay;
			if (hint.CapacityIsSet) pool.capacity = hint.Capacity;
			if (hint.ClearIsSet) pool.autoClear = hint.Clear;
			//Debug.Log("Apply hint "+hint+" to pool "+pool.name);
		}

		public IEnumerable<Pool> FindPools(string name) {
			foreach (var pool in Pools) {
				if (pool.name == name) yield return pool;
			}
		}

		static HashSet<Pool> Pools;
		public static void Register(Pool pool) {
			if (Pools == null) Pools = new HashSet<Pool>();
			Instance.ApplyHint(pool);
			Pools.Add(pool);
			//Debug.Log("Register pool "+pool.name);
		}

		static HashSet<Pool> DecayingPools;
		public static void RegisterDecay(Pool pool) {
			if (DecayingPools == null) DecayingPools = new HashSet<Pool>();
			if ((pool.decay > float.Epsilon) && !float.IsInfinity(pool.decay)) {
				pool.decayingMaximum = 1f;
				DecayingPools.Add(pool);
				//Debug.Log("Decaying pool "+pool.name);
			} else {
				DecayingPools.Remove(pool);
				//Debug.Log("Not decaying pool "+pool.name);
			}
		}

		void Awake() {
			if (Pools == null) return;
			foreach (var pool in Pools) {
				ApplyHint(pool);
			}
		}

		void Update() {
			if (DecayingPools == null) return;
			if (DecayingPools.Count == 0) return;
			float dt = Time.deltaTime;
			foreach (var pool in DecayingPools) {
				int count = pool.count;
				int capacity = pool.capacity;
				if (count <= capacity) continue;
				int extraTotal = count - capacity;
				int extraActive = pool.countActive - capacity;
				float extraGauge = extraActive / (float)extraTotal;
				float decayGauge = pool.decayingMaximum;
				if (decayGauge > extraGauge) {
					if (decayGauge <= pool.decayThreshold) {
						pool.Clear(Mathf.CeilToInt(extraTotal*(1f-decayGauge)));
						pool.decayingMaximum = 1f;
					} else {
						pool.decayingMaximum = decayGauge * Mathf.Pow(2f, -dt/pool.decay);
					}
				} else {
					pool.decayingMaximum = extraGauge;
				}
			}
		}

		// When application is quitting, OnApplicationQuit() is called before OnDestroy().
		// If derived class has OnApplicationQuit(), it would be called instead of
		// base class' OnApplicationQuit().

		void OnDestroy() {
			if (Pools == null) return;

			List<Pool> to_unregister = null;

			foreach (var pool in Pools) {
				pool.RequiredCapacity_Log();

				//Debug.Log("Before: "+pool.name+" : "+pool.countActive+" : "+pool.count+" : "+pool.capacity);
				if (pool.autoClear || pool.autoUnregister) {
					pool.RequiredCapacity_Reset();
					pool.Clear(-1, -1);
				}
				//Debug.Log("After: "+pool.name+" : "+pool.countActive+" : "+pool.count+" : "+pool.capacity);

				if (pool.autoUnregister) {
					if (to_unregister == null) to_unregister = new List<Pool>();
					to_unregister.Add(pool);
				}
			}

			if (to_unregister != null) {
				for (int i = 0; i < to_unregister.Count; i++) {
					var pool = to_unregister[i];
					//Debug.Log("Unregister pool "+pool.name);
					Pools.Remove(pool);
					DecayingPools.Remove(pool);
				}
			}
		}
	}

	// Log capacities for each scene so that the corresponding tweaks can be set in editor?

	public abstract class Pool {
		public float decayingMaximum {get; set;}
		public float decayThreshold {get; set;}

		public int requiredCapacity {get; set;}
		[System.Diagnostics.Conditional("UNITY_EDITOR")]
		public void RequiredCapacity_Update() {
			requiredCapacity = Mathf.Max(requiredCapacity, countActive);
		}
		[System.Diagnostics.Conditional("UNITY_EDITOR")]
		public void RequiredCapacity_Reset() {
			requiredCapacity = 0;
		}
		[System.Diagnostics.Conditional("UNITY_EDITOR")]
		public void RequiredCapacity_Log(bool reset=false) {
			//Debug.Log("Pool "+name+" active max = "+requiredCapacity);
			if (reset) RequiredCapacity_Reset();
		}

		public string name {get; protected set;}
		public bool autoClear {get; set;}
		public bool autoUnregister {get; set;}
		public abstract int capacity {get; set;}
		public abstract float decay {get; set;}
		public abstract void Clear(int n_inactive=-1, int n_active=0);
		public abstract int count {get;}
		public abstract int countActive {get;}
	}

	// Not a singleton, because there may be multiple pools for the same type
	// but which manage semantically different objects.
	// However, pools are expected to be stored in static fields.
	// Thread-safe?
	public class Pool<T> : Pool where T : class {
		public abstract class Gate {
			public abstract T Create(object args=null);
			public abstract T Show(T obj, object args=null, bool force=false);
			public virtual void Destroy(T obj) {
			}
			public virtual void Hide(T obj) {
			}
		}

		// Capacity > 0 -> preallocate
		// Capacity <= 0 -> just set the soft limit
		int _capacity = 0;
		public override int capacity {
			get {
				lock (_lock) {
					return _capacity;
				}
			}
			set {
				lock (_lock) {
					bool preallocate = (value > 0);
					_capacity = Mathf.Abs(value);
					int difference = _capacity - count;

					if (difference > 0) {
						if (preallocate) Preallocate(difference);
					} else if (difference < 0) {
						if (decay <= 0) Clear(-difference);
					}
				}
			}
		}
		void Preallocate(int n) {
			//Debug.Log("Pool "+name+" preallocate "+n);
			for (int i = 0; i < n; i++) {
				inactive.AddLast(gate.Create());
			}
		}

		// not locked because it influences only PoolManager
		float _decay = 0f;
		public override float decay {
			get { return _decay; }
			set {
				_decay = value;
				PoolManager.RegisterDecay(this);
			}
		}

		public override void Clear(int n_inactive=-1, int n_active=0) {
			lock (_lock) {
				//Debug.Log("Pool "+name+" clear "+n_inactive+", "+n_active);
				ClearList(inactive, n_inactive);
				ClearList(active, Mathf.Min(n_active, 0));
			}
		}
		void ClearList(LinkedList<T> list, int n) {
			n = (n < 0) ? list.Count : Mathf.Min(n, list.Count);
			for (int i = 0; i < n; i++) {
				var obj = list.Last.Value;
				gate.Destroy(obj);
				list.RemoveLast();
			}
		}

		public override int count {
			get {
				lock (_lock) {
					return active.Count + inactive.Count;
				}
			}
		}
		public override int countActive {
			get {
				lock (_lock) {
					return active.Count;
				}
			}
		}

		LinkedList<T> active = new LinkedList<T>();
		LinkedList<T> inactive = new LinkedList<T>();

		Dictionary<T, LinkedListNode<T>> active_map = null;
		public bool fastSearch {
			get {
				lock (_lock) {
					return (active_map != null);
				}
			}
			set {
				lock (_lock) {
					if (value && (active_map == null)) {
						active_map = new Dictionary<T, LinkedListNode<T>>(active.Count);
						for (var node = active.First; node != null; node = node.Next) {
							active_map[node.Value] = node;
						}
					} else if (!value && (active_map != null)) {
						active_map = null;
					}
				}
			}
		}
		
		public Gate gate {get; protected set;}

		object _lock; // any private object is fine

		public Pool(Gate gate, string name=null, int capacity=0, float decay=0f, bool clear=true, bool fast=false, float decayThreshold=0.75f) {
			_lock = active; // any private object is fine
			this.gate = gate;
			this.name = (name != null) ? name : typeof(T).ToString();
			this.decay = decay;
			this.capacity = capacity;
			this.autoClear = clear;
			this.fastSearch = fast;
			this.decayThreshold = decayThreshold;
			PoolManager.Register(this);
		}

		public IEnumerable<T> Taken {
			get {
				lock (_lock) { // I suppose this is meaningless to lock?
					for (var node = active.First; node != null; node = node.Next) {
						yield return node.Value;
					}
				}
			}
		}

		LinkedListNode<T> AddActive(T obj) {
			lock (_lock) {
				var node = active.AddLast(obj);
				RequiredCapacity_Update();
				if (active_map != null) active_map[obj] = node;
				return node;
			}
		}
		void AddActive(LinkedListNode<T> node) {
			lock (_lock) {
				active.AddLast(node);
				RequiredCapacity_Update();
				if (active_map != null) active_map[node.Value] = node;
			}
		}
		void RemoveActive(LinkedListNode<T> node) {
			lock (_lock) {
				active.Remove(node);
				if (active_map != null) active_map.Remove(node.Value);
			}
		}

		public T Take() {
			#pragma warning disable 0168
			var pool_manager = PoolManager.Instance; // make sure it exists
			#pragma warning restore 0168

			T obj;

			lock (_lock) {
				var node = inactive.First;
				if (node == null) {
					//Debug.Log("Pool "+name+" take new");
					obj = gate.Create();
					node = AddActive(obj);
				} else {
					//Debug.Log("Pool "+name+" take existing");
					inactive.Remove(node);
					AddActive(node);
					obj = node.Value;
				}
				gate.Show(obj);
			}

			return obj;
		}

		public T Take(object args) {
			#pragma warning disable 0168
			var pool_manager = PoolManager.Instance; // make sure it exists
			#pragma warning restore 0168

			T obj = null;

			lock (_lock) {
				var node = inactive.First;

				if (node == null) {
					//Debug.Log("Pool "+name+" take new");
					// all objs are active, need to create another one
					obj = gate.Create(args);
					node = AddActive(obj);
					gate.Show(obj, args, true);
					return obj;
				}

				// Search for the first obj that matches the arguments
				for (; node != null; node = node.Next) {
					obj = node.Value;
					if (gate.Show(obj, args) != null) break;
				}

				if (node == null) {
					//Debug.Log("Pool "+name+" take converted");
					// No matching obj found; convert any one
					node = inactive.First;
					obj = gate.Show(node.Value, args, true);
					node.Value = obj;
				} else {
					//Debug.Log("Pool "+name+" take existing");
				}

				inactive.Remove(node);
				AddActive(node);
			}

			return obj;
		}
		
		public T Release(T obj) {
			if (obj == null) return obj;

			#pragma warning disable 0168
			var pool_manager = PoolManager.Instance; // make sure it exists
			#pragma warning restore 0168

			lock (_lock) {
				LinkedListNode<T> node = null;
				if (active_map != null) {
					if (!active_map.TryGetValue(obj, out node)) node = null;
				} else {
					// Search from the end, as objects with smaller lifetime
					// are more likely to be close to end of list
					for (node = active.Last; node != null; node = node.Previous) {
						//if (object.ReferenceEquals(node.Value, obj)) break;
						if (node.Value == obj) break;
					}
				}

				if (node == null) return obj; // not found

				if ((decay > 0) || ((active.Count+inactive.Count) <= _capacity)) {
					//Debug.Log("Pool "+name+" release hide");
					gate.Hide(obj);
					RemoveActive(node);
					inactive.AddLast(node);
				} else {
					//Debug.Log("Pool "+name+" release destroy");
					gate.Destroy(obj);
					RemoveActive(node);
				}
			}

			return null;
		}
	}
}
// Note:
// When Unity Object is destroyed, (obj == null) will be true only if the ==
// operator is accessed through one of the Unity Object types; if type of
// obj is object/interface/generic type, then (obj == null) might return false.

/// Strategies:
/// Allocation:
/// * preallocate expected amount on program start (capacity > 0, no hints in scenes)
/// * preallocate expected amount on scene start (capacity <= 0, in scenes capacity > 0)
/// * allocate on demand (capacity <= 0)
/// Deallocation:
/// * completely delete when scene ends (extra hint in current scene, or use a transitional scene)
/// * keep until next scene starts and see if capacity should be increased/decreased (capacity hint in next scene)
/// * keep until program ends (no hints in scenes)
/// On "spike" (when number of requested active objects exceeds the capacity):
/// * destroy extra objects as soon as they are released (decay <= 0)
/// * destroy extra objects gradually over time (decay > 0)
/// * never decrease number of pool entries (decay = infinity)
