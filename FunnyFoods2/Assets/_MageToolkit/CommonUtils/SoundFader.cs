﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using Bini.Utils;

public class SoundFader : MonoBehaviour {
	public float duration = 0;

	AudioSource source = null;
	float volume_start;
	float time_start;

	void Start() {
		source = this.GetComponent<AudioSource>();
		if ((source == null) || (duration == 0)) {
			this.enabled = false;
			return;
		}
		volume_start = source.volume;
		time_start = Time.time;
	}

	void Update() {
		if ((source == null) || (duration == 0)) {
			this.enabled = false;
			return;
		}
		float abs_duration = Mathf.Abs(duration);
		float t = Mathf.Clamp01((Time.time - time_start)/abs_duration);
		if (duration > 0) {
			source.volume = Mathf.Lerp(volume_start, 1f, t);
		} else {
			source.volume = Mathf.Lerp(volume_start, 0, t);
		}
		if (t >= 1) this.enabled = false;
	}

	public static void SetFade(Transform tfm, float duration) {
		if (tfm == null) return;
		SetFade(tfm.gameObject, duration);
	}
	public static void SetFade(GameObject gameObj, float duration) {
		if (gameObj == null) return;
		if (gameObj.GetComponent<AudioSource>() == null) return;
		var fader = gameObj.EnsureComponent<SoundFader>();
		if (fader == null) return;
		fader.enabled = true;
		fader.source = fader.GetComponent<AudioSource>();
		fader.duration = duration;
		fader.volume_start = fader.source.volume;
		fader.time_start = Time.time;
	}

	public static void FadeAll(float duration) {
		var cam = Camera.main;
		if (cam == null) return;

		foreach (Transform child in cam.transform) {
			SetFade(child.gameObject, duration);
		}
	}
}
