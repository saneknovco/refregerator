using System;
using System.Collections;
using System.Collections.Generic;
using System.Runtime.InteropServices;
using UnityEngine;

namespace Bini.Utils {
	public struct Vector2i {
		public int x, y;

		public float magnitude {
			get { return Mathf.Sqrt(x*x + y*y); }
		}

		public int sqrMagnitude {
			get { return x*x + y*y; }
		}

		public Vector2i(int x, int y) {
			this.x = x;
			this.y = y;
		}

		public Vector2i Mul(Vector2i u) {
			return new Vector2i(x * u.x, y * u.y);
		}
		public Vector2 Div(Vector2 u) {
			return new Vector2(x / u.x, y / u.y);
		}
		public Vector2i abs {
			get { return new Vector2i(Mathf.Abs(x), Mathf.Abs(y)); }
		}
		public int min {
			get { return Mathf.Min(x, y); }
		}
		public int max {
			get { return Mathf.Max(x, y); }
		}

		public static implicit operator Vector2i(Vector2 v) {
			//return new Vector2i((int)v.x, (int)v.y);
			return new Vector2i(Mathf.RoundToInt(v.x), Mathf.RoundToInt(v.y));
		}
		public static implicit operator Vector2(Vector2i v) {
			return new Vector2(v.x, v.y);
		}

		public static Vector2i operator +(Vector2i a, Vector2i b) {
			return new Vector2i(a.x + b.x, a.y + b.y);
		}
		
		public static Vector2i operator -(Vector2i a, Vector2i b) {
			return new Vector2i(a.x - b.x, a.y - b.y);
		}
		
		public override string ToString() {
			return string.Format("Vector2i({0}, {1})", x.ToString(), y.ToString());
		}
		
		public bool Equals(Vector2i other) {
			return (other.x == x) && (other.y == y);
		}

		public override int GetHashCode() {
			int h1 = x.GetHashCode();
			int h2 = y.GetHashCode();

			// https://stackoverflow.com/questions/7120845/equivalent-of-tuple-net-4-for-net-framework-3-5
			//return (h1 << 3) ^ h2;
			return (h1 << 5) + h1 ^ h2; // default .NET formula (?)

			// https://gist.github.com/michaelbartnett/5652076
			//int hash = 17;
			//hash = hash * 23 + h1;
			//hash = hash * 23 + h2;
			//return hash;

			//return x.GetHashCode() ^ y.GetHashCode();
		}

		public override bool Equals(object other) {
			var casted = other as Vector2i?;
			if ((object)casted == null) return false;
			return Equals(casted.Value);
		}

		public static bool operator ==(Vector2i a, Vector2i b) {
			return (a.x == b.x) && (a.y == b.y);
		}

		public static bool operator !=(Vector2i a, Vector2i b) {
			return (a.x != b.x) || (a.y != b.y);
		}

		// See http://docs.xamarin.com/guides/ios/advanced_topics/limitations/
		// It's recommended to manually pass IEqualityComparer<TKey> instance
		// to Dictionary<TKey,TValue> constructor, or otherwise Mono would
		// attempt to use capabilities that are not allowed on iOS.
		public class EqualityComparer : IEqualityComparer<Vector2i> {
			public bool Equals(Vector2i a, Vector2i b) {
				return (a.x == b.x) && (a.y == b.y);
			}
			public int GetHashCode(Vector2i a) {
				return a.GetHashCode();
			}
		}
	}

	public struct IntRect {
		public int xMin;
		public int xMax;
		public int yMin;
		public int yMax;
		public bool initialized;
		
		public IntRect(int x, int y, int w, int h) {
			xMin = x;
			yMin = y;
			xMax = x + w;
			yMax = y + h;
			initialized = true;
		}

		public static implicit operator IntRect(Rect r) {
			return new IntRect((int)r.x, (int)r.y, (int)r.width, (int)r.height);
		}
		public static implicit operator Rect(IntRect r) {
			return new Rect(r.x, r.y, r.w, r.h);
		}

		public void Encapsulate(Vector2i v) {
			Encapsulate(v.x, v.y);
		}
		public void Encapsulate(int x, int y) {
			if (!initialized) {
				xMin = xMax = x;
				yMin = yMax = y;
				initialized = true;
				return;
			}
			xMin = System.Math.Min(xMin, x);
			xMax = System.Math.Max(xMax, x);
			yMin = System.Math.Min(yMin, y);
			yMax = System.Math.Max(yMax, y);
		}
		public void Encapsulate(IntRect r) {
			if (!initialized) {
				xMin = r.xMin; yMin = r.yMin;
				xMax = r.xMax; yMax = r.yMax;
				initialized = true;
				return;
			}
			xMin = System.Math.Min(xMin, r.xMin);
			xMax = System.Math.Max(xMax, r.xMax);
			yMin = System.Math.Min(yMin, r.yMin);
			yMax = System.Math.Max(yMax, r.yMax);
		}

		public void Expand(Vector2i v) {
			Expand(v.x, v.y);
		}
		public void Expand(int d) {
			Expand(d, d);
		}
		public void Expand(int dx, int dy) {
			xMin -= dx;
			yMin -= dy;
			xMax += dx;
			yMax += dy;
		}

		// necessary due to inapplicability of the "center" concept
		public void Move(Vector2i v) {
			Move(v.x, v.y);
		}
		public void Move(int dx, int dy) {
			xMin += dx;
			xMax += dx;
			yMin += dy;
			yMax += dy;
		}
		
		public int xMid {
			get { return (xMin + xMax)/2; }
		}
		public int yMid {
			get { return (yMin + yMax)/2; }
		}
		
		public int x {
			get { return xMin; }
			set {
				xMax = value + w;
				xMin = value;
			}
		}
		public int y {
			get { return yMin; }
			set {
				yMax = value + h;
				yMin = value;
			}
		}
		
		public int w {
			get { return xMax - xMin; }
			set { xMax = xMin + value; }
		}
		public int h {
			get { return yMax - yMin; }
			set { yMax = yMin + value; }
		}

		public int width {get{return w;} set{w = value;}}
		public int height {get{return h;} set{h = value;}}

		public int wAbs {
			get { return Mathf.Abs(w); }
		}
		public int hAbs {
			get { return Mathf.Abs(h); }
		}
		public int area {
			get { return wAbs * hAbs; }
		}
		
		public int xMinAbs {
			get { return Mathf.Min(xMin, xMax); }
		}
		public int xMaxAbs {
			get { return Mathf.Max(xMin, xMax); }
		}
		
		public int yMinAbs {
			get { return Mathf.Min(yMin, yMax); }
		}
		public int yMaxAbs {
			get { return Mathf.Max(yMin, yMax); }
		}

		public Vector2i size {
			get { return new Vector2i(w, h); }
		}
		public Vector2 extents {
			get { return new Vector2(w*0.5f, h*0.5f); }
		}
		public Vector2i min {
			get { return new Vector2i(xMin, yMin); }
		}
		public Vector2i max {
			get { return new Vector2i(xMax, yMax); }
		}

		public override string ToString() {
			return string.Format("IntRect({0}, {1}, {2}, {3})",
				x.ToString(), y.ToString(), w.ToString(), h.ToString());
		}

		public bool Equals(IntRect other) {
			return (other.xMin == xMin) && (other.yMin == yMin) &&
				(other.xMax == xMax) && (other.yMax == yMax);
		}

		public override int GetHashCode() {
			return xMin.GetHashCode() ^ yMin.GetHashCode() ^
				xMax.GetHashCode() ^ yMax.GetHashCode();
		}

		public override bool Equals(object other) {
			var casted = other as IntRect?;
			if ((object)casted == null) return false;
			return Equals(casted.Value);
		}

		public static bool operator ==(IntRect a, IntRect b) {
			return (a.xMin == b.xMin) && (a.yMin == b.yMin) &&
				(a.xMax == b.xMax) && (a.yMax == b.yMax);
		}

		public static bool operator !=(IntRect a, IntRect b) {
			return (a.xMin != b.xMin) || (a.yMin != b.yMin) ||
				(a.xMax != b.xMax) || (a.yMax != b.yMax);
		}
	}

	public enum GrayscaleMode {
		Lightness,
		Luminosity,
		Average,
		Perceived,
		W3C,
	}

	public static class ColorExt {
		public static int ToRGBA(Color32 c) {
			return c.r | (c.g << 8) | (c.b << 16) | (c.a << 24);
		}
		public static Color32 FromRGBA(int i) {
			byte r = (byte)(i & 0xff);
			byte g = (byte)((i >> 8) & 0xff);
			byte b = (byte)((i >> 16) & 0xff);
			byte a = (byte)((i >> 24) & 0xff);
			return new Color32(r, g, b, a);
		}

		public static Color SetR(this Color c, float v) {
			c.r = v;
			return c;
		}
		public static Color SetG(this Color c, float v) {
			c.g = v;
			return c;
		}
		public static Color SetB(this Color c, float v) {
			c.b = v;
			return c;
		}
		public static Color SetA(this Color c, float v) {
			c.a = v;
			return c;
		}

		public static Color32 SetR(this Color32 c, byte v) {
			c.r = v;
			return c;
		}
		public static Color32 SetG(this Color32 c, byte v) {
			c.g = v;
			return c;
		}
		public static Color32 SetB(this Color32 c, byte v) {
			c.b = v;
			return c;
		}
		public static Color32 SetA(this Color32 c, byte v) {
			c.a = v;
			return c;
		}

		public static Color Subtract(this Color c1, Color c0) {
			return new Color(c1.r-c0.r, c1.g-c0.g, c1.b-c0.b, c1.a-c0.a);
		}
		public static Color32 Subtract(this Color32 c1, Color32 c0) {
			return new Color32((byte)(c1.r-c0.r), (byte)(c1.g-c0.g), (byte)(c1.b-c0.b), (byte)(c1.a-c0.a));
		}
		
		public static Color Add(this Color c1, Color c0) {
			return new Color(c1.r+c0.r, c1.g+c0.g, c1.b+c0.b, c1.a+c0.a);
		}
		public static Color32 Add(this Color32 c1, Color32 c0) {
			return new Color32((byte)(c1.r+c0.r), (byte)(c1.g+c0.g), (byte)(c1.b+c0.b), (byte)(c1.a+c0.a));
		}
		
		public static Color Multiply(this Color c, Color f) {
			return new Color(c.r*f.r, c.g*f.g, c.b*f.b, c.a*f.a);
		}
		public static Color32 Multiply(this Color32 c, Color32 f) {
			return new Color32((byte)(c.r*f.r/255), (byte)(c.g*f.g/255),
				(byte)(c.b*f.b/255), (byte)(c.a*f.a/255));
		}
		public static Color Multiply(this Color c, float f) {
			return new Color(c.r*f, c.g*f, c.b*f, c.a*f);
		}
		public static Color32 Multiply(this Color32 c, float f) {
			return new Color32((byte)(c.r*f), (byte)(c.g*f), (byte)(c.b*f), (byte)(c.a*f));
		}
		public static Color32 Multiply(this Color32 c, int n, int d) {
			return new Color32((byte)(c.r*n/d), (byte)(c.g*n/d), (byte)(c.b*n/d), (byte)(c.a*n/d));
		}

	    public static bool SameAs(this Color c0, ref Color c1) {
			return (c0.r == c1.r) && (c0.g == c1.g) && (c0.b == c1.b) && (c0.a == c1.a);
	    }
	    public static bool SameAs(this Color32 c0, ref Color32 c1) {
			return (c0.r == c1.r) && (c0.g == c1.g) && (c0.b == c1.b) && (c0.a == c1.a);
	    }
		
		public static Color Sum(params Color[] colors) {
			float r = 0f, g = 0f, b = 0f, a = 0f;
			for (int i = 0; i < colors.Length; i++) {
				Color c = colors[i];
				r += c.r;
				g += c.g;
				b += c.b;
				a += c.a;
			}
			return new Color(r, g, b, a);
		}
		public static Color32 Sum(params Color32[] colors) {
			int r = 0, g = 0, b = 0, a = 0;
			for (int i = 0; i < colors.Length; i++) {
				Color32 c = colors[i];
				r += c.r;
				g += c.g;
				b += c.b;
				a += c.a;
			}
			return new Color32((byte)r, (byte)g, (byte)b, (byte)a);
		}
		
		public static Color Average(params Color[] colors) {
			float r = 0f, g = 0f, b = 0f, a = 0f;
			for (int i = 0; i < colors.Length; i++) {
				Color c = colors[i];
				r += c.r;
				g += c.g;
				b += c.b;
				a += c.a;
			}
			float n = (float)colors.Length;
			return new Color(r/n, g/n, b/n, a/n);
		}
		public static Color32 Average(params Color32[] colors) {
			int r = 0, g = 0, b = 0, a = 0;
			for (int i = 0; i < colors.Length; i++) {
				Color32 c = colors[i];
				r += c.r;
				g += c.g;
				b += c.b;
				a += c.a;
			}
			double n = (double)colors.Length;
			return new Color32((byte)(r/n), (byte)(g/n), (byte)(b/n), (byte)(a/n));
		}
		
		public static Color Min(params Color[] colors) {
			Color c0 = colors[0];
			for (int i = 1; i < colors.Length; i++) {
				Color c = colors[i];
				if (c.r < c0.r) c0.r = c.r;
				if (c.g < c0.g) c0.g = c.g;
				if (c.b < c0.b) c0.b = c.b;
				if (c.a < c0.a) c0.a = c.a;
			}
			return c0;
		}
		public static Color32 Min(params Color32[] colors) {
			Color32 c0 = colors[0];
			for (int i = 1; i < colors.Length; i++) {
				Color32 c = colors[i];
				if (c.r < c0.r) c0.r = c.r;
				if (c.g < c0.g) c0.g = c.g;
				if (c.b < c0.b) c0.b = c.b;
				if (c.a < c0.a) c0.a = c.a;
			}
			return c0;
		}
		
		public static Color Max(params Color[] colors) {
			Color c0 = colors[0];
			for (int i = 1; i < colors.Length; i++) {
				Color c = colors[i];
				if (c.r > c0.r) c0.r = c.r;
				if (c.g > c0.g) c0.g = c.g;
				if (c.b > c0.b) c0.b = c.b;
				if (c.a > c0.a) c0.a = c.a;
			}
			return c0;
		}
		public static Color32 Max(params Color32[] colors) {
			Color32 c0 = colors[0];
			for (int i = 1; i < colors.Length; i++) {
				Color32 c = colors[i];
				if (c.r > c0.r) c0.r = c.r;
				if (c.g > c0.g) c0.g = c.g;
				if (c.b > c0.b) c0.b = c.b;
				if (c.a > c0.a) c0.a = c.a;
			}
			return c0;
		}
		
		public static float Grayscale(float r, float g, float b, GrayscaleMode mode=GrayscaleMode.Average) {
			switch (mode) {
			case (GrayscaleMode.Lightness):
				// http://docs.gimp.org/en/gimp-tool-desaturate.html
				return (Mathf.Max(r, g, b) + Mathf.Min(r, g, b))/2f;
			case (GrayscaleMode.Luminosity):
				// http://docs.gimp.org/en/gimp-tool-desaturate.html
				return (0.21f * r) + (0.72f * g) + (0.07f * b);
			case (GrayscaleMode.Average):
				// http://docs.gimp.org/en/gimp-tool-desaturate.html
				return (r + g + b)/3f;
			case (GrayscaleMode.Perceived):
				// http://alienryderflex.com/hsp.html
				return Mathf.Sqrt(0.241f*r*r + 0.691f*g*g + 0.068f*b*b);
			case (GrayscaleMode.W3C):
				// http://www.w3.org/TR/AERT#color-contrast
				return (r * 0.299f) + (g * 0.587f) + (b * 0.144f);
			default:
				return (r + g + b)/3f;
			}
		}
		public static Color Grayscale(this Color c, GrayscaleMode mode=GrayscaleMode.Average) {
			float v = Grayscale(c.r, c.g, c.b, mode);
			return new Color(v, v, v, c.a);
		}
		public static Color32 Grayscale(this Color32 c, GrayscaleMode mode=GrayscaleMode.Average) {
			byte v = (byte)(Grayscale(c.r/255f, c.g/255f, c.b/255f, mode)*255);
			return new Color32(v, v, v, c.a);
		}
	}

	public class ColorPalette {
		Color[] palette;
		int palette_size;
		public ColorPalette(Color[] palette) {
			this.palette = palette;
			palette_size = palette.Length;
		}
		public Color FindRGBA(Color c) {
			var best_dist = 5f; // (1^2)*4 + 1
			var best_color = c;
			for (int i = 0; i < palette_size; i++) {
				var pc = palette[i];
				var dr = c.r - pc.r;
				var dg = c.g - pc.g;
				var db = c.b - pc.b;
				var da = c.b - pc.b;
				var dist = dr*dr + dg*dg + db*db + da*da;
				if (dist < best_dist) {
					best_dist = dist;
					best_color = pc;
				}
			}
			return best_color;
		}
		public Color FindRGB(Color c) {
			var best_dist = 5f; // (1^2)*4 + 1
			var best_color = c;
			for (int i = 0; i < palette_size; i++) {
				var pc = palette[i];
				var dr = c.r - pc.r;
				var dg = c.g - pc.g;
				var db = c.b - pc.b;
				var dist = dr*dr + dg*dg + db*db;
				if (dist < best_dist) {
					best_dist = dist;
					best_color = pc;
				}
			}
			return best_color;
		}
		
		static float Color4_factor = 15f;
		static float Color5_factor = 31f;
		static float Color6_factor = 63f;
		public static Color RGBA4444(Color c) {
			c.r = ((int)(c.r * Color4_factor + 0.5f) / Color4_factor);
			c.g = ((int)(c.g * Color4_factor + 0.5f) / Color4_factor);
			c.b = ((int)(c.b * Color4_factor + 0.5f) / Color4_factor);
			c.a = ((int)(c.a * Color4_factor + 0.5f) / Color4_factor);
			return c;
		}
		public static Color RGB565(Color c) {
			c.r = ((int)(c.r * Color5_factor + 0.5f) / Color5_factor);
			c.g = ((int)(c.g * Color6_factor + 0.5f) / Color6_factor);
			c.b = ((int)(c.b * Color5_factor + 0.5f) / Color5_factor);
			c.a = 1f;
			return c;
		}
	}

	public class Color32Palette {
		Color32[] palette;
		int palette_size;
		public Color32Palette(Color32[] palette) {
			this.palette = palette;
			palette_size = palette.Length;
		}
		public Color32 FindRGBA(Color32 c) {
			var best_dist = 260101; // (255^2)*4 + 1
			var best_color = c;
			for (int i = 0; i < palette_size; i++) {
				var pc = palette[i];
				var dr = c.r - pc.r;
				var dg = c.g - pc.g;
				var db = c.b - pc.b;
				var da = c.b - pc.b;
				var dist = dr*dr + dg*dg + db*db + da*da;
				if (dist < best_dist) {
					best_dist = dist;
					best_color = pc;
				}
			}
			return best_color;
		}
		public Color32 FindRGB(Color32 c) {
			var best_dist = 260101; // (255^2)*4 + 1
			var best_color = c;
			for (int i = 0; i < palette_size; i++) {
				var pc = palette[i];
				var dr = c.r - pc.r;
				var dg = c.g - pc.g;
				var db = c.b - pc.b;
				var dist = dr*dr + dg*dg + db*db;
				if (dist < best_dist) {
					best_dist = dist;
					best_color = pc;
				}
			}
			return best_color;
		}

		static float Color4_factor = 15f / 255f;
		static float Color5_factor = 31f / 255f;
		static float Color6_factor = 63f / 255f;
		public static Color32 RGBA4444(Color32 c) {
			c.r = (byte)((int)(c.r * Color4_factor + 0.5f) / Color4_factor + 0.5f);
			c.g = (byte)((int)(c.g * Color4_factor + 0.5f) / Color4_factor + 0.5f);
			c.b = (byte)((int)(c.b * Color4_factor + 0.5f) / Color4_factor + 0.5f);
			c.a = (byte)((int)(c.a * Color4_factor + 0.5f) / Color4_factor + 0.5f);
			return c;
		}
		public static Color32 RGB565(Color32 c) {
			c.r = (byte)((int)(c.r * Color5_factor + 0.5f) / Color5_factor + 0.5f);
			c.g = (byte)((int)(c.g * Color6_factor + 0.5f) / Color6_factor + 0.5f);
			c.b = (byte)((int)(c.b * Color5_factor + 0.5f) / Color5_factor + 0.5f);
			c.a = 255;
			return c;
		}
	}

	public struct ColorHSV {
		public float h, s, v, a;

		public ColorHSV(float h, float s, float v, float a=1f) {
			this.h = h;
			this.s = s;
			this.v = v;
			this.a = a;
		}

		public void Sanitize() {
			h = ((h % 1f) + 1f) % 1f;
			s = Mathf.Clamp01(s);
			v = Mathf.Clamp01(v);
			a = Mathf.Clamp01(a);
		}

		// expects/returns values in range [0..1]
		public static implicit operator ColorHSV(Color c) {
			float minVal = Mathf.Min(Mathf.Min(c.r, c.g), c.b);
			float maxVal = Mathf.Max(Mathf.Max(c.r, c.g), c.b);
			float delta = maxVal - minVal;

			float v = maxVal;

			if (delta == 0f) return new ColorHSV(0f, 0f, v, c.a);
			float s = delta / maxVal;

			float h;
			if (c.r == maxVal) {
				h = (c.g - c.b) / (6f * delta);
			} else if (c.g == maxVal) {
				h = (1f / 3f) + (c.b - c.r) / (6f * delta);
			} else {
				h = (2f / 3f) + (c.r - c.g) / (6f * delta);
			}
			h = ((h % 1f) + 1f) % 1f;

			return new ColorHSV(h, s, v, c.a);
		}
		public static implicit operator ColorHSV(Color32 c) {
			return (Color)c;
		}

		// expects/returns values in range [0..1]
		public static implicit operator Color(ColorHSV c) {
			float s = Mathf.Clamp01(c.s);
			float v = Mathf.Clamp01(c.v);
			if (s == 0f) return new Color(v, v, v, c.a);
			float h = ((c.h % 1f) + 1f) % 1f;
			var var_h = h * 6f;
			var var_i = Mathf.FloorToInt(var_h);
			var var_1 = v * (1f - s);
			var var_2 = v * (1f - s * (var_h - var_i));
			var var_3 = v * (1f - s * (1f - (var_h - var_i)));

			switch (var_i) {
			case 1: return new Color(var_2, v, var_1, c.a);
			case 2: return new Color(var_1, v, var_3, c.a);
			case 3: return new Color(var_1, var_2, v, c.a);
			case 4: return new Color(var_3, var_1, v, c.a);
			case 5: return new Color(v, var_1, var_2, c.a);
			default: return new Color(v, var_3, var_1, c.a); // 0 or 6
			}
		}
		public static implicit operator Color32(ColorHSV c) {
			return (Color)c;
		}
	}

	public class Pixmap<T> {
		public T[] pixels;
		public int w;
		public int h;
		public Vector2i size { get {return new Vector2i(w, h);} }
		public int count { get { return w*h; } }
		public int width {get{return w;} set{w = value;}}
		public int height {get{return h;} set{h = value;}}

		public Pixmap() : this(null, 0, 0) {} // for serializability
		public Pixmap(int w, int h) : this(new T[w * h], w, h) {}
		public Pixmap(int w, int h, T value) {
			this.w = w;
			this.h = h;
			pixels = new T[w * h];
			for (int i = 0, iCnt = count; i < iCnt; i++) pixels[i] = value;
		}
		public Pixmap(T[] pixels, int w, int h) {
			this.w = w;
			this.h = h;
			this.pixels = pixels;
		}

		public T this[int x, int y] {
			get { return pixels[x + y * w]; }
			set { pixels[x + y * w] = value; }
		}
		public T Get(int x, int y, T outside) {
			if ((x < 0) || (x >= w) || (y < 0) || (y >= h)) return outside;
			return pixels[x + y * w];
		}
		public void Set(int x, int y, T value) {
			if ((x < 0) || (x >= w) || (y < 0) || (y >= h)) return;
			pixels[x + y * w] = value;
		}
		
		public int ToIndex(int x, int y) { return x + y * w; }
		public int ToIndex(Vector2i p) { return p.x + p.y * w; }
		
		public Vector2i ToPoint(int i) { int x = i % w, y = (i - x) / w; return new Vector2i(x, y); }
		public void ToPoint(int i, out int x, out int y) { x = i % w; y = (i - x) / w; }

		public Pixmap<T> Copy() {
			var result = new Pixmap<T>(w, h);
			Array.Copy(pixels, result.pixels, count);
			return result;
		}
		
		public void Clear() {
			Fill(default(T));
		}
		public void Fill(T v) {
			for (int i = 0, iCnt = count; i < iCnt; i++) {
				pixels[i] = v;
			}
		}
		public void Fill(System.Func<int, T> filler) {
			for (int i = 0, iCnt = count; i < iCnt; i++) {
				pixels[i] = filler(i);
			}
		}
		public void Fill(System.Func<int, int, T> filler) {
			for (int y = 0; y < h; y++) {
				int iy = y * w;
				for (int x = 0; x < w; x++) {
					int i = x + iy;
					pixels[i] = filler(x, y);
				}
			}
		}

		public Pixmap<T2> Convert<T2>(System.Func<T, T2> converter) {
			var result = new Pixmap<T2>(w, h);
			for (int i = 0, iCnt = count; i < iCnt; i++) {
				result.pixels[i] = converter(pixels[i]);
			}
			return result;
		}
		public Pixmap<T2> Convert<T2>(System.Func<int, T, T2> converter) {
			var result = new Pixmap<T2>(w, h);
			for (int i = 0, iCnt = count; i < iCnt; i++) {
				result.pixels[i] = converter(i, pixels[i]);
			}
			return result;
		}
		public Pixmap<T2> Convert<T2>(System.Func<int, int, T, T2> converter) {
			var result = new Pixmap<T2>(w, h);
			for (int y = 0; y < h; y++) {
				int iy = y * w;
				for (int x = 0; x < w; x++) {
					int i = x + iy;
					result.pixels[i] = converter(x, y, pixels[i]);
				}
			}
			return result;
		}
		public Pixmap<T2> Convert<T2>(System.Func<int, int, T, T2> converter, IntRect region, T outside) {
	//		int x0 = Mathf.Max(0, region.xMinAbs);
	//		int y0 = Mathf.Max(0, region.yMinAbs);
	//		int x1 = Mathf.Min(w, region.xMaxAbs);
	//		int y1 = Mathf.Min(h, region.yMaxAbs);
			int dx = region.xMinAbs;
			int dy = region.yMinAbs;
			int w2 = region.wAbs;
			int h2 = region.hAbs;
			var result = new Pixmap<T2>(w2, h2);
			for (int y2 = 0; y2 < h2; y2++) {
				int y = y2 + dy;
				int iy = y * w;
				int jy = y2 * w2;
				for (int x2 = 0; x2 < w2; x2++) {
					int x = x2 + dx;
					int i = x + iy;
					int j = x2 + jy;
					if ((x >= 0) && (x < w) && (y >= 0) && (y < w)) {
						result.pixels[j] = converter(x, y, pixels[i]);
					} else {
						result.pixels[j] = converter(x, y, outside);
					}
				}
			}
	//		for (int y = y0; y < y1; y++) {
	//			int iy = y * w;
	//			int jy = (y - dy) * w2;
	//			for (int x = x0; x < x1; x++) {
	//				int i = x + iy;
	//				int j = (x - dx) + jy;
	//				result.pixels[j] = converter(x, y, pixels[i]);
	//			}
	//		}
			return result;
		}

		public void Apply(System.Func<T, T> filter) {
			for (int i = 0, iCnt = count; i < iCnt; i++) {
				pixels[i] = filter(pixels[i]);
			}
		}
		public void Apply(System.Func<int, T, T> filter) {
			for (int i = 0, iCnt = count; i < iCnt; i++) {
				pixels[i] = filter(i, pixels[i]);
			}
		}
		public void Apply(System.Func<int, int, T, T> filter) {
			for (int y = 0; y < h; y++) {
				int iy = y * w;
				for (int x = 0; x < w; x++) {
					int i = x + iy;
					pixels[i] = filter(x, y, pixels[i]);
				}
			}
		}
		public void Apply(System.Func<int, int, T, T> filter, IntRect region) {
			int x0 = Mathf.Max(0, region.xMinAbs);
			int y0 = Mathf.Max(0, region.yMinAbs);
			int x1 = Mathf.Min(w, region.xMaxAbs);
			int y1 = Mathf.Min(h, region.yMaxAbs);
			for (int y = y0; y < y1; y++) {
				int iy = y * w;
				for (int x = x0; x < x1; x++) {
					int i = x + iy;
					pixels[i] = filter(x, y, pixels[i]);
				}
			}
		}

		public void Scan(System.Action<T> scanner) {
			for (int i = 0, iCnt = count; i < iCnt; i++) {
				scanner(pixels[i]);
			}
		}
		public void Scan(System.Action<int, T> scanner) {
			for (int i = 0, iCnt = count; i < iCnt; i++) {
				scanner(i, pixels[i]);
			}
		}
		public void Scan(System.Action<int, int, T> scanner) {
			for (int y = 0; y < h; y++) {
				int iy = y * w;
				for (int x = 0; x < w; x++) {
					int i = x + iy;
					scanner(x, y, pixels[i]);
				}
			}
		}
		public void Scan(System.Action<int, int, T> scanner, IntRect region) {
			int x0 = Mathf.Max(0, region.xMinAbs);
			int y0 = Mathf.Max(0, region.yMinAbs);
			int x1 = Mathf.Min(w, region.xMaxAbs);
			int y1 = Mathf.Min(h, region.yMaxAbs);
			for (int y = y0; y < y1; y++) {
				int iy = y * w;
				for (int x = x0; x < x1; x++) {
					int i = x + iy;
					scanner(x, y, pixels[i]);
				}
			}
		}

		public bool Scan(System.Func<T, bool> scanner, bool cmp) {
			for (int i = 0, iCnt = count; i < iCnt; i++) {
				if (scanner(pixels[i]) == cmp) return cmp;
			}
			return !cmp;
		}
		public bool Scan(System.Func<int, T, bool> scanner, bool cmp) {
			for (int i = 0, iCnt = count; i < iCnt; i++) {
				if (scanner(i, pixels[i]) == cmp) return cmp;
			}
			return !cmp;
		}
		public bool Scan(System.Func<int, int, T, bool> scanner, bool cmp) {
			for (int y = 0; y < h; y++) {
				int iy = y * w;
				for (int x = 0; x < w; x++) {
					int i = x + iy;
					if (scanner(x, y, pixels[i]) == cmp) return cmp;
				}
			}
			return !cmp;
		}
		public bool Scan(System.Func<int, int, T, bool> scanner, IntRect region, bool cmp) {
			int x0 = Mathf.Max(0, region.xMinAbs), y0 = Mathf.Max(0, region.yMinAbs);
			int x1 = Mathf.Min(w, region.xMaxAbs), y1 = Mathf.Min(h, region.yMaxAbs);
			for (int y = y0; y < y1; y++) {
				int iy = y * w;
				for (int x = x0; x < x1; x++) {
					int i = x + iy;
					if (scanner(x, y, pixels[i]) == cmp) return cmp;
				}
			}
			return !cmp;
		}

		public void ScanPerimeter(System.Action<T> scanner) {
			int x = 0, y = 0;
			y = 0;   for (x = 0; x < w; x++) { scanner(pixels[x+y*w]); }
			y = h-1; for (x = 0; x < w; x++) { scanner(pixels[x+y*w]); }
			x = 0;   for (y = 1; y < h-1; y++) { scanner(pixels[x+y*w]); }
			x = w-1; for (y = 1; y < h-1; y++) { scanner(pixels[x+y*w]); }
		}
		public void ScanPerimeter(System.Action<int, int, T> scanner) {
			int x = 0, y = 0;
			y = 0;   for (x = 0; x < w; x++) { scanner(x, y, pixels[x+y*w]); }
			y = h-1; for (x = 0; x < w; x++) { scanner(x, y, pixels[x+y*w]); }
			x = 0;   for (y = 1; y < h-1; y++) { scanner(x, y, pixels[x+y*w]); }
			x = w-1; for (y = 1; y < h-1; y++) { scanner(x, y, pixels[x+y*w]); }
		}
		public void ScanPerimeter(System.Action<int, int, T> scanner, IntRect region) {
			int x0 = Mathf.Max(0, region.xMinAbs), y0 = Mathf.Max(0, region.yMinAbs);
			int x1 = Mathf.Min(w, region.xMaxAbs), y1 = Mathf.Min(h, region.yMaxAbs);
			int x = x0, y = y0;
			y = y0;   for (x = x0; x < x1; x++) { scanner(x, y, pixels[x+y*w]); }
			y = y1-1; for (x = x0; x < x1; x++) { scanner(x, y, pixels[x+y*w]); }
			x = x0;   for (y = y0+1; y < y1-1; y++) { scanner(x, y, pixels[x+y*w]); }
			x = x1-1; for (y = y0+1; y < y1-1; y++) { scanner(x, y, pixels[x+y*w]); }
		}

		public bool ScanPerimeter(System.Func<T, bool> scanner, bool cmp) {
			int x = 0, y = 0;
			y = 0;   for (x = 0; x < w; x++) { if (scanner(pixels[x+y*w]) == cmp) return cmp; }
			y = h-1; for (x = 0; x < w; x++) { if (scanner(pixels[x+y*w]) == cmp) return cmp; }
			x = 0;   for (y = 1; y < h-1; y++) { if (scanner(pixels[x+y*w]) == cmp) return cmp; }
			x = w-1; for (y = 1; y < h-1; y++) { if (scanner(pixels[x+y*w]) == cmp) return cmp; }
			return !cmp;
		}
		public bool ScanPerimeter(System.Func<int, int, T, bool> scanner, bool cmp) {
			int x = 0, y = 0;
			y = 0;   for (x = 0; x < w; x++) { if (scanner(x, y, pixels[x+y*w]) == cmp) return cmp; }
			y = h-1; for (x = 0; x < w; x++) { if (scanner(x, y, pixels[x+y*w]) == cmp) return cmp; }
			x = 0;   for (y = 1; y < h-1; y++) { if (scanner(x, y, pixels[x+y*w]) == cmp) return cmp; }
			x = w-1; for (y = 1; y < h-1; y++) { if (scanner(x, y, pixels[x+y*w]) == cmp) return cmp; }
			return !cmp;
		}
		public bool ScanPerimeter(System.Func<int, int, T, bool> scanner, IntRect region, bool cmp) {
			int x0 = Mathf.Max(0, region.xMinAbs), y0 = Mathf.Max(0, region.yMinAbs);
			int x1 = Mathf.Min(w, region.xMaxAbs), y1 = Mathf.Min(h, region.yMaxAbs);
			int x = x0, y = y0;
			y = y0;   for (x = x0; x < x1; x++) { if (scanner(x, y, pixels[x+y*w]) == cmp) return cmp; }
			y = y1-1; for (x = x0; x < x1; x++) { if (scanner(x, y, pixels[x+y*w]) == cmp) return cmp; }
			x = x0;   for (y = y0+1; y < y1-1; y++) { if (scanner(x, y, pixels[x+y*w]) == cmp) return cmp; }
			x = x1-1; for (y = y0+1; y < y1-1; y++) { if (scanner(x, y, pixels[x+y*w]) == cmp) return cmp; }
			return !cmp;
		}

		public bool _AspectSizeSame(ref int w_new, ref int h_new, bool keep_aspect=false) {
			if (keep_aspect) {
				float ratio = Mathf.Min(w_new / (float)w, h_new / (float)h);
				w_new = (int)(w*ratio); h_new = (int)(h*ratio);
			}
			return ((w_new == w) & (h_new == h)) ;
		}

		public Pixmap<T> Resized(int w_new, int h_new, bool keep_aspect=false) {
			if (_AspectSizeSame(ref w_new, ref h_new, keep_aspect)) Copy();
			var res = new Pixmap<T>(w_new, h_new);
			var pixels_new = res.pixels;
			for (int y_new = 0; y_new < h_new; y_new++) {
				int iy_new = y_new * w_new;
				int iy = (y_new*h/h_new) * w;
				for (int x_new = 0; x_new < w_new; x_new++) {
					int i_new = x_new + iy_new;
					int i = (x_new*w/w_new) + iy;
					pixels_new[i_new] = pixels[i];
				}
			}
			return res;
		}
		public void Resize(int w_new, int h_new, bool keep_aspect=false) {
			if (_AspectSizeSame(ref w_new, ref h_new, keep_aspect)) return;
			var resized = Resized(w_new, h_new);
			pixels = resized.pixels; w = resized.w; h = resized.h;
		}

		public abstract class Accumulator<T> {
			public abstract void Reset();
			public abstract void Add(T value, float weight);
			public abstract T Get();
		}
		public Pixmap<T> Resampled(Accumulator<T> accumulator, int w_new, int h_new, bool keep_aspect=false) {
			if (_AspectSizeSame(ref w_new, ref h_new, keep_aspect)) Copy();
			MagicKernel.CalcKernels(w, h, w_new, h_new);
			var res = new Pixmap<T>(w_new, h_new);
			int i_new = 0;
			for (int y = 0; y < h_new; y++) {
				var ky = MagicKernel.ky[y];
				for (int x = 0; x < w_new; x++) {
					var kx = MagicKernel.kx[x];
					accumulator.Reset();
					for (int y0 = ky.min, yki = ky.ki; y0 <= ky.max; y0++, yki++) {
						float wy = MagicKernel.wy[yki];
						for (int x0 = kx.min, xki = kx.ki; x0 <= kx.max; x0++, xki++) {
							float wx = MagicKernel.wx[xki];
							accumulator.Add(pixels[x0+y0*w], wx*wy);
						}
					}
					res.pixels[i_new] = accumulator.Get(); i_new++;
				}
			}
			return res;
		}
		public void Resample(Accumulator<T> accumulator, int w_new, int h_new, bool keep_aspect=false) {
			if (_AspectSizeSame(ref w_new, ref h_new, keep_aspect)) return;
			var resampled = Resampled(accumulator, w_new, h_new);
			pixels = resampled.pixels; w = resampled.w; h = resampled.h;
		}

		public Pixmap<T> Subregion(IntRect rect) {
			int rx = rect.xMinAbs;
			int ry = rect.yMinAbs;
			int rw = rect.wAbs;
			int rh = rect.hAbs;
			T[] res_pixels = new T[rw*rh];
			
			int x_min = Mathf.Max(rx, 0);
			int y_min = Mathf.Max(ry, 0);
			int x_max = Mathf.Min(rx+rw, w);
			int y_max = Mathf.Min(ry+rh, h);
			
			int src_i = x_min + y_min * w;
			int dst_i = (x_min - rx) + (y_min - ry) * rw;
			int L = x_max - x_min;
			int src_end = x_min + y_max * w;
			while (src_i < src_end) {
				System.Array.Copy(pixels, src_i, res_pixels, dst_i, L);
				src_i += w;
				dst_i += rw;
			}
			
			return new Pixmap<T>(res_pixels, rw, rh);
		}
		
		public void Blit(Pixmap<T> pixmap, int x, int y) {
			int rw = pixmap.w;
			int rh = pixmap.h;
			
			int x_min = Mathf.Max(x, 0);
			int y_min = Mathf.Max(y, 0);
			int x_max = Mathf.Min(x+rw, w);
			int y_max = Mathf.Min(y+rh, h);
			
			int rx_min = x_min - x;
			int ry_min = y_min - y;
			//int rx_max = rx_min + (x_max - x_min); // not used
			int ry_max = ry_min + (y_max - y_min);
			
			int src_i = rx_min + ry_min * rw;
			int dst_i = x_min + y_min * w;
			int L = x_max - x_min;
			int src_end = rx_min + ry_max * rw;
			while (src_i < src_end) {
				System.Array.Copy(pixmap.pixels, src_i, pixels, dst_i, L);
				src_i += rw;
				dst_i += w;
			}
		}

		public void Blend<T2>(Pixmap<T2> dst, int ofsX, int ofsY,
		                  System.Func<T, T2, T2> operation, uint[] skipmap=null) {
			var dst_pixels = dst.pixels;
			Paint<T2>(dst, ofsX, ofsY, (j, i) => {dst_pixels[i] = 
				operation(pixels[j], dst_pixels[i]);}, skipmap);
		}
		public void Paint<T2>(Pixmap<T2> dst, int ofsX, int ofsY,
		                      System.Action<int, int> operation, uint[] skipmap=null) {
			int dst_w = dst.w, dst_h = dst.h;
			int x0 = Mathf.Max(0, ofsX);
			int y0 = Mathf.Max(0, ofsY);
			int x1 = Mathf.Min(dst_w, ofsX+w);
			int y1 = Mathf.Min(dst_h, ofsY+h);

			if (skipmap == null) {
				for (int y = y0; y < y1; y++) {
					int iy = y * dst_w;
					int jy = (y - ofsY) * w - ofsX;
					for (int x = x0; x < x1; x++) {
						operation(x + jy, x + iy);
					}
				}
			} else {
				for (int y = y0; y < y1; y++) {
					int iy = y * dst_w;
					int jy = (y - ofsY) * w - ofsX;
					for (int x = x0; x < x1; x++) {
					skip:;
						int j = x + jy;
						
						uint dxy = skipmap[j];
						if (dxy != 0) { // heuristic: dxy > N ?
							if (dxy == 1) continue;
							if (dxy == 0xFFFFFFFF) return;
							int dx = (short)(dxy & 0xFFFF);
							int dy = (short)(dxy >> 16);
							x += dx;
							if (dy != 0) { // expected to always be >= 0
								y += dy;
								if (y >= y1) return;
								iy = y * dst_w;
								jy = (y - ofsY) * w - ofsX;
							}
							if (x >= x1) break;
							if (x < x0) x = x0;
							goto skip;
						}
						
						operation(j, x + iy);
					}
				}
			}
		}

		public Pixmap<T> Extract(IntRect rect) {
			var res = new Pixmap<T>(rect.wAbs, rect.hAbs);
			res.Blit(this, -rect.xMinAbs, -rect.yMinAbs);
			return res;
		}
		public Pixmap<T> Extract(IntRect rect, T background) {
			var res = new Pixmap<T>(rect.wAbs, rect.hAbs, background);
			res.Blit(this, -rect.xMinAbs, -rect.yMinAbs);
			return res;
		}
		
		public IntRect CropRect(System.Predicate<T> predicate) {
			var crop_rect = new IntRect();
			int xmin, xmax, ymin, ymax;

			// Top
			for (int y = 0; y < h; y++) {
				int iy = y * w;
				for (int x = 0; x < w; x++) {
					if (predicate(pixels[x + iy])) {
						crop_rect.Encapsulate(x, y);
						goto skipTop;
					}
				}
			}
		skipTop:

			// Bottom
			ymax = crop_rect.yMax;
			for (int y = h-1; y > ymax; y--) {
				int iy = y * w;
				for (int x = w-1; x >= 0; x--) {
					if (predicate(pixels[x + iy])) {
						crop_rect.Encapsulate(x, y);
						goto skipBottom;
					}
				}
			}
		skipBottom:

			// Left
			xmin = crop_rect.xMin;
			ymin = crop_rect.yMin;
			ymax = crop_rect.yMax;
			for (int x = 0; x < xmin; x++) {
				for (int y = ymax; y > ymin; y--) {
					if (predicate(pixels[x + y * w])) {
						crop_rect.Encapsulate(x, y);
						goto skipLeft;
					}
				}
			}
		skipLeft:

			// Right
			xmax = crop_rect.xMax;
			ymin = crop_rect.yMin;
			ymax = crop_rect.yMax;
			for (int x = w-1; x > xmax; x--) {
				for (int y = ymin; y < ymax; y++) {
					if (predicate(pixels[x + y * w])) {
						crop_rect.Encapsulate(x, y);
						goto skipRight;
					}
				}
			}
		skipRight:

			// Make it immediately suitable for cropping
			crop_rect.xMax += 1;
			crop_rect.yMax += 1;
			return crop_rect;
		}

		public Pixmap<T> Crop(System.Predicate<T> predicate) {
			return Subregion(CropRect(predicate));
		}
		
		public Pixmap<Vector2i> DistanceField(System.Predicate<T> predicate) {
			var df = new Pixmap<Vector2i>(w, h);
			df.DistanceTransform(this, predicate);
			return df;
		}
		public Pixmap<int> DistanceField(System.Func<T, int> conversion) {
			var df = new Pixmap<int>(w, h);
			df.DistanceTransform(this, conversion);
			return df;
		}
		public Pixmap<float> DistanceField(System.Func<T, float> conversion) {
			var df = new Pixmap<float>(w, h);
			df.DistanceTransform(this, conversion);
			return df;
		}
		
		// Thinning adapted from Chasan's code
		// http://www.chasanc.com/content/view/64/97/
		public Pixmap<byte> Skeleton(System.Predicate<T> predicate, bool trim=true, int max_iterations=-1) {
			var data1 = new Pixmap<byte>(w, h);
			var data2 = new Pixmap<byte>(w, h);
			var positions = new int[w*h];
			int n_positions = 0;
			
			for (int i = 0, iCnt = count; i < iCnt; i++) {
				byte P = (byte)(predicate(pixels[i]) ? 1 : 0);
				data1.pixels[i] = P;
				data2.pixels[i] = P;
				if (P != 0) {
					positions[n_positions] = i;
					n_positions++;
				}
			}

			if (max_iterations < 0) max_iterations = int.MaxValue;

			bool flip = false;
			bool erode = true;
			int iterations = 0;
			while (erode && (iterations < max_iterations)) {
				erode = false;
				
				for (int ipos = n_positions-1; ipos >= 0; ipos--) {
					int pos = positions[ipos];
					int x, y;
					x = pos % w;
					y = (pos - x) / w;
					//y = Math.DivRem(pos, w, out x);
					
					byte P1 = data1[x, y];
					if (P1 == 0) {
						data2[x, y] = P1;
						if (n_positions > 0) {
							Array.Copy(positions, ipos+1, positions, ipos, n_positions-ipos-1);
							n_positions--;
						}
						continue;
					}
					
					byte P2 = data1.Get(x-1, y, 0);
					byte P3 = data1.Get(x-1, y+1, 0);
					byte P4 = data1.Get(x, y+1, 0);
					byte P5 = data1.Get(x+1, y+1, 0);
					byte P6 = data1.Get(x+1, y, 0);
					byte P7 = data1.Get(x+1, y-1, 0);
					byte P8 = data1.Get(x, y-1, 0);
					byte P9 = data1.Get(x-1, y-1, 0);
					
					bool c1, c2, c3, c4;
					int n;
					
					// Condititon 1: from 2 to 6 non-empty neghbours
					n = P2 + P3 + P4 + P5 + P6 + P7 + P8 + P9;
					c1 = (n > 1) && (n < 7);
					
					// Condititon 2: excatly 1 non-empty cluster of neighbours
					n = 0;
					if ((P3 - P2) == 1) n++;
					if ((P4 - P3) == 1) n++;
					if ((P5 - P4) == 1) n++;
					if ((P6 - P5) == 1) n++;
					if ((P7 - P6) == 1) n++;
					if ((P8 - P7) == 1) n++;
					if ((P9 - P8) == 1) n++;
					if ((P2 - P9) == 1) n++;
					c2 = (n == 1);
					
					// Conditions 3 and 4: triangle patterns of non-empty neighbours are absent
					if (flip) {
						c3 = ((P2 & P4 & P8) == 0); // left-triangle
						c4 = ((P2 & P6 & P8) == 0); // down-triangle
					} else {
						c3 = ((P2 & P4 & P6) == 0); // up-triangle
						c4 = ((P4 & P6 & P8) == 0); // right-triangle
					}

					bool condition = c1 && c2 && c3 && c4;
					if (condition) {
						byte _P2 = data2.Get(x-1, y, 0);
						byte _P3 = data2.Get(x-1, y+1, 0);
						byte _P4 = data2.Get(x, y+1, 0);
						byte _P5 = data2.Get(x+1, y+1, 0);
						byte _P6 = data2.Get(x+1, y, 0);
						byte _P7 = data2.Get(x+1, y-1, 0);
						byte _P8 = data2.Get(x, y-1, 0);
						byte _P9 = data2.Get(x-1, y-1, 0);
						byte _P_all = (byte)(_P2|_P3|_P4|_P5|_P6|_P7|_P8|_P9);
						// All neighbors were removed in this pass,
						// so this is the last point. It shouldn't
						// disappear, or the topology will change.
						if (_P_all == 0) condition = false;
					}

					if (condition) {
						if (P1 != 0) erode = true;
						data2[x, y] = 0;
					} else {
						data2[x, y] = P1;
					}
				}

				// We MUST copy the whole contents of current iteration
				// to avoid glitches caused by outdated data
				System.Array.Copy(data2.pixels, data1.pixels, data1.count);

				flip = !flip;
				iterations++;
			}
			
			if (trim) {
				for (int y = 0; y < h; y++) {
					for (int x = 0; x < w; x++) {
						byte P1 = data1[x, y];
						if (P1 == 0) continue;
						
						byte P2 = data1.Get(x-1, y, 0);
						byte P3 = data1.Get(x-1, y+1, 0);
						byte P4 = data1.Get(x, y+1, 0);
						byte P5 = data1.Get(x+1, y+1, 0);
						byte P6 = data1.Get(x+1, y, 0);
						byte P7 = data1.Get(x+1, y-1, 0);
						byte P8 = data1.Get(x, y-1, 0);
						byte P9 = data1.Get(x-1, y-1, 0);
						
						int pattern = (
							(P2 << 7) |
							(P3 << 6) |
							(P4 << 5) |
							(P5 << 4) |
							(P6 << 3) |
							(P7 << 2) |
							(P8 << 1) |
							(P9 << 0));
						
						bool unnecessary = false;
						
						// All rotations of corner pattern
						unnecessary |= (pattern == 130); // 10000010
						unnecessary |= (pattern == 160); // 10100000
						unnecessary |= (pattern == 040); // 00101000
						unnecessary |= (pattern == 010); // 00001010
						
						// All rotations of S and Z tetromino patterns
						unnecessary |= (pattern == 134); // 10000110
						unnecessary |= (pattern == 176); // 10110000
						unnecessary |= (pattern == 011); // 00001011
						unnecessary |= (pattern == 104); // 01101000
						unnecessary |= (pattern == 161); // 10100001
						unnecessary |= (pattern == 044); // 00101100
						unnecessary |= (pattern == 194); // 11000010
						unnecessary |= (pattern == 026); // 00011010
						
						if (unnecessary) data1[x, y] = 0;
					}
				}
			}
			
			return data1;
		}

		// Adapted from http://www.simdesign.nl/tips/tip002.html
		// CenterX, CenterY:
		//   The center of the disk (float precision). Note that [0, 0]
		//   would be the center of the first pixel. To draw in the
		//   exact middle of a 100x100 bitmap, use CenterX = 49.5 and
		//   CenterY = 49.5
		// Radius:
		//   The radius of the drawn disk in pixels (float precision)
		// Feather:
		//   The feather area. Use 1 pixel for a 1-pixel antialiased
		//   area. Pixel centers outside 'Radius + Feather / 2' become
		//   0, pixel centers inside 'Radius - Feather/2' become 255.
		//   Using a value of 0 will yield a bilevel image.
		// Copyright (c) 2003 Nils Haeck M.Sc. www.simdesign.nl
		public void ApplyCircle(float CenterX, float CenterY, float Radius, float Feather,
		                        System.Func<int, int, T, float, T> filter) {
			// Determine some helpful values (singles)
			float RPF2 = (Radius + Feather/2f); RPF2 *= RPF2;
			float RMF2 = (Radius - Feather/2f); RMF2 *= RMF2;
			
			// Determine bounds:
			int LX = Mathf.Max(Mathf.FloorToInt(CenterX - RPF2), 0);
			int RX = Mathf.Min(Mathf.CeilToInt(CenterX + RPF2), w - 1);
			int LY = Mathf.Max(Mathf.FloorToInt(CenterY - RPF2), 0);
			int RY = Mathf.Min(Mathf.CeilToInt(CenterY + RPF2), h - 1);
			
			// Optimization run: find squares of X first
			var SqX = new float[RX - LX + 1];
			for (int x = LX; x <= RX; x++) {
				float sqx = (x - CenterX); sqx *= sqx;
				SqX[x - LX] = sqx;
			}
			
			// Loop through Y values
			for (int y = LY; y <= RY; y++) {
				int iy = y * w;

				float SqY = (y - CenterY); SqY *= SqY;

				// Loop through X values
				for (int x = LX; x <= RX; x++) {
					int i = x + iy;

					// determine squared distance from center for this pixel
					float SqDist = SqY + SqX[x - LX];

					float factor = 1f; // "inside inner circle"
					if (SqDist >= RMF2) {
						if (SqDist < RPF2) {
							// We are inbetween the inner and outer bound
							factor = 0.5f + (Radius - Mathf.Sqrt(SqDist))/Feather;
						} else {
							factor = 0f; // "inside outer circle"
						}
					}

					pixels[i] = filter(x, y, pixels[i], factor);
				}
			}
		}

		// C# allows casting operators to be defined only in the corresponding class
		public static explicit operator Pixmap<T>(Texture tex) {
			var tex2D = (Texture2D)tex;
			int w = tex.width, h = tex.height;
			var TT = typeof(T);
			if (TT == typeof(Color)) {
				return (new Pixmap<Color>(tex2D.GetPixels(), w, h)) as Pixmap<T>;
			} else if (TT == typeof(Color32)) {
				return (new Pixmap<Color32>(tex2D.GetPixels32(), w, h)) as Pixmap<T>;
			}
			throw new ArgumentException("Texture->"+TT.Name+" conversion was not defined");
		}
	}

	public static class GraphicsExtensions {
		public static Vector2 size(this Texture tex) {
			return new Vector2(tex.width, tex.height);
		}

		public static Pixmap<Color> Resampled(this Pixmap<Color> pixmap, int w_new, int h_new, bool keep_aspect=false) {
			if (pixmap._AspectSizeSame(ref w_new, ref h_new, keep_aspect)) pixmap.Copy();
			MagicKernel.CalcKernels(pixmap.w, pixmap.h, w_new, h_new);
			var res = new Pixmap<Color>(w_new, h_new);
			int i_new = 0;
			for (int y = 0; y < h_new; y++) {
				var ky = MagicKernel.ky[y];
				for (int x = 0; x < w_new; x++) {
					var kx = MagicKernel.kx[x];
					Color Ay = default(Color);
					for (int y0 = ky.min, yki = ky.ki; y0 <= ky.max; y0++, yki++) {
						float wy = MagicKernel.wy[yki];
						Color Ax = default(Color);
						for (int x0 = kx.min, xki = kx.ki; x0 <= kx.max; x0++, xki++) {
							float wx = MagicKernel.wx[xki];
							var c = pixmap.pixels[x0+y0*pixmap.w];
							Ax.r += c.r * wx;
							Ax.g += c.g * wx;
							Ax.b += c.b * wx;
							Ax.a += c.a * wx;
						}
						Ay.r += Ax.r * wy;
						Ay.g += Ax.g * wy;
						Ay.b += Ax.b * wy;
						Ay.a += Ax.a * wy;
					}
					res.pixels[i_new] = Ay; i_new++;
				}
			}
			return res;
		}
		public static void Resample(this Pixmap<Color> pixmap, int w_new, int h_new, bool keep_aspect=false) {
			if (pixmap._AspectSizeSame(ref w_new, ref h_new, keep_aspect)) return;
			var resampled = pixmap.Resampled(w_new, h_new);
			pixmap.pixels = resampled.pixels; pixmap.w = resampled.w; pixmap.h = resampled.h;
		}
		public static Pixmap<Color> Crop(this Pixmap<Color> pixmap, float level = 0f) {
			return pixmap.Crop((c) => {return (c.a > level);});
		}
		public static Texture2D ToTexture(this Pixmap<Color> pixmap, Texture tex) {
			return pixmap.ToTexture(tex, false);
		}
		public static Texture2D ToTexture(this Pixmap<Color> pixmap, Texture tex, bool mipmap) {
			var tex2D = (Texture2D)tex;
			tex2D.SetPixels(pixmap.pixels);
			tex2D.Apply(mipmap);
			return tex2D;
		}
		public static Texture2D ToTexture(this Pixmap<Color> pixmap, bool mipmap=false) {
			//Texture2D tex = new Texture2D(pixmap.w, pixmap.h, TextureFormat.ARGB32, mipmap);
			// Seems like RGBA is universally supported format on all platforms
			// Note: however, for grabbing backbuffer ARGB32 or RGB24 is needed
			Texture2D tex = new Texture2D(pixmap.w, pixmap.h, TextureFormat.RGBA32, mipmap);
			tex.filterMode = FilterMode.Point;
			tex.wrapMode = TextureWrapMode.Clamp;
			return pixmap.ToTexture(tex, mipmap);
		}
		public static void FloydSteinberg(this Pixmap<Color> pixmap, TextureFormat format) {
			if ((format == TextureFormat.RGBA4444) || (format == TextureFormat.ARGB4444)) {
				pixmap.FloydSteinberg(ColorPalette.RGBA4444);
			} else if (format == TextureFormat.RGB565) {
				pixmap.FloydSteinberg(ColorPalette.RGB565);
			}
		}
		public static void FloydSteinberg(this Pixmap<Color> pixmap, Func<Color, Color> palette_converter) {
			int w = pixmap.w, h = pixmap.h;
			var pixels = pixmap.pixels;
			
			int max_x = w-1;
			int max_y = h-1;
			
			int diR = 1;
			int diDL = w-1;
			int diD = w;
			int diDR = w+1;
			
			float fR = 7f/16f;
			float fDL = 3f/16f;
			float fD = 5f/16f;
			float fDR = 1f/16f;

			for (int y = 0; y < h; y++) {
				bool not_max_y = (y < max_y);
				
				for (int x = 0; x < w; x++) {
					bool not_max_x = (x < max_x);
					
					int i = (x)+(y)*w;
					var oldpixel = pixels[i];
					var newpixel = palette_converter(oldpixel);
					pixels[i] = newpixel;

					var dr = oldpixel.r - newpixel.r;
					var dg = oldpixel.g - newpixel.g;
					var db = oldpixel.b - newpixel.b;
					var da = oldpixel.a - newpixel.a;

					if (not_max_x) {
						FloydSteinberg_AddError(pixels, i+diR, fR, dr, dg, db, da);
					}
					if (not_max_y) {
						if (x > 0) {
							FloydSteinberg_AddError(pixels, i+diDL, fDL, dr, dg, db, da);
						}
						FloydSteinberg_AddError(pixels, i+diD, fD, dr, dg, db, da);
						if (not_max_x) {
							FloydSteinberg_AddError(pixels, i+diDR, fDR, dr, dg, db, da);
						}
					}
				}
			}
		}
		static void FloydSteinberg_AddError(Color[] pixels, int i, float f, float dr, float dg, float db, float da) {
			var c = pixels[i];
			c.r += dr*f; c.g += dg*f; c.b += db*f; c.a += da*f;
			pixels[i] = c;
		}
		
		public static Pixmap<Color32> Resampled(this Pixmap<Color32> pixmap, int w_new, int h_new, bool keep_aspect=false) {
			if (pixmap._AspectSizeSame(ref w_new, ref h_new, keep_aspect)) pixmap.Copy();
			MagicKernel.CalcKernels(pixmap.w, pixmap.h, w_new, h_new);
			var res = new Pixmap<Color32>(w_new, h_new);
			Color32 c_res = default(Color32);
			int i_new = 0;
			for (int y = 0; y < h_new; y++) {
				var ky = MagicKernel.ky[y];
				for (int x = 0; x < w_new; x++) {
					var kx = MagicKernel.kx[x];
					Color Ay = default(Color);
					for (int y0 = ky.min, yki = ky.ki; y0 <= ky.max; y0++, yki++) {
						float wy = MagicKernel.wy[yki];
						Color Ax = default(Color);
						for (int x0 = kx.min, xki = kx.ki; x0 <= kx.max; x0++, xki++) {
							float wx = MagicKernel.wx[xki];
							var c = pixmap.pixels[x0+y0*pixmap.w];
							Ax.r += c.r * wx;
							Ax.g += c.g * wx;
							Ax.b += c.b * wx;
							Ax.a += c.a * wx;
						}
						Ay.r += Ax.r * wy;
						Ay.g += Ax.g * wy;
						Ay.b += Ax.b * wy;
						Ay.a += Ax.a * wy;
					}
					c_res.r = (byte)(Ay.r);
					c_res.g = (byte)(Ay.g);
					c_res.b = (byte)(Ay.b);
					c_res.a = (byte)(Ay.a);
					res.pixels[i_new] = c_res; i_new++;
				}
			}
			return res;
		}
		public static void Resample(this Pixmap<Color32> pixmap, int w_new, int h_new, bool keep_aspect=false) {
			if (pixmap._AspectSizeSame(ref w_new, ref h_new, keep_aspect)) return;
			var resampled = pixmap.Resampled(w_new, h_new);
			pixmap.pixels = resampled.pixels; pixmap.w = resampled.w; pixmap.h = resampled.h;
		}
		public static Pixmap<Color32> Crop(this Pixmap<Color32> pixmap, byte level = 0) {
			return pixmap.Crop((c) => {return (c.a > level);});
		}
		public static Texture2D ToTexture(this Pixmap<Color32> pixmap, Texture tex) {
			return pixmap.ToTexture(tex, false);
		}
		public static Texture2D ToTexture(this Pixmap<Color32> pixmap, Texture tex, bool mipmap) {
			var tex2D = (Texture2D)tex;
			tex2D.SetPixels32(pixmap.pixels);
			tex2D.Apply(mipmap);
			return tex2D;
		}
		public static Texture2D ToTexture(this Pixmap<Color32> pixmap, bool mipmap=false) {
			//Texture2D tex = new Texture2D(pixmap.w, pixmap.h, TextureFormat.ARGB32, mipmap);
			// Seems like RGBA is universally supported format on all platforms
			// Note: however, for grabbing backbuffer ARGB32 or RGB24 is needed
			Texture2D tex = new Texture2D(pixmap.w, pixmap.h, TextureFormat.RGBA32, mipmap);
			tex.filterMode = FilterMode.Point;
			tex.wrapMode = TextureWrapMode.Clamp;
			return pixmap.ToTexture(tex, mipmap);
		}
		public static void FloydSteinberg(this Pixmap<Color32> pixmap, TextureFormat format) {
			if ((format == TextureFormat.RGBA4444) || (format == TextureFormat.ARGB4444)) {
				pixmap.FloydSteinberg(Color32Palette.RGBA4444);
			} else if (format == TextureFormat.RGB565) {
				pixmap.FloydSteinberg(Color32Palette.RGB565);
			}
		}
		public static void FloydSteinberg(this Pixmap<Color32> pixmap, Func<Color32, Color32> palette_converter) {
			int w = pixmap.w, h = pixmap.h;
			var pixels = pixmap.pixels;
			
			int max_x = w-1;
			int max_y = h-1;
			
			int diR = 1;
			int diDL = w-1;
			int diD = w;
			int diDR = w+1;
			
			float fR = 7f/16f;
			float fDL = 3f/16f;
			float fD = 5f/16f;
			float fDR = 1f/16f;
			
			for (int y = 0; y < h; y++) {
				bool not_max_y = (y < max_y);
				
				for (int x = 0; x < w; x++) {
					bool not_max_x = (x < max_x);
					
					int i = (x)+(y)*w;
					var oldpixel = pixels[i];
					var newpixel = palette_converter(oldpixel);
					pixels[i] = newpixel;

					var dr = oldpixel.r - newpixel.r;
					var dg = oldpixel.g - newpixel.g;
					var db = oldpixel.b - newpixel.b;
					var da = oldpixel.a - newpixel.a;
					
					if (not_max_x) {
						FloydSteinberg_AddError(pixels, i+diR, fR, dr, dg, db, da);
					}
					if (not_max_y) {
						if (x > 0) {
							FloydSteinberg_AddError(pixels, i+diDL, fDL, dr, dg, db, da);
						}
						FloydSteinberg_AddError(pixels, i+diD, fD, dr, dg, db, da);
						if (not_max_x) {
							FloydSteinberg_AddError(pixels, i+diDR, fDR, dr, dg, db, da);
						}
					}
				}
			}
		}
		static void FloydSteinberg_AddError(Color32[] pixels, int i, float f, int dr, int dg, int db, int da) {
			var c = pixels[i];
			c.r = (byte)Mathf.Clamp(c.r + dr*f + 0.5f, 0, 255);
			c.g = (byte)Mathf.Clamp(c.g + dg*f + 0.5f, 0, 255);
			c.b = (byte)Mathf.Clamp(c.b + db*f + 0.5f, 0, 255);
			c.a = (byte)Mathf.Clamp(c.a + da*f + 0.5f, 0, 255);
			pixels[i] = c;
		}
		public static byte[] ToBytes(this Pixmap<Color32> pixmap, TextureFormat format) {
			return pixmap.ToBytes(null, 0, format);
		}
		public static byte[] ToBytes(this Pixmap<Color32> pixmap, byte[] data, TextureFormat format) {
			return pixmap.ToBytes(data, 0, format);
		}
		public static byte[] ToBytes(this Pixmap<Color32> pixmap, byte[] data=null, int offset=0, TextureFormat format=TextureFormat.RGBA32) {
			var pixels = pixmap.pixels;
			int n = pixmap.count;
			int j = offset;
			switch (format) {
			case TextureFormat.RGBA32:
				if (data == null) data = new byte[n*4];
				var gch = GCHandle.Alloc(pixels, GCHandleType.Pinned);
				Marshal.Copy(gch.AddrOfPinnedObject(), data, j, n*4);
				gch.Free();
				return data;
			case TextureFormat.ARGB32:
				if (data == null) data = new byte[n*4];
				for (int i = 0; i < n; i++) {
					var c = pixels[i];
					data[j++] = c.a;
					data[j++] = c.r;
					data[j++] = c.g;
					data[j++] = c.b;
				}
				return data;
			case TextureFormat.BGRA32:
				if (data == null) data = new byte[n*4];
				for (int i = 0; i < n; i++) {
					var c = pixels[i];
					data[j++] = c.b;
					data[j++] = c.g;
					data[j++] = c.r;
					data[j++] = c.a;
				}
				return data;
			case TextureFormat.RGB24:
				if (data == null) data = new byte[n*3];
				for (int i = 0; i < n; i++) {
					var c = pixels[i];
					data[j++] = c.r;
					data[j++] = c.g;
					data[j++] = c.b;
				}
				return data;
			case TextureFormat.Alpha8:
				if (data == null) data = new byte[n];
				for (int i = 0; i < n; i++) {
					var c = pixels[i];
					data[j++] = c.a;
				}
				return data;
			case TextureFormat.RGBA4444:
				if (data == null) data = new byte[n*2];
				for (int i = 0; i < n; i++) {
					var c = pixels[i];
					//data[j++] = (byte)(((c.b >> 4) << 4) | (c.a >> 4));
					//data[j++] = (byte)(((c.r >> 4) << 4) | (c.g >> 4));
					data[j++] = (byte)(((c.b*15/255) << 4) | (c.a*15/255));
					data[j++] = (byte)(((c.r*15/255) << 4) | (c.g*15/255));
				}
				return data;
			case TextureFormat.ARGB4444:
				if (data == null) data = new byte[n*2];
				for (int i = 0; i < n; i++) {
					var c = pixels[i];
					//data[j++] = (byte)(((c.g >> 4) << 4) | (c.b >> 4));
					//data[j++] = (byte)(((c.a >> 4) << 4) | (c.r >> 4));
					data[j++] = (byte)(((c.g*15/255) << 4) | (c.b*15/255));
					data[j++] = (byte)(((c.a*15/255) << 4) | (c.r*15/255));
				}
				return data;
			case TextureFormat.RGB565:
				if (data == null) data = new byte[n*2];
				for (int i = 0; i < n; i++) {
					var c = pixels[i];
					//int w = ((c.r >> 3) << 11) | ((c.g >> 2) << 5) | (c.b >> 3);
					int w = ((c.r*31/255) << 11) | ((c.g*63/255) << 5) | (c.b*31/255);
					data[j++] = (byte)(w & 0xFF);
					data[j++] = (byte)(w >> 8);
				}
				return data;
			default:
				throw new ArgumentException("Pixmap<Color32>.ToBytes(): "+format+" not supported");
			}
		}
		public static void FromBytes(this Pixmap<Color32> pixmap, byte[] data, TextureFormat format) {
			pixmap.FromBytes(data, 0, format);
		}
		public static void FromBytes(this Pixmap<Color32> pixmap, byte[] data, int offset=0, TextureFormat format=TextureFormat.RGBA32) {
			var pixels = pixmap.pixels;
			int n = pixmap.count;
			int j = offset;
			Color32 c = new Color32(0, 0, 0, 255);
			switch (format) {
			case TextureFormat.RGBA32:
				var gch = GCHandle.Alloc(pixmap.pixels, GCHandleType.Pinned);
				Marshal.Copy(data, j, gch.AddrOfPinnedObject(), n*4);
				gch.Free();
				break;
			case TextureFormat.ARGB32:
				for (int i = 0; i < n; i++) {
					c.a = data[j++];
					c.r = data[j++];
					c.g = data[j++];
					c.b = data[j++];
					pixels[i] = c;
				}
				break;
			case TextureFormat.BGRA32:
				for (int i = 0; i < n; i++) {
					c.b = data[j++];
					c.g = data[j++];
					c.r = data[j++];
					c.a = data[j++];
					pixels[i] = c;
				}
				break;
			case TextureFormat.RGB24:
				for (int i = 0; i < n; i++) {
					c.r = data[j++];
					c.g = data[j++];
					c.b = data[j++];
					pixels[i] = c;
				}
				break;
			case TextureFormat.Alpha8:
				for (int i = 0; i < n; i++) {
					c.a = data[j++];
					pixels[i] = c;
				}
				break;
			case TextureFormat.RGBA4444:
				for (int i = 0; i < n; i++) {
					byte b0 = data[j++];
					byte b1 = data[j++];
					//c.b = (byte)((b0 >> 4) << 4);
					//c.a = (byte)(b0 << 4);
					//c.r = (byte)((b1 >> 4) << 4);
					//c.g = (byte)(b1 << 4);
					c.b = (byte)((b0 >> 4)*255/15);
					c.a = (byte)((b0 & 0xF)*255/15);
					c.r = (byte)((b1 >> 4)*255/15);
					c.g = (byte)((b1 & 0xF)*255/15);
					pixels[i] = c;
				}
				break;
			case TextureFormat.ARGB4444:
				for (int i = 0; i < n; i++) {
					byte b0 = data[j++];
					byte b1 = data[j++];
					//c.g = (byte)((b0 >> 4) << 4);
					//c.b = (byte)(b0 << 4);
					//c.a = (byte)((b1 >> 4) << 4);
					//c.r = (byte)(b1 << 4);
					c.g = (byte)((b0 >> 4)*255/15);
					c.b = (byte)((b0 & 0xF)*255/15);
					c.a = (byte)((b1 >> 4)*255/15);
					c.r = (byte)((b1 & 0xF)*255/15);
					pixels[i] = c;
				}
				break;
			case TextureFormat.RGB565:
				for (int i = 0; i < n; i++) {
					int w = data[j++] | (data[j++] << 8);
					//c.r = (byte)(((w & 0xF800) >> 11) << 3);
					//c.g = (byte)(((w & 0x7E0) >> 5) << 2);
					//c.b = (byte)((w & 0x1F) << 3);
					c.r = (byte)(((w & 0xF800) >> 11)*255/31);
					c.g = (byte)(((w & 0x7E0) >> 5)*255/63);
					c.b = (byte)((w & 0x1F)*255/31);
					pixels[i] = c;
				}
				break;
			default:
				throw new ArgumentException("Pixmap<Color32>.FromBytes(): "+format+" not supported");
			}
		}

		// Danielsson's 8SED algorithm
		// Adapted from http://www.codersnotes.com/notes/signed-distance-fields
		public static void DistanceTransform(this Pixmap<Vector2i> df_pixmap) {
			df_pixmap.DistanceTransform<int>(null, null);
		}
		public static void DistanceTransform<T>(this Pixmap<Vector2i> df_pixmap, Pixmap<T> pixmap,
		                                          System.Predicate<T> predicate, int ofsX=0, int ofsY=0) {
			int w = df_pixmap.w, h = df_pixmap.h;
			var df = df_pixmap.pixels;
			int empty_w = w*3, empty_h = h*3; // reliably large enough
			Vector2i inside = new Vector2i(0, 0);
			Vector2i empty = new Vector2i(empty_w, empty_h);

			bool use_pixmap = (pixmap != null);
			bool use_pixmap_i = use_pixmap;
			T[] pixels = null;
			int pw = 0, ph = 0;
			if (use_pixmap) {
				pw = pixmap.w;
				ph = pixmap.h;
				pixels = pixmap.pixels;
				use_pixmap_i &= ((pw == w) && (ph == h));
				use_pixmap_i &= ((ofsX == 0) && (ofsY == 0));
			}

			int xmax = w-1;
			int ymax = h-1;

			for (int y = 0; y < h; y++) {
				bool b_y = (y > 0);
				int iy = y * w;
				for (int x = 0; x < w; x++) {
					bool b_x = (x > 0);
					bool _b_x = (x < xmax);
					int i = x + iy;

					Vector2i c;
					if (use_pixmap) {
						if (use_pixmap_i) {
							c = (predicate(pixels[i]) ? inside : empty);
						} else {
							int _x = x - ofsX, _y = y - ofsY;
							if ((_x >= 0) && (_x < pw) && (_y >= 0) && (_y < ph)) {
								c = (predicate(pixels[_x+_y*pw]) ? inside : empty);
							} else {
								c = empty;
							}
						}
					} else {
						c = df[i];
						if ((c.x != 0) || (c.y != 0)) c = empty;
					}

					Vector2i p;
					if (b_x) {
						p = df[i-1]; p.x--;
						if (p.sqrMagnitude < c.sqrMagnitude) c = p;
					}
					if (b_y) {
						p = df[i-w]; p.y--;
						if (p.sqrMagnitude < c.sqrMagnitude) c = p;
					}
					if (b_x && b_y) {
						p = df[i-1-w]; p.x--; p.y--;
						if (p.sqrMagnitude < c.sqrMagnitude) c = p;
					}
					if (_b_x && b_y) {
						p = df[i+1-w]; p.x++; p.y--;
						if (p.sqrMagnitude < c.sqrMagnitude) c = p;
					}

					df[i] = c;
				}
				for (int x = xmax; x >= 0; x--) {
					bool b_x = (x < xmax);
					int i = x + iy;
					Vector2i c = df[i];
					if (b_x) {
						Vector2i p = df[i+1]; p.x++;
						if (p.sqrMagnitude < c.sqrMagnitude) c = p;
					}
					df[i] = c;
				}
			}

			for (int y = ymax; y >= 0; y--) {
				bool b_y = (y < ymax);
				int iy = y * w;
				for (int x = xmax; x >= 0; x--) {
					bool b_x = (x < xmax);
					bool _b_x = (x > 0);
					int i = x + iy;
					
					Vector2i c = df[i];

					Vector2i p;
					if (b_x) {
						p = df[i+1]; p.x++;
						if (p.sqrMagnitude < c.sqrMagnitude) c = p;
					}
					if (b_y) {
						p = df[i+w]; p.y++;
						if (p.sqrMagnitude < c.sqrMagnitude) c = p;
					}
					if (b_x && b_y) {
						p = df[i+1+w]; p.x++; p.y++;
						if (p.sqrMagnitude < c.sqrMagnitude) c = p;
					}
					if (_b_x && b_y) {
						p = df[i-1+w]; p.x--; p.y++;
						if (p.sqrMagnitude < c.sqrMagnitude) c = p;
					}

					df[i] = c;
				}
				for (int x = 0; x < w; x++) {
					bool b_x = (x > 0);
					int i = x + iy;
					Vector2i c = df[i];
					if (b_x) {
						Vector2i p = df[i-1]; p.x--;
						if (p.sqrMagnitude < c.sqrMagnitude) c = p;
					}
					df[i] = c;
				}
			}
		}

		// Meijster's algorithm for Educlidean Distance Transform
		// Has the best speed*ease-of-implementation among surveyed in this paper:
		// http://www.lems.brown.edu/~rfabbri/stuff/fabbri-EDT-survey-ACMCSurvFeb2008.pdf
		// The source code is adapted from the ANIMAL imaging library
		// http://animal.sourceforge.net/
		// 0 is seed pixels, 1 is where distance should be calculated
		public static void DistanceTransform<T>(this Pixmap<int> df, Pixmap<T> pixmap, System.Func<T, int> conversion) {
			var pixels = pixmap.pixels;
			var df_pixels = df.pixels;
			int n = df.count;
			int maxvalue = (df.w+1)*(df.w+1) + (df.h+1)*(df.h+1) + 1;
			for (int i = 0; i < n; i++) {
				int v = conversion(pixels[i]);
				df_pixels[i] = ((v >= 0) ? v : maxvalue);
			}
			df.DistanceTransform();
		}
		public static void DistanceTransform(this Pixmap<int> im) {
			int h = im.h, w = im.w;
			var pixels = im.pixels;
			int n = im.count;

			// Vertical columnwise EDT
			// Lotufo's 1D EDT is equivalent to Meijster's scans 1 and 2.
			// What really matters is the 2nd stage.
			for (int x = 0; x < w; x++) {
				int b = 1;
				int i = x + w;
				//for (int y = 1; y < h; y++) {
				while (i < n) {
					var xy1b = pixels[i-w] + b;
					if (pixels[i] > xy1b) {
						pixels[i] = xy1b;
						b += 2;
					} else {
						b = 1;
					}
					i += w;
				}
				b = 1;
				i -= (w+w); // i = x + (h-2)*w
				//for (int y = h-2; y >= 0; y--) {
				while (i >= 0) {
					var xy1b = pixels[i+w] + b;
					if (pixels[i] > xy1b) {
						pixels[i] = xy1b;
						b += 2;
					} else {
						b = 1;
					}
					i -= w;
				}
			}

			var s = new int[w];
			var t = new int[w];
			var row_copy = new int[w];

			int iy = 0;
			for (int y = 0; y < h; y++) {
				// s is a parabola origin
				// t is the point of intersection with other parabola
				int q, sq, tq;
				q = sq = tq = s[0] = t[0] = 0;

				for (int x = 1; x < w; ++x) {
					var im_y_x = pixels[x+iy];

					while ((tq-sq)*(tq-sq) + pixels[sq+iy] > (tq-x)*(tq-x) + im_y_x) {
						if (--q == -1) break;
						sq = s[q];
						tq = t[q];
					}

					if (q == -1) {
						q = 0; s[0] = sq = x; tq = t[0];
					} else {
						int tx = 1 + (((x*x + im_y_x) - (sq*sq + pixels[sq+iy])) / (2*(x - sq)));
						if (tx < w) {
							++q;
							s[q] = sq = x;
							t[q] = tq = tx;
						}
					}
				}

				Array.Copy(pixels, iy, row_copy, 0, w);

				for (int x = w-1; x != -1; --x) {
					pixels[x+iy] = (x-sq)*(x-sq) + row_copy[sq];
					if (x == tq) {
						if (--q == -1) break;
						sq = s[q];
						tq = t[q];
					}
				}

				iy += w;
			}
		}

		// Adapted from http://cs.brown.edu/~pff/dt/
		// 0 is seed pixels, INF is where distance should be calculated
		public static void DistanceTransform<T>(this Pixmap<float> df, Pixmap<T> pixmap, System.Func<T, float> conversion) {
			var pixels = pixmap.pixels;
			var df_pixels = df.pixels;
			int n = df.count;
			float maxvalue = (df.w+1)*(df.w+1) + (df.h+1)*(df.h+1) + 1;
			for (int i = 0; i < n; i++) {
				float v = conversion(pixels[i]);
				df_pixels[i] = ((v >= 0) ? v : maxvalue);
			}
			df.DistanceTransform();
		}
		public static void DistanceTransform(this Pixmap<float> im) {
			int w = im.w, h = im.h;
			int wh_max = Mathf.Max(w, h);
			var pixels = im.pixels;
			var f = new float[wh_max];
			//var d = new float[wh_max]; // not used
			var v = new int[wh_max];
			var z = new float[wh_max+1];

			const float INF = 1e20f;
			
			// dt of 2d function using squared distance
			// transform along columns
			for (int x = 0; x < w; x++) {
				int i = x;
				for (int y = 0; y < h; y++) {f[y] = pixels[i]; i += w;}

				// dt of 1d function using squared distance
				int n = h;
				v[0] = 0; z[0] = -INF; z[1] = +INF;
				int k = 0;
				for (int q = 1; q < n; q++) {
					var qq_fq = q*q + f[q];
					while (true) {
						var vk = v[k];
						var s = (qq_fq - (vk*vk + f[vk])) / (2*(q - vk));
						if (s > z[k]) {k++; v[k] = q; z[k] = s; z[k+1] = +INF; break;}
						k--;
					}
				}
				k = 1;
				i = x;
				for (int q = 0; q < n; q++) {
					while (z[k] < q) k++;
					var vk = v[k-1];
					pixels[i] = (q-vk)*(q-vk) + f[vk];
					i += w;
				}
			}
			// transform along rows
			for (int y = 0; y < h; y++) {
				int iy = y * w;
				Array.Copy(pixels, iy, f, 0, w);

				// dt of 1d function using squared distance
				int n = w;
				v[0] = 0; z[0] = -INF; z[1] = +INF;
				int k = 0;
				for (int q = 1; q < n; q++) {
					var qq_fq = q*q + f[q];
					while (true) {
						var vk = v[k];
						var s = (qq_fq - (vk*vk + f[vk])) / (2*(q - vk));
						if (s > z[k]) {k++; v[k] = q; z[k] = s; z[k+1] = +INF; break;}
						k--;
					}
				}
				k = 1;
				for (int q = 0; q < n; q++) {
					while (z[k] < q) k++;
					var vk = v[k-1];
					pixels[q+iy] = (q-vk)*(q-vk) + f[vk];
				}
			}
		}
	}

	public static class MagicKernel {
		public struct KernelInfo { public short ki, min; public int max; }
		public static KernelInfo[] kx = new KernelInfo[8192];
		public static KernelInfo[] ky = new KernelInfo[8192];
		public static float[] wx = new float[32768];
		public static float[] wy = new float[32768];
		// Adapted from Magic Kernel (http://johncostella.webs.com/magic/)
		public static void CalcKernels(int w, int h, int w_new, int h_new) {
			CalcKernels(w_new, w, kx, wx);
			CalcKernels(h_new, h, ky, wy);
		}
		public static void CalcKernels(int len, int len0, KernelInfo[] kernel_i, float[] kernel_w) {
			float scale = len / (float)len0;
			float step = 1.0f / scale;
			float start = 0.5f * (step - 1.0f);
			var k = default(KernelInfo);
			for (int i = 0, ki = 0; i < len; ++i) {
				float x = start + i * step;
				if (step < 1f) {
					int j = Mathf.RoundToInt(x);
					k.ki = (short)ki;
					k.min = (short)(j - 1);
					k.max = (short)(j + 1);
					float f = x - j, w = 0.75f - f * f;
					float fn = f - 0.5f, wn = 0.5f * fn * fn;
					float fp = f + 0.5f, wp = 0.5f * fp * fp;
					if (k.min < 0) {
						k.min = 0;
						if (k.max >= len0) {
							k.max = len0-1;
							kernel_w[ki] = w+wn+wp; ++ki;
						} else {
							kernel_w[ki] = w+wn; ++ki;
							kernel_w[ki] = wp; ++ki;
						}
					} else {
						if (k.max >= len0) {
							k.max = len0-1;
							kernel_w[ki] = wn; ++ki;
							kernel_w[ki] = w+wp; ++ki;
						} else {
							kernel_w[ki] = wn; ++ki;
							kernel_w[ki] = w; ++ki;
							kernel_w[ki] = wp; ++ki;
						}
					}
				} else {
					float hw = 1.5f * step;
					k.ki = (short)ki;
					k.min = (short)Mathf.CeilToInt(x - hw);
					k.max = (short)Mathf.FloorToInt(x + hw);
					float sum = 0;
					for (int j = k.min; j <= k.max; ++j) {
						float p = (j - x) * scale; if (p < 0f) p = -p;
						float w;
						if (p > 0.5f) {
							float z = p - 1.5f;
							w = 0.5f * z * z;
						} else {
							w = 0.75f - p * p;
						}
						sum += w;
						if ((j == k.min) | (j > 0) | (j < len0)) {
							kernel_w[ki] = w; ++ki;
						} else {
							kernel_w[ki] += w;
						}
					}
					if (k.min < 0) k.min = 0;
					if (k.max >= len0) k.max = len0-1;
					float norm = 1f / sum;
					for (int j = k.min, _ki = k.ki; j <= k.max; ++j, ++_ki) {
						kernel_w[_ki] *= norm;
					}
				}
				kernel_i[i] = k;
			}
		}
	}

	public class ByteAccumulator : Pixmap<byte>.Accumulator<byte> {
		float acc;
		public override void Reset() { acc = 0f; }
		public override void Add(byte value, float weight) { acc += value * weight; }
		public override byte Get() { return (byte)(acc+0.5f); }
	}
	public class SByteAccumulator : Pixmap<sbyte>.Accumulator<sbyte> {
		float acc;
		public override void Reset() { acc = 0f; }
		public override void Add(sbyte value, float weight) { acc += value * weight; }
		public override sbyte Get() { return (sbyte)(acc+0.5f); }
	}
	public class UShortAccumulator : Pixmap<ushort>.Accumulator<ushort> {
		float acc;
		public override void Reset() { acc = 0f; }
		public override void Add(ushort value, float weight) { acc += value * weight; }
		public override ushort Get() { return (ushort)(acc+0.5f); }
	}
	public class ShortAccumulator : Pixmap<short>.Accumulator<short> {
		float acc;
		public override void Reset() { acc = 0f; }
		public override void Add(short value, float weight) { acc += value * weight; }
		public override short Get() { return (short)(acc+0.5f); }
	}
	public class UIntAccumulator : Pixmap<uint>.Accumulator<uint> {
		double acc;
		public override void Reset() { acc = 0f; }
		public override void Add(uint value, float weight) { acc += value * weight; }
		public override uint Get() { return (uint)(acc+0.5); }
	}
	public class IntAccumulator : Pixmap<int>.Accumulator<int> {
		double acc;
		public override void Reset() { acc = 0f; }
		public override void Add(int value, float weight) { acc += value * weight; }
		public override int Get() { return (int)(acc+0.5); }
	}
	public class ULongAccumulator : Pixmap<ulong>.Accumulator<ulong> {
		double acc;
		public override void Reset() { acc = 0f; }
		public override void Add(ulong value, float weight) { acc += value * weight; }
		public override ulong Get() { return (ulong)(acc+0.5); }
	}
	public class LongAccumulator : Pixmap<long>.Accumulator<long> {
		double acc;
		public override void Reset() { acc = 0f; }
		public override void Add(long value, float weight) { acc += value * weight; }
		public override long Get() { return (long)(acc+0.5); }
	}
	public class FloatAccumulator : Pixmap<float>.Accumulator<float> {
		float acc;
		public override void Reset() { acc = 0f; }
		public override void Add(float value, float weight) { acc += value * weight; }
		public override float Get() { return acc; }
	}
	public class DoubleAccumulator : Pixmap<double>.Accumulator<double> {
		double acc;
		public override void Reset() { acc = 0f; }
		public override void Add(double value, float weight) { acc += value * weight; }
		public override double Get() { return acc; }
	}
	public class ColorAccumulator : Pixmap<Color>.Accumulator<Color> {
		Color c;
		public override void Reset() {
			c = default(Color);
		}
		public override void Add(Color value, float weight) {
			c.r += value.r * weight;
			c.g += value.g * weight;
			c.b += value.b * weight;
			c.a += value.a * weight;
		}
		public override Color Get() {
			return c;
		}
	}
	public class Color32Accumulator : Pixmap<Color32>.Accumulator<Color32> {
		Color c;
		public override void Reset() {
			c = default(Color);
		}
		public override void Add(Color32 value, float weight) {
			c.r += value.r * weight;
			c.g += value.g * weight;
			c.b += value.b * weight;
			c.a += value.a * weight;
		}
		public override Color32 Get() {
			Color32 res;
			res.r = (byte)(c.r+0.5f); res.g = (byte)(c.g+0.5f); res.b = (byte)(c.b+0.5f); res.a = (byte)(c.a+0.5f);
			return res;
		}
	}
	public class Vector4Accumulator : Pixmap<Vector4>.Accumulator<Vector4> {
		Vector4 v;
		public override void Reset() {
			v = default(Vector4);
		}
		public override void Add(Vector4 value, float weight) {
			v.x += value.x * weight;
			v.y += value.y * weight;
			v.z += value.z * weight;
			v.w += value.w * weight;
		}
		public override Vector4 Get() {
			return v;
		}
	}
	public class Vector3Accumulator : Pixmap<Vector3>.Accumulator<Vector3> {
		Vector3 v;
		public override void Reset() {
			v = default(Vector3);
		}
		public override void Add(Vector3 value, float weight) {
			v.x += value.x * weight;
			v.y += value.y * weight;
			v.z += value.z * weight;
		}
		public override Vector3 Get() {
			return v;
		}
	}
	public class Vector2Accumulator : Pixmap<Vector2>.Accumulator<Vector2> {
		Vector2 v;
		public override void Reset() {
			v = default(Vector2);
		}
		public override void Add(Vector2 value, float weight) {
			v.x += value.x * weight;
			v.y += value.y * weight;
		}
		public override Vector2 Get() {
			return v;
		}
	}

	public class SkipmapCache {
		int datasize = 0;
		public uint[][] skipmaps;
		
		int radiusX = 0;
		int radiusY = 0;
		int windowW = 0;
		int windowH = 0;
		int windowArea = 0;

		public SkipmapCache(Pixmap<Color32> pixmap=null, int rX=-1, int rY=-1) {
			Update(pixmap, rX, rY);
		}

		public int ID(int dx, int dy) {
			if ((Mathf.Abs(dx) > radiusX) || (Mathf.Abs(dy) > radiusY)) {
				dx = 0; dy = 0;
			}
			return (dx+radiusX) + (dy+radiusY) * windowW;
		}
		public uint[] this[int dx, int dy] {
			get {
				if (skipmaps == null) return null;
				return skipmaps[ID(dx, dy)];
			}
		}
		
		public void Update(Pixmap<Color32> pixmap, int rX=-1, int rY=-1) {
			if (pixmap == null) return;
			
			var pixels = pixmap.pixels;
			int n = pixmap.count;
			int w = pixmap.w, h = pixmap.h;
			
			bool realloc = (skipmaps == null);
			realloc |= (n != datasize);
			realloc |= (rX >= 0) && (rX != radiusX);
			realloc |= (rY >= 0) && (rY != radiusY);

			if (realloc) {
				if (rX >= 0) radiusX = rX;
				if (rY >= 0) radiusY = rY;
				windowW = 1 + radiusX * 2;
				windowH = 1 + radiusY * 2;
				windowArea = windowW * windowH;
				datasize = n;

				// reuse if size is bigger
				if ((skipmaps == null) || (skipmaps.Length < windowArea)) {
					skipmaps = new uint[windowArea][];
				}
				for (int id = 0; id < windowArea; id++) {
					var skipmap = skipmaps[id];
					if ((skipmap == null) || (skipmap.Length < datasize)) {
						skipmaps[id] = new uint[datasize];
					}
				}
			}
			
			Color32 outside = Color.clear;
			
			int[] last_i = new int[windowArea];
			for (int id = 0; id < windowArea; id++) {
				last_i[id] = -1;
			}
			
			for (int i = n-1; i >= 0; i--) {
				var c = pixels[i];
				int x = i % w;
				int y = (i - x) / w;
				
				for (int dy = -radiusY; dy <= radiusY; dy++) {
					int _y = y + dy;
					int i_y = _y * w;
					int idy = radiusX + (dy+radiusY) * windowW;
					bool y_inside = (_y >= 0) && (_y < h);
					
					for (int dx = -radiusX; dx <= radiusX; dx++) {
						int _x = x + dx;
						bool x_inside = (_x >= 0) && (_x < w);
						int id = dx + idy;
						
						// The previous pixel that will be painted over by the current pixel
						Color32 _c = (x_inside && y_inside) ? pixels[_x + i_y] : outside;
						
						bool skip = (c.a == 0);
						if ((dx != 0) || (dy != 0)) {
							skip |= (_c.a == 255) && (_c.r == c.r) && (_c.g == c.g) && (_c.b == c.b);
						}

						if (skip) {
							int xy = last_i[id];
							if (xy == -1) {
								skipmaps[id][i] = 0xFFFFFFFF;
							} else {
								int last_x = (xy & 0xFFFF);
								int last_y = (xy >> 16);
								ushort _dx = (ushort)(last_x - x);
								ushort _dy = (ushort)(last_y - y);
								uint dxy = (uint)(_dx | (_dy << 16));
								skipmaps[id][i] = dxy;
							}
						} else {
							int xy = x | (y << 16);
							last_i[id] = xy;
							skipmaps[id][i] = 0;
						}
					}
				}
			}
		}
	}

	public class PixmapPainter {
		SkipmapCache skipmap_cache = new SkipmapCache();
		public uint[] Skipmap {get; private set;}
		void SetSkipmap(int dx, int dy) {
			Skipmap = ((skipmap_cache == null) ? null : skipmap_cache[dx, dy]);
		}
		
		public Pixmap<Color32> Brush {get; private set;}

		public bool IsStroke { get {return is_stroke;} }

		bool is_stroke = false;
		int prev_x, prev_y;
		
		public static Pixmap<Color32> CircleBrush(float size, float feather=1f) {
			return CircleBrush(size, feather, Color.white);
		}
		public static Pixmap<Color32> CircleBrush(float size, float feather, Color32 color) {
			int sz = Mathf.CeilToInt(size);
			var pixmap = new Pixmap<Color32>(sz, sz);
			pixmap.ApplyCircle((sz-1)*0.5f, (sz-1)*0.5f, (sz-feather)*0.5f, feather,
			                   (x, y, c, f) => {color.a = (byte)(f*255); return color;});
			return pixmap;
		}
		
		public void SetBrush(Pixmap<Color32> brush) {
			SkipmapCache skipmap_cache = null;
			if (brush != null) skipmap_cache = new SkipmapCache(brush, 1, 1);
			SetBrush(brush, skipmap_cache);
		}
		public void SetBrush(Pixmap<Color32> brush, SkipmapCache skipmap_cache) {
			Brush = brush;
			this.skipmap_cache = skipmap_cache;
			EndStroke();
		}

		public void Move(int x, int y, bool end_stroke=false) {
			if (end_stroke) EndStroke();
			if (is_stroke) {
				if ((x == prev_x) && (y == prev_y)) return;
				SetSkipmap(x-prev_x, y-prev_y);
			} else {
				is_stroke = true;
			}
			prev_x = x;
			prev_y = y;
		}
		public void EndStroke() {
			is_stroke = false;
			SetSkipmap(0, 0);
		}
	}
}