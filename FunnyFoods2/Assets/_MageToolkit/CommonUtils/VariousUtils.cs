#if UNITY_EDITOR
using UnityEditor;
#endif
using UnityEngine;
using System;
using System.Collections;
using System.Collections.Generic;

namespace Bini.Utils {
	// primary use is to avoid unnecessary allocations
	public class Boxed<T> {
		public T Value;
		public Boxed() {}
		public Boxed(T v) {
			Value = v;
		}
		public static implicit operator Boxed<T>(T v) {
			return new Boxed<T>(v);
		}
		public static explicit operator T(Boxed<T> bv) {
			return bv.Value;
		}
	}

	public class GraphMesh {
		public class Vertex {
			public int id;
			public List<Edge> edges = new List<Edge>();
			
			public Vertex(int id) {
				this.id = id;
			}
			
			public override int GetHashCode() {
				return id;
			}
			
			public bool Equals(Vertex other) {
				return id.Equals(other.id);
			}
			
			public Edge GetEdge(Vertex other) {
				for (int i = 0; i < edges.Count; i++) {
					var edge = edges[i];
					if (edge.Other(this).Equals(other)) return edge;
				}
				return new Edge(this, other);
			}
			
			// useful for contours
			public Edge OtherEdge(Edge e) {
				for (int i = 0; i < edges.Count; i++) {
					var edge = edges[i];
					if (!edge.Equals(e)) return edge;
				}
				return null;
			}
		}
		
		public class Edge {
			public Vertex v0;
			public Vertex v1;
			public List<Triangle> triangles = new List<Triangle>();
			
			public Edge(Vertex v0, Vertex v1) {
				this.v0 = v0;
				this.v1 = v1;
				v0.edges.Add(this);
				v1.edges.Add(this);
			}
			
			public override int GetHashCode() {
				return v0.GetHashCode() ^ v1.GetHashCode();
			}
			
			public bool Equals(Edge other) {
				return (v0.Equals(other.v0) && v1.Equals(other.v1)) ||
					(v0.Equals(other.v1) && v1.Equals(other.v0));
			}
			
			public Vertex Other(Vertex v) {
				if (v.Equals(v0)) return v1;
				if (v.Equals(v1)) return v0;
				return null;
			}
		}
		
		public class Triangle {
			public Vertex v0;
			public Vertex v1;
			public Vertex v2;
			public Edge e0;
			public Edge e1;
			public Edge e2;
			
			public Triangle(Vertex v0, Vertex v1, Vertex v2) {
				this.v0 = v0;
				this.v1 = v1;
				this.v2 = v2;
				e0 = v0.GetEdge(v1);
				e0.triangles.Add(this);
				e1 = v1.GetEdge(v2);
				e1.triangles.Add(this);
				e2 = v2.GetEdge(v0);
				e2.triangles.Add(this);
			}
			
			public override int GetHashCode() {
				return v0.GetHashCode() ^ v1.GetHashCode() ^ v2.GetHashCode();
			}
			
			public bool Equals(Triangle other) {
				return (v0.Equals(other.v0) && v1.Equals(other.v1) && v2.Equals(other.v2));
			}
		}
		
		public Dictionary<int, Vertex> verts_map = new Dictionary<int, Vertex>();
		public Dictionary<int, Vertex>.ValueCollection vertices {
			get { return verts_map.Values; }
		}
		public HashSet<Edge> edges = new HashSet<Edge>();
		public List<Triangle> triangles = new List<Triangle>();
		
		public GraphMesh(int[] indices, int mode=3) {
			if (mode == 3) {
				for (int i = 0; i < indices.Length; i += 3) {
					var tri = new Triangle(TryAddVertex(indices[i]),
						TryAddVertex(indices[i+1]), TryAddVertex(indices[i+2]));
					if (!edges.Contains(tri.e0)) edges.Add(tri.e0);
					if (!edges.Contains(tri.e1)) edges.Add(tri.e1);
					if (!edges.Contains(tri.e2)) edges.Add(tri.e2);
					triangles.Add(tri);
				}
			} else if (mode == 2) {
				for (int i = 0; i < indices.Length; i += 2) {
					var edge = TryAddVertex(indices[i]).GetEdge(TryAddVertex(indices[i+1]));
					if (!edges.Contains(edge)) edges.Add(edge);
				}
			} else {
				for (int i = 0; i < indices.Length; i += 1) {
					TryAddVertex(indices[i]);
				}
			}
		}
		Vertex TryAddVertex(int i) {
			Vertex v;
			if (verts_map.TryGetValue(i, out v)) return v;
			v = new Vertex(i);
			verts_map.Add(i, v);
			return v;
		}
		
		public static GraphMesh CreateEdgesOfRank(int[] indices, int n_min, int n_max=-1) {
			var graph_mesh = new GraphMesh(indices);
			var border_indices = new List<int>();
			foreach (var edge in graph_mesh.EdgesOfRank(n_min, n_max)) {
				border_indices.Add(edge.v0.id);
				border_indices.Add(edge.v1.id);
			}
			graph_mesh = new GraphMesh(border_indices.ToArray(), 2);
			return graph_mesh;
		}
		
		public IEnumerable<Edge> EdgesOfRank(int n_min, int n_max=-1) {
			if (n_max < 0) n_max = n_min;
			foreach (var edge in edges) {
				int n = edge.triangles.Count;
				if ((n >= n_min) && (n <= n_max)) yield return edge;
			}
		}
	}

	public static class MeshUtils {
		// Note: Unity now has SkinnedMeshRenderer.BakeMesh(mesh)
		// Adapted from ImogenPoot's code
		// http://forum.unity3d.com/threads/14378-Raycast-without-colliders
		public static Vector3[] SkinnedVertices(SkinnedMeshRenderer skin, bool toWorld=false) {
			var mesh = skin.sharedMesh;
			var vertices = mesh.vertices;
			var bindposes = mesh.bindposes;
			var boneWeights = mesh.boneWeights;
			var bones = skin.bones;
			var tfm = skin.transform;
			var newVert = new Vector3[vertices.Length];
			var matrices = new Matrix4x4[bones.Length];
			for (int i = 0; i < bones.Length; i++) {
				matrices[i] = bones[i].LocalToParentMatrix() * bindposes[i];
			}
			for (int i = 0; i < vertices.Length; i++) {
				BoneWeight bw = boneWeights[i];
				Vector3 src_pos = vertices[i];
				Vector3 dst_pos = Vector3.zero;
				dst_pos += matrices[bw.boneIndex0].MultiplyPoint3x4(src_pos) * bw.weight0;
				dst_pos += matrices[bw.boneIndex1].MultiplyPoint3x4(src_pos) * bw.weight1;
				dst_pos += matrices[bw.boneIndex2].MultiplyPoint3x4(src_pos) * bw.weight2;
				dst_pos += matrices[bw.boneIndex3].MultiplyPoint3x4(src_pos) * bw.weight3;
				if (toWorld) dst_pos = tfm.TransformPoint(dst_pos);
//				Vector3 localPt;
//				if (Mathf.Abs(bw.weight0) > 0.00001f) {
//					localPt = bindposes[bw.boneIndex0].MultiplyPoint3x4(src_pos);
//					dst_pos += bones[bw.boneIndex0].TransformPoint(localPt) * bw.weight0;
//				}
//				if (Mathf.Abs(bw.weight1) > 0.00001f) {
//					localPt = bindposes[bw.boneIndex1].MultiplyPoint3x4(src_pos);
//					dst_pos += bones[bw.boneIndex1].TransformPoint(localPt) * bw.weight1;
//				}
//				if (Mathf.Abs(bw.weight2) > 0.00001f) {
//					localPt = bindposes[bw.boneIndex2].MultiplyPoint3x4(src_pos);
//					dst_pos += bones[bw.boneIndex2].TransformPoint(localPt) * bw.weight2;
//				}
//				if (Mathf.Abs(bw.weight3) > 0.00001f) {
//					localPt = bindposes[bw.boneIndex3].MultiplyPoint3x4(src_pos);
//					dst_pos += bones[bw.boneIndex3].TransformPoint(localPt) * bw.weight3;
//				}
				newVert[i] = dst_pos;
			}
			return newVert;
		}

		public static Bounds CalculateBounds(Mesh mesh) {
			var vertices = mesh.vertices;
			if (vertices.Length == 0) return new Bounds(Vector3.zero, Vector3.zero);
			Bounds bounds = new Bounds(vertices[0], Vector3.zero);
			for (int i = 1; i < vertices.Length; i++) {
				bounds.Encapsulate(vertices[i]);
			}
			return bounds;
		}

		public static Rect CalculateUVBounds(Mesh mesh, int uv_set=0) {
			Vector2[] uv;
			if (uv_set == 0) uv = mesh.uv;
			else if (uv_set == 1) uv = mesh.uv2;
			else if (uv_set == 2) uv = mesh.uv2;
			else return new Rect(0, 0, 0, 0);
			if (uv.Length == 0) return new Rect(0, 0, 0, 0);
			Rect bounds = new Rect(uv[0].x, uv[0].y, 0, 0);
			for (int i = 1; i < uv.Length; i++) {
				var p = uv[i];
				bounds.xMin = Mathf.Min(bounds.xMin, p.x);
				bounds.xMax = Mathf.Max(bounds.xMax, p.x);
				bounds.yMin = Mathf.Min(bounds.yMin, p.y);
				bounds.yMax = Mathf.Max(bounds.yMax, p.y);
			}
			return bounds;
		}

		public static Rect FullUVRect(Mesh mesh) {
			if (mesh == null) return new Rect();

			var bounds = CalculateBounds(mesh);
			var uv_bounds = CalculateUVBounds(mesh);
			
			var uv0 = uv_bounds.min();
			var uv1 = uv_bounds.max();
			var uvD = uv1 - uv0;

			if (uvD.min() <= float.Epsilon) return new Rect();

			var p0 = bounds.min.To2D();
			var p1 = bounds.max.To2D();
			var pD = p1 - p0;

			var pD_uvD = pD.Div(uvD);
			p0 += Vector2.Scale(Vector2.zero - uv0, pD_uvD);
			p1 += Vector2.Scale(Vector2.one - uv1, pD_uvD);
			return new Rect(p0.x, p0.y, p1.x-p0.x, p1.y-p0.y);
		}

		public static Rect SpriteRect(Bounds bounds, bool horizontal=false) {
			if (horizontal) {
				return new Rect(bounds.min.x, bounds.min.z, bounds.size.x, bounds.size.z);
			} else {
				return new Rect(bounds.min.x, bounds.min.y, bounds.size.x, bounds.size.y);
			}
		}

		public static Rect SpriteSubregion(Bounds bounds,
				Rect tex_region, Vector2 tex_size, bool horizontal=false) {
			return SpriteSubregion(SpriteRect(bounds, horizontal), tex_region, tex_size);
		}
		public static Rect SpriteSubregion(Rect bounds, Rect tex_region, Vector2 tex_size) {
			return new Rect(
				bounds.x + bounds.width * (tex_region.x/tex_size.x),
				bounds.y + bounds.height * (tex_region.y/tex_size.y),
				bounds.width * (tex_region.width/tex_size.x),
				bounds.height * (tex_region.height/tex_size.y));
		}

		public static GameObject Sprite(Bounds bounds, bool horizontal=false) {
			return Sprite(SpriteRect(bounds, horizontal), horizontal);
		}
		public static GameObject Sprite(Rect rect, bool horizontal=false) {
			var obj = new GameObject("sprite", typeof(MeshRenderer));
			var mesh_filter = obj.AddComponent<MeshFilter>();
			mesh_filter.sharedMesh = SpriteMesh(rect, horizontal);
			return obj;
		}
		
		public static Mesh SpriteMesh(Bounds bounds, bool horizontal=false) {
			return SpriteMesh(SpriteRect(bounds, horizontal), horizontal);
		}
		public static Mesh SpriteMesh(Rect rect, bool horizontal=false) {
			Mesh mesh = new Mesh();
			Matrix4x4? matrix = null;
			if (horizontal) {
				var m = Matrix4x4.identity;
				m[1, 1] = 0;
				m[2, 1] = 1;
				matrix = m;
			}
			SetSpriteMesh(mesh, matrix, rect, 0, new Rect(0, 0, 1, 1));
			return mesh;
		}

		static Vector3[] sprite_vertices = new Vector3[4];
		static Vector3[] sprite_normals = new Vector3[4];
		static Vector2[] sprite_uv = new Vector2[4];
		static int[] sprite_triangles = new int[]{0, 1, 2, 0, 2, 3};
		public static void SetSpriteMesh(Mesh mesh, Matrix4x4? matrix, Rect pos, int ipos=0, Rect? uv=null, int iuv=0, Rect? uv1=null, int iuv1=0, Rect? uv2=null, int iuv2=0) {
			int i;

			i = ((ipos % 4) + 4);
			if (matrix.HasValue) {
				var _matrix = matrix.Value;
				sprite_vertices[(0+i) % 4] = _matrix.MultiplyVector(new Vector3(pos.xMin, pos.yMin));
				sprite_vertices[(1+i) % 4] = _matrix.MultiplyVector(new Vector3(pos.xMin, pos.yMax));
				sprite_vertices[(2+i) % 4] = _matrix.MultiplyVector(new Vector3(pos.xMax, pos.yMax));
				sprite_vertices[(3+i) % 4] = _matrix.MultiplyVector(new Vector3(pos.xMax, pos.yMin));
			} else {
				sprite_vertices[(0+i) % 4] = new Vector3(pos.xMin, pos.yMin);
				sprite_vertices[(1+i) % 4] = new Vector3(pos.xMin, pos.yMax);
				sprite_vertices[(2+i) % 4] = new Vector3(pos.xMax, pos.yMax);
				sprite_vertices[(3+i) % 4] = new Vector3(pos.xMax, pos.yMin);
			}
			mesh.vertices = sprite_vertices;

			Vector3 normal = Vector3.back;
			if (matrix.HasValue) normal = matrix.Value.MultiplyVector(normal);
			sprite_normals[0] = sprite_normals[1] = sprite_normals[2] = sprite_normals[3] = normal;
			mesh.normals = sprite_normals;

			if (uv.HasValue) {
				i = ((iuv % 4) + 4);
				var _uv = uv.Value;
				sprite_uv[(0+i) % 4] = new Vector2(_uv.xMin, _uv.yMin);
				sprite_uv[(1+i) % 4] = new Vector2(_uv.xMin, _uv.yMax);
				sprite_uv[(2+i) % 4] = new Vector2(_uv.xMax, _uv.yMax);
				sprite_uv[(3+i) % 4] = new Vector2(_uv.xMax, _uv.yMin);
				mesh.uv = sprite_uv;
			}

			if (uv1.HasValue) {
				i = ((iuv1 % 4) + 4);
				var _uv = uv1.Value;
				sprite_uv[(0+i) % 4] = new Vector2(_uv.xMin, _uv.yMin);
				sprite_uv[(1+i) % 4] = new Vector2(_uv.xMin, _uv.yMax);
				sprite_uv[(2+i) % 4] = new Vector2(_uv.xMax, _uv.yMax);
				sprite_uv[(3+i) % 4] = new Vector2(_uv.xMax, _uv.yMin);
				mesh.uv2 = sprite_uv;
			}

			if (uv2.HasValue) {
				i = ((iuv2 % 4) + 4);
				var _uv = uv2.Value;
				sprite_uv[(0+i) % 4] = new Vector2(_uv.xMin, _uv.yMin);
				sprite_uv[(1+i) % 4] = new Vector2(_uv.xMin, _uv.yMax);
				sprite_uv[(2+i) % 4] = new Vector2(_uv.xMax, _uv.yMax);
				sprite_uv[(3+i) % 4] = new Vector2(_uv.xMax, _uv.yMin);
				mesh.uv2 = sprite_uv;
			}

			mesh.triangles = sprite_triangles;

			mesh.RecalculateBounds();
		}

		public static Mesh CopyMesh(Mesh src) {
			Mesh mesh = new Mesh();
			CopyMesh(src, mesh);
			return mesh;
		}
		public static void CopyMesh(Mesh src, Mesh mesh) {
			mesh.Clear();
			mesh.vertices = src.vertices;
			mesh.bounds = src.bounds;
			mesh.normals = src.normals;
			mesh.tangents = src.tangents;
			mesh.boneWeights = src.boneWeights;
			mesh.bindposes = src.bindposes;
			mesh.colors32 = src.colors32;
			mesh.uv = src.uv;
			mesh.uv2 = src.uv2;
			mesh.uv2 = src.uv2;
			mesh.triangles = src.triangles;
			for (int i = 0; i < src.subMeshCount; i++) {
				mesh.SetIndices(src.GetIndices(i), src.GetTopology(i), i);
			}
		}
	}

	public static class ObjectUtils {
		public static Matrix4x4 LocalToParentMatrix(this Transform tfm) {
			return Matrix4x4.TRS(tfm.localPosition, tfm.localRotation, tfm.localScale);
		}
		public static Matrix4x4 ParentToLocalMatrix(this Transform tfm) {
			return tfm.LocalToParentMatrix().inverse;
		}

		public static Matrix4x4 LocalToObjectMatrix(this Transform tfm, Transform other) {
			if (other == null) return tfm.localToWorldMatrix;
			return other.worldToLocalMatrix * tfm.localToWorldMatrix;
		}
		public static Matrix4x4 ObjectToLocalMatrix(this Transform tfm, Transform other) {
			if (other == null) return tfm.localToWorldMatrix;
			return tfm.worldToLocalMatrix * other.localToWorldMatrix;
		}
		
		public static void EnableAllComponents(this GameObject obj, bool state,
			bool recursive=false, bool nonbehaviors=true, bool behaviors=true) {
			if (nonbehaviors) {
				var renderer = obj.GetComponent<Renderer>();
				if (renderer != null) renderer.enabled = state;
				var collider = obj.GetComponent<Collider>();
				if (collider != null) collider.enabled = state;
				var particleEmitter = obj.GetComponent<ParticleEmitter>();
				if (particleEmitter != null) particleEmitter.enabled = state;
				var particleSystem = obj.GetComponent<ParticleSystem>(); // ? not exactly 'enabled'
				if (particleSystem != null) particleSystem.enableEmission = state;
				//var cloth = obj.GetComponent<Cloth>(); // not used in Mage projects
				//if (cloth != null) cloth.enabled = state;
				//var lodGroup = obj.GetComponent<LODGroup>(); // not used in Mage projects
				//if (lodGroup != null) lodGroup.enabled = state;
			}
			if (behaviors) {
				var behaviours = obj.GetComponents<Behaviour>();
				for (int i = 0; i < behaviours.Length; i++) {
					var behaviour = behaviours[i];
					if (behaviour == null) continue;
					behaviour.enabled = state;
				}
			}

			if (!recursive) return;

			var obj_tfm = obj.transform;
			var n_children = obj_tfm.childCount;
			for (int i = 0; i < n_children; i++) {
				obj_tfm.GetChild(i).gameObject.EnableAllComponents(state, recursive, nonbehaviors, behaviors);
			}
		}

		public static void ResetToParent(this Transform tfm, Transform parent) {
			if (tfm.parent != parent) tfm.parent = parent;
			tfm.localPosition = Vector3.zero;
			tfm.localRotation = Quaternion.identity;
			tfm.localScale = Vector3.one;
		}

		public static Transform WalkHierarchy<T>(this Transform tfm,
			System.Func<T, bool> action, bool depth_first=true, bool all_components=false) where T : Component {
			return tfm.WalkHierarchy((_tfm) => {
				if (all_components) {
					var cs = _tfm.GetComponents<T>();
					if (cs == null) return false;
					for (int i = 0; i < cs.Length; i++) {
						var c = cs[i];
						if ((c != null) && action(c)) return true;
					}
				} else {
					var c = _tfm.GetComponent<T>();
					if ((c != null) && action(c)) return true;
				}
				return false;
			}, depth_first);
		}
		public static Transform WalkHierarchy(this Transform tfm,
			System.Func<Transform, bool> action, bool depth_first=true) {
			if (depth_first) {
				var stack = new Stack<Transform>();
				stack.Push(tfm);
				while (stack.Count != 0) {
					tfm = stack.Pop();
					if (action(tfm)) return tfm;
					// Reverse order to match the recursive/FIFO order
					for (int i = tfm.childCount-1; i != -1; i--) {
						stack.Push(tfm.GetChild(i));
					}
				}
			} else {
				var queue = new Queue<Transform>();
				queue.Enqueue(tfm);
				while (queue.Count != 0) {
					tfm = queue.Dequeue();
					if (action(tfm)) return tfm;
					for (int i = 0; i < tfm.childCount; i++) {
						queue.Enqueue(tfm.GetChild(i));
					}
				}
			}
			return null;
		}

		public static void WalkHierarchy<T>(this Transform tfm,
			System.Action<T> action, bool depth_first=true, bool all_components=false) where T : Component {
			tfm.WalkHierarchy((_tfm) => {
				if (all_components) {
					var cs = _tfm.GetComponents<T>();
					if (cs == null) return;
					for (int i = 0; i < cs.Length; i++) {
						var c = cs[i];
						if (c != null) action(c);
					}
				} else {
					var c = _tfm.GetComponent<T>();
					if (c != null) action(c);
				}
			}, depth_first);
		}
		public static void WalkHierarchy(this Transform tfm,
			System.Action<Transform> action, bool depth_first=true) {
			if (depth_first) {
				var stack = new Stack<Transform>();
				stack.Push(tfm);
				while (stack.Count != 0) {
					tfm = stack.Pop();
					action(tfm);
					// Reverse order to match the recursive/FIFO order
					for (int i = tfm.childCount-1; i != -1; i--) {
						stack.Push(tfm.GetChild(i));
					}
				}
			} else {
				var queue = new Queue<Transform>();
				queue.Enqueue(tfm);
				while (queue.Count != 0) {
					tfm = queue.Dequeue();
					action(tfm);
					for (int i = 0; i < tfm.childCount; i++) {
						queue.Enqueue(tfm.GetChild(i));
					}
				}
			}
		}

		public static void RecursiveSetLayer(this Transform tfm, string layer_name) {
			tfm.RecursiveSetLayer(LayerMask.NameToLayer(layer_name));
		}
		public static void RecursiveSetLayer(this Transform tfm, int layer) {
			tfm.gameObject.layer = layer;
			int n_children = tfm.childCount;
			for (int i = 0; i < n_children; i++) {
				tfm.GetChild(i).RecursiveSetLayer(layer);
			}
		}

		public static void RecursiveSetTag(this Transform tfm, string tag) {
			tfm.tag = tag;
			int n_children = tfm.childCount;
			for (int i = 0; i < n_children; i++) {
				tfm.GetChild(i).RecursiveSetTag(tag);
			}
		}

		public static string HierarchyPath(this Transform tfm, Transform root=null) {
			if (tfm == root) return "";

			string tfmPath = tfm.name;
			while (true) {
				tfm = tfm.parent;
				if (tfm == root) return tfmPath;
				// Return null if tfm is not a child of root at all
				if (tfm == null) return null;
				tfmPath = tfm.name + "/" + tfmPath;
			}
		}

		public static Transform FindPartial(this Transform tfm, string part, bool depth_first=false) {
			bool is_path = part.Contains("/");
			return tfm.WalkHierarchy((_tfm) => {
				if (_tfm == tfm) return false; // ignore parent
				string full_path = (is_path ? _tfm.HierarchyPath(tfm.parent)+"/" : _tfm.name);
				return full_path.Contains(part);
			}, depth_first);
		}

		public static bool HasParent(this Transform tfm, Transform parent, bool checkSelf=false) {
			if (checkSelf && tfm == parent) return true;
			while (true) {
				tfm = tfm.parent;
				if (tfm == parent) return true;
				if (tfm == null) return false;
			}
		}

		public static bool IsSelectedHierarchy(this Transform tfm, bool checkSelf=true) {
#if UNITY_EDITOR
			var activeObj = Selection.activeGameObject;
			if (activeObj == null) return false;
			return activeObj.transform.HasParent(tfm, checkSelf);
#else
			return false;
#endif
		}

		public static T EnsureComponent<T>(this Component c) where T : Component {
			return EnsureComponent<T>(c.gameObject);
		}
		public static T EnsureComponent<T>(this GameObject gameObj) where T : Component {
			T component = gameObj.GetComponent<T>();
			if (component == null) component = gameObj.AddComponent<T>();
			return component;
		}

		public static void CloneMeshAndRenderer(GameObject src, GameObject dst) {
			var src_skinned_renderer = src.GetComponent<SkinnedMeshRenderer>();
			if (src_skinned_renderer != null) {
				var dst_skinned_renderer = dst.AddComponent<SkinnedMeshRenderer>();
				dst_skinned_renderer.rootBone = src_skinned_renderer.rootBone;
				dst_skinned_renderer.bones = src_skinned_renderer.bones;
				dst_skinned_renderer.quality = src_skinned_renderer.quality;
				dst_skinned_renderer.updateWhenOffscreen = src_skinned_renderer.updateWhenOffscreen;
				dst_skinned_renderer.sharedMesh = src_skinned_renderer.sharedMesh;
			} else {
				var src_mesh_filter = src.GetComponent<MeshFilter>();
				var dst_mesh_filter = dst.AddComponent<MeshFilter>();
				dst_mesh_filter.sharedMesh = src_mesh_filter.sharedMesh;
				dst.AddComponent<MeshRenderer>(); // no settings to initialize
			}
			var src_renderer = src.GetComponent<Renderer>();
			var dst_renderer = dst.GetComponent<Renderer>();
#if UNITY_5
			dst_renderer.shadowCastingMode = src_renderer.shadowCastingMode;
#else
			dst_renderer.castShadows = src_renderer.castShadows;
#endif
			dst_renderer.receiveShadows = src_renderer.receiveShadows;
			dst_renderer.useLightProbes = src_renderer.useLightProbes;
			dst_renderer.sharedMaterial = src_renderer.sharedMaterial;
		}

		public static Material CopyMaterial(Material src, Material dst=null) {
			if (dst == null) dst = new Material(src);
			dst.shader = src.shader;
			dst.CopyPropertiesFromMaterial(src); // seems like it copies ONLY shader properties and nothing else
			if (src.renderQueue > 0) dst.renderQueue = src.renderQueue;
			foreach (var kw in dst.shaderKeywords) {
				dst.DisableKeyword(kw);
			}
			foreach (var kw in src.shaderKeywords) {
				dst.EnableKeyword(kw);
			}
			return dst;
		}

		public static Mesh GetMesh(GameObject gameObj) {
			var skinned_renderer = gameObj.GetComponent<SkinnedMeshRenderer>();
			if (skinned_renderer != null) return skinned_renderer.sharedMesh;
			var mesh_filter = gameObj.GetComponent<MeshFilter>();
			if (mesh_filter != null) return mesh_filter.sharedMesh;
			return null;
		}
		
		public static void SetMesh(GameObject gameObj, Mesh mesh) {
			var skinned_renderer = gameObj.GetComponent<SkinnedMeshRenderer>();
			if (skinned_renderer != null) {
				skinned_renderer.sharedMesh = mesh;
				return;
			}
			var mesh_filter = gameObj.GetComponent<MeshFilter>();
			if (mesh_filter == null) return;
			mesh_filter.sharedMesh = mesh;
		}

		public static void TryDestroy(UnityEngine.Object obj) {
			try {
				if (obj != null) UnityEngine.Object.Destroy(obj);
			} catch  {
			}
		}
		public static Transform Dispose(Transform tfm) {
			if (tfm == null) return null;
			TryDestroy(tfm.gameObject);
			return null;
		}
		public static GameObject Dispose(GameObject gameObj) {
			TryDestroy(gameObj);
			return null;
		}
		public static Mesh Dispose(Mesh mesh) {
			TryDestroy(mesh);
			return null;
		}
		public static RenderTexture Dispose(RenderTexture tex) {
			if (tex != null) tex.Release();
			TryDestroy(tex);
			return null;
		}
		public static Texture2D Dispose(Texture2D tex) {
			TryDestroy(tex);
			return null;
		}
		public static Texture Dispose(Texture tex) {
			TryDestroy(tex);
			return null;
		}
		public static Material Dispose(Material mat) {
			TryDestroy(mat);
			return null;
		}

		public static IEnumerable<KeyValuePair<string, ShaderPropertyType>> EnumerateProperties(this Shader shader) {
#if UNITY_EDITOR
			int nprops = ShaderUtil.GetPropertyCount(shader);
			for (int i = 0; i < nprops; i++) {
				var propname = ShaderUtil.GetPropertyName(shader, i);
				var proptype = ShaderUtil.GetPropertyType(shader, i);
				ShaderPropertyType res_proptype = ShaderPropertyType.Color;
				switch (proptype) {
				case ShaderUtil.ShaderPropertyType.Color:
					res_proptype = ShaderPropertyType.Color;
					break;
				case ShaderUtil.ShaderPropertyType.Vector:
					res_proptype = ShaderPropertyType.Vector;
					break;
				case ShaderUtil.ShaderPropertyType.Float:
					res_proptype = ShaderPropertyType.Float;
					break;
				case ShaderUtil.ShaderPropertyType.Range:
					res_proptype = ShaderPropertyType.Range;
					break;
				case ShaderUtil.ShaderPropertyType.TexEnv:
					res_proptype = ShaderPropertyType.TexEnv;
					break;
				}
				yield return new KeyValuePair<string, ShaderPropertyType>(propname, res_proptype);
			}
#else
			throw new NotImplementedException("Unity does not support getting shader properties at runtime");
#endif
		}

		public static bool IsComponentEnabled(Component component, bool checkGameObj=false) {
			if (component == null) return false;
			if (checkGameObj && !component.gameObject.activeInHierarchy) return false;
			var behaviour = component as Behaviour;
			if (behaviour != null) return behaviour.enabled;
			var renderer = component as Renderer;
			if (renderer != null) return renderer.enabled;
			var collider = component as Collider;
			if (collider != null) return collider.enabled;
			var particleEmitter = component as ParticleEmitter;
			if (particleEmitter != null) return particleEmitter.enabled;
			var cloth = component as Cloth;
			if (cloth != null) return cloth.enabled;
			var lodGroup = component as LODGroup;
			if (lodGroup != null) return lodGroup.enabled;
			return true;
		}
		public static void SetComponentEnabled(Component component, bool enabled) {
			if (component == null) return;
			var behaviour = component as Behaviour;
			if (behaviour != null) {behaviour.enabled = enabled; return;}
			var renderer = component as Renderer;
			if (renderer != null) {renderer.enabled = enabled; return;}
			var collider = component as Collider;
			if (collider != null) {collider.enabled = enabled; return;}
			var particleEmitter = component as ParticleEmitter;
			if (particleEmitter != null) {particleEmitter.enabled = enabled; return;}
			var cloth = component as Cloth;
			if (cloth != null) {cloth.enabled = enabled; return;}
			var lodGroup = component as LODGroup;
			if (lodGroup != null) {lodGroup.enabled = enabled; return;}
		}

		public static bool DetectSpriteCollision(Transform transform, float alpha, Camera cam=null) {
			return DetectSpriteCollision(transform, alpha, Input.mousePosition, cam);
		}
		public static bool DetectSpriteCollision(Transform transform, float alpha, Vector3 screen_pos, Camera cam=null) {
			if (cam == null) cam = Camera.main;
			Ray ray = cam.ScreenPointToRay(screen_pos);
			float ray_distance = cam.farClipPlane - cam.nearClipPlane;
			return DetectSpriteCollision(transform, alpha, ray, ray_distance, new Bounds(), true);
		}
		public static bool DetectSpriteCollision(Transform transform, float alpha, Ray ray, float distance, Bounds bounds, bool get_bounds=false) {
			var gameObject = transform.gameObject;
			if (!gameObject.activeInHierarchy) return false;

			var ray_world = ray;
			var ray_local = ray;

			var m = transform.worldToLocalMatrix;
			ray_local.origin = m.MultiplyPoint(ray_local.origin);
			ray_local.direction = m.MultiplyVector(ray_local.direction);
			
			Vector2 uv;

			var collider = transform.GetComponent<Collider>();
			var renderer = transform.GetComponent<Renderer>();
			if (get_bounds) {
				var mesh = ObjectUtils.GetMesh(transform.gameObject);
				if (mesh != null) {
					bounds = mesh.bounds;
					ray = ray_local;
				} else {
					if (renderer != null) {
						bounds = renderer.bounds; // in world space
						ray = ray_world;
					} else if (collider != null) {
						bounds = collider.bounds; // in world space
						ray = ray_world;
					}
				}
			} else {
				ray = ray_local;
			}
			// May be problematic for bounds with size.z=0 ?
			//if (!bounds.IntersectRay(ray, out distance)) return false;
			//var p = ray.origin + ray.direction * distance;
			var p = ray.origin; // GUI is always orthographic, anyway
			uv.x = (p.x - bounds.min.x) / bounds.size.x;
			uv.y = (p.y - bounds.min.y) / bounds.size.y;
			if ((uv.x < 0f) || (uv.x > 1f) || (uv.y < 0f) || (uv.y > 1f)) return false;

			var mesh_collider = collider as MeshCollider;
			if (mesh_collider != null) {
				RaycastHit hitInfo;
				if (!mesh_collider.Raycast(ray_local, out hitInfo, distance)) return false;
				uv = hitInfo.textureCoord;
			}
			
			if (alpha >= 0) {
				if (renderer == null) return false;
				if (!renderer.enabled) return false;
				var mat = renderer.sharedMaterial;
				var tex = ((mat != null) && mat.HasProperty("_MainTex")) ? mat.mainTexture as Texture2D : null;
				if (tex != null) {
					try {
						Color color;
						if (tex.filterMode == FilterMode.Point) {
							int x = (int)(uv.x*tex.width);
							int y = (int)(uv.y*tex.height);
							color = tex.GetPixel(x, y);
						} else {
							color = tex.GetPixelBilinear(uv.x, uv.y);
						}
						if (alpha > 0) {
							if (color.a < alpha) return false;
						} else {
							if (color.a <= alpha) return false;
						}
					} catch (UnityException) {
						// Texture is not readable
						if (Debug.isDebugBuild) Debug.LogError(string.Format(
							"Collision detection warning: {0} has unreadable texture {1}",
							transform.name, tex.name));
					}
				}
			}
			
			return true;
		}
	}

	public enum ShaderPropertyType {
		Color,
		Vector,
		Float,
		Range,
		TexEnv
	}

	public static class RuntimeAssets {
		public const string GC_NAME = "\a DELETE ME \a";
		
		static HashSet<UnityEngine.Object> to_destroy_set = new HashSet<UnityEngine.Object>();

		public static void GCCollect() {
			List<UnityEngine.Object> to_destroy = null;
			
			//*
			var objs = Resources.FindObjectsOfTypeAll(typeof(UnityEngine.Object));
			for (int i = 0; i < objs.Length; i++) {
				var obj = objs[i];
				if (obj == null) continue;
				// Runtime-created objects have ID < 0
				//if (obj.GetInstanceID() >= 0) continue; // this is an Editor asset
				if (obj.name == RuntimeAssets.GC_NAME) {
					if (to_destroy == null) to_destroy = new List<UnityEngine.Object>();
					to_destroy.Add(obj);
				}
			}
			//*/
			
			/*
			foreach (var obj in to_destroy_set) {
				if (obj == null) continue;
				if (to_destroy == null) to_destroy = new List<UnityEngine.Object>();
				to_destroy.Add(obj);
			}
			*/
			
			if (to_destroy == null) {
				//Debug.Log("GC-collecting: 0 : 0");
			} else {
				// Materials should be destroyed before textures?
				Order<Material>(to_destroy);

				int n_destroyed = 0;
				for (int i = 0; i < to_destroy.Count; i++) {
					var obj = to_destroy[i];
					try {
						if (obj != null) {
							UnityEngine.Object.Destroy(obj);
							n_destroyed++;
						}
					} catch {
					}
				}
				
				//Debug.Log("GC-collecting: " + to_destroy.Count+" : "+n_destroyed);
			}
			
			to_destroy_set.Clear();
		}
		static void Order<T>(List<UnityEngine.Object> to_destroy) where T : UnityEngine.Object {
			int i_last = 0;
			for (int i = 0; i < to_destroy.Count; i++) {
				var obj = to_destroy[i] as T;
				if (obj != null) {
					if (i > i_last) {
						to_destroy[i] = to_destroy[i_last];
						to_destroy[i_last] = obj;
					}
					i_last++;
				}
			}
		}

		public static T MarkGC<T>(this T obj, bool mark=true) where T : UnityEngine.Object {
			if (obj == null) return null;
			if (obj.GetInstanceID() >= 0) return obj;
			obj.name = GC_NAME;
			/*
			if (mark) {
				to_destroy_set.Add(obj);
			} else {
				to_destroy_set.Remove(obj);
			}
			*/
			return obj;
		}
	}

	public static class CollectionsUtils {
		public static T First<T>(this IEnumerable<T> items) {
			using (IEnumerator<T> iter = items.GetEnumerator()) {
				iter.MoveNext();
				return iter.Current;
			}
		}

		public static T Get<K,T>(this IDictionary<K,T> d, K k, T dflt=default(T)) {
			T v;
			return d.TryGetValue(k, out v) ? v : dflt;
		}

		public static int Resize<T>(ref T[] collection, int n, T dflt=default(T)) {
			int diff = n - (collection != null ? collection.Length : 0);
			System.Array.Resize(ref collection, n); // creates array if null
			if ((diff > 0) && !dflt.Equals(default(T))) {
				for (int i = 1; i <= diff; i++) collection[n-i] = dflt;
			}
			return diff;
		}
		public static int Resize<T>(ref List<T> collection, int n, T dflt=default(T)) {
			if (collection == null) collection = new List<T>();
			int diff = n - collection.Count;
			if (diff < 0) {
				collection.RemoveRange(n, -diff);
			} else if (diff > 0) {
				for (int i = 0; i < diff; i++) collection.Add(dflt);
			}
			return diff;
		}

		public static T SafeGet<T>(T[] collection, int i, T dflt=default(T)) {
			if (collection == null) return dflt;
			if ((i < 0) || (i >= collection.Length)) return dflt;
			return collection[i];
		}
		public static T SafeGet<T>(List<T> collection, int i, T dflt=default(T)) {
			if (collection == null) return dflt;
			if ((i < 0) || (i >= collection.Count)) return dflt;
			return collection[i];
		}

		public static int IndexOfLocalMinimum<T>(IList<T> points, System.Func<T,float> metric, int i0=0) {
			int imin = 0, imax = points.Count-1;
			var pL = points[Mathf.Max(i0-1, imin)];
			var pC = points[i0];
			var pR = points[Mathf.Min(i0+1, imax)];
			var dL = metric(pL);
			var dC = metric(pC);
			var dR = metric(pR);
			int di = 0, iend = -1;
			float prev_d = dC;
			int prev_i = i0;
			if ((dL < dC) && (dL < dR)) {
				for (int i = i0-1; i >= imin; i--) {
					var p = points[i];
					var d = metric(p);
					if (d > prev_d) return prev_i; // minimum was on previous step
					prev_d = d;
					prev_i = i;
				}
			} else if (dR < dC) {
				for (int i = i0+1; i <= imax; i++) {
					var p = points[i];
					var d = metric(p);
					if (d > prev_d) return prev_i; // minimum was on previous step
					prev_d = d;
					prev_i = i;
				}
			} else {
				return i0;
			}
			return prev_i;
		}
	}

	public static class SceneObjects {
		// GameObject.FindObjectsOfType() returns only active objects in the scene.
		// Resources.FindObjectsOfTypeAll returns not just objects from the scene.

		static HideFlags NonSceneHideFlags = HideFlags.NotEditable | HideFlags.HideAndDontSave;

		public static IEnumerable<T> Components<T>(bool include_inactive=true) where T : Component {
			if (include_inactive) {
				var objects = Resources.FindObjectsOfTypeAll(typeof(T));
				for (int i = 0; i < objects.Length; i++) {
					T component = objects[i] as T;
					if (component == null) continue;
					//if (component.hideFlags == HideFlags.NotEditable) continue;
					//if (component.hideFlags == HideFlags.HideAndDontSave) continue;
					if ((component.hideFlags & NonSceneHideFlags) != HideFlags.None) continue;

	#if UNITY_EDITOR
					Transform tfm = component.transform;
					//string assetPath = AssetDatabase.GetAssetPath(tfm.root.gameObject);
					//if (!string.IsNullOrEmpty(assetPath)) continue;
					if (AssetDatabase.Contains(tfm.root.gameObject)) continue;
	#endif

					yield return component;
				}
			} else {
				var objects = GameObject.FindObjectsOfType(typeof(T));
				for (int i = 0; i < objects.Length; i++) {
					T component = objects[i] as T;
					if (component == null) continue;
					yield return component;
				}
			}
		}

		public static IEnumerable<GameObject> Objects(bool include_inactive=true) {
			if (include_inactive) {
				var objects = Resources.FindObjectsOfTypeAll(typeof(GameObject));
				for (int i = 0; i < objects.Length; i++) {
					GameObject obj = objects[i] as GameObject;
					//if (obj.hideFlags == HideFlags.NotEditable) continue;
					//if (obj.hideFlags == HideFlags.HideAndDontSave) continue;
					if ((obj.hideFlags & NonSceneHideFlags) != HideFlags.None) continue;

	#if UNITY_EDITOR
					Transform tfm = obj.transform;
					//string assetPath = AssetDatabase.GetAssetPath(tfm.root.gameObject);
					//if (!string.IsNullOrEmpty(assetPath)) continue;
					if (AssetDatabase.Contains(tfm.root.gameObject)) continue;
	#endif

					yield return obj;
				}
			} else {
				var objects = GameObject.FindObjectsOfType(typeof(GameObject));
				for (int i = 0; i < objects.Length; i++) {
					GameObject obj = objects[i] as GameObject;
					yield return obj;
				}
			}
		}

		public static IEnumerable<Transform> Transforms(bool include_inactive=true) {
			return Components<Transform>(include_inactive);
		}

		public static HashSet<Transform> Roots(bool include_inactive=true) {
			var roots = new HashSet<Transform>();
			foreach (Transform tfm in Components<Transform>(include_inactive)) {
				roots.Add(tfm.root);
			}
			return roots;
		}

		public static IEnumerable<GameObject> ByName(string name, bool include_inactive=true) {
			foreach (GameObject obj in Objects(include_inactive)) {
				if (obj.name == name) yield return obj;
			}
		}

		public static IEnumerable<GameObject> ByTag(string tag, bool include_inactive=true) {
			foreach (GameObject obj in Objects(include_inactive)) {
				if (obj.tag == tag) yield return obj;
			}
		}

		public static T FirstComponent<T>(bool include_inactive=true, bool checkOnlyGameObj=true) where T : Component {
			if (checkOnlyGameObj) {
				if (!include_inactive) return UnityEngine.Object.FindObjectOfType<T>();
				T first = null;
				foreach (var component in Components<T>(include_inactive)) {
					if (component.gameObject.activeInHierarchy) return component;
					if (first == null) first = component;
				}
				return first;
			} else {
				T first = null;
				foreach (var component in Components<T>(include_inactive)) {
					if (ObjectUtils.IsComponentEnabled(component, true)) return component;
					if (first == null) first = component;
				}
				return first;
			}
		}
	}

	public static class FileUtils {
		public static string StripFilename(string path, bool normalize_separator=false) {
			string dir = System.IO.Path.GetDirectoryName(path);
			if (normalize_separator) dir = dir.Replace("\\", "/");
			if (!string.IsNullOrEmpty(dir)) {
				if (!(dir.EndsWith("/") || dir.EndsWith("\\"))) dir += "/";
			}
			return dir;
		}
		public static string StripExtension(string path) {
			string dir, file;
			SplitPath(path, out dir, out file, false);
			return dir + file;
		}
		public static void SplitPath(string path, out string dir, out string file, bool add_ext=false) {
			string ext;
			SplitPath(path, out dir, out file, out ext);
			if (add_ext) file += ext;
		}
		public static void SplitPath(string path, out string dir, out string file, out string ext) {
			if (string.IsNullOrEmpty(path)) {
				ext = "";
				file = "";
				dir = "";
			} else {
				ext = System.IO.Path.GetExtension(path);
				file = System.IO.Path.GetFileNameWithoutExtension(path);
				dir = System.IO.Path.GetDirectoryName(path).Replace("\\", "/");
				if (!string.IsNullOrEmpty(dir) && !dir.EndsWith("/")) dir += "/";
			}
		}

		public static string GetAssetPath(UnityEngine.Object assetObject) {
			if (assetObject == null) return null;
			#if UNITY_EDITOR
			return AssetDatabase.GetAssetPath(assetObject);
			#else
			return null;
			#endif
		}

		public static string GetAssetPath(int instanceID) {
			#if UNITY_EDITOR
			return AssetDatabase.GetAssetPath(instanceID);
			#else
			return null;
			#endif
		}
		
		public static void SplitAssetPath(UnityEngine.Object assetObject, out string dir, out string file) {
			string ext;
			SplitAssetPath(assetObject, out dir, out file, out ext);
		}
		public static void SplitAssetPath(UnityEngine.Object assetObject,
			out string dir, out string file, out string ext) {
			SplitPath(GetAssetPath(assetObject), out dir, out file, out ext);
		}
		public static void SplitAssetPath(int instanceID, out string dir, out string file) {
			string ext;
			SplitAssetPath(instanceID, out dir, out file, out ext);
		}
		public static void SplitAssetPath(int instanceID,
			out string dir, out string file, out string ext) {
			SplitPath(GetAssetPath(instanceID), out dir, out file, out ext);
		}

		public static string CurrentScenePath {
			get {
				#if UNITY_EDITOR
				return EditorApplication.currentScene;
				#else
				return null;
				#endif
			}
		}
		
		static string _ProjectPath = null;
		public static string ProjectPath {
			get {
				#if UNITY_EDITOR
				if (string.IsNullOrEmpty(_ProjectPath)) {
					// Attention: Application.dataPath returns "<ProjectPath>/Assets"!
					_ProjectPath = Application.dataPath.TrimEnd('/', '\\');
					_ProjectPath = System.IO.Path.GetDirectoryName(_ProjectPath)+"/";
				}
				#endif
				return _ProjectPath;
			}
		}

		public static bool AssetExists(UnityEngine.Object assetObject) {
			return AssetExists(GetAssetPath(assetObject));
		}
		public static bool AssetExists(int instanceID) {
			return AssetExists(GetAssetPath(instanceID));
		}
		public static bool AssetExists(string path) {
			if (string.IsNullOrEmpty(path)) return false;
			#if UNITY_EDITOR
			return System.IO.File.Exists(ProjectPath+path);
			#else
			return false;
			#endif
		}

		public static void EnsureDir(string path, bool ignore_after_slash=true) {
			if (path.EndsWith("/") || path.EndsWith("\\")) {
				path = path.TrimEnd('/', '\\');
			} else if (ignore_after_slash) {
				path = System.IO.Path.GetDirectoryName(path);
			}
			if (!System.IO.Directory.Exists(path)) System.IO.Directory.CreateDirectory(path);
	//		if (System.IO.Directory.Exists(path)) return;
	//		
	//		var dirs = path.Split("\\/".ToCharArray(), StringSplitOptions.RemoveEmptyEntries);
	//		path = "";
	//		foreach (string dir in dirs) {
	//			path += dir+"/";
	//			if (!System.IO.Directory.Exists(path)) System.IO.Directory.CreateDirectory(path);
	//		}
		}

		#if UNITY_EDITOR
		public static void EnsureAssetDir(string path, bool ignore_after_slash=true) {
			if (path.EndsWith("/") || path.EndsWith("\\")) {
				path = path.TrimEnd('/', '\\');
			} else if (ignore_after_slash) {
				path = System.IO.Path.GetDirectoryName(path);
			}
			if (!System.IO.Directory.Exists(ProjectPath+path)) System.IO.Directory.CreateDirectory(ProjectPath+path);
	//		if (System.IO.Directory.Exists(ProjectPath+path)) return;
	//
	//		var dirs = path.Split("\\/".ToCharArray(), StringSplitOptions.RemoveEmptyEntries);
	//		path = ProjectPath.TrimEnd('/', '\\');
	//		string localpath = "";
	//		foreach (string dir in dirs) {
	//			path += "/"+dir;
	//			if (!System.IO.Directory.Exists(path)) AssetDatabase.CreateFolder(localpath, dir);
	//			localpath += ((localpath.Length == 0) ? "" : "/") + dir;
	//		}
		}

		public static void EnsureReadable(Texture texture) {
			string path = AssetDatabase.GetAssetPath(texture);
			if (string.IsNullOrEmpty(path)) return;

			var textureImporter = AssetImporter.GetAtPath(path) as TextureImporter;
			if (!textureImporter.isReadable) {
				textureImporter.isReadable = true;

				textureImporter.textureType = TextureImporterType.Advanced;

				AssetDatabase.WriteImportSettingsIfDirty(path);
				AssetDatabase.ImportAsset(path);
			}
		}

		public static T SaveAsset<T>(T asset, string path=null, bool preserve_links=true) where T : UnityEngine.Object {
			if (asset == null) return null;
			if (string.IsNullOrEmpty(path)) {
				if (!AssetDatabase.Contains(asset)) return null;
				path = AssetDatabase.GetAssetPath(asset);
				AssetDatabase.SaveAssets();
			} else {
				//if (AssetDatabase.Contains(asset)) return null;
				var old_asset = AssetDatabase.LoadMainAssetAtPath(path) as T;
				if (preserve_links && (old_asset != null)) {
					//EditorUtility.CopySerialized(asset, old_asset);
					//AssetDatabase.SaveAssets();
					string tmp_path = "Assets/"+System.IO.Path.GetRandomFileName()+System.IO.Path.GetExtension(path);
					AssetDatabase.CreateAsset(asset, tmp_path);
					System.IO.File.Copy(tmp_path, path, true);
					AssetDatabase.DeleteAsset(tmp_path);
				} else {
					FileUtils.EnsureAssetDir(path);
					AssetDatabase.CreateAsset(asset, path);
					AssetDatabase.SaveAssets();
				}
			}
			//AssetDatabase.SaveAssets();
			AssetDatabase.Refresh();
			AssetDatabase.ImportAsset(path);
			return AssetDatabase.LoadAssetAtPath(path, typeof(T)) as T;
		}
		#endif
	}

	public static class MathUtils {
		public static float DistToSegment(Vector3 v0, Vector3 v1, Vector3 p) {
			var dv = v1 - v0;
			var dv_size = dv.magnitude;
			if (dv_size <= Mathf.Epsilon) return Vector3.Distance(v0, p);
			var n = dv * (1f/dv_size);
			var dp = p - v0;
			var proj = Mathf.Clamp(Vector3.Dot(dp, n), 0f, dv_size);
			return (dp - (n*proj)).magnitude;
		}
		
		static Vector4 RGBA_ENCODE_FLOAT = new Vector4(1f, 255f, 65025f, 16581375f);
		static Vector4 RGBA_DECODE_FLOAT = new Vector4(1f, 1f/255f, 1f/65025f, 1/16581375f);
		public static Vector4 EncodeFloatRGBA(float v) {
			const float _255 = 1f/255f;
			Vector4 enc = RGBA_ENCODE_FLOAT * Mathf.Clamp01(v);
			enc.y = enc.y % 1f; enc.z = enc.z % 1f; enc.w = enc.w % 1f;
			enc.x -= enc.y * _255; enc.y -= enc.z * _255; enc.z -= enc.w * _255;
			return enc;
		}
		public static float DecodeFloatRGBA(Vector4 rgba) {
			return Vector4.Dot(rgba, RGBA_DECODE_FLOAT);
		}
	}

	public enum SplineType {
		Linear, Cubic, Catmull, Hermite
	}
	public class SplineInterpolator {
		public SplineType spline_type;

		float m_pos, m_neg;
		void CalcM() {
			float tension_wrk = (1f - _tension) * 0.5f;
			m_pos = (1f + _bias) * tension_wrk;
			m_neg = (1f - _bias) * tension_wrk;
		}

		float _tension = 0f;
		public float tension {
			get {return _tension;}
			set {_tension = value; CalcM();}
		}

		float _bias = 0f;
		public float bias {
			get {return _bias;}
			set {_bias = value; CalcM();}
		}

		public SplineInterpolator(SplineType spline_type) {
			this.spline_type = spline_type;
			CalcM();
		}

		public Vector4 this[float t, float tension, float bias] {
			get {
				this.tension = tension;
				this.bias = bias;
				return this[t];
			}
		}
		public Vector4 this[float t, float tension] {
			get {
				this.tension = tension;
				return this[t];
			}
		}
		public Vector4 this[float t] {
			get {
				float t2, t3;
				switch (spline_type) {
				case SplineType.Cubic:
					t2 = t*t; t3 = t*t2;
					float t23 = t2 - t3;
					return new Vector4(
						(-t + t2 + t23),
						(1f - t2 - t23),
						(t + t23),
						(-t23));
				case SplineType.Catmull:
					t2 = t*t; t3 = t*t2;
					float t05 = 0.5f*t;
					float t205 = 0.5f*t2;
					float t220 = t2 + t2;
					float t305 = 0.5f*t3;
					float t315 = t3 + t305;
					return new Vector4(
						(-t305 + t2 - t05),
						(t315 - (t220 + t205) + 1f),
						(-t315 + t220 + t05),
						(t305 - t205));
				case SplineType.Hermite:
					t2 = t*t; t3 = t*t2;
					float t3_t2 = t3 - t2;
					float t32_t23 = t3_t2 + t3_t2 - t2;
					float a0 = t32_t23 + 1f;
					float a1 = t3_t2 - t2 + t;
					float a2 = t3_t2;
					float a3 = -t32_t23;
					float a1p = a1*m_pos;
					float a1n = a1*m_neg;
					float a2p = a2*m_pos;
					float a2n = a2*m_neg;
					return new Vector4(
						(-a1p),
						(a0 + a1p - a1n - a2p),
						(a1n + a2p - a2n + a3),
						(a2n));
				default: //case SplineType.Linear:
					return new Vector4(0f, 1f - t, t, 0f);
				}
			}
		}
	}

	/*
	#linear
	def intl(objs, i, t, tr):
	    p1 = calct(objs[i],t)
	    p2 = calct(objs[i+1], t)

	    r = p1 + (p2 - p1)*tr

	    return r

	def intc(objs, i, t, tr, tipo=3, tension=0.0, bias=0.0):

	    ncurves =len(objs)

	    #if 2 curves go to linear interpolation regardless the one you choose
	    if ncurves<3:
	        return intl(objs, i, t, tr)
	    else:

	        #calculates the points to be interpolated on each curve
	        if i==0:
	            p0 = calct(objs[i], t)
	            p1 = p0
	            p2 = calct(objs[i+1], t)
	            p3 = calct(objs[i+2], t)
	        else:
	            if ncurves-2 == i:
	                p0 = calct(objs[i-1], t)
	                p1 = calct(objs[i], t)
	                p2 = calct(objs[i+1], t)
	                p3 = p2
	            else:
	                p0 = calct(objs[i-1], t)
	                p1 = calct(objs[i], t)
	                p2 = calct(objs[i+1], t)
	                p3 = calct(objs[i+2], t)


	    #calculates the interpolation between those points
	    #i used methods from this page: http://paulbourke.net/miscellaneous/interpolation/

	    if tipo==0:
	        #linear
	        return intl(objs, i, t, tr)
	    elif tipo == 1:
	        #natural cubic
	        t2 = tr*tr
	        a0 = p3-p2-p0+p1
	        a1 = p0-p1-a0
	        a2 = p2-p0
	        a3 = p1
	        return a0*tr*t2 + a1*t2+a2*tr+a3
	    elif tipo == 2:
	        #catmull it seems to be working. ill leave it for now.
	        t2 = tr*tr
	        a0 = -0.5*p0 +1.5*p1 -1.5*p2 +0.5*p3
	        a1 = p0 - 2.5*p1 + 2*p2 -0.5*p3
	        a2 = -0.5*p0 + 0.5 *p2
	        a3 = p1
	        return a0*tr*tr + a1*t2+a2*tr+a3

	    elif tipo == 3:
	        #hermite
	        tr2 = tr*tr
	        tr3 = tr2*tr
	        m0 = (p1-p0)*(1+bias)*(1-tension)/2
	        m0+= (p2-p1)*(1-bias)*(1-tension)/2
	        m1 = (p2-p1)*(1+bias)*(1-tension)/2
	        m1+= (p3-p2)*(1-bias)*(1-tension)/2
	        a0 = 2*tr3 - 3*tr2 + 1
	        a1 = tr3 - 2 * tr2+ tr
	        a2 = tr3 - tr2
	        a3 = -2*tr3 + 3*tr2

	        return a0*p1+a1*m0+a2*m1+a3*p2

	*/


	public static class VectorExtensions {
		public static Vector3 To3D(this Vector2 v) {
			return new Vector3(v.x, v.y);
		}
		public static Vector3 To3D(this Vector2 v, float z) {
			return new Vector3(v.x, v.y, z);
		}
		public static Vector4 To4D(this Vector2 v) {
			return new Vector4(v.x, v.y);
		}
		public static Vector4 To4D(this Vector2 v, float z) {
			return new Vector4(v.x, v.y, z);
		}
		public static Vector4 To4D(this Vector2 v, float z, float w) {
			return new Vector4(v.x, v.y, z, w);
		}

		public static Vector2 To2D(this Vector3 v) {
			return new Vector2(v.x, v.y);
		}
		public static Vector4 To4D(this Vector3 v) {
			return new Vector4(v.x, v.y, v.z);
		}
		public static Vector4 To4D(this Vector3 v, float w) {
			return new Vector4(v.x, v.y, v.z, w);
		}

		public static Vector2 To2D(this Vector4 v) {
			return new Vector2(v.x, v.y);
		}
		public static Vector3 To3D(this Vector4 v) {
			return new Vector3(v.x, v.y, v.z);
		}

		public static Vector2 Mul(this Vector2 v, float u) {
			return new Vector2(v.x * u, v.y * u);
		}
		public static Vector3 Mul(this Vector3 v, float u) {
			return new Vector3(v.x * u, v.y * u, v.z * u);
		}
		public static Vector4 Mul(this Vector4 v, float u) {
			return new Vector4(v.x * u, v.y * u, v.z * u, v.w * u);
		}
		
		public static Vector2 Div(this Vector2 v, float u) {
			return new Vector2(v.x / u, v.y / u);
		}
		public static Vector3 Div(this Vector3 v, float u) {
			return new Vector3(v.x / u, v.y / u, v.z / u);
		}
		public static Vector4 Div(this Vector4 v, float u) {
			return new Vector4(v.x / u, v.y / u, v.z / u, v.w / u);
		}
		
		public static Vector2 Mul(this Vector2 v, Vector2 u) {
			return new Vector2(v.x * u.x, v.y * u.y);
		}
		public static Vector3 Mul(this Vector3 v, Vector3 u) {
			return new Vector3(v.x * u.x, v.y * u.y, v.z * u.z);
		}
		public static Vector4 Mul(this Vector4 v, Vector4 u) {
			return new Vector4(v.x * u.x, v.y * u.y, v.z * u.z, v.w * u.w);
		}

		public static Vector2 Div(this Vector2 v, Vector2 u) {
			return new Vector2(v.x / u.x, v.y / u.y);
		}
		public static Vector3 Div(this Vector3 v, Vector3 u) {
			return new Vector3(v.x / u.x, v.y / u.y, v.z / u.z);
		}
		public static Vector4 Div(this Vector4 v, Vector4 u) {
			return new Vector4(v.x / u.x, v.y / u.y, v.z / u.z, v.w / u.w);
		}
		
		public static Vector2 abs(this Vector2 v) {
			return new Vector2(Mathf.Abs(v.x), Mathf.Abs(v.y));
		}
		public static Vector3 abs(this Vector3 v) {
			return new Vector3(Mathf.Abs(v.x), Mathf.Abs(v.y), Mathf.Abs(v.z));
		}
		public static Vector4 abs(this Vector4 v) {
			return new Vector4(Mathf.Abs(v.x), Mathf.Abs(v.y), Mathf.Abs(v.z), Mathf.Abs(v.w));
		}

		public static float min(this Vector2 v) {
			return Mathf.Min(v.x, v.y);
		}
		public static float min(this Vector3 v) {
			return Mathf.Min(Mathf.Min(v.x, v.y), v.z);
		}
		public static float min(this Vector4 v) {
			return Mathf.Min(Mathf.Min(v.x, v.y), Mathf.Min(v.z, v.w));
		}
		
		public static float max(this Vector2 v) {
			return Mathf.Max(v.x, v.y);
		}
		public static float max(this Vector3 v) {
			return Mathf.Max(Mathf.Max(v.x, v.y), v.z);
		}
		public static float max(this Vector4 v) {
			return Mathf.Max(Mathf.Max(v.x, v.y), Mathf.Max(v.z, v.w));
		}

		public static Vector2 size(this Rect r) {
			return new Vector2(r.width, r.height);
		}
		public static Vector2 extents(this Rect r) {
			return new Vector2(r.width*0.5f, r.height*0.5f);
		}
		public static Vector2 min(this Rect r) {
			return new Vector2(r.xMin, r.yMin);
		}
		public static Vector2 max(this Rect r) {
			return new Vector2(r.xMax, r.yMax);
		}

		// ATTENTION: structs are passed by value, and C# doesn't allow "this ref"
		public static Rect Encapsulate(this Rect r, Vector2 v) {
			return r.Encapsulate(v.x, v.y);
		}
		public static Rect Encapsulate(this Rect r, float x, float y) {
			r.xMin = Mathf.Min(r.xMin, x);
			r.xMax = Mathf.Max(r.xMax, x);
			r.yMin = Mathf.Min(r.yMin, y);
			r.yMax = Mathf.Max(r.yMax, y);
			return r;
		}
		public static Rect Encapsulate(this Rect r, Rect _r) {
			r.xMin = Mathf.Min(r.xMin, _r.xMin);
			r.xMax = Mathf.Max(r.xMax, _r.xMax);
			r.yMin = Mathf.Min(r.yMin, _r.yMin);
			r.yMax = Mathf.Max(r.yMax, _r.yMax);
			return r;
		}

		public static Rect Expand(this Rect r, Vector2 v) {
			r.Expand(v.x, v.y);
			return r;
		}
		public static Rect Expand(this Rect r, float d) {
			r.Expand(d, d);
			return r;
		}
		public static Rect Expand(this Rect r, float dx, float dy) {
			r.xMin -= dx;
			r.yMin -= dy;
			r.xMax += dx;
			r.yMax += dy;
			return r;
		}

		public static Vector2 vertex(this Rect r, int ix, int iy) {
			return new Vector2((ix > 0) ? r.xMax : r.xMin, (iy > 0) ? r.yMax : r.yMin);
		}
		public static Vector3 vertex(this Bounds b, int ix, int iy, int iz) {
			Vector3 min = b.min, max = b.max;
			return new Vector3((ix > 0) ? max.x: min.x, (iy > 0) ? max.y: min.y, (iz > 0) ? max.z: min.z);
		}

		public static Bounds ToBounds(this Rect r, float cz=0f, float sz=0f) {
			return new Bounds(r.center.To3D(cz), r.size().To3D(sz));
		}
		public static Rect ToRect(this Bounds b) {
			return new Rect(b.min.x, b.min.y, b.size.x, b.size.y);
		}

		public static Vector3 TransformDirection(this Transform tfm, Vector3 v, Transform other) {
			v = tfm.localToWorldMatrix.MultiplyVector(v);
			if (other == null) return v;
			return other.worldToLocalMatrix.MultiplyVector(v);
		}
		public static Vector3 TransformPoint(this Transform tfm, Vector3 v, Transform other) {
			v = tfm.localToWorldMatrix.MultiplyPoint3x4(v);
			if (other == null) return v;
			return other.worldToLocalMatrix.MultiplyPoint3x4(v);
		}
		
		public static Vector3 InverseTransformDirection(this Transform tfm, Vector3 v, Transform other) {
			if (other != null) v = other.localToWorldMatrix.MultiplyVector(v);
			return tfm.worldToLocalMatrix.MultiplyVector(v);
		}
		public static Vector3 InverseTransformPoint(this Transform tfm, Vector3 v, Transform other) {
			if (other != null) v = other.localToWorldMatrix.MultiplyPoint3x4(v);
			return tfm.worldToLocalMatrix.MultiplyPoint3x4(v);
		}

		public static Vector3 ExtractPosition(this Matrix4x4 m) {
			return m.GetColumn(3);
		}
		public static Quaternion ExtractRotation(this Matrix4x4 m) {
			return Quaternion.LookRotation(m.GetColumn(2), m.GetColumn(1));
		}
		public static Vector3 ExtractScale(this Matrix4x4 m) {
			return new Vector3(m.GetColumn(0).magnitude, m.GetColumn(1).magnitude, m.GetColumn(2).magnitude);
		}
	}

	public static class BitMask {
		public static ulong Make(params ulong[] bits) {
			ulong res = 0;
			int shift = 0;
			for (int i = bits.Length-1; i >= 0; i--) {
				res |= bits[i] << shift;
				shift++;
			}
			return res;
		}
	}

	public static class PlatformUtils {
		static int screenResolutionX = 0;
		static int screenResolutionY = 0;
		static void GetScreenResolution() {
			//See also Display.main.systemWidth, Display.main.systemHeight?
			// Note: in editor, this seems to always be 640x480
			var resolutions = Screen.resolutions;
			for (int i = 0; i < resolutions.Length; i++) {
				var resolution = resolutions[i];
				if (resolution.width > screenResolutionX) {
					if (resolution.height > screenResolutionY) {
						screenResolutionX = resolution.width;
						screenResolutionY = resolution.height;
					}
				}
			}
			// This actually happens (e.g. on Android):
			if (screenResolutionX == 0) screenResolutionX = Screen.width;
			if (screenResolutionY == 0) screenResolutionY = Screen.height;
		}
		public static int ScreenResolutionX {
			get {
				if (screenResolutionX == 0) GetScreenResolution();
				return screenResolutionX;
			}
		}
		public static int ScreenResolutionY {
			get {
				if (screenResolutionY == 0) GetScreenResolution();
				return screenResolutionY;
			}
		}

		static float pixelsPerCm = 0f;
		public static float PixelsPerCm {
			get {
				if (pixelsPerCm > 0) return pixelsPerCm;

				float dpi = Screen.dpi;

				if (dpi <= 0) {
					int width = ScreenResolutionX;
					int height = ScreenResolutionY;

					// take some guess
					switch (Application.platform) {
					case (RuntimePlatform.OSXEditor):
					case (RuntimePlatform.OSXPlayer):
					case (RuntimePlatform.OSXWebPlayer):
					case (RuntimePlatform.OSXDashboardPlayer):
						// http://en.wikipedia.org/wiki/Dots_per_inch
						dpi = 72f;
						break;
					case (RuntimePlatform.WindowsEditor):
					case (RuntimePlatform.WindowsPlayer):
					case (RuntimePlatform.WindowsWebPlayer):
						// http://en.wikipedia.org/wiki/Dots_per_inch
						dpi = 96f;
						break;
#if UNITY_5
					case (RuntimePlatform.WSAPlayerARM):
					case (RuntimePlatform.WSAPlayerX64):
					case (RuntimePlatform.WSAPlayerX86):
#else
					case (RuntimePlatform.MetroPlayerARM):
					case (RuntimePlatform.MetroPlayerX64):
					case (RuntimePlatform.MetroPlayerX86):
#endif
						// Use the same value as for Windows?
						dpi = 96f;
						break;
					case (RuntimePlatform.LinuxPlayer):
						dpi = 96f; // ? see http://scanline.ca/dpi/
						break;
					case (RuntimePlatform.Android):
						// http://developer.android.com/guide/practices/screens_support.html
						// See also: http://forum.unity3d.com/threads/203017-Finding-*physical*-screen-size
						//dpi = 160f; // default "medium-size" dpi
						//float xdpi = DisplayMetricsAndroid.XDPI;
						//float ydpi = DisplayMetricsAndroid.YDPI;
						//if ((xdpi > 0) && (ydpi > 0)) dpi = Mathf.Sqrt(xdpi*xdpi + ydpi*ydpi);
						//dpi = 160f * DisplayMetricsAndroid.Density;
						dpi = DisplayMetricsAndroid.DensityDPI;
						if (dpi <= 0) {
							int size_min = Mathf.Min(width, height);
							int size_max = Mathf.Max(width, height);
							if ((size_max >= 960) && (size_min >= 720)) {
								dpi = 320f; // xlarge screens, xhdpi
							} else if ((size_max >= 640) && (size_min >= 480)) {
								dpi = 240f; // large screens, hdpi
							} else if ((size_max >= 470) && (size_min >= 320)) {
								dpi = 160f; // normal screens, mdpi
							} else { // expected to be at least 426 x 320
								dpi = 120f; // small screens, ldpi
							}
						}
						break;
					case (RuntimePlatform.BlackBerryPlayer):
						// http://en.wikipedia.org/wiki/List_of_displays_by_pixel_density
						dpi = 356f;
						break;
					case (RuntimePlatform.IPhonePlayer):
						// http://stackoverflow.com/questions/1365112/what-dpi-resolution-is-used-for-an-iphone-app
						if (width == 640) {
							dpi = 326f;
						} else if (width == 1024) {
							dpi = 132f; // 163 for iPad mini
						} else if (width == 2048) {
							dpi = 264f; // 326 for iPad mini (retina)
						} else {
							dpi = 163f;
						}
						break;
					case (RuntimePlatform.WP8Player):
						// http://blogs.windows.com/windows/b/extremewindows/archive/2013/07/15/
						// windows-8-1-dpi-scaling-enhancements.aspx
						dpi = 140f; // ?
						break;
					default:
						// Use the same value as for Windows?
						dpi = 96f;
						break;
					}
				}

				// DPI points fit in 1 inch (2.54 cm)
				pixelsPerCm = dpi / 2.54f; // inch -> cm

				return pixelsPerCm;
			}
		}
	}

	// Adapted from http://answers.unity3d.com/questions/161281/is-there-a-way-to-android-physical-screen-size.html
	public static class DisplayMetricsAndroid {
		// The logical density of the display
		public static float Density { get; private set; }
		
		// The screen density expressed as dots-per-inch
		public static int DensityDPI { get; private set; }
		
		// The absolute height of the display in pixels
		public static int HeightPixels { get; private set; }
		
		// The absolute width of the display in pixels
		public static int WidthPixels { get; private set; }
		
		// A scaling factor for fonts displayed on the display
		public static float ScaledDensity { get; private set; }
		
		// The exact physical pixels per inch of the screen in the X dimension
		public static float XDPI { get; private set; }
		
		// The exact physical pixels per inch of the screen in the Y dimension
		public static float YDPI { get; private set; }
		
		static DisplayMetricsAndroid() {
			// the following code is not compiled by iOS platform
	#if UNITY_ANDROID
			// Early out if we're not on an Android device
			if (Application.platform != RuntimePlatform.Android) {
				return;
			}
			
			// The following is equivalent to this Java code:
			//
			// metricsInstance = new DisplayMetrics();
			// UnityPlayer.currentActivity.getWindowManager().getDefaultDisplay().getMetrics(metricsInstance);
			//
			// ... which is pretty much equivalent to the code on this page:
			// http://developer.android.com/reference/android/util/DisplayMetrics.html
			
			using (
				AndroidJavaClass unityPlayerClass = new AndroidJavaClass("com.unity3d.player.UnityPlayer"),
				metricsClass = new AndroidJavaClass("android.util.DisplayMetrics")
				) {
				using (
					AndroidJavaObject metricsInstance = new AndroidJavaObject("android.util.DisplayMetrics"),
					activityInstance = unityPlayerClass.GetStatic<AndroidJavaObject>("currentActivity"),
					windowManagerInstance = activityInstance.Call<AndroidJavaObject>("getWindowManager"),
					displayInstance = windowManagerInstance.Call<AndroidJavaObject>("getDefaultDisplay")
					) {
					displayInstance.Call("getMetrics", metricsInstance);
					Density = metricsInstance.Get<float>("density");
					DensityDPI = metricsInstance.Get<int>("densityDpi");
					HeightPixels = metricsInstance.Get<int>("heightPixels");
					WidthPixels = metricsInstance.Get<int>("widthPixels");
					ScaledDensity = metricsInstance.Get<float>("scaledDensity");
					XDPI = metricsInstance.Get<float>("xdpi");
					YDPI = metricsInstance.Get<float>("ydpi");
				}
			}
	#endif
		}
	}

	public class CurveSegmentIterator<T> {
		public T p0;
		public T p1;
		public T p2;
		public T p3;
		
		private bool next_segment = false;
		private IEnumerator<T> enumerator;
		
		public CurveSegmentIterator(IEnumerable<T> enumerable) {
			enumerator = enumerable.GetEnumerator();
		}
		
		public CurveSegmentIterator(IEnumerator<T> enumerator) {
			this.enumerator = enumerator;
		}
		
		public bool MoveNext() {
			bool moved = enumerator.MoveNext();
			if (moved) {
				T current = enumerator.Current;
				if (next_segment) {
					p0 = p1;
					p1 = p2;
					p2 = p3;
					p3 = current;
				} else {
					p0 = current;
					p1 = p0;
					moved = enumerator.MoveNext();
					if (moved) {
						p2 = enumerator.Current;
						moved = enumerator.MoveNext();
						if (moved) {
							p3 = enumerator.Current;
							next_segment = true;
						} else {
							p3 = p2;
						}
					} else {
						p2 = p0;
						p3 = p0;
						return false;
					}
				}
			} else {
				if (!next_segment) {
					return false;
				} else {
					p0 = p1;
					p1 = p2;
					p2 = p3;
					next_segment = false;
				}
			}
			return true;
		}
	}

	public class SimpleGraph<T> {
		public class Node {
			public T Data;

			int cluster = -1;
			public int Cluster {
				get { return cluster; }
				set { cluster = (value < 1) ? -1 : value; }
			}
			public bool Valid {
				get { return cluster != 0; }
				set { if (!value) cluster = 0; }
			}

			List<Node> links = null;
			public IEnumerable<Node> Links {
				get {
					if (links != null) {
						foreach (var node in links) {
							if (node.Valid) yield return node;
						}
					}
				}
			}
			public int LinksCount {
				get {
					if (links == null) return 0;
					int n = 0;
					foreach (var node in links) {
						if (node.Valid) n++;
					}
					return n;
				}
			}
			public void Cleanup() {
				if (links == null) return;
				for (int i = links.Count-1; i >= 0; i--) {
					var node = links[i];
					if (!node.Valid) links.RemoveAt(i);
				}
				if (links.Count == 0) {
					links = null;
				} else {
					links.TrimExcess();
				}
			}

			public int ClusterLinksCount {
				get {
					if (links == null) return 0;
					int n = 0;
					foreach (var node in links) {
						if (node.Valid && (node.cluster == cluster)) n++;
					}
					return n;
				}
			}

			public Node Other(Node prev, bool cluster_only=false) {
				if (links == null) return null;
				foreach (var node in links) {
					if ((!node.Valid) || (node == prev)) continue;
					if (!cluster_only || (node.cluster == cluster)) return node;
				}
				return null;
			}
			
			public Node Other(HashSet<Node> prev, bool cluster_only=false) {
				if (links == null) return null;
				foreach (var node in links) {
					if ((!node.Valid) || prev.Contains(node)) continue;
					if (!cluster_only || (node.cluster == cluster)) return node;
				}
				return null;
			}

			public Node(T data) {
				Data = data;
			}

			public Node FindLink(T data) {
				foreach (var node in links) {
					if (node.Valid && node.Data.Equals(data)) return node;
				}
				return null;
			}

			public void AddLink(Node node, bool bidirectional=false) {
				if (node == this) return;
				if (node == null) return;
				if (!node.Valid) return;
				if (links == null) {
					links = new List<Node>();
				} else if (links.Contains(node)) {
					return;
				}
				links.Add(node);
				if (bidirectional) node.AddLink(this);
			}

			public void RemoveLink(Node node, bool bidirectional=false) {
				if (node == this) return;
				if (node == null) return;
				if (links != null) {
					links.Remove(node);
				}
				if (bidirectional) node.RemoveLink(this);
			}

			public bool LinkedTo(Node node, bool self_linked = false) {
				if (node == this) return self_linked;
				if (node == null) return false;
				if (!node.Valid) return false;
				if (links == null) return false;
				foreach (var link in links) {
					if (link == node) return true;
				}
				return false;
			}
			public bool LinkedToAny(IEnumerable<Node> nodes, bool self_linked = false) {
				if (links == null) return false;
				foreach (var node in nodes) {
					if (!node.Valid) continue;
					if (self_linked && (node == this)) return true;
					foreach (var link in links) {
						if (link == node) return true;
					}
				}
				return false; // if nodes is empty, result is false
			}
			public bool LinkedToAll(IEnumerable<Node> nodes, bool self_linked = false) {
				if (links == null) return false;
				bool linked = false;
				foreach (var node in nodes) {
					if (!node.Valid) continue;
					linked = false;
					if (self_linked && (node == this)) {
						linked = true;
					} else {
						foreach (var link in links) {
							if (link == node) {
								linked = true;
								break;
							}
						}
					}
					if (!linked) return false;
				}
				return linked; // if nodes is empty, result is false
			}
		}

		List<Node> nodes = new List<Node>();
		public IEnumerable<Node> Nodes {
			get {
				foreach (var node in nodes) {
					if (node.Valid) yield return node;
				}
			}
		}
		public int NodesCount {
			get {
				int n = 0;
				foreach (var node in nodes) {
					if (node.Valid) n++;
				}
				return n;
			}
		}
		public void Cleanup() {
			for (int i = nodes.Count-1; i >= 0; i--) {
				var node = nodes[i];
				if (!node.Valid) {
					nodes.RemoveAt(i);
					continue;
				}
				node.Cleanup();
			}
			nodes.TrimExcess();
		}

		public Node Add(T data) {
			var node = new Node(data);
			nodes.Add(node);
			return node;
		}

		public Node Find(T data) {
			foreach (var node in nodes) {
				if (node.Valid && node.Data.Equals(data)) return node;
			}
			return null;
		}

		public static Node StartingNode(IEnumerable<Node> nodes, bool cluster_only=false) {
			Node best_node = null;
			int best_links_count = int.MaxValue;
			Node best_cnode = null;
			int best_clinks_count = int.MaxValue;
			foreach (var node in nodes) {
				if (!node.Valid) continue;
				int clinks_count = node.ClusterLinksCount;
				int links_count = (cluster_only ? clinks_count : node.LinksCount);
				if ((best_node == null) || (links_count < best_links_count)) {
					best_node = node;
					best_links_count = links_count;
					if (best_links_count == 0) return best_node;
				}
				if (cluster_only) continue;
				if ((best_cnode == null) || (clinks_count < best_clinks_count)) {
					best_cnode = node;
					best_clinks_count = clinks_count;
				}
			}
			if (!cluster_only) {
				if (best_clinks_count < best_links_count) return best_cnode;
			}
			return best_node;
		}

		public SimpleGraph<List<Node>> ClusterizeData(System.Func<T, T, bool> same_as, bool successive=true) {
			return Clusterize((node0, node1, clusters) => {
				return same_as(node0.Data, node1.Data);
			}, successive);
		}
		public SimpleGraph<List<Node>> ClusterizeData(System.Func<T, T, List<List<Node>>, bool> same_as, bool successive=true) {
			return Clusterize((node0, node1, clusters) => {
				return same_as(node0.Data, node1.Data, clusters);
			}, successive);
		}
		public SimpleGraph<List<Node>> Clusterize(System.Func<Node, Node, List<List<Node>>, bool> same_as, bool successive=true) {
			var clusters = new List<List<Node>>();
			var all_nodes = new HashSet<Node>();
			foreach (var node in Nodes) {
				node.Cluster = -1;
				all_nodes.Add(node);
			}

			var first_node = StartingNode(all_nodes);

			var first_nodes = new HashSet<Node>();

			int cluster_id = 0;
			while (all_nodes.Count != 0) {
				var cluster = new List<Node>();
				var cluster_set = new HashSet<Node>();
				clusters.Add(cluster);
				cluster_id++;

				var parent = first_node;
				// Necessary when same_as() has side-effects
				same_as(parent, parent, clusters);
				parent.Cluster = cluster_id;
				cluster.Add(parent);
				cluster_set.Add(parent);
				all_nodes.Remove(parent);
				first_nodes.Remove(parent);

				first_node = null;

				for (int i = 0; i < cluster.Count; i++) {
					var node = cluster[i];
					foreach (var link in node.Links) {
						if (link.Cluster != -1) continue;
						if (cluster_set.Contains(link)) continue;
						if (!same_as(node, link, clusters)) {
							if (successive) {
								first_node = link;
								first_nodes.Add(link);
							}
							continue;
						}
						link.Cluster = cluster_id;
						cluster.Add(link);
						cluster_set.Add(parent);
						all_nodes.Remove(link);
						first_nodes.Remove(link);
					}
				}

				//if (!successive) first_node = null;

				if (first_node == null) first_node = StartingNode(first_nodes);
				if (first_node == null) first_node = StartingNode(all_nodes);
			}

			var cgraph = new SimpleGraph<List<Node>>();
			for (int i = 0; i < clusters.Count; i++) {
				cgraph.Add(clusters[i]);
			}
			for (int i = 0; i < clusters.Count; i++) {
				var cluster = clusters[i];
				var cnode = cgraph.nodes[i];
				foreach (var node in cluster) {
					foreach (var link in node.Links) {
						var cnode2 = cgraph.nodes[link.Cluster-1];
						cnode.AddLink(cnode2, true); // maybe bidirectional is not necessary?
					}
				}
			}

			return cgraph;
		}
	}

	public class ChangeListener<T> {
		public T saved;
		System.Action on_change = null;
		public bool initialized = false;
		public bool changed = false;
		public ChangeListener(System.Action on_change) {
			this.on_change = on_change;
		}
		public ChangeListener(System.Action on_change, T init) {
			this.on_change = on_change;
			saved = init;
			initialized = true;
		}
		public void Update(T new_value) {
			changed = false;
			if (initialized) {
				if (EqualityComparer<T>.Default.Equals(new_value, saved)) return;
				changed = true;
				on_change();
				saved = new_value;
			} else {
				saved = new_value;
				on_change();
				initialized = true;
			}
		}
	}

	// Adapted from
	// http://www.blackwasp.co.uk/CircularBuffer.aspx
	// http://florianreischl.blogspot.com/2010/01/generic-c-ringbuffer.html
	public class CircularQueue<T> : IEnumerable<T>, IEnumerable {
		T[] _buffer;
		int _head;
		int _tail;
		int _length;
		int _bufferSize;
		object _lock = new object();
		
		public CircularQueue(int bufferSize) {
			_buffer = new T[bufferSize];
			_bufferSize = bufferSize;
			_head = bufferSize - 1;
		}
		
		public bool IsEmpty {
			get { return _length == 0; }
		}
		
		public bool IsFull {
			get { return _length == _bufferSize; }
		}
		
		private int NextPosition(int position) {
			return (position + 1) % _bufferSize;
		}
		
		public void Enqueue(T toAdd) {
			lock (_lock) {
				_head = NextPosition(_head);
				_buffer[_head] = toAdd;
				if (IsFull) {
					_tail = NextPosition(_tail);
				} else {
					_length++;
				}
			}
		}
		
		public T Dequeue() {
			lock (_lock) {
				if (IsEmpty) throw new InvalidOperationException("Queue exhausted");
				
				T dequeued = _buffer[_tail];
				_tail = NextPosition(_tail);
				_length--;
				return dequeued;
			}
		}
		
		public IEnumerator<T> GetEnumerator() {
			while (!IsEmpty) {
				yield return Dequeue();
			}
		}
		
		IEnumerator IEnumerable.GetEnumerator() {
			return this.GetEnumerator();
		}
	}
}
