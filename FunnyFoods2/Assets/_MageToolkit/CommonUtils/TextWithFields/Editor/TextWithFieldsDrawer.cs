using UnityEditor;
using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.Globalization;
using Bini.Utils;

[CustomPropertyDrawer(typeof(TextWithFields))]
partial class TextWithFieldsDrawer : PropertyDrawer {
	#region Storages
	class FieldStorage<T> {
		SerializedProperty storage;
		HashSet<int> used, available;
		System.Func<SerializedProperty,T> getter;
		System.Action<SerializedProperty,T> setter;
		System.Func<T,string> serializer;
		System.Func<string,T> deserializer;

		public FieldStorage(System.Func<SerializedProperty,T> getter,
		                    System.Action<SerializedProperty,T> setter,
		                    System.Func<T,string> serializer,
		                    System.Func<string,T> deserializer,
		                    SerializedProperty property, string name=null) {
			SetProperty(property, name);
			used = new HashSet<int>();
			available = new HashSet<int>();
			this.getter = getter;
			this.setter = setter;
			this.serializer = serializer;
			this.deserializer = deserializer;
		}
		public void SetProperty(SerializedProperty property, string name=null) {
			if (!string.IsNullOrEmpty(name)) property = property.FindPropertyRelative(name);
			storage = property;
		}
		public void Clear() {
			used.Clear();
			available.Clear();
			for (int i = 0; i < storage.arraySize; i++) {
				available.Add(i);
			}
		}
		public void Unstore(int index) {
			used.Remove(index);
			available.Add(index);
			if ((index < 0) || (index >= storage.arraySize)) return;
			setter(storage.GetArrayElementAtIndex(index), default(T));
		}
		public void Store(int index) {
			used.Add(index);
			available.Remove(index);
		}
		public int Add(T item) {
			int index;
			if (available.Count > 0) {
				index = available.First();
				available.Remove(index);
			} else {
				index = storage.arraySize;
				storage.InsertArrayElementAtIndex(index);
			}
			setter(storage.GetArrayElementAtIndex(index), item);
			used.Add(index);
			return index;
		}
		public T this[int index] {
			get {
				if ((index < 0) || (index >= storage.arraySize)) return default(T);
				return getter(storage.GetArrayElementAtIndex(index));
			}
			set {
				if ((index < 0) || (index >= storage.arraySize)) return;
				setter(storage.GetArrayElementAtIndex(index), value);
			}
		}
		public int Count {
			get { return storage.arraySize; }
		}
		public int AddSerialized(string data) {
			return Add(deserializer(data));
		}
		public string GetSerialized(int index) {
			if ((index < 0) || (index >= storage.arraySize)) return null;
			return serializer(getter(storage.GetArrayElementAtIndex(index)));
		}
	}

	FieldStorage<UnityEngine.Object> objects_storage;
	FieldStorage<AnimationCurve> curves_storage;
	FieldStorage<Color> colors_storage;
	FieldStorage<bool> toggles_storage;
	FieldStorage<float> percentages_storage;

	void InitStorages() {
		// "NullReferenceException: Object reference not set to an instance of an object" on prop
		objects_storage = new FieldStorage<UnityEngine.Object>(
			(prop) => {return prop.objectReferenceValue;},
		(prop, value) => {prop.objectReferenceValue = value;},
		SerializeObject, DeserializeObject, property, "objects");
		curves_storage = new FieldStorage<AnimationCurve>(
			(prop) => {return prop.FindPropertyRelative("curve").animationCurveValue;},
		(prop, value) => {prop.FindPropertyRelative("curve").animationCurveValue = value;},
		SerializeCurve, DeserializeCurve, property, "curves");
		colors_storage = new FieldStorage<Color>(
			(prop) => {return prop.colorValue;},
		(prop, value) => {prop.colorValue = value;},
		SerializeColor, DeserializeColor, property, "colors");
		toggles_storage = new FieldStorage<bool>(
			(prop) => {return prop.boolValue;},
		(prop, value) => {prop.boolValue = value;},
		SerializeToggle, DeserializeToggle, property, "toggles");
		percentages_storage = new FieldStorage<float>(
			(prop) => {return prop.floatValue;},
		(prop, value) => {prop.floatValue = value;},
		SerializePercentage, DeserializePercentage, property, "percentages");
	}
	void ReloadStorages() {
		objects_storage.SetProperty(property, "objects");
		curves_storage.SetProperty(property, "curves");
		colors_storage.SetProperty(property, "colors");
		toggles_storage.SetProperty(property, "toggles");
		percentages_storage.SetProperty(property, "percentages");
	}
	#endregion

	#region Stored caret
	SerializedProperty caret_L;
	SerializedProperty caret_E;
	void ReloadSerializedCaret() {
		caret_L = property.FindPropertyRelative("caret_L");
		caret_E = property.FindPropertyRelative("caret_E");
	}
	void SaveCaretPosition() {
		caret_L.intValue = selectionEnd.iL;
		caret_E.intValue = selectionEnd.iE;
	}
	void LoadCaretPosition() {
		selectionEnd.iL = caret_L.intValue;
		selectionEnd.iE = caret_E.intValue;
		update_selection_on_UndoRedo = true;
	}
	#endregion

	#region Lines
	IList<string> lines;
	List<TextWithFields.DisplayLine> dLines;
	void ReloadLines() {
		lines = new SerializedArrayOfString(property.FindPropertyRelative("lines"));
		if (lines.Count == 0) lines.Add("");
	}
	void MatchLinesCount() {
		int nSrc = dLines.Count;
		int nDst = lines.Count;
		for (int nL = nDst; nL < nSrc; nL++) {
			dLines.RemoveAt(dLines.Count-1);
		}
		for (int nL = nSrc; nL < nDst; nL++) {
			dLines.Add(new TextWithFields.DisplayLine());
		}
	}
	#endregion

	#region Init/update text area
	void InitTextArea() {
		FindSerializers();
		
		ReloadLines();
		//if (lines.Count == 0) lines.Add("");

		InitStorages();

		ReloadSerializedCaret();
		
		dLines = new List<TextWithFields.DisplayLine>();
		
		serializer_state = new TextWithFields.SerializerState();
		serializer_state.base_style = text_style;
		serializer_state.lines = lines;
		serializer_state.dLines = dLines;
		
		DeserializeFull();

		// Make sure beginning of the text is displayed
		SetSelectionEnd(new CaretPosition(0, 0), true, true);

		Undo.undoRedoPerformed += OnUndoRedo;
	}

	bool UndoRedoHappened = false;
	void OnUndoRedo() {
		UndoRedoHappened = true;
		try {
			property.FindPropertyRelative("lines");
		} catch (System.Exception) {
			Undo.undoRedoPerformed -= OnUndoRedo;
		}
	}

	float last_txtRect_width = -1;
	bool prev_wrapWords = false;
	bool update_selection_on_UndoRedo = false;
	int toggled_fold_iL = -1;
	void UpdateTextArea() {
		if (dLines == null) InitTextArea();
		ReloadLines();
		ReloadSerializedCaret();
		ReloadStorages();

		if (UndoRedoHappened) {
			DeserializeFull();
			UndoRedoHappened = false;
		}
		
		if (wrapWords && (txtRectR.width != last_txtRect_width)) {
			last_txtRect_width = txtRectR.width;
			AddModifiedLinesAll();
		} else if (wrapWords != prev_wrapWords) {
			prev_wrapWords = wrapWords;
			AddModifiedLinesAll();
		}

		// Choosing cursor depending on current element under mouse is too laggy
		GUI.SetNextControlName(this_control_name);
		EditorGUIUtility.AddCursorRect(txtRectR, MouseCursor.Arrow, txtControlID);
		
		ProcessTextInput(); // Important: after txtControlID is known

		if (recalculate_layout) RecalculateLayout();
		if (update_selection_on_UndoRedo) {
			SetSelectionEnd(selectionEnd, true, true);
			update_selection_on_UndoRedo = false;
		}
		if (ensure_caret_visibility) {
			EnsureCaretVisibility();
			ensure_caret_visibility = false; // important: after
		}

		toggled_fold_iL = -1;

		GUI.Box(boxRect, "", box_style);

		// NOTE: BeginGroup() changes the origin!

		var txtGroupRect = txtRect;
		var txtGroupRectL = txtRectL;
		var txtGroupRectR = txtRectR;

		txtRectL.x -= txtRect.x;
		txtRectL.y -= txtRect.y;
		txtRectR.x -= txtRect.x;
		txtRectR.y -= txtRect.y;
		txtRect.x = 0;
		txtRect.y = 0;

		GUI.BeginGroup(txtGroupRect);
		RenderLines(true, false, false);
		GUI.EndGroup();

		GUI.BeginGroup(txtGroupRectR);
		RenderLines(false, true, false);
		RenderLines(false, false, true);
		GUI.EndGroup();

		if (toggled_fold_iL != -1) {
			ToggleFold(toggled_fold_iL);
			toggled_fold_iL = -1;
		}
	}
	#endregion
	
	#region Text input handling
	// [+] Cut, Copy, Paste: Ctrl+X, Ctrl+C, Ctrl+V | Shift+Delete, Ctrl+Insert, Shift+Insert
	// [+] Navigation: <^v> (1 character), Ctrl+<> (word), home, end, Ctrl+home, Ctrl+end, PageUp, PageDown
	// [+] Shift+Navigation: adjust selection
	// [+] Character input
	// [+] Delete; Backspace (removes combining characters first)
	// [+] Mouse click/drag on non-selected
	// [-] Mouse drag on selected: drag&drop text: decided to not implement (easily achievable with Ctrl+X)
	// [+] Drag&drop into text area
	// [+] Tab, Enter/Return: smart indentation
	// [+] Undo, Redo: Ctrl+Z, Ctrl+Y/Ctrl+Shift+Z -- record continuous typing as one block
	// [+] Actual saving to & loading from serialized property
	// [+] Fields!
	// [+] Folding
	bool wasTab = false;
	void ProcessTextInput() {
		if (!GUI_enabled) return;

		if (EditorGUIUtility.keyboardControl == txtControlID) {
			// There seems to be no way to consume Unity's tab focus event, so we need to restore focus
			if ((@event.type == EventType.KeyDown) && (@event.keyCode == KeyCode.Tab)) wasTab = true;
		} else if (wasTab) {
			wasTab = false;
			Focus();
		}
		
		if (mouse_is_inside) {
			if (@event.type == EventType.mouseDown) {
				Focus();
			} else if (@event.type == EventType.scrollWheel) {
				Scroll(@event.delta * lineHeight_Text);
				consume_event = true;
			}
		}
		
		ProcessTextInput_DragAndDrop(); // this control does not have to be active

		if (EditorGUIUtility.keyboardControl == txtControlID) {
			if (@event.type == EventType.KeyDown) {
				ProcessTextInput_KeyDown();
			} else if (@event.type == EventType.KeyUp) {
				ProcessTextInput_KeyUp();
			} else if (@event.type == EventType.MouseDown) {
				ProcessTextInput_MouseDown();
			} else if (@event.type == EventType.MouseUp) {
				ProcessTextInput_MouseUp();
			} else if (@event.type == EventType.MouseDrag) {
				ProcessTextInput_MouseDrag();
			} else if (@event.type == EventType.MouseMove) {
				ProcessTextInput_MouseMove();
			}
		}
	}
	void ProcessTextInput_KeyDown() {
		consume_event = true;
		bool actionKey = EditorGUI.actionKey || @event.control || @event.command;
		if (@event.keyCode == KeyCode.Home) {
			MoveSelectionEnd(actionKey ? CaretMoves.DocStart : CaretMoves.LineStart, !@event.shift);
		} else if (@event.keyCode == KeyCode.End) {
			MoveSelectionEnd(actionKey ? CaretMoves.DocEnd : CaretMoves.LineEnd, !@event.shift);
		} else if (@event.keyCode == KeyCode.LeftArrow) {
			MoveSelectionEnd(actionKey ? CaretMoves.WordStart : CaretMoves.SymbolPrev, !@event.shift);
		} else if (@event.keyCode == KeyCode.RightArrow) {
			MoveSelectionEnd(actionKey ? CaretMoves.WordEnd : CaretMoves.SymbolNext, !@event.shift);
		} else if (@event.keyCode == KeyCode.UpArrow) {
			if (actionKey) Scroll(0, -lineHeight_Text); else MoveSelectionEnd(CaretMoves.LineUp, !@event.shift);
		} else if (@event.keyCode == KeyCode.DownArrow) {
			if (actionKey) Scroll(0, lineHeight_Text); else MoveSelectionEnd(CaretMoves.LineDown, !@event.shift);
		} else if (@event.keyCode == KeyCode.PageUp) {
			MoveSelectionEnd(CaretMoves.PageUp, !@event.shift);
		} else if (@event.keyCode == KeyCode.PageDown) {
			MoveSelectionEnd(CaretMoves.PageDown, !@event.shift);
		} else if (@event.keyCode == KeyCode.Delete) {
			if (selectionStart == selectionEnd) {
				selectionEnd = AdvanceCaret(selectionEnd, true);
			}
			selectionText = "";
			ensure_caret_visibility = true; //EnsureCaretVisibility();
		} else if (@event.keyCode == KeyCode.Backspace) {
			if (selectionStart == selectionEnd) {
				selectionEnd = AdvanceCaret(selectionEnd, false);
				string s = selectionTextLocal; // uses '\n', not system's newline
				if (!string.IsNullOrEmpty(s)) selectionText = StringMinusCodepoint(s);
			} else {
				selectionText = "";
			}
			ensure_caret_visibility = true; //EnsureCaretVisibility();
		} else if (@event.keyCode == KeyCode.Escape) {
			Unfocus();
			DragAndDrop.PrepareStartDrag(); // Clear generic data
		} else if (@event.character != '\0') {
			ProcessTextInput_CharInput();
		} else if (@event.keyCode == KeyCode.F3) {
			FindAndSelect(search_text, search_ignore_case, !@event.shift);
		} else {
			//consume_event = false;
		}
	}
	void ProcessTextInput_CharInput() {
		string input_string = InputCharOrPair();
		if (string.IsNullOrEmpty(input_string)) return;

		bool reindent_event = false;
		Vector2 save_scroll = scrollPos;
		int iL0 = selectionStart.iL;
		int iE0 = selectionStart.iE;
		int dE0 = dLines[iL0].elements.Count - iE0;
		int iL1 = selectionEnd.iL;
		int iE1 = selectionEnd.iE;
		int dE1 = dLines[iL1].elements.Count - iE1;

		if (input_string == "\n") {
			input_string += dLines[selectionMin.iL].indentation_text;
		} else if (input_string == "\t") {
			bool unindent = @event.shift;
			if ((selectionStart != selectionEnd) || unindent) {
				reindent_event = true;

				CaretPosition selMin = selectionMin, selMax = selectionMax;
				selMin.iE = 0;
				selMax.iE = dLines[selMax.iL].elements.Count;
				selectionStart = selMin;
				selectionEnd = selMax;

				input_string = selectionText;

				var input_lines = input_string.Split(newline_separators, System.StringSplitOptions.None);
				for (int i = 0; i < input_lines.Length; i++) {
					var input_line = input_lines[i];
					if (unindent) {
						if (!string.IsNullOrEmpty(input_line) && char.IsWhiteSpace(input_line, 0)) {
							input_lines[i] = input_line.Substring(1);
						}
					} else {
						input_lines[i] = "\t"+input_line;
					}
				}

				input_string = string.Join("\n", input_lines);
			}
		}

		selectionText = input_string;

		if (reindent_event) {
			if (iE0 != 0) iE0 = Mathf.Max(dLines[iL0].elements.Count-dE0, 0);
			if (iE1 != 0) iE1 = Mathf.Max(dLines[iL1].elements.Count-dE1, 0);
			SetSelectionEnd(new CaretPosition(iL0, iE0), true, false);
			SetSelectionEnd(new CaretPosition(iL1, iE1), false, false);
			ScrollTo(save_scroll);
		} else {
			ensure_caret_visibility = true; //EnsureCaretVisibility();
		}
	}
	void ProcessTextInput_KeyUp() {
		consume_event = true;
		bool actionKey = EditorGUI.actionKey || @event.control || @event.command;
		// When key modifiers are active, Unity doesn't generate KeyDown events
		if (actionKey) { // Ctrl on PC, Command on Mac
			if (@event.keyCode == KeyCode.V) {
				modified_last_time = 0; // ensure a separate Undo group
				selectionText = ClipboardGet();
				ensure_caret_visibility = true; //EnsureCaretVisibility();
			} else if (@event.keyCode == KeyCode.C) {
				ClipboardSet(selectionText, false);
			} else if (@event.keyCode == KeyCode.X) {
				modified_last_time = 0; // ensure a separate Undo group
				ClipboardSet(selectionText, false);
				selectionText = "";
				ensure_caret_visibility = true; //EnsureCaretVisibility();
			} else if (@event.keyCode == KeyCode.A) {
				MoveSelectionEnd(CaretMoves.DocStart, true, false);
				MoveSelectionEnd(CaretMoves.DocEnd, false, false);
			} else if (@event.keyCode == KeyCode.Insert) {
				ClipboardSet(selectionText, false);
			} else if (@event.keyCode == KeyCode.F) {
				search_text = selectionTextLocal;
			}
		} else if (@event.shift) {
			if (@event.keyCode == KeyCode.Insert) {
				selectionText = ClipboardGet();
				ensure_caret_visibility = true; //EnsureCaretVisibility();
			} else if (@event.keyCode == KeyCode.Delete) {
				ClipboardSet(selectionText, false);
				selectionText = "";
				ensure_caret_visibility = true; //EnsureCaretVisibility();
			}
		}
	}
	void ProcessTextInput_MouseDown() {
		if (mouse_is_insideR) {
			consume_event = true;
			dragPosStart = mp;
			if (@event.button == 2) {
				dragScrollStart = scrollPos;
			} else {
				// Click: empty selection
				// Double click: select word
				// Triple click: select whole line (including \n)
				// Drag from empty: select range
				// Drag from non-empty: drag&drop (+change system cursor)
				// LMB - select/drag, RMB - context menu?, MMB - scroll

				var mouse_caret = NearestCaretPos(mp, TextPosMode.Global, false);
				SetSelectionEnd(mouse_caret, !@event.shift);

				if (@event.button == 0) {
					mouseSelectionMode = 1;
					if (!@event.shift) {
						useWordL=true; useWordR=true;
						if (clickCount == 2) {
							mouseSelectionMode = 2;
							MoveSelectionEnd(CaretMoves.WordStart, true, false);
							MoveSelectionEnd(CaretMoves.WordEnd, false, false);
						} else if (clickCount >= 3) {
							mouseSelectionMode = 3;
							MoveSelectionEnd(CaretMoves.LineStart, true, false);
							MoveSelectionEnd(CaretMoves.LineDown, false, false);
						}
						useWordL=true; useWordR=false;
					}
					mouseSelectionMin = selectionMin;
					mouseSelectionMax = selectionMax;
				}
			}
		}
	}
	void ProcessTextInput_MouseUp() {
		if (mouse_is_insideR) {
			DragAndDrop.PrepareStartDrag(); // Clear generic data
			consume_event = true;
		}
	}
	void ProcessTextInput_MouseDrag() {
		if (mouseSelectionMode != 0) {
			consume_event = true;
			var mouse_caret = NearestCaretPos(mp, TextPosMode.Global, true);
			if (mouseSelectionMode == 1) {
				SetSelectionEnd(mouseSelectionMin, true, false);
				SetSelectionEnd(mouse_caret, false, true);
			} else if (mouseSelectionMode == 2) {
				useWordL=true; useWordR=true;
				if (mouse_caret <= mouseSelectionMin) {
					SetSelectionEnd(mouseSelectionMax, true, false);
					SetSelectionEnd(mouse_caret, false, false);
					MoveSelectionEnd(CaretMoves.WordStart, false, true);
				} else if (mouse_caret >= mouseSelectionMax) {
					SetSelectionEnd(mouseSelectionMin, true, false);
					SetSelectionEnd(mouse_caret, false, false);
					MoveSelectionEnd(CaretMoves.WordEnd, false, true);
				} else {
					SetSelectionEnd(mouseSelectionMin, true, false);
					SetSelectionEnd(mouseSelectionMax, false, false);
				}
				useWordL=true; useWordR=false;
			} else {
				if (mouse_caret <= mouseSelectionMin) {
					SetSelectionEnd(mouseSelectionMax, true, false);
					SetSelectionEnd(mouse_caret, false, false);
					MoveSelectionEnd(CaretMoves.LineStart, false, true);
				} else if (mouse_caret >= mouseSelectionMax) {
					SetSelectionEnd(mouseSelectionMin, true, false);
					SetSelectionEnd(mouse_caret, false, false);
					MoveSelectionEnd(CaretMoves.LineEnd, false, false);
					MoveSelectionEnd(CaretMoves.SymbolNext, false, true);
				} else {
					SetSelectionEnd(mouseSelectionMin, true, false);
					SetSelectionEnd(mouseSelectionMax, false, false);
				}
			}
		} else if (@event.button == 2) {
			consume_event = true;
			ScrollTo(dragScrollStart - (mp - dragPosStart));
		} else if (mouse_is_insideR) {
			// consume only if there is actually a dragged content;
			// otherwise it would prevent scrollbar frow working
			// normally when mouse is in text area
			//consume_event = true;
		}
	}
	void ProcessTextInput_MouseMove() {
		if (mouse_is_insideR) {
			//consume_event = true;
		}
	}
	void ProcessTextInput_DragAndDrop() {
		if (mouse_is_insideR) {
			//consume_event = true;
			var mouse_caret = NearestCaretPos(mp, TextPosMode.Global, true, false);
			var text_under_mouse = ElementText(mouse_caret);
			bool ignore_drag = TextWithFields.IsTag(text_under_mouse, TextWithFields.TAG_OBJECT);
			if (!ignore_drag) {
				// https://helmutduregger.wordpress.com/2012/03/20/using-draganddrop-with-unity-gui/
				// http://forum.unity3d.com/threads/223242-Drag-and-Drop-in-the-editor-Explanation
				if (@event.type == EventType.DragUpdated) {
					DragAndDrop.AcceptDrag();
					DragAndDrop.visualMode = DragAndDropVisualMode.Link;
					mouse_caret = NearestCaretPos(mp, TextPosMode.Global, true);
					SetSelectionEnd(mouse_caret, true, true);
					Focus();
					consume_event = true;
				} else if (@event.type == EventType.DragPerform) {
					string result = "";
					foreach (var obj in DragAndDrop.objectReferences) {
						string obj_field_txt = TextWithFields.MakeMeta(
							TextWithFields.TAG_OBJECT+SerializeObject(obj));
						if (result.Length != 0) result += ", ";
						result += obj_field_txt;
					}
					selectionText = result;
					ensure_caret_visibility = true; //EnsureCaretVisibility();
					consume_event = true;
				} else if (@event.type == EventType.DragExited) {
					// This event can happen when:
					// * User pressed Esc
					// * Mouse is out of the 3D View
					// * User unpressed mouse while holding Alt
					consume_event = true;
				}
			}
		}
	}
	#endregion

	#region Text rendering
	Color indentation_viz_color = new Color(0.5f, 0.5f, 0.5f, 0.5f);
	void RenderLines(bool renderFolds, bool renderFields, bool renderText) {
		bool is_repaint = (@event.type == EventType.Repaint);
		bool renderFoldsOnly = !(renderFields || renderText);

		if (renderFolds) {
			GUI.Box(txtRectL, "", box_style);
		}

		CaretPosition selMin = selectionMin, selMax = selectionMax;

		bool is_focused = (EditorGUIUtility.keyboardControl == txtControlID);

		float offset_x = Mathf.Round(txtRectR.x - scrollPos.x);
		float offset_y = Mathf.Round(txtRectR.y - scrollPos.y);

		float scrollX0 = scrollPos.x;
		float scrollX1 = scrollPos.x+txtRectR.width;
		float scrollY0 = scrollPos.y;
		float scrollY1 = scrollPos.y+txtRectR.height;

		#region SEARCH VISIBLE LINES
		int iL0 = NearestCaretPos(new Vector2(1, scrollY0)).iL;
		int iL1 = NearestCaretPos(new Vector2(1, scrollY1)).iL;
		#endregion

		int increment = 1;
		for (int iL = iL0; iL <= iL1; iL += increment) {
			var dLine = dLines[iL];
			increment = dLine.fold_count+1;

			var lrect = dLine.rect;
			lrect.x += offset_x;
			lrect.y += offset_y;

			#region FOLD BUTTON
			if (renderFolds && (dLine.can_fold || (dLine.fold_count != 0))) {
				float foldSize = Mathf.Min(txtRectL.width, lrect.height);
				Rect foldRect = new Rect(txtRectL.x, lrect.y, foldSize, foldSize);
				foldRect.x += (txtRectL.width - foldSize)*0.5f;
				foldRect.y += (lrect.height - foldSize)*0.5f;
				if (GUI.Button(foldRect, (dLine.fold_count != 0) ? "+" : "-", fold_style)) {
					toggled_fold_iL = iL;
				}
			}
			#endregion

			if (renderFoldsOnly) continue;

			int nElements = dLine.elements.Count;
			int iEmax = nElements-1;

			#region SEARCH VISIBLE ELEMENTS
			int iE0, iE1;
			if (wrapWords) {
				iE0 = InsertionSearch(dLine.elements, scrollY0 - dLine.rect.y, (pos, elem) => {
					return (pos > elem.yMaxL) ? 1 : -1;
				});
				iE0 = Mathf.Max(iE0, 0);
				iE1 = InsertionSearch(dLine.elements, scrollY1 - dLine.rect.y, (pos, elem) => {
					return (pos >= elem.yMinL) ? 1 : -1;
				});
				iE1 = Mathf.Min(iE1, iEmax);
			} else {
				iE0 = InsertionSearch(dLine.elements, scrollX0, (pos, elem) => {
					return System.Math.Sign(pos - elem.rect.x);
				});
				iE0 = Mathf.Max(iE0-1, 0);
				iE1 = InsertionSearch(dLine.elements, scrollX1, (pos, elem) => {
					return System.Math.Sign(pos - (elem.rect.x + elem.rect.width));
				});
				iE1 = Mathf.Min(iE1+1, iEmax);
			}
			#endregion

			bool show_selection = is_focused && ((iL >= selMin.iL) && (iL <= selMax.iL));
			show_selection &= renderText;
			int iE0_sel = (iL == selMin.iL) ? selMin.iE : 0;
			int iE1_sel = (iL == selMax.iL) ? selMax.iE : nElements+1;

			float xMin = 0, yMin = 0, xMax = 0, yMax = 0;
			var elements = dLine.elements;
			for (int iE = iE0; iE <= iE1; iE++) {
				var elem = elements[iE];

				var rect = elem.rect;
				rect.x += lrect.x;
				rect.y += lrect.y;

				if (elem.IsMeta) {
					if (renderFields) RenderField(rect, elem.text, iL);
				} else if (renderText) {
					if (is_repaint && showIndentation && (iE > 0) && (iE < dLine.indentation)) {
						var rectL = elem.rectL;
						rectL.x += lrect.x;
						rectL.y += lrect.y;
						DrawTextCursor(rectL, false, indentation_viz_color);
					}
					if (is_repaint) DrawText(elem.guiStyle, rect, elem.text);
				}

				#region SELECTION
				if (show_selection) {
					if (iE0_sel == iE1_sel) {
						if ((iE0_sel == 0) && (iE == 0)) {
							if (is_repaint) DrawTextCursor(rect, false);
						} else if (iE0_sel == iE+1) {
							if (is_repaint) DrawTextCursor(rect, true);
						}
					} else if ((iE0_sel <= iE) && (iE1_sel > iE)) {
						bool is_start = (iE0_sel == iE) || ((iE0_sel < iE) && (elements[iE-1].yMaxL <= elem.yMinL));
						bool is_full_line = (iE1_sel > nElements) || ((iE1_sel > iE+1) && (elements[iE+1].yMinL >= elem.yMaxL));
						bool is_end = (iE1_sel == iE+1) || (iE == iE1) || ((iE1_sel > iE+1) && (elements[iE+1].yMinL >= elem.yMaxL));

						if (is_start) {
							xMin = elem.rect.xMin;
							yMin = elem.yMinL;
							xMax = elem.rect.xMax;
							yMax = elem.yMaxL;
						} else {
							xMin = Mathf.Min(xMin, elem.rect.xMin);
							yMin = Mathf.Min(yMin, elem.yMinL);
							xMax = Mathf.Max(xMax, elem.rect.xMax);
							yMax = Mathf.Max(yMax, elem.yMaxL);
						}

						if (is_end) {
							rect = Rect.MinMaxRect(xMin, yMin, xMax, yMax);
							rect.x += lrect.x;
							rect.y += lrect.y;
							if (is_full_line) rect.xMax = txtRectR.xMax;
							if (is_repaint) DrawTextSelection(rect);
						}
					}
				}
				#endregion
			}

			#region SELECTION FOR END-OF-LINE CASE
			if (show_selection && (iE0_sel == nElements)) {
				Rect rect;
				if (nElements != 0) {
					rect = elements[nElements-1].rectL;
					rect.x += lrect.x;
					rect.y += lrect.y;
					rect.x += rect.width;
				} else {
					rect = lrect;
				}

				if (iE0_sel == iE1_sel) {
					if (nElements == 0) { // don't draw second time if it's a non-empty line
						rect.width = 1;
						if (is_repaint) DrawTextCursor(rect, false);
					}
				} else {
					rect.xMax = txtRectR.xMax;
					if (is_repaint) DrawTextSelection(rect);
				}
			}
			#endregion

			bool show_line_separators = true;
			if (show_line_separators) {
				Rect rect = lrect;
				rect.yMin = rect.yMax-1;
				rect.xMax = txtRectR.xMax;
				DrawTextSelection(rect, indentation_viz_color);
			}
		}
	}
	void RenderField(Rect rect, string tag_and_data, int iL) {
		string field_data = tag_and_data;
		string tag = TextWithFields.TagAndData(ref field_data);
		int storage_id;
		if (!int.TryParse(field_data, out storage_id)) tag = null;

		switch (tag) {
		case TextWithFields.TAG_OBJECT:
			var prev_obj = objects_storage[storage_id];
			var new_obj = ObjectField(rect, prev_obj);
			if (new_obj != prev_obj) AddModifiedLine(iL);
			objects_storage[storage_id] = new_obj;
			break;
		case TextWithFields.TAG_CURVE:
			curves_storage[storage_id] = EditorGUI.CurveField(rect, curves_storage[storage_id]);
			break;
		case TextWithFields.TAG_COLOR:
			colors_storage[storage_id] = EditorGUI.ColorField(rect, colors_storage[storage_id]);
			break;
		case TextWithFields.TAG_TOGGLE:
			toggles_storage[storage_id] = EditorGUI.Toggle(rect, toggles_storage[storage_id]);
			break;
		case TextWithFields.TAG_PERCENTAGE:
			// Slider always has a 50-pixel textbox on right side
			// (its reaction zone extends even further than 50 pixels)
			Rect slider_rect = rect;
			slider_rect.width += 50-1;
			slider_rect.x += 1;
			slider_rect.x -= rect.x;
			slider_rect.y -= rect.y;
			GUI.BeginGroup(rect);
			percentages_storage[storage_id] = EditorGUI.Slider(
				slider_rect, percentages_storage[storage_id], 0f, 1f);
			GUI.EndGroup();
			break;
		default:
			GUI.backgroundColor = Color.red;
			GUI.Box(rect, "?");
			GUI.backgroundColor = GUI_backgroundColor;
			break;
		}
	}
	Object ObjectField(Rect rect, Object obj) {
		if (obj == null) {
			var prev_color = EditorStyles.objectField.normal.textColor;
			
			Color no_obj_color = prev_color;
			no_obj_color.a = 0.25f;
			EditorStyles.objectField.normal.textColor = no_obj_color;
			
			obj = EditorGUI.ObjectField(rect, null, typeof(Object), true);
			
			EditorStyles.objectField.normal.textColor = prev_color;
		} else {
			obj = EditorGUI.ObjectField(rect, obj, typeof(Object), true);
		}
		// A weird error can be caused by referencing object that later was deleted
		// See http://forum.unity3d.com/threads/108578-Odd-Unity-Editor-Error
		// So, make sure that if obj is comparable to null, we replace it with actual null
		return (obj != null) ? obj : null;
	}
	void DrawText(GUIStyle style, Rect rect, string text) {
		if (@event.type != EventType.Repaint) return;
		gui_content.text = text;
		style.Draw(rect, gui_content, txtControlID);
	}
	void DrawTextCursor(Rect rect, bool atEnd, Color color) {
		var prev_color = skin.settings.cursorColor;
		skin.settings.cursorColor = color;
		DrawTextCursor(rect, atEnd);
		skin.settings.cursorColor = prev_color;
	}
	void DrawTextCursor(Rect rect, bool atEnd) {
		if (@event.type != EventType.Repaint) return;
		if (atEnd) rect.x += rect.width;
		selection_style.fontSize = (int)rect.height;
		gui_content.text = "";
		selection_style.DrawWithTextSelection(rect, gui_content, txtControlID, 0, 0);
	}
	void DrawTextSelection(Rect rect, Color color) {
		var prev_color = skin.settings.selectionColor;
		skin.settings.selectionColor = color;
		DrawTextSelection(rect);
		skin.settings.selectionColor = prev_color;
	}
	void DrawTextSelection(Rect rect) {
		//if (@event.type != EventType.Repaint) return;
		var wrap_rect = rect;
		rect.x = 0;
		rect.y = 0;
		GUI.BeginGroup(wrap_rect);
		if (@event.type == EventType.Repaint) {
			selection_style.fontSize = Mathf.Max((int)rect.height, 8); // avoid 1-pixel height fonts (too many letters!)
			int len = Mathf.CeilToInt(rect.width/TextWidth(selection_style, "W"));
			if (len > 0) {
				gui_content.text = new string('W', len);
				selection_style.DrawWithTextSelection(rect, gui_content, txtControlID, 0, gui_content.text.Length);
			}
		}
		GUI.EndGroup();
	}
	#endregion
}
