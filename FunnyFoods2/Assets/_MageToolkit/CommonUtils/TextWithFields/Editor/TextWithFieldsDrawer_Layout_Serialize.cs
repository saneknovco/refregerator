using UnityEditor;
using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.Globalization;
using Bini.Utils;

[CustomPropertyDrawer(typeof(TextWithFields))]
partial class TextWithFieldsDrawer : PropertyDrawer {
	#region Field serialization
	static char[] record_separators = new char[]{'\t'};
	static char[] subrecord_separators = new char[]{';'};

	public static List<GameObject> cachedSceneObjects = null;

	public static string SerializeObject(UnityEngine.Object data) {
		if (data == null) return "$";
		string obj_path = "";
		string hashcode = "";

		// obj_data is either an object in scene or an asset
		// object prefabs can have asset path, but if they
		// are in the scene, we treat them as normal objects
		var gObj = data as GameObject;
		if (gObj != null) {
			// Check if this is a prefab asset in project folder
			if (!AssetDatabase.Contains(gObj.transform.root.gameObject)) {
				obj_path = gObj.transform.HierarchyPath();
				// Hashcode is necessary only for scene objects,
				// since assets are uniquely defined by their paths
				hashcode = gObj.GetHashCode().ToString();
			}
		}
		if (string.IsNullOrEmpty(obj_path)) {
			obj_path = FileUtils.GetAssetPath(data);
			if (obj_path == null) obj_path = "";
		}

		var str_builder = new System.Text.StringBuilder();
		str_builder.Append("$");
		str_builder.Append(obj_path);
		str_builder.Append("\t");
		str_builder.Append(hashcode);
		return str_builder.ToString();
	}
	public static UnityEngine.Object DeserializeObject(string data) {
		try {
			if (data[0] != '$') return null;
			var records = data.Substring(1).Split(record_separators);
			string obj_path = records[0];
			string hashcode = records[1];
			if (!string.IsNullOrEmpty(hashcode)) {
				int hash = int.Parse(hashcode);
				if (cachedSceneObjects == null) {
					cachedSceneObjects = new List<GameObject>(SceneObjects.Objects());
				}
				string obj_name = System.IO.Path.GetFileName(obj_path);
				GameObject best_obj = null;
				bool found_path = false;
				for (int i = 0; i < cachedSceneObjects.Count; i++) {
					var gObj = cachedSceneObjects[i];
					//if (gObj.name != obj_name) continue;
					if (gObj.transform.HierarchyPath() == obj_path) {
						if (gObj.GetHashCode() == hash) {
							return gObj;
						}
						best_obj = gObj;
						found_path = true;
					} else if (gObj.name == obj_name) {
						if (gObj.GetHashCode() == hash) {
							return gObj;
						}
						if (!found_path) best_obj = gObj;
					}
				}
				return best_obj;
			} else {
				AssetDatabase.ImportAsset(obj_path);
				return AssetDatabase.LoadAssetAtPath(obj_path, typeof(UnityEngine.Object));
			}
		} catch (System.Exception) {
			return null;
		}
	}
	
	public static string SerializeCurve(AnimationCurve data) {
		if (data == null) return "$";
		var str_builder = new System.Text.StringBuilder();
		str_builder.Append("$");
		str_builder.Append(data.preWrapMode.ToString());
		str_builder.Append("\t");
		str_builder.Append(data.postWrapMode.ToString());
		str_builder.Append("\t");
		for (int i = 0; i < data.keys.Length; i++) {
			var key = data.keys[i];
			str_builder.Append(key.time);
			str_builder.Append(";");
			str_builder.Append(key.value);
			str_builder.Append(";");
			str_builder.Append(key.tangentMode);
			str_builder.Append(";");
			str_builder.Append(key.inTangent);
			str_builder.Append(";");
			str_builder.Append(key.outTangent);
			str_builder.Append("\t");
		}
		return str_builder.ToString();
	}
	public static AnimationCurve DeserializeCurve(string data) {
		try {
			if (data[0] != '$') return new AnimationCurve();
			var records = data.Substring(1).Split(record_separators);
			var curve = new AnimationCurve();
			curve.preWrapMode = (WrapMode)System.Enum.Parse(typeof(WrapMode), records[0]);
			curve.postWrapMode = (WrapMode)System.Enum.Parse(typeof(WrapMode), records[1]);
			for (int i = 2; i < records.Length; i++) {
				if (string.IsNullOrEmpty(records[i])) continue;
				var key = new Keyframe();
				var key_records = records[i].Split(subrecord_separators);
				key.time = float.Parse(key_records[0]);
				key.value = float.Parse(key_records[1]);
				key.tangentMode = int.Parse(key_records[2]);
				key.inTangent = float.Parse(key_records[3]);
				key.outTangent = float.Parse(key_records[4]);
				curve.AddKey(key);
			}
			return curve;
		} catch (System.Exception) {
			return new AnimationCurve();
		}
	}
	
	public static string SerializeColor(Color data) {
		var str_builder = new System.Text.StringBuilder();
		str_builder.Append("$");
		str_builder.Append(data.r);
		str_builder.Append("\t");
		str_builder.Append(data.g);
		str_builder.Append("\t");
		str_builder.Append(data.b);
		str_builder.Append("\t");
		str_builder.Append(data.a);
		return str_builder.ToString();
	}
	public static Color DeserializeColor(string data) {
		try {
			if (data[0] != '$') return Color.white;
			var records = data.Substring(1).Split(record_separators);
			float r = float.Parse(records[0]);
			float g = float.Parse(records[1]);
			float b = float.Parse(records[2]);
			float a = float.Parse(records[3]);
			return new Color(r, g, b, a);
		} catch (System.Exception) {
			return Color.white;
		}
	}
	
	public static string SerializeToggle(bool data) {
		return "$"+data;
	}
	public static bool DeserializeToggle(string data) {
		try {
			if (data[0] != '$') return false;
			return bool.Parse(data.Substring(1));
		} catch (System.Exception) {
			return false;
		}
	}
	
	public static string SerializePercentage(float data) {
		return "$"+data;
	}
	public static float DeserializePercentage(string data) {
		try {
			if (data[0] != '$') return 0f;
			return Mathf.Clamp01(float.Parse(data.Substring(1)));
		} catch (System.Exception) {
			return 0f;
		}
	}
	#endregion
	
	#region Serialization/Deserealization
	delegate void SerializerDelegate(TextWithFields.SerializerState serializer_state);
	SerializerDelegate deserializer;
	SerializerDelegate serializer;
	TextWithFields.SerializerState serializer_state;

	// To not be included in final build, custom (de)serializers can use
	// [System.Diagnostics.Conditional("UNITY_EDITOR")]

	void FindSerializers() {
		var target_obj = property.serializedObject.targetObject; // MonoBehaviour or ScriptableObject

		deserializer = GetDelegate<SerializerDelegate>(target_obj, property.name+"_Deserializer");
		if (deserializer == null) deserializer = DefaultDeserializer;

		serializer = GetDelegate<SerializerDelegate>(target_obj, property.name+"_Serializer");
		if (serializer == null) serializer = DefaultSerializer;
	}

	string PreprocessInput(string input_string) {
		var str_builder = new System.Text.StringBuilder();
		cachedSceneObjects = null;
		foreach (string txt_elem in TextWithFields.ParseElements(input_string)) {
			string txt = txt_elem;
			if (TextWithFields.IsMeta(txt_elem)) {
				string field_data = txt_elem;
				string tag = TextWithFields.TagAndData(ref field_data);
				int storage_id = -1;
				switch (tag) {
				case TextWithFields.TAG_OBJECT: storage_id = objects_storage.AddSerialized(field_data); break;
				case TextWithFields.TAG_CURVE: storage_id = curves_storage.AddSerialized(field_data); break;
				case TextWithFields.TAG_COLOR: storage_id = colors_storage.AddSerialized(field_data); break;
				case TextWithFields.TAG_TOGGLE: storage_id = toggles_storage.AddSerialized(field_data); break;
				case TextWithFields.TAG_PERCENTAGE: storage_id = percentages_storage.AddSerialized(field_data); break;
				}
				if (storage_id >= 0) {
					txt = TextWithFields.MakeMeta(tag+storage_id);
				}
			}
			str_builder.Append(txt);
		}
		cachedSceneObjects = null;
		return str_builder.ToString();
	}
	string PostprocessOutput(string input_string) {
		var str_builder = new System.Text.StringBuilder();
		foreach (string txt_elem in TextWithFields.ParseElements(input_string)) {
			string txt = txt_elem;
			if (TextWithFields.IsMeta(txt_elem)) {
				string field_data = txt_elem;
				string tag = TextWithFields.TagAndData(ref field_data);
				int storage_id;
				if (!int.TryParse(field_data, out storage_id)) tag = null;
				field_data = null;
				switch (tag) {
				case TextWithFields.TAG_OBJECT: field_data = objects_storage.GetSerialized(storage_id); break;
				case TextWithFields.TAG_CURVE: field_data = curves_storage.GetSerialized(storage_id); break;
				case TextWithFields.TAG_COLOR: field_data = colors_storage.GetSerialized(storage_id); break;
				case TextWithFields.TAG_TOGGLE: field_data = toggles_storage.GetSerialized(storage_id); break;
				case TextWithFields.TAG_PERCENTAGE: field_data = percentages_storage.GetSerialized(storage_id); break;
				}
				if (field_data != null) {
					txt = TextWithFields.MakeMeta(tag+field_data);
				}
			}
			str_builder.Append(txt);
		}
		return str_builder.ToString();
	}

	void UnstoreRange(CaretPosition selMin, CaretPosition selMax) {
		foreach (var elem in IterateElements(selMin, selMax)) {
			if (elem.IsMeta) {
				string field_data = elem.text;
				string tag = TextWithFields.TagAndData(ref field_data);
				int storage_id;
				if (!int.TryParse(field_data, out storage_id)) tag = null;
				switch (tag) {
				case TextWithFields.TAG_OBJECT: objects_storage.Unstore(storage_id); break;
				case TextWithFields.TAG_CURVE: curves_storage.Unstore(storage_id); break;
				case TextWithFields.TAG_COLOR: colors_storage.Unstore(storage_id); break;
				case TextWithFields.TAG_TOGGLE: toggles_storage.Unstore(storage_id); break;
				case TextWithFields.TAG_PERCENTAGE: percentages_storage.Unstore(storage_id); break;
				}
			}
		}
	}

	void ReplaceSelection(string input_string) {
		EnsureUnfoldedAtSelection();

		if ((Time.realtimeSinceStartup - modified_last_time) > modified_group_period) {
			// Begin new undo group (but update caret position first)
			SaveCaretPosition();
			Undo.IncrementCurrentGroup();
		}
		modified_last_time = Time.realtimeSinceStartup;

		int undoIndex = Undo.GetCurrentGroup();

		// Calculate indices...
		CaretPosition selMin = selectionMin, selMax = selectionMax;
		int nAffected = selMax.iL - selMin.iL + 1;
		int nFromEnd = dLines[selMax.iL].elements.Count - selMax.iE;
		
		// Free any fields that are within the replaced part
		UnstoreRange(selMin, selMax);

		// Parse input string and asign fields' indices
		input_string = PreprocessInput(input_string);
		
		// Serialize prefix and suffix, combine with input
		string prefix = Serialize(selMin.iL, 0, selMin.iL, selMin.iE, false);
		string suffix = Serialize(selMax.iL, selMax.iE, selMax.iL, dLines[selMax.iL].elements.Count, false);
		input_string = prefix + input_string + suffix;

		// Split result to lines
		var replace_lines = input_string.Split(newline_separators, System.StringSplitOptions.None);
		int nFinal = replace_lines.Length;

		// Ensure the necessary number of lines to replace
		if (nAffected > nFinal) {
			for (int nL = nFinal; nL < nAffected; nL++) {
				lines.RemoveAt(selMin.iL+nFinal);
				dLines.RemoveAt(selMin.iL+nFinal);
			}
		} else if (nAffected < nFinal) {
			for (int nL = nAffected; nL < nFinal; nL++) {
				lines.Insert(selMin.iL+nL, "");
				dLines.Insert(selMin.iL+nL, new TextWithFields.DisplayLine());
			}
		}

		int iL_min = selMin.iL;
		int iL_max = selMin.iL+(nFinal-1);

		// Actually replace lines, make sure dLines are reseted
		for (int iL = iL_min; iL <= iL_max; iL++) {
			lines[iL] = replace_lines[iL-iL_min];
			dLines[iL] = new TextWithFields.DisplayLine();
		}

		// Deserialize till the end, due to potential folding changes
		Deserialize(iL_min, iL_max, false);

		selMax.iL = iL_max;
		selMax.iE = dLines[selMax.iL].elements.Count - nFromEnd;
		SetSelectionEnd(selMax, true, false);
		
		SaveCaretPosition();

		Undo.CollapseUndoOperations(undoIndex);
	}

	void ToggleFold(int iL) {
		// Begin new undo group (but update caret position first)
		SaveCaretPosition();
		Undo.IncrementCurrentGroup();
		modified_last_time = 0;

		// Calculate how many lines we need to fold
		var dLine = dLines[iL];
		int prev_fold_count = dLine.fold_count;
		int fold_count = 0;
		if (dLine.fold_count == 0) {
			for (int _iL = iL+1; _iL < dLines.Count; _iL++) {
				var _dLine = dLines[_iL];
				if (_dLine.isAllWhitespace) continue;
				if (_dLine.fold_level <= dLine.fold_level) break;
				fold_count = _iL - iL;
			}
		}

		// nothing has changed in the line except for the fold tag, so we can just replace it
		dLine.fold_count = fold_count;
		lines[iL] = Serialize(iL, 0, iL, dLine.elements.Count, false);

		// deserialize the rest of the document (further folds are affected)
		Deserialize(iL, iL, false);
		AddModifiedLine(iL, iL+prev_fold_count);

		SaveCaretPosition();
	}
	void UnfoldRange(int iL0, int iL1) {
		bool were_folded = false;
		bool were_unfoldings = false;

		for (int iL = iL1; iL >= 0; iL--) {
			if (iL < iL0) break;

			var dLine = dLines[iL];

			if (dLine.is_folded) {
				were_folded = true;
				iL0--;
			}

			if (were_folded && dLine.can_fold && (dLine.fold_count != 0)) {
				were_unfoldings = true;
				iL1 = Mathf.Max(iL1, iL+dLine.fold_count);

				dLine.fold_count = 0;
				lines[iL] = Serialize(iL, 0, iL, dLine.elements.Count, false);
				Deserialize(iL, iL, false);
			}
		}

		if (were_unfoldings) {
			// Begin new undo group (but update caret position first)
			SaveCaretPosition();
			Undo.IncrementCurrentGroup();
			modified_last_time = 0;
			
			AddModifiedLine(iL0, iL1);
			
			SaveCaretPosition();
		}
	}

	static void DefaultDeserializer(TextWithFields.SerializerState serializer_state) {
		var style = serializer_state.base_style;

		var lines = serializer_state.lines;
		var dLines = serializer_state.dLines;
		int iL0 = serializer_state.iL0;
		int iL1 = serializer_state.iL1;

		int iL_last_nonwhitespace = 0;
		// Search for preceding non-whitespace dLine
		for (int iL = iL0-1; iL >= 0; iL--) {
			if (!dLines[iL].isAllWhitespace) {
				iL_last_nonwhitespace = iL;
				break;
			}
		}

		for (int iL = iL0; iL <= iL1; iL++) {
			var line = lines[iL];
			var dLine = dLines[iL];

			// TODO: actually test styling?
			dLine.zero_style = style; // for now

			var elements = new List<TextWithFields.DisplayElement>();

			bool only_whitespace = true;
			dLine.indentation = 0;
			dLine.can_fold = (dLine.fold_count != 0);
			dLine.fold_level = 0;

			foreach (string text_data in TextWithFields.ParseElements(line)) {
				if (TextWithFields.IsTag(text_data, TextWithFields.TAG_FOLD)) {
					string meta_data = text_data;
					TextWithFields.TagAndData(ref meta_data);
					int fold_count;
					if (!int.TryParse(meta_data, out fold_count)) fold_count = 0;
					dLine.fold_count = fold_count;
				} else {
					if (only_whitespace) {
						only_whitespace = char.IsWhiteSpace(text_data, 0);
						if (only_whitespace) dLine.indentation += 1;
					}
					var elem = new TextWithFields.DisplayElement();
					elem.text = text_data;
					elem.guiStyle = style;
					elements.Add(elem);
				}
			}

			dLine.fold_level = (dLine.indentation+1); // ?

			dLine.isAllWhitespace = only_whitespace;

			if (!only_whitespace) {
				if (iL > 0) {
					if (dLine.indentation > dLines[iL_last_nonwhitespace].indentation) {
						dLines[iL_last_nonwhitespace].can_fold = true;
					} else {
						dLines[iL_last_nonwhitespace].can_fold = false;
					}
				}
				iL_last_nonwhitespace = iL;
			} else {
				dLine.can_fold = false;
				dLine.fold_count = 0;
			}

			dLine.elements = elements;
			dLine.iL = iL;
			dLine.iE = 0;
		}

		// Check if last line can be folded due to out-of-range lines
		for (int iL = iL1+1; iL < dLines.Count; iL++) {
			var dLine = dLines[iL];
			if (!dLine.isAllWhitespace) {
				if (dLine.indentation > dLines[iL_last_nonwhitespace].indentation) {
					dLines[iL_last_nonwhitespace].can_fold = true;
				} else {
					dLines[iL_last_nonwhitespace].can_fold = false;
				}
				break;
			}
		}
	}
	void Deserialize(bool load_caret_pos=true) {
		MatchLinesCount();
		deserializer(serializer_state);
		AddModifiedLine(serializer_state.iL0, serializer_state.iL1);
		if (load_caret_pos) LoadCaretPosition();
	}
	void Deserialize(int iL0, int iL1, bool load_caret_pos=true) {
		serializer_state.iL0 = Mathf.Max(iL0, 0);
		serializer_state.iL1 = Mathf.Min(iL1, lines.Count-1);
		Deserialize(load_caret_pos);
	}
	void DeserializeFull() {
		objects_storage.Clear();
		curves_storage.Clear();
		colors_storage.Clear();
		toggles_storage.Clear();
		percentages_storage.Clear();

		Deserialize(0, lines.Count-1);

		for (int iL = 0; iL < lines.Count; iL++) {
			var dLine = dLines[iL];
			var elements = dLine.elements;
			int nElements = elements.Count;
			for (int iE = 0; iE < nElements; iE++) {
				var elem = elements[iE];
				if (elem.IsMeta) {
					string field_data = elem.text;
					string tag = TextWithFields.TagAndData(ref field_data);
					int storage_id;
					if (!int.TryParse(field_data, out storage_id)) tag = null;
					switch (tag) {
					case TextWithFields.TAG_OBJECT: objects_storage.Store(storage_id); break;
					case TextWithFields.TAG_CURVE: curves_storage.Store(storage_id); break;
					case TextWithFields.TAG_COLOR: colors_storage.Store(storage_id); break;
					case TextWithFields.TAG_TOGGLE: toggles_storage.Store(storage_id); break;
					case TextWithFields.TAG_PERCENTAGE: percentages_storage.Store(storage_id); break;
					}
				}
			}
		}
	}

	static void DefaultSerializer(TextWithFields.SerializerState serializer_state) {
		bool for_clipboard = serializer_state.for_clipboard;

		string newline = serializer_state.newline;
		if (string.IsNullOrEmpty(newline)) newline = System.Environment.NewLine;

		var lines = serializer_state.lines;
		var dLines = serializer_state.dLines;
		int iL0 = serializer_state.iL0;
		int iL1 = serializer_state.iL1;
		int iE0 = serializer_state.iE0;
		int iE1 = serializer_state.iE1;

		var str_builder = new System.Text.StringBuilder();

		for (int iL = iL0; iL <= iL1; iL++) {
			var dLine = dLines[iL];

			int nElements = dLine.elements.Count;
			int _iE0 = (iL == iL0) ? iE0 : 0;
			int _iE1 = (iL == iL1) ? iE1 : nElements+1;

			// Include folding information if it's valid and at least one character is actually required
			if ((_iE0 != _iE1) && dLine.can_fold && (dLine.fold_count > 0)) {
				str_builder.Append(TextWithFields.MakeMeta(TextWithFields.TAG_FOLD + dLine.fold_count));
			}
			
			for (int iE = _iE0; iE < _iE1; iE++) {
				if (iE >= nElements) {
					str_builder.Append(newline);
					break;
				} else {
					var elem = dLine.elements[iE];
					str_builder.Append(elem.text);
				}
			}
		}

		serializer_state.result = str_builder.ToString();
	}
	string Serialize() {
		serializer(serializer_state);
		string result = serializer_state.result;
		return serializer_state.for_clipboard ? PostprocessOutput(result) : result;
	}
	string Serialize(int iL0, int iE0, int iL1, int iE1, bool for_clipboard=true, string newline=null) {
		return Serialize(new CaretPosition(iL0, iE0), new CaretPosition(iL1, iE1), for_clipboard, newline);
	}
	string Serialize(CaretPosition p0, CaretPosition p1, bool for_clipboard=true, string newline=null) {
		var pMin = CaretPosition.Min(p0, p1);
		var pMax = CaretPosition.Max(p0, p1);
		serializer_state.iL0 = pMin.iL;
		serializer_state.iE0 = pMin.iE;
		serializer_state.iL1 = pMax.iL;
		serializer_state.iE1 = pMax.iE;
		serializer_state.for_clipboard = for_clipboard;
		serializer_state.newline = newline;
		return Serialize();
	}
	string SerializeSelection(bool for_clipboard=true, string newline=null) {
		return Serialize(selectionStart, selectionEnd, for_clipboard, newline);
	}
	#endregion

	#region LAYOUT
	int modified_line0 = -1;
	int modified_line1 = -1;
	bool recalculate_layout {
		get { return (modified_line0 >= 0) && (modified_line1 >= 0); }
	}
	void AddModifiedLinesAll() {
		AddModifiedLine(0);
		AddModifiedLine(lines.Count-1);
	}
	void AddModifiedLine(int iL0, int iL1) {
		AddModifiedLine(iL0);
		AddModifiedLine(iL1);
	}
	void AddModifiedLine(int iL) {
		modified_line0 = (modified_line0 < 0) ? iL : Mathf.Min(modified_line0, iL);
		modified_line1 = (modified_line1 < 0) ? iL : Mathf.Max(modified_line1, iL);
	}
	void ResetModifiedLines() {
		modified_line0 = -1;
		modified_line1 = -1;
	}

	static string tab = "\t";
	void RecalculateLayout() {
		if ((modified_line0 < 0) || (modified_line1 < 0)) return;

		int windowW = (wrapWords ? (int)(txtRectR.width) - 1 : int.MaxValue);

		maxDocWidth = 0;
		maxDocHeight = 0;

		int lineX = 0, lineY = 0;
		int lineW = 0, lineH = 0;

		if (modified_line0 > 0) {
			var lrect = dLines[modified_line0-1].rect;
			lineX = Mathf.RoundToInt(lrect.x);
			lineY = Mathf.RoundToInt(lrect.y)+Mathf.RoundToInt(lrect.height);
		}

		for (int iL = 0; iL < modified_line0; iL++) {
			var dLine = dLines[iL];
			maxDocWidth = Mathf.Max(maxDocWidth, Mathf.RoundToInt(dLine.rect.width));
			maxDocHeight = lineY;
		}

		for (int iL = modified_line0; iL < dLines.Count; iL++) {
			var dLine = dLines[iL];
			if (dLine.is_folded) modified_line1 = iL;

			if (iL <= modified_line1) {
				UpdateLineElements(dLine, windowW, out lineW, out lineH);
			} else {
				lineW = Mathf.RoundToInt(dLine.rect.width);
				lineH = Mathf.RoundToInt(dLine.rect.height);
			}

			dLine.rect = new Rect(lineX, lineY, lineW, lineH);
			lineY += lineH;

			maxDocWidth = Mathf.Max(maxDocWidth, lineW);
			maxDocHeight = lineY;

			if (dLine.fold_count > 0) {
				Rect folded_rect = new Rect(lineX, lineY, 0, 0);
				// skip folded lines
				for (int fc = 0; fc < dLine.fold_count; fc++) {
					iL+= 1;
					if (iL >= dLines.Count) break;
					var _dLine = dLines[iL];
					_dLine.rect = folded_rect;
				}
			}
		}

		var _last_caret_rect = last_caret_rect;
		last_caret_rect = CaretRect(selectionEnd);
		last_caret_rect.x = _last_caret_rect.x;

		ResetModifiedLines();
	}
	void UpdateLineElements(TextWithFields.DisplayLine dLine, int windowW, out int lineW, out int lineH) {
		if (dLine.elements.Count == 0) {
			var WH = ElementSize(dLine.zero_style, null, 0, 0);
			lineW = 0;
			lineH = (int)WH.y;
		} else {
			lineW = 0;
			lineH = 0;
			
			int linePartN = 0, wordN = 0;
			int linePartW = 0, wordW = 0;
			int linePartH = 0, wordH = 0;
			
			int jE0 = 0, jE1 = 0;
			
			bool isLoD0 = false, isLoD1 = false;
			
			int x = 0, y = 0, w = 0, h = 0;
			int nElements = dLine.elements.Count, iEmax = nElements-1;
			for (int iE = 0; iE < nElements; iE++) {
				var elem = dLine.elements[iE];
				
				var WH = ElementSize(elem, linePartW+wordW);
				w = (int)WH.x; h = (int)WH.y;
				elem.rect = new Rect(0, 0, w, h);
				dLine.elements[iE] = elem; // copy back (elem is a struct)
				
				isLoD1 = char.IsLetterOrDigit(elem.text, 0);
				if (!isLoD1 || !isLoD0) {
					// End of the last word. We already know that it fits.
					linePartW += wordW; wordW = 0;
					linePartN += wordN; wordN = 0;
					linePartH = Mathf.Max(linePartH, wordH); wordH = 0;
				}
				isLoD0 = isLoD1;
				
				int wordN0 = wordN;
				int wordW0 = wordW;
				int wordH0 = wordH;
				wordN += 1;
				wordW += w;
				wordH = Mathf.Max(wordH, h);
				
				if (linePartW+wordW > windowW) {
					if (elem.text == tab) { // this is a variable-width element
						wordW -= w;
						WH = ElementSize(elem, 0);
						w = (int)WH.x; h = (int)WH.y;
						wordW += w;
						elem.rect = new Rect(0, 0, w, h);
						dLine.elements[iE] = elem;
					}
					
					if (linePartN == 0) { // window is smaller than this word
						if (wordN > 1) { // split at last element
							linePartN += wordN0; wordN = 1;
							linePartW += wordW0; wordW = w;
							linePartH = Mathf.Max(linePartH, wordH0); wordH = h;
						} else { // window is smaller than this element
							linePartN += wordN; wordN = 0;
							linePartW += wordW; wordW = 0;
							linePartH = Mathf.Max(linePartH, wordH); wordH = 0;
						}
					}
					
					UpdateLinePart(dLine, ref x, ref y, iE, wordN,
					               ref linePartN, ref linePartW, ref linePartH, ref lineW, ref lineH);
				}
			}
			
			linePartW += wordW; wordW = 0;
			linePartN += wordN; wordN = 0;
			linePartH = Mathf.Max(linePartH, wordH); wordH = 0;
			
			UpdateLinePart(dLine, ref x, ref y, iEmax, wordN,
			               ref linePartN, ref linePartW, ref linePartH, ref lineW, ref lineH);
		}
	}
	void UpdateLinePart(TextWithFields.DisplayLine dLine, ref int x, ref int y, int iE, int wordN,
	                    ref int linePartN, ref int linePartW, ref int linePartH, ref int lineW, ref int lineH) {
		int jE1 = iE - wordN;
		int jE0 = jE1 - (linePartN-1);

		x = 0;
		int yMinL = y;
		int yMaxL = y+linePartH;
		for (int jE = jE0; jE <= jE1; jE++) {
			var elem = dLine.elements[jE];
			elem.rect = new Rect(x, yMaxL - elem.rect.height, elem.rect.width, elem.rect.height);
			elem.yMinL = yMinL;
			elem.yMaxL = yMaxL;
			dLine.elements[jE] = elem;
			x += (int)elem.rect.width;
		}
		y = yMaxL;

		lineW = Mathf.Max(lineW, linePartW);
		lineH = yMaxL;
		
		linePartN = 0;
		linePartW = 0;
		linePartH = 0;
	}
	#endregion
}
