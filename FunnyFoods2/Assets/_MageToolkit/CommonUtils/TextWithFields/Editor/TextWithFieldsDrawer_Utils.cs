﻿using UnityEditor;
using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.Globalization;
//using Bini.Utils;

partial class TextWithFieldsDrawer : PropertyDrawer {
	#region UNITY CALLBACKS
	public override float GetPropertyHeight(SerializedProperty property, GUIContent label) {
		this.property = property; // necessary for DisplayedLinesCount
		return EditorGUIUtility.singleLineHeight*(DisplayedLinesCount+2); // 1 for scrollbar, 1 for toolbar
	}
	public override void OnGUI(Rect mainRect, SerializedProperty property, GUIContent label) {
		StoreGUIState(mainRect, property, label);
		UpdateScrollbars();
		UpdateResizer();
		UpdateToolbar();
		UpdateTextArea(); // Important: after everything else
		RestoreGUIState();
	}
	#endregion
	
	#region GUI work data; init/restore GUI state
	string this_control_name;
	string search_control_name;
	SerializedProperty property;
	EditorWindow window;

	int txtControlID;

	Event @event; // @ escapes C# keywords
	bool consume_event;
	Vector2 mp;
	bool mouse_is_inside, mouse_is_insideL, mouse_is_insideR;
	int clickCount = 0;
	float last_click_time = 0;
	int mouseSelectionMode = 0;
	CaretPosition mouseSelectionMin, mouseSelectionMax;
	Vector2 dragScrollStart, dragPosStart;
	
	float modified_group_period = 0.6f;
	float modified_last_time = 0;
	
	GUIContent gui_content;
	float lineHeight, lineHeight_Text, tab_size, tab_size_min = 2f;
	bool triggerRepaint;
	
	Color GUI_color, GUI_contentColor, GUI_backgroundColor;
	bool GUI_enabled;
	int EditorGUI_indentLevel;
	
	// If we will need detailed mouse movement events, see EditorWindow.wantsMouseMove
	//window.wantsMouseMove = true;
	
	void StoreGUIState(Rect mainRect, SerializedProperty property, GUIContent label) {
		if (gui_content == null) gui_content = new GUIContent();
		gui_content.text = null;
		
		if (text_style == null) InitStyles();
		
		txtControlID = EditorGUIUtility.GetControlID(FocusType.Keyboard);
		if (this_control_name == null) {
			this_control_name = "TextWithFields:"+txtControlID;
			search_control_name = this_control_name+":search";
		}

		lineHeight = EditorGUIUtility.singleLineHeight;
		TextSize(text_style, " ", out tab_size, out lineHeight_Text, true);
		tab_size = Mathf.Round(tab_size*TextWithFieldsPreferences.TabSize);

		CalculateRects(mainRect);
		
		this.property = property;
		
		window = EditorWindow.focusedWindow;
		if (window == null) window = EditorWindow.mouseOverWindow;
		
		@event = Event.current;
		consume_event = false;
		
		mp = @event.mousePosition;
		mouse_is_inside = txtRect.Contains(mp);
		mouse_is_insideL = txtRectL.Contains(mp);
		mouse_is_insideR = txtRectR.Contains(mp);
		
		float clickPeriod = 0.4f;
		if (Time.realtimeSinceStartup > (last_click_time+clickPeriod)) {
			clickCount = 0;
		}
		if (@event.type == EventType.mouseDown) {
			last_click_time = Time.realtimeSinceStartup;
			clickCount++;
		} else if (@event.type == EventType.mouseUp) {
			mouseSelectionMode = 0;
		}
		
		modified_group_period = TextWithFieldsPreferences.SameUndoTime;
		
		triggerRepaint = false;
		
		GUI_color = GUI.color;
		GUI_contentColor = GUI.contentColor;
		GUI_backgroundColor = GUI.backgroundColor;
		GUI_enabled = GUI.enabled;
		
		// Using BeginProperty / EndProperty on the parent property means that
		// prefab override logic works on the entire property.
		EditorGUI.BeginProperty(mainRect, label, property);
		
		// Don't make child fields be indented
		EditorGUI_indentLevel = EditorGUI.indentLevel;
		EditorGUI.indentLevel = 0;
	}
	void RestoreGUIState() {
		if (consume_event) @event.Use();
		
		// Set indent back to what it was
		EditorGUI.indentLevel = EditorGUI_indentLevel;
		
		EditorGUI.EndProperty();
		
		GUI.color = GUI_color;
		GUI.contentColor = GUI_contentColor;
		GUI.backgroundColor = GUI_backgroundColor;
		GUI.enabled = GUI_enabled;
		
		if (triggerRepaint && (window != null)) window.Repaint();
	}
	#endregion
	
	#region STYLES
	GUISkin skin;
	GUIStyle box_style, text_style, selection_style, fold_style, field_style;
	void InitStyles() {
		// e.g. for skin.settings.selectionColor, skin.settings.doubleClickSelectsWord, etc.
		skin = EditorGUIUtility.GetBuiltinSkin(EditorSkin.Inspector);
		
		box_style = new GUIStyle(EditorStyles.textField);

		text_style = new GUIStyle(EditorStyles.label);
		text_style.active.textColor = EditorStyles.textField.active.textColor;
		text_style.focused.textColor = EditorStyles.textField.focused.textColor;
		text_style.hover.textColor = EditorStyles.textField.hover.textColor;
		text_style.normal.textColor = EditorStyles.textField.normal.textColor;
		text_style.richText = false;
		text_style.wordWrap = false;
		text_style.contentOffset = Vector2.zero;
		text_style.border = new RectOffset(0, 0, 0, 0);
		text_style.margin = new RectOffset(0, 0, 0, 0);
		text_style.overflow = new RectOffset(0, 0, 0, 0);
		text_style.padding = new RectOffset(0, 0, 0, 0);
		text_style.fontSize = Mathf.RoundToInt(TextWithFieldsPreferences.FontSize * EditorGUIUtility.singleLineHeight);

		selection_style = new GUIStyle(text_style);
		selection_style.normal.textColor = Color.clear;
		selection_style.active.textColor = Color.clear;
		selection_style.focused.textColor = Color.clear;
		selection_style.hover.textColor = Color.clear;

		fold_style = new GUIStyle(EditorStyles.miniButton);
		fold_style.border = new RectOffset(0, 0, 0, 0);
		fold_style.margin = new RectOffset(0, 0, 0, 0);
		fold_style.overflow = new RectOffset(0, 0, 0, 0);
		fold_style.padding = new RectOffset(0, 0, 0, 0);
		fold_style.contentOffset = -Vector2.one;

		field_style = EditorStyles.objectField;

		toggle_style_off = new GUIStyle(EditorStyles.miniButton);
		toggle_style_off.contentOffset = -Vector2.one;
		toggle_style_on = new GUIStyle(toggle_style_off);
		toggle_style_on.normal = toggle_style_on.active;

		popup_style_invisible = new GUIStyle(EditorStyles.popup);
		popup_style_invisible.normal.textColor = Color.clear;
		popup_style_invisible.normal.background = null;
		popup_style_invisible.active = popup_style_invisible.normal;
		popup_style_invisible.focused = popup_style_invisible.normal;
		popup_style_invisible.hover = popup_style_invisible.normal;
	}

	GUIStyle toggle_style_on, toggle_style_off;
	bool ToggleButton(Rect rect, string label, bool value, string tooltip=null) {
		gui_content.text = label;
		gui_content.tooltip = tooltip;
		value = GUI.Button(rect, gui_content, (value ? toggle_style_on : toggle_style_off)) ? !value : value;
		gui_content.tooltip = null;
		return value;
	}

	GUIStyle popup_style_invisible;
	System.Enum EnumPopup(Rect rect, string label, System.Enum selected, string tooltip=null) {
		// EditorGUI.EnumPopup(rect, label, enum) and the like are glitchy
		gui_content.text = label;
		gui_content.tooltip = tooltip;
		GUI.Box(addFieldRect, gui_content, EditorStyles.popup);
		selected = EditorGUI.EnumPopup(rect, selected, popup_style_invisible);
		gui_content.tooltip = null;
		return selected;
	}
	#endregion
	
	#region RECT CALCULATIONS
	Rect addFieldRect, selectionRect, wrapWordsRect, showIndentationRect, searchRect, caseRect;
	Rect txtRect, scrRectV, scrRectH, resizerRect, boxRect;
	Rect txtRectL, txtRectR;
	void CalculateRects(Rect mainRect) {
		Rect toolRect = SplitRectH0(ref mainRect, lineHeight);
		addFieldRect = SplitRectW0(ref toolRect, TextWidth(EditorStyles.popup, "Add"));
		SplitRectW0(ref toolRect, 2);
		wrapWordsRect = SplitRectW0(ref toolRect, TextWidth(EditorStyles.miniButton, "\\n"));
		SplitRectW0(ref toolRect, 2);
		showIndentationRect = SplitRectW0(ref toolRect, TextWidth(EditorStyles.miniButton, " | "));
		SplitRectW0(ref toolRect, 2);
		selectionRect = SplitRectW0(ref toolRect, TextWidth(EditorStyles.label, selectionEnd.ToString()));
		SplitRectW0(ref toolRect, 2);
		caseRect = SplitRectW1(ref toolRect, TextWidth(EditorStyles.miniButton, "Cc"));
		SplitRectW1(ref toolRect, 2);
		searchRect = toolRect;

		SplitRectWH(mainRect, out txtRect, out scrRectV, out scrRectH, out resizerRect, lineHeight, lineHeight, true, true);
		boxRect = txtRect;

		txtRectR = txtRect;
		txtRectL = SplitRectW0(ref txtRectR, 12);

		float margin = 2f;
		txtRectR.xMin += 1f;
		txtRectR.yMin += margin;
		txtRectR.xMax -= margin;
		txtRectR.yMax -= margin;
	}
	#endregion
	
	#region TOOLBAR
	bool wrapWords = false;
	bool showIndentation = true;
	string search_text = "";
	bool search_ignore_case = false;

	void UpdateToolbar() {
		AddFieldPopup();

		GUI.Box(selectionRect, selectionEnd.ToString(), EditorStyles.label); // LabelField is glitchy

		wrapWords = ToggleButton(wrapWordsRect, "\\n", wrapWords, "Wrap words");

		showIndentation = ToggleButton(showIndentationRect, " | ", showIndentation, "Show indentation");

		SearchBar();

		search_ignore_case = ToggleButton(caseRect, "Cc", search_ignore_case, "Ignore case");
	}

	public enum AddFieldType {
		Nothing, Object, Curve, Color, Toggle, Percentage
	}
	void AddFieldPopup() {
		var add_field = (AddFieldType)EnumPopup(addFieldRect, "Add", AddFieldType.Nothing, "Add field");
		if (add_field != AddFieldType.Nothing) {
			string tag = null;
			if (add_field == AddFieldType.Object) {
				tag = TextWithFields.TAG_OBJECT;
			} else if (add_field == AddFieldType.Curve) {
				tag = TextWithFields.TAG_CURVE;
			} else if (add_field == AddFieldType.Color) {
				tag = TextWithFields.TAG_COLOR;
			} else if (add_field == AddFieldType.Toggle) {
				tag = TextWithFields.TAG_TOGGLE;
			} else if (add_field == AddFieldType.Percentage) {
				tag = TextWithFields.TAG_PERCENTAGE;
			}
			selectionText = TextWithFields.MakeMeta(tag + "$");
			ensure_caret_visibility = true; //EnsureCaretVisibility();
			Focus();
		}
	}

	void SearchBar() {
		bool do_search = false;
		if (@event.type == EventType.KeyUp) {
			do_search = (@event.keyCode == KeyCode.KeypadEnter) || (@event.keyCode == KeyCode.Return);
		}

		GUI.SetNextControlName(search_control_name);
		search_text = EditorGUI.TextField(searchRect, search_text);

		if ((GUI.GetNameOfFocusedControl() == search_control_name) && do_search) {
			@event.Use();
			FindAndSelect(search_text, search_ignore_case);
			Focus();
		}
	}
	#endregion
	
	#region SCROLLBARS
	Vector2 scrollPos;
	float maxDocWidth, maxDocHeight;
	float hScrollSize { get { return txtRectR.width; } }
	float hScrollMax { get { return Mathf.Max(hScrollSize, maxDocWidth+1); } } // 1 to make caret visible
	float vScrollSize { get { return txtRectR.height; } }
	float vScrollMax { get { return Mathf.Max(vScrollSize, maxDocHeight); } }
	void UpdateScrollbars() {
		GUI.enabled = GUI_enabled && (hScrollSize < hScrollMax);
		scrollPos.x = GUI.HorizontalScrollbar(scrRectH, scrollPos.x, hScrollSize, 0, hScrollMax);
		GUI.enabled = GUI_enabled && (vScrollSize < vScrollMax);
		scrollPos.y = GUI.VerticalScrollbar(scrRectV, scrollPos.y, vScrollSize, 0, vScrollMax);
		GUI.enabled = GUI_enabled;
	}
	void Scroll(float dx, float dy) {
		ScrollTo(scrollPos.x+dx, scrollPos.y+dy);
	}
	void Scroll(Vector2 delta) {
		ScrollTo(scrollPos+delta);
	}
	void ScrollTo(float x, float y) {
		scrollPos.x = Mathf.Clamp(x, 0, hScrollMax-hScrollSize);
		scrollPos.y = Mathf.Clamp(y, 0, vScrollMax-vScrollSize);
	}
	void ScrollTo(Vector2 pos) {
		scrollPos.x = Mathf.Clamp(pos.x, 0, hScrollMax-hScrollSize);
		scrollPos.y = Mathf.Clamp(pos.y, 0, vScrollMax-vScrollSize);
	}
	#endregion
	
	#region RESIZER
	bool resizer_dragging = false;
	Vector2 resizer_start_pos;
	float resizer_start_linecount;
	void UpdateResizer() {
		int resizerControlID = EditorGUIUtility.GetControlID(FocusType.Keyboard);
		EditorGUIUtility.AddCursorRect(resizerRect, MouseCursor.ResizeVertical, resizerControlID);
		
		if (!GUI_enabled) return;
		
		if (resizerRect.Contains(mp)) {
			if (Event.current.type == EventType.mouseDown) {
				resizer_dragging = true;
				resizer_start_pos = mp;
				resizer_start_linecount = DisplayedLinesCount;
			}
		}
		if (resizer_dragging) {
			if (Event.current.type == EventType.mouseDrag) {
				DisplayedLinesCount = resizer_start_linecount + (mp.y - resizer_start_pos.y)/lineHeight;
			}
			if (Event.current.type == EventType.mouseUp) {
				resizer_dragging = false;
			}
		}
	}
	#endregion

	#region UTILITY METHODS
	enum TextPosMode {
		Local, Scrolled, Global
	}
	Vector2 TextPosConvert(float x, float y, TextPosMode mSrc, TextPosMode mDst) {
		if ((mSrc == TextPosMode.Global) && (mDst != TextPosMode.Global)) {
			x += Mathf.Round(scrollPos.x);
			y += Mathf.Round(scrollPos.y);
		} else if ((mSrc == TextPosMode.Local) && (mDst != TextPosMode.Local)) {
			x -= Mathf.Round(scrollPos.x);
			y -= Mathf.Round(scrollPos.y);
		}
		if ((mDst == TextPosMode.Global) && (mSrc != TextPosMode.Global)) {
			x += Mathf.Round(txtRectR.x);
			y += Mathf.Round(txtRectR.y);
		} else if ((mDst == TextPosMode.Local) && (mSrc != TextPosMode.Local)) {
			x -= Mathf.Round(txtRectR.x);
			y -= Mathf.Round(txtRectR.y);
		}
		return new Vector2(x, y);
	}

	float DisplayedLinesCount {
		get {
			float displayedLinesCount = property.FindPropertyRelative("displayedLinesCount").floatValue;
			if (displayedLinesCount <= 0) displayedLinesCount = TextWithFieldsPreferences.DisplayedLines;
			return displayedLinesCount;
		}
		set {
			property.FindPropertyRelative("displayedLinesCount").floatValue = Mathf.Max(value, 0f);
		}
	}

	void Focus() {
		GUI.FocusControl(this_control_name); // ?
		EditorGUIUtility.editingTextField = true; // ?
		//EditorGUIUtility.editingTextField = false;
		//EditorGUIUtility.hotControl = txtControlID; // ?
		EditorGUIUtility.keyboardControl = txtControlID;
		triggerRepaint = true;
	}
	void Unfocus() {
		GUI.FocusControl("");
		EditorGUIUtility.editingTextField = false;
		EditorGUIUtility.hotControl = 0;
		EditorGUIUtility.keyboardControl = 0;
		triggerRepaint = true;
	}

	Vector2 ElementSize(TextWithFields.DisplayElement elem, float x=0f, float y=0f) {
		return ElementSize(elem.guiStyle, elem.text, x, y);
	}
	Vector2 ElementSize(GUIStyle style, string data, float x=0f, float y=0f) {
		float W, H;
		if (!string.IsNullOrEmpty(data) && char.IsWhiteSpace(data, 0)) {
			TextSize(style, data, out W, out H, true);
			if (data[0] == '\t') {
				W = tab_size - (x % tab_size);
				if (W < tab_size_min) W += tab_size;
				W = Mathf.Round(W);
			}
		} else {
			TextSize(style, data, out W, out H);
		}
		return new Vector2(W, H);
	}

	const float fieldWidth_Unknown = 1f; // proportionally to height
	const float fieldWidth_Object = 3f; // proportionally to height
	const float fieldWidth_Curve = 3f; // proportionally to height
	const float fieldWidth_Color = 2f; // proportionally to height
	const float fieldWidth_Toggle = 1f; // proportionally to height
	const float fieldWidth_Percentage = 5f; // proportionally to height
	Vector2 TextSize(GUIStyle style, string s, bool surround=false) {
		float w, h;
		TextSize(style, s, out w, out h, surround);
		return new Vector2(w, h);
	}
	void TextSize(GUIStyle style, string s, out float w, out float h, bool surround=false) {
		if (string.IsNullOrEmpty(s)) {
			gui_content.text = "|";
			Vector2 v = style.CalcSize(gui_content);
			w = 0;
			h = Mathf.Round(v.y);
		} else if (TextWithFields.IsMeta(s)) {
			h = lineHeight;
			if (TextWithFields.IsTag(s, TextWithFields.TAG_OBJECT)) {
				w = fieldWidth_Object * h; // proportional to height
				int storage_id;
				if (int.TryParse(TextWithFields.GetMetaData(s), out storage_id)) {
					var obj = objects_storage[storage_id];
					gui_content.text = (obj != null) ? obj.name : "None";
					w = Mathf.Max(w, field_style.CalcSize(gui_content).x);
				}
			} else if (TextWithFields.IsTag(s, TextWithFields.TAG_CURVE)) {
				w = fieldWidth_Curve * h; // proportional to height
			} else if (TextWithFields.IsTag(s, TextWithFields.TAG_COLOR)) {
				w = fieldWidth_Color * h; // proportional to height
			} else if (TextWithFields.IsTag(s, TextWithFields.TAG_TOGGLE)) {
				w = fieldWidth_Toggle * h; // proportional to height
			} else if (TextWithFields.IsTag(s, TextWithFields.TAG_PERCENTAGE)) {
				w = fieldWidth_Percentage * h; // proportional to height
			} else {
				w = fieldWidth_Unknown * h; // proportional to height
			}
			w = Mathf.Round(w);
			h = Mathf.Round(h);
		} else if (surround) {
			Vector2 v, q;
			gui_content.text = "-"+s+"-";
			v = style.CalcSize(gui_content);
			gui_content.text = "--";
			q = style.CalcSize(gui_content);
			w = Mathf.Round(v.x - q.x);
			h = Mathf.Round(v.y);
		} else {
			gui_content.text = s;
			Vector2 v = style.CalcSize(gui_content);
			w = Mathf.Round(v.x);
			h = Mathf.Round(v.y);
		}
	}
	float TextWidth(GUIStyle style, string s, bool surround=false) {
		// CalcMinMaxWidth is buggy for non-standard font sizes!
		float w, h;
		TextSize(style, s, out w, out h, surround);
		return w;
	}
	float TextHeight(GUIStyle style, string s) {
		float w, h;
		TextSize(style, s, out w, out h, false);
		return h;
	}
	#endregion
	
	#region STRING UTILITIES
	static string[] newline_separators = new string[]{"\r\n", "\n", "\r"};
	
	char input_char1 = '\0';
	string InputCharOrPair() {
		char input_char = Event.current.character;
		if (char.IsHighSurrogate(input_char) || char.IsLowSurrogate(input_char)) {
			if (input_char1 != '\0') {
				string result = SurrogatePairToString(input_char, input_char1);
				input_char1 = '\0';
				return result;
			}
			input_char1 = input_char;
			return null;
		} else {
			input_char1 = '\0';
			return (input_char != '\0') ? char.ToString(input_char) : null;
		}
	}
	
	static string SurrogatePairToString(char a, char b) {
		int utf32 = 0;
		if (char.IsHighSurrogate(a) && char.IsLowSurrogate(b)) {
			utf32 = char.ConvertToUtf32(a, b);
		} else if (char.IsHighSurrogate(b) && char.IsLowSurrogate(a)) {
			utf32 = char.ConvertToUtf32(b, a);
		}
		return (utf32 == 0) ? null : char.ConvertFromUtf32(utf32);
	}

	static string StringMinusCodepoint(string s) {
		if (string.IsNullOrEmpty(s) || TextWithFields.IsMeta(s)) return "";
		// .NET/Mono encodes surrogates as high first, low second
		if ((s.Length > 1) && char.IsLowSurrogate(s[s.Length-1]) &&
		    char.IsHighSurrogate(s[s.Length-2])) return s.Substring(0, s.Length-2);
		return s.Substring(0, s.Length-1);
	}
	#endregion

	#region UTILITY FUNCTIONS (STATIC)
	static int BinarySearch<T,T2>(IList<T> collection, T2 key, System.Func<T2, T, int> Comparer) {
		return BinarySearch(collection, key, Comparer, 0, collection.Count-1);
	}
	static int BinarySearch<T,T2>(IList<T> collection, T2 key, System.Func<T2, T, int> Comparer, int low, int high) {
		while (low <= high) {
			int middle = (high + low) / 2;
			int cmp_result = Comparer(key, collection[middle]);
			if (cmp_result < 0) {
				high = middle - 1;
			} else if (cmp_result > 0) {
				low = middle + 1;
			} else {
				return middle;
			}
		}
		return -1;
	}
	
	static int InsertionSearch<T,T2>(IList<T> collection, T2 key, System.Func<T2, T, int> Comparer) {
		return InsertionSearch(collection, key, Comparer, 0, collection.Count-1);
	}
	static int InsertionSearch<T,T2>(IList<T> collection, T2 key, System.Func<T2, T, int> Comparer, int low, int high) {
		if (high < 0) return 0;
		while (low < high) {
			int middle = (high + low) / 2;
			int cmp_result = Comparer(key, collection[middle]);
			if (cmp_result < 0) {
				high = middle - 1;
			} else if (cmp_result > 0) {
				low = middle + 1;
			} else {
				return middle;
			}
		}
		// check if it goes before or after current item
		return (Comparer(key, collection[low]) > 0) ? (low + 1) : low;
	}
	
	// Adapted from http://forum.unity3d.com/threads/24101-Copy-TextField-or-TextArea-text-to-Clipboard
	static System.Reflection.PropertyInfo systemCopyBufferProperty = null;
	static string ClipboardGet(int mode=0) {
		if (mode == 0) {
			return EditorGUIUtility.systemCopyBuffer;
		} else if (mode == 1) {
			if (systemCopyBufferProperty == null) {
				var bindingFlags = System.Reflection.BindingFlags.Static | System.Reflection.BindingFlags.NonPublic;
				systemCopyBufferProperty = typeof(GUIUtility).GetProperty("systemCopyBuffer", bindingFlags);
			}
			return systemCopyBufferProperty.GetValue(null, null) as string;
		} else {
			TextEditor te = new TextEditor();
			te.Paste();
			return te.content.text;
		}
	}
	static void ClipboardSet(string data, bool even_empty=true, int mode=0) {
		if (!even_empty && string.IsNullOrEmpty(data)) return;
		if (mode == 0) {
			EditorGUIUtility.systemCopyBuffer = data;
		} else if (mode == 1) {
			if (systemCopyBufferProperty == null) {
				var bindingFlags = System.Reflection.BindingFlags.Static | System.Reflection.BindingFlags.NonPublic;
				systemCopyBufferProperty = typeof(GUIUtility).GetProperty("systemCopyBuffer", bindingFlags);
			}
			systemCopyBufferProperty.SetValue(null, data, null);
		} else {
			TextEditor te = new TextEditor();
			te.content = new GUIContent(data);
			te.SelectAll();
			te.Copy();
		}
	}

	static void SplitRectWH(Rect src, out Rect r00, out Rect r10, out Rect r01, out Rect r11, float w, float h, bool wEnd=false, bool hEnd=false) {
		if (wEnd) w = src.width - w;
		if (hEnd) h = src.height - h;
		float _w = src.width - w;
		float _h = src.height - h;
		r00 = new Rect(src.x, src.y, w, h);
		r10 = new Rect(src.x+w, src.y, _w, h);
		r01 = new Rect(src.x, src.y+h, w, _h);
		r11 = new Rect(src.x+w, src.y+h, _w, _h);
	}
	static Rect SplitRectW0(ref Rect src, float w, bool wEnd=false) {
		if (wEnd) w = src.width - w;
		Rect result = new Rect(src.x, src.y, w, src.height);
		src.width -= w;
		src.x += w;
		return result;
	}
	static Rect SplitRectW1(ref Rect src, float w, bool wEnd=false) {
		if (wEnd) w = src.width - w;
		src.width -= w;
		Rect result = new Rect(src.x+src.width, src.y, w, src.height);
		return result;
	}
	
	static Rect SplitRectH0(ref Rect src, float h, bool hEnd=false) {
		if (hEnd) h = src.height - h;
		Rect result = new Rect(src.x, src.y, src.width, h);
		src.height -= h;
		src.y += h;
		return result;
	}
	static Rect SplitRectH1(ref Rect src, float h, bool hEnd=false) {
		if (hEnd) h = src.height - h;
		src.height -= h;
		Rect result = new Rect(src.x, src.y+src.height, src.width, h);
		return result;
	}
	
	static T GetDelegate<T>(object obj, string name, bool case_sensitive=false) where T : class {
		var type = obj.GetType();
		
		var bindflags = System.Reflection.BindingFlags.NonPublic|System.Reflection.BindingFlags.Public|
			System.Reflection.BindingFlags.Static|System.Reflection.BindingFlags.Instance;
		if (!case_sensitive) bindflags |= System.Reflection.BindingFlags.IgnoreCase;
		
		T method = null;
		var method_info = type.GetMethod(name, bindflags);
		if (method_info != null) {
			try {
				if (method_info.IsStatic) {
					method = System.Delegate.CreateDelegate(typeof(T), method_info) as T;
				} else {
					method = System.Delegate.CreateDelegate(typeof(T), obj, method_info) as T;
				}
			} catch (System.ArgumentException) {
				// when method arguments are incompatible
			}
		}
		return method;
	}
	#endregion
}

public class TextWithFieldsPreferences {
	// Have we loaded the prefs yet
	static bool prefsLoaded = false;
	
	// The Preferences
	static float displayedLines = 0;
	public static float DisplayedLines {
		get { return EditorPrefs.GetFloat("TextWithFields:displayedLines", 10); }
	}
	
	static float fontSize = 0; // default font is 13, singleLineHeight is 19?
	public static float FontSize {
		get { return EditorPrefs.GetFloat("TextWithFields:fontSize", 0.7f); }
	}
	
	static float tabSize = 0;
	public static float TabSize {
		get { return EditorPrefs.GetFloat("TextWithFields:tabSize", 6); }
	}
	
	static float sameUndoTime = 0;
	public static float SameUndoTime {
		get { return EditorPrefs.GetFloat("TextWithFields:sameUndoTime", 0.6f); }
	}
	
	// Add preferences section named "TextWithFields" to the Preferences Window
	[PreferenceItem("TextWithFields")]
	static void PreferencesGUI() {
		// Load the preferences
		if (!prefsLoaded) {
			displayedLines = DisplayedLines;
			fontSize = FontSize;
			tabSize = TabSize;
			sameUndoTime = SameUndoTime;
			prefsLoaded = true;
		}
		
		// Preferences GUI
		displayedLines = EditorGUILayout.FloatField("Displayed Lines", displayedLines);
		fontSize = EditorGUILayout.FloatField("Font Size", fontSize);
		tabSize = EditorGUILayout.FloatField("Tab Size", tabSize);
		sameUndoTime = EditorGUILayout.FloatField("Same Undo Time", sameUndoTime);

		// Save the preferences
		if (GUI.changed) {
			EditorPrefs.SetFloat("TextWithFields:displayedLines", displayedLines);
			EditorPrefs.SetFloat("TextWithFields:fontSize", fontSize);
			EditorPrefs.SetFloat("TextWithFields:tabSize", tabSize);
			EditorPrefs.SetFloat("TextWithFields:sameUndoTime", sameUndoTime);
		}
	}
}

class SerializedArrayOfString : IList<string> {
	SerializedProperty _lines;
	
	public SerializedArrayOfString(SerializedProperty lines) {
		_lines = lines;
	}
	
	#region Implementation of IEnumerable
	public IEnumerator<string> GetEnumerator() {
		int n = _lines.arraySize;
		for (int i = 0; i < n; i++) {
			yield return _lines.GetArrayElementAtIndex(i).stringValue;
		}
	}
	IEnumerator IEnumerable.GetEnumerator() {
		return GetEnumerator();
	}
	#endregion
	
	#region Implementation of ICollection<T>
	public void Add(string item) {
		Insert(Count, item);
	}
	public void Clear() {
		_lines.ClearArray();
	}
	public bool Contains(string item) {
		return IndexOf(item) != -1;
	}
	public void CopyTo(string[] array, int arrayIndex) {
		if (array == null) throw new System.ArgumentNullException();
		if (arrayIndex < 0) throw new System.ArgumentOutOfRangeException();
		int n = _lines.arraySize;
		if (array.Length < arrayIndex+n) throw new System.ArgumentException();
		for (int i = 0; i < n; i++) {
			array[arrayIndex+i] = _lines.GetArrayElementAtIndex(i).stringValue;
		}
	}
	public bool Remove(string item) {
		int i = IndexOf(item);
		if (i != -1) RemoveAt(i);
		return (i != -1);
	}
	public int Count {
		get { return _lines.arraySize; }
	}
	public bool IsReadOnly {
		get { return false; }
	}
	#endregion
	
	#region Implementation of IList<T>
	public int IndexOf(string item) {
		int n = _lines.arraySize;
		for (int i = 0; i < n; i++) {
			if (item == _lines.GetArrayElementAtIndex(i).stringValue) return i;
		}
		return -1;
	}
	public void Insert(int index, string item) {
		if ((index < 0) || (index > _lines.arraySize)) throw new System.ArgumentOutOfRangeException();
		_lines.InsertArrayElementAtIndex(index);
		_lines.GetArrayElementAtIndex(index).stringValue = item;
	}
	public void RemoveAt(int index) {
		if ((index < 0) || (index >= _lines.arraySize)) throw new System.ArgumentOutOfRangeException();
		_lines.DeleteArrayElementAtIndex(index);
	}
	public string this[int index] {
		get {
			if (_lines.arraySize == 0) _lines.InsertArrayElementAtIndex(0);
			if ((index < 0) || (index >= _lines.arraySize)) throw new System.ArgumentOutOfRangeException();
			return _lines.GetArrayElementAtIndex(index).stringValue;
		}
		set {
			if (_lines.arraySize == 0) _lines.InsertArrayElementAtIndex(0);
			if ((index < 0) || (index >= _lines.arraySize)) throw new System.ArgumentOutOfRangeException();
			_lines.GetArrayElementAtIndex(index).stringValue = value;
		}
	}
	#endregion
	
	public void Move(int srcIndex, int dstIndex) {
		if ((srcIndex < 0) || (srcIndex >= _lines.arraySize)) throw new System.ArgumentOutOfRangeException();
		if ((dstIndex < 0) || (dstIndex >= _lines.arraySize)) throw new System.ArgumentOutOfRangeException();
		_lines.MoveArrayElement(srcIndex, dstIndex);
	}
}
