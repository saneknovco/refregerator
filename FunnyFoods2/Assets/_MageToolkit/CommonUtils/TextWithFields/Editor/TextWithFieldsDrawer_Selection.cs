﻿using UnityEditor;
using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.Globalization;
//using Bini.Utils;

partial class TextWithFieldsDrawer : PropertyDrawer {
	#region CARET / SELECTION
	struct CaretPosition {
		public int iL, iE;
		public CaretPosition(CaretPosition cp) {
			iL = cp.iL; iE = cp.iE;
		}
		public CaretPosition(int iL, int iE) {
			this.iL = iL; this.iE = iE;
		}
		public override string ToString() {
			return string.Format("L:{0}, C:{1}", iL+1, iE+1);
		}
		public bool Equals(CaretPosition other) {
			return (other.iL == iL) && (other.iE == iE);
		}
		public override int GetHashCode() {
			return (iL << 5) + iL ^ iE; // default .NET formula (?)
		}
		public override bool Equals(object other) {
			var casted = other as CaretPosition?;
			return (casted == null) ? false : Equals(casted.Value);
		}
		public static bool operator ==(CaretPosition a, CaretPosition b) {
			return (a.iL == b.iL) && (a.iE == b.iE);
		}
		public static bool operator !=(CaretPosition a, CaretPosition b) {
			return (a.iL != b.iL) || (a.iE != b.iE);
		}
		public static bool operator <(CaretPosition a, CaretPosition b) {
			return (a.iL == b.iL) ? (a.iE < b.iE) : (a.iL < b.iL);
		}
		public static bool operator >(CaretPosition a, CaretPosition b) {
			return (a.iL == b.iL) ? (a.iE > b.iE) : (a.iL > b.iL);
		}
		public static bool operator <=(CaretPosition a, CaretPosition b) {
			return (a.iL == b.iL) ? (a.iE <= b.iE) : (a.iL <= b.iL);
		}
		public static bool operator >=(CaretPosition a, CaretPosition b) {
			return (a.iL == b.iL) ? (a.iE >= b.iE) : (a.iL >= b.iL);
		}
		public static CaretPosition Min(CaretPosition a, CaretPosition b) {
			return (a <= b) ? a : b;
		}
		public static CaretPosition Max(CaretPosition a, CaretPosition b) {
			return (a <= b) ? b : a;
		}
	}

	CaretPosition selectionStart, selectionEnd;

	CaretPosition selectionMin {
		get { return CaretPosition.Min(selectionStart, selectionEnd); }
	}
	CaretPosition selectionMax {
		get { return CaretPosition.Max(selectionStart, selectionEnd); }
	}

	string selectionTextLocal {
		get { return SerializeSelection(false, "\n"); }
		set { ReplaceSelection(value); }
	}
	string selectionText {
		get { return SerializeSelection(); }
		set { ReplaceSelection(value); }
	}

	void FindAndSelect(string text, bool ignoreCase=false, bool forward=true) {
		if (string.IsNullOrEmpty(text)) return;
		
		text = text.Replace("\r\n", "\n").Replace("\r", "\n");
		var pieces = new List<string>(TextWithFields.ParseElements(text));
		
		int nLines = dLines.Count;
		int nMatched = 0, nRequired = pieces.Count, piecesMax = pieces.Count-1;

		var p0 = (forward ? selectionMax : AdvanceCaret(selectionMin, false, false));
		var pMin = p0;
		var pMax = pMin;
		
		var p = p0;
		bool first_time = true;
		while ((p != p0) || first_time) {
			string elem_txt = ElementText(p, true);
			string to_match = (forward ? pieces[nMatched] : pieces[piecesMax-nMatched]);
			
			if (string.Compare(elem_txt, to_match, ignoreCase) == 0) {
				if (nMatched == 0) {
					pMin = p;
					if (!forward) AdvanceCaret(ref pMin, true, true);
				}

				nMatched++;

				if (nMatched == nRequired) {
					pMax = p;
					if (forward) AdvanceCaret(ref pMax, true, true);
					break;
				}
			} else {
				nMatched = 0;
			}
			
			first_time = false;
			
			// Advance with wrap-around; if wrapped, reset match count
			if (AdvanceCaret(ref p, forward, false)) nMatched = 0;
		}
		
		if (nMatched == nRequired) {
			SetSelectionEnd(pMin, true, true);
			SetSelectionEnd(pMax, false, true);
			EnsureUnfoldedAtSelection();
		}
	}

	string ElementText(CaretPosition p, bool forward=true) {
		if (!forward) {
			if (p.iE > 0) {
				p.iE--;
			} else if (p.iL > 0) {
				p.iL--;
				p.iE = dLines[p.iL].elements.Count;
			} else {
				return "";
			}
		}
		var dLine = dLines[p.iL];
		return (p.iE < dLine.elements.Count) ? dLine.elements[p.iE].text : "\n";
	}

	CaretPosition AdvanceCaret(CaretPosition p, bool forward, bool stopAtEnds=true) {
		AdvanceCaret(ref p, forward, stopAtEnds);
		return p;
	}
	bool AdvanceCaret(ref CaretPosition p, bool forward, bool stopAtEnds) {
		bool wrapped_around = false;
		if (forward) {
			p.iE++;
			if (p.iE > dLines[p.iL].elements.Count) {
				if (stopAtEnds) {
					if (p.iL < dLines.Count-1) {
						p.iL++;
						p.iE = 0;
					} else {
						p.iE = dLines[p.iL].elements.Count;
					}
				} else {
					p.iL++;
					if (p.iL >= dLines.Count) {
						p.iL = 0;
						wrapped_around = true;
					}
					p.iE = 0;
				}
			}
		} else {
			p.iE--;
			if (p.iE < 0) {
				if (stopAtEnds) {
					if (p.iL > 0) {
						p.iL--;
						p.iE = dLines[p.iL].elements.Count;
					} else {
						p.iE = 0;
					}
				} else {
					p.iL--;
					if (p.iL < 0) {
						p.iL = dLines.Count-1;
						wrapped_around = true;
					}
					p.iE = dLines[p.iL].elements.Count;
				}
			}
		}
		return wrapped_around;
	}

	IEnumerable<TextWithFields.DisplayElement> IterateElements(CaretPosition p0, CaretPosition p1) {
		if (p0 == p1) yield break;
		bool forward = (p1 > p0);
		var p = p0;
		if (forward) {
			while (p != p1) {
				var dLine = dLines[p.iL];
				if (p.iE < dLine.elements.Count) yield return dLine.elements[p.iE];
				if (AdvanceCaret(ref p, true, false)) break;
			}
		} else {
			while (p != p1) {
				if (AdvanceCaret(ref p, false, false)) break;
				var dLine = dLines[p.iL];
				if (p.iE < dLine.elements.Count) yield return dLine.elements[p.iE];
			}
		}
	}

	bool ensure_caret_visibility = false;
	Rect last_caret_rect = new Rect();
	void SetSelectionEnd(CaretPosition p, bool move_start=true, bool autoscroll=true) {
		selectionEnd = p;
		last_caret_rect = CaretRect(p);
		if (move_start) selectionStart = selectionEnd;
		if (autoscroll) ensure_caret_visibility = true; //EnsureCaretVisibility();
	}
	void MoveSelectionEnd(CaretMoves cm, bool move_start=true, bool autoscroll=true) {
		selectionEnd = MoveCaret(selectionEnd, cm, ref last_caret_rect);
		if (move_start) selectionStart = selectionEnd;
		if (autoscroll) ensure_caret_visibility = true; //EnsureCaretVisibility();
	}
	void EnsureCaretVisibility() {
		//ensure_caret_visibility = true; // make sure it will get updated after re-layout
		float xMin = last_caret_rect.xMin;
		float yMin = last_caret_rect.yMin;
		float xMax = last_caret_rect.xMax;
		float yMax = last_caret_rect.yMax;
		float x = scrollPos.x, y = scrollPos.y;
		float w = txtRectR.width, h = txtRectR.height;
		if (x > xMin) x = xMin;
		if (x+w < xMax) x = Mathf.Max(0, xMax-w);
		if (y > yMin) y = yMin;
		if (y+h < yMax) y = Mathf.Max(0, yMax-h);
		scrollPos = new Vector2(x, y);
	}
	void EnsureUnfoldedAtSelection() {
		UnfoldRange(selectionMin.iL, selectionMax.iL);
	}

	Rect CaretRect(CaretPosition p, TextPosMode pos_mode=TextPosMode.Local, bool useElementHeight=true) {
		var dLine = dLines[p.iL];
		Rect result = new Rect(dLine.rect.x, dLine.rect.y, 1, 0);

		if (dLine.elements.Count != 0) {
			TextWithFields.DisplayElement elem;
			if (p.iE == 0) {
				elem = dLine.elements[0];
				result.x += elem.rect.x;
			} else {
				elem = dLine.elements[Mathf.Min(p.iE, dLine.elements.Count)-1];
				result.x += elem.rect.x + elem.rect.width;
			}
			if (useElementHeight) {
				result.y += elem.rect.y;
				result.height = elem.rect.height;
			} else {
				result.y += elem.yMinL;
				result.height = elem.yMaxL - elem.yMinL;
			}
		} else {
			result.height = dLine.rect.height;
		}
		
		var xy = TextPosConvert(result.x, result.y, TextPosMode.Local, pos_mode);
		result.x = xy.x; result.y = xy.y;
		
		if ((pos_mode == TextPosMode.Global) && (p.iE > dLine.elements.Count)) {
			result.xMax = txtRectR.xMax;
		}
		
		return result;
	}
	int NearesNonFoldedLine(int iL, bool forward, bool search_back=true) {
		int iL_save = iL;
		if (forward) {
			for (; iL < dLines.Count; iL++) {
				if (!dLines[iL].is_folded) return iL;
			}
			if (search_back) {
				iL = iL_save; // nothing found; try in other direction
				for (; iL >= 0; iL--) {
					if (!dLines[iL].is_folded) return iL;
				}
			}
		} else {
			for (; iL >= 0; iL--) {
				if (!dLines[iL].is_folded) return iL;
			}
			if (search_back) {
				iL = iL_save; // nothing found; try in other direction
				for (; iL < dLines.Count; iL++) {
					if (!dLines[iL].is_folded) return iL;
				}
			}
		}
		return iL_save;
	}
	CaretPosition NearestCaretPos(Vector2 v, TextPosMode pos_mode=TextPosMode.Local, bool slide=false, bool use_center=true) {
		int iLMin = NearesNonFoldedLine(0, true);
		int iLMax = NearesNonFoldedLine(dLines.Count-1, false);

		v = TextPosConvert(v.x, v.y, pos_mode, TextPosMode.Local);

		float yMin = dLines[iLMin].rect.yMin;
		float yMax = dLines[iLMax].rect.yMax;

		if (slide) {
			if (v.y < yMin) return new CaretPosition(iLMin, 0);
			if (v.y >= yMax) return new CaretPosition(iLMax, dLines[iLMax].elements.Count);
		} else {
			v.y = Mathf.Clamp(v.y, yMin, yMax);
		}

		int iL, iE;

		iL = BinarySearch(dLines, v.y, (pos, _dLine) => {
			if (pos < _dLine.rect.y) return -1;
			if ((pos >= _dLine.rect.yMax) || _dLine.is_folded) return 1;
			return 0;
		}, iLMin, iLMax);

		if (iL == -1) {
			iL = (Mathf.Abs(v.y-yMin) < Mathf.Abs(v.y-yMax)) ? iLMin : iLMax;
		}

		var dLine = dLines[iL];

		v.y -= dLine.rect.y;

		if (use_center) {
			iE = InsertionSearch(dLine.elements, v, (pos, elem) => {
				if (pos.y < elem.yMinL) return -1;
				if (pos.y >= elem.yMaxL) return 1;
				return System.Math.Sign(pos.x - elem.rect.center.x);
			});
		} else {
			iE = InsertionSearch(dLine.elements, v, (pos, elem) => {
				if (pos.y < elem.yMinL) return -1;
				if (pos.y >= elem.yMaxL) return 1;
				return System.Math.Sign(pos.x - elem.rect.xMax);
			});
		}

		iE = Mathf.Clamp(iE, 0, dLine.elements.Count);
		
		return new CaretPosition(iL, iE);
	}
	CaretPosition MoveCaretY(CaretPosition p, ref Rect r, float dy, int side, bool keep_scroll=false) {
		float y_offset = scrollPos.y - r.y;
		var _r = CaretRect(p, TextPosMode.Local, false);
		float y = (side < 0) ? _r.yMin : _r.yMax;
		var v = new Vector2(r.x, y+dy);
		p = NearestCaretPos(v, TextPosMode.Local, true);
		r = CaretRect(p);
		r.x = v.x;
		if (keep_scroll) scrollPos.y = y_offset + r.y;
		return p;
	}
	
	enum CaretMoves {
		LineStart, LineEnd,
		DocStart, DocEnd,
		WordStart, WordEnd,
		SymbolPrev, SymbolNext,
		LineUp, LineDown,
		PageUp, PageDown,
	}
	bool useWordL=true, useWordR=false;
	CaretPosition MoveCaret(CaretPosition p, CaretMoves cm, ref Rect r) {
		switch (cm) {
		case CaretMoves.LineStart:
			p.iE = 0;
			r = CaretRect(p);
			break;
		case CaretMoves.LineEnd:
			p.iE = dLines[p.iL].elements.Count;
			r = CaretRect(p);
			break;
		case CaretMoves.DocStart:
			p.iL = 0;
			p.iE = 0;
			r = CaretRect(p);
			break;
		case CaretMoves.DocEnd:
			p.iL = dLines.Count-1;
			p.iE = dLines[p.iL].elements.Count;
			r = CaretRect(p);
			break;
		case CaretMoves.WordStart:
			if (p.iE > 0) {
				var elements = dLines[p.iL].elements;
				while (p.iE != 0) {
					p.iE -= 1;
					if (IsAtWordBoundary(p, useWordL, useWordR, elements)) break;
				}
			} else if (p.iL > 0) {
				p.iL = NearesNonFoldedLine(p.iL-1, false);
				p.iE = dLines[p.iL].elements.Count;
			}
			r = CaretRect(p);
			break;
		case CaretMoves.WordEnd:
			if (p.iE < dLines[p.iL].elements.Count) {
				var elements = dLines[p.iL].elements;
				while (p.iE != elements.Count) {
					p.iE += 1;
					if (IsAtWordBoundary(p, useWordL, useWordR, elements)) break;
				}
			} else if (p.iL < (dLines.Count-1)) {
				p.iL = NearesNonFoldedLine(p.iL+1, true);
				p.iE = 0;
			}
			r = CaretRect(p);
			break;
		case CaretMoves.SymbolPrev:
			if (p.iE > 0) {
				p.iE -= 1;
			} else if (p.iL > 0) {
				p.iL = NearesNonFoldedLine(p.iL-1, false);
				p.iE = dLines[p.iL].elements.Count;
			}
			r = CaretRect(p);
			break;
		case CaretMoves.SymbolNext:
			if (p.iE < dLines[p.iL].elements.Count) {
				p.iE += 1;
			} else if (p.iL < (dLines.Count-1)) {
				p.iL = NearesNonFoldedLine(p.iL+1, true);
				p.iE = 0;
			}
			r = CaretRect(p);
			break;
		case CaretMoves.LineUp:
			p = MoveCaretY(p, ref r, -1, -1);
			break;
		case CaretMoves.LineDown:
			p = MoveCaretY(p, ref r, 1, 1);
			break;
		case CaretMoves.PageUp:
			p = MoveCaretY(p, ref r, -txtRectR.height, -1, true);
			break;
		case CaretMoves.PageDown:
			p = MoveCaretY(p, ref r, txtRectR.height, 1, true);
			break;
		}
		return p;
	}
	bool IsAtWordBoundary(CaretPosition p, bool useL, bool useR, List<TextWithFields.DisplayElement> elements=null) {
		if (elements == null) elements = dLines[p.iL].elements;
		if (p.iE <= 0) {
			return true;
		} else if (p.iE >= elements.Count) {
			return true;
		} else {
			var ecL = ElementCategory(elements[p.iE-1]);
			var ecR = ElementCategory(elements[p.iE]);
			bool bL = (ecR != 0) && (ecL != ecR);
			bool bR = (ecL != 0) && (ecL != ecR);
			return (bL && useL) || (bR && useR);
		}
	}
	int ElementCategory(TextWithFields.DisplayElement elem) {
		var c = elem.text[0];
		if (char.IsLetterOrDigit(c)) return 2;
		if (char.IsWhiteSpace(c) || char.IsSeparator(c)) return 0;
		if (c == TextWithFields.META_START) return 3;
		return 1;
	}
	TextWithFields.DisplayElement Element_Left(CaretPosition p) {
		var elements = dLines[p.iL].elements;
		if (p.iE <= 0) {
			return default(TextWithFields.DisplayElement);
		} else {
			return elements[p.iE-1];
		}
	}
	TextWithFields.DisplayElement Element_Right(CaretPosition p) {
		var elements = dLines[p.iL].elements;
		if (p.iE >= elements.Count) {
			return default(TextWithFields.DisplayElement);
		} else {
			return elements[p.iE];
		}
	}
	#endregion
}
