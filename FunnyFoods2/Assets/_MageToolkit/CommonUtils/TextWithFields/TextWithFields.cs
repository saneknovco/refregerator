﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.Globalization;
//using Bini.Utils;

[System.Serializable]
public class TextWithFields {
	[SerializeField] float displayedLinesCount;
	[SerializeField] public string[] lines;

	// To be able to restore cursor position after undo/redo
	[SerializeField] int caret_L;
	[SerializeField] int caret_E;

	[SerializeField] public UnityEngine.Object[] objects;
	[SerializeField] public CurveContainer[] curves;
	[SerializeField] public Color[] colors;
	[SerializeField] public bool[] toggles;
	[SerializeField] public float[] percentages;

	// Unity does not directly serialize arrays/lists of AnimationCurves
	[System.Serializable]
	public class CurveContainer {
		[SerializeField] public AnimationCurve curve;
		public CurveContainer(AnimationCurve curve=null) {
			this.curve = (curve != null) ? curve : new AnimationCurve();
		}
	}

	// Text style can have all sorts of properties (bold, italic, underline type,
	//   crossed out, underscript, superscript, etc.; interval, kerning, etc.;
	//   font, size, color, background color, etc.)
	// Links to objects (or hyperlinks in general) are independent from element
	//   visualization style, actually.

	// DisplayElement adds 9*4=36 bytes overhead per element, but since
	// editable documents aren't expected to be too big, it's acceptable.
	
	// Represents a "grapheme cluster" -- a field or a
	//   stylized group of unicode characters that is
	//   always selected as a whole.
	// REQUIREMENT: it should be possible to reconstruct line's
	//   serialized form from just the DisplayLine.
	// Thus state object needs to carry all the meta-information
	//   (font style, other style information, hyperlink/object/field).
	// This means we explicitly don't support elements that can't
	//   be represented as a formatted text (e.g. tables).
	public struct DisplayElement {
		public GUIStyle guiStyle;
		public string text;
		public Rect rect;
		public object state;

		public int yMinL, yMaxL;
		public Rect rectL {
			get { return Rect.MinMaxRect(rect.xMin, yMinL, rect.xMax, yMaxL); }
		}

		public override string ToString() {
			return text;
		}

		public bool IsMeta {
			get { return (text[0] == META_START) && (text[text.Length-1] == META_END); }
		}
	}

	// Lines of "plain text" are stored, and any meta information is recorded in
	// "escaped" blocks (e.g. ETX ... STX) (so we can avoid the necessity to
	// html-(un)escape things). Each display element needs to store plain text
	// string anyway -- it's used for text rendering.
	// Fold state is stored in line (not in any character, since line can be empty)
	public class DisplayLine {
		public List<DisplayElement> elements;
		public Rect rect;

		public GUIStyle zero_style;
		public object zero_state;

		public bool can_fold;
		public int fold_level;
		public int fold_count;
		public bool is_folded {
			get { return ((int)rect.height) <= 0; }
		}

		public int iL, iE;
		public int indentation;
		public bool isAllWhitespace;

		public string indentation_text {
			get {
				var str_builder = new System.Text.StringBuilder();
				for (int _iE = 0; _iE < indentation; _iE++) {
					str_builder.Append(elements[_iE].text);
				}
				return str_builder.ToString();
			}
		}
	}

	// A change in one line may affect other lines, even the ones
	// earlier in the document (e.g. #endregion makes #region foldable)

	// A line is always re-formatted (style, layout) from scratch.
	// Re-format on any modification of contents (text, formatting, fields)
	// or if window changes width and word wrap is on.
	public class SerializerState {
		public DisplayLine dLine;
		public string line;
		public GUIStyle base_style;
		public Dictionary<object, GUIStyle> styles;
		public object state;

		public int iL0, iE0; // affected range start
		public int iL1, iE1; // affected range end
		public IList<string> lines;
		public IList<DisplayLine> dLines;

		public bool for_clipboard = false;
		public string newline = null;

		public string result;

		public SerializerState() {
			styles = new Dictionary<object, GUIStyle>();
		}
	}
	
	public const string TAG_FOLD = "#fold=";
	public const string TAG_OBJECT = "&object=";
	public const string TAG_CURVE = "&curve=";
	public const string TAG_COLOR = "&color=";
	public const string TAG_TOGGLE = "&toggle=";
	public const string TAG_PERCENTAGE = "&percentage=";
	
	public const char META_START = (char)3; // ETX (end of text)
	public const char META_END = (char)2; // STX (start of text)

	public static bool IsMeta(string text) {
		//if (string.IsNullOrEmpty(text)) return false; // should never happen in practice
		return (text[0] == META_START) && (text[text.Length-1] == META_END);
	}

	public static string MakeMeta(string text) {
		return META_START + text + META_END;
	}
	
	public static bool IsTag(string text, string tag) { // not expected to be empty
		if ((text[0] != META_START) || (text[text.Length-1] != META_END)) return false;
		return string.Compare(text, 1, tag, 0, tag.Length, System.StringComparison.InvariantCulture) == 0;
	}
	
	public static string TagAndData(ref string s) { // not expected to be empty
		int i_sep = s.IndexOf('=', 1);
		if (i_sep == -1) return null;
		string tag = s.Substring(1, i_sep);
		s = s.Substring(i_sep+1, (s.Length-1) - (i_sep+1));
		return tag;
	}

	public static string GetMetaData(string s) { // not expected to be empty
		int i_sep = s.IndexOf('=', 1);
		if (i_sep == -1) return null;
		return s.Substring(i_sep+1, (s.Length-1) - (i_sep+1));
	}

	public static IEnumerable<string> ParseElements(string line) {
		if (string.IsNullOrEmpty(line)) yield break;
		
		var indices = StringInfo.ParseCombiningCharacters(line);
		
		int indices_cnt = indices.Length;
		int iMax = indices_cnt-1;
		for (int i = 0; i < indices_cnt; i++) {
			int idx0 = indices[i];
			int idx1 = (i != iMax) ? indices[i+1] : indices_cnt;
			char c = line[idx0];
			
			if (c == META_START) {
				int meta_length = idx1-idx0;

				for (int j = i+1; j < indices_cnt; j++) {
					int _idx0 = indices[j];
					int _idx1 = (j != iMax) ? indices[j+1] : indices_cnt;
					char _c = line[_idx0];
					
					if (_c == META_END) {
						meta_length = _idx1-idx0;
						i = j;
					} else if (_c == META_START) {
						break;
					}
				}
				
				yield return line.Substring(idx0, meta_length);
			} else {
				yield return line.Substring(idx0, idx1-idx0);
			}
		}
	}
}
