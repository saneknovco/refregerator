#define DISABLE_DEBUG_GUI

using UnityEngine;
using System.Collections.Generic;
using Bini.Utils;

// Callbacks for other scripts to display extra information?

// Main/settings window:
// * GUI scale regulator
// * pause/unpause game
// * open logs
// * open hierarchy
// * open object's components
// * open scenarios

// Logs:
// * toolbar: clear (clear all?)
// * toolbar: filter by log type
// * click on log -> open info window with full trace
// Scroll to the last log when it's added?
// - filter by scenario object?
// - filter by scenario thread

// Hierarchy:
// * tree-like structure
// * clicking on fold icon folds/unfolds
// * clicking on the object opens the components mode/window
//   (if it's a mode, then "back" button is necessary)

// Components:
// * tree-like structure:
// ** main level: components, gameobject properties
// ** sub-level: component's properties
// * clicking on fold icon folds/unfolds
// * clicking on the component enables/disables?

// Scenario:
// * tree-like structure
// ** scenarios -- act as togglable filter for logs?
// *** threads -- act as togglable filter for logs?
// ** str vars, obj vars

[MonoBehaviourSingleton(OnlyActive=false, AutoCreate=true)]
public class DebugGUI : MonoBehaviourSingleton<DebugGUI> {
	#region LOG
	public struct Log {
		public string logString;
		public string stackTrace;
		public LogType logType;
		public string tag;

		public static Color32 color_error = Color.red;
		public static Color32 color_warning = Color.yellow;
		public static Color32 color_log = Color.white;
		public Color32 color {
			get {
				switch (logType) {
				case LogType.Exception:
				case LogType.Error:
				case LogType.Assert:
					return color_error;
				case LogType.Warning:
					return color_warning;
				default:
					return color_log;
				}
			}
		}

		public bool isError {
			get { return (logType == LogType.Exception) |
					(logType == LogType.Error) |
					(logType == LogType.Assert); }
		}
		public bool isWarning {
			get { return (logType == LogType.Warning); }
		}
		public bool isLog {
			get { return (logType == LogType.Log); }
		}

		public Log(string logString, string stackTrace, LogType logType, string tag=null) {
			this.logString = logString;
			this.stackTrace = stackTrace;
			this.logType = logType;
			this.tag = tag;
		}
	}

	public static string LogTag = null;
	public static LinkedList<Log> logs = new LinkedList<Log>();
	public static bool LoggingEnabled = true;

	static void HandleLog(string logString, string stackTrace, LogType logType) {
		if (LoggingEnabled) {
			logs.AddLast(new Log(logString, stackTrace, logType, LogTag));
		}
		LogTag = null;
	}

	public static void NinjaLog(object message, Object context=null) {
		bool prev_enabled = LoggingEnabled;
		LoggingEnabled = false;
		if (Debug.isDebugBuild) Debug.Log(message, context);
//#if UNITY_DEBUG
//		var _settings = AppSettings.Instance.CP;
//		if (Debug.isDebugBuild && _settings.isTrace) Debug.Log((@"DLCLoader: MakeRequest: " + uri).AddTime());
//#endif
		LoggingEnabled = prev_enabled;
	}
	#endregion

	public bool ShowGUI = true;
	public bool StartHidden = true;
	public bool CenterOnShow = true;

	float lastTimeClick = 0f;
	public float clickTimeWindow = 1f;
	public float thresholdNear = 20f;
	public float thresholdFar = 40f;
	List<Vector2> clicks = new List<Vector2>();
	
	static float gui_scale_factor = 0f;
	public static float GUIScaleFactor {
		get {
			gui_scale_factor = Mathf.Clamp(gui_scale_factor, -1f, 1f);
			float default_size = 20f;
			float scl = (PlatformUtils.PixelsPerCm * 0.5f)/default_size;
			scl *= Mathf.Pow(2f, gui_scale_factor);
			return scl;
		}
	}

	static float fps = 0f;
	static float fps_frames = 0f;
	static float fps_last_update = 0f;
	static float fps_update_delay = 0.1f;

	void OnEnable() {
		InitWindows();
#if UNITY_5
		Application.logMessageReceived += HandleLog;
#else
		Application.RegisterLogCallback(HandleLog);
#endif
		
	}
	
	void OnDisable() {
#if UNITY_5
		Application.logMessageReceived -= HandleLog;		
#else
		Application.RegisterLogCallback(null);
#endif
	}

	void Update() {
		float current_time = Time.realtimeSinceStartup;

		fps_frames += 1;
		if (current_time > fps_last_update + fps_update_delay) {
			fps = fps_frames / (current_time - fps_last_update);
			fps_last_update = current_time;
			fps_frames = 0;
		}

		Vector2 mouse = new Vector2(Input.mousePosition.x, Input.mousePosition.y);
		if (Input.GetMouseButtonDown(0)) {
			if (Mathf.Abs(current_time - lastTimeClick) > clickTimeWindow) {
				clicks.Clear();
			}

			clicks.Add(mouse);
			if (clicks.Count > 3) {
				clicks.RemoveRange(0, clicks.Count-3);
			}
			if (clicks.Count == 3) {
				// "Cheat code" to show debug GUI on mobile platforms
				// Click somewhere, then somewhere far enough,
				// then somewhere close enough to the first point
				var d0 = clicks[1] - clicks[0];
				//var d1 = clicks[2] - clicks[1]; // not used
				var d2 = clicks[0] - clicks[2];
				if (d0.magnitude > thresholdFar) {
					if (d2.magnitude < thresholdNear) {
						wndMain.Show();
					}
				}
			}

			lastTimeClick = current_time;
		}
	}

#if !DISABLE_DEBUG_GUI
	void OnGUI() {
		if (!ShowGUI) return;

		var prev_matrix = GUI.matrix;
		float scl = GUIScaleFactor;
		GUI.matrix = Matrix4x4.TRS(Vector3.zero, Quaternion.identity, new Vector3(scl, scl, 1));

		GuiWindow.CenterOnShow = CenterOnShow;

		wndMain.Draw();
		wndLogs.Draw();
		wndInfobox.Draw();

		for (int i = 0; i < extraWindows.Count; i++) {
			extraWindows[i].Draw();
		}

		GUI.matrix = prev_matrix;
	}
#endif

	#region WINDOW DEFINITIONS
	public MainDebugWindow wndMain;
	public LogsWindow wndLogs;
	public InfoboxWindow wndInfobox;

	public List<GuiWindow> extraWindows = new List<GuiWindow>();
	public void AddExtraWindow(GuiWindow wnd) {
		extraWindows.Add(wnd);
	}
	public GuiWindow FindExtraWindow(string title) {
		for (int i = 0; i < extraWindows.Count; i++) {
			if (extraWindows[i].title == title) return extraWindows[i];
		}
		return null;
	}
	public void RemoveExtraWindow(string title) {
		var wnd = FindExtraWindow(title);
		if (wnd != null) extraWindows.Remove(wnd);
	}
	public void RemoveExtraWindow(GuiWindow wnd) {
		try {
			extraWindows.Remove(wnd);
		} catch (System.Exception) {
		}
	}

	public void InitWindows(bool force_show=false) {
		if (force_show) {
			gameObject.SetActive(true);
			this.enabled = true;
			ShowGUI = true;
			StartHidden = false;
		}

		if (wndMain == null) {
			wndMain = new MainDebugWindow();
			wndMain.debug_gui = this;
		}
		if (wndLogs == null) {
			wndLogs = new LogsWindow();
			wndLogs.debug_gui = this;
		}
		if (wndInfobox == null) {
			wndInfobox = new InfoboxWindow();
			wndInfobox.debug_gui = this;
		}
		if (!StartHidden) wndMain.Show();
	}
	
	public class MainDebugWindow : GuiWindow {
		public DebugGUI debug_gui;
		static float current_level = 0;
		public MainDebugWindow() : base() {
			title = "Debug";
			windowRect = new Rect(0, 0, 200, 200);
			BodyCallback = Draw_Body;
			current_level = Application.loadedLevel;
		}
		void Draw_Body(GuiWindow wnd) {
			GUILayout.BeginHorizontal();
			if (GUILayout.Button("<")) DebugGUI.gui_scale_factor -= 0.125f;
			if (Mathf.Abs(Time.timeScale) <= float.Epsilon) {
				if (GUILayout.Button("Resume")) Time.timeScale = 1f;
			} else {
				if (GUILayout.Button("Pause")) Time.timeScale = 0f;
			}
			if (GUILayout.Button(">")) DebugGUI.gui_scale_factor += 0.125f;
			GUILayout.EndHorizontal();

			GUILayout.BeginHorizontal();
			DebugGUI.fps_update_delay = GUILayout.HorizontalSlider(DebugGUI.fps_update_delay, 0f, 1f);
			GUILayout.Button(string.Format("{0,6:0.00}", DebugGUI.fps), GUILayout.ExpandWidth(false));
			GUILayout.EndHorizontal();

			if (GUILayout.Button("Logs")) debug_gui.wndLogs.Show();
			GUILayout.Button("Scene");
			GUILayout.Button("Object");

			var extraWindows = debug_gui.extraWindows;
			for (int i = 0; i < extraWindows.Count; i++) {
				var extraWnd = extraWindows[i];
				if (GUILayout.Button(extraWnd.title)) extraWnd.Show();
			}
			
			current_level = GUILayout.HorizontalSlider(current_level, 0, Application.levelCount-1);
			int level_id = Mathf.RoundToInt(current_level);
			if (GUILayout.Button("Load level "+level_id)) Application.LoadLevel(level_id);
		}
	}

	public class LogsWindow : GuiWindow {
		public DebugGUI debug_gui;
		public string filterTag = null;
		static bool hide_Logs = false;
		static bool hide_Warnings = false;
		static bool hide_Errors = false;
		static bool use_tag_filter = true;
		public LogsWindow() : base() {
			title = "Logs";
			windowRect = new Rect(0, 0, 200, 200);
			minWindowSize.x = 128;
			Center();
			BodyCallback = Draw_Body;
			ToolbarCallback = Draw_Toolbar;
		}
		static GUIStyle btn_style = null;
		void InitStyles() {
			if (btn_style == null) {
				btn_style = new GUIStyle(GUI.skin.button);
				btn_style.alignment = TextAnchor.MiddleCenter;
				btn_style.margin = new RectOffset(0, 0, 0, 0);
			}
		}
		void Draw_Toolbar(GuiWindow wnd) {
			InitStyles();
			btn_style.alignment = TextAnchor.MiddleCenter;

			GUI.color = wnd.GUI_color;
			if (GUILayout.Button("Clear", btn_style)) DebugGUI.logs.Clear();
			
			GUI.color = Color.green;
			if (!use_tag_filter) GUI.color = GUI.color.SetA(0.5f);
			use_tag_filter = GUILayout.Toggle(use_tag_filter, "#", btn_style);

			GUI.color = DebugGUI.Log.color_log;
			if (hide_Logs) GUI.color = GUI.color.SetA(0.5f);
			hide_Logs = !GUILayout.Toggle(!hide_Logs, ".", btn_style);
			
			GUI.color = DebugGUI.Log.color_warning;
			if (hide_Warnings) GUI.color = GUI.color.SetA(0.5f);
			hide_Warnings = !GUILayout.Toggle(!hide_Warnings, "?", btn_style);
			
			GUI.color = DebugGUI.Log.color_error;
			if (hide_Errors) GUI.color = GUI.color.SetA(0.5f);
			hide_Errors = !GUILayout.Toggle(!hide_Errors, "!", btn_style);

			GUI.color = wnd.GUI_color;
		}
		void Draw_Body(GuiWindow wnd) {
			InitStyles();
			btn_style.alignment = TextAnchor.MiddleLeft;

			bool filter_by_tag = !string.IsNullOrEmpty(filterTag) && use_tag_filter;
			for (var node = DebugGUI.logs.First; node != null; node = node.Next) {
				var log = node.Value;
				if (hide_Logs && log.isLog) continue;
				if (hide_Warnings && log.isWarning) continue;
				if (hide_Errors && log.isError) continue;
				if (filter_by_tag && (filterTag != log.tag)) continue;
				GUI.contentColor = log.color;
				GUI.backgroundColor = GUI.contentColor.SetA(0f);
				if (GUILayout.Button(log.logString, btn_style, GUILayout.ExpandWidth(false))) {
					debug_gui.wndInfobox.ShowContent(
						log.logString + "\n\n" + log.stackTrace, GUI.contentColor);
				}
			}
		}
	}

	public class InfoboxWindow : GuiWindow {
		public DebugGUI debug_gui;
		public string content = "";
		public Color color = Color.white;
		public InfoboxWindow() : base() {
			title = "Info";
			windowRect = new Rect(0, 0, 200, 200);
			Center();
			BodyCallback = Draw_Body;
		}
		static GUIStyle btn_style = null;
		void InitStyles() {
			if (btn_style == null) {
				btn_style = new GUIStyle(GUI.skin.button);
				btn_style.alignment = TextAnchor.MiddleCenter;
				btn_style.margin = new RectOffset(0, 0, 0, 0);
				btn_style.wordWrap = true;
			}
		}
		void Draw_Body(GuiWindow wnd) {
			InitStyles();
			GUI.contentColor = color;
			if (GUILayout.Button(content, btn_style,
			                     GUILayout.ExpandHeight(true), GUILayout.ExpandWidth(true))) {
				wnd.is_visible = false;
			}
		}
		public void ShowContent(string content, Color color) {
			this.content = content.Trim();
			this.color = color;
			Show();
		}
	}
	#endregion
}

public class GuiWindow {
	public static bool CenterOnShow = false;
	
	public int id = 0;
	public string title = "";
	public Rect windowRect;
	public bool is_visible = false;
	public bool modal = false;
	public System.Action<GuiWindow> BodyCallback;
	public System.Action<GuiWindow> ToolbarCallback;
	public object object_data = null;

	public bool manualClose = false;
	public bool mayClose = false;
	public bool attemptToClose = false;
	
	public Color GUI_backgroundColor {get; private set;}
	public Color GUI_color {get; private set;}
	public Color GUI_contentColor {get; private set;}
	
	public Vector2 scrollPosition = Vector2.zero;
	
	bool isResizing = false;
	bool isDragging = false;
	Rect resizeStart = new Rect();
	protected Vector2 minWindowSize = new Vector2(96, 96);

	float last_gui_scale = -1f;
	
	static Vector2 visibleWindowOffset = new Vector2(12, 12);
	static GUIStyle styleWindowResize = null;
	static GUIContent gcDrag = new GUIContent("//", "drag to resize");
	
	public GuiWindow(System.Action<GuiWindow> BodyCallback=null, System.Action<GuiWindow> ToolbarCallback=null) {
		id = this.GetHashCode();
		this.BodyCallback = BodyCallback;
		this.ToolbarCallback = ToolbarCallback;
		this.windowRect = new Rect(0, 0, minWindowSize.x, minWindowSize.y);
		this.title = "window";
	}
	public void Draw() {
		if (!is_visible) return;

		GUI_backgroundColor = GUI.backgroundColor;
		GUI_color = GUI.color;
		GUI_contentColor = GUI.contentColor;
		
		if (mayClose) {
			GUI.backgroundColor = GUI_backgroundColor.SetA(GUI_backgroundColor.a*0.5f);
			GUI.color = GUI_color.SetA(GUI_color.a*0.5f);
			GUI.contentColor = GUI_contentColor.SetA(GUI_contentColor.a*0.5f);
		}

		float curr_gui_scale = DebugGUI.GUIScaleFactor;
		if (last_gui_scale != curr_gui_scale) {
			// Keep visual position/size the same
			if (last_gui_scale > 0) {
				float factor = last_gui_scale / curr_gui_scale;
				windowRect.x *= factor;
				windowRect.y *= factor;
				windowRect.width *= factor;
				windowRect.height *= factor;
			}
			last_gui_scale = curr_gui_scale;
		}

		if (modal) {
			GUI.ModalWindow(id, windowRect, DoWindow, title);
		} else {
			GUI.Window(id, windowRect, DoWindow, title);
		}

		GUI.backgroundColor = GUI_backgroundColor;
		GUI.color = GUI_color;
		GUI.contentColor = GUI_contentColor;
	}
	protected virtual void DrawBody() {
		if (BodyCallback != null) {
			scrollPosition = GUILayout.BeginScrollView(scrollPosition, true, true);
			BodyCallback(this);
			GUILayout.EndScrollView();
		}
	}
	protected virtual void DrawToolbar() {
		if (ToolbarCallback != null) {
			GUILayout.BeginHorizontal();
			ToolbarCallback(this);
			GUILayout.EndHorizontal();
		}
	}
	void DoWindow(int windowID) {
		ResizeWindow();
		DrawToolbar();
		DrawBody();
		DragWindow();
	}

	public void Show() {
		if (!is_visible && CenterOnShow) Center();
		is_visible = true;
	}
	public void Hide() {
		is_visible = false;
	}
	public void Center() {
		float scl = last_gui_scale;
		if (scl <= 0) scl = DebugGUI.GUIScaleFactor;
		windowRect.x = (Screen.width/scl - windowRect.width)*0.5f;
		windowRect.y = (Screen.height/scl - windowRect.height)*0.5f;
	}

	// Adapted from http://answers.unity3d.com/questions/17676/guiwindow-resize-window.html
	void ResizeWindow() {
		if (styleWindowResize == null) styleWindowResize = GUI.skin.button;
		
		attemptToClose = false;
		mayClose = false;
		
		Vector2 mouse = new Vector2(Input.mousePosition.x, Screen.height - Input.mousePosition.y);
		mouse = GUIUtility.ScreenToGUIPoint(mouse);
		
		float size = styleWindowResize.CalcHeight(gcDrag, 100f);
		Rect r = new Rect(windowRect.width-size, windowRect.height-size, size, size);
		float margin_x = styleWindowResize.margin.right;
		float margin_y = styleWindowResize.margin.bottom;
		r.x -= margin_x;
		r.y -= margin_y;
		r.width += margin_x;
		r.height += margin_y;

		if (!isResizing) {
			if ((Event.current.type == EventType.mouseDown) && r.Contains(mouse)) {
				isResizing = true;
				resizeStart = new Rect(mouse.x, mouse.y, windowRect.width, windowRect.height);
				// the GUI.Button below will eat the event, and this way it will show its active state
				//Event.current.Use();
			}
		} else if (isResizing) {
			float w = resizeStart.width + (mouse.x - resizeStart.x);
			float h = resizeStart.height + (mouse.y - resizeStart.y);
			float _w = w - visibleWindowOffset.x;
			float _h = h - visibleWindowOffset.y;
			mayClose = ((_w*_w + _h*_h) <= (size*size));
			
			if ((Event.current.type == EventType.mouseUp) || !Input.GetMouseButton(0)) {
				isResizing = false;
				if (mayClose) {
					attemptToClose = true;
					if (!manualClose) {
						is_visible = false;
						// restore start size, since the intent is to close, not to resize
						windowRect.width = resizeStart.width;
						windowRect.height = resizeStart.height;
					}
				}
			} else {
				windowRect.width = Mathf.Max(minWindowSize.x, w);
				windowRect.height = Mathf.Max(minWindowSize.y, h);
			}

			//if (mayClose && !manualClose) {
			//	windowRect.width = resizeStart.width;
			//	windowRect.height = resizeStart.height;
			//}
		}
		
		var backgroundColor = GUI.backgroundColor;
		GUI.backgroundColor = Color.clear;
		GUI.Button(r, gcDrag, styleWindowResize);
		GUI.backgroundColor = backgroundColor;
	}
	void DragWindow() {
		if (isResizing) return;

		Vector2 mouse = new Vector2(Input.mousePosition.x, Screen.height - Input.mousePosition.y);
		mouse = GUIUtility.ScreenToGUIPoint(mouse);
		
		Rect r = new Rect(0, 0, windowRect.width, windowRect.height);
		if (!isDragging) {
			if ((Event.current.type == EventType.mouseDown) && r.Contains(mouse)) {
				isDragging = true;
				resizeStart = new Rect(mouse.x, mouse.y, windowRect.x, windowRect.y);
				Event.current.Use();
			}
		} else if (isDragging) {
			if ((Event.current.type == EventType.mouseUp) || !Input.GetMouseButton(0)) {
				isDragging = false;
			} else {
				windowRect.x += (mouse.x - resizeStart.x);
				windowRect.y += (mouse.y - resizeStart.y);
			}
		}
	}
}
