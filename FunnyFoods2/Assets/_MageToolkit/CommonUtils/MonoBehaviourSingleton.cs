﻿// Adapted from: http://wiki.unity3d.com/index.php/Singleton
using UnityEngine;

namespace Bini.Utils {
	/// ASSUMPTION: at all times, there is no more than one active instance.
	/// Responsibility for compliance with this condition lies with the user.
	/// 
	/// If there are several instances, use first active
	/// If no active but there are inactive -- use first inactive (if allowed)
	/// Whenever an instance is activated, it's assumed to be the most "current".
	/// If no instance was found, a new active instance is created (if allowed).
	/// Otherwise, a hidden instance is created and kept until a real instance
	/// is activated or another scene is loaded. (Instance returns null)

	/// <summary>
	/// Be aware this will not prevent a non singleton constructor such as `T myT = new T();`
	/// To prevent that, add `protected T () {}` to your singleton class.
	/// As a note, this is made as MonoBehaviour because we need Coroutines.
	/// </summary>
	public abstract class MonoBehaviourSingleton<T> : MonoBehaviour where T : MonoBehaviour {
		static object _lock = new object();

		static MonoBehaviourSingletonAttribute _attr = null;
		static void InitAttr() {
			var attrs = typeof(T).GetCustomAttributes(typeof(MonoBehaviourSingletonAttribute), true);
			if ((attrs != null) && (attrs.Length != 0)) {
				_attr = attrs[0] as MonoBehaviourSingletonAttribute;
			} else {
				_attr = new MonoBehaviourSingletonAttribute();
			}
		}

		static HideFlags _hideFlags = HideFlags.HideInInspector | HideFlags.NotEditable |
			HideFlags.HideInHierarchy | HideFlags.DontSave;

		static T _instance;
		static bool is_hidden = false;
		static void InitInstance() {
			_instance = SceneObjects.FirstComponent<T>(!_attr.OnlyActive, false);
			is_hidden = false;
			
			if (_instance == null) {
				var gameObj = new GameObject(typeof(T).ToString());
				_instance = gameObj.AddComponent<T>();
				
				if (!_attr.AutoCreate || !Application.isPlaying) {
					gameObj.hideFlags = _hideFlags;
					gameObj.SetActive(false);
					_instance.hideFlags = _hideFlags;
					_instance.enabled = false;
					is_hidden = true;
					//Debug.Log("MonoBehaviourSingleton<"+typeof(T).ToString()+"> not found; created hidden");
				} else {
					if (_attr.DontDestroyOnLoad) DontDestroyOnLoad(gameObj);
					//Debug.Log("MonoBehaviourSingleton<"+typeof(T).ToString()+"> not found; created public");
				}
			} else {
				//Debug.Log("MonoBehaviourSingleton<"+typeof(T).ToString()+"> found");
			}
		}
		
		public static T Instance {
			get {
				// already destroyed on application quit; won't create again
				if (applicationIsQuitting) {
					if (Application.isPlaying) return null;
					applicationIsQuitting = false;
				}

				lock (_lock) {
					if (_attr == null) InitAttr();

					if (_instance == null) InitInstance();

					// To avoid borderline-case checks, we assume that signletons aren't expected
					// to deactivate without a replacement being activated
					//if (_attr.OnlyActive && !(_instance.enabled && _instance.gameObject.activeInHierarchy)) return null;

					return is_hidden ? null : _instance;
				}
			}
			protected set {
				// already destroyed on application quit; won't create again
				if (applicationIsQuitting) {
					if (Application.isPlaying) return;
					applicationIsQuitting = false;
				}

				lock (_lock) {
					if (value == null) {
						if (is_hidden) return;
						_instance = null;
					} else {
						/*
						// dangerous. hidden objects would not be saved anyway
						if (is_hidden) {
							if (Application.isPlaying) {
								UnityEngine.Object.Destroy(_instance.gameObject);
							} else {
								UnityEngine.Object.DestroyImmediate(_instance.gameObject);
							}
						}
						*/
						_instance = value;
					}
					is_hidden = false;
				}
			}
		}

		void Awake() {
			Instance = this.GetComponent<T>();
		}
		void OnEnable() {
			Instance = this.GetComponent<T>();
		}

		static bool applicationIsQuitting = false;
		/// <summary>
		/// When Unity quits, it destroys objects in a random order.
		/// In principle, a Singleton is only destroyed when application quits.
		/// If any script calls Instance after it have been destroyed, 
		///   it will create a buggy ghost object that will stay on the Editor scene
		///   even after stopping playing the Application. Really bad!
		/// So, this was made to be sure we're not creating that buggy ghost object.
		/// </summary>
		void OnApplicationQuit() {
			applicationIsQuitting = true;
		}
	}

	[System.AttributeUsage(System.AttributeTargets.Class)]
	public class MonoBehaviourSingletonAttribute : System.Attribute {
		public bool OnlyActive {get; private set;}
		public bool AutoCreate {get; private set;}
		public bool DontDestroyOnLoad {get; private set;}
		public MonoBehaviourSingletonAttribute(bool only_active=false, bool auto_create=false, bool dont_destroy_on_load=false) {
			OnlyActive = only_active;
			AutoCreate = auto_create;
			DontDestroyOnLoad = dont_destroy_on_load;
			if (!Application.isPlaying) {
				AutoCreate = false;
				DontDestroyOnLoad = false;
			}
		}
	}
}
