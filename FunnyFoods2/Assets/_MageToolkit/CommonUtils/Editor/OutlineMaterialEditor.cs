﻿using UnityEngine;
using UnityEditor;
using System.Linq; // defines extension method Array.Contains()
using System.Collections.Generic;

public class OutlineMaterialEditor : MaterialEditor {
	enum SamplesCount {
		SAMPLE_4, SAMPLE_8//, SAMPLE_12, SAMPLE_16, SAMPLE_24, SAMPLE_32
	}

	public override void OnInspectorGUI() {
		base.OnInspectorGUI();
		
		if (!isVisible) return;
		
		Material targetMat = target as Material;

		var keywords = targetMat.shaderKeywords;

		bool use_sample8 = keywords.Contains("SAMPLE_8");
		bool use_alphamap = keywords.Contains("USE_SEPARATE_ALPHA_MAP");

		var samples = (use_sample8 ? SamplesCount.SAMPLE_8 : SamplesCount.SAMPLE_4);

		EditorGUI.BeginChangeCheck();

		samples = (SamplesCount)EditorGUILayout.EnumPopup("Samples", samples);
		use_alphamap = EditorGUILayout.Toggle("Use AlphaMap", use_alphamap);

		if (EditorGUI.EndChangeCheck()) {
			use_sample8 = (samples == SamplesCount.SAMPLE_8);
			if (use_sample8 & use_alphamap) {
				targetMat.shaderKeywords = new string[] {"SAMPLE_8", "USE_SEPARATE_ALPHA_MAP"};
			} else if (use_sample8) {
				targetMat.shaderKeywords = new string[] {"SAMPLE_8"};
			} else if (use_alphamap) {
				targetMat.shaderKeywords = new string[] {"USE_SEPARATE_ALPHA_MAP"};
			} else {
				targetMat.shaderKeywords = null;
			}
			EditorUtility.SetDirty(targetMat);
		}
	}
}