﻿using UnityEngine;
using UnityEditor;
using System.Linq; // defines extension method Array.Contains()
using System.Collections.Generic;

public class AlphamapMaterialInspector : MaterialEditor {
	public override void OnInspectorGUI() {
		base.OnInspectorGUI();
		
		if (!isVisible) return;
		
		Material targetMat = target as Material;

		var keywords = targetMat.shaderKeywords;

		bool use_alphamap = keywords.Contains("USE_SEPARATE_ALPHA_MAP");

		EditorGUI.BeginChangeCheck();

		use_alphamap = EditorGUILayout.Toggle("Use AlphaMap", use_alphamap);

		if (EditorGUI.EndChangeCheck()) {
			targetMat.shaderKeywords = (use_alphamap ? new string[] {"USE_SEPARATE_ALPHA_MAP"} : null);

			EditorUtility.SetDirty(targetMat);
		}
	}
}