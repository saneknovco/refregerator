﻿using UnityEditor;
using UnityEngine;
using System.Collections.Generic;
using System.IO;
using System.Text.RegularExpressions;
using Bini.Utils;

//[InitializeOnLoad]
public class SaveCachedAssets {
	class CachedAsset {
		public Object asset;
		public string name = "";
		
		public string tag = "";
		public bool isMain = false;
		
		public bool isAsset = false;
		public string path = "";
		
		public List<GameObject> gameobjs = new List<GameObject>();
		
		public CachedAsset(Object asset, GameObject obj, string tag="", bool isMain=false) {
			this.asset = asset;
			name = MakeValidFileName(asset.name);
			
			this.tag = MakeValidFileName(tag);
			this.isMain = isMain;
			
			isAsset = AssetDatabase.Contains(asset);
			path = AssetDatabase.GetAssetOrScenePath(asset);
			
			gameobjs.Add(obj);
		}
	}
	
	static string MakeValidFileName(string s) {
		// TODO: remove " (Instance)" ?
		return Regex.Replace(s, "\\W", "_");
	}
	
	static void MemAsset(Dictionary<Object, CachedAsset> assets,
			Object asset, GameObject obj, string tag="", Object main_asset=null) {
		if (asset == null) return;
		if (assets.ContainsKey(asset)) return;
		assets.Add(asset, new CachedAsset(asset, obj, tag, (asset == main_asset)));
	}
	
	static Dictionary<Object, CachedAsset> ExtractObjectAssets(GameObject obj) {
		var assets = new Dictionary<Object, CachedAsset>();
		
		var renderer = obj.GetComponent<Renderer>();
		if (renderer != null) {
			foreach (var material in renderer.sharedMaterials) {
				if (material == null) continue;
				MemAsset(assets, material, obj, "", renderer.sharedMaterial);
				
				var shader = material.shader;
				if (shader == null) continue;
				
				int nprops = ShaderUtil.GetPropertyCount(shader);
				for (int i = 0; i < nprops; i++) {
					var propname = ShaderUtil.GetPropertyName(shader, i);
					
					var proptype = ShaderUtil.GetPropertyType(shader, i);
					if (proptype != ShaderUtil.ShaderPropertyType.TexEnv) continue;
					
					var tex = material.GetTexture(propname);
					if (tex == null) continue;
					MemAsset(assets, tex, obj, propname, material.mainTexture);
				}
			}
		}
		
		var mesh_filter = obj.GetComponent<MeshFilter>();
		if (mesh_filter != null) {
			MemAsset(assets, mesh_filter.sharedMesh, obj, "");
		}
		
		var mesh_collider = obj.GetComponent<MeshCollider>();
		if (mesh_collider != null) {
			MemAsset(assets, mesh_collider.sharedMesh, obj, "_collider");
		}
		
		return assets;
	}
	
	static void FillAssetNames(Dictionary<Object, CachedAsset> assets, string obj_name) {
		// Compute name counts
		var names = new Dictionary<string, int>();
		if (!string.IsNullOrEmpty(obj_name)) names.Add(obj_name, 1);
		
		int empty_count = 0;
		foreach (var asset in assets.Values) {
			string name = asset.name;
			if (string.IsNullOrEmpty(name)) {
				if (!asset.isAsset) empty_count++;
				continue;
			}
			
			int count;
			if (names.TryGetValue(name, out count)) {
				names[name] = count + 1;
			} else {
				names.Add(name, 1);
			}
		}
		
		if (empty_count == 0) return; // Nothing to do here
		
		// Find most-used name
		int maxcount = 0;
		string bestname = "";
		foreach (var kv in names) {
			if (kv.Value > maxcount) {
				maxcount = kv.Value;
				bestname = kv.Key;
			} else if ((kv.Value == maxcount) && (bestname == obj_name)) {
				bestname = kv.Key;
			}
		}
		
		foreach (var asset in assets.Values) {
			if (asset.isAsset) continue;
			if (!string.IsNullOrEmpty(asset.name)) continue;
			asset.name = bestname;
		}
	}
	
	[MenuItem("Assets/Save Cached Assets")]
	static void SaveAssets() {
		EditorUtility.ClearProgressBar();
		
		var assets = new Dictionary<Object, CachedAsset>();
		
		foreach (var obj in SceneObjects.Objects(true)) {
			string obj_name = MakeValidFileName(obj.name);
			var obj_assets = ExtractObjectAssets(obj);
			FillAssetNames(obj_assets, obj_name);
			
			foreach (var kv in obj_assets) {
				if (kv.Value.isAsset) continue;
				CachedAsset asset;
				if (assets.TryGetValue(kv.Key, out asset)) {
					asset.gameobjs.AddRange(kv.Value.gameobjs);
					if (string.IsNullOrEmpty(asset.tag)) asset.tag = kv.Value.tag;
				} else {
					assets.Add(kv.Key, kv.Value);
				}
			}
		}
		
		float assets_count = assets.Count;
		float progress = 0f;
		EditorUtility.DisplayProgressBar("Saving Cached Assets", "", 0f);
		foreach (var asset in assets.Values) {
			if (!string.IsNullOrEmpty(asset.tag)) continue;
			SaveAsset(asset);
			progress += 1f / assets_count;
			EditorUtility.DisplayProgressBar("Saving Cached Assets", asset.name, progress);
		}
		foreach (var asset in assets.Values) {
			if (string.IsNullOrEmpty(asset.tag)) continue;
			SaveAsset(asset);
			progress += 1f / assets_count;
			EditorUtility.DisplayProgressBar("Saving Cached Assets", asset.name, progress);
		}
		EditorUtility.ClearProgressBar();
		
		System.GC.Collect();
		EditorUtility.UnloadUnusedAssets();
	}
	
	static void SaveAsset(CachedAsset asset) {
		//Selection.objects = asset.gameobjs.ToArray();
		//EditorApplication.RepaintHierarchyWindow();
		//SceneView.RepaintAll();
		
		// Allowed extensions:
		// mat -- for materials
		// cubemap - for cubemaps
		// GUISkin -- for gui skins
		// anim -- for animation clips
		// asset -- for other assets (Mesh, ScriptableObject, etc.)
		// prefab -- for prefabs (GameObjects?)
		// shader -- for imported shaders
		// png jpg ... -- for imported textures
		// blend fbx ... -- for imported models
		string ext = "", subdir = "", category = "";
		if (asset.asset is Texture2D) {
			category = "Texture";
			subdir = "Textures/";
			ext = "png";
		} else if (asset.asset is Material) {
			category = "Material";
			subdir = "Materials/";
			ext = "mat";
		} else if (asset.asset is Mesh) {
			category = "Mesh";
			subdir = "Meshes/";
			ext = "asset";
		} else {
			return; // (not implemented)
		}
		
		string dir, file;
		FileUtils.SplitPath(asset.path, out dir, out file);
		if (!string.IsNullOrEmpty(dir)) {
			dir += subdir;
			FileUtils.EnsureAssetDir(dir);
		}
		file = asset.name;
		
		var objnames = new List<string>();
		foreach (var obj in asset.gameobjs) {
			objnames.Add(obj.name);
		}
		string title = category+" "+asset.tag+": "+string.Join(", ", objnames.ToArray());
		
		string path = EditorUtility.SaveFilePanel(title, dir, file+"."+ext, ext);
		if (!path.StartsWith(FileUtils.ProjectPath)) {
			return;
		} else {
			path = path.Substring(FileUtils.ProjectPath.Length);
		}
		
		if (category == "Texture") {
			var old_asset = asset.asset as Texture2D;
			var new_asset = Texture2D_ToAsset(path, old_asset);
			ReplaceTextures(old_asset, new_asset, asset.gameobjs);
		} else if (category == "Material") {
			var old_asset = asset.asset as Material;
			var new_asset = Generic_ToAsset<Material>(path, old_asset);
			ReplaceMaterials(old_asset, new_asset, asset.gameobjs);
		} else if (category == "Mesh") {
			var old_asset = asset.asset as Mesh;
			var new_asset = Generic_ToAsset<Mesh>(path, old_asset);
			ReplaceMeshes(old_asset, new_asset, asset.gameobjs);
		}
	}
	
	static void ReplaceTextures(Texture old_asset, Texture new_asset, IEnumerable<GameObject> gameobjs) {
		if (new_asset == old_asset) return;
		
		foreach (var obj in gameobjs) {
			var renderer = obj.GetComponent<Renderer>();
			if (renderer != null) {
				foreach (var material in renderer.sharedMaterials) {
					if (material == null) continue;
					
					var shader = material.shader;
					if (shader == null) continue;
					
					int nprops = ShaderUtil.GetPropertyCount(shader);
					for (int i = 0; i < nprops; i++) {
						var propname = ShaderUtil.GetPropertyName(shader, i);
						
						var proptype = ShaderUtil.GetPropertyType(shader, i);
						if (proptype != ShaderUtil.ShaderPropertyType.TexEnv) continue;
						
						var tex = material.GetTexture(propname);
						if (tex == old_asset) {
							material.SetTexture(propname, new_asset);
						}
					}
				}
			}
		}
	}
	
	static void ReplaceMaterials(Material old_asset, Material new_asset, IEnumerable<GameObject> gameobjs) {
		if (new_asset == old_asset) return;
		
		foreach (var obj in gameobjs) {
			var renderer = obj.GetComponent<Renderer>();
			if (renderer != null) {
				for (int i = 0; i < renderer.sharedMaterials.Length; i++) {
					var material = renderer.sharedMaterials[i];
					if (material == old_asset) {
						renderer.sharedMaterials[i] = new_asset;
					}
				}
			}
		}
	}
	
	static void ReplaceMeshes(Mesh old_asset, Mesh new_asset, IEnumerable<GameObject> gameobjs) {
		if (new_asset == old_asset) return;
		
		foreach (var obj in gameobjs) {
			var mesh_filter = obj.GetComponent<MeshFilter>();
			if (mesh_filter != null) {
				if (mesh_filter.sharedMesh == old_asset) {
					mesh_filter.sharedMesh = new_asset;
				}
			}
			
			var mesh_collider = obj.GetComponent<MeshCollider>();
			if (mesh_collider != null) {
				if (mesh_collider.sharedMesh == old_asset) {
					mesh_collider.sharedMesh = new_asset;
				}
			}
		}
	}
	
	static Texture2D Texture2D_ToAsset(string path, Texture2D tex) {
		//if (AssetDatabase.Contains(tex)) return tex;
		
		#region Gather import settings
		bool isReadable = true;
		try {
			tex.GetPixel(0, 0);
		} catch (System.Exception) {
			isReadable = false;
		}
		
		var textureFormat = TextureImporterFormat.ARGB32;
		try {
			textureFormat = (TextureImporterFormat) System.Enum.Parse(
				typeof(TextureImporterFormat),
				System.Enum.GetName(typeof(TextureFormat), tex.format));
		} catch (System.Exception) {
		}
		
		bool mipmapEnabled = (tex.mipmapCount > 1);
		
		var wrapMode = tex.wrapMode;
		
		var filterMode = tex.filterMode;
		#endregion
		
		// Convert the texture to a format compatible with EncodeToPNG
		if ((tex.format != TextureFormat.ARGB32) && (tex.format != TextureFormat.RGB24)) {
			tex = ((Pixmap<Color32>)tex).ToTexture();
		}
		// Write the texture to file
		var pngData = tex.EncodeToPNG();
		if (pngData != null) {
			FileUtils.EnsureAssetDir(path);
			File.WriteAllBytes(path, pngData);
		}
		
		AssetDatabase.Refresh();
		
		#region Apply import settings
		var textureImporter = AssetImporter.GetAtPath(path) as TextureImporter;
		textureImporter.textureType = TextureImporterType.Advanced;
		textureImporter.textureFormat = textureFormat;
		textureImporter.isReadable = isReadable;
		textureImporter.maxTextureSize = 4096;
		textureImporter.npotScale = TextureImporterNPOTScale.None;
		textureImporter.mipmapEnabled = mipmapEnabled;
		textureImporter.wrapMode = wrapMode;
		textureImporter.filterMode = filterMode;
		AssetDatabase.WriteImportSettingsIfDirty(path);
		#endregion
		
		AssetDatabase.ImportAsset(path);
		return AssetDatabase.LoadAssetAtPath(path, typeof(Texture2D)) as Texture2D;
	}
	
	static T Generic_ToAsset<T>(string path, T asset) where T : Object {
		FileUtils.EnsureAssetDir(path);
		
		var storedAsset = AssetDatabase.LoadAssetAtPath(path, typeof(T)) as T;
		if (storedAsset != null) {
			EditorUtility.CopySerialized(asset, storedAsset);
		} else {
			AssetDatabase.CreateAsset(asset, path);
		}
		AssetDatabase.SaveAssets();
		
		AssetDatabase.Refresh();
		
		AssetDatabase.WriteImportSettingsIfDirty(path);
		
		AssetDatabase.ImportAsset(path);
		//if (storedAsset == null) {
			storedAsset = AssetDatabase.LoadAssetAtPath(path, typeof(T)) as T;
		//}
		return storedAsset;
	}
}
