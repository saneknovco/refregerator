﻿#define DONT_CACHE_ASSETLIB

using UnityEngine;
using UnityEditor;
using UnityEditor.SceneManagement;
using System.Collections.Generic;
using System.IO;
using System.Text.RegularExpressions;
using System.Linq;

public class BuildManager_Window : EditorWindow {
	public static BuildManager_Window window = null;

	static bool initialized {
		get { return (window != null); }
	}

	BuildManager_Window() {
		window = this;
	}

	// Static constructor is called if the class has [InitializeOnLoad] attribute
	// Or if window is opened as a part of default editor layout
	static BuildManager_Window() {
		Init(true);
	}

	[MenuItem("Mage/Build Manager...", priority = 1)]
    static void InitFromMenu() {
		Init(false);
	}
	static void Init(bool window_exists=false) {
		if (initialized) return;

		if (!window_exists) {
			// If GetWindow() is called from the static constructor
			// while the window is still open, Unity throws an error!
			window = GetWindow<BuildManager_Window>();
			window.titleContent.text = "Build Manager";
			window.autoRepaintOnSceneChange = true; // maybe disable later
		}
	}	

	MultiBuildInfo multi_buid_info = new MultiBuildInfo();
	List<BuildTargetInfo> target_infos = null;
	GUIStyle style_normal, style_active;
	float targets_min_w = 0;
	Vector2 targets_scroll;

	void InitTargets() {
		if (target_infos != null) return;

		style_normal = new GUIStyle(GUI.skin.label);
		style_active = new GUIStyle(style_normal);
		style_active.fontStyle = FontStyle.Bold;
		style_active.font = EditorStyles.boldFont;

		targets_min_w = 0;
		var content = new GUIContent();
		target_infos = new List<BuildTargetInfo>();
		var names = System.Enum.GetNames(typeof(BuildTarget));
		foreach (string name in names) {
			var target = (BuildTarget)System.Enum.Parse(typeof(BuildTarget), name);
			var target_info = new BuildTargetInfo(target);
			if (target_info.obsolete) continue;
			target_infos.Add(target_info);
			content.text = target_info.name;
			targets_min_w = Mathf.Max(targets_min_w, style_active.CalcSize(content).x);
		}
		target_infos.Sort((ti0, ti1) => (ti0.name.CompareTo(ti1.name)));
		content.text = "";
		targets_min_w += GUI.skin.toggle.CalcSize(content).x * 2f;

		// Unity seems to recompile scripts after each build,
		// so we have to serialize the build jobs in between.
		// Loading jobs should be sufficient on window initialization
		// (happens also when the scripts are reloaded).
		LoadJob();
	}

	GUIStyle TargetStyle(BuildTarget target) {
		return (target == EditorUserBuildSettings.activeBuildTarget ? style_active : style_normal);
	}

	Vector2 scenes_scroll;

	bool build_all = true;

	void OnGUI() {
		if (!initialized) return;

		var content = new GUIContent();

		BuildTargetInfo active_info = new BuildTargetInfo();

		int build_button_pressed = 0;

		EditorGUI.BeginDisabledGroup(PlatformLibraryCache.EditorIsBusy | (build_jobs != null)); {
			GUILayout.BeginHorizontal(); {
				targets_scroll = GUILayout.BeginScrollView(targets_scroll, false, true, GUILayout.Width(targets_min_w+8)); {
					EditorGUILayout.Separator();
					InitTargets(); // anything accessing GUI must happen in a GUI callback
					foreach (var target_info in target_infos) {
						if (target_info.target == EditorUserBuildSettings.activeBuildTarget) active_info = target_info;
						GUILayout.BeginHorizontal(); {
							bool build = multi_buid_info.GetTarget(target_info.target);
							build = GUILayout.Toggle(build, "", GUILayout.ExpandWidth(false));
							content.text = target_info.name; content.tooltip = target_info.tooltip;
							if (GUILayout.Button(content, TargetStyle(target_info.target))) {
								if (EditorUtility.DisplayDialog("Switch platform", "Make <"+target_info.name+"> platform active?", "Yes", "No")) {
									PlatformLibraryCache.Switch(target_info.target);
								}
							}
							multi_buid_info.SetTarget(target_info.target, build);
						} GUILayout.EndHorizontal();
					}
				} GUILayout.EndScrollView();

				GUILayout.BeginVertical(); {
					GUILayout.BeginHorizontal(); {
						GUILayout.Label("Scenes:", style_active, GUILayout.ExpandWidth(false));
						//GUILayout.Button("Add current", GUILayout.ExpandWidth(false));
					} GUILayout.EndHorizontal();
					scenes_scroll = GUILayout.BeginScrollView(scenes_scroll, false, true, GUI.skin.horizontalScrollbar, GUI.skin.verticalScrollbar, GUI.skin.box, GUILayout.MinHeight(210)); {
						var build_scenes = EditorBuildSettings.scenes;
						int enabled_count = 0;
						foreach (var build_scene in build_scenes) {
							var scene_name = build_scene.path;
							scene_name = scene_name.Substring("Assets/".Length);
							scene_name = scene_name.Substring(0, scene_name.Length - ".unity".Length);
							GUILayout.BeginHorizontal(); {
								build_scene.enabled = GUILayout.Toggle(build_scene.enabled, scene_name);
								if (build_scene.enabled) {
									GUILayout.Label(enabled_count.ToString(), GUILayout.ExpandWidth(false));
									enabled_count++;
								}
							} GUILayout.EndHorizontal();
						}
						//EditorSceneManager.sceneCount;
						//EditorSceneManager.GetSceneAt();
						EditorBuildSettings.scenes = build_scenes;
					} GUILayout.EndScrollView();

					GUILayout.BeginVertical(); {
						GUILayout.BeginHorizontal(); {
							GUILayout.Label("Name:", style_active, GUILayout.Width(45));
							multi_buid_info.BuildName = EditorGUILayout.TextField(multi_buid_info.BuildName);
							content.text = "All"; content.tooltip = "Build for all the selected platforms or only for the active platform";
							build_all = GUILayout.Toggle(build_all, content, EditorStyles.miniButton, GUILayout.Width(26));
							content.text = "\u2714 Build"; content.tooltip = (build_all ? "Build for all the selected platforms" : "Build for <"+active_info.name+">");
							if (GUILayout.Button(content, EditorStyles.miniButton, GUILayout.Width(55))) {
								build_button_pressed = 1;
							}
						} GUILayout.EndHorizontal();
						GUILayout.BeginHorizontal(); {
							GUILayout.Label("Path:", style_active, GUILayout.Width(45));
							multi_buid_info.BuildDir = EditorGUILayout.TextField(multi_buid_info.BuildDir);
							content.text = "..."; content.tooltip = "Select directory for the builds";
							if (GUILayout.Button(content, EditorStyles.miniButton, GUILayout.Width(26))) {
								GUI.FocusControl("");
								string save_dir = EditorUtility.SaveFolderPanel("Build location", multi_buid_info.BuildDir, "");
								if (!string.IsNullOrEmpty(save_dir)) {
									string save_path_rel = RelativePathToProject(save_dir);
									multi_buid_info.BuildDir = (save_dir.Length < save_path_rel.Length ? save_dir : save_path_rel);
								}
							}
							content.text = "\u25B6 Run"; content.tooltip = "Build & run for <"+active_info.name+">";
							if (GUILayout.Button(content, EditorStyles.miniButton, GUILayout.Width(55))) {
								build_button_pressed = 3;
							}
						} GUILayout.EndHorizontal();

						EditorGUILayout.Separator();

						if (GUILayout.Button("Player Settings", GUILayout.ExpandWidth(false))) {
							EditorApplication.ExecuteMenuItem("Edit/Project Settings/Player");
						}
						EditorUserBuildSettings.development = GUILayout.Toggle(EditorUserBuildSettings.development, "Development Build");
						EditorGUI.BeginDisabledGroup(!EditorUserBuildSettings.development); {
							EditorUserBuildSettings.connectProfiler = GUILayout.Toggle(EditorUserBuildSettings.connectProfiler, "Autoconnect Profiler");
							EditorUserBuildSettings.allowDebugging = GUILayout.Toggle(EditorUserBuildSettings.allowDebugging, "Script Debugging");
						} EditorGUI.EndDisabledGroup();
					} GUILayout.EndVertical();
				} GUILayout.EndVertical();
			} GUILayout.EndHorizontal();
		} EditorGUI.EndDisabledGroup();

		// Build after all GUI is done (invoking the build process from inside GUI layouting code results in unity GUI exception afterwards)
		if (build_button_pressed != 0) {
			GUI.FocusControl("");
			if (string.IsNullOrEmpty(multi_buid_info.BuildDir)) {
				EditorUtility.DisplayDialog("Cannot build", "Build directory is not specified", "Ok");
			} else if (string.IsNullOrEmpty(multi_buid_info.BuildName)) {
				EditorUtility.DisplayDialog("Cannot build", "Build name is not specified", "Ok");
			} else {
				if (build_button_pressed == 1) {
					build_jobs = multi_buid_info.BuildJobs(build_all, false);
				} else if (build_button_pressed == 3) {
					build_jobs = multi_buid_info.BuildJobs(false, true);
				}
			}
		}
	}

	Queue<MultiBuildInfo.MultiBuildJob> build_jobs = null;

	void Update() {
		if (build_jobs == null) return;
		if (build_jobs.Count == 0) { build_jobs = null; return; }
		if (PlatformLibraryCache.EditorIsBusy) return;
		var job = build_jobs.Dequeue();
		SaveJob(); // in case of some error, at least we would skip this item
		job.Do();
	}

	void LoadJob() {
		string lib_dir = GetLibraryPath();
		string job_file = lib_dir+"/MultiBuild.job";
		if (!System.IO.File.Exists(job_file)) return;
		var lines = System.IO.File.ReadAllLines(job_file, System.Text.Encoding.UTF8);
		int i = 0;
		while (i < lines.Length) {
			if (string.IsNullOrEmpty(lines[i])) {
				i++;
			} else {
				var job = new MultiBuildInfo.MultiBuildJob();
				job.ParseFromLines(lines, ref i);
				if (build_jobs == null) build_jobs = new Queue<MultiBuildInfo.MultiBuildJob>();
				build_jobs.Enqueue(job);
			}
		}
	}
	void SaveJob() {
		string lib_dir = GetLibraryPath();
		string job_file = lib_dir+"/MultiBuild.job";
		if ((build_jobs == null) || (build_jobs.Count == 0)) {
			System.IO.File.Delete(job_file);
			build_jobs = null;
			return;
		}
		var str_builder = new System.Text.StringBuilder();
		var jobs_array = build_jobs.ToArray();
		foreach (var job in jobs_array) {
			job.AppendToString(str_builder);
			str_builder.AppendLine("");
		}
		System.IO.File.WriteAllText(job_file, str_builder.ToString(), System.Text.Encoding.UTF8);
	}

	void OnDestroy() {
		if (!initialized) return;

		window = null;
	}

	// =========================================================== //

	static string GetLibraryPath() {
		string proj_dir = System.IO.Path.GetDirectoryName(Application.dataPath);
		return System.IO.Path.Combine(proj_dir, "Library");
	}

	static string RelativePathToProject(string path) {
		string proj_dir = System.IO.Path.GetDirectoryName(Application.dataPath);
		return RelativePath(proj_dir, path);
	}
	// Adapted from https://www.iandevlin.com/blog/2010/01/csharp/generating-a-relative-path-in-csharp
	static char[] dir_separators = new char[]{System.IO.Path.DirectorySeparatorChar, System.IO.Path.AltDirectorySeparatorChar};
	static string RelativePath(string absPath, string relTo) {
		string[] absDirs = absPath.Split(dir_separators);
		string[] relDirs = relTo.Split(dir_separators);
		// Get the shortest of the two paths
		int len = absDirs.Length < relDirs.Length ? absDirs.Length : relDirs.Length;
		// Use to determine where in the loop we exited
		int lastCommonRoot = -1;
		int index;

		// Find common root
		for (index = 0; index < len; index++) {
			if (absDirs[index] == relDirs[index]) {
				lastCommonRoot = index;
			} else {
				break;
			}
		}
		if (lastCommonRoot == -1) return relTo; // we didn't find a common prefix

		// Build up the relative path
		var relativePath = new System.Text.StringBuilder();
		// Add the ..
		for (index = lastCommonRoot + 1; index < absDirs.Length; index++) {
			if (absDirs[index].Length > 0) {
				relativePath.Append("..");
				relativePath.Append("/"); //relativePath.Append(dir_separators[0]);
			}
		}
		// Add the folders
		for (index = lastCommonRoot + 1; index < relDirs.Length - 1; index++) {
			relativePath.Append(relDirs[index]);
			relativePath.Append("/"); //relativePath.Append(dir_separators[0]);
		}
		relativePath.Append(relDirs[relDirs.Length - 1]);
		return relativePath.ToString();
	}
}

class MultiBuildInfo {
	static string config_file_name = "MultiBuildInfo.txt";

	string build_dir = "";
	string build_name = "";
	HashSet<BuildTarget> targets = new HashSet<BuildTarget>();

	List<BuildTarget> targets_sorted {
		get {
			var res = new List<BuildTarget>(targets);
			res.Sort((t0, t1) => (BuildTargetInfo.TargetToGroup(t0).CompareTo(BuildTargetInfo.TargetToGroup(t1))));
			return res;
		}
	}

	public MultiBuildInfo() {
		Load();
	}

	public string BuildDir {
		get { return build_dir; }
		set {
			if (string.IsNullOrEmpty(value)) value = "";
			if (build_dir == value) return;
			build_dir = value; Save();
		}
	}
	public string BuildName {
		get { return build_name; }
		set {
			if (string.IsNullOrEmpty(value)) value = "";
			if (build_name == value) return;
			build_name = value; Save();
		}
	}

	public bool GetTarget(BuildTarget target) {
		return targets.Contains(target);
	}
	public void SetTarget(BuildTarget target, bool build) {
		if (targets.Contains(target)) {
			if (!build) { targets.Remove(target); Save(); }
		} else {
			if (build) { targets.Add(target); Save(); }
		}
	}

	public void Load() {
		string path = System.IO.Path.Combine(GetLibraryPath(), config_file_name);
		if (!System.IO.File.Exists(path)) return;

		foreach (var line in System.IO.File.ReadAllLines(path, System.Text.Encoding.UTF8)) {
			string k, v;
			if (!SplitBy(line, "=", out k, out v)) continue;
			k = k.ToLower();
			if (k == "builddir") {
				build_dir = v;
			} else if (k == "buildname") {
				build_name = v;
			} else if (k == "targets") {
				var target_names = v.Split(',');
				foreach (var target_name in target_names) {
					try {
						targets.Add((BuildTarget)System.Enum.Parse(typeof(BuildTarget), target_name.Trim()));
					} catch (System.ArgumentException) {
					}
				}
			}
		}
	}

	public void Save() {
		string path = System.IO.Path.Combine(GetLibraryPath(), config_file_name);

		var lines = new List<string>();

		lines.Add("BuildDir="+build_dir);
		lines.Add("BuildName="+build_name);

		var target_names = new string[targets.Count];
		int i = 0;
		foreach (var target in targets) {
			target_names[i] = target.ToString();
			i++;
		}
		lines.Add("Targets="+string.Join(",", target_names));

		System.IO.File.WriteAllLines(path, lines.ToArray(), System.Text.Encoding.UTF8);
	}

	public class MultiBuildJob {
		public bool do_switch=false, do_build=false;
		public string[] scene_paths;
		public string platform_path;
		public BuildTarget target;
		public BuildOptions options;

		public void Do() {
			if (do_switch) PlatformLibraryCache.Switch(target);
			if (do_build) BuildPipeline.BuildPlayer(scene_paths, platform_path, target, options);
		}

		public void AppendToString(System.Text.StringBuilder str_builder) {
			str_builder.AppendLine(do_switch.ToString());
			str_builder.AppendLine(do_build.ToString());
			str_builder.AppendLine(target.ToString());
			str_builder.AppendLine(options.ToString());
			str_builder.AppendLine(platform_path);
			if (scene_paths != null) {
				foreach (string scene_path in scene_paths) {
					str_builder.AppendLine(scene_path);
				}
			}
			str_builder.AppendLine("");
		}

		public void ParseFromLines(string[] lines, ref int i) {
			do_switch = bool.Parse(lines[i]); i++;
			do_build = bool.Parse(lines[i]); i++;
			target = (BuildTarget)System.Enum.Parse(typeof(BuildTarget), lines[i]); i++;
			options = (BuildOptions)System.Enum.Parse(typeof(BuildOptions), lines[i]); i++;
			platform_path = lines[i]; i++;
			var _scene_paths = new List<string>();
			while (!string.IsNullOrEmpty(lines[i])) {
				_scene_paths.Add(lines[i]); i++;
			}
			scene_paths = _scene_paths.ToArray();
		}
	}

	public Queue<MultiBuildJob> BuildJobs(bool all, bool run) {
		string[] scene_paths = GetScenePaths();

		BuildOptions options = BuildOptions.None;

		if (run) options |= BuildOptions.AutoRunPlayer;
		if ((!all) | (targets.Count == 1)) options |= BuildOptions.ShowBuiltPlayer;

		if (EditorUserBuildSettings.development) options |= BuildOptions.Development;
		if (EditorUserBuildSettings.connectProfiler) options |= BuildOptions.ConnectWithProfiler;
		if (EditorUserBuildSettings.allowDebugging) options |= BuildOptions.AllowDebugging;

		var jobs = new Queue<MultiBuildJob>();

		if (all) {
			var _targets = targets_sorted;
			foreach (BuildTarget _target in _targets) {
				string platform_path = CaclPlatformPath(_target);
				//PlatformLibraryCache.Switch(_target);
				//BuildPipeline.BuildPlayer(scene_paths, platform_path, _target, options);
				jobs.Enqueue(new MultiBuildJob(){do_switch=true, do_build=true, scene_paths=scene_paths, platform_path=platform_path, target=_target, options=options});
			}
			BuildTarget target = EditorUserBuildSettings.activeBuildTarget;
			//PlatformLibraryCache.Switch(target); // switch back
			jobs.Enqueue(new MultiBuildJob(){do_switch=true, do_build=false, target=target});
		} else {
			BuildTarget target = EditorUserBuildSettings.activeBuildTarget;
			string platform_path = CaclPlatformPath(target);
			//BuildPipeline.BuildPlayer(scene_paths, platform_path, target, options);
			jobs.Enqueue(new MultiBuildJob(){do_switch=false, do_build=true, scene_paths=scene_paths, platform_path=platform_path, target=target, options=options});
		}

		return jobs;
	}

	string CaclPlatformPath(BuildTarget target) {
		var target_info = new BuildTargetInfo(target);
		string proj_dir = System.IO.Path.GetDirectoryName(Application.dataPath);
		string platform_path = System.IO.Path.Combine(proj_dir, build_dir+"/"+target_info.name+"/"+build_name+"/");
		if (!System.IO.Directory.Exists(platform_path)) System.IO.Directory.CreateDirectory(platform_path);
		platform_path += build_name+target_info.extension;
		return platform_path;
	}

	string[] GetScenePaths() {
		var scene_paths = new List<string>();
		foreach (var build_scene in EditorBuildSettings.scenes) {
			if (build_scene.enabled) scene_paths.Add(build_scene.path);
		}
		return scene_paths.ToArray();
	}

	static string GetLibraryPath() {
		string proj_dir = System.IO.Path.GetDirectoryName(Application.dataPath);
		return System.IO.Path.Combine(proj_dir, "Library");
	}
	static bool SplitBy(string s, string t, out string s0, out string s1) {
		s0 = s; s1 = "";
		if (string.IsNullOrEmpty(t)) return false;
		int i = s.IndexOf(t);
		if (i < 0) return false;
		s0 = s.Substring(0, i);
		s1 = s.Substring(i+t.Length);
		return true;
	}
}

static class PlatformLibraryCache {
	public static void Switch(BuildTargetGroup new_platform) {
		Switch(BuildTargetInfo.GroupToTarget(new_platform));
	}
	public static void Switch(BuildTarget target) {
		if (target == EditorUserBuildSettings.activeBuildTarget) return;
		if (EditorIsBusy) throw new System.InvalidOperationException("Cannot switch platform (editor is compiling/updating or in play mode)");

		var old_platform = BuildTargetInfo.TargetToGroup(EditorUserBuildSettings.activeBuildTarget);
		var new_platform = BuildTargetInfo.TargetToGroup(target);

		if (new_platform != old_platform) {
			string lib_dir = GetLibraryPath();
			string old_dir = System.IO.Path.Combine(lib_dir, "Cache_"+old_platform.ToString());
			string new_dir = System.IO.Path.Combine(lib_dir, "Cache_"+new_platform.ToString());

			// Note: we cannot move the Library directory directly, because some files in it are in use.
			// However, the "metadata" and "ShaderCache" directories (which contain the actual bulk of
			// the project assets) can be freely moved (at least in Unity 5.3.2).
			string[] items = new string[]{"metadata", "ShaderCache"};
			#if !DONT_CACHE_ASSETLIB
			ShuffleFiles(lib_dir, old_dir, new_dir, items);
			#else
			foreach (string platform_name in System.Enum.GetNames(typeof(BuildTargetGroup))) {
				ClearDirectory(System.IO.Path.Combine(lib_dir, "Cache_"+platform_name));
			}
			#endif

			BiniUberShader.SwitchPlatform(old_platform.ToString(), new_platform.ToString());
		}

		EditorUserBuildSettings.SwitchActiveBuildTarget(target);

		AssetDatabase.Refresh();
	}

	static void ShuffleFiles(string lib_dir, string old_dir, string new_dir, string[] items) {
		if (!System.IO.Directory.Exists(lib_dir)) return;

		if (!System.IO.Directory.Exists(old_dir)) {
			System.IO.Directory.CreateDirectory(old_dir);
		} else {
			ClearDirectory(old_dir);
		}

		foreach (string item in items) {
			string lib_item = System.IO.Path.Combine(lib_dir, item);
			string old_item = System.IO.Path.Combine(old_dir, item);
			string new_item = System.IO.Path.Combine(new_dir, item);

			if (System.IO.File.Exists(new_item) | System.IO.Directory.Exists(new_item)) {
				if (System.IO.File.Exists(lib_item)) {
					System.IO.File.Move(lib_item, old_item);
					System.IO.File.Move(new_item, lib_item);
				} else if (System.IO.Directory.Exists(lib_item)) {
					System.IO.Directory.Move(lib_item, old_item);
					System.IO.Directory.Move(new_item, lib_item);
				}
			} else {
				// After the file shuffling, valid files/directories
				// must remain, or Unity will hang on platform switch
				if (System.IO.File.Exists(lib_item)) {
					System.IO.File.Copy(lib_item, old_item);
				} else if (System.IO.Directory.Exists(lib_item)) {
					CopyDirectory(lib_item, old_item);
				}
			}
		}
	}

	static string GetLibraryPath() {
		string proj_dir = System.IO.Path.GetDirectoryName(Application.dataPath);
		return System.IO.Path.Combine(proj_dir, "Library");
	}

	static void ClearDirectory(string path) {
		if (!System.IO.Directory.Exists(path)) return;
		ClearDirectory(new DirectoryInfo(path));
	}
	static void ClearDirectory(DirectoryInfo directory) {
		foreach (FileInfo file in directory.GetFiles()) file.Delete();
		foreach (DirectoryInfo subDirectory in directory.GetDirectories()) subDirectory.Delete(true);
	}

	static void CopyDirectory(string source, string target) {
		if (!System.IO.Directory.Exists(source)) return;
		CopyDirectory(new DirectoryInfo(source), new DirectoryInfo(target));
	}
	static void CopyDirectory(DirectoryInfo source, DirectoryInfo target) {
		foreach (DirectoryInfo dir in source.GetDirectories()) CopyDirectory(dir, target.CreateSubdirectory(dir.Name));
		foreach (FileInfo file in source.GetFiles()) file.CopyTo(Path.Combine(target.FullName, file.Name));
	}

	public static bool EditorIsBusy {
		get { return EditorApplication.isCompiling | EditorApplication.isUpdating | EditorApplication.isPlaying | EditorApplication.isPaused; }
	}
}

public struct BuildTargetInfo {
	public string name {get; private set;}
	public string tooltip {get; private set;}
	public bool obsolete {get; private set;}
	public BuildTarget target {get; private set;}
	public BuildTargetGroup group {get; private set;}

	public BuildTargetInfo(BuildTarget target) {
		this.target = target;
		this.group = TargetToGroup(target);
		this.name = TargetToName(target);
		this.tooltip = TargetToTooltip(target);
		this.obsolete = IsObsolete(target);
		// WebPlayer is still an option in Unity 5.3.2, but will be deprecated soon
		this.obsolete |= (target == BuildTarget.WebPlayer) | (target == BuildTarget.WebPlayerStreamed);
	}

	public string extension {
		get {
			switch (target) {
			case BuildTarget.Android: return ".apk";
			case BuildTarget.Tizen: return ".tpk";
			case BuildTarget.iOS: return "";
			case BuildTarget.WebGL: return "";
			case BuildTarget.SamsungTV: return "";
			case BuildTarget.StandaloneLinux: return "";
			case BuildTarget.StandaloneLinux64: return "";
			case BuildTarget.StandaloneLinuxUniversal: return "";
			case BuildTarget.StandaloneOSXIntel: return ".app";
			case BuildTarget.StandaloneOSXIntel64: return ".app";
			case BuildTarget.StandaloneOSXUniversal: return ".app";
			case BuildTarget.StandaloneWindows: return ".exe";
			case BuildTarget.StandaloneWindows64: return ".exe";
			}
			throw new System.NotImplementedException("Build file extension for the platform <"+target+"> is not implemented yet");
		}
	}

	// ATTENTION: EditorUserBuildSettings.selectedBuildTargetGroup may not correlate with active build target
	public static BuildTargetGroup ActivePlatform {
		get { return TargetToGroup(EditorUserBuildSettings.activeBuildTarget); }
	}

	public static bool GroupIsValid(BuildTargetGroup group) {
		try {
			GroupToTarget(group);
			return true;
		} catch (System.ArgumentException) {
			return false;
		}
	}

	static bool IsObsolete(System.Enum value) {
		var fi = value.GetType().GetField(value.ToString());
		var attributes = fi.GetCustomAttributes(typeof(System.ObsoleteAttribute), false);
		return (attributes != null && attributes.Length > 0);
	}

	public static BuildTargetGroup TargetToGroup(BuildTarget target) {
		switch (target) {
		case BuildTarget.Android: return BuildTargetGroup.Android;
		case BuildTarget.BlackBerry: return BuildTargetGroup.BlackBerry;
		case BuildTarget.StandaloneGLESEmu: return BuildTargetGroup.GLESEmu;
		case BuildTarget.iOS: return BuildTargetGroup.iOS;
		case BuildTarget.Nintendo3DS: return BuildTargetGroup.Nintendo3DS;
		case BuildTarget.PS3: return BuildTargetGroup.PS3;
		case BuildTarget.PS4: return BuildTargetGroup.PS4;
		case BuildTarget.PSM: return BuildTargetGroup.PSM;
		case BuildTarget.PSP2: return BuildTargetGroup.PSP2;
		case BuildTarget.SamsungTV: return BuildTargetGroup.SamsungTV;
		case BuildTarget.Tizen: return BuildTargetGroup.Tizen;
		case BuildTarget.tvOS: return BuildTargetGroup.tvOS;
		case BuildTarget.WebGL: return BuildTargetGroup.WebGL;
		case BuildTarget.WebPlayer: return BuildTargetGroup.WebPlayer;
		case BuildTarget.WebPlayerStreamed: return BuildTargetGroup.WebPlayer;
		case BuildTarget.WiiU: return BuildTargetGroup.WiiU;
		case BuildTarget.WP8Player: return BuildTargetGroup.WP8;
		case BuildTarget.WSAPlayer: return BuildTargetGroup.WSA;
		case BuildTarget.XBOX360: return BuildTargetGroup.XBOX360;
		case BuildTarget.XboxOne: return BuildTargetGroup.XboxOne;
		}
		return BuildTargetGroup.Standalone;
	}
	public static BuildTarget GroupToTarget(BuildTargetGroup group) {
		switch (group) {
		case BuildTargetGroup.Android: return BuildTarget.Android;
		case BuildTargetGroup.BlackBerry: return BuildTarget.BlackBerry;
		case BuildTargetGroup.GLESEmu: return BuildTarget.StandaloneGLESEmu;
		case BuildTargetGroup.iOS: return BuildTarget.iOS;
		case BuildTargetGroup.Nintendo3DS: return BuildTarget.Nintendo3DS;
		case BuildTargetGroup.PS3: return BuildTarget.PS3;
		case BuildTargetGroup.PS4: return BuildTarget.PS4;
		case BuildTargetGroup.PSM: return BuildTarget.PSM;
		case BuildTargetGroup.PSP2: return BuildTarget.PSP2;
		case BuildTargetGroup.SamsungTV: return BuildTarget.SamsungTV;
		case BuildTargetGroup.Standalone: return EditorUserBuildSettings.selectedStandaloneTarget;
		case BuildTargetGroup.Tizen: return BuildTarget.Tizen;
		case BuildTargetGroup.tvOS: return BuildTarget.tvOS;
		case BuildTargetGroup.WebGL: return BuildTarget.WebGL;
		case BuildTargetGroup.WebPlayer: return (EditorUserBuildSettings.webPlayerStreamed ? BuildTarget.WebPlayerStreamed : BuildTarget.WebPlayer);
		case BuildTargetGroup.WiiU: return BuildTarget.WiiU;
		case BuildTargetGroup.WP8: return BuildTarget.WP8Player;
		case BuildTargetGroup.WSA: return BuildTarget.WSAPlayer;
		case BuildTargetGroup.XBOX360: return BuildTarget.XBOX360;
		case BuildTargetGroup.XboxOne: return BuildTarget.XboxOne;
		}
		throw new System.ArgumentException("Unknown platform: "+group);
	}
	public static string TargetToTooltip(BuildTarget target) {
		switch (target) {
		case BuildTarget.Android: return "Mobile (Android)"; // also Desktop?
		case BuildTarget.BlackBerry: return "Mobile (BlackBerry)";
		case BuildTarget.iOS: return "Mobile (Apple)";
		case BuildTarget.Nintendo3DS: return "Portable console (Nintendo)";
		case BuildTarget.PS3: return "Console (Sony)";
		case BuildTarget.PS4: return "Console (Sony)";
		case BuildTarget.PSM: return "Mobile & Portable console (Sony)";
		case BuildTarget.PSP2: return "Portable console (Sony)";
		case BuildTarget.SamsungTV: return "TV (Samsung)";
		case BuildTarget.StandaloneGLESEmu: return "Desktop";
		case BuildTarget.StandaloneLinux: return "Desktop (Ubuntu/Debian)";
		case BuildTarget.StandaloneLinux64: return "Desktop (Ubuntu/Debian)";
		case BuildTarget.StandaloneLinuxUniversal: return "Desktop (Ubuntu/Debian)";
		case BuildTarget.StandaloneOSXIntel: return "Desktop (Apple)";
		case BuildTarget.StandaloneOSXIntel64: return "Desktop (Apple)";
		case BuildTarget.StandaloneOSXUniversal: return "Desktop (Apple)";
		case BuildTarget.StandaloneWindows: return "Desktop (Microsoft)";
		case BuildTarget.StandaloneWindows64: return "Desktop (Microsoft)";
		case BuildTarget.Tizen: return "Mobile, TV, Desktop (Tizen)";
		case BuildTarget.tvOS: return "TV (Apple)";
		case BuildTarget.WebGL: return "Web";
		case BuildTarget.WebPlayer: return "Web (Unity)";
		case BuildTarget.WebPlayerStreamed: return "Web (Unity)";
		case BuildTarget.WiiU: return "Console (Nintendo)";
		case BuildTarget.WP8Player: return "Mobile (Microsoft)";
		case BuildTarget.WSAPlayer: return "Mobile, Desktop (Microsoft)";
		case BuildTarget.XBOX360: return "Console (Microsoft)";
		case BuildTarget.XboxOne: return "Console (Microsoft)";
		}
		return "";
	}
	public static string TargetToName(BuildTarget target) {
		switch (target) {
		case BuildTarget.Android: return "Android";
		case BuildTarget.BlackBerry: return "BlackBerry";
		case BuildTarget.iOS: return "iOS";
		case BuildTarget.Nintendo3DS: return "Nintendo 3DS";
		case BuildTarget.PS3: return "PlayStation 3";
		case BuildTarget.PS4: return "PlayStation 4";
		case BuildTarget.PSM: return "PlayStation Mobile";
		case BuildTarget.PSP2: return "PlayStation Vita"; // formerly known as "PlayStation Portable 2"
		case BuildTarget.SamsungTV: return "Samsung TV";
		case BuildTarget.StandaloneGLESEmu: return "GLES Emulation";
		case BuildTarget.StandaloneLinux: return "Linux x86";
		case BuildTarget.StandaloneLinux64: return "Linux 64";
		case BuildTarget.StandaloneLinuxUniversal: return "Linux x86+64";
		case BuildTarget.StandaloneOSXIntel: return "Mac OS X x86";
		case BuildTarget.StandaloneOSXIntel64: return "Mac OS X 64";
		case BuildTarget.StandaloneOSXUniversal: return "Mac OS X x86+64";
		case BuildTarget.StandaloneWindows: return "Windows x86";
		case BuildTarget.StandaloneWindows64: return "Windows x86+64";
		case BuildTarget.Tizen: return "Tizen";
		case BuildTarget.tvOS: return "tvOS";
		case BuildTarget.WebGL: return "WebGL";
		case BuildTarget.WebPlayer: return "WebPlayer";
		case BuildTarget.WebPlayerStreamed: return "WebPlayer (streamed)";
		case BuildTarget.WiiU: return "Wii U";
		case BuildTarget.WP8Player: return "Windows Phone";
		case BuildTarget.WSAPlayer: return "Windows Store";
		case BuildTarget.XBOX360: return "Xbox 360";
		case BuildTarget.XboxOne: return "Xbox One";
		}
		return target.ToString();
	}
}