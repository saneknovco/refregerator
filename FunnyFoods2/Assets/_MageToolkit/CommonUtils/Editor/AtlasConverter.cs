﻿using UnityEditor;
using UnityEngine;
using System.Collections.Generic;
using System.IO;
using System.Text.RegularExpressions;
using System.Linq;
using Bini.Utils;

public class AtlasConverter_Window : EditorWindow {
	public static AtlasConverter_Window window = null;
	
	static bool initialized {
		get { return (window != null); }
	}
	
	AtlasConverter_Window() {
		window = this;
	}
	
	// Static constructor is called if the class has [InitializeOnLoad] attribute
	// Or if window is opened as a part of default editor layout
	static AtlasConverter_Window() {
		Init(true);
	}
	
	[MenuItem("Window/Atlas Converter")]
	static void InitFromMenu() {
		Init(false);
	}
	static void Init(bool window_exists=false) {
		if (initialized) return;
		
		if (!window_exists) {
			// If GetWindow() is called from the static constructor
			// while the window is still open, Unity throws an error!
			window = GetWindow<AtlasConverter_Window>();
			window.title = "Atlas Convert";
			window.autoRepaintOnSceneChange = true; // maybe disable later
		}
		
		//SceneView.onSceneGUIDelegate += OnSceneGUI;
	}
	
	//Transform active_transform = null;

	bool process_scene = false;
	bool process_children = true;
	bool modify_source_assets = false;
	bool single_material = true;
	int max_atlas_size = 2048;
	int mipmaps_count = 0;
	bool make_alpha_tex = true;
	int alpha_tex_downsize = 1;

	void OnGUI() {
		if (!initialized) return;
		
		//if (active_transform != Selection.activeTransform) {
		//	active_transform = Selection.activeTransform;
		//}

		process_scene = EditorGUILayout.Toggle("Process whole scene", process_scene);
		EditorGUI.BeginDisabledGroup(process_scene);
		process_children = EditorGUILayout.Toggle("Process children", process_children);
		EditorGUI.EndDisabledGroup();

		EditorGUILayout.Space();

		modify_source_assets = EditorGUILayout.Toggle("Modify source assets", modify_source_assets);
		single_material = EditorGUILayout.Toggle("Single material", single_material);

		EditorGUILayout.Space();

		max_atlas_size = EditorGUILayout.IntField("Max atlas size", max_atlas_size);
		mipmaps_count = EditorGUILayout.IntField("Mipmaps", mipmaps_count);

		EditorGUILayout.Space();

		make_alpha_tex = EditorGUILayout.Toggle("Generate alpha-map", make_alpha_tex);
		EditorGUI.BeginDisabledGroup(!make_alpha_tex);
		alpha_tex_downsize = EditorGUILayout.IntField("Alpha-map downsize", alpha_tex_downsize);
		alpha_tex_downsize = Mathf.Max(alpha_tex_downsize, 0);
		EditorGUI.EndDisabledGroup();

		EditorGUILayout.Space();

		EditorGUILayout.BeginHorizontal();
		if (GUILayout.Button("Build")) Process_Main();
		if (GUILayout.Button("Revert")) Revert_Main();
		EditorGUILayout.EndHorizontal();
	}
	
	//static void OnSceneGUI(SceneView sceneview) {
	//	if (!initialized) return;
	//}
	
	// Called 100 times per second on all visible windows.
	//void Update() {
	//	if (!initialized) return;
	//}
	
	void OnDestroy() {
		if (!initialized) return;
		
		// Somewhy OnSceneGUI would still continue to be invoked
		//SceneView.onSceneGUIDelegate -= OnSceneGUI;
		
		window = null;
	}

	// =========================================================== //

	class AtlasTexture {
		public string name, final_name;
		public TextureImporterFormat format = TextureImporterFormat.AutomaticTruecolor;
		public TextureWrapMode wrap_mode = TextureWrapMode.Clamp;
		public Pixmap<Color32> pixmap;
		public int dx0 = 0, dy0 = 0, dx1 = 0, dy1 = 0; // margins
		public Pixmap<Color32> tex_pm; // final pixmap
		public Texture2D tex; // final texture
		public Rect rect; // cropped relative to final
		public Rect container_rect;
		public bool opaque_perimeter = false;
		public Texture2D container_tex;
		public Texture2D alpha_tex;

		public AtlasTexture() {
		}
		public AtlasTexture(AtlasTexture src) {
			format = src.format;
			wrap_mode = src.wrap_mode;
			pixmap = src.pixmap;
			dx0 = src.dx0;
			dy0 = src.dy0;
			dx1 = src.dx1;
			dy1 = src.dy1;
		}

		public Texture2D GetTex(int block_w, int block_h, int mip_size) {
			if (!tex) tex = GetTexPM(block_w, block_h, mip_size).ToTexture();
			return tex;
		}

		public Pixmap<Color32> GetTexPM(int block_w, int block_h, int mip_size) {
			if (tex_pm == null) {
				opaque_perimeter = ((dx0 == 0) & (dy0 == 0) & (dx1 == 0) & (dy1 == 0));
				if (opaque_perimeter) opaque_perimeter = pixmap.Scan((c) => (c.a == 255), false);

				dx0 = Mathf.Max(dx0, mip_size); dx1 = Mathf.Max(dx1, mip_size);
				dy0 = Mathf.Max(dy0, mip_size); dy1 = Mathf.Max(dy1, mip_size);

				int w = dx0+pixmap.w+dx1, h = dy0+pixmap.h+dy1;
				w = Mathf.CeilToInt(w / (float)block_w) * block_w;
				h = Mathf.CeilToInt(h / (float)block_h) * block_h;
				int dx01 = w - pixmap.w, dy01 = h - pixmap.h;
				dx0 = dx01 / 2; dx1 = dx01 - dx0;
				dy0 = dy01 / 2; dy1 = dy01 - dy0;

				var r = new IntRect(-dx0, -dy0, dx0+pixmap.w+dx1, dy0+pixmap.h+dy1);
				tex_pm = pixmap.Extract(r);
				FillWrap();

				float _w = 1f / r.width, _h = 1f / r.height;
				rect = new Rect(dx0*_w, dy0*_h, pixmap.w*_w, pixmap.h*_h);
			}
			return tex_pm;
		}

		void FillWrap() {
			int x0 = dx0, y0 = dy0, x1 = dx0+pixmap.w-1, y1 = dy0+pixmap.h-1;
			tex_pm.Apply((x, y, c) => {
				if ((x >= x0) & (x <= x1) & (y >= y0) & (y <= y1)) return c;
				if (wrap_mode == TextureWrapMode.Repeat) {
					if (x < x0) x = x1; else if (x > x1) x = x0;
					if (y < y0) y = y1; else if (y > y1) y = y0;
					c = tex_pm[x, y];
				} else {
					if (x < x0) x = x0; else if (x > x1) x = x1;
					if (y < y0) y = y0; else if (y > y1) y = y1;
					c = tex_pm[x, y];
					if (!opaque_perimeter) c.a = 0;
				}
				return c;
			});
		}

		// ================================================= //

		static List<AtlasTexture> cache_cropped = new List<AtlasTexture>();
		static Dictionary<Texture2D, AtlasTexture> cache_by_tex = new Dictionary<Texture2D, AtlasTexture>();
		static Dictionary<string, AtlasTexture> cache_by_path = new Dictionary<string, AtlasTexture>();

		public static void ClearCache() {
			cache_cropped.Clear();
			cache_by_tex.Clear();
			cache_by_path.Clear();
		}

		public static Dictionary<TextureImporterFormat, List<AtlasTexture>> TexturesByFormat() {
			var formats_texs = new Dictionary<TextureImporterFormat, List<AtlasTexture>>();
			foreach (var atlas_tex in cache_cropped) {
				List<AtlasTexture> format_texs;
				if (formats_texs.TryGetValue(atlas_tex.format, out format_texs)) {
					format_texs.Add(atlas_tex);
				} else {
					format_texs = new List<AtlasTexture>();
					format_texs.Add(atlas_tex);
					formats_texs.Add(atlas_tex.format, format_texs);
				}
			}
			return formats_texs;
		}

		static AtlasTexture Cached(Texture2D tex) {
			AtlasTexture atlas_tex = null;

			string path = AssetDatabase.GetAssetPath(tex);
			if (string.IsNullOrEmpty(path)) {
				if (cache_by_tex.TryGetValue(tex, out atlas_tex)) return atlas_tex;

				atlas_tex = new AtlasTexture();
				atlas_tex.pixmap = (Pixmap<Color32>)tex;
				atlas_tex.format = TextureImporterFormat.AutomaticTruecolor;
				atlas_tex.wrap_mode = tex.wrapMode;

				cache_by_tex.Add(tex, atlas_tex);
			} else {
				if (cache_by_path.TryGetValue(path, out atlas_tex)) return atlas_tex;

				var textureImporter = AssetImporter.GetAtPath(path) as TextureImporter;
				var readable = textureImporter.isReadable;
				var format = textureImporter.textureFormat;
				var npot = textureImporter.npotScale;
				var maxsize = textureImporter.maxTextureSize;

				bool not_truecolor = ((tex.format != TextureFormat.RGBA32) & (tex.format != TextureFormat.ARGB32) & (tex.format != TextureFormat.BGRA32));

				if ((!readable) | (npot != TextureImporterNPOTScale.None) | not_truecolor) {
					textureImporter.isReadable = true;
					textureImporter.textureFormat = TextureImporterFormat.AutomaticTruecolor;
					textureImporter.npotScale = TextureImporterNPOTScale.None;
					textureImporter.maxTextureSize = 8192;
					AssetDatabase.WriteImportSettingsIfDirty(path);
					AssetDatabase.ImportAsset(path);

					tex = AssetDatabase.LoadAssetAtPath(path, typeof(Texture2D)) as Texture2D;

					if (format == TextureImporterFormat.ARGB32) format = TextureImporterFormat.AutomaticTruecolor;
					if (format == TextureImporterFormat.RGBA32) format = TextureImporterFormat.AutomaticTruecolor;

					atlas_tex = new AtlasTexture();
					atlas_tex.pixmap = (Pixmap<Color32>)tex;
					//atlas_tex.format = (not_truecolor ? format : TextureImporterFormat.AutomaticTruecolor);
					atlas_tex.format = format;
					atlas_tex.wrap_mode = tex.wrapMode;

					textureImporter.isReadable = readable;
					textureImporter.textureFormat = format;
					textureImporter.npotScale = npot;
					textureImporter.maxTextureSize = maxsize;
					AssetDatabase.WriteImportSettingsIfDirty(path);
					AssetDatabase.ImportAsset(path);
				} else {
					format = TextureImporterFormat.AutomaticTruecolor;
					
					atlas_tex = new AtlasTexture();
					atlas_tex.pixmap = (Pixmap<Color32>)tex;
					atlas_tex.format = format;
					atlas_tex.wrap_mode = tex.wrapMode;
				}

				cache_by_path.Add(path, atlas_tex);
			}

			return atlas_tex;
		}
		
		public static AtlasTexture Get(Texture tex, ref Rect uv_rect) {
			var atlas_tex = new AtlasTexture(Cached(tex as Texture2D));

			// Get actually used part of the texture
			int w = atlas_tex.pixmap.width, h = atlas_tex.pixmap.height;
			uv_rect.x *= w; uv_rect.width *= w;
			uv_rect.y *= h; uv_rect.height *= h;
			int x0 = Mathf.Clamp(Mathf.FloorToInt(uv_rect.xMin), 0, w);
			int y0 = Mathf.Clamp(Mathf.FloorToInt(uv_rect.yMin), 0, h);
			int x1 = Mathf.Clamp(Mathf.CeilToInt(uv_rect.xMax), 0, w);
			int y1 = Mathf.Clamp(Mathf.CeilToInt(uv_rect.yMax), 0, h);
			w = x1 - x0; h = y1 - y0;

			var used_rect = new IntRect(x0, y0, w, h);
			atlas_tex.pixmap = atlas_tex.pixmap.Subregion(used_rect);
			uv_rect.center -= new Vector2(used_rect.xMin, used_rect.yMin);

			// Test for single-color
			Color32 single_color;
			if (IsSingleColor(atlas_tex.pixmap, out single_color)) {
				atlas_tex.pixmap = new Pixmap<Color32>(2, 2, single_color);
				uv_rect = new Rect(0.5f, 0.5f, 1f, 1f);
			}
			
			// Crop by alpha
			var crop_rect = atlas_tex.pixmap.CropRect((c) => {return c.a != 0;});
			atlas_tex.pixmap = atlas_tex.pixmap.Subregion(crop_rect);
			uv_rect.center -= new Vector2(crop_rect.xMin, crop_rect.yMin);

			// Update margins
			w = crop_rect.width; h = crop_rect.height;
			x0 = Mathf.FloorToInt(uv_rect.xMin);
			y0 = Mathf.FloorToInt(uv_rect.yMin);
			x1 = Mathf.CeilToInt(uv_rect.xMax);
			y1 = Mathf.CeilToInt(uv_rect.yMax);
			atlas_tex.dx0 = Mathf.Max(atlas_tex.dx0, Mathf.Max(-x0, 0));
			atlas_tex.dy0 = Mathf.Max(atlas_tex.dy0, Mathf.Max(-y0, 0));
			atlas_tex.dx1 = Mathf.Max(atlas_tex.dx1, Mathf.Max(x1-w, 0));
			atlas_tex.dy1 = Mathf.Max(atlas_tex.dy1, Mathf.Max(y1-h, 0));

			// Calculate new UV rect
			float _w = 1f / w, _h = 1f / h;
			uv_rect.x *= _w; uv_rect.width *= _w;
			uv_rect.y *= _h; uv_rect.height *= _h;

			// Test against already known atlas textures
			foreach (var _atlas_tex in cache_cropped) {
				if (!Compare(atlas_tex, _atlas_tex)) continue;
				// Update margins
				_atlas_tex.dx0 = Mathf.Max(_atlas_tex.dx0, atlas_tex.dx0);
				_atlas_tex.dy0 = Mathf.Max(_atlas_tex.dy0, atlas_tex.dy0);
				_atlas_tex.dx1 = Mathf.Max(_atlas_tex.dx1, atlas_tex.dx1);
				_atlas_tex.dy1 = Mathf.Max(_atlas_tex.dy1, atlas_tex.dy1);
				return _atlas_tex;
			}

			cache_cropped.Add(atlas_tex);

			return atlas_tex;
		}

		static bool IsSingleColor(Pixmap<Color32> pixmap, out Color32 c0) {
			c0 = pixmap.pixels[0];
			for (int i = 1; i < pixmap.count; i++) {
				if (!pixmap.pixels[i].SameAs(ref c0)) return false;
			}
			return true;
		}

		static bool Compare(AtlasTexture at0, AtlasTexture at1) {
			if (at0.format != at1.format) return false;
			var pm0 = at0.pixmap;
			var pm1 = at1.pixmap;
			if ((pm0.w != pm1.w) | (pm0.h != pm1.h)) return false;
			var pixels0 = pm0.pixels;
			var pixels1 = pm1.pixels;
			int n = pm0.w * pm0.h;
			for (int i = 0; i < n; i++) {
				var c0 = pixels0[i];
				var c1 = pixels1[i];
				if ((c0.a == 0) & (c1.a == 0)) continue;
				if ((c0.r != c1.r) | (c0.g != c1.g) | (c0.b != c1.b) | (c0.a != c1.a)) return false;
			}
			return true;
		}
	}

	class AtlasItem {
		public GameObject src_obj;
		public string name;
		public Mesh src_mesh;
		public Material src_material;
		public Rect src_uv;
		public Rect uv_rect;
		public AtlasTexture atlas_tex;

		public AtlasItem(GameObject gameObj, Mesh mesh, Material material, bool full_init=true) {
			src_obj = gameObj;
			name = Regex.Replace(gameObj.name.Trim(), @"\W", "_");
			src_mesh = mesh;
			src_material = material;
			if (full_init) {
				src_uv = MeshUtils.CalculateUVBounds(src_mesh, 0);
				uv_rect = src_uv;
				atlas_tex = AtlasTexture.Get(src_material.mainTexture, ref uv_rect);
				if (string.IsNullOrEmpty(atlas_tex.name)) atlas_tex.name = name;
			}
		}

		static Rect TransformRect(Rect child, Rect parent) {
			child.x *= parent.width;
			child.y *= parent.height;
			child.width *= parent.width;
			child.height *= parent.height;
			child.x += parent.x;
			child.y += parent.y;
			return child;
		}

		public Rect dst_uv {
			get { return TransformRect(TransformRect(uv_rect, atlas_tex.rect), atlas_tex.container_rect); }
		}
	}

	struct TextureFormatInfo {
		public bool POT, square;
		public int alpha;
		public int block_w, block_h;
		public int min_w, min_h;

		void Init(bool POT, bool square, int alpha, int block_w, int block_h, int min_w, int min_h) {
			this.POT = POT; this.square = square;
			this.alpha = alpha;
			this.block_w = block_w; this.block_h = block_h;
			this.min_w = min_w; this.min_h = min_h;
		}
		public TextureFormatInfo(bool POT, bool square, int alpha, int block_w, int block_h, int min_w, int min_h) {
			this.POT = POT; this.square = square;
			this.alpha = alpha;
			this.block_w = block_w; this.block_h = block_h;
			this.min_w = min_w; this.min_h = min_h;
		}
		public TextureFormatInfo(TextureImporterFormat format, bool mipmap) {
			// All fields must be assigned before calling using <this> object
			this.POT = false; this.square = false;
			this.alpha = 8;
			this.block_w = 1; this.block_h = 1;
			this.min_w = 1; this.min_h = 1;

			switch (format) {
			case TextureImporterFormat.AutomaticTruecolor: Init(false, false, 8, 1, 1, 1, 1); break;
			case TextureImporterFormat.RGBA32: Init(false, false, 8, 1, 1, 1, 1); break;
			case TextureImporterFormat.ARGB32: Init(false, false, 8, 1, 1, 1, 1); break;
			case TextureImporterFormat.RGB24: Init(false, false, 0, 1, 1, 1, 1); break;
			case TextureImporterFormat.Automatic16bit: Init(false, false, 4, 1, 1, 1, 1); break;
			case TextureImporterFormat.RGBA16: Init(false, false, 4, 1, 1, 1, 1); break;
			case TextureImporterFormat.ARGB16: Init(false, false, 4, 1, 1, 1, 1); break;
			case TextureImporterFormat.RGB16: Init(false, false, 0, 1, 1, 1, 1); break;
			case TextureImporterFormat.Alpha8: Init(false, false, 8, 1, 1, 1, 1); break;

			case TextureImporterFormat.AutomaticCompressed: Init(true, false, -1, 4, 4, 8, 8); break;
			case TextureImporterFormat.AutomaticCrunched: Init(true, false, -1, 4, 4, 8, 8); break;

			case TextureImporterFormat.DXT1: Init(mipmap, false, 0, 4, 4, 4, 4); break;
			case TextureImporterFormat.DXT1Crunched: Init(mipmap, false, 0, 4, 4, 4, 4); break;
			case TextureImporterFormat.DXT5: Init(mipmap, false, -1, 4, 4, 4, 4); break;
			case TextureImporterFormat.DXT5Crunched: Init(mipmap, false, -1, 4, 4, 4, 4); break;

			case TextureImporterFormat.ETC_RGB4: Init(true, false, 0, 4, 4, 4, 4); break;
			case TextureImporterFormat.ETC2_RGB4: Init(mipmap, false, 0, 4, 4, 4, 4); break;
			case TextureImporterFormat.ETC2_RGB4_PUNCHTHROUGH_ALPHA: Init(mipmap, false, -1, 4, 4, 4, 4); break;
			case TextureImporterFormat.ETC2_RGBA8: Init(mipmap, false, -1, 4, 4, 4, 4); break;
			case TextureImporterFormat.EAC_R: Init(mipmap, false, 0, 4, 4, 4, 4); break;
			case TextureImporterFormat.EAC_R_SIGNED: Init(mipmap, false, 0, 4, 4, 4, 4); break;
			case TextureImporterFormat.EAC_RG: Init(mipmap, false, 0, 4, 4, 4, 4); break;
			case TextureImporterFormat.EAC_RG_SIGNED: Init(mipmap, false, 0, 4, 4, 4, 4); break;

			case TextureImporterFormat.PVRTC_RGB2: Init(true, true, 0, 4, 4, 8, 8); break;
			case TextureImporterFormat.PVRTC_RGB4: Init(true, true, 0, 4, 4, 8, 8); break;
			case TextureImporterFormat.PVRTC_RGBA2: Init(true, true, -1, 4, 4, 8, 8); break;
			case TextureImporterFormat.PVRTC_RGBA4: Init(true, true, -1, 4, 4, 8, 8); break;

			case TextureImporterFormat.ATC_RGB4: Init(mipmap, false, 0, 4, 4, 4, 4); break;
			case TextureImporterFormat.ATC_RGBA8: Init(mipmap, false, -1, 4, 4, 4, 4); break;

			case TextureImporterFormat.ASTC_RGBA_4x4: Init(mipmap, false, -1, 1, 1, 1, 1); break;
			case TextureImporterFormat.ASTC_RGBA_5x5: Init(mipmap, false, -1, 1, 1, 1, 1); break;
			case TextureImporterFormat.ASTC_RGBA_6x6: Init(mipmap, false, -1, 1, 1, 1, 1); break;
			case TextureImporterFormat.ASTC_RGBA_8x8: Init(mipmap, false, -1, 1, 1, 1, 1); break;
			case TextureImporterFormat.ASTC_RGBA_10x10: Init(mipmap, false, -1, 1, 1, 1, 1); break;
			case TextureImporterFormat.ASTC_RGBA_12x12: Init(mipmap, false, -1, 1, 1, 1, 1); break;
			case TextureImporterFormat.ASTC_RGB_4x4: Init(mipmap, false, 0, 1, 1, 1, 1); break;
			case TextureImporterFormat.ASTC_RGB_5x5: Init(mipmap, false, 0, 1, 1, 1, 1); break;
			case TextureImporterFormat.ASTC_RGB_6x6: Init(mipmap, false, 0, 1, 1, 1, 1); break;
			case TextureImporterFormat.ASTC_RGB_8x8: Init(mipmap, false, 0, 1, 1, 1, 1); break;
			case TextureImporterFormat.ASTC_RGB_10x10: Init(mipmap, false, 0, 1, 1, 1, 1); break;
			case TextureImporterFormat.ASTC_RGB_12x12: Init(mipmap, false, 0, 1, 1, 1, 1); break;
			}
		}
	}

	List<AtlasItem> atlas_items = null;

	void Collect(bool full_init) {
		AtlasTexture.ClearCache();
		atlas_items = new List<AtlasItem>();

		if (process_scene) {
			foreach (var mesh_renderer in SceneObjects.Components<MeshRenderer>()) {
				AddRenderer(mesh_renderer, full_init);
			}
		} else {
			var selection_mode = (process_children ? SelectionMode.Deep : SelectionMode.Unfiltered);
			foreach (var tfm in Selection.GetTransforms(selection_mode)) {
				var mesh_renderer = tfm.GetComponent<MeshRenderer>();
				if (!mesh_renderer) continue;
				AddRenderer(mesh_renderer, full_init);
			}
		}
	}
	void AddRenderer(MeshRenderer mesh_renderer, bool full_init) {
        // Uni2D uses its own atlas system
#if !MAGE_NO_UNI2D
		if (mesh_renderer.GetComponent<Uni2DSprite>()) return;
#endif

		var mesh_filter = mesh_renderer.GetComponent<MeshFilter>();
		if (!mesh_filter) return;

		if (!mesh_filter.sharedMesh) return;
		if (!mesh_renderer.sharedMaterial) return;

		var tex = mesh_renderer.sharedMaterial.mainTexture;
		if (!tex) return;
		//if (tex.wrapMode == TextureWrapMode.Repeat) return; // TODO: should we ignore tiling textures?

		string path = AssetDatabase.GetAssetPath(tex);
		var textureImporter = AssetImporter.GetAtPath(path) as TextureImporter;
		if (!textureImporter) return; // ignore non-importable textures (e.g. built-in ones)
		
		var atlas_item = new AtlasItem(mesh_renderer.gameObject, mesh_filter.sharedMesh, mesh_renderer.sharedMaterial, full_init);
		atlas_items.Add(atlas_item);

		EditorUtility.DisplayProgressBar("Gathering objects...", atlas_items.Count+" objects", 0f);
	}

	void Cleanup() {
		atlas_items = null;
		AtlasTexture.ClearCache();
	}

	Texture2D MakeTextureAsset(string path, Texture2D tex, TextureImporterFormat format, bool POT, int max_size=8192, bool readable=false) {
		// Write the texture to file
		var pngData = tex.EncodeToPNG();
		FileUtils.EnsureAssetDir(path);
		File.WriteAllBytes(path, pngData);
		
		AssetDatabase.Refresh();

		max_size = Mathf.Clamp(max_size, 32, 8192); // Current Unity limits

		var textureImporter = AssetImporter.GetAtPath(path) as TextureImporter;
		textureImporter.textureType = TextureImporterType.Advanced;
		textureImporter.textureFormat = format;
		textureImporter.isReadable = readable;
		textureImporter.maxTextureSize = max_size;
		textureImporter.npotScale = (POT ? TextureImporterNPOTScale.ToNearest : TextureImporterNPOTScale.None);
		// Attention: mobile may not support trilinear filtering on NPOT textures.
		// A well enough working solution is to disable the mipmaps.
		// Mipmap bias seems to be ignored on mobile (or at least on my Android)
		textureImporter.mipmapEnabled = (mipmaps_count > 0);
		textureImporter.mipmapFilter = TextureImporterMipFilter.KaiserFilter;
		textureImporter.wrapMode = TextureWrapMode.Clamp;
		textureImporter.filterMode = FilterMode.Trilinear;
		AssetDatabase.WriteImportSettingsIfDirty(path);
		AssetDatabase.ImportAsset(path);

		return AssetDatabase.LoadAssetAtPath(path, typeof(Texture2D)) as Texture2D;
	}

	IEnumerable<Transform> ParentsIterator() {
		if (process_scene) return SceneObjects.Roots();
		return Selection.GetTransforms(SelectionMode.TopLevel);
	}

	void BuildAtlases(string scene_path) {
		var formats_texs = AtlasTexture.TexturesByFormat();

		string path = null;

		var parents_names = new List<string>();
		foreach (var tfm in ParentsIterator()) {
			parents_names.Add(Regex.Replace(tfm.name.Trim(), @"\W", "_"));
		}

		foreach (var format_texs in formats_texs) {
			var format = format_texs.Key;
			var atlas_texs = format_texs.Value;

			var tfi = new TextureFormatInfo(format, (mipmaps_count > 0));

			int mip_size = (1 << mipmaps_count);
			int block_w = tfi.block_w * mip_size;
			int block_h = tfi.block_h * mip_size;

			var pixmaps = atlas_texs.Select((atlas_tex, progress) => {
				EditorUtility.DisplayProgressBar("Building atlases...", format+", texture #"+progress, (progress+1)/(atlas_texs.Count+1f));
				return atlas_tex.GetTexPM(block_w, block_h, mip_size);
			}).ToArray();

			int pack_w = 0, pack_h = 0;
			var rects = TryPack(pixmaps, ref pack_w, ref pack_h, true, true);
			if (rects == null) continue;
			//Pack_POT_square(pixmaps, ref pack_w, ref pack_h, ref rects, tfi);

			var atlas_pm = new Pixmap<Color32>(pack_w, pack_h);
			for (int i = 0; i < pixmaps.Length; i++) {
				var pm = pixmaps[i];
				var r = rects[i];
				atlas_pm.Blit(pm, Mathf.RoundToInt(r.x), Mathf.RoundToInt(r.y));
				r.x = r.x / pack_w; r.width = r.width / pack_w;
				r.y = r.y / pack_h; r.height = r.height / pack_h;
				rects[i] = r;
			}

			atlas_pm = ResizeAtlasPixmap(atlas_pm, rects, tfi);

			string final_name = string.Join(",", parents_names.ToArray())+"("+format+")";
			path = System.IO.Path.Combine(scene_path, "Atlases/"+final_name+".png");
			var container_tex = MakeTextureAsset(path, atlas_pm.ToTexture(false), format, tfi.POT, max_atlas_size);

			Texture2D alpha_tex = null;
			if (make_alpha_tex) {
				int max_size = Mathf.Max(container_tex.width, container_tex.height);
				max_size = Mathf.Min(max_size, max_atlas_size);
				max_size = max_size / (1 << alpha_tex_downsize);
				if (!Mathf.IsPowerOfTwo(max_size)) max_size = Mathf.NextPowerOfTwo(max_size);
				//if (!Mathf.IsPowerOfTwo(max_size)) max_size = Mathf.ClosestPowerOfTwo(max_size);
				//max_size >>= alpha_tex_downsize;
				path = System.IO.Path.Combine(scene_path, "Atlases/"+final_name+"_Alpha.png");
				alpha_tex = MakeTextureAsset(path, atlas_pm.ToTexture(false), TextureImporterFormat.Alpha8, false, max_size);
			}

			for (int i = 0; i < atlas_texs.Count; i++) {
				var atlas_tex = atlas_texs[i];
				atlas_tex.container_tex = container_tex;
				atlas_tex.alpha_tex = alpha_tex;
				atlas_tex.final_name = final_name;
				atlas_tex.container_rect = rects[i];
			}

			EditorUtility.DisplayProgressBar("Building atlases...", format+" done", 1f);
		}

		int _progress = 0;
		foreach (var atlas_item in atlas_items) {
			var gameObj = atlas_item.src_obj;
			var mesh_filter = gameObj.GetComponent<MeshFilter>();
			var mesh_renderer = gameObj.GetComponent<MeshRenderer>();

			var mesh = Mesh.Instantiate(mesh_filter.sharedMesh);
			var src_uv = atlas_item.src_uv;
			var dst_uv = atlas_item.dst_uv;
			ModifyUV(mesh, src_uv, dst_uv);
			path = null;
			if (modify_source_assets) path = AssetDatabase.GetAssetPath(mesh_filter.sharedMesh);
			if (string.IsNullOrEmpty(path)) path = System.IO.Path.Combine(scene_path, "Atlases/"+atlas_item.name+".asset");
			mesh = FileUtils.SaveAsset(mesh, path, true);
			mesh_filter.sharedMesh = mesh;

			var material = Material.Instantiate(mesh_renderer.sharedMaterial);
			material.mainTexture = atlas_item.atlas_tex.container_tex;
			if (make_alpha_tex) {
				material.shader = Shader.Find("BiniUber/RGB+A/T/Blend: Alpha");
				material.SetTexture("_MainTex_Alpha", atlas_item.atlas_tex.alpha_tex);
			} else {
				material.shader = Shader.Find("BiniUber/RGBA/T/Blend: Alpha");
			}
			string mat_name = (single_material ? atlas_item.atlas_tex.final_name : atlas_item.name);
			path = null;
			if (modify_source_assets) path = AssetDatabase.GetAssetPath(mesh_renderer.sharedMaterial);
			if (string.IsNullOrEmpty(path)) path = System.IO.Path.Combine(scene_path, "Atlases/"+mat_name+".mat");
			material = FileUtils.SaveAsset(material, path, true);
			mesh_renderer.sharedMaterial = material;

			_progress++;
			EditorUtility.DisplayProgressBar("Updating objects...", gameObj.name, _progress/(atlas_items.Count+1f));
		}

		EditorUtility.DisplayProgressBar("Updating objects...", "", 1f);
	}

	void Pack_POT_square(Pixmap<Color32>[] pixmaps, ref int pack_w, ref int pack_h, ref Rect[] rects, TextureFormatInfo tfi) {
		if (!tfi.POT) return;

		int w0 = FloorPow2(pack_w), w1 = CeilPow2(pack_w);
		int h0 = FloorPow2(pack_h), h1 = CeilPow2(pack_h);

		bool w_pow2 = (pack_w == w0), h_pow2 = (pack_h == h0);
		bool is_square = (pack_w == pack_h);

		if ((w_pow2 & h_pow2) & (is_square | !tfi.square)) return;

		Rect[] rects00 = null, rects10 = null, rects01 = null, rects11 = null;
		if (tfi.square) {
			w0 = h0 = Mathf.Max(w0, h0);
			w1 = h1 = Mathf.Max(w1, h1);
			rects00 = TryPack(pixmaps, ref w0, ref h0, false);
			rects11 = TryPack(pixmaps, ref w1, ref h1, false);
		} else {
			rects00 = TryPack(pixmaps, ref w0, ref h0, false);
			rects10 = TryPack(pixmaps, ref w1, ref h0, false);
			rects01 = TryPack(pixmaps, ref w0, ref h1, false);
			rects11 = TryPack(pixmaps, ref w1, ref h1, false);
		}

		if (rects00 != null) { rects = rects00; pack_w = w0; pack_h = h0; return; }

		if ((rects10 != null) & (rects01 != null)) {
			if (w1*h0 <= w0*h1) {
				rects = rects10; pack_w = w1; pack_h = h0; return;
			} else {
				rects = rects01; pack_w = w0; pack_h = h1; return;
			}
		} else {
			if (rects10 != null) { rects = rects10; pack_w = w1; pack_h = h0; return; }
			if (rects01 != null) { rects = rects01; pack_w = w0; pack_h = h1; return; }
		}

		if (rects11 != null) { rects = rects11; pack_w = w1; pack_h = h1; return; }
	}

	int FloorPow2(int v) {
		if (Mathf.IsPowerOfTwo(v)) { return v; } else { return Mathf.NextPowerOfTwo(v) >> 1; }
	}
	int CeilPow2(int v) {
		if (Mathf.IsPowerOfTwo(v)) { return v; } else { return Mathf.NextPowerOfTwo(v); }
	}

	Rect[] TryPack(Pixmap<Color32>[] pixmaps, ref int pack_w, ref int pack_h, bool grow_mode, bool log_error=false) {
		try {
			return TreeBinPacker.Pack(IterPixmapSizes(pixmaps), ref pack_w, ref pack_h, grow_mode);
		} catch (TreeBinPacker.PackerException exc) {
			if (log_error) Debug.LogError(exc.Message);
			return null;
		}
	}

	IEnumerable<Vector2> IterPixmapSizes(Pixmap<Color32>[] pixmaps) {
		for (int i = 0; i < pixmaps.Length; i++) {
			yield return new Vector2(pixmaps[i].w, pixmaps[i].h);
		}
	}

	Pixmap<Color32> ResizeAtlasPixmap(Pixmap<Color32> pixmap, Rect[] rects, TextureFormatInfo tfi) {
		if ((pixmap.w < tfi.min_w) | (pixmap.h < tfi.min_h)) {
			var src_size = new Vector2(pixmap.width, pixmap.height);
			var crop_rect = new IntRect(0, 0, pixmap.w, pixmap.h);
			crop_rect.w = Mathf.Max(crop_rect.w, tfi.min_w);
			crop_rect.h = Mathf.Max(crop_rect.h, tfi.min_h);
			var offset = new Vector2(crop_rect.x, crop_rect.y);
			pixmap = pixmap.Extract(crop_rect, Color.clear);
			var dst_size = new Vector2(pixmap.width, pixmap.height);
			ModifyUVRects(rects, src_size, dst_size, offset);
		}
		return pixmap;
	}

	void ModifyUVRects(Rect[] rects, Vector2 src_size, Vector2 dst_size, Vector2 offset=default(Vector2)) {
		for (int i = 0; i < rects.Length; i++) {
			var r = rects[i];
			r.x = ((r.x * src_size.x) - offset.x) / dst_size.x;
			r.y = ((r.y * src_size.y) - offset.y) / dst_size.y;
			r.width = (r.width * src_size.x) / dst_size.x;
			r.height = (r.height * src_size.y) / dst_size.y;
			rects[i] = r;
		}
	}

	void ModifyUV(Mesh mesh, Rect src_uv, Rect dst_uv) {
		var uvs = mesh.uv;
		for (int i = 0; i < uvs.Length; i++) {
			var uv = uvs[i];
			uv.x = ((uv.x - src_uv.x) / src_uv.width) * dst_uv.width + dst_uv.x;
			uv.y = ((uv.y - src_uv.y) / src_uv.height) * dst_uv.height + dst_uv.y;
			uvs[i] = uv;
		}
		mesh.uv = uvs;
		mesh.UploadMeshData(false);
	}

	// TODO: figure out how to apply modifications to prefabs

	string GetScenePath(string err_msg) {
		string scene_path = EditorApplication.currentScene;
		if (string.IsNullOrEmpty(scene_path)) {
			EditorUtility.DisplayDialog(err_msg, "Scene has no file path.", "Cancel");
			return null;
		}
		return System.IO.Path.GetDirectoryName(scene_path);
	}

	void Process_Main() {
		string scene_path = GetScenePath("Cannot build atlases");
		if (string.IsNullOrEmpty(scene_path)) return;

		Collect(true);
		BuildAtlases(scene_path);
		Cleanup();

		EditorUtility.ClearProgressBar();
	}

	void Revert_Main() {
		string scene_path = GetScenePath("Cannot revert atlases");
		if (string.IsNullOrEmpty(scene_path)) return;

		Collect(false);
		RevertAtlases(scene_path);
		Cleanup();
		
		EditorUtility.ClearProgressBar();
	}

	static string[] asset_suffixes = new string[]{"_mat", "_mesh", "_CTRL"};
	List<KeyValuePair<string, string>> ParseAssetPaths(string asset_type, string scene_path, params string[] dir_names) {
		var asset_paths = new List<KeyValuePair<string, string>>();
		foreach (string dir_name in dir_names) {
			string dir_path = System.IO.Path.Combine(scene_path, dir_name);
			if (!System.IO.Directory.Exists(dir_path)) continue;
			string[] guids = AssetDatabase.FindAssets("t:"+asset_type, new string[]{dir_path});
			foreach (string guid in guids) {
				string asset_path = AssetDatabase.GUIDToAssetPath(guid);
				string asset_name = System.IO.Path.GetFileNameWithoutExtension(asset_path);
				foreach (string asset_suffix in asset_suffixes) {
					if (asset_name.EndsWith(asset_suffix)) asset_name = asset_name.Substring(0, asset_name.Length - asset_suffix.Length);
				}
				asset_paths.Add(new KeyValuePair<string, string>(asset_name, asset_path));
			}
		}
		asset_paths.Sort((itemA, itemB) => { return itemA.Key.Length.CompareTo(itemB.Key.Length); });
		return asset_paths;
	}
	T LoadAsset<T>(string name, List<KeyValuePair<string, string>> asset_paths) where T : UnityEngine.Object {
		foreach (var kv in asset_paths) {
			if (kv.Key != name) continue;
			AssetDatabase.ImportAsset(kv.Value);
			return AssetDatabase.LoadAssetAtPath(kv.Value, typeof(T)) as T;
		}
		return null;
	}

	void RevertAtlases(string scene_path) {
		var material_paths = ParseAssetPaths("Material", scene_path, "Materials", "Textures/Materials");
		var mesh_paths = ParseAssetPaths("Mesh", scene_path, "Meshes", "Materials/SpriteMeshes", "Textures/Materials/SpriteMeshes");

		int _progress = 0;
		foreach (var atlas_item in atlas_items) {
			var gameObj = atlas_item.src_obj;

			var mesh = LoadAsset<Mesh>(gameObj.name, mesh_paths);
			if (mesh == null) Debug.LogError("Cannot revert "+gameObj.name+": mesh not found");

			var material = LoadAsset<Material>(gameObj.name, material_paths);
			if (material == null) Debug.LogError("Cannot revert "+gameObj.name+": material not found");

			if ((mesh == null) || (material == null)) continue;

			var mesh_filter = gameObj.GetComponent<MeshFilter>();
			mesh_filter.sharedMesh = mesh;

			var mesh_renderer = gameObj.GetComponent<MeshRenderer>();
			mesh_renderer.sharedMaterial = material;
			
			_progress++;
			EditorUtility.DisplayProgressBar("Updating objects...", gameObj.name, _progress/(atlas_items.Count+1f));
		}
		
		EditorUtility.DisplayProgressBar("Updating objects...", "", 1f);
	}

	// Converted from http://jwezorek.com/2013/01/sprite-packing-in-python/ (by Joe Wezorek)
	// Which, in turn, based on http://codeincomplete.com/posts/2011/5/7/bin_packing/ (by Jake Gordon)
	static class TreeBinPacker {
		public class PackerException : System.Exception {
			public PackerException(string message) : base("Rectangle packer: "+message) {}
		}

		struct rectangle {
			public int x, y, w, h;
			public rectangle(int x=0, int y=0, int w=0, int h=0) {
				this.x = x; this.y = y; this.w = w; this.h = h;
			}
			public void split_vert(int y, out rectangle top, out rectangle bottom) {
				top = new rectangle(this.x, this.y, this.w, y);
				bottom = new rectangle(this.x, this.y+y, this.w, this.h-y);
			}
			public void split_horz(int x, out rectangle left, out rectangle right) {
				left = new rectangle(this.x, this.y, x, this.h);
				right = new rectangle(this.x+x, this.y, this.w-x, this.h);
			}
			public int area() {
				return this.w * this.h;
			}
			public int max_side() {
				return Mathf.Max(this.w, this.h);
			}
			public bool can_contain(int w, int h) {
				return (this.w >= w) & (this.h >= h);
			}
			public bool is_congruent_with(int w, int h) {
				return (this.w == w) & (this.h == h);
			}
			public bool should_split_vertically(int w, int h) {
				if (this.w == w) return true;
				else if (this.h == h) return false;
				//TODO: come up with a better heuristic
				rectangle top, bottom, left, right;
				this.split_vert(h, out top, out bottom);
				this.split_horz(h, out left, out right);
				return bottom.area() > right.area();
			}
			public bool should_grow_vertically(int w, int h) {
				bool can_grow_vert = (this.w >= w);
				bool can_grow_horz = (this.h >= h);
				if (!can_grow_vert & !can_grow_horz) throw new PackerException("Unable to grow!");
				if (can_grow_vert & !can_grow_horz) return true;
				if (can_grow_horz & !can_grow_vert) return false;
				//return (this.h + h < this.w + w); // original
				return ((this.h + h)*this.w < (this.w + w)*this.h); // modified
			}
			public rectangle copy() {
				return new rectangle(x, y, w, h);
			}
			public void grow(int w, int h) {
				this.w += w; this.h += h;
			}
		}

		class rect_node {
			public int img_id = -1;
			public rectangle rect;
			public rect_node child0=null, child1=null;

			public rect_node(int img_id, rectangle rect, rect_node child0=null, rect_node child1=null) {
				this.img_id = img_id;
				this.rect = rect;
				this.child0 = child0;
				this.child1 = child1;
			}
			public rect_node clone() {
				var _child0 = (this.child0 != null ? this.child0.clone() : null);
				var _child1 = (this.child1 != null ? this.child1.clone() : null);
				return new rect_node(this.img_id, this.rect.copy(), _child0, _child1);
			}
			public bool is_leaf() {
				return (this.child0 == null) & (this.child1 == null);
			}
			public bool is_empty_leaf() {
				return this.is_leaf() & (this.img_id < 0);
			}
			public void split_node(int img_w, int img_h, int img_id) {
				if (!this.is_leaf()) throw new PackerException("Attempted to split non-leaf");
				if (!this.rect.can_contain(img_w, img_h)) throw new PackerException("Attempted to place an img in a node it doesn't fit");
				if (this.rect.is_congruent_with(img_w, img_h)) {
					this.img_id = img_id; // fits exactly -> we are done
				} else {
					rectangle rect0, rect1;
					if (this.rect.should_split_vertically(img_w, img_h)) {
						this.rect.split_vert(img_h, out rect0, out rect1);
					} else {
						this.rect.split_horz(img_w, out rect0, out rect1);
					}
					this.child0 = new rect_node(-1, rect0);
					this.child1 = new rect_node(-1, rect1);
					this.child0.split_node(img_w, img_h, img_id);
				}
			}
			public void grow_node(int img_w, int img_h, int img_id) {
				if (this.is_empty_leaf()) throw new PackerException("Attempted to grow an empty leaf");
				rect_node _child0, _child1;
				_child0 = this.clone();
				if (this.rect.should_grow_vertically(img_w, img_h)) {
					_child1 = new rect_node(-1, new rectangle(this.rect.x, this.rect.y+this.rect.h, this.rect.w, img_h));
					this.rect.grow(0, img_h);
				} else {
					_child1 = new rect_node(-1, new rectangle(this.rect.x+this.rect.w, this.rect.y, img_w, this.rect.h));
					this.rect.grow(img_w, 0);
				}
				this.img_id = -1;
				this.child0 = _child0;
				this.child1 = _child1;
				this.child1.split_node(img_w, img_h, img_id);
			}
			public rect_node find_empty_leaf(int img_w, int img_h) {
				if (this.is_empty_leaf()) {
					return (this.rect.can_contain(img_w, img_h) ? this : null);
				} else {
					if (this.is_leaf()) return null;
					var leaf = this.child0.find_empty_leaf(img_w, img_h);
					return (leaf != null ? leaf : this.child1.find_empty_leaf(img_w, img_h));
				}
			}
			public void walk(System.Action<int, rectangle> callback) {
				if (this.is_leaf()) {
					if (this.img_id >= 0) callback(this.img_id, this.rect);
				} else {
					this.child0.walk(callback);
					this.child1.walk(callback);
				}
			}
		}

		public static Rect[] Pack(IEnumerable<Vector2> sizes, out int w, out int h) {
			w = 0; h = 0;
			return Pack(sizes, ref w, ref h, true);
		}
		public static Rect[] Pack(IEnumerable<Vector2> sizes, int w, int h) {
			return Pack(sizes, ref w, ref h, false);
		}
		public static Rect[] Pack(IEnumerable<Vector2> sizes, ref int w, ref int h, bool grow_mode) {
			var img_infos = new List<KeyValuePair<int, Vector2>>();
			foreach (Vector2 size in sizes) {
				img_infos.Add(new KeyValuePair<int, Vector2>(img_infos.Count, size));
			}
			img_infos.Sort((itemA, itemB) => { return -SizeAreaCmp(itemA.Value, itemB.Value); }); // sort by area (secondary key)
			img_infos.Sort((itemA, itemB) => { return -SizeMaxCmp(itemA.Value, itemB.Value); }); // sort by max dimension (primary key)

			rect_node root = null;
			foreach (var img_info in img_infos) {
				int index = img_info.Key;
				int img_w = Mathf.RoundToInt(img_info.Value.x), img_h = Mathf.RoundToInt(img_info.Value.y);

				if (root == null) {
					if (grow_mode) {
						root = new rect_node(-1, new rectangle(0, 0, img_w, img_h));
					} else {
						root = new rect_node(-1, new rectangle(0, 0, w, h));
					}
					root.split_node(img_w, img_h, index);
					continue;
				}

				var leaf = root.find_empty_leaf(img_w, img_h);
				if (leaf != null) {
					leaf.split_node(img_w, img_h, index);
				} else {
					if (grow_mode) {
						root.grow_node(img_w, img_h, index);
					} else {
						throw new PackerException("Images do not fit into a "+w+"x"+h+" rectangle");
					}
				}
			}

			if (grow_mode) { w = root.rect.w; h = root.rect.h; }

			var result = new Rect[img_infos.Count];
			root.walk((i, r) => { result[i] = new Rect(r.x, r.y, r.w, r.h); });
			return result;
		}
		static int SizeAreaCmp(Vector2 vA, Vector2 vB) {
			return (vA.x * vA.y).CompareTo(vB.x * vB.y);
		}
		static int SizeMaxCmp(Vector2 vA, Vector2 vB) {
			return Mathf.Max(vA.x, vA.y).CompareTo(Mathf.Max(vB.x, vB.y));
		}
	}
}
