using UnityEditor;
using UnityEngine;
using System.Collections.Generic;
using Bini.Utils;

[CustomEditor(typeof(Looker))]
public class LookerEditor : Editor {
	Looker looker;

	void OnEnable() {
		looker = target as Looker;
	}

	void OnSceneGUI() {
		EditorGUI.BeginChangeCheck();

		var tfm = looker.transform;
		var parent = tfm.parent;
		var initial_pos = tfm.localPosition + ((Vector3)looker.initialOffset);
		Vector2 axisX = looker.axisX, axisY = looker.axisY, zero_pos_offset = Vector2.zero;
		float reactionRange = looker.reactionRange;

		zero_pos_offset = DoHandle(parent, initial_pos, zero_pos_offset);

		axisX = DoHandle(parent, initial_pos, axisX);
		axisX = -DoHandle(parent, initial_pos, -axisX);
		
		axisY = DoHandle(parent, initial_pos, axisY);
		axisY = -DoHandle(parent, initial_pos, -axisY);

		var r0 = Quaternion.FromToRotation(Vector3.left, (Vector3)(axisX.normalized+axisY.normalized));
		reactionRange = DoRadiusHandle(parent, initial_pos, reactionRange, r0);

		if (EditorGUI.EndChangeCheck()) {
			Undo.RecordObject(target, "Move looker handles");
			looker.axisX = axisX;
			looker.axisY = axisY;
			looker.initialOffset += zero_pos_offset;
			looker.reactionRange = reactionRange;
			EditorUtility.SetDirty(target);
		}
	}
	
	static Vector2 DoHandle(Transform parent, Vector3 initial_pos, Vector2 handle_pos) {
		Vector3 p0 = initial_pos + ((Vector3)handle_pos);
		if (parent != null) p0 = parent.TransformPoint(p0);
		
		Handles.color = Color.red;
		Vector3 p1 = Handles.FreeMoveHandle(p0, Quaternion.identity,
			0.06f*HandleUtility.GetHandleSize(p0), Vector3.zero, Handles.RectangleCap);
		
		if ((p1 - p0).sqrMagnitude > float.Epsilon) {
			if (parent != null) p1 = parent.InverseTransformPoint(p1);
			p1 -= initial_pos;
			
			handle_pos.x = p1.x;
			handle_pos.y = p1.y;
		}
		
		return handle_pos;
	}

	static float DoRadiusHandle(Transform parent, Vector3 initial_pos, float radius, Quaternion r0) {
		Vector3 p0 = initial_pos;
		if (parent != null) p0 = parent.TransformPoint(p0);
		
		float handle_size = HandleUtility.GetHandleSize(p0);
		//Handles.color = Color.black;
		//Handles.DrawWireDisc(p0, Vector3.back, radius * ((handle_size-1.0f)/handle_size));
		Handles.color = Color.white;
		float radius1 = Handles.RadiusHandle(r0, p0, radius);
		
		if (Mathf.Abs(radius1 - radius) > float.Epsilon) radius = radius1;
		
		return radius;
	}
}
