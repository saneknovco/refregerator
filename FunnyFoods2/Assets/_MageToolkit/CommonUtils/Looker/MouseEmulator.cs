﻿using UnityEngine;
using System.Collections.Generic;
using Bini.Utils;

[MonoBehaviourSingleton(OnlyActive=false, AutoCreate=true)]
public class MouseEmulator : MonoBehaviourSingleton<MouseEmulator> {
	public bool UserInput = true;

	public bool Button0 = false;
	public bool Button1 = false;
	public bool Button2 = false;

	public int Button0_Delta = 0;
	public int Button1_Delta = 0;
	public int Button2_Delta = 0;

	float update_time = -1;
	void Update() {
		if (!UserInput) return;
		if (Time.time == update_time) return;
		update_time = Time.time;
		int _btn0 = Button0 ? 1 : 0;
		int _btn1 = Button1 ? 1 : 0;
		int _btn2 = Button2 ? 1 : 0;
		int btn0 = InputMT.GetMouseButton(0) ? 1 : 0;
		int btn1 = InputMT.GetMouseButton(1) ? 1 : 0;
		int btn2 = InputMT.GetMouseButton(2) ? 1 : 0;
		Button0_Delta = btn0 - _btn0;
		Button1_Delta = btn1 - _btn1;
		Button2_Delta = btn2 - _btn2;
		Button0 = (btn0 != 0);
		Button1 = (btn1 != 0);
		Button2 = (btn1 != 0);
		if (Button0 || Button1) {
			transform.position = ActualMousePos3D;
		}
	}

	public bool GetButton(int id) {
		Update();
		if (id == 0) return Button0;
		if (id == 1) return Button1;
		if (id == 2) return Button2;
		return false;
	}
	public void SetButton(int id, bool v) {
		if (id == 0) Button0 = v;
		if (id == 1) Button1 = v;
		if (id == 2) Button2 = v;
	}

	public bool GetButtonDown(int id) {
		Update();
		if (id == 0) return (Button0_Delta > 0);
		if (id == 1) return (Button1_Delta > 0);
		if (id == 2) return (Button2_Delta > 0);
		return false;
	}
	public bool GetButtonUp(int id) {
		Update();
		if (id == 0) return (Button0_Delta < 0);
		if (id == 1) return (Button1_Delta < 0);
		if (id == 2) return (Button2_Delta < 0);
		return false;
	}
	public void SetButtonDelta(int id, int v) {
		if (id == 0) Button0_Delta = System.Math.Sign(v);
		if (id == 1) Button0_Delta = System.Math.Sign(v);
		if (id == 2) Button0_Delta = System.Math.Sign(v);
	}

	public Vector3 GetPos() {
		Update();
		return transform.position;
	}
	public void SetPos(Vector3 p) {
		transform.position = p;
	}

	public static Vector3 ActualMousePos3D {
		get { return Camera.main.ScreenToWorldPoint(InputMT.mousePosition); }
	}

	// Animation events
	void ButtonOn(int id) {
		if (id == 0) Button0 = true;
		if (id == 1) Button1 = true;
		if (id == 2) Button2 = true;
	}
	void ButtonOff(int id) {
		if (id == 0) Button0 = false;
		if (id == 1) Button1 = false;
		if (id == 2) Button2 = false;
	}
}
