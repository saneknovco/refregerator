#if UNITY_EDITOR
using UnityEditor;
#endif
using UnityEngine;
using System.Collections.Generic;
using Bini.Utils;

public class Looker : MonoBehaviour {
	public float reactionRange = 100f; // in absolute (world) units
	public Vector2 initialOffset = Vector2.zero;
	public Vector2 axisX = Vector2.right;
	public Vector2 axisY = Vector2.up;
	public AnimationCurve offsetDependency;

	public enum LookModes {
		Mouse, Object, MouseAndObject
	}
	public LookModes lookMode = LookModes.Mouse;
	bool considerMouse { get { return lookMode != LookModes.Object; } }
	bool considerObject { get { return lookMode != LookModes.Mouse; } }

	public Transform lookObject = null;
	public Vector3 lookPosition = Vector3.zero;
	public float lookLag = 5f;

	public bool UseMouseEmulator = true;
	MouseEmulator mouse_emulator = null;

	public float residualTime = 2f;
	float last_observation_time = -1f;

	Vector3 last_target_pos;
	Vector3 starting_pos;
	Vector3 zero_pos;
	Matrix4x4 axes_matrix;
	Matrix4x4 axes_matrix_inv;
	Vector3 prev_p;

	void Start() {
		starting_pos = transform.localPosition;
		zero_pos = starting_pos + new Vector3(initialOffset.x, initialOffset.y, 0);
		last_target_pos = starting_pos;
		prev_p = starting_pos;

		EnsureOffsetDependency();

		CalcMatrices();
	}

	void EnsureOffsetDependency() {
		if ((offsetDependency == null) || (offsetDependency.length == 0)) {
			offsetDependency = new AnimationCurve(new Keyframe[]{
				new Keyframe(0, 0, 0, 0), new Keyframe(1, 1, 0, 0)
			});
		}
	}

	static Vector3 TransformPoint(Transform tfm, Vector3 v) {
		return (tfm == null) ? v : tfm.localToWorldMatrix.MultiplyPoint3x4(v);
	}
	static Vector3 TransformDirection(Transform tfm, Vector3 v) {
		return (tfm == null) ? v : tfm.localToWorldMatrix.MultiplyVector(v);
	}
	static Vector3 InverseTransformPoint(Transform tfm, Vector3 v) {
		return (tfm == null) ? v : tfm.worldToLocalMatrix.MultiplyPoint3x4(v);
	}
	static Vector3 InverseTransformDirection(Transform tfm, Vector3 v) {
		return (tfm == null) ? v : tfm.worldToLocalMatrix.MultiplyVector(v);
	}

	Vector3 CurrentTargetPos() {
		bool mouse_pressed = false, obj_exists = false;
		Vector3 mouse_pos = Vector3.zero, obj_pos = Vector3.zero, curr_p = Vector3.zero;

		ShouldReturnToStartingPos = false;

		if (considerObject) {
			obj_exists = (lookObject != null);
			obj_pos = TransformPoint(lookObject, lookPosition);
		}
		if (considerMouse) {
			if (UseMouseEmulator) {
				if (mouse_emulator == null) mouse_emulator = MouseEmulator.Instance;
				mouse_pressed = mouse_emulator.GetButton(0);
				mouse_pos = mouse_emulator.transform.position;
			} else {
				mouse_pressed = Input.GetMouseButton(0);
				mouse_pos = Camera.main.ScreenToWorldPoint(Input.mousePosition);
			}
		}

		if (mouse_pressed) {
			curr_p = InverseTransformPoint(transform.parent, mouse_pos);
			last_observation_time = Time.time;
		} else if (obj_exists) {
			curr_p = InverseTransformPoint(transform.parent, obj_pos);
			last_observation_time = Time.time;
		} else {
			if ((Time.time > last_observation_time+residualTime) || (last_observation_time < 0)) {
				curr_p = starting_pos;
				ShouldReturnToStartingPos = true;
			} else {
				curr_p = last_target_pos;
			}
		}

		last_target_pos = curr_p;

		return curr_p;
	}

	bool ShouldReturnToStartingPos = false;

	void Update() {
		if (Application.isEditor) CalcMatrices(); // in case of playtest modifications
		
		var tfm = transform;
		var parent = tfm.parent;
		
		var delta = CurrentTargetPos() - zero_pos;
		delta.z = 0;

		if (!ShouldReturnToStartingPos) {
			var delta_abs = TransformDirection(parent, delta);
			float t = Mathf.Clamp01(delta_abs.magnitude/reactionRange);
			float offset = offsetDependency.Evaluate(t);
			
			delta = axes_matrix_inv.MultiplyVector(delta.normalized*t);
			
			delta = axes_matrix.MultiplyVector(delta.normalized*offset);
		}

		var curr_p = zero_pos + delta;
		curr_p = (prev_p*lookLag + curr_p)/(1f+lookLag);
		
		prev_p = curr_p;

		tfm.localPosition = curr_p;
	}
	
	void CalcMatrices() {
		axes_matrix = Matrix4x4.identity;
		axes_matrix.SetColumn(0, new Vector4(axisX.x, axisX.y, 0, 0));
		axes_matrix.SetColumn(1, new Vector4(axisY.x, axisY.y, 0, 0));
		axes_matrix_inv = axes_matrix.inverse;
	}
	
#if UNITY_EDITOR
	[DrawGizmo(GizmoType.Active)]
	static void RenderCustomGizmo(Transform tfm, GizmoType gizmoType) {
		var looker = tfm.GetComponent<Looker>();
		if (looker == null) return;

		var parent = tfm.parent;
		var initial_pos = tfm.localPosition + (Vector3)looker.initialOffset;
		var initial_pos_abs = TransformPoint(parent, initial_pos);
		looker.CalcMatrices();
		var axes_matrix = looker.axes_matrix;
		
		Gizmos.color = new Color(1, 1, 0, 0.5f);

		Vector3 axisX = (Vector3)looker.axisX, axisY = (Vector3)looker.axisY;

		var p_prev = TransformPoint(parent, -axisX + initial_pos);
		var p_curr = TransformPoint(parent, axisX + initial_pos);
		Gizmos.DrawLine(p_prev, p_curr);
		
		p_prev = TransformPoint(parent, -axisY + initial_pos);
		p_curr = TransformPoint(parent, axisY + initial_pos);
		Gizmos.DrawLine(p_prev, p_curr);
		
		int n = 32;
		float angle_factor = Mathf.PI*2f/n;

		Gizmos.color = new Color(1, 1, 0);
		for (int i = 0; i <= n; i++) {
			float angle = i*angle_factor;
			
			p_curr = axes_matrix.MultiplyVector(new Vector3(
				Mathf.Sin(angle), Mathf.Cos(angle))) + initial_pos;
			p_curr = TransformPoint(parent, p_curr);

			if (i != 0) Gizmos.DrawLine(p_prev, p_curr);

			p_prev = p_curr;
		}

		// no need, the radius handle displays a circle already
//		Gizmos.color = new Color(1, 1, 1, 0.75f);
//		for (int i = 0; i <= n; i++) {
//			float angle = i*angle_factor;
//			p_curr = initial_pos_abs + new Vector3(Mathf.Sin(angle), Mathf.Cos(angle)) * looker.reactionRange;
//			if ((i != 0) && (i % 2 != 0)) Gizmos.DrawLine(p_prev, p_curr);
//			p_prev = p_curr;
//		}

		if (looker.lookMode != LookModes.Mouse) {
			if (looker.lookObject != null) {
				p_prev = TransformPoint(parent, initial_pos);
				p_curr = TransformPoint(looker.lookObject, looker.lookPosition);
				Gizmos.color = new Color(0.5f, 1, 0, 0.5f);
				Gizmos.DrawLine(p_prev, p_curr);
			}
		}
	}
#endif
}
