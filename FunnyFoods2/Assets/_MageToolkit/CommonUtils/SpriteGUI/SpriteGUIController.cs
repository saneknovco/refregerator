﻿using UnityEngine;
using System.Collections.Generic;
using Bini.Utils;

// WARNING: at each update, current GUI system calls WalkHierarchy
// (which creates temporary stack/queue), and widgets with
// distribute_children=true rebuild temporary lists ->
// creates garbage with the speed of several mb/s in the Fish scene.

[ExecuteInEditMode]
[MonoBehaviourSingleton(OnlyActive=true, AutoCreate=false)]
public class SpriteGUIController : MonoBehaviourSingleton<SpriteGUIController> {
	[HideInInspector]
	public Camera cam;
	[HideInInspector]
	public Transform root_container;
	[HideInInspector]
	public SpriteGUIWidget root_widget;

	public bool is_dirty = false;
	public void SetDirty() {
		is_dirty = true;
	}

	void Setup() {
		// Make sure this always points to current instance
		// (e.g. when switching between edit and play modes)
		Instance = this;

		if (cam == null) {
			cam = gameObject.GetComponentInChildren<Camera>();
			if (cam == null) {
				var camObj = new GameObject("GUI Camera");
				cam = camObj.AddComponent<Camera>();
			}
			cam.transform.ResetToParent(transform);
			cam.gameObject.layer = gameObject.layer;

			cam.clearFlags = CameraClearFlags.Nothing;
			cam.backgroundColor = Color.clear;
			cam.depth = 1;
			cam.orthographic = true;
			cam.nearClipPlane = 1;
			cam.farClipPlane = 1000;
			cam.cullingMask = (1 << gameObject.layer);
			//Camera.main.cullingMask &= ~cam.cullingMask;
		}

		if (root_container == null) {
			root_container = transform.Find("RootContainer");
			if (root_container == null) {
				var rootObj = new GameObject("RootContainer");
				root_container = rootObj.transform;
			}
			root_container.ResetToParent(transform);
			root_container.gameObject.layer = gameObject.layer;
		}

		if (root_widget == null) {
			root_widget = root_container.GetComponent<SpriteGUIWidget>();
			if (root_widget == null) {
				root_widget = root_container.gameObject.AddComponent<SpriteGUIWidget>();
			}
		}
	}

	void UpdateScreen() {
		var scale = transform.localScale;
		scale.z = 1;
		scale.x = scale.y;
		if (transform.localScale != scale) {
			transform.localScale = scale;
			SetDirty();
		}

		float cam_scale = scale.y;
		float window_w = Screen.width;
		float window_h = Screen.height;
		if (Application.isEditor && !Application.isPlaying) {
			if (Camera.current != null) {
				window_w = window_h * Camera.current.aspect;
			}
		} else {
		}
		var new_orthographicSize = window_h * 0.5f * cam_scale;
		if (cam.orthographicSize != new_orthographicSize) {
			cam.orthographicSize = new_orthographicSize;
			SetDirty();
		}

		float near = cam.nearClipPlane * 2;
		float far = cam.farClipPlane - cam.nearClipPlane;
		float range = far - near;

		var old_bounds = root_widget.GetBounds();
		var new_bounds = new Bounds(new Vector3(0, 0, range*0.5f),
			new Vector3(window_w, window_h, range));
		if (old_bounds != new_bounds) {
			root_widget.SetBounds(new_bounds);
			SetDirty();
		}

		var container_pos = root_container.localPosition;
		container_pos.z = near;
		if (root_container.localPosition != container_pos) {
			root_container.localPosition = container_pos;
			SetDirty();
		}
	}

	void Start() {
		Setup();
		UpdateScreen();
		UpdateLayout();
	}

	void OnDrawGizmos() {
		// We need some method that correctly works with Editor camera.
		// OnRenderObject() is called much more frequently -> CPU load
		if (Application.isEditor && !Application.isPlaying) {
			Setup();
			UpdateScreen();
			UpdateLayout();

			var bounds = root_widget.GetBounds();
			Gizmos.matrix = root_container.localToWorldMatrix;
			if (is_dirty) {
				Gizmos.color = Color.red.SetA(0.5f);
			} else {
				Gizmos.color = Color.green.SetA(0.5f);
			}
			Gizmos.DrawWireCube(bounds.center, bounds.size);
			Gizmos.matrix = Matrix4x4.identity;
		}
	}

	void UpdateLayout() {
		int i = 1;
		while (is_dirty) {
			is_dirty = false;
			root_widget.UpdateLayout();
			if (i == 10) break;
			i++;
		}
	}

	void Update() {
		Setup();

		if (Application.isEditor && !Application.isPlaying) {
			UpdateLayout();
		} else {
			UpdateScreen();
		}

		//UpdateLayout();
	}

	void LateUpdate() {
		root_widget.ListenRecursive();
		UpdateLayout();

		DetectMouseCollision();
		#if MAGE_MT
		if (InputMT.GetMouseButtonDown(0) || InputMT.GetMouseButtonDown(1) || InputMT.GetMouseButtonDown(2))
		#else
		if (Input.GetMouseButtonDown(0) || Input.GetMouseButtonDown(1) || Input.GetMouseButtonDown(2))
		#endif
		{
			ClickedWidget = _WidgetUnderMouse;
		}
	}

	SpriteGUIWidget _WidgetUnderMouse = null;
	public SpriteGUIWidget WidgetUnderMouse {
		get {
			DetectMouseCollision();
			return _WidgetUnderMouse;
		}
	}

	SpriteGUIWidget _PrevWidgetUnderMouse = null;
	public SpriteGUIWidget PrevWidgetUnderMouse {
		get {
			DetectMouseCollision();
			return _PrevWidgetUnderMouse;
		}
	}

	SpriteGUIWidget _LastWidgetUnderMouse = null;
	public SpriteGUIWidget LastWidgetUnderMouse {
		get {
			DetectMouseCollision();
			return _LastWidgetUnderMouse;
		}
	}

	public SpriteGUIWidget ClickedWidget {get; private set;}

	int last_collision_frame = -1;
	void DetectMouseCollision() {
		if (last_collision_frame >= Time.frameCount) return;

		#if MAGE_MT
		Ray ray = cam.ScreenPointToRay(InputMT.mousePosition);
		#else
		Ray ray = cam.ScreenPointToRay(Input.mousePosition);
		#endif
		float ray_distance = cam.farClipPlane - cam.nearClipPlane;

		SpriteGUIWidget best_widget = null;
		float best_dist = float.PositiveInfinity;

		root_container.WalkHierarchy((child) => {
			var widget = child.GetComponent<SpriteGUIWidget>();
			if (widget == null) return;
			if (widget.DetectCollision(ray, ray_distance)) {
				Vector3 p = cam.WorldToScreenPoint(widget.transform.position);
				if (p.z <= best_dist) {
					best_dist = p.z;
					best_widget = widget;
				}
			}
		});

		if (_WidgetUnderMouse != null) _LastWidgetUnderMouse = _WidgetUnderMouse;
		_PrevWidgetUnderMouse = _WidgetUnderMouse;
		_WidgetUnderMouse = best_widget;

		last_collision_frame = Time.frameCount;
	}

	public void EmulateMouseCollision(SpriteGUIWidget widget, int nframes=0) {
		if (_WidgetUnderMouse != null) _LastWidgetUnderMouse = _WidgetUnderMouse;
		_PrevWidgetUnderMouse = _WidgetUnderMouse;
		_WidgetUnderMouse = widget;
		
		last_collision_frame = Time.frameCount + nframes;
	}
}
