﻿using UnityEngine;
using System.Collections.Generic;
using Bini.Utils;

[ExecuteInEditMode]
public class SpriteGUIWidget : MonoBehaviour {
	public enum CoordinateUnits {
		None, Pixel, Millimeter, Self, Parent, Screen,
	}
	public enum FitRules {
		Width, Height, Fit, FitAspect,
	}
	public SpriteGUIWidget NormalizationRuleTemplate = null;
	public CoordinateUnits NormalizationUnitX = CoordinateUnits.None;
	public CoordinateUnits NormalizationUnitY = CoordinateUnits.None;
	public FitRules NormalizationFit = FitRules.FitAspect;
	public Vector2 NormalizationScale = Vector2.one;

	public enum Alignments {
		None, Origin, Start, Middle, End,
	}
	public Alignments AlignmentX = Alignments.None;
	public Alignments AlignmentY = Alignments.None;
	public CoordinateUnits AlignmentUnitsX = CoordinateUnits.None;
	public CoordinateUnits AlignmentUnitsY = CoordinateUnits.None;
	public Vector2 AlignmentOffset = Vector2.zero;

	public bool UseOriginX = false;
	public bool UseOriginY = false;
	public Vector2 Origin = Vector2.zero;

	public enum DistributeAxes {
		None, X, Y,
	}
	public enum DistributeModes {
		None, Start, Middle, End, Distribute, Spread,
	}
	public DistributeAxes DistributeAxis = DistributeAxes.None;
	public DistributeModes DistributeMode = DistributeModes.None;

	public bool NormalizeBounds = false;
	public Bounds NonmeshBounds = new Bounds();

	public enum CollisionModes {
		None, Geometry, Transparency,
	}
	public CollisionModes CollisionMode = CollisionModes.Transparency;

	#region Listeners (on things that can be changed from the editor)
	// position, rotation, scale, parent
	ChangeListener<Vector3> position_listener;
	ChangeListener<Vector3> scale_listener;
	ChangeListener<Transform> parent_listener;
	ChangeListener<int> childcount_listener;

	// Normalization rules
	ChangeListener<SpriteGUIWidget> NormalizationRuleTemplate_listener;
	ChangeListener<CoordinateUnits> NormalizationUnitX_listener;
	ChangeListener<CoordinateUnits> NormalizationUnitY_listener;
	ChangeListener<FitRules> NormalizationFit_listener;
	ChangeListener<Vector2> NormalizationScale_listener;

	ChangeListener<Alignments> AlignmentX_listener;
	ChangeListener<Alignments> AlignmentY_listener;
	ChangeListener<CoordinateUnits> AlignmentUnitsX_listener;
	ChangeListener<CoordinateUnits> AlignmentUnitsY_listener;
	ChangeListener<Vector2> AlignmentOffset_listener;

	ChangeListener<bool> UseOriginX_listener;
	ChangeListener<bool> UseOriginY_listener;
	ChangeListener<Vector2> Origin_listener;

	ChangeListener<bool> NormalizeBounds_listener;
	ChangeListener<Bounds> NonmeshBounds_listener;

	ChangeListener<DistributeAxes> DistributeAxis_listener;
	ChangeListener<DistributeModes> DistributeMode_listener;

	bool listeners_initialized = false;

	void InitListener<T>(ref ChangeListener<T> listener, System.Action on_change) {
		if (listener == null) listener = new ChangeListener<T>(on_change);
	}
	void InitListeners() {
		InitListener<Vector3>(ref position_listener, OnSomethingChanged);
		InitListener<Vector3>(ref scale_listener, OnSomethingChanged);
		InitListener<Transform>(ref parent_listener, OnParentChanged);
		InitListener<int>(ref childcount_listener, OnSomethingChanged);

		InitListener<SpriteGUIWidget>(ref NormalizationRuleTemplate_listener, OnSomethingChanged);
		InitListener<CoordinateUnits>(ref NormalizationUnitX_listener, OnSomethingChanged);
		InitListener<CoordinateUnits>(ref NormalizationUnitY_listener, OnSomethingChanged);
		InitListener<FitRules>(ref NormalizationFit_listener, OnSomethingChanged);
		InitListener<Vector2>(ref NormalizationScale_listener, OnSomethingChanged);

		InitListener<Alignments>(ref AlignmentX_listener, OnSomethingChanged);
		InitListener<Alignments>(ref AlignmentY_listener, OnSomethingChanged);
		InitListener<CoordinateUnits>(ref AlignmentUnitsX_listener, OnSomethingChanged);
		InitListener<CoordinateUnits>(ref AlignmentUnitsY_listener, OnSomethingChanged);
		InitListener<Vector2>(ref AlignmentOffset_listener, OnSomethingChanged);

		InitListener<bool>(ref UseOriginX_listener, OnSomethingChanged);
		InitListener<bool>(ref UseOriginY_listener, OnSomethingChanged);
		InitListener<Vector2>(ref Origin_listener, OnSomethingChanged);

		InitListener<DistributeAxes>(ref DistributeAxis_listener, OnSomethingChanged);
		InitListener<DistributeModes>(ref DistributeMode_listener, OnSomethingChanged);

		InitListener<bool>(ref NormalizeBounds_listener, OnSomethingChanged);
		InitListener<Bounds>(ref NonmeshBounds_listener, OnSomethingChanged);

		listeners_initialized = true;
	}

	void Listen() {
		if (!listeners_initialized) InitListeners();

		Transform this_transform = transform;
		position_listener.Update(this_transform.localPosition);
		scale_listener.Update(this_transform.localScale);
		parent_listener.Update(this_transform.parent);
		childcount_listener.Update(this_transform.childCount);

		NormalizationRuleTemplate_listener.Update(NormalizationRuleTemplate);
		NormalizationUnitX_listener.Update(NormalizationUnitX);
		NormalizationUnitY_listener.Update(NormalizationUnitY);
		NormalizationFit_listener.Update(NormalizationFit);
		NormalizationScale_listener.Update(NormalizationScale);

		AlignmentX_listener.Update(AlignmentX);
		AlignmentY_listener.Update(AlignmentY);
		AlignmentUnitsX_listener.Update(AlignmentUnitsX);
		AlignmentUnitsY_listener.Update(AlignmentUnitsY);
		AlignmentOffset_listener.Update(AlignmentOffset);

		UseOriginX_listener.Update(UseOriginX);
		UseOriginY_listener.Update(UseOriginY);
		Origin_listener.Update(Origin);

		DistributeAxis_listener.Update(DistributeAxis);
		DistributeMode_listener.Update(DistributeMode);

		NormalizeBounds_listener.Update(NormalizeBounds);
		NonmeshBounds_listener.Update(NonmeshBounds);
	}

	void OnSomethingChanged() {
		SpriteGUIController.Instance.SetDirty();
	}
	void OnParentChanged() {
		this_parent = transform.parent;
		if (this_parent != null) {
			this_parent_widget = this_parent.GetComponent<SpriteGUIWidget>();
		} else {
			this_parent_widget = null;
		}
		SpriteGUIController.Instance.SetDirty();
	}
	#endregion

	Transform this_parent = null;
	SpriteGUIWidget this_parent_widget = null;

	MeshFilter mesh_filter = null;

	public void SetBounds(Vector3 center, Vector3 size) {
		SetBounds(new Bounds(center, size));
	}
	public void SetBounds(Bounds new_bounds) {
		NonmeshBounds = new_bounds;
		SpriteGUIController.Instance.SetDirty(); // ? 
	}
	public Bounds GetBounds() {
		if (mesh_filter == null) {
			mesh_filter = GetComponent<MeshFilter>();
			if (mesh_filter == null) {
				mesh_filter = gameObject.AddComponent<MeshFilter>();
			}
		}
		if (mesh_filter.sharedMesh == null) return NonmeshBounds;
		return mesh_filter.sharedMesh.bounds;
	}
	public Bounds GetScaledBounds() {
		var bounds = GetBounds();
		var scale = transform.localScale;
		bounds.min = Vector3.Scale(bounds.min, scale);
		bounds.max = Vector3.Scale(bounds.max, scale);
		return bounds;
	}

	void OnDrawGizmosSelected() {
		var bounds = GetBounds();
		Gizmos.matrix = transform.localToWorldMatrix;
		Gizmos.color = Color.green.SetA(0.5f);
		Gizmos.DrawWireCube(bounds.center, bounds.size);
		Gizmos.matrix = Matrix4x4.identity;
	}

	void OnEnable() {
		// Invoked both in play and edit modes

		// Cached transform is about ~2 times faster than this.transform
		// and about ~5 times faster than GetComponent<Transform>()
		// (at least for the test objects; perhaps it depends on the
		// number of object's components)

		InitListeners();

		Listen();
	}

	void Start() {
		if (GetComponent<Collider>() != null) GetComponent<Collider>().enabled = false;

		Listen();

		UpdateLayout();
	}
	
	void Update() {
		//Listen();

		if (Application.isEditor && !Application.isPlaying) {
		} else {
			Listen();
		}
	}

	public void ListenRecursive() {
		Listen();

		int n_children = transform.childCount;
		for (int i = 0; i < n_children; i++) {
			var child = transform.GetChild(i);
			var widget = child.GetComponent<SpriteGUIWidget>();
			if (widget == null) continue;
			widget.ListenRecursive();
		}
	}

	public void UpdateLayout() {
		if (!gameObject.activeInHierarchy) return;
		if (!this.enabled) return;

		//Debug.Log("UpdateLayout(): "+this.name);

		var new_layer = transform.parent.gameObject.layer;
		if (new_layer != gameObject.layer) gameObject.layer = new_layer;

		UpdateNormalization();
		UpdateAlignment();

		bool distribute_children = (DistributeAxis != DistributeAxes.None) &&
			(DistributeMode != DistributeModes.None);
		if (distribute_children) {
			wrk_child_wdigets.Clear();
			wrk_child_positions.Clear();
		}

		int n_children = transform.childCount;
		for (int i = 0; i < n_children; i++) {
			var child = transform.GetChild(i);
			var widget = child.GetComponent<SpriteGUIWidget>();
			if (widget == null) continue;
			widget.UpdateLayout();
			if (distribute_children) wrk_child_wdigets.Add(widget);
		}

		if (distribute_children) UpdateDistribution();
	}

	static List<SpriteGUIWidget> wrk_child_wdigets = new List<SpriteGUIWidget>();
	static List<float> wrk_child_positions = new List<float>();
	void UpdateDistribution() {
		if (this_parent_widget == null) return;

		int n_children = wrk_child_wdigets.Count;
		if (n_children == 0) return;

		wrk_child_wdigets.Sort((widgetA, widgetB) => {
			return widgetA.name.CompareTo(widgetB.name);
		});

		Bounds this_bounds = GetBounds();

		int axis = 0;
		if (DistributeAxis == DistributeAxes.Y) axis = 1;

		float total_size = 0f;
		for (int i = 0; i < wrk_child_wdigets.Count; i++) {
			var widget = wrk_child_wdigets[i];
			var bounds = widget.GetScaledBounds();
			wrk_child_positions.Add(total_size - bounds.min[axis]);
			total_size += bounds.size[axis];
		}

		float offset = 0f;
		float increment = 0f;
		switch (DistributeMode) {
		case DistributeModes.Start:
			offset = this_bounds.min[axis];
			break;
		case DistributeModes.Middle:
			offset = this_bounds.center[axis] - total_size * 0.5f;
			break;
		case DistributeModes.End:
			offset = this_bounds.max[axis] - total_size;
			break;
		case DistributeModes.Distribute:
			if (n_children == 1) {
				offset = this_bounds.center[axis] - total_size * 0.5f;
			} else {
				increment = (this_bounds.size[axis] - total_size) / (n_children-1);
				offset = this_bounds.min[axis];
			}
			break;
		case DistributeModes.Spread:
			if (n_children == 1) {
				offset = this_bounds.center[axis] - total_size * 0.5f;
			} else {
				increment = (this_bounds.size[axis] - total_size) / (n_children+1);
				offset = this_bounds.min[axis] + increment;
			}
			break;
		}

		for (int i = 0; i < n_children; i++) {
			var widget = wrk_child_wdigets[i];
			var tfm = widget.transform;
			var pos = tfm.localPosition;

			Vector3 new_pos = pos;
			new_pos[axis] = wrk_child_positions[i] + offset;

			if (new_pos != pos) tfm.localPosition = new_pos;

			offset += increment;
		}
	}

	public void SetPosWithOrigin(float x, float y) {
		Bounds this_scaled_bounds = GetScaledBounds();
		if (UseOriginX) x += -this_scaled_bounds.size.x * Origin.x - this_scaled_bounds.min.x;
		if (UseOriginY) y += -this_scaled_bounds.size.y * Origin.y - this_scaled_bounds.min.y;
		Vector3 pos = new Vector3(x, y, transform.localPosition.z);
		if (pos != transform.localPosition) {
			transform.localPosition = pos;
		}
	}

	void UpdateAlignment() {
		if (this_parent_widget == null) return;

		if ((AlignmentX == Alignments.None) && (AlignmentY == Alignments.None)) return;

		Bounds parent_bounds = this_parent_widget.GetBounds();
		Bounds root_bounds = SpriteGUIController.Instance.root_widget.GetBounds();
		Bounds this_bounds = GetScaledBounds();

		Vector3 pos = transform.localPosition;

		if (AlignmentX != Alignments.None) {
			float p = 0f;
			switch(AlignmentX) {
			case Alignments.Start:
				p = parent_bounds.min.x;
				break;
			case Alignments.Middle:
				p = parent_bounds.center.x;
				break;
			case Alignments.End:
				p = parent_bounds.max.x;
				break;
			}

			float offset = AlignmentOffset.x;
			switch (AlignmentUnitsX) {
			case CoordinateUnits.None:
				offset = 0f;
				break;
			case CoordinateUnits.Millimeter:
				offset *= PlatformUtils.PixelsPerCm;
				break;
			case CoordinateUnits.Self:
				offset *= this_bounds.size.x;
				break;
			case CoordinateUnits.Parent:
				offset *= parent_bounds.size.x;
				break;
			case CoordinateUnits.Screen:
				offset *= root_bounds.size.x;
				break;
			}
			p += offset;

			if (UseOriginX) p += -this_bounds.size.x * Origin.x - this_bounds.min.x;

			pos.x = p;
		}

		if (AlignmentY != Alignments.None) {
			float p = 0f;
			switch(AlignmentY) {
			case Alignments.Start:
				p = parent_bounds.min.y;
				break;
			case Alignments.Middle:
				p = parent_bounds.center.y;
				break;
			case Alignments.End:
				p = parent_bounds.max.y;
				break;
			}

			float offset = AlignmentOffset.y;
			switch (AlignmentUnitsY) {
			case CoordinateUnits.None:
				offset = 0f;
				break;
			case CoordinateUnits.Millimeter:
				offset *= PlatformUtils.PixelsPerCm;
				break;
			case CoordinateUnits.Self:
				offset *= this_bounds.size.y;
				break;
			case CoordinateUnits.Parent:
				offset *= parent_bounds.size.y;
				break;
			case CoordinateUnits.Screen:
				offset *= root_bounds.size.y;
				break;
			}
			p += offset;

			if (UseOriginY) p += -this_bounds.size.y * Origin.y - this_bounds.min.y;

			pos.y = p;
		}

		if (pos != transform.localPosition) {
			transform.localPosition = pos;
		}
	}

	void UpdateNormalization() {
		if (this_parent_widget == null) return;

		var norm_rule_x = NormalizationUnitX;
		var norm_rule_y = NormalizationUnitY;
		var norm_side = NormalizationFit;
		var norm_scale = NormalizationScale;

		if (NormalizationRuleTemplate != null) {
			norm_rule_x = NormalizationRuleTemplate.NormalizationUnitX;
			norm_rule_y = NormalizationRuleTemplate.NormalizationUnitY;
			norm_side = NormalizationRuleTemplate.NormalizationFit;
			norm_scale = NormalizationRuleTemplate.NormalizationScale;

			if (norm_rule_x == CoordinateUnits.None) norm_rule_x = NormalizationUnitX;
			if (norm_rule_y == CoordinateUnits.None) norm_rule_y = NormalizationUnitY;
			norm_scale.Scale(NormalizationScale);
		}

		if ((norm_rule_x == CoordinateUnits.None) &&
			(norm_rule_y == CoordinateUnits.None)) return;

		Vector3 localScale = transform.localScale;
		Vector3 scale = new Vector3(norm_scale.x, norm_scale.y, localScale.z);

		Bounds bounds = GetBounds();
		float src_w = bounds.size.x;
		float src_h = bounds.size.y;

		if (NormalizeBounds) {
			src_w = 1f;
			src_h = 1f;
		}

		float dest_w = src_w;
		float dest_h = src_h;
		float factor = 1f;

		switch (norm_rule_x) {
		case CoordinateUnits.Pixel:
			dest_w = 1f;
			break;
		case CoordinateUnits.Millimeter:
			factor = PlatformUtils.PixelsPerCm * 0.1f;
			dest_w = factor;
			break;
		case CoordinateUnits.Parent:
			if (this_parent_widget != null) {
				var parent_bounds = this_parent_widget.GetBounds();
				dest_w = parent_bounds.size.x;
			}
			break;
		case CoordinateUnits.Screen:
			var gui_controller = SpriteGUIController.Instance;
			var parent_bounds = gui_controller.root_widget.GetBounds();
			dest_w = parent_bounds.size.x;
			break;
		}

		switch (norm_rule_y) {
		case CoordinateUnits.Pixel:
			dest_h = 1f;
			break;
		case CoordinateUnits.Millimeter:
			factor = PlatformUtils.PixelsPerCm * 0.1f;
			dest_h = factor;
			break;
		case CoordinateUnits.Parent:
			if (this_parent_widget != null) {
				var parent_bounds = this_parent_widget.GetBounds();
				dest_h = parent_bounds.size.y;
			}
			break;
		case CoordinateUnits.Screen:
			var gui_controller = SpriteGUIController.Instance;
			var parent_bounds = gui_controller.root_widget.GetBounds();
			dest_h = parent_bounds.size.y;
			break;
		}

		switch (norm_side) {
		case FitRules.Width:
			if (src_w > float.Epsilon) {
				factor = dest_w / src_w;
				scale.x *= factor;
				scale.y *= factor;
			}
			break;
		case FitRules.Height:
			if (src_h > float.Epsilon) {
				factor = dest_h / src_h;
				scale.x *= factor;
				scale.y *= factor;
			}
			break;
		case FitRules.Fit:
			if (src_w > float.Epsilon) {
				scale.x *= dest_w / src_w;
			}
			if (src_h > float.Epsilon) {
				scale.y *= dest_h / src_h;
			}
			break;
		case FitRules.FitAspect:
			if ((src_w > float.Epsilon) && (src_h > float.Epsilon)) {
				factor = Mathf.Min(dest_w / src_w, dest_h / src_h);
			} else if (src_w > float.Epsilon) {
				factor = dest_w / src_w;
			} else if (src_h > float.Epsilon) {
				factor = dest_h / src_h;
			} else {
				factor = 1f;
			}
			scale.x *= factor;
			scale.y *= factor;
			break;
		}

		if (!NormalizeBounds) {
			if (localScale != scale) transform.localScale = scale;
		} else {
			Bounds new_bounds = new Bounds(bounds.center,
				new Vector3(scale.x, scale.y, bounds.size.z));
			if (bounds != new_bounds) SetBounds(new_bounds);
		}
	}

	public bool DetectCollision(Ray ray, float distance) {
		if (CollisionMode == CollisionModes.None) return false;
		if (!gameObject.activeInHierarchy) return false;

		var m = transform.worldToLocalMatrix;
		ray.origin = m.MultiplyPoint(ray.origin);
		ray.direction = m.MultiplyVector(ray.direction);

		Vector2 uv;

		var mesh_collider = GetComponent<Collider>() as MeshCollider;
		if (mesh_collider != null) {
			RaycastHit hitInfo;
			if (!mesh_collider.Raycast(ray, out hitInfo, distance)) return false;
			uv = hitInfo.textureCoord;
		} else {
			var bounds = GetBounds();
			// May be problematic for bounds with size.z=0 ?
			//if (!bounds.IntersectRay(ray, out distance)) return false;
			//var p = ray.origin + ray.direction * distance;
			var p = ray.origin; // GUI is always orthographic, anyway
			uv.x = (p.x - bounds.min.x) / bounds.size.x;
			uv.y = (p.y - bounds.min.y) / bounds.size.y;
			if ((uv.x < 0f) || (uv.x > 1f) || (uv.y < 0f) || (uv.y > 1f)) return false;
		}

		if (CollisionMode == CollisionModes.Transparency) {
			if (GetComponent<Renderer>() == null) return false;
			if (!GetComponent<Renderer>().enabled) return false;
			var mat = GetComponent<Renderer>().sharedMaterial;
			var tex = (mat != null) ? mat.mainTexture as Texture2D : null;
			if (tex != null) {
				try {
					var color = tex.GetPixelBilinear(uv.x, uv.y);
					if (color.a < 0.25f) return false;
				} catch (UnityException) {
					// Texture is not readable
					if (Debug.isDebugBuild) Debug.LogError(string.Format(
						"GUI widget selection failed: {0} has unreadable texture {1}",
						this.name, tex.name));
				}
			}
		}

		return true;
	}
}
