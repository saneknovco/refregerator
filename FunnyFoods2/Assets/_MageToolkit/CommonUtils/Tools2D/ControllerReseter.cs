﻿using UnityEngine;
using System.Collections.Generic;
using Bini.Utils;

public class ControllerReseter : MonoBehaviour {
	public void ResetController(string parameters) {
		ResetController(parameters, transform.parent);
	}

	public static void ResetController(string parameters, Transform controller) {
		if (controller == null) return;

		bool p_apply = false;
		Vector3 p = Vector3.zero;
		
		bool r_apply = false;
		Quaternion r = Quaternion.identity;

		bool s_apply = false;
		Vector3 s = Vector3.one;

		ParseParameters(parameters, controller,
			out p_apply, out p, out r_apply, out r, out s_apply, out s);

		if (p_apply) controller.localPosition = p;
		if (r_apply) controller.localRotation = r;
		if (s_apply) controller.localScale = s;
	}

	public static void ParseParameters(string parameters, Transform controller,
		out bool p_apply, out Vector3 p,
		out bool r_apply, out Quaternion r,
		out bool s_apply, out Vector3 s)
	{
		p_apply = false;
		p = Vector3.zero;

		r_apply = false;
		r = Quaternion.identity;

		s_apply = false;
		s = Vector3.one;

		if (controller == null) return;

		string prefix = controller.name+":";
		if (!parameters.StartsWith(prefix)) return;
		parameters = parameters.Substring(prefix.Length);

		var toks = parameters.Split(';');

		foreach (var tok in toks) {
			int ieq = tok.IndexOf('=');
			if (ieq == -1) continue;

			string name = tok.Substring(0, ieq).Trim();
			string[] vals = tok.Substring(ieq+1).Split(',');

			switch (name) {
			case "p":
				p_apply = true;
				p.x = float.Parse(vals[0].Trim());
				p.y = float.Parse(vals[1].Trim());
				p.z = float.Parse(vals[2].Trim());
				break;
			case "r":
				r_apply = true;
				r.x = float.Parse(vals[0].Trim());
				r.y = float.Parse(vals[1].Trim());
				r.z = float.Parse(vals[2].Trim());
				r.w = float.Parse(vals[3].Trim());
				break;
			case "s":
				s_apply = true;
				s.x = float.Parse(vals[0].Trim());
				s.y = float.Parse(vals[1].Trim());
				s.z = float.Parse(vals[2].Trim());
				break;
			}
		}
	}

	public static AnimationEvent FindResetEvent(IEnumerable<AnimationEvent> events, Transform controller) {
		if (controller == null) return null;
		string prefix = controller.name+":";
		foreach (var evt in events) {
			if (evt.time != 0) continue;
			if (evt.functionName != "ResetController") continue;
			if (!evt.stringParameter.StartsWith(prefix)) continue;
			return evt;
		}
		return null;
	}

	public static string MakeParameters(Transform controller, bool p_apply, bool r_apply, bool s_apply) {
		if (controller == null) return "";
		string prefix = controller.name+":";

		Vector3 p = controller.localPosition;
		string sp = string.Format("p={0},{1},{2}", p.x, p.y, p.z);

		Quaternion r = controller.localRotation;
		string sr = string.Format("r={0},{1},{2},{3}", r.x, r.y, r.z, r.w);

		Vector3 s = controller.localScale;
		string ss = string.Format("s={0},{1},{2}", s.x, s.y, s.z);

		string result = prefix;
		if (p_apply) result += sp+";";
		if (r_apply) result += sr+";";
		if (s_apply) result += ss+";";

		return result;
	}
}
