using UnityEditor;
using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;
using Bini.Utils;

//[InitializeOnLoad]
public class Tools2D_Window : EditorWindow {
	public static Tools2D_Window window = null;
	
	static bool initialized {
		get { return (window != null); }
	}
	
	Tools2D_Window() {
		window = this;
	}
	
	// Static constructor is called if the class has [InitializeOnLoad] attribute
	// Or if window is opened as a part of default editor layout
	static Tools2D_Window() {
		Init(true);
	}
	
	[MenuItem("Window/2D Tools")]
	static void InitFromMenu() {
		Init(false);
	}
	static void Init(bool window_exists=false) {
		if (initialized) return;
		
		if (!window_exists) {
			// If GetWindow() is called from the static constructor
			// while the window is still open, Unity throws an error!
			window = GetWindow<Tools2D_Window>();
			window.title = "2D Tools";
			window.autoRepaintOnSceneChange = true; // maybe disable later
		}
		
		SceneView.onSceneGUIDelegate += OnSceneGUI;
	}

	Transform active_transform = null;
	bool active_obj_is_sprite = false;
	int active_sprite_i_shift = 0;
	Rect active_sprite_rect = new Rect();
	Rect active_sprite_uv0 = new Rect();
	Rect active_sprite_uv1 = new Rect();
	Rect active_sprite_uv2 = new Rect();
	Vector2 active_sprite_pivot = new Vector2();

	enum SpriteColliderType {
		None, Box3D, Box2D, Circle, Polygon
	}
	SpriteColliderType collider_type = SpriteColliderType.None;
	static Component AddSpriteCollider(GameObject gameobj) {
		switch (window.collider_type) {
		case SpriteColliderType.Box3D: return gameobj.AddComponent<BoxCollider>();
		case SpriteColliderType.Box2D: return gameobj.AddComponent<BoxCollider2D>();
		case SpriteColliderType.Circle: return gameobj.AddComponent<CircleCollider2D>();
		case SpriteColliderType.Polygon: return gameobj.AddComponent<PolygonCollider2D>();
		default: return null;
		}
	}

	void OnGUI() {
		if (!initialized) return;

		if (active_transform != Selection.activeTransform) {
			active_transform = Selection.activeTransform;
		}

		active_obj_is_sprite = SpriteTexDragger.IsAnActualSprite(active_transform);
		if (active_obj_is_sprite) {
			var mesh_filter = active_transform.GetComponent<MeshFilter>();
			var mesh = mesh_filter.sharedMesh;
			active_sprite_rect = MeshUtils.CalculateBounds(mesh).ToRect();
			active_sprite_uv0 = MeshUtils.CalculateUVBounds(mesh, 0);
			active_sprite_uv1 = MeshUtils.CalculateUVBounds(mesh, 1);
			active_sprite_uv2 = MeshUtils.CalculateUVBounds(mesh, 2);
			active_sprite_pivot = -active_sprite_rect.center.Div(active_sprite_rect.extents());
		} else {
			active_sprite_rect = new Rect();
			active_sprite_uv0 = new Rect();
			active_sprite_uv1 = new Rect();
			active_sprite_uv2 = new Rect();
			active_sprite_pivot = new Vector2();
		}

		int i_shift = EditorGUILayout.IntSlider("Index Offset", active_sprite_i_shift, 0, 3);
		Vector2 new_pivot = EditorGUILayout.Vector2Field("Sprite Handle (relative)", active_sprite_pivot);
		Rect new_rect = EditorGUILayout.RectField("Sprite Rect", active_sprite_rect);
		Rect uv0 = EditorGUILayout.RectField("UV 0", active_sprite_uv0);
		Rect uv1 = EditorGUILayout.RectField("UV 1", active_sprite_uv1);
		Rect uv2 = EditorGUILayout.RectField("UV 2", active_sprite_uv2);

		collider_type = (SpriteColliderType)EditorGUILayout.EnumPopup("New Sprite Collider", collider_type);

		if (active_obj_is_sprite) {
			if (new_pivot != active_sprite_pivot) {
				Vector2 abs_delta = (new_pivot - active_sprite_pivot).Mul(active_sprite_rect.extents());
				new_rect.position -= abs_delta;
			}

			bool modify = false;
			modify |= (i_shift != active_sprite_i_shift);
			modify |= (new_rect != active_sprite_rect);
			modify |= (uv0 != active_sprite_uv0);
			modify |= (uv1 != active_sprite_uv1);
			modify |= (uv2 != active_sprite_uv2);
			if (modify) {
				active_sprite_i_shift = i_shift;
				var mesh_filter = active_transform.GetComponent<MeshFilter>();
				var mesh = mesh_filter.sharedMesh;
				if (FileUtils.AssetExists(mesh)) {
					float bounds_depth = mesh.bounds.size.z;
					MeshUtils.SetSpriteMesh(mesh, null, new_rect, i_shift, uv0, i_shift, uv1, i_shift, uv2, i_shift);
					mesh.bounds = new Bounds(mesh.bounds.center, mesh.bounds.size + (Vector3.forward * bounds_depth));
					mesh_filter.sharedMesh = FileUtils.SaveAsset<Mesh>(mesh);
				}
			}
		}
	}

	static void OnSceneGUI(SceneView sceneview) {
		if (!initialized) return;
		
		DragSpriteHandle.OnSceneGUI();
		
		SpriteTexDragger_Create.OnSceneGUI();
		SpriteTexDragger_Change.OnSceneGUI();
	}
	
	// Called 100 times per second on all visible windows.
//	void Update() {
//		if (!initialized) return;
//	}
	
	void OnDestroy() {
		if (!initialized) return;
		
		// Somewhy OnSceneGUI would still continue to be invoked
		SceneView.onSceneGUIDelegate -= OnSceneGUI;
		
		SpriteTexDragger_Create.Dispose();
		SpriteTexDragger_Change.Dispose();
		
		window = null;
	}

	static Transform Raycast(Vector3 screen_pos) {
		Transform hit_tfm = null;
		var ray = HandleUtility.GUIPointToWorldRay(screen_pos);
		float best_distance = float.PositiveInfinity;

		var hit = default(RaycastHit);
		if (Physics.Raycast(ray, out hit)) {
			hit_tfm = hit.transform;
			best_distance = hit.distance;
		}

		var hit2d = Physics2D.GetRayIntersection(ray);
		if (hit2d.transform) {
			if (hit_tfm == null) {
				hit_tfm = hit2d.transform;
				best_distance = hit2d.distance;
			} else if (hit2d.distance < best_distance) {
				hit_tfm = hit2d.transform;
				best_distance = hit2d.distance;
			}
		}

		float distance;
		var active_tfm = RaycastActive(ray, out distance);
		if (active_tfm) {
			if (hit_tfm == null) {
				hit_tfm = active_tfm;
				best_distance = distance;
			} else if (distance < best_distance) {
				hit_tfm = active_tfm;
				best_distance = distance;
			}
		}

		return hit_tfm;
	}

	static Transform RaycastActive(Ray ray, out float distance) {
		distance = float.PositiveInfinity;

		var active_tfm = GetActiveTransform();
		if (!active_tfm) return null;

		var mesh_filter = active_tfm.GetComponent<MeshFilter>();
		if (!mesh_filter) return null;

		var mesh = mesh_filter.sharedMesh;
		if (!mesh) return null;

		var plane = new Plane(active_tfm.forward, active_tfm.position);
		if (!plane.Raycast(ray, out distance)) return null;

		var point = active_tfm.InverseTransformPoint(ray.GetPoint(distance));

		var rect = MeshUtils.CalculateBounds(mesh).ToRect();
		if (!rect.Contains((Vector2)point)) return null;

		return active_tfm;
	}
	
	static Vector3 PlaneAxis(Component component, bool absolute=false) {
		Mesh mesh = null;
		if (component != null) {
			var mesh_filter = component.GetComponent<MeshFilter>();
			if (mesh_filter != null) mesh = mesh_filter.sharedMesh;
		}
		var axis = PlaneAxis(mesh);
		if (absolute) {
			if (component != null) {
				axis = component.transform.TransformDirection(axis);
			}
		}
		return axis.normalized;
	}
	static Vector3 PlaneAxis(Mesh mesh) {
		if (mesh == null) return Vector3.forward;
		var verts = mesh.vertices;
		if (verts.Length < 3) return Vector3.forward;
		var tris = mesh.triangles;
		if (tris.Length < 1) return Vector3.forward;
		// All mesh triangles are expected to be coplanar
		var v0 = mesh.vertices[mesh.triangles[0]];
		var v1 = mesh.vertices[mesh.triangles[1]];
		var v2 = mesh.vertices[mesh.triangles[2]];
		return Vector3.Cross(v0 - v1, v2 - v1).normalized;
	}

	public static bool FindIntersection(Vector3 screen_pos, out Vector3 intersection_point) {
		var ray = HandleUtility.GUIPointToWorldRay(screen_pos);
		
		var tfm = GetActiveTransform();
		
		var pos = (tfm != null) ? tfm.position : Vector3.zero;
		var normal = (tfm != null) ? tfm.forward : Vector3.forward;
		var plane = new Plane(normal, pos);
		
		float distance = 0; 
		bool result = plane.Raycast(ray, out distance);
		intersection_point = (result) ? ray.GetPoint(distance) : Vector3.zero;
		return result;
	}

	public static Transform GetActiveTransform() {
		var active_tfm = Selection.activeTransform;
		var main_cam = Camera.main;
		if (main_cam && (active_tfm == main_cam.transform)) active_tfm = null;
		return active_tfm;
	}

	// In recent versions, Unity consumes the Alt modifier on OSX
	#if UNITY_EDITOR_OSX
	const EventModifiers MODIFIERS_CREATE = EventModifiers.Shift;
	#else
	const EventModifiers MODIFIERS_CREATE = EventModifiers.Alt;
	#endif
	const EventModifiers MODIFIERS_REPLACE = EventModifiers.Control | EventModifiers.Command;
	const EventModifiers MODIFIERS_ANY = MODIFIERS_CREATE | MODIFIERS_REPLACE;

	static bool CheckModifiers(Event e, EventModifiers modifiers) {
		return ((e.modifiers & modifiers) != EventModifiers.None);
	}

	class DragSpriteHandle {
		// Singleton pattern
		private DragSpriteHandle() {}
		static DragSpriteHandle _instance = null;
		public static DragSpriteHandle instance {
			get {
				if (_instance == null) _instance = new DragSpriteHandle();
				return _instance;
			}
		}
		
		static bool modify_shared_mesh = true;
		
		Vector3 current_drag_normal;
		Quaternion current_drag_orientation;
		Vector3 current_drag_pos;
		Vector3 initial_drag_pos;
		
		bool create_new_asset = true;
		
		Tool last_tool;
		
		Vector2 screen_offset;
		
		public static void OnSceneGUI() {
			instance._OnSceneGUI();
		}
		void _OnSceneGUI() {
			Event e = Event.current;
			
			string dnd_data_name = "DragHandle";
			
			var active_tfm = Selection.activeTransform;
			
			object generic_obj = DragAndDrop.GetGenericData(dnd_data_name);
			if (generic_obj != null) {
				Handles.PositionHandle(current_drag_pos, current_drag_orientation);
				bool raycasted = RaycastMouseOnPlane(e.mousePosition + screen_offset);
				
				bool finished = false;
				
				if (e.type == EventType.DragUpdated) {
					if (CheckModifiers(e, MODIFIERS_ANY)) {
						// Remind user that (s)he has to unpress Alt/Ctrl
						DragAndDrop.visualMode = DragAndDropVisualMode.None;
						if (CheckModifiers(e, MODIFIERS_REPLACE)) {
							create_new_asset = true;
						} else if (CheckModifiers(e, MODIFIERS_CREATE)) {
							create_new_asset = false;
						}
					} else if (!raycasted) {
						// Don't move handle blindly!
						DragAndDrop.visualMode = DragAndDropVisualMode.None;
					} else {
						DragAndDrop.AcceptDrag();
						DragAndDrop.visualMode = DragAndDropVisualMode.Move;
					}
					e.Use();
				} else if (e.type == EventType.DragPerform) {
					// To get here, user has to unpress Alt/Ctrl _BEFORE_ unpressing mouse.
					// (This is the quirk of Unity's behavior)
					Undo.RegisterSceneUndo("Change Sprite Handle");
					ChangeHandle(active_tfm.gameObject, current_drag_pos, create_new_asset);
					e.Use();
					finished = true;
				} else if (e.type == EventType.DragExited) {
					// This event can happen when:
					// * User pressed Esc
					// * Mouse is out of the 3D View
					// * User unpressed mouse while holding Alt
				} else if (e.isMouse) {
					finished = true;
				}
				
				if (finished) {
					DragAndDrop.SetGenericData(dnd_data_name, null);
					Tools.current = last_tool;
				}
			}
			
			if (active_tfm == null) return;
			if (CheckModifiers(e, MODIFIERS_ANY)) {
				if (e.type == EventType.MouseDown) {
					if (CheckModifiers(e, MODIFIERS_REPLACE)) {
						create_new_asset = true;
					} else if (CheckModifiers(e, MODIFIERS_CREATE)) {
						create_new_asset = false;
					}
					
					var ps = Camera.current.WorldToScreenPoint(active_tfm.position);
					var psm = new Vector2(ps.x, Camera.current.pixelRect.height - ps.y);
					screen_offset = psm - e.mousePosition;
					
					initial_drag_pos = active_tfm.position;
					current_drag_pos = initial_drag_pos;
					current_drag_normal = PlaneAxis(active_tfm, true);
					current_drag_orientation = Quaternion.LookRotation(current_drag_normal);
					
					last_tool = Tools.current;
					Tools.current = Tool.None;
					
					DragAndDrop.PrepareStartDrag();
					DragAndDrop.StartDrag(dnd_data_name);
					DragAndDrop.visualMode = DragAndDropVisualMode.Move;
					DragAndDrop.paths = new string[]{"///"}; // is this necessary?
					DragAndDrop.SetGenericData(dnd_data_name, initial_drag_pos);
					
					e.Use();
				}
			}
		}
		
		bool RaycastMouseOnPlane(Vector2 screen_pos) {
			var ray = HandleUtility.GUIPointToWorldRay(screen_pos);
			var plane = new Plane(current_drag_normal, initial_drag_pos);
			float distance = 0; 
			bool result = plane.Raycast(ray, out distance);
			if (result) current_drag_pos = ray.GetPoint(distance);
			return result;
		}

		static void ChangeHandle(GameObject obj, Vector3 global_handle, bool create_new_asset=true) {
			var mesh_filter = obj.GetComponent<MeshFilter>();
			var collider = obj.GetComponent<Collider>();
			var collider2d = obj.GetComponent<Collider2D>();
			var mesh_collider = collider as MeshCollider;
			
			bool filter_collider_same = false;
			if ((mesh_filter != null) && (mesh_collider != null)) {
				filter_collider_same = (mesh_filter.sharedMesh == mesh_collider.sharedMesh);
			}
			
			var new_handle = obj.transform.InverseTransformPoint(global_handle);
			
			// Move visible mesh
			if ((mesh_filter != null) && (mesh_filter.sharedMesh != null)) {
				mesh_filter.sharedMesh = OffsetMeshAsset(mesh_filter.sharedMesh, new_handle, create_new_asset);
			}
			
			// Move collider
			if ((mesh_collider != null) && (mesh_collider.sharedMesh != null)) {
				if (filter_collider_same) {
					mesh_collider.sharedMesh = mesh_filter.sharedMesh;
				} else {
					mesh_collider.sharedMesh = OffsetMeshAsset(mesh_collider.sharedMesh, new_handle, create_new_asset);
				}
			} else if (mesh_collider == null) {
				if (collider != null) {
					OffsetCollider(collider, new_handle);
				} else if (collider2d != null) {
					OffsetCollider2D(collider2d, new_handle);
				}
			}
			
			// Store children positions
			var child_pos = new Vector3[obj.transform.childCount];
			for (int i = 0; i < obj.transform.childCount; i++) {
				child_pos[i] = obj.transform.GetChild(i).position;
			}
			
			// Move object to match its old visual position
			obj.transform.position = global_handle;
			
			// Restore children positions
			for (int i = 0; i < obj.transform.childCount; i++) {
				obj.transform.GetChild(i).position = child_pos[i];
			}
		}
		
		static Mesh OffsetMeshAsset(Mesh mesh, Vector3 new_handle, bool create_new_asset=true) {
			bool create_asset = false;
			
			string path = AssetDatabase.GetAssetPath(mesh);
			if (string.IsNullOrEmpty(path)) {
				string dir, file;
				FileUtils.SplitPath(EditorApplication.currentScene, out dir, out file);
				if (string.IsNullOrEmpty(dir)) {
					path = "Assets/Meshes/";
				} else {
					path = dir + "Meshes/";
				}
				file = mesh.name;
				if (string.IsNullOrEmpty(file)) {
					file = "mesh_" + System.DateTime.Now.ToString() +
						"_" + Time.realtimeSinceStartup.ToString();
				}
				file = Regex.Replace(mesh.name, "\\W", "_");
				path += file + ".asset";
				create_asset = true;
			} else if (create_new_asset) {
				path = AssetDatabase.GenerateUniqueAssetPath(path);
				create_asset = true;
			}
			
			if ((!modify_shared_mesh) || create_new_asset) {
				mesh = MeshUtils.CopyMesh(mesh);
				create_asset = true;
			}
			
			OffsetMesh(mesh, new_handle);
			
			if (modify_shared_mesh) {
				if (create_new_asset || create_asset) {
					FileUtils.EnsureAssetDir(path);
					AssetDatabase.CreateAsset(mesh, path);
				}
				AssetDatabase.SaveAssets();
				AssetDatabase.ImportAsset(path);
				mesh = AssetDatabase.LoadAssetAtPath(path, typeof(Mesh)) as Mesh;
			}
			
			return mesh;
		}
		
		static void OffsetMesh(Mesh mesh, Vector3 new_handle) {
			var vertices = mesh.vertices;
			for (int i = 0; i < vertices.Length; i++) {
				vertices[i] -= new_handle;
			}
			mesh.vertices = vertices;
			// Offset bounds instead or recalculating, since they could have
			// been specifically set to a different size than the actual bounding box
			mesh.bounds = new Bounds(mesh.bounds.center - new_handle, mesh.bounds.size);
		}
		
		static void OffsetCollider(Collider collider, Vector3 new_handle) {
			if (collider is BoxCollider) {
				(collider as BoxCollider).center -= new_handle;
			} else if (collider is SphereCollider) {
				(collider as SphereCollider).center -= new_handle;
			} else if (collider is CapsuleCollider) {
				(collider as CapsuleCollider).center -= new_handle;
			} else if (collider is WheelCollider) {
				(collider as WheelCollider).center -= new_handle;
			}
		}
		
		static void OffsetCollider2D(Collider2D collider2d, Vector2 new_handle) {
#if UNITY_5
            if (collider2d is BoxCollider2D) {
				(collider2d as BoxCollider2D).offset -= new_handle;
			} else if (collider2d is CircleCollider2D) {
				(collider2d as CircleCollider2D).offset -= new_handle;
			} else if (collider2d is PolygonCollider2D) {
				(collider2d as PolygonCollider2D).offset -= new_handle;
			} else if (collider2d is EdgeCollider2D) {
				(collider2d as EdgeCollider2D).offset -= new_handle;
			}
#else
            Debug.LogWarning("OffsetCollider2D not implementation: " + collider2d.name + "not offset at " + new_handle);
#endif
        }
	}
	
	abstract class SpriteTexDragger {
		public static string MaterialsSavePath = "Materials/";
		public static string MeshesSavePath = "SpriteMeshes/";

		public static string MaterialSavePath(string dir, string filename) {
			return dir + MaterialsSavePath + filename + ".mat";
		}
		public static string MeshSavePath(string dir, string filename) {
			return dir + MeshesSavePath + filename + ".asset";
		}

		abstract protected bool VerifyEvent();
		abstract protected bool ProcessDrag();
		abstract protected void RegisterUndo();
		abstract protected void Clear();
		abstract protected void Cleanup();
		
		protected void _OnSceneGUI() {
			bool has_drag_data = (DragAndDrop.objectReferences.Length != 0);
			if (!(VerifyEvent() && has_drag_data)) {
				Cleanup();
				return;
			}
			
			Event e = Event.current;
			if (e.type == EventType.DragUpdated) {
				if (ProcessDrag()) {
					DragAndDrop.AcceptDrag();
					DragAndDrop.visualMode = DragAndDropVisualMode.Generic;
					e.Use();
				}
			} else if (e.type == EventType.DragPerform) {
				RegisterUndo();
				Clear();
				Resources.UnloadUnusedAssets();
				e.Use();
			} else if (e.type == EventType.DragExited) {
				Cleanup();
				Resources.UnloadUnusedAssets();
				e.Use();
			}
		}
		
		protected class DragMaterial {
			public Material material = null;
			public Mesh mesh = null;

			public DragMaterial(Texture texture) {
				string dir, filename;
				FileUtils.SplitAssetPath(texture, out dir, out filename);

				LoadMaterial(dir, filename);

				if (material == null) {
					material = new Material(Shader.Find("Unlit/Transparent"));
					material.name = filename;
					material.mainTexture = texture;
				}

				LoadMesh(dir + MaterialsSavePath, filename);
			}

			public DragMaterial(Material material) {
				string dir, filename;
				FileUtils.SplitAssetPath(material, out dir, out filename);

				this.material = material;

				LoadMesh(dir, filename);
			}

			void LoadMaterial(string dir, string filename) {
				string path = SpriteTexDragger.MaterialSavePath(dir, filename);
				if (FileUtils.AssetExists(path)) {
					//AssetDatabase.ImportAsset(path); // ?
					material = AssetDatabase.LoadAssetAtPath(path,
						typeof(Material)) as Material;
				}
			}

			void LoadMesh(string dir, string filename) {
				string path = SpriteTexDragger.MeshSavePath(dir, filename);
				if (FileUtils.AssetExists(path)) {
					//AssetDatabase.ImportAsset(path); // ?
					mesh = AssetDatabase.LoadAssetAtPath(path,
						typeof(Mesh)) as Mesh;
				}
			}

			public void Cleanup() {
				if ((material != null) && !AssetDatabase.Contains(material)) {
					Object.DestroyImmediate(material);
				}
				if ((mesh != null) && !AssetDatabase.Contains(mesh)) {
					Object.DestroyImmediate(mesh);
				}
			}
		}
		
		protected static List<DragMaterial> ConvertDragMaterials(bool only_one=false) {
			var dragmats = new List<DragMaterial>();
			foreach (var obj_ref in DragAndDrop.objectReferences) {
				Material mat_ref = obj_ref as Material;
				Texture tex_ref = null;
				if (mat_ref != null) {
					if (mat_ref.mainTexture != null) {
						dragmats.Add(new DragMaterial(mat_ref));
					}
				} else {
					tex_ref = obj_ref as Texture;
					if (tex_ref != null) {
						dragmats.Add(new DragMaterial(tex_ref));
					}
				}
				if (only_one && (dragmats.Count == 1)) break;
			}
			return dragmats;
		}

		public static void SaveAssets(ref Material material, ref Mesh mesh) {
			if (!AssetDatabase.Contains(material)) {
				string dir, filename;
				FileUtils.SplitAssetPath(material.mainTexture, out dir, out filename);

				string path = MaterialSavePath(dir, filename);
				FileUtils.EnsureAssetDir(path);
				AssetDatabase.CreateAsset(material, path);
				AssetDatabase.SaveAssets();
				AssetDatabase.ImportAsset(path);
				material = AssetDatabase.LoadAssetAtPath(path, typeof(Material)) as Material;
			}

			if ((mesh != null) && !AssetDatabase.Contains(mesh)) {
				string dir, filename;
				FileUtils.SplitAssetPath(material, out dir, out filename);

				string path = MeshSavePath(dir, filename);
				FileUtils.EnsureAssetDir(path);
				AssetDatabase.CreateAsset(mesh, path);
				AssetDatabase.SaveAssets();
				AssetDatabase.ImportAsset(path);
				mesh = AssetDatabase.LoadAssetAtPath(path, typeof(Mesh)) as Mesh;
			}
		}

		public static bool IsAnActualSprite(Transform new_sprite) {
			if (new_sprite == null) return false;
			var mesh_filter = new_sprite.GetComponent<MeshFilter>();
			var sp_renderer = new_sprite.GetComponent<Renderer>();
			if ((mesh_filter == null) || (sp_renderer == null)) {
				return false;
			} else {
				var mesh = mesh_filter.sharedMesh;
				if (mesh == null) return false;
				if (sp_renderer.sharedMaterials.Length != 1) return false;
				if (mesh.vertices.Length != 4) return false;
				if ((mesh.triangles.Length/3) != 2) return false;
			}
			return true;
		}
		
		public static Mesh MakeSpriteMesh(Texture tex, Mesh ref_mesh=null) {
			float tex_w = (float)tex.width;
			float tex_h = (float)tex.height;
			
			float origin_x = 0.5f;
			float origin_y = 0.5f;
			
			if (ref_mesh != null) {
				var min = ref_mesh.bounds.min;
				var size = ref_mesh.bounds.size;
				origin_x = -min.x / size.x;
				origin_y = -min.y / size.y;
			}
			
//			var mesh_min = new Vector3(-tex_w*origin_x, -tex_h*origin_y, 0);
//			var mesh_w = new Vector3(tex_w, 0, 0);
//			var mesh_h = new Vector3(0, tex_h, 0);
//			
//			var vertices = new Vector3[]{
//				mesh_min,
//				mesh_min + mesh_h,
//				mesh_min + mesh_w + mesh_h,
//				mesh_min + mesh_w,
//			};
//			var uv = new Vector2[]{
//				new Vector2(0f, 0f),
//				new Vector2(0f, 1f),
//				new Vector2(1f, 1f),
//				new Vector2(1f, 0f),
//			};
//			var triangles = new int[]{
//				0, 1, 2,
//				0, 2, 3,
//			};
//			
//			var mesh = new Mesh();
//			mesh.vertices = vertices;
//			mesh.uv = uv;
//			mesh.triangles = triangles;
//			mesh.RecalculateBounds();
			var mesh = MeshUtils.SpriteMesh(new Rect(-tex_w*origin_x, -tex_h*origin_y, tex_w, tex_h));
			mesh.bounds = new Bounds(mesh.bounds.center, mesh.bounds.size + Vector3.forward);
			
			return mesh;
		}
	}
	
	class SpriteTexDragger_Create : SpriteTexDragger {
		// Singleton pattern
		private SpriteTexDragger_Create() {}
		static SpriteTexDragger_Create _instance = null;
		public static SpriteTexDragger_Create instance {
			get {
				if (_instance == null) _instance = new SpriteTexDragger_Create();
				return _instance;
			}
		}
		
		public static void OnSceneGUI() {
			instance._OnSceneGUI();
		}
		
		public static void Dispose() {
			instance.Cleanup();
		}
		
		List<DragMaterial> drag_materials = new List<DragMaterial>();
		List<Transform> drag_transforms = new List<Transform>();
		
		override protected bool VerifyEvent() {
			Event e = Event.current;
			return CheckModifiers(e, MODIFIERS_CREATE);
		}
		
		override protected bool ProcessDrag() {
			if (drag_materials.Count == 0) {
				// If Undo is registered here, Unity hangs/crashes!
				LoadDragMaterials();
				CreateSprites();
			}
			
			bool proceed = (drag_materials.Count != 0);
			
			if (proceed) {
				// Move the sprites
				Vector3 p;
				Event e = Event.current;
				if (FindIntersection(e.mousePosition, out p)) {
					var active_tfm = GetActiveTransform();
					if (active_tfm != null) {
						p = active_tfm.InverseTransformPoint(p);
					}
					foreach (var tfm in drag_transforms) {
						tfm.localPosition = new Vector3(p.x, p.y, tfm.localPosition.z);
					}
				} else {
					// Don't move sprites blindly!
					proceed = false;
				}
			}
			
			return proceed;
		}
		
		override protected void RegisterUndo() {
			// Store relevant object properties to recreate objects
			// after the Undo state is registered
			var tmp_objs = new List<ObjTmpStorage>();
			foreach (var tfm in drag_transforms) {
				tmp_objs.Add(new ObjTmpStorage(tfm));
				Object.DestroyImmediate(tfm.gameObject);
			}
			
			Undo.RegisterSceneUndo("Drag texture to sprite(s)");
			
			for (int i = 0; i < drag_transforms.Count; i++) {
				drag_transforms[i] = tmp_objs[i].Recreate();
			}
		}
		private class ObjTmpStorage {
			string name;
			Material material;
			Mesh mesh;
			Transform parent;
			Vector3 position;
			Quaternion rotation;
			Vector3 scale;
			
			public ObjTmpStorage(Transform tfm) {
				name = tfm.name;
				material = tfm.GetComponent<Renderer>().sharedMaterial;
				mesh = tfm.GetComponent<MeshFilter>().sharedMesh;
				parent = tfm.parent;
				position = tfm.localPosition;
				rotation = tfm.localRotation;
				scale = tfm.localScale;
			}
			
			public Transform Recreate() {
				SpriteTexDragger.SaveAssets(ref material, ref mesh);

				var game_obj = new GameObject(name);
				
				game_obj.AddComponent<MeshRenderer>();
				game_obj.GetComponent<Renderer>().sharedMaterial = material;
				
				var mesh_filter = game_obj.AddComponent<MeshFilter>();
				mesh_filter.sharedMesh = mesh;
				
				var tfm = game_obj.transform;
				tfm.parent = parent;
				tfm.localPosition = position;
				tfm.localRotation = rotation;
				tfm.localScale = scale;
				
				// Also add a collider (to be able to pick it afterwards)
				//game_obj.AddComponent<BoxCollider>();
				AddSpriteCollider(game_obj);
				
				return tfm;
			}
		}
		
		override protected void Clear() {
			ClearDragTransforms();
			ClearDragMaterials();
		}
		
		override protected void Cleanup() {
			UnloadDragTransforms();
			UnloadDragMaterials();
		}
		
		void LoadDragMaterials() {
			drag_materials = ConvertDragMaterials();
		}
		
		void ClearDragMaterials() {
			drag_materials.Clear();
		}
		
		void UnloadDragMaterials() {
			foreach (var dragmat in drag_materials) {
				dragmat.Cleanup();
			}
			ClearDragMaterials();
		}
		
		void CreateSprites() {
			if (drag_materials.Count == 0) return;
			
			var active_tfm = GetActiveTransform();
			
			foreach (var dragmat in drag_materials) {
				var material = dragmat.material;
				var mesh = dragmat.mesh;
				var tex = material.mainTexture;
				string name = tex.name;
				if (name.Length == 0) name = material.name;
				
				var game_obj = new GameObject(name);
				
				game_obj.AddComponent<MeshRenderer>();
				game_obj.GetComponent<Renderer>().sharedMaterial = material;
				
				var mesh_filter = game_obj.AddComponent<MeshFilter>();
				if (mesh == null) mesh = MakeSpriteMesh(tex);
				mesh_filter.sharedMesh = mesh;
				
				var tfm = game_obj.transform;
				tfm.parent = active_tfm;
				tfm.localPosition = -Vector3.forward * drag_transforms.Count;
				tfm.localRotation = Quaternion.identity;
				tfm.localScale = Vector3.one;
				
				drag_transforms.Add(tfm);
			}
		}
		
		void ClearDragTransforms() {
			drag_transforms.Clear();
		}
		
		void UnloadDragTransforms() {
			foreach (var tfm in drag_transforms) {
				Object.DestroyImmediate(tfm.gameObject);
			}
			ClearDragTransforms();
		}
	}
	
	class SpriteTexDragger_Change : SpriteTexDragger {
		// Singleton pattern
		private SpriteTexDragger_Change() {}
		static SpriteTexDragger_Change _instance = null;
		public static SpriteTexDragger_Change instance {
			get {
				if (_instance == null) _instance = new SpriteTexDragger_Change();
				return _instance;
			}
		}
		
		public static void OnSceneGUI() {
			instance._OnSceneGUI();
		}
		
		public static void Dispose() {
			instance.Cleanup();
		}
		
		DragMaterial drag_material = null;
		Transform current_sprite = null;
		Material stored_material = null;
		Mesh stored_mesh = null;
		
		override protected bool VerifyEvent() {
			Event e = Event.current;
			return CheckModifiers(e, MODIFIERS_REPLACE);
		}
		
		override protected bool ProcessDrag() {
			if (drag_material == null) {
				LoadDragMaterial();
			}
			if (drag_material == null) return false;
			
			Event e = Event.current;
			var new_sprite = Raycast(e.mousePosition);

			if ((new_sprite != null) && (new_sprite != current_sprite)) {
				// Make sure we deal with an actual sprite
				if (!IsAnActualSprite(new_sprite)) new_sprite = null;
			}
			
			SetActiveSprite(new_sprite);
			return (new_sprite != null);
		}
		
		override protected void RegisterUndo() {
			var new_sprite = current_sprite;
			SetActiveSprite(null);
			
			Undo.RegisterSceneUndo("Drag texture to sprite");
			
			SetActiveSprite(new_sprite, true, true);
		}
		
		override protected void Clear() {
			ClearActiveSprite();
			ClearDragMaterial();
		}
		
		override protected void Cleanup() {
			UnloadActiveSprite();
			UnloadDragMaterial();
		}
		
		void SetActiveSprite(Transform new_sprite,
			bool recreate_collider=false, bool save_assets=false) {
			if (current_sprite == new_sprite) return;
			
			if (current_sprite != null) {
				var sp_renderer = current_sprite.GetComponent<Renderer>();
				sp_renderer.sharedMaterial = stored_material;
				stored_material = null;
				
				var mesh_filter = current_sprite.GetComponent<MeshFilter>();
				mesh_filter.sharedMesh = stored_mesh;
				stored_mesh = null;
			}
			
			current_sprite = new_sprite;
			
			if (current_sprite != null) {
				var material = drag_material.material;
				var mesh = drag_material.mesh;

				var sp_renderer = current_sprite.GetComponent<Renderer>();
				stored_material = sp_renderer.sharedMaterial;
				sp_renderer.sharedMaterial = material;
				
				var tex = drag_material.material.mainTexture;
				
				bool overwrite_mesh = false;
				
				var mesh_filter = current_sprite.GetComponent<MeshFilter>();
				stored_mesh = mesh_filter.sharedMesh;
				if ((mesh == null) || !AssetDatabase.Contains(mesh_filter.sharedMesh)) {
					mesh = MakeSpriteMesh(tex, stored_mesh);
				} else if (!SpriteMeshSizeCheck(tex, mesh)) {
					mesh = MakeSpriteMesh(tex, stored_mesh);
					overwrite_mesh = true;
				}
				mesh_filter.sharedMesh = mesh;

				if (save_assets) {
					material = sp_renderer.sharedMaterial;
					
					Mesh no_mesh = null; // don't save the mesh
					SaveAssets(ref material, ref no_mesh);
					
					if (overwrite_mesh) {
						MeshUtils.CopyMesh(mesh, drag_material.mesh);
						
						string dir, filename;
						FileUtils.SplitAssetPath(material, out dir, out filename);
						
						string path = MeshSavePath(dir, filename);
						AssetDatabase.SaveAssets();
						AssetDatabase.ImportAsset(path);
						mesh = AssetDatabase.LoadAssetAtPath(path, typeof(Mesh)) as Mesh;
						
						mesh_filter.sharedMesh = mesh;
					}
					
					sp_renderer.sharedMaterial = material;
				}

				if (recreate_collider) {
					var collider = current_sprite.GetComponent<Collider>();
					if (collider != null) Object.DestroyImmediate(collider);
					var collider2d = current_sprite.GetComponent<Collider2D>();
					if (collider2d != null) Object.DestroyImmediate(collider2d);
					//current_sprite.gameObject.AddComponent<BoxCollider>();
					AddSpriteCollider(current_sprite.gameObject);
				}
			}
		}
		
		static bool SpriteMeshSizeCheck(Texture tex, Mesh mesh) {
			var bounds = MeshUtils.CalculateBounds(mesh);
			float dw = Mathf.Abs(bounds.size.x - tex.width);
			float dh = Mathf.Abs(bounds.size.y - tex.height);
			return (dw < 0.5f) && (dh < 0.5f);
		}
		
		void ClearActiveSprite() {
			current_sprite = null;
			stored_material = null;
			stored_mesh = null;
		}
		
		void UnloadActiveSprite() {
			SetActiveSprite(null);
			ClearActiveSprite();
		}
		
		void LoadDragMaterial() {
			var dragmats = ConvertDragMaterials(true);
			if (dragmats.Count != 0) drag_material = dragmats[0];
		}
		
		void ClearDragMaterial() {
			drag_material = null;
		}
		
		void UnloadDragMaterial() {
			if (drag_material != null) {
				drag_material.Cleanup();
			}
			ClearDragMaterial();
		}
	}
}
