﻿using UnityEngine;
using System.Collections;

[RequireComponent(typeof(Light))]
public class BiniUberLight : MonoBehaviour {
	Light light = null;

	void Start() {
		light = GetComponent<Light>();
	}
	
	void LateUpdate() {
		if (!light) return;
		if (light.type != LightType.Point) return;
		Vector4 pos_size, color;
		pos_size = (Vector4)light.transform.position;
		pos_size.w = 1f/(light.range*light.range);
		color.x = light.color.r;
		color.y = light.color.g;
		color.z = light.color.b;
		color.w = light.intensity;
		Shader.SetGlobalVector("_LightPosSize", pos_size);
		Shader.SetGlobalVector("_LightColor", color);
	}
}
