Shader "BiniUber/RGBA/TVC/Blend: LinearBurn" {
	Properties {
		[Header(Bini Uber)]
		[BiniUberHeader] _BiniUber ("BiniUber", Float) = 0
		[Space]
		
		_Color ("Main Color", Color) = (1,1,1,1)
		
		_MainTex ("Main Texture (RGBA)", 2D) = "white" {}
	}
	
	SubShader {
		Tags { "Queue"="Transparent" "IgnoreProjector"="True" "RenderType"="Transparent" }
		//Cull Off
		ZTest Always
		ZWrite Off
		Lighting Off
		Fog { Mode Off }
		
		BindChannels {
			Bind "Vertex", vertex
			Bind "TexCoord", texcoord
			Bind "Color", color
		}
		
		Pass {
			Blend One One
			BlendOp RevSub
			
			SetTexture[_MainTex] { combine texture * primary }
			SetTexture[_MainTex] { ConstantColor [_Color] combine previous * constant }
			
			SetTexture [_MainTex] {
				ConstantColor (0,0,0,1)
				combine one-previous lerp (previous) constant, constant
			}
		}
	}
	
	SubShader {
		Tags { "Queue"="Transparent" "IgnoreProjector"="True" "RenderType"="Transparent" }
		//Cull Off
		ZTest Always
		ZWrite Off
		Lighting Off
		Fog { Mode Off }
		
		Pass {
			Blend One One
			BlendOp RevSub
			
			CGPROGRAM
			#pragma vertex vert
			#pragma fragment frag
			
			#include "UnityCG.cginc"
			
			#define USE_VERTEX_COLOR 1
			#define USE_CONSTANT_COLOR 1
			#include "BiniUber.cginc"
			
			DECLARE_CONSTANT_COLOR
			DECLARE_TEX(_MainTex)
			
			struct appdata {
				DEFAULT_VERTEX_INPUT
			};
			
			struct v2f {
				DEFAULT_FRAGMENT_INPUT
			};
			
			v2f vert(appdata v) {
				v2f o;
				DEFAULT_VERTEX_CODE
				return o;
			}
			
			fixed4 frag(v2f i) : COLOR0 {
				fixed4 final = TINT(TEX_RGBA(_MainTex, i.texcoord0));
				return fixed4((1 - final.rgb) * final.a, final.a);
			}
			ENDCG
		}
	}
}
