using UnityEngine;
using UnityEditor;
using System.Collections.Generic;

public class BiniUberHeaderDrawer : MaterialPropertyDrawer {
	string[] shader_types = null;

	void InitShaderTypes() {
		if (shader_types != null) return;

		var _shader_types_set = new HashSet<string>();

		var guids = AssetDatabase.FindAssets("BiniUber t:Shader");
		foreach (var guid in guids) {
			string path = AssetDatabase.GUIDToAssetPath(guid);
			string name = System.IO.Path.GetFileNameWithoutExtension(path);
			var shader = AssetDatabase.LoadAssetAtPath<Shader>(path);
			var info = (BiniUberShader.Info)shader;
			_shader_types_set.Add(info.Name);
		}

		var _shader_types = new List<string>(_shader_types_set);
		_shader_types.Sort();
		shader_types = _shader_types.ToArray();
	}

	GUIStyle label_style;
	Color default_label_color;

	MaterialProperty prop;
	MaterialEditor editor;

	public BiniUberHeaderDrawer() {
		InitShaderTypes();

		label_style = new GUIStyle(EditorStyles.label);
		default_label_color = label_style.normal.textColor;
	}

	bool ShaderTypePopup(Rect rect, bool mixed, ref string value) {
		int i = System.Array.IndexOf(shader_types, value);
		if (i < 0) i = 0;
		EditorGUI.BeginChangeCheck();
		EditorGUI.showMixedValue = mixed;
		i = EditorGUI.Popup(rect, i, shader_types);
		EditorGUI.showMixedValue = false;
		if (EditorGUI.EndChangeCheck()) {
			value = shader_types[i];
			return true;
		}
		return false;
	}
	
	bool ShaderFlag(Rect rect, string label, bool mixed, ref bool value, int side=0) {
		if (mixed) value = false;
		EditorGUI.BeginChangeCheck();
		GUIStyle style;
		if (side < 0) {
			style = EditorStyles.miniButtonLeft;
		} else if (side > 0) {
			style = EditorStyles.miniButtonRight;
		} else {
			style = EditorStyles.miniButtonMid;
		}
		value = EditorGUI.Toggle(rect, value, style);
		label_style.normal.textColor = (mixed ? Color.gray : default_label_color);
		EditorGUI.LabelField(rect, label, label_style);
		return EditorGUI.EndChangeCheck();
	}

	public override void OnGUI(Rect position, MaterialProperty prop, string label, MaterialEditor editor) {
		this.prop = prop; this.editor = editor;

		bool mixed_name, mixed_alpha, mixed_color, mixed_vertex;
		var info = Collect(prop, out mixed_name, out mixed_alpha, out mixed_color, out mixed_vertex);

		var rect = position;
		float total_width = (rect.width+12);
		float platform_width = 16;
		float alpha_width = 48;
		float color_width = 36;
		float vertex_width = 43;
		float selector_space = 14;
		float selector_width = total_width - selector_space - platform_width - alpha_width - color_width - vertex_width;

		rect.width = platform_width;
		if (OverridesButton(rect)) PopupWindow.Show(rect, new OverridesPopup(prop));

		EditorGUI.BeginDisabledGroup(!info);

		rect.x += platform_width;
		rect.width = selector_width;
		if (ShaderTypePopup(rect, mixed_name, ref info.Name)) ApplyName(prop, info);

		rect.x += selector_width + selector_space - platform_width;
		rect.width = alpha_width;
		if (ShaderFlag(rect, "RGB+A", mixed_alpha, ref info.UseSeparateAlpha, -1)) ApplyAlpha(prop, info);

		rect.x += alpha_width;
		rect.width = color_width;
		if (ShaderFlag(rect, "Color", mixed_color, ref info.UseConstantColor, 0)) ApplyColor(prop, info);

		rect.x += color_width;
		rect.width = vertex_width;
		if (ShaderFlag(rect, "Vertex", mixed_vertex, ref info.UseVertexColor, 1)) ApplyVertex(prop, info);

		EditorGUI.EndDisabledGroup();

		EditorGUI.showMixedValue = false;

		SyncOverrides(Event.current.type);
	}

	static bool sync_overrides = false;
	void SyncOverrides(EventType event_type) {
		switch (Event.current.type) {
		case EventType.Used:
		case EventType.ExecuteCommand:
		case EventType.MouseUp:
		case EventType.KeyUp:
			sync_overrides = true;
			SyncOverrides();
			break;
		case EventType.Repaint:
			if (sync_overrides) {
				sync_overrides = false;
				SyncOverrides();
			}
			break;
		}
	}
	void SyncOverrides() {
		foreach (Material mat in prop.targets) {
			BiniUberShader.SyncReadPlatformOverrides(mat, active_platform.ToString());
		}
	}

	BiniUberShader.Info Collect(MaterialProperty prop, out bool mixed_name, out bool mixed_alpha, out bool mixed_color, out bool mixed_vertex) {
		mixed_name = mixed_alpha = mixed_color = mixed_vertex = true;

		var info0 = default(BiniUberShader.Info);
		var info = default(BiniUberShader.Info);
		bool info_initialized = false;

		foreach (Material mat in prop.targets) {
			info = (BiniUberShader.Info)mat.shader;
			if (info) {
				if (!info_initialized) {
					mixed_name = mixed_alpha = mixed_color = mixed_vertex = false;
					info_initialized = true;
				} else {
					mixed_name = (info.Name != info0.Name);
					mixed_alpha = (info.UseSeparateAlpha != info0.UseSeparateAlpha);
					mixed_color = (info.UseConstantColor != info0.UseConstantColor);
					mixed_vertex = (info.UseVertexColor != info0.UseVertexColor);
				}
				info0 = info;
			}
		}

		return info;
	}

	void ApplyName(MaterialProperty prop, BiniUberShader.Info info) {
		foreach (Material mat in prop.targets) {
			Undo.RecordObject(mat, "Set shader");
			BiniUberShader.Set(mat, info.Name);
		}
		sync_overrides = true;
	}
	void ApplyAlpha(MaterialProperty prop, BiniUberShader.Info info) {
		foreach (Material mat in prop.targets) {
			Undo.RecordObject(mat, "Toggle RGB+A");
			BiniUberShader.UseSeparateAlpha(mat, info.UseSeparateAlpha);
		}
		sync_overrides = true;
	}
	void ApplyColor(MaterialProperty prop, BiniUberShader.Info info) {
		foreach (Material mat in prop.targets) {
			Undo.RecordObject(mat, "Toggle Color");
			BiniUberShader.UseConstantColor(mat, info.UseConstantColor);
		}
		sync_overrides = true;
	}
	void ApplyVertex(MaterialProperty prop, BiniUberShader.Info info) {
		foreach (Material mat in prop.targets) {
			Undo.RecordObject(mat, "Toggle Vertex");
			BiniUberShader.UseVertexColor(mat, info.UseVertexColor);
		}
		sync_overrides = true;
	}

	static BuildTargetGroup TargetToGroup(BuildTarget target) {
		switch (target) {
		case BuildTarget.Android: return BuildTargetGroup.Android;
		case BuildTarget.BlackBerry: return BuildTargetGroup.BlackBerry;
		case BuildTarget.StandaloneGLESEmu: return BuildTargetGroup.GLESEmu;
		case BuildTarget.iOS: return BuildTargetGroup.iOS;
		case BuildTarget.Nintendo3DS: return BuildTargetGroup.Nintendo3DS;
		case BuildTarget.PS3: return BuildTargetGroup.PS3;
		case BuildTarget.PS4: return BuildTargetGroup.PS4;
		case BuildTarget.PSM: return BuildTargetGroup.PSM;
		case BuildTarget.PSP2: return BuildTargetGroup.PSP2;
		case BuildTarget.SamsungTV: return BuildTargetGroup.SamsungTV;
		case BuildTarget.Tizen: return BuildTargetGroup.Tizen;
		case BuildTarget.tvOS: return BuildTargetGroup.tvOS;
		case BuildTarget.WebGL: return BuildTargetGroup.WebGL;
		case BuildTarget.WebPlayer: return BuildTargetGroup.WebPlayer;
		case BuildTarget.WebPlayerStreamed: return BuildTargetGroup.WebPlayer;
		case BuildTarget.WiiU: return BuildTargetGroup.WiiU;
		case BuildTarget.WP8Player: return BuildTargetGroup.WP8;
		case BuildTarget.WSAPlayer: return BuildTargetGroup.WSA;
		case BuildTarget.XBOX360: return BuildTargetGroup.XBOX360;
		case BuildTarget.XboxOne: return BuildTargetGroup.XboxOne;
		}
		return BuildTargetGroup.Standalone;
	}
	static BuildTargetGroup active_platform {
		get { return TargetToGroup(EditorUserBuildSettings.activeBuildTarget); }
	}

	bool OverridesButton(Rect rect) {
		var style = EditorStyles.radioButton; // EditorStyles.foldout ?
		return GUI.Button(rect, new GUIContent("", "Platform-specific overrides"), style);
	}

	public class OverridesPopup : PopupWindowContent {
		class PropState {
			public bool same = true;
			public bool overrides_on = false;
			public bool overrides_off = false;
			public int overridden {
				get {
					if (overrides_on & overrides_off) return 0;
					if (overrides_on) return 1;
					if (overrides_off) return -1;
					return -1;
				}
			}
			public void Reset() {
				same = true;
				overrides_on = false;
				overrides_off = false;
			}
		}

		class MatMap {
			public Material mat;
			public Dictionary<string, BiniUberShader.PlatformOverride> map;

			public MatMap(Material mat, string platform_name, string platform_current) {
				this.mat = mat;

				if (platform_name == platform_current) { // make sure it's up-to-date
					BiniUberShader.ReadPlatformOverrides(mat, platform_name);
				}

				map = BiniUberShader.GetPlatformOverrides(mat, platform_name);

				if (string.IsNullOrEmpty(platform_name)) { // make sure default is up-to-date
					var curr_map = BiniUberShader.GetPlatformOverrides(mat, platform_current);
					foreach (var kv in map) {
						if (!curr_map.ContainsKey(kv.Key)) kv.Value.Read(mat);
					}
				}
			}
		}

		MatMap[][] platforms_mat_maps;

		MatMap[] mat_maps_default;
		MatMap[] mat_maps_current;
		MatMap[] mat_maps_sel;

		List<Material> shader_mats;
		Dictionary<string, string> props_dict;
		HashSet<Shader> shaders_set;

		string platform_current;
		int platform_current_i;
		List<KeyValuePair<string, string>> platforms_kv;
		PropState[] platforms_state;
		bool[] platforms_selection;
		int platforms_sel_n = 0, platforms_sel_i = -1;

		int uber_count = 0;
		List<KeyValuePair<string, string>> props_kv;
		PropState[] props_state;
		bool[] props_selection;
		int props_sel_n = 0;

		MaterialProperty prop;
		Editor editor; // needed to access Editor.Repaint()

		public OverridesPopup(MaterialProperty prop) : base() {
			this.prop = prop;
			editor = new Editor();
			InitPlatforms();
			InitMatMaps();
			InitProps();
			RefreshStates();
		}

		void InitPlatforms() {
			platform_current = active_platform.ToString();

			platforms_kv = new List<KeyValuePair<string, string>>();
			platforms_kv.Add(new KeyValuePair<string, string>("", "<Default>"));
			foreach (var platform_name in System.Enum.GetNames(typeof(BuildTargetGroup))) {
				if (platform_name == "Unknown") continue;
				if (IsObsolete(platform_name)) continue;
				if (platform_name == "WebPlayer") continue; // also obsolete
				platforms_kv.Add(new KeyValuePair<string, string>(platform_name, platform_name));
			}

			platforms_kv.Sort((item0, item1) => (item0.Value.CompareTo(item1.Value)));

			platforms_state = new PropState[platforms_kv.Count];
			for (int i = 0; i < platforms_state.Length; i++) {
				platforms_state[i] = new PropState();
			}

			platforms_selection = new bool[platforms_kv.Count];
			for (int i = 0; i < platforms_kv.Count; i++) {
				if (platforms_kv[i].Key == platform_current) {
					platforms_selection[i] = true;
					platform_current_i = i;
					break;
				}
			}
		}
		static bool IsObsolete(string platform_name) {
			var value = (BuildTargetGroup)System.Enum.Parse(typeof(BuildTargetGroup), platform_name);
			var fi = value.GetType().GetField(platform_name);
			var attributes = fi.GetCustomAttributes(typeof(System.ObsoleteAttribute), false);
			return (attributes != null && attributes.Length > 0);
		}

		void InitMatMaps() {
			shader_mats = new List<Material>();
			foreach (Material mat in prop.targets) { // Collect
				var shader = mat.shader;
				if (!shader) continue;
				shader_mats.Add(mat);
			}

			props_dict = new Dictionary<string, string>();
			shaders_set = new HashSet<Shader>();

			platforms_mat_maps = new MatMap[platforms_kv.Count][];
			for (int i = 0; i < platforms_kv.Count; i++) {
				string platform_name = platforms_kv[i].Key;

				var mat_maps = new MatMap[shader_mats.Count];
				platforms_mat_maps[i] = mat_maps;

				for (int j = 0; j < shader_mats.Count; j++) {
					var mat_map = new MatMap(shader_mats[j], platform_name, platform_current);
					mat_maps[j] = mat_map;
					CollectProps(mat_map);
				}
			}

			mat_maps_default = platforms_mat_maps[0];
			mat_maps_current = platforms_mat_maps[platform_current_i];
		}

		void CollectProps(MatMap mat_map) {
			var shader = mat_map.mat.shader;
			var info = (BiniUberShader.Info)shader;
			if (info) {
				BiniUberShader.PlatformOverride po = null;
				if (mat_map.map.TryGetValue("?Shader", out po)) info.Name = po.data;
				if (mat_map.map.TryGetValue("?RGB+A", out po)) info.UseSeparateAlpha = po.AsBool();
				if (mat_map.map.TryGetValue("?Color", out po)) info.UseConstantColor = po.AsBool();
				if (mat_map.map.TryGetValue("?Vertex", out po)) info.UseVertexColor = po.AsBool();
				shader = (Shader)info;
			}
			if (shaders_set.Contains(shader)) return;
			foreach (var kv in BiniUberShader.IterOverridableProps(shader)) {
				if (!props_dict.ContainsKey(kv.Key)) props_dict.Add(kv.Key, kv.Value);
			}
			shaders_set.Add(shader);
		}

		void InitProps() {
			props_kv = new List<KeyValuePair<string, string>>();
			foreach (var kv in props_dict) {
				if (kv.Key.StartsWith("?")) uber_count++;
				props_kv.Add(kv);
			}

			props_kv.Sort((item0, item1) => { // Sort
				bool uber0 = item0.Key.StartsWith("?");
				bool uber1 = item1.Key.StartsWith("?");
				if (!(uber0 | uber1)) return item0.Value.CompareTo(item1.Value);
				if (uber0 & !uber1) return -1;
				if (!uber0 & uber1) return 1;
				for (int i = 0; i < BiniUberShader.uber_prop_names.Length; i++) {
					string uber_name = BiniUberShader.uber_prop_names[i];
					if (uber_name == item0.Key) return -1;
					if (uber_name == item1.Key) return 1;
				}
				return 0;
			});

			for (int i = 0; i < props_kv.Count; i++) { // Rename
				if (!PropIsST(i)) continue;
				var kv = props_kv[i];
				kv = new KeyValuePair<string, string>(kv.Key, " ^ Tiling & Offset");
				props_kv[i] = kv;
			}

			props_state = new PropState[props_kv.Count];
			for (int i = 0; i < props_state.Length; i++) {
				props_state[i] = new PropState();
			}

			props_selection = new bool[props_kv.Count];
		}
		bool PropIsST(int i) {
			if (i == 0) return false;
			var kv0 = props_kv[i-1];
			var kv = props_kv[i];
			if (kv.Key != kv0.Key+"_ST") return false;
			if (kv.Value != kv0.Value+BiniUberShader.texture_ST_suffix_name) return false;
			return true;
		}

		BiniUberShader.PlatformOverride GetFromMap(Dictionary<string, BiniUberShader.PlatformOverride> map, string key) {
			BiniUberShader.PlatformOverride po = null;
			if (map.TryGetValue(key, out po)) { return po; } else { return null; }
		}
		string GetPOData(BiniUberShader.PlatformOverride po) {
			if (po == null) return null;
			if (string.IsNullOrEmpty(po.data)) return null;
			return po.data;
		}

		void RefreshStates() {
			platforms_sel_n = 0;
			platforms_sel_i = -1;
			for (int i = 0; i < platforms_state.Length; i++) {
				platforms_state[i].Reset();
				if (!platforms_selection[i]) continue;
				platforms_sel_n++;
				platforms_sel_i = i;
			}
			if (platforms_sel_n != 1) platforms_sel_i = -1;

			props_sel_n = 0;
			for (int i = 0; i < props_state.Length; i++) {
				props_state[i].Reset();
				if (!props_selection[i]) continue;
				props_sel_n++;
			}

			mat_maps_sel = null;
			if (platforms_sel_i >= 0) {
				mat_maps_sel = platforms_mat_maps[platforms_sel_i];
			}

			for (int i = 0; i < platforms_kv.Count; i++) {
				var mat_maps = platforms_mat_maps[i];
				for (int j = 0; j < mat_maps.Length; j++) {
					var map = mat_maps[j].map;
					var map_default = mat_maps_default[j].map;
					var map_sel = (mat_maps_sel != null ? mat_maps_sel[j].map : null);

					if (platforms_selection[i] & (i != 0)) {
						for (int k = 0; k < props_kv.Count; k++) {
							var s = props_state[k];
							if (!s.same & s.overrides_on & s.overrides_off) continue; // won't change anyway
							var kv = props_kv[k];
							var po = GetFromMap(map, kv.Key);
							var po_default = GetFromMap(map_default, kv.Key);
							if (s.same) s.same = ((po == null) || (GetPOData(po) == GetPOData(po_default)));
							if (po != null) { s.overrides_on = true; } else { s.overrides_off = true; }
						}
					}

					if (props_sel_n > 0) {
						var s = platforms_state[i];
						if (s.same | !s.overrides_on | !s.overrides_off) {
							for (int k = 0; k < props_kv.Count; k++) {
								if (!props_selection[k]) continue;
								var kv = props_kv[k];
								var po = GetFromMap(map, kv.Key);
								if (platforms_sel_n == 1) {
									var po_sel = GetFromMap(map_sel, kv.Key);
									var po_default = GetFromMap(map_default, kv.Key);
									if (s.same) s.same = (GetPOData(po??po_default) == GetPOData(po_sel??po_default));
								}
								if (po != null) { s.overrides_on = true; } else { s.overrides_off = true; }
							}
						}
					}
				}
			}

			// Default cannot "have overrides"
			platforms_state[0].overrides_on = false;
			platforms_state[0].overrides_off = false;
		}

		bool ShouldWritePO(int id, int j, string prop_key) {
			if (id == platform_current_i) return true;
			if ((id == 0) && !mat_maps_current[j].map.ContainsKey(prop_key)) return true;
			return false;
		}
		void ReadPlatform(int id) {
			var platform_name = platforms_kv[platforms_sel_i].Key;
			var mat_maps = platforms_mat_maps[id];
			for (int j = 0; j < mat_maps.Length; j++) {
				var mat_map = mat_maps[j];
				var mat_map_sel = mat_maps_sel[j];
				if (props_sel_n > 0) {
					Undo.RecordObject(mat_map.mat, "Read Material Override");
					for (int k = 0; k < props_kv.Count; k++) {
						if (!props_selection[k]) continue;
						var prop_key = props_kv[k].Key;
						var po = mat_map.map[prop_key];
						mat_map_sel.map[prop_key] = po;
						BiniUberShader.SetPlatformOverrides(mat_map.mat, platform_name, mat_map_sel.map);
						if (ShouldWritePO(platforms_sel_i, j, prop_key)) po.Write(mat_map.mat);
					}
				}
			}
			RefreshStates();
			editor.Repaint();
		}
		void WritePlatform(int id) {
			var platform_name = platforms_kv[id].Key;
			var mat_maps = platforms_mat_maps[id];
			for (int j = 0; j < mat_maps.Length; j++) {
				var mat_map = mat_maps[j];
				var mat_map_sel = mat_maps_sel[j];
				if (props_sel_n > 0) {
					Undo.RecordObject(mat_map.mat, "Write Material Override");
					for (int k = 0; k < props_kv.Count; k++) {
						if (!props_selection[k]) continue;
						var prop_key = props_kv[k].Key;
						var po = mat_map_sel.map[prop_key];
						mat_map.map[prop_key] = po;
						BiniUberShader.SetPlatformOverrides(mat_map.mat, platform_name, mat_map.map);
						if (ShouldWritePO(id, j, prop_key)) po.Write(mat_map.mat);
					}
				}
			}
			RefreshStates();
			editor.Repaint();
		}
		void OverridePlatform(int id, int overridden) {
			if (overridden == 0) return; // just in case
			var platform_name = platforms_kv[id].Key;
			var mat_maps = platforms_mat_maps[id];
			for (int j = 0; j < mat_maps.Length; j++) {
				var mat_map = mat_maps[j];
				if (props_sel_n > 0) {
					Undo.RecordObject(mat_map.mat, "Toggle Material Override");
					for (int k = 0; k < props_kv.Count; k++) {
						if (!props_selection[k]) continue;
						BiniUberShader.EnablePlatformOverride(mat_map.mat, platform_name, props_kv[k].Key, (overridden > 0), false, platform_current);
					}
					mat_map.map = BiniUberShader.GetPlatformOverrides(mat_map.mat, platform_name);
				}
			}
			RefreshStates();
			editor.Repaint();
		}

		void ReadProp(int id) {
			for (int i = 0; i < platforms_kv.Count; i++) {
				var platform_name = platforms_kv[i].Key;
				var mat_maps = platforms_mat_maps[i];
				for (int j = 0; j < mat_maps.Length; j++) {
					var mat_map = mat_maps[j];
					var mat_map_default = mat_maps_default[j];
					if (platforms_selection[i] & (i != 0)) {
						Undo.RecordObject(mat_map.mat, "Read Material Override");
						var prop_key = props_kv[id].Key;
						var po = mat_map_default.map[prop_key];
						mat_map.map[prop_key] = po;
						BiniUberShader.SetPlatformOverrides(mat_map.mat, platform_name, mat_map.map);
						if (i == platform_current_i) po.Write(mat_map.mat);
					}
				}
			}
			RefreshStates();
			editor.Repaint();
		}
		void WriteProp(int id) {
			for (int i = 0; i < platforms_kv.Count; i++) {
				var platform_name = platforms_kv[i].Key;
				var mat_maps = platforms_mat_maps[i];
				for (int j = 0; j < mat_maps.Length; j++) {
					var mat_map = mat_maps[j];
					var mat_map_default = mat_maps_default[j];
					if (platforms_selection[i] & (i != 0)) {
						Undo.RecordObject(mat_map.mat, "Write Material Override");
						var prop_key = props_kv[id].Key;
						var po = mat_map.map[prop_key];
						mat_map_default.map[prop_key] = po;
						BiniUberShader.SetPlatformOverrides(mat_map.mat, null, mat_map_default.map);
						if (!mat_maps_current[j].map.ContainsKey(prop_key)) po.Write(mat_map.mat);
					}
				}
			}
			RefreshStates();
			editor.Repaint();
		}
		void OverrideProp(int id, int overridden) {
			if (overridden == 0) return; // just in case
			for (int i = 0; i < platforms_kv.Count; i++) {
				var platform_name = platforms_kv[i].Key;
				var mat_maps = platforms_mat_maps[i];
				for (int j = 0; j < mat_maps.Length; j++) {
					var mat_map = mat_maps[j];
					if (platforms_selection[i] & (i != 0)) {
						Undo.RecordObject(mat_map.mat, "Toggle Material Override");
						BiniUberShader.EnablePlatformOverride(mat_map.mat, platform_name, props_kv[id].Key, (overridden > 0), false, platform_current);
						mat_map.map = BiniUberShader.GetPlatformOverrides(mat_map.mat, platform_name);
					}
				}
			}
			RefreshStates();
			editor.Repaint();
		}

		public override Vector2 GetWindowSize() {
			return window_size;
		}

		public override void OnOpen() {
			//Debug.Log("Popup opened: " + this);
		}

		public override void OnClose() {
			//Debug.Log("Popup closed: " + this);
			sync_overrides = true;
		}

		Texture2D bkg_tex;
		GUIStyle style_normal, style_active;
		GUIStyle style_normal_sel, style_active_sel;
		void InitStyles() {
			if (style_normal != null) return;
			bkg_tex = new Texture2D(1, 1);
			bkg_tex.SetPixel(0, 0, new Color(0f, 0.5f, 1f, 0.25f));
			bkg_tex.Apply();
			style_normal = new GUIStyle(GUI.skin.label);
			style_active = new GUIStyle(style_normal);
			style_active.fontStyle = FontStyle.Bold;
			style_active.font = EditorStyles.boldFont;
			style_normal_sel = new GUIStyle(style_normal);
			SetStyleBackground(style_normal_sel, bkg_tex);
			style_active_sel = new GUIStyle(style_active);
			SetStyleBackground(style_active_sel, bkg_tex);
			CalcSizes();
		}
		void SetStyleBackground(GUIStyle style, Texture2D background) {
			style.active.background = background;
			style.focused.background = background;
			style.hover.background = background;
			style.normal.background = background;
			style.onActive.background = background;
			style.onFocused.background = background;
			style.onHover.background = background;
			style.onNormal.background = background;
		}
		GUIStyle GetStyle(bool active, bool selected) {
			if (active) {
				return (selected ? style_active_sel : style_active);
			} else {
				return (selected ? style_normal_sel : style_normal);
			}
		}

		string label_valueget = "\u25B2", label_valueset = "\u25BC";
		Color color_valueget = new Color(0.5f, 0f, 0f), color_valueset = new Color(0f, 0.5f, 0f);

		float box_margin = 1;
		float line_height = 0;
		float width_button, width_buttons;
		Vector2 platforms_size;
		Vector2 props_size;
		Vector2 window_size = new Vector2(32, 32); // just some non-zero initial value

		void CalcSizes() {
			line_height = style_active.CalcHeight(new GUIContent("#"), 100);

			width_button = GUI.skin.toggle.normal.background.width - 2;
			width_buttons = width_button * 3;

			foreach (var kv in platforms_kv) {
				var size = style_active.CalcSize(new GUIContent(kv.Value));
				platforms_size.x = Mathf.Max(platforms_size.x, size.x);
				platforms_size.y += size.y;
			}
			platforms_size += Vector2.one * box_margin * 2;
			platforms_size.x += width_buttons;
			if (uber_count > 0) platforms_size.y += 1;

			foreach (var kv in props_kv) {
				var size = style_normal.CalcSize(new GUIContent(kv.Value));
				props_size.x = Mathf.Max(props_size.x, size.x);
				props_size.y += size.y;
			}
			props_size += Vector2.one * box_margin * 2;
			props_size.x += width_buttons;

			window_size.x = platforms_size.x + props_size.x;
			window_size.y = Mathf.Max(platforms_size.y, props_size.y);
			window_size.y += line_height;
		}

		public override void OnGUI(Rect rect) {
			InitStyles();

			GUI.Box(new Rect(0, 0, platforms_size.x, window_size.y), "");
			GUI.Box(new Rect(platforms_size.x, 0, props_size.x, window_size.y), "");

			var rRow = new Rect(0, 0, 0, line_height);

			rRow.x = box_margin;
			rRow.y = box_margin;
			rRow.width = platforms_size.x - box_margin*2;
			for (int i = 0; i < platforms_kv.Count; i++) {
				var kv = platforms_kv[i];
				var s = platforms_state[i];
				var rCol = rRow;
				rCol.width = width_button;
				GUI.enabled = (!s.same) & (props_sel_n > 0) & (platforms_sel_n == 1);
				if (CheckboxButton(rCol, new GUIContent(label_valueget, "Read from "+kv.Value), color_valueget)) ReadPlatform(i);
				rCol.x += rCol.width;
				if (CheckboxButton(rCol, new GUIContent(label_valueset, "Write to "+kv.Value), color_valueset)) WritePlatform(i);
				rCol.x += rCol.width;
				GUI.enabled = (props_sel_n > 0);
				int overridden = UncertainToggle(rCol, s.overridden, new GUIContent("", "Override "+kv.Value), (Event.current.alt?-1:1));
				if (overridden != s.overridden) OverridePlatform(i, overridden);
				GUI.enabled = true;
				rCol.x += rCol.width;
				rCol.width = rRow.xMax - rCol.x;
				if (GUI.Button(rCol, kv.Value, GetStyle((kv.Key == platform_current), platforms_selection[i]))) {
					Select(platforms_selection, i);
				}
				rRow.y += line_height;
			}
			rRow.y = window_size.y - line_height;
			if (GUI.Button(rRow, "(Un)select all")) SelectAll(platforms_selection);

			bool props_enabled = (platforms_sel_n > 0);
			if ((platforms_sel_n == 1) & (platforms_sel_i == 0)) props_enabled = false;

			rRow.x = box_margin + platforms_size.x;
			rRow.y = box_margin;
			rRow.width = props_size.x - box_margin*2;
			for (int i = 0; i < props_kv.Count; i++) {
				var kv = props_kv[i];
				var s = props_state[i];
				if ((uber_count > 0) & (i == uber_count)) {
					GUI.color = new Color(1, 1, 1, 0.25f);
					GUI.Box(new Rect(rRow.x, rRow.y, rRow.width, 1), "");
					GUI.color = Color.white;
					rRow.y += 1;
				}
				var rCol = rRow;
				rCol.width = width_button;
				GUI.enabled = (!s.same) & props_enabled;
				if (CheckboxButton(rCol, new GUIContent(label_valueget, "Read from <Default>"), color_valueget)) ReadProp(i);
				rCol.x += rCol.width;
				GUI.enabled = (!s.same) & props_enabled & (platforms_sel_n == 1);
				if (CheckboxButton(rCol, new GUIContent(label_valueset, "Write to <Default>"), color_valueset)) WriteProp(i);
				rCol.x += rCol.width;
				GUI.enabled = props_enabled;
				int overridden = UncertainToggle(rCol, s.overridden, new GUIContent("", "Override"), (Event.current.alt?-1:1));
				if (overridden != s.overridden) OverrideProp(i, overridden);
				GUI.enabled = true;
				rCol.x += rCol.width;
				rCol.width = rRow.xMax - rCol.x;
				string tooltip = (kv.Key.StartsWith("?") ? "" : kv.Key); // show the shader variable name as a tooltip
				if (GUI.Button(rCol, new GUIContent(kv.Value, tooltip), GetStyle(false, props_selection[i]))) {
					Select(props_selection, i);
				}
				rRow.y += line_height;
			}
			rRow.y = window_size.y - line_height;
			if (GUI.Button(rRow, "(Un)select all")) SelectAll(props_selection);
		}

		bool CheckboxButton(Rect rect, GUIContent content, Color color) {
			bool clicked = GUI.Toggle(rect, false, "");
			var _color = GUI.contentColor;
			GUI.contentColor = color;
			GUI.Label(rect, content, EditorStyles.whiteLabel); // text must be white, or color won't have effect
			GUI.contentColor = _color;
			return clicked;
		}

		int UncertainToggle(Rect rect, int value, GUIContent content, int dflt=-1) {
			if (value != 0) return (GUI.Toggle(rect, (value > 0), content) ? 1 : -1);
			var _color = GUI.color;
			var color = _color;
			color.a *= 0.5f;
			bool t0 = GUI.Toggle(rect, false, content);
			GUI.color = color;
			bool t1 = GUI.Toggle(rect, true, content);
			GUI.color = _color;
			return (t0 ? dflt : 0); // first toggle consumes the event
		}

		void Select(bool[] selection, int i) {
			if (!(Event.current.shift | Event.current.control)) {
				bool b = selection[i];
				int cnt = 0;
				for (int j = 0; j < selection.Length; j++) { if (selection[j]) cnt++; }
				for (int j = 0; j < selection.Length; j++) { selection[j] = false; }
				if (!b | (cnt > 1)) selection[i] = true;
			} else {
				selection[i] = !selection[i];
			}
			RefreshStates();
		}
		void SelectAll(bool[] selection) {
			int cnt = 0;
			for (int j = 0; j < selection.Length; j++) { if (selection[j]) cnt++; }
			for (int j = 0; j < selection.Length; j++) { selection[j] = (cnt < selection.Length); }
			RefreshStates();
		}
	}
}

public class BiniUberGrayscaleDrawer : MaterialPropertyDrawer {
	public override void OnGUI(Rect position, MaterialProperty prop, string label, MaterialEditor editor) {
		EditorGUI.BeginChangeCheck();
		EditorGUI.showMixedValue = prop.hasMixedValue;

		var value = BiniUberShader.Vector4ToGrayscaleMode(prop.vectorValue);
		value = (BiniUberShader.GrayscaleMode)EditorGUI.EnumPopup(position, label, value);

		EditorGUI.showMixedValue = false;
		if (EditorGUI.EndChangeCheck()) {
			prop.vectorValue = BiniUberShader.GrayscaleModeToVector4(value);
		}
	}
}
