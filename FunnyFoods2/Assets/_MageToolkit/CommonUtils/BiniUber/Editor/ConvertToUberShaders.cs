﻿using UnityEngine;
using UnityEditor;
using System.Collections.Generic;

public static class ConvertToUberShaders {
	[MenuItem("Mage/Remove Unused UberShaders", priority = 1)]
	public static void RemoveUnusedUberShaders() {
		var used_shaders = new HashSet<string>();
		var materials = AssetDatabase.FindAssets("t:Material");
		float fi = 0, fn = materials.Length;
		foreach (var guid in materials) {
			fi += 1;
			if (EditorUtility.DisplayCancelableProgressBar("Remove Unused UberShaders", "Analyzing materials...", fi/fn)) return;
			string path = AssetDatabase.GUIDToAssetPath(guid);
			//AssetDatabase.ImportAsset(path);
			var material = AssetDatabase.LoadAssetAtPath<Material>(path);
			if (material && material.shader) used_shaders.Add(material.shader.name);
		}

		var unused_shaders = new HashSet<string>();
		var shaders = AssetDatabase.FindAssets("t:Shader");
		fi = 0; fn = shaders.Length;
		foreach (var guid in shaders) {
			fi += 1;
			if (EditorUtility.DisplayCancelableProgressBar("Remove Unused UberShaders", "Analyzing shaders...", fi/fn)) return;
			string path = AssetDatabase.GUIDToAssetPath(guid);
			//AssetDatabase.ImportAsset(path);
			var shader = AssetDatabase.LoadAssetAtPath<Shader>(path);
			if (!shader) continue;
			if (used_shaders.Contains(shader.name)) continue;
			if (!path.Contains("BiniUber")) continue;
			if (!path.Contains("Resources")) continue;
			unused_shaders.Add(path);
		}

		EditorUtility.ClearProgressBar();

		int n = unused_shaders.Count;
		if (!EditorUtility.DisplayDialog("Remove Unused UberShaders", n+" unused UberShaders found", "Remove", "Cancel")) return;

		string proj_dir = System.IO.Path.GetDirectoryName(Application.dataPath);

		foreach (var path in unused_shaders) {
			if (!path.Contains("/Resources/")) continue;
			string dir_path = System.IO.Path.GetDirectoryName(path);
			string dir_abs = proj_dir+"/"+dir_path;
			string new_path = path.Replace("/Resources/", "/Unused/");
			string new_dir_path = System.IO.Path.GetDirectoryName(new_path);
			string new_dir_abs = proj_dir+"/"+new_dir_path;
			if (System.IO.Directory.Exists(new_dir_abs)) continue;
			System.IO.Directory.CreateDirectory(new_dir_abs);
			System.IO.File.Copy(dir_abs+"/BiniUber.cginc", new_dir_abs+"/BiniUber.cginc", true);
		}

		AssetDatabase.Refresh();

		foreach (var path in unused_shaders) {
			if (!path.Contains("/Resources/")) continue;
			string new_path = path.Replace("/Resources/", "/Unused/");
			AssetDatabase.MoveAsset(path, new_path);
		}

		AssetDatabase.Refresh();

		(new Editor()).Repaint();
	}

	[MenuItem("Mage/Convert To UberShaders", priority = 1)]
	public static void ConvertMain() {
		//alpha_tex_sync_full = EditorUtility.DisplayDialog("Convert To UberShaders", "Main->Alpha texture synchronization", "All", "Only new");
		var materials = GatherMaterials();
		if (materials.Count == 0) return;
		float progress = 0;
		foreach (var material in materials) {
			progress += 1;
			if (EditorUtility.DisplayCancelableProgressBar("Convert To UberShaders", "Processing...", progress/materials.Count)) break;
			ConvertMaterial(material);
		}
		EditorUtility.ClearProgressBar();
		AssetDatabase.SaveAssets();
		(new Editor()).Repaint();
	}

	static List<Material> GatherMaterials() {
		string proj_dir = System.IO.Path.GetDirectoryName(Application.dataPath);
		var paths_set = new HashSet<string>();
		var dirs_list = new List<string>();
		foreach (var obj in Selection.GetFiltered(typeof(UnityEngine.Object), SelectionMode.Assets)) {
			var path = AssetDatabase.GetAssetPath(obj);
			if (System.IO.Directory.Exists(proj_dir+"/"+path)) {
				dirs_list.Add(path);
			} else if ((obj as Material) != null) {
				paths_set.Add(path);
			}
		}
		if (dirs_list.Count > 0) { // FindAssets() with empty dir array searches in the whole project
			foreach (var guid in AssetDatabase.FindAssets("t:Material", dirs_list.ToArray())) {
				paths_set.Add(AssetDatabase.GUIDToAssetPath(guid));
			}
		}

		var materials = new List<Material>();
		foreach (var path in paths_set) {
			//AssetDatabase.ImportAsset(path);
			var material = AssetDatabase.LoadAssetAtPath<Material>(path);
			if (material) materials.Add(material);
		}

		return materials;
	}

	static void ConvertMaterial(Material material) {
		if (!ConvertShader(material)) return;
		SyncAlphaTexture(material);
	}

	// Generally, the intent is determined by the contents of the original texture.
	// Separate alpha-texture is supposed to be fully derivative and exactly the same as the original.
	// * original texture contains transparency
	//   * ALL original texture's formats (including platform overrides) support alpha-channel
	//     * no alpha-texture needed
	//   * NOT ALL original texture's formats (including platform overrides) support alpha-channel
	//     * generate alpha-texture (max-size = 32 for platforms which support alpha-channel)
	//     * assign RGB+A material overrides for the corresponding platforms
	// * original texture is opaque
	//   * no alpha-texture needed

	// Eugene says that for now useless alpha-textures' assets should be deleted;
	// if it causes too much trouble, an option to not delete them can be easily added

	static bool alpha_tex_sync_full = false;

	static void SyncAlphaTexture(Material material) {
		var tex = material.mainTexture;

		int w0 = 0, h0 = 0;
		if (tex) { w0 = tex.width; h0 = tex.height; }

		bool texA_needed = RequiresAlphaTexture(tex);

		// Clearing RGB+A is needed in either case
		foreach (var platform_name in System.Enum.GetNames(typeof(BuildTargetGroup))) {
			BiniUberShader.DeletePlatformOverride(material, platform_name, "?RGB+A");
		}

		if (!texA_needed) {
			var info = (BiniUberShader.Info)material.shader;
			info.UseSeparateAlpha = false;
			material.shader = (Shader)info;
		}

		string path = AssetDatabase.GetAssetPath(tex);
		if (string.IsNullOrEmpty(path)) return; // not an asset

		var textureImporter = AssetImporter.GetAtPath(path) as TextureImporter;
		if (!textureImporter) return; // built-in texture

		string ext = System.IO.Path.GetExtension(path);
		string pathA = path.Substring(0, path.Length - ext.Length) + "_Alpha" + ext;

		if (!texA_needed) {
			AssetDatabase.DeleteAsset(pathA);
		} else {
			string proj_dir = System.IO.Path.GetDirectoryName(Application.dataPath);

			string active_platform_name = BuildTargetGroupToTexturePlatform(active_platform.ToString());
			bool supports_alpha_default = FormatSupportsAlpha(textureImporter.textureFormat);
			bool active_platform_supports_alpha = supports_alpha_default;

			bool texA_existed = System.IO.File.Exists(proj_dir+"/"+pathA);

			System.IO.File.Copy(proj_dir+"/"+path, proj_dir+"/"+pathA, true);
			AssetDatabase.Refresh();

			int max_size = CalcMaxSize(w0, h0);

			var textureImporterA = AssetImporter.GetAtPath(pathA) as TextureImporter;
			textureImporterA.textureType = TextureImporterType.Advanced;
			textureImporterA.textureFormat = TextureImporterFormat.Alpha8;
			textureImporterA.alphaIsTransparency = textureImporter.alphaIsTransparency;
			textureImporterA.grayscaleToAlpha = textureImporter.grayscaleToAlpha;
			textureImporterA.wrapMode = textureImporter.wrapMode;
			textureImporterA.filterMode = textureImporter.filterMode;
			textureImporterA.anisoLevel = textureImporter.anisoLevel;
			textureImporterA.mipmapEnabled = textureImporter.mipmapEnabled;
			textureImporterA.mipmapFilter = textureImporter.mipmapFilter;
			textureImporterA.mipMapBias = textureImporter.mipMapBias;
			textureImporterA.mipmapFadeDistanceStart = textureImporter.mipmapFadeDistanceStart;
			textureImporterA.mipmapFadeDistanceEnd = textureImporter.mipmapFadeDistanceEnd;
			textureImporterA.borderMipmap = textureImporter.borderMipmap;
			textureImporterA.fadeout = textureImporter.fadeout;
			if (alpha_tex_sync_full | !texA_existed) {
				textureImporterA.name = textureImporter.name;
				textureImporterA.isReadable = false;
				textureImporterA.npotScale = textureImporter.npotScale;
			}

			if (!texA_existed) textureImporterA.maxTextureSize = (supports_alpha_default ? 32 : max_size);

			var po = new BiniUberShader.PlatformOverride("?RGB+A", null);
			po.FromBool(supports_alpha_default);
			BiniUberShader.SetPlatformOverride(material, null, po);

			foreach (var platform_name in TexturePlatforms()) {
				int _maxsize;
				TextureImporterFormat _format;
				textureImporter.GetPlatformTextureSettings(platform_name, out _maxsize, out _format);
				bool supports_alpha = FormatSupportsAlpha(_format);
				if (supports_alpha == supports_alpha_default) {
					textureImporterA.ClearPlatformTextureSettings(platform_name);
				} else {
					textureImporterA.GetPlatformTextureSettings(platform_name, out _maxsize, out _format);
					if (!texA_existed) _maxsize = (supports_alpha ? 32 : max_size);
					textureImporterA.SetPlatformTextureSettings(platform_name, _maxsize, textureImporterA.textureFormat);
					po.FromBool(supports_alpha);
					BiniUberShader.SetPlatformOverride(material, TexturePlatformToBuildTargetGroup(platform_name).ToString(), po);
				}
				if (platform_name == active_platform_name) active_platform_supports_alpha = supports_alpha;
			}

			textureImporterA.SaveAndReimport();

			var texA = AssetDatabase.LoadAssetAtPath(pathA, typeof(Texture2D)) as Texture2D;

			var info = (BiniUberShader.Info)material.shader;
			info.UseSeparateAlpha = true;
			material.shader = (Shader)info;
			material.SetTexture("_MainTex_Alpha", texA);
			if (active_platform_supports_alpha) {
				info.UseSeparateAlpha = false;
				material.shader = (Shader)info;
			}
		}
	}

	static int CalcMaxSize(int w0, int h0) {
		int max_size = Mathf.Max(w0, h0);
		int next_size = (Mathf.IsPowerOfTwo(max_size) ? max_size : Mathf.NextPowerOfTwo(max_size));
		float max_shrink;
		if (max_size <= 256) {
			max_shrink = 1.5f;
		} else if (max_size <= 512) {
			max_shrink = 2f;
		} else {
			max_shrink = 2.5f;
		}
		int shrink_size = next_size / 2;
		while (shrink_size * max_shrink >= max_size) {
			shrink_size = shrink_size / 2;
		}
		return Mathf.Max(shrink_size * 2, 32);
	}

	static bool RequiresAlphaTexture(Texture tex) {
		if (!tex) return false;

		string path = AssetDatabase.GetAssetPath(tex);
		if (string.IsNullOrEmpty(path)) return false; // not an asset

		var textureImporter = AssetImporter.GetAtPath(path) as TextureImporter;
		if (!textureImporter) return false; // non-importable (e.g. a built-in texture)

		string active_platform_name = BuildTargetGroupToTexturePlatform(active_platform.ToString());

		var format = textureImporter.textureFormat;
		var maxsize = textureImporter.maxTextureSize;
		var npot = textureImporter.npotScale;
		var readable = textureImporter.isReadable;

		bool supports_alpha = FormatSupportsAlpha(format);

		int _maxsize = maxsize;
		TextureImporterFormat _format = format;
		foreach (var platform_name in TexturePlatforms()) {
			textureImporter.GetPlatformTextureSettings(platform_name, out _maxsize, out _format);
			supports_alpha &= FormatSupportsAlpha(_format);
		}

		if (supports_alpha) return false; // alpha-channel is supported, no need for separate alpha-texture

		return textureImporter.DoesSourceTextureHaveAlpha();
	}

	static KeyValuePair<string, BuildTargetGroup>[] texture_platforms = new KeyValuePair<string, BuildTargetGroup>[]{
		// As seen in the texture inspector (with only the free platforms)
		new KeyValuePair<string, BuildTargetGroup>("Web", BuildTargetGroup.WebPlayer),
		new KeyValuePair<string, BuildTargetGroup>("Standalone", BuildTargetGroup.Standalone),
		new KeyValuePair<string, BuildTargetGroup>("iPhone", BuildTargetGroup.iOS),
		new KeyValuePair<string, BuildTargetGroup>("tvOS", BuildTargetGroup.tvOS),
		new KeyValuePair<string, BuildTargetGroup>("Android", BuildTargetGroup.Android),
		new KeyValuePair<string, BuildTargetGroup>("Tizen", BuildTargetGroup.Tizen),
		new KeyValuePair<string, BuildTargetGroup>("WebGL", BuildTargetGroup.WebGL),
		new KeyValuePair<string, BuildTargetGroup>("Samsung TV", BuildTargetGroup.SamsungTV),
		// These are taken from Build Settings (not tested)
		new KeyValuePair<string, BuildTargetGroup>("Xbox 360", BuildTargetGroup.XBOX360),
		new KeyValuePair<string, BuildTargetGroup>("Xbox One", BuildTargetGroup.XboxOne),
		new KeyValuePair<string, BuildTargetGroup>("PS3", BuildTargetGroup.PS3),
		new KeyValuePair<string, BuildTargetGroup>("PS Vita", BuildTargetGroup.PSP2),
		new KeyValuePair<string, BuildTargetGroup>("PS4", BuildTargetGroup.PS4),
		new KeyValuePair<string, BuildTargetGroup>("Windows Store", BuildTargetGroup.WSA),
		// These are suspected (not tested)
		new KeyValuePair<string, BuildTargetGroup>("BlackBerry", BuildTargetGroup.BlackBerry),
		new KeyValuePair<string, BuildTargetGroup>("Nintendo 3DS", BuildTargetGroup.Nintendo3DS),
		new KeyValuePair<string, BuildTargetGroup>("PS Mobile", BuildTargetGroup.PSM),
		new KeyValuePair<string, BuildTargetGroup>("Wii U", BuildTargetGroup.WiiU),
		new KeyValuePair<string, BuildTargetGroup>("Windows Phone", BuildTargetGroup.WP8),
	};
	static IEnumerable<string> TexturePlatforms() {
		for (int i = 0; i < texture_platforms.Length; i++) {
			yield return texture_platforms[i].Key;
		}
	}
	static BuildTargetGroup TexturePlatformToBuildTargetGroup(string platform) {
		for (int i = 0; i < texture_platforms.Length; i++) {
			var kv = texture_platforms[i];
			if (kv.Key == platform) return kv.Value;
		}
		return BuildTargetGroup.Unknown;
	}
	static string BuildTargetGroupToTexturePlatform(BuildTargetGroup btg) {
		for (int i = 0; i < texture_platforms.Length; i++) {
			var kv = texture_platforms[i];
			if (kv.Value == btg) return kv.Key;
		}
		return "";
	}
	static string BuildTargetGroupToTexturePlatform(string btg) {
		var t = typeof(BuildTargetGroup);
		if (!System.Enum.IsDefined(t, btg)) return "";
		return BuildTargetGroupToTexturePlatform((BuildTargetGroup)System.Enum.Parse(t, btg));
	}

	static BuildTargetGroup TargetToGroup(BuildTarget target) {
		switch (target) {
		case BuildTarget.Android: return BuildTargetGroup.Android;
		case BuildTarget.BlackBerry: return BuildTargetGroup.BlackBerry;
		case BuildTarget.StandaloneGLESEmu: return BuildTargetGroup.GLESEmu;
		case BuildTarget.iOS: return BuildTargetGroup.iOS;
		case BuildTarget.Nintendo3DS: return BuildTargetGroup.Nintendo3DS;
		case BuildTarget.PS3: return BuildTargetGroup.PS3;
		case BuildTarget.PS4: return BuildTargetGroup.PS4;
		case BuildTarget.PSM: return BuildTargetGroup.PSM;
		case BuildTarget.PSP2: return BuildTargetGroup.PSP2;
		case BuildTarget.SamsungTV: return BuildTargetGroup.SamsungTV;
		case BuildTarget.Tizen: return BuildTargetGroup.Tizen;
		case BuildTarget.tvOS: return BuildTargetGroup.tvOS;
		case BuildTarget.WebGL: return BuildTargetGroup.WebGL;
		case BuildTarget.WebPlayer: return BuildTargetGroup.WebPlayer;
		case BuildTarget.WebPlayerStreamed: return BuildTargetGroup.WebPlayer;
		case BuildTarget.WiiU: return BuildTargetGroup.WiiU;
		case BuildTarget.WP8Player: return BuildTargetGroup.WP8;
		case BuildTarget.WSAPlayer: return BuildTargetGroup.WSA;
		case BuildTarget.XBOX360: return BuildTargetGroup.XBOX360;
		case BuildTarget.XboxOne: return BuildTargetGroup.XboxOne;
		}
		return BuildTargetGroup.Standalone;
	}
	static BuildTargetGroup active_platform {
		get { return TargetToGroup(EditorUserBuildSettings.activeBuildTarget); }
	}

	static bool FormatSupportsAlpha(TextureImporterFormat format) {
		switch (format) {
		case TextureImporterFormat.AutomaticTruecolor: return true;
		case TextureImporterFormat.RGBA32: return true;
		case TextureImporterFormat.ARGB32: return true;
		case TextureImporterFormat.RGB24: return false;
		case TextureImporterFormat.Automatic16bit: return true;
		case TextureImporterFormat.RGBA16: return true;
		case TextureImporterFormat.ARGB16: return true;
		case TextureImporterFormat.RGB16: return false;
		case TextureImporterFormat.Alpha8: return true;

		case TextureImporterFormat.AutomaticCompressed: return true;
		case TextureImporterFormat.AutomaticCrunched: return true;

		case TextureImporterFormat.DXT1: return false;
		case TextureImporterFormat.DXT1Crunched: return false;
		case TextureImporterFormat.DXT5: return true;
		case TextureImporterFormat.DXT5Crunched: return true;

		case TextureImporterFormat.ETC_RGB4: return false;
		case TextureImporterFormat.ETC2_RGB4: return false;
		case TextureImporterFormat.ETC2_RGB4_PUNCHTHROUGH_ALPHA: return true;
		case TextureImporterFormat.ETC2_RGBA8: return true;
		case TextureImporterFormat.EAC_R: return false;
		case TextureImporterFormat.EAC_R_SIGNED: return false;
		case TextureImporterFormat.EAC_RG: return false;
		case TextureImporterFormat.EAC_RG_SIGNED: return false;

		case TextureImporterFormat.PVRTC_RGB2: return false;
		case TextureImporterFormat.PVRTC_RGB4: return false;
		case TextureImporterFormat.PVRTC_RGBA2: return true;
		case TextureImporterFormat.PVRTC_RGBA4: return true;

		case TextureImporterFormat.ATC_RGB4: return false;
		case TextureImporterFormat.ATC_RGBA8: return true;

		case TextureImporterFormat.ASTC_RGBA_4x4: return true;
		case TextureImporterFormat.ASTC_RGBA_5x5: return true;
		case TextureImporterFormat.ASTC_RGBA_6x6: return true;
		case TextureImporterFormat.ASTC_RGBA_8x8: return true;
		case TextureImporterFormat.ASTC_RGBA_10x10: return true;
		case TextureImporterFormat.ASTC_RGBA_12x12: return true;
		case TextureImporterFormat.ASTC_RGB_4x4: return false;
		case TextureImporterFormat.ASTC_RGB_5x5: return false;
		case TextureImporterFormat.ASTC_RGB_6x6: return false;
		case TextureImporterFormat.ASTC_RGB_8x8: return false;
		case TextureImporterFormat.ASTC_RGB_10x10: return false;
		case TextureImporterFormat.ASTC_RGB_12x12: return false;
		}
		return false;
	}

	#region MAPPING
	// =================== MAPPING =================== //

	struct TextureInfo {
		public Texture texture;
		public Vector2 offset;
		public Vector2 scale;
	}
	static Dictionary<string, TextureInfo> map_texture = new Dictionary<string, TextureInfo>();
	static Dictionary<string, float> map_float = new Dictionary<string, float>();
	static Dictionary<string, Color> map_color = new Dictionary<string, Color>();
	static void MapTexture(Material material, string src_name, string dst_name) {
		var ti = default(TextureInfo);
		ti.texture = material.GetTexture(src_name);
		ti.offset = material.GetTextureOffset(src_name);
		ti.scale = material.GetTextureScale(src_name);
		map_texture[dst_name] = ti;
	}
	static void ResetMaps() {
		map_texture.Clear();
		map_float.Clear();
		map_color.Clear();
	}
	static void ApplyMaps(Material material) {
		foreach (var kv in map_texture) {
			material.SetTexture(kv.Key, kv.Value.texture);
			material.SetTextureOffset(kv.Key, kv.Value.offset);
			material.SetTextureScale(kv.Key, kv.Value.scale);
		}
		foreach (var kv in map_float) material.SetFloat(kv.Key, kv.Value);
		foreach (var kv in map_color) material.SetColor(kv.Key, kv.Value);
	}

	static bool ConvertShader(Material material) {
		if (!material.shader) return false;
		if ((BiniUberShader.Info)material.shader) return true;

		var info = default(BiniUberShader.Info);

		ResetMaps();

		switch (material.shader.name) {
		case "Unlit/Transparent":
			info.Name = "Blend: Alpha";
			break;
		case "Unlit/AlphaSelfIllumWithFade":
			info.Name = "Blend: Alpha";
			info.UseConstantColor = true;
			break;
		case "Unlit/TintedTextureShader":
			info.Name = "Blend: Alpha";
			info.UseConstantColor = true;
			break;
		case "Custom/AndroidAlpha":
			info.Name = "Blend: Alpha";
			info.UseConstantColor = true;
			info.UseSeparateAlpha = true;
			MapTexture(material, "_AlphaMap", "_MainTex_Alpha");
			break;
		case "Unlit/AndroidTransparent":
			info.Name = "Blend: Alpha";
			info.UseSeparateAlpha = true;
			MapTexture(material, "_AlphaMap", "_MainTex_Alpha");
			break;
		case "Unlit/AndroidTransparentAndrew":
			info.Name = "Blend: Alpha";
			info.UseConstantColor = true;
			info.UseSeparateAlpha = true;
			MapTexture(material, "_AlphaMap", "_MainTex_Alpha");
			break;

		case "BlendMode/Addition":
			info.Name = "Blend: Add";
			info.UseConstantColor = true;
			break;
		case "BlendMode/Alpha":
			info.Name = "Blend: Alpha";
			info.UseConstantColor = true;
			break;
		case "BlendMode/ColorBurn":
			info.Name = "Blend: ColorBurn";
			info.UseConstantColor = true;
			break;
		case "BlendMode/Darken":
			info.Name = "Blend: Darken";
			info.UseConstantColor = true;
			break;
		case "BlendMode/Dissolve":
			info.Name = "Blend: Dissolve";
			info.UseConstantColor = true;
			break;
		case "BlendMode/Dodge":
			info.Name = "Blend: Dodge";
			info.UseConstantColor = true;
			break;
		case "BlendMode/HardLight":
			info.Name = "Blend: HardLight";
			info.UseConstantColor = true;
			break;
		case "BlendMode/Lighten":
			info.Name = "Blend: Lighten";
			info.UseConstantColor = true;
			break;
		case "BlendMode/LinearBurn":
			info.Name = "Blend: LinearBurn";
			info.UseConstantColor = true;
			break;
		case "BlendMode/LinearLight":
			info.Name = "Blend: LinearLight";
			info.UseConstantColor = true;
			break;
		case "BlendMode/Multiply":
			info.Name = "Blend: Multiply";
			info.UseConstantColor = true;
			map_color["_AddFactor"] = new Color(1, 1, 1, material.GetFloat("_Factor"));
			break;
		case "BlendMode/Screen":
			info.Name = "Blend: Screen";
			info.UseConstantColor = true;
			map_color["_SubFactor"] = new Color(0, 0, 0, material.GetFloat("_Factor"));
			break;
		case "BlendMode/Subtract":
			info.Name = "Blend: Subtract";
			info.UseConstantColor = true;
			break;

		case "Unlit/UnlitDodge":
			info.Name = "Blend: Dodge";
			info.UseConstantColor = true;
			material.color = new Color(material.color.r, material.color.g, material.color.b, material.color.a * material.GetFloat("_Opacity"));
			break;
		case "Unlit/UnlitMultiply":
			info.Name = "Blend: Multiply";
			break;
		case "Unlit/UnlitMultiplyTransparent":
			info.Name = "Blend: Multiply";
			info.UseConstantColor = true;
			break;
		case "Unlit/UnlitScreen":
			info.Name = "Blend: Screen";
			info.UseConstantColor = true;
			break;

		case "Custom/ModifyHSV":
			info.Name = "Color: HSV";
			break;
		case "Custom/SiluetteShader":
			info.Name = "Color: Silhouette Alpha";
			info.UseConstantColor = true;
			break;
		case "Unlit/Siluet_Multiply":
			info.Name = "Color: Silhouette Multiply";
			info.UseConstantColor = true;
			break;
		case "Unlit/Siluet_MultiplyTransparent":
			info.Name = "Color: Silhouette Multiply";
			info.UseConstantColor = true;
			break;
		case "Custom/MaskedTextures":
			info.Name = "Compose: Mask";
			info.UseConstantColor = true;
			break;
		case "Custom/OutlineShader":
			info.Name = "Compose: Outline";
			info.UseConstantColor = true;
			if ((material.GetFloat("_AlphaMin") != 0f) | (material.GetFloat("_AlphaMax") != 1f) | (material.GetFloat("_Sharpness") != 1f)) {
				material.shaderKeywords = new string[]{"USE_SHARPNESS"}; material.SetFloat("_UseSharpness", 1);
			} else {
				material.shaderKeywords = null; material.SetFloat("_UseSharpness", 0);
			}
			break;
		case "Custom/OutlineShader2":
			info.Name = "Compose: Outline";
			info.UseConstantColor = true;
			if ((material.GetFloat("_AlphaMin") != 0f) | (material.GetFloat("_AlphaMax") != 1f) | (material.GetFloat("_Sharpness") != 1f)) {
				material.shaderKeywords = new string[]{"USE_SHARPNESS"}; material.SetFloat("_UseSharpness", 1);
			} else {
				material.shaderKeywords = null; material.SetFloat("_UseSharpness", 0);
			}
			break;
		}

		var shader = (Shader)info;
		if (!shader) return false;

		material.shader = shader;
		ApplyMaps(material);

		return true;
	}
	#endregion

//	class MaterialOverrideSynchronizer : AssetPostprocessor {
//		static void OnPostprocessAllAssets(string[] importedAssets, string[] deletedAssets, string[] movedAssets, string[] movedFromAssetPaths) {
//			Debug.Log("MaterialOverrideSynchronizer!");
//			string active_platform_name = active_platform.ToString();
//			bool repaint = false;
//			for (int i = 0; i < importedAssets.Length; i++) {
//				if (System.IO.Path.GetExtension(importedAssets[i]).ToLower() != ".mat") continue;
//				Debug.Log("Trying to import "+importedAssets[i]);
//				var material = AssetDatabase.LoadAssetAtPath<Material>(importedAssets[i]);
//				if (!(material && material.shader)) continue;
//				if (!material.HasProperty("_BiniUber")) continue;
//				//continue;
//				BiniUberShader.SyncWritePlatformOverrides(material, active_platform_name);
//				repaint = true;
//			}
//			if (repaint) { (new Editor()).Repaint(); }
//		}
//	}

	[MenuItem("Mage/Synchronize Material Overrides", priority = 1)]
	public static void SyncMaterialOverrides() {
		int direction = EditorUtility.DisplayDialogComplex(
			"Synchronize Material Overrides", "Choose synchronization direction:",
			"Material->Overrides", "Cancel", "Overrides->Material");
		if (direction == 1) return; // CANCEL

		string active_platform_name = active_platform.ToString();
		string info_text = (direction == 0 ? "Material->Overrides" : "Overrides->Material");

		var materials = AssetDatabase.FindAssets("t:Material");
		int i = 0, n = materials.Length;
		foreach (var guid in materials) {
			i += 1;
			EditorUtility.DisplayProgressBar("Synchronize Material Overrides", info_text+" ("+i+"/"+n+")", i/(float)n);
			var material = AssetDatabase.LoadAssetAtPath<Material>(AssetDatabase.GUIDToAssetPath(guid));
			if (!(material && material.shader)) continue;
			if (!material.HasProperty("_BiniUber")) continue;
			if (direction == 0) {
				BiniUberShader.SyncReadPlatformOverrides(material, active_platform_name); // read from material
			} else {
				BiniUberShader.SyncWritePlatformOverrides(material, active_platform_name); // write to material
			}
		}

		EditorUtility.ClearProgressBar();

		AssetDatabase.SaveAssets();
		AssetDatabase.Refresh();

		(new Editor()).Repaint();
	}
}
