﻿using UnityEngine;
#if UNITY_EDITOR
using UnityEditor;
#endif
using System.Collections.Generic;

public static class BiniUberShader {
	public struct Info {
		public string Name;
		public bool UseSeparateAlpha;
		public bool UseConstantColor;
		public bool UseVertexColor;

		public static implicit operator bool(Info info) {
			return !string.IsNullOrEmpty(info.Name);
		}

		public static explicit operator Info(Shader shader) {
			if (!shader) return default(Info);
			return (Info)shader.name;
		}
		public static explicit operator Info(string shader_name) {
			var info = new Info();

			if (string.IsNullOrEmpty(shader_name)) return info;
			if (!shader_name.StartsWith("BiniUber/")) return info;
			int i = 9; // length of "BiniUber/"

			if (shader_name.IndexOf("RGBA/", i, 5) >= 0) {
				info.UseSeparateAlpha = false; i += 5;
			} else if (shader_name.IndexOf("RGB+A/", i, 6) >= 0) {
				info.UseSeparateAlpha = true; i += 6;
			} else {
				return info;
			}
			
			if (shader_name.IndexOf("T/", i, 2) >= 0) {
				info.UseConstantColor = false; info.UseVertexColor = false; i += 2;
			} else if (shader_name.IndexOf("TC/", i, 3) >= 0) {
				info.UseConstantColor = true; info.UseVertexColor = false; i += 3;
			} else if (shader_name.IndexOf("TV/", i, 3) >= 0) {
				info.UseConstantColor = false; info.UseVertexColor = true; i += 3;
			} else if (shader_name.IndexOf("TVC/", i, 4) >= 0) {
				info.UseConstantColor = true; info.UseVertexColor = true; i += 4;
			} else {
				return info;
			}

			info.Name = shader_name.Substring(i);

			return info;
		}

		public static explicit operator Shader(Info info) {
			return Shader.Find((string)info);
		}
		public static explicit operator string(Info info) {
			string alpha_mode, color_mode;
			alpha_mode = (info.UseSeparateAlpha ? "RGB+A" : "RGBA");
			if (info.UseVertexColor) {
				color_mode = (info.UseConstantColor ? "TVC" : "TV");
			} else {
				color_mode = (info.UseConstantColor ? "TC" : "T");
			}
			return string.Format("BiniUber/{0}/{1}/{2}", alpha_mode, color_mode, info.Name);
		}
	}

	public static void Set(Material material, string name) {
		if (!material) return;
		string shader_name = (material.shader ? material.shader.name : "");
		bool alpha = material.HasProperty("_MainTex_Alpha");
		bool color = material.HasProperty("_Color");
		bool vertex = shader_name.Contains("Particle");
		Set(material, name, alpha, color, vertex);
	}
	public static void Set(Material material, string name, bool alpha, bool color, bool vertex) {
		if (!material) return;
		var info = (Info)material.shader;
		if (info.Name == name) return;
		if (string.IsNullOrEmpty(info.Name)) {
			info.UseSeparateAlpha = alpha;
			info.UseConstantColor = color;
			info.UseVertexColor = vertex;
		}
		info.Name = name;
		material.shader = (Shader)info;
	}

	public static void UseSeparateAlpha(Material material, bool value) {
		if (!material) return;
		var info = (Info)material.shader;
		if (string.IsNullOrEmpty(info.Name)) return;
		if (info.UseSeparateAlpha == value) return;
		info.UseSeparateAlpha = value;
		material.shader = (Shader)info;
	}

	public static void UseConstantColor(Material material, bool value) {
		if (!material) return;
		var info = (Info)material.shader;
		if (string.IsNullOrEmpty(info.Name)) return;
		if (info.UseConstantColor == value) return;
		info.UseConstantColor = value;
		material.shader = (Shader)info;
	}
	
	public static void UseVertexColor(Material material, bool value) {
		if (!material) return;
		var info = (Info)material.shader;
		if (string.IsNullOrEmpty(info.Name)) return;
		if (info.UseVertexColor == value) return;
		info.UseVertexColor = value;
		material.shader = (Shader)info;
	}

	public enum GrayscaleMode {
		Lightness, Luminosity, Average, Perceived, W3C
	}
	public static GrayscaleMode Vector4ToGrayscaleMode(Vector4 v) {
		if (v.w < 0) {
			return GrayscaleMode.Lightness;
		} else if (v.w > 0) {
			return GrayscaleMode.Perceived;
		} else if (v.y < 0.5f) {
			return GrayscaleMode.Average; // 1/3, 1/3, 1/3
		} else if (v.z < 0.1f) {
			return GrayscaleMode.Luminosity; // 0.21, 0.72, 0.07
		} else {
			return GrayscaleMode.W3C; // 0.299, 0.587, 0.144
		}
	}
	public static Vector4 GrayscaleModeToVector4(GrayscaleMode value) {
		switch (value) {
		case GrayscaleMode.Lightness: return new Vector4(1, 1, 1, -1);
		case GrayscaleMode.Luminosity: return new Vector4(0.21f, 0.72f, 0.07f, 0);
		case GrayscaleMode.Average: return new Vector4(1f/3f, 1f/3f, 1f/3f, 0);
		case GrayscaleMode.Perceived: return new Vector4(0.490918f, 0.831264f, 0.260768f, 1);
		case GrayscaleMode.W3C: return new Vector4(0.299f, 0.587f, 0.144f, 0);
		}
		return Vector4.zero;
	}

	#if UNITY_EDITOR
	public class PlatformOverride {
		public string name = null;
		public string data = null;

		public PlatformOverride(string name, string data) {
			this.name = name; this.data = data;
		}

		public bool AsBool(bool fallback=false) {
			if (string.IsNullOrEmpty(data)) return fallback;
			bool res;
			return (bool.TryParse(data, out res) ? res : fallback);
		}
		public int AsInt(int fallback=0) {
			if (string.IsNullOrEmpty(data)) return fallback;
			int res;
			return (int.TryParse(data, out res) ? res : fallback);
		}
		public float AsFloat(float fallback=0f) {
			if (string.IsNullOrEmpty(data)) return fallback;
			float res;
			return (float.TryParse(data, out res) ? res : fallback);
		}
		public Vector4 AsVector(Vector4 fallback=default(Vector4)) {
			if (string.IsNullOrEmpty(data)) return fallback;
			var vs = data.Split(',');
			if (vs.Length != 4) return fallback;
			Vector4 res = fallback;
			if (!float.TryParse(vs[0], out res.x)) return fallback;
			if (!float.TryParse(vs[1], out res.y)) return fallback;
			if (!float.TryParse(vs[2], out res.z)) return fallback;
			if (!float.TryParse(vs[3], out res.w)) return fallback;
			return res;
		}
		public Color AsColor(Color fallback=default(Color)) {
			if (string.IsNullOrEmpty(data)) return fallback;
			var vs = data.Split(',');
			if (vs.Length != 4) return fallback;
			Color res = fallback;
			if (!float.TryParse(vs[0], out res.r)) return fallback;
			if (!float.TryParse(vs[1], out res.g)) return fallback;
			if (!float.TryParse(vs[2], out res.b)) return fallback;
			if (!float.TryParse(vs[3], out res.a)) return fallback;
			return res;
		}
		public T AsAsset<T>(T fallback=null) where T : Object {
			if (string.IsNullOrEmpty(data)) return fallback;
			string path = AssetDatabase.GUIDToAssetPath(data);
			AssetDatabase.ImportAsset(path);
			return AssetDatabase.LoadAssetAtPath(path, typeof(T)) as T;
		}

		public void FromBool(bool value) {
			data = value.ToString();
		}
		public void FromInt(int value) {
			data = value.ToString();
		}
		public void FromFloat(float value) {
			data = value.ToString();
		}
		public void FromVector(Vector4 value) {
			data = value.x+","+value.y+","+value.z+","+value.w;
		}
		public void FromColor(Color value) {
			data = value.r+","+value.g+","+value.b+","+value.a;
		}
		public void FromAsset(Object value) {
			string path = AssetDatabase.GetAssetPath(value);
			data = AssetDatabase.AssetPathToGUID(path);
		}

		static char[] escape_chars = new char[]{'"', ',', ':', ' '};
		static string Escape(string s) {
			if (s.IndexOfAny(escape_chars) < 0) return s; // doesn't need escaping
			return "\"" + s.Replace("\"", "\"\"") + "\"";
		}
		public void Serialize(System.Text.StringBuilder str_builder) {
			if (str_builder.Length != 0) {
				str_builder.Append(',');
				str_builder.Append(' ');
			}
			str_builder.Append(Escape(name));
			str_builder.Append(':');
			str_builder.Append(Escape(data));
		}
		public static PlatformOverride Deserialize(string s, ref int i, System.Text.StringBuilder str_builder=null) {
			if (string.IsNullOrEmpty(s) || (i < 0) || (i >= s.Length)) { i = -1; return null; }
			if (str_builder == null) str_builder = new System.Text.StringBuilder();
			str_builder.Length = 0;
			string name = null, data = null;
			bool inside_quotes = false;
			for (; i <= s.Length; i++) {
				char c = (i == s.Length ? ',' : s[i]);
				if (inside_quotes) {
					if (c == '"') {
						int i2 = i+1;
						char c2 = (i2 == s.Length ? ',' : s[i2]);
						if (c2 == '"') {
							str_builder.Append(c); // escaped quotes
							i++;
						} else {
							inside_quotes = false;
						}
					} else {
						str_builder.Append(c);
					}
				} else {
					if (c == ':') {
						if (string.IsNullOrEmpty(name)) {
							name = str_builder.ToString().Trim();
						} else {
							name += ":" + str_builder.ToString().Trim();
						}
						str_builder.Length = 0;
					} else if (c == ',') {
						if (string.IsNullOrEmpty(name)) {
							name = str_builder.ToString().Trim();
							data = "";
						} else {
							data = str_builder.ToString().Trim();
						}
						i++;
						str_builder.Length = 0;
						break;
					} else if (c == '"') {
						inside_quotes = true;
					} else {
						str_builder.Append(c);
					}
				}
			}
			if (i >= s.Length) i = -1;
			str_builder.Length = 0;
			if (string.IsNullOrEmpty(name)) return null;
			return new PlatformOverride(name, data);
		}

		public bool Write(Material mat) {
			var shader = mat.shader;
			if (!shader) return false;
			if (name.StartsWith("?")) {
				var info = (Info)shader;
				if (!info) return false;
				switch (name) {
				case "?Shader": info.Name = data; break;
				case "?RGB+A": info.UseSeparateAlpha = AsBool(); break;
				case "?Color": info.UseConstantColor = AsBool(); break;
				case "?Vertex": info.UseVertexColor = AsBool(); break;
				}
				var new_shader = (Shader)info;
				if (new_shader) {mat.shader = new_shader; return true; }
			} else {
				int n = ShaderUtil.GetPropertyCount(shader);
				for (int i = 0; i < n; i++) {
					string prop_name = ShaderUtil.GetPropertyName(shader, i);
					var prop_type = ShaderUtil.GetPropertyType(shader, i);
					if (prop_name == name) {
						switch (prop_type) {
						case ShaderUtil.ShaderPropertyType.Color: mat.SetColor(prop_name, AsColor()); return true;
						case ShaderUtil.ShaderPropertyType.Float: mat.SetFloat(prop_name, AsFloat()); return true;
						case ShaderUtil.ShaderPropertyType.Range: mat.SetFloat(prop_name, AsFloat()); return true;
						case ShaderUtil.ShaderPropertyType.TexEnv: mat.SetTexture(prop_name, AsAsset<Texture>()); return true;
						case ShaderUtil.ShaderPropertyType.Vector: mat.SetVector(prop_name, AsVector()); return true;
						}
					} else if ((prop_type == ShaderUtil.ShaderPropertyType.TexEnv) & (prop_name+"_ST" == name)) {
						var v = AsVector();
						mat.SetTextureScale(prop_name, new Vector2(v.x, v.y));
						mat.SetTextureOffset(prop_name, new Vector2(v.z, v.w));
						return true;
					}
				}
			}
			return false;
		}
		public bool Read(Material mat) {
			var shader = mat.shader;
			if (!shader) return false;
			if (name.StartsWith("?")) {
				var info = (Info)shader;
				if (!info) return false;
				switch (name) {
				case "?Shader": data = info.Name; return true;
				case "?RGB+A": FromBool(info.UseSeparateAlpha); return true;
				case "?Color": FromBool(info.UseConstantColor); return true;
				case "?Vertex": FromBool(info.UseVertexColor); return true;
				}
			} else {
				int n = ShaderUtil.GetPropertyCount(shader);
				for (int i = 0; i < n; i++) {
					string prop_name = ShaderUtil.GetPropertyName(shader, i);
					var prop_type = ShaderUtil.GetPropertyType(shader, i);
					if (prop_name == name) {
						switch (prop_type) {
						case ShaderUtil.ShaderPropertyType.Color: FromColor(mat.GetColor(prop_name)); return true;
						case ShaderUtil.ShaderPropertyType.Float: FromFloat(mat.GetFloat(prop_name)); return true;
						case ShaderUtil.ShaderPropertyType.Range: FromFloat(mat.GetFloat(prop_name)); return true;
						case ShaderUtil.ShaderPropertyType.TexEnv: FromAsset(mat.GetTexture(prop_name)); return true;
						case ShaderUtil.ShaderPropertyType.Vector: FromVector(mat.GetVector(prop_name)); return true;
						}
					} else if ((prop_type == ShaderUtil.ShaderPropertyType.TexEnv) & (prop_name+"_ST" == name)) {
						var scale = mat.GetTextureScale(prop_name);
						var offset = mat.GetTextureOffset(prop_name);
						FromVector(new Vector4(scale.x, scale.y, offset.x, offset.y)); return true;
					}
				}
			}
			return false;
		}
	}

	public static IEnumerable<PlatformOverride> IterPlatformOverrides(Material material, string platform) {
		if (platform == null) platform = ""; // "" means "default/common values"
		string platform_tag = material.GetTag("Platform:"+platform, false, "");
		var str_builder = new System.Text.StringBuilder();
		int i = 0;
		while (i >= 0) {
			var po = PlatformOverride.Deserialize(platform_tag, ref i, str_builder);
			if (po != null) yield return po;
		}
	}
	public static Dictionary<string, PlatformOverride> GetPlatformOverrides(Material material, string platform) {
		var po_map = new Dictionary<string, PlatformOverride>();
		foreach (var po in IterPlatformOverrides(material, platform)) {
			po_map[po.name] = po;
		}
		return po_map;
	}
	public static void SetPlatformOverrides(Material material, string platform, Dictionary<string, PlatformOverride> po_map) {
		if (platform == null) platform = ""; // "" means "default/common values"
		var str_builder = new System.Text.StringBuilder();
		foreach (var po in po_map.Values) {
			if (po != null) po.Serialize(str_builder);
		}
		material.SetOverrideTag("Platform:"+platform, str_builder.ToString());
	}
	public static void WritePlatformOverrides(Material material, string platform) {
		foreach (var po in IterPlatformOverrides(material, platform)) {
			po.Write(material);
		}
	}
	public static void ReadPlatformOverrides(Material material, string platform) {
		if (platform == null) platform = ""; // "" means "default/common values"
		var str_builder = new System.Text.StringBuilder();
		foreach (var po in IterPlatformOverrides(material, platform)) {
			po.Read(material);
			po.Serialize(str_builder);
		}
		material.SetOverrideTag("Platform:"+platform, str_builder.ToString());
	}

	public static void SyncReadPlatformOverrides(Material material, string active_platform) {
		var non_overriden = new HashSet<string>();
		foreach (var kv in IterOverridableProps(material.shader)) {
			non_overriden.Add(kv.Key);
		}

		var str_builder = new System.Text.StringBuilder();
		foreach (var po in IterPlatformOverrides(material, active_platform)) {
			non_overriden.Remove(po.name);
			po.Read(material);
			po.Serialize(str_builder);
		}
		material.SetOverrideTag("Platform:"+active_platform, str_builder.ToString());

		var po_map_default = GetPlatformOverrides(material, null);
		foreach (var prop_name in non_overriden) {
			var po = new PlatformOverride(prop_name, null);
			po.Read(material);
			po_map_default[prop_name] = po;
		}
		SetPlatformOverrides(material, null, po_map_default);
	}
	public static void SyncWritePlatformOverrides(Material material, string active_platform) {
		var po_map_default = GetPlatformOverrides(material, null);
		var po_map_active = GetPlatformOverrides(material, active_platform);
		foreach (var kv in IterOverridableProps(material.shader)) {
			PlatformOverride po = null;
			if (po_map_active.TryGetValue(kv.Key, out po) || po_map_default.TryGetValue(kv.Key, out po)) {
				po.Write(material);
			}
		}
	}

	public static void SwitchPlatform(Material material, string active_platform, string platform) {
		var po_map_active = GetPlatformOverrides(material, active_platform);
		var po_map_default = GetPlatformOverrides(material, null);
		var po_map = GetPlatformOverrides(material, platform);

		// Update & revert active overrides
		foreach (var po_active in po_map_active.Values) {
			po_active.Read(material); // update active overrides
			if (!po_map_default.ContainsKey(po_active.name)) continue;
			var po_default = po_map_default[po_active.name];
			po_default.Write(material); // restore default values
		}
		SetPlatformOverrides(material, active_platform, po_map_active);

		// All overrides were reverted. At this point, all values are considered default.
		foreach (var kv in IterOverridableProps(material.shader)) {
			if (!po_map_default.ContainsKey(kv.Key)) continue;
			var po_default = po_map_default[kv.Key];
			po_default.Read(material); // update default values
		}
		SetPlatformOverrides(material, null, po_map_default);

		// Apply new overrides
		foreach (var po in po_map.Values) {
			po.Write(material);
        }  

		// This does not seem to fix the problem with materials
		// not updating their overrides after platform switch
		//string path = AssetDatabase.GetAssetPath(material);
		//AssetDatabase.SaveAssets();
		//AssetDatabase.ImportAsset(path);
    }
    public static void SwitchPlatform(string active_platform, string platform) {
		string[] guids = AssetDatabase.FindAssets("t:Material");
		foreach (string guid in guids) {
			string path = AssetDatabase.GUIDToAssetPath(guid);
			//AssetDatabase.ImportAsset(path); // just in case (?)
			var material = AssetDatabase.LoadAssetAtPath<Material>(path);
			if (material) SwitchPlatform(material, active_platform, platform);
		}

        // EUGENE'S MOD
        AssetDatabase.SaveAssets();
        AssetDatabase.Refresh(ImportAssetOptions.Default);
    }

    public static bool HasPlatformOverride(Material material, string platform, string prop_name) {
		return (GetPlatformOverride(material, platform, prop_name) != null);
	}
	public static PlatformOverride GetPlatformOverride(Material material, string platform, string prop_name) {
		foreach (var po in IterPlatformOverrides(material, platform)) {
			if (po.name == prop_name) return po;
		}
		return null;
	}
	public static void SetPlatformOverride(Material material, string platform, PlatformOverride po, bool only_if_new=false) {
		if (po == null) return;
		var po_map = GetPlatformOverrides(material, platform);
		if (only_if_new && po_map.ContainsKey(po.name)) return; // already has override
		po_map[po.name] = po;
		SetPlatformOverrides(material, platform, po_map);
	}
	public static void DeletePlatformOverride(Material material, string platform, string prop_name) {
		var po_map = GetPlatformOverrides(material, platform);
		if (po_map.ContainsKey(prop_name)) po_map.Remove(prop_name);
		SetPlatformOverrides(material, platform, po_map);
	}
	public static void WritePlatformOverride(Material material, string platform, string prop_name) {
		var po = GetPlatformOverride(material, platform, prop_name);
		if (po != null) po.Write(material);
	}
	public static PlatformOverride ReadPlatformOverride(Material material, string prop_name) {
		var po = new PlatformOverride(prop_name, null);
		return (po.Read(material) ? po : null);
	}
	public static void EnablePlatformOverride(Material material, string platform, string prop_name, bool enable, bool modify_default=false, string active_platform=null) {
		if (string.IsNullOrEmpty(active_platform)) active_platform = platform;
		if (enable) {
			if (platform == active_platform) {
				var po = ReadPlatformOverride(material, prop_name);
				SetPlatformOverride(material, platform, po);
				SetPlatformOverride(material, null, po);
			} else {
				var po = GetPlatformOverride(material, null, prop_name);
				if ((po == null) || !HasPlatformOverride(material, active_platform, prop_name)) {
					po = ReadPlatformOverride(material, prop_name);
				}
				SetPlatformOverride(material, platform, po, true);
			}
		} else {
			if (modify_default) {
				if (platform == active_platform) {
					var po = ReadPlatformOverride(material, prop_name);
					DeletePlatformOverride(material, platform, prop_name);
					SetPlatformOverride(material, null, po);
				} else {
					var po = GetPlatformOverride(material, platform, prop_name);
					DeletePlatformOverride(material, platform, prop_name);
					SetPlatformOverride(material, null, po);
					if ((po != null) && !HasPlatformOverride(material, active_platform, prop_name)) po.Write(material);
				}
			} else {
				if (platform == active_platform) {
					DeletePlatformOverride(material, platform, prop_name);
					WritePlatformOverride(material, null, prop_name);
				} else {
					DeletePlatformOverride(material, platform, prop_name);
				}
			}
		}
	}

	public static string[] uber_prop_names = new string[]{"?Shader", "?RGB+A", "?Color", "?Vertex"};
	public static string texture_ST_suffix_name = " scale, offset";

	public static IEnumerable<KeyValuePair<string, string>> IterOverridableProps(Shader shader, bool AlphaST=false) {
		if (!shader) yield break;

		if ((Info)shader) {
			foreach (string prop_name in uber_prop_names) {
				string prop_label = prop_name.Substring(1);
				yield return new KeyValuePair<string, string>(prop_name, prop_label);
			}
		}

		int n = ShaderUtil.GetPropertyCount(shader);
		for (int i = 0; i < n; i++) {
			if (ShaderUtil.IsShaderPropertyHidden(shader, i)) continue;

			string prop_name = ShaderUtil.GetPropertyName(shader, i);
			if (prop_name == "_BiniUber") continue;

			var prop_type = ShaderUtil.GetPropertyType(shader, i);
			if (prop_type == ShaderUtil.ShaderPropertyType.TexEnv) {
				string prop_label = ShaderUtil.GetPropertyDescription(shader, i);
				yield return new KeyValuePair<string, string>(prop_name, prop_label);
				if (AlphaST || !prop_name.EndsWith("_Alpha")) {
					yield return new KeyValuePair<string, string>(prop_name+"_ST", prop_label+texture_ST_suffix_name);
				}
			} else {
				string prop_label = ShaderUtil.GetPropertyDescription(shader, i);
				yield return new KeyValuePair<string, string>(prop_name, prop_label);
			}
		}
	}
	#endif
}
