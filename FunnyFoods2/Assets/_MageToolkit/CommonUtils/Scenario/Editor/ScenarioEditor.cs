using UnityEditor;
using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using Bini.Utils;

// TODO:
// * drag&drop entries instead of using up/down arrows?
//   (it also might be useful to change hierarchy of entries)
// * modifier + "+" button -> switch between duplicate, insert empty, insert child?

[CustomEditor(typeof(Scenario))]
public class ScenarioEditor : Editor {
	Scenario scenario;

	void OnEnable() {
		scenario = target as Scenario;
	}
	
	public override void OnInspectorGUI() {
		Undo.RecordObject(scenario, "Edit scenario");

		EditorGUILayout.BeginHorizontal();
		if (MiniButton("Save")) SaveToText();
		if (MiniButton("Load")) LoadFromText();
		scenario.multiline = MiniToggleButton(scenario.multiline, "Multiline: off", "Multiline: on");
		scenario.advanced_view = MiniToggleButton(scenario.advanced_view,
			"Advanced mode: off", "Advanced mode: on");
		if (scenario.advanced_view) {
			scenario.compact_view = MiniToggleButton(scenario.compact_view,
				"Compact view: off", "Compact view: on");
		}
		EditorGUILayout.EndHorizontal();
		DrawSubentries(scenario.root_entry, 1);

		if (GUI.changed) EditorUtility.SetDirty(scenario);
	}

	void SaveToText() {
		string scene_path = EditorApplication.currentScene;
		string scene_dir = System.IO.Path.GetDirectoryName(scene_path);
		string scene_file = System.IO.Path.GetFileNameWithoutExtension(scene_path);
		string scenario_path = EditorUtility.SaveFilePanel(
			"Save scenario to a spreadsheet file",
			scene_dir,
			scene_file + ".scenario.xls",
			"xls");
		if (string.IsNullOrEmpty(scenario_path)) return;
		string txt = scenario.root_entry.ToText(true);
		System.IO.File.WriteAllText(scenario_path, txt);
	}

	void LoadFromText() {
		//string txt = scenario.root_entry.ToText(true);
		string txt = SerializeToTextWithFields(scenario.root_entry);
		EditorGUIUtility.systemCopyBuffer = txt;
//		string scene_path = EditorApplication.currentScene;
//		string scene_dir = System.IO.Path.GetDirectoryName(scene_path);
//		//string scene_file = System.IO.Path.GetFileNameWithoutExtension(scene_path);
//		string scenario_path = EditorUtility.OpenFilePanel(
//			"Load scenario from text file",
//			scene_dir,
//			"txt");
//		if (string.IsNullOrEmpty(scenario_path)) return;
//		Debug.LogError("Load scenario from text file -- not implemented!");
	}
	string SerializeToTextWithFields(ScenarioEntry entry) {
		var str_builder = new System.Text.StringBuilder();
		for (int i = 0; i < entry.subentries.Count; i++) {
			SerializeToTextWithFields(entry.subentries[i], "", str_builder);
		}
		return str_builder.ToString();
	}
	void SerializeToTextWithFields(ScenarioEntry entry, string indentation, System.Text.StringBuilder str_builder) {
		str_builder.Append(indentation);
		str_builder.Append(entry.val_data);
		if (entry.obj_data != null) {
			string obj_serialized = TextWithFieldsDrawer.SerializeObject(entry.obj_data);
			str_builder.Append(" ");
			str_builder.Append(TextWithFields.MakeMeta(TextWithFields.TAG_OBJECT+obj_serialized));
		}
		str_builder.AppendLine();
		indentation += "\t";
		for (int i = 0; i < entry.subentries.Count; i++) {
			SerializeToTextWithFields(entry.subentries[i], indentation, str_builder);
		}
	}

	enum EntryOperation {
		None, MoveUp, MoveDown, Duplicate, Remove
	}

	EntryOperation DrawEntry(ScenarioEntry entry, ScenarioEntry container, int entry_id, int level) {
		EntryOperation entryop = EntryOperation.None;

		if (!styles_initialized) InitStyles();

		const string group_prefix0 = "nested";
		const string group_prefix = group_prefix0+" ";
		bool draw_as_group = false;
		bool draw_foldout = true;
		
		int subentries_count = 0;
		if ((entry != null) && (entry.subentries != null)) {
			subentries_count = entry.subentries.Count;
		}

		if (!scenario.advanced_view) {
			if (level == 1) {
				//if ((entry != null) && !((entry.val_data == group_prefix0) ||
				//	entry.val_data.StartsWith(group_prefix))) return entryop;
				//draw_as_group = true;
				if (entry == null) {
					draw_as_group = true;
				} else {
					draw_as_group |= (entry.val_data == group_prefix0);
					draw_as_group |= entry.val_data.StartsWith(group_prefix);
				}
			} else {
				//if (entry == null) return entryop;
				if (entry != null) {
					draw_foldout = ScenarioStages.IsStageFoldout(entry.val_data);
					draw_foldout |= (subentries_count != 0);
				} else {
					if (subentries_count != 0) return entryop;
					draw_foldout = false;
				}
			}
		}

		if (entry != null) {
			var horiz_rect = EditorGUILayout.BeginHorizontal();

			draw_foldout = (subentries_count != 0) || entry.show_subentries;

			if (draw_foldout) {
				entry.show_subentries = GUILayout.Toggle(
					entry.show_subentries, GUIContent.none, style_foldout[0], GUILayout.ExpandWidth(false));
			} else {
				entry.show_subentries = GUILayout.Toggle(
					entry.show_subentries, GUIContent.none, style_foldout[1], GUILayout.ExpandWidth(false));
				//GUILayout.Toggle(false, GUIContent.none, style_foldout[1], GUILayout.ExpandWidth(false));
				//EditorGUI.indentLevel++;
//				EditorGUILayout.Space();
//				EditorGUILayout.Space();
			}

			if (!draw_as_group) {
				entry.val_data = TextField(entry.val_data);
				entry.obj_data = ObjectField(entry.obj_data);
			} else {
				entry.val_data = group_prefix + TextField(
					entry.val_data.Substring(group_prefix.Length));
			}

			if (MiniButton("\u25B2", -1)) entryop = EntryOperation.MoveUp;
			if (MiniButton("\u25BC", 1)) entryop = EntryOperation.MoveDown;
			if (MiniButton("+", -1)) entryop = EntryOperation.Duplicate;
			if (MiniButton("x", 1)) entryop = EntryOperation.Remove;
			
			EditorGUILayout.EndHorizontal();
			
			if (entry.show_subentries && draw_foldout) {
				EditorGUILayout.BeginHorizontal();
				//EditorGUILayout.Space();
				//EditorGUILayout.Space();
				GUILayout.Toggle(false, GUIContent.none, style_foldout[2], GUILayout.ExpandWidth(false));
				EditorGUILayout.BeginVertical();
				//EditorGUI.indentLevel++;
				DrawSubentries(entry, level+1);
				//EditorGUI.indentLevel--;
				EditorGUILayout.EndVertical();
				EditorGUILayout.EndHorizontal();
			}
		} else {
			var horiz_rect = EditorGUILayout.BeginHorizontal();
			
			if (draw_foldout) {
				GUILayout.Toggle(false, GUIContent.none, style_foldout[2], GUILayout.ExpandWidth(false));
			} else {
				GUILayout.Toggle(false, GUIContent.none, style_foldout[2], GUILayout.ExpandWidth(false));
				//EditorGUI.indentLevel++;
//				EditorGUILayout.Space();
//				EditorGUILayout.Space();
			}

			if (!draw_as_group) {
				var val_data = TextField("");
				var obj_data = ObjectField(null);
				if (!string.IsNullOrEmpty(val_data)) {
					container.Add(val_data);
				} else if (obj_data != null) {
					container.Add(obj_data);
				}
			} else {
				var val_data = TextField("");
				if (!string.IsNullOrEmpty(val_data)) {
					container.Add(group_prefix + val_data);
				}
			}

			MiniButton("\u25B2", -1, false);
			MiniButton("\u25BC", 1, false);
			MiniButton("+", -1, false);
			MiniButton("x", 1, false);
			
			EditorGUILayout.EndHorizontal();
		}

		return entryop;
	}
	
	void DrawSubentries(ScenarioEntry entry, int level) {
		if (entry == null) return;
		if (entry.subentries == null) return;

		int modified_id = -1;
		EntryOperation modification_op = EntryOperation.None;
		for (int i = 0; i < entry.subentries.Count; i++) {
			var entryop = DrawEntry(entry.subentries[i], entry, i, level);
			if (entryop != EntryOperation.None) {
				modified_id = i;
				modification_op = entryop;
			}
		}
		if (modification_op != EntryOperation.None) {
			GUI.FocusControl("");
		}
		if (modification_op == EntryOperation.MoveUp) {
			entry.MoveUp(modified_id);
		} else if (modification_op == EntryOperation.MoveDown) {
			entry.MoveDown(modified_id);
		} else if (modification_op == EntryOperation.Duplicate) {
			entry.Duplicate(modified_id);
		} else if (modification_op == EntryOperation.Remove) {
			entry.Remove(modified_id);
		}

		if (!scenario.compact_view || !scenario.advanced_view || (entry.subentries.Count == 0)) {
			DrawEntry(null, entry, entry.subentries.Count, level);
		}
	}
	
	Object ObjectField(Object obj) {
		// A weird error can be caused by referencing object that later was deleted
		// See http://forum.unity3d.com/threads/108578-Odd-Unity-Editor-Error
		// So, make sure that if obj is comparable to null, we replace it with actual null
		if (obj == null) obj = null;
		if (obj == null) {
			var prev_color = EditorStyles.objectField.normal.textColor;
			
			Color no_obj_color = prev_color;
			no_obj_color.a = 0.25f;
			EditorStyles.objectField.normal.textColor = no_obj_color;
			
			obj = EditorGUILayout.ObjectField(obj, typeof(Object), true);
			
			EditorStyles.objectField.normal.textColor = prev_color;
		} else {
			obj = EditorGUILayout.ObjectField(obj, typeof(Object), true);
		}
		if (obj == null) obj = null;
		return obj;
	}
	string TextField(string text) {
		if (scenario.multiline) {
			return EditorGUILayout.TextArea(text, style_textarea);
		}
		return EditorGUILayout.TextField(text);
	}

	GUIStyle[] style_miniButtonLeft;
	GUIStyle[] style_miniButtonRight;
	GUIStyle[] style_miniButton;

	GUIStyle[] style_foldout;

	GUIStyle style_textarea;

	GUIStyle[] DisabledButtonStyles(GUIStyle style) {
		return DisabledButtonStyles(style, Color.gray);
	}
	GUIStyle[] DisabledButtonStyles(GUIStyle style, Color disabled_color) {
		var result = new GUIStyle[2];
		result[0] = new GUIStyle(style);
		result[1] = new GUIStyle(style);
		result[1].normal.textColor = disabled_color;
		return result;
	}

	bool styles_initialized = false;
	void InitStyles() {
		if (styles_initialized) return;

		style_miniButtonLeft = DisabledButtonStyles(EditorStyles.miniButtonLeft);
		style_miniButtonRight = DisabledButtonStyles(EditorStyles.miniButtonRight);
		style_miniButton = DisabledButtonStyles(EditorStyles.miniButton);

		style_foldout = new GUIStyle[3];
		style_foldout[0] = new GUIStyle(EditorStyles.foldout);
		style_foldout[1] = new GUIStyle(style_foldout[0]);
		style_foldout[1].normal.background = null;
		style_foldout[2] = new GUIStyle(style_foldout[1]);
		style_foldout[2].active = style_foldout[2].normal;

		style_textarea = new GUIStyle(EditorStyles.textField);
		style_textarea.wordWrap = true;

		styles_initialized = true;
	}

	bool MiniButton(string label, int side=0, bool enabled=true) {
		if (!styles_initialized) InitStyles();
		GUIStyle style;
		if (side < 0) {
			style = style_miniButtonLeft[enabled?0:1];
		} else if (side > 0) {
			style = style_miniButtonRight[enabled?0:1];
		} else {
			style = style_miniButton[enabled?0:1];
		}
		return GUILayout.Button(label, style, GUILayout.ExpandWidth(false));
	}

	bool MiniToggleButton(bool state, string label0, string label1, int side=0, bool enabled=true) {
		if (state) {
			if (MiniButton(label1, side, enabled)) state = !state;
		} else {
			if (MiniButton(label0, side, enabled)) state = !state;
		}
		return state;
	}
}

/*
Big ones:
U+25B2 (Black up-pointing triangle ▲)
U+25BC (Black down-pointing triangle ▼)
U+25C0 (Black left-pointing triangle ◀)
U+25B6 (Black right-pointing triangle ▶)

Big white ones:
U+25B3 (White up-pointing triangle △)
U+25BD (White down-pointing triangle ▽)
U+25C1 (White left-pointing triangle ◁)
U+25B7 (White right-pointing triangle ▷)

There is also some smalller triangles:
U+25B4 (Black up-pointing small triangle ▴)
U+25C2 (Black left-pointing small triangle ◂)
U+25BE (Black down-pointing small triangle ▾)
U+25B8 (Black right-pointing small triangle ▸)

Also some white ones:
U+25C3 (White left-pointing small triangle ◃)
U+25BF (White down-pointing small triangle ▿)
U+25B9 (White right-pointing small triangle ▹)
U+25B5 (White up-pointing small triangle ▵)
*/
