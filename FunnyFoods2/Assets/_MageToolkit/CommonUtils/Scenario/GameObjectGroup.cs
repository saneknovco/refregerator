﻿using UnityEngine;
using System.Collections.Generic;
using System.Text.RegularExpressions;
using Bini.Utils;

// Pretty much a GameObject's attribute
public class GameObjectGroup : MonoBehaviour {
	public string FilterName = "";
	public string FilterTag = "";
	public LayerMask FilterLayers = -1;
	public bool FilterChildren = false;
	public bool FilterInverse = false;
	public bool FilterRegex = false;
	public bool FilterPath = false;

	public bool CanPass(GameObject gObj) {
		if ((FilterLayers & (1 << gObj.layer)) == 0) {
			return FilterInverse;
		}
		if (!string.IsNullOrEmpty(FilterTag)) {
			if (gObj.tag != FilterTag) return FilterInverse;
		}
		if (!string.IsNullOrEmpty(FilterName)) {
			string _name = (FilterPath ? gObj.transform.HierarchyPath() : gObj.name);
			if (FilterRegex) {
				bool is_match = Regex.IsMatch(_name, FilterName);
				if (!is_match) return FilterInverse;
			} else {
				if (FilterPath) {
					if (!_name.Contains(FilterName)) return FilterInverse;
				} else {
					if (_name != FilterName) return FilterInverse;
				}
			}
		}

		return !FilterInverse;
	}

	public void WalkFiltered(System.Action<Transform> action, Transform tfm=null) {
		if (tfm == null) tfm = transform;
		int childCount = tfm.childCount;
		for (int i = 0; i < childCount; i++) {
			var child = tfm.GetChild(i);
			if (CanPass(child.gameObject)) action(child);
			if (FilterChildren) WalkFiltered(action, child);
		}
	}

	public IEnumerable<Transform> FilteredChildren(Transform tfm=null) {
		if (tfm == null) tfm = transform;
		int childCount = tfm.childCount;
		for (int i = 0; i < childCount; i++) {
			var child = tfm.GetChild(i);
			if (CanPass(child.gameObject)) yield return child;
			if (!FilterChildren) continue;
			foreach (var subchild in FilteredChildren(child)) yield return subchild;
		}
	}
}
