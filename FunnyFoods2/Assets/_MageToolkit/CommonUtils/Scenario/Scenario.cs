using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using Bini.Utils;

#if SCENARIO_OLD
[System.Serializable]
#endif
public class ScenarioEntry {
	public string val_data = "";
	public Object obj_data = null;
	
	public bool show_subentries = false;

#if SCENARIO_OLD
	[SerializeField]
#endif
	public List<ScenarioEntry> subentries = null;
	
	public int subentries_count {
		get { return (subentries == null) ? 0 : subentries.Count; }
	}
	
	public ScenarioEntry(string val_data="") {
		this.val_data = val_data;
	}
	public ScenarioEntry(Object obj_data) {
		this.obj_data = obj_data;
	}
	public ScenarioEntry(ScenarioEntry template) {
		val_data = template.val_data;
		obj_data = template.obj_data;
		
		if (template.subentries != null) {
			subentries = new List<ScenarioEntry>();
			for (int i = 0; i < template.subentries.Count; i++) {
				subentries.Add(new ScenarioEntry(template.subentries[i]));
			}
		}
	}
	
	public void Add(string val_data) {
		if (subentries == null) subentries = new List<ScenarioEntry>();
		subentries.Add(new ScenarioEntry(val_data));
	}
	public void Add(Object obj_data) {
		if (subentries == null) subentries = new List<ScenarioEntry>();
		subentries.Add(new ScenarioEntry(obj_data));
	}
	
	public void Duplicate(int i) {
		if (subentries == null) subentries = new List<ScenarioEntry>();
		var entry = subentries[i];
		subentries.Insert(i+1, new ScenarioEntry(entry));
	}
	
	public void Remove(int i) {
		if (subentries == null) return;
		subentries.RemoveAt(i);
	}
	
	public void MoveUp(int i) {
		if (subentries == null) return;
		if (i < 1) return;
		var entry = subentries[i];
		subentries.RemoveAt(i);
		subentries.Insert(i-1, entry);
	}
	
	public void MoveDown(int i) {
		if (subentries == null) return;
		if (i > subentries.Count-2) return;
		var entry = subentries[i];
		subentries.RemoveAt(i);
		subentries.Insert(i+1, entry);
	}

	public string ToText(bool ignore_level_0=false) {
		var lines = new List<string>();
		ToText(lines, "", ignore_level_0);
		string newline = System.Environment.NewLine;
		//string newline = "\n";
		return string.Join(newline, lines.ToArray());
	}
	void ToText(List<string> lines, string indentation, bool ignore_level_0) {
		if (!ignore_level_0) {
			//string line = val_data + "\t" + ObjDataToText();
			//string line = ObjDataToText() + "\t" + _data;
			//lines.Add(indentation + line);
			lines.Add(ObjDataToText() + "\t" + indentation + TableProcssorEscape(val_data));
			indentation += "\t";
			//indentation += "  ";
		}
		for (int i = 0; i < subentries.Count; i++) {
			subentries[i].ToText(lines, indentation, false);
		}
	}
	string ObjDataToText() {
		if (obj_data == null) return "\t\t";
		string type_name = obj_data.GetType().Name;
		string hashcode = "";
		string obj_name = obj_data.name;
		string obj_path = "";
		// obj_data is either an object in scene or an asset
		// object prefabs can have asset path, but if they
		// are in the scene, we treat them as normal objects
		var gObj = obj_data as GameObject;
		if (gObj != null) {
			obj_path = FileUtils.StripFilename(gObj.transform.HierarchyPath());
			// Hashcode is necessary only for scene objects,
			// since assets are uniquely defined by their paths
			hashcode = "#" + obj_data.GetHashCode().ToString();
		}
		if (string.IsNullOrEmpty(obj_path)) {
			obj_path = FileUtils.GetAssetPath(obj_data);
			if (obj_path == null) obj_path = "";
		}
		string esc_type_hash = TableProcssorEscape(type_name+hashcode);
		string esc_path = TableProcssorEscape(obj_path);
		string esc_name = TableProcssorEscape(obj_name);
		return esc_type_hash+"\t"+esc_path+"\t"+esc_name;
	}
	string TableProcssorEscape(string text) {
		if (text.Contains("\"") || text.Contains("\r") || text.Contains("\n")) {
			text = "\"" + text.Replace("\"", "\"\"") + "\"";
		}
		return text;
	}
}

public class Scenario : MonoBehaviour {
#if SCENARIO_OLD
	[SerializeField]
#else
	[System.NonSerialized]
#endif
	public ScenarioEntry root_entry;
	
	public bool compact_view = false;
	public bool advanced_view = false;
	public bool multiline = false;
}
