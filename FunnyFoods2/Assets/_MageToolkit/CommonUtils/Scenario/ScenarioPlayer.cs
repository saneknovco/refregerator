using UnityEngine;
using System.Collections.Generic;
using System.Text.RegularExpressions;
using Bini.Utils;

//[RequireComponent(typeof(Scenario))]
public class ScenarioPlayer : MonoBehaviour {
	public static Dictionary<string, string> global_vars = new Dictionary<string, string>();
	public static Dictionary<string, Object> global_obj_vars = new Dictionary<string, Object>();
	
	public static string GetVar(string name, string dflt="", bool add_brackets=false) {
		if (add_brackets) name = "["+name+"]";
		string ws;
		if (!global_vars.TryGetValue(name, out ws)) return dflt;
		return ws;
	}
	public static int GetIntVar(string name, int dflt=0, bool add_brackets=false) {
		if (add_brackets) name = "["+name+"]";
		string ws;
		if (!global_vars.TryGetValue(name, out ws)) return dflt;
		int v;
		if (!int.TryParse(ws, out v)) return dflt;
		return v;
	}
	public static float GetFloatVar(string name, float dflt=0f, bool add_brackets=false) {
		if (add_brackets) name = "["+name+"]";
		string ws;
		if (!global_vars.TryGetValue(name, out ws)) return dflt;
		float v;
		if (!float.TryParse(ws, out v)) return dflt;
		return v;
	}
	public static bool GetBoolVar(string name, bool dflt=false, bool add_brackets=false) {
		if (add_brackets) name = "["+name+"]";
		string ws;
		if (!global_vars.TryGetValue(name, out ws)) return dflt;
		int v;
		if (!int.TryParse(ws, out v)) return dflt;
		return (v != 0);
	}
	public static Object GetObjVar(string name, Object dflt=null, bool add_brackets=false) {
		if (add_brackets) name = "{"+name+"}";
		Object obj;
		if (!global_obj_vars.TryGetValue(name, out obj)) return dflt;
		return obj;
	}

	public static void SetVar(string name, string v, bool add_brackets=false) {
		if (add_brackets) name = "["+name+"]";
		global_vars[name] = v;
	}
	public static void SetIntVar(string name, int v, bool add_brackets=false) {
		if (add_brackets) name = "["+name+"]";
		global_vars[name] = v.ToString();
	}
	public static void SetFloatVar(string name, float v, bool add_brackets=false) {
		if (add_brackets) name = "["+name+"]";
		global_vars[name] = v.ToString();
	}
	public static void SetBoolVar(string name, bool v, bool add_brackets=false) {
		if (add_brackets) name = "["+name+"]";
		global_vars[name] = (v ? 1 : 0).ToString();
	}
	public static void SetObjVar(string name, Object v, bool add_brackets=false) {
		if (add_brackets) name = "{"+name+"}";
		global_obj_vars[name] = v;
	}

	public static void UpdateTimeVars() {
		//SetFloatVar("[Time.time]", Time.time);
		//SetFloatVar("[Time.realtimeSinceStartup]", Time.realtimeSinceStartup);
		SetFloatVar("[Time.time]", SyncTime); // a hack -- for now
	}

	public ScenarioWindow wndScenario;
	public static HashSet<string> all_thread_tags = new HashSet<string>();
	
	public bool PrintTrace = false;
	public bool InitOnAwake = false;
	public bool UseText = false;
	public bool UseBracketComments = false;

	public TextWithFields source_code;

	bool initialized = false;

	Scenario scenario;
	ScenarioEntry root_entry;
	
	internal List<ScenarioThread> threads = new List<ScenarioThread>();
	
	void Init() {
		if (initialized) return;
		initialized = true;

		//if (scenario != null) return;

		UpdateTimeVars();

		foreach (var stage_info in ScenarioStages.StageInfos) {
			if (stage_info.Value.OnScenarioInit != null) {
				stage_info.Value.OnScenarioInit();
			}
		}

		scenario = GetComponent<Scenario>();
		if (scenario == null) UseText = true;

		if (UseText) {
			root_entry = new ScenarioEntry();
			root_entry.subentries = new List<ScenarioEntry>();
			ParseSourceCode();
		} else {
			root_entry = scenario.root_entry;
		}

		AddRootStageThread(true);
	}

	void ParseSourceCode() {
		int nObjects = source_code.objects.Length;
		var levels = new List<ScenarioEntry>();
		var indentations = new List<int>();

		levels.Add(root_entry);
		indentations.Add(-1);

		var lines = source_code.lines;
		for (int iL = 0; iL < lines.Length; iL++) {
			var line = lines[iL];

			bool only_whitespace = true;
			int indentation = 0;

			var str_builder = new System.Text.StringBuilder();
			var entry = new ScenarioEntry();
			entry.subentries = new List<ScenarioEntry>();
			UnityEngine.Object obj_data = null;

			foreach (string text_data in TextWithFields.ParseElements(line)) {
				if (TextWithFields.IsMeta(text_data)) {
					if (TextWithFields.IsTag(text_data, TextWithFields.TAG_OBJECT)) {
						string meta_data = text_data;
						TextWithFields.TagAndData(ref meta_data);
						int storage_id;
						if (!int.TryParse(meta_data, out storage_id)) storage_id = -1;
						if ((storage_id >= 0) && (storage_id < nObjects)) {
							obj_data = source_code.objects[storage_id];
						}
						only_whitespace = false;
					}
				} else {
					if (only_whitespace) {
						only_whitespace = char.IsWhiteSpace(text_data, 0);
						if (only_whitespace) indentation += 1;
						//if (text_data[0] == '\t') indentation += 1;
					}
					if (!only_whitespace) {
						str_builder.Append(text_data);
					}
				}
			}

			if (only_whitespace) continue;
			
			entry.val_data = str_builder.ToString();
			entry.obj_data = obj_data;

			while (indentation < indentations[indentations.Count-1]) {
				levels.RemoveAt(levels.Count-1);
				indentations.RemoveAt(indentations.Count-1);
			}

			ScenarioEntry parent_level;

			if (indentation > indentations[indentations.Count-1]) {
				parent_level = levels[levels.Count-1];
				levels.Add(entry);
				indentations.Add(indentation);
			} else {
				parent_level = levels[levels.Count-2];
				levels[levels.Count-1] = entry;
			}
			
			parent_level.subentries.Add(entry);
		}
	}

	public void AddRootStageThread(bool update=false) {
		UpdateTimeVars();

		var root_stage = ScenarioStages.Stage_Nested.Create(
			null, root_entry) as ScenarioStages.Stage_Nested;
		root_stage.substitutes = new Dictionary<string, string>();
		root_stage.obj_substitutes = new Dictionary<string, Object>();
		root_stage.scenario_player = this;
		root_stage.scenario_thread = new ScenarioThread("", root_stage);
		root_stage.Init("", null);
		
		threads.Add(root_stage.scenario_thread);

		if (update) root_stage.Update();
	}

	void OnLevelWasLoaded(int level) {
		UpdateTimeVars();

		foreach (var stage_info in ScenarioStages.StageInfos) {
			if (stage_info.Value.OnLevelWasLoaded != null) {
				stage_info.Value.OnLevelWasLoaded();
			}
		}
	}

	void Awake() {
		var debug_gui = DebugGUI.Instance;
		if (debug_gui != null) {
			if (wndScenario == null) {
				wndScenario = new ScenarioWindow();
				debug_gui.AddExtraWindow(wndScenario);
			}
			//debug_gui.InitWindows(true);
		}
		
		if (InitOnAwake) Init();
	}

	void Start() {
		Init();
	}

	void Update() {
		bool do_cleanup = false;

		UpdateTimeVars();
		
		bool were_exceptions = false;
		
		for (int i = 0; i < threads.Count; i++) {
			var thread = threads[i];
			try {
				thread.root_stage.Update();
			} catch (System.Exception exc) {
				were_exceptions = true;
				DebugGUI.LogTag = thread.tag;
				if (Debug.isDebugBuild) Debug.LogException(exc);
				// Stop this thread's execution to make the error more noticeable
				thread.root_stage.is_finished = true;
			}
			if (thread.root_stage.is_finished) {
				do_cleanup = true;
			}
		}
		
		if (were_exceptions) {
			var debug_gui = DebugGUI.Instance;
			debug_gui.InitWindows();
			//debug_gui.InitWindows(true);
			if (!debug_gui.wndLogs.is_visible) {
				var rect = debug_gui.wndLogs.windowRect;
				rect.width = Screen.width*0.5f;
				rect.height = Screen.height*0.5f;
				debug_gui.wndLogs.windowRect = rect;
				debug_gui.wndLogs.scrollPosition = new Vector2(0, float.PositiveInfinity);
				debug_gui.wndLogs.Show();
			}
		}
		
		if (do_cleanup) {
			for (int i = threads.Count-1; i >= 0; i--) {
				var thread = threads[i];
				if (thread.root_stage.is_finished) {
					threads.RemoveAt(i);
				}
			}
		}
	}

	void OnGUI() {
		for (int i = 0; i < threads.Count; i++) {
			threads[i].root_stage.OnGUI();
		}
	}
	
	public static bool SyncTimeRealtime {get; set;}
	public static AudioSource SyncTimeSound {get; set;}
	public static float SyncTime {
		get {
			if (SyncTimeSound != null) return SyncTimeSound.time;
			return SyncTimeRealtime ? Time.realtimeSinceStartup : Time.time;
		}
	}

	public class ScenarioWindow : GuiWindow {
		public ScenarioWindow() : base() {
			title = "Scenarios";
			windowRect = new Rect(0, 0, 200, 200);
			Center();
			BodyCallback = Draw_Body;
		}
		static GUIStyle btn_style = null;
		void InitStyles() {
			if (btn_style == null) {
				btn_style = new GUIStyle(GUI.skin.button);
				btn_style.alignment = TextAnchor.MiddleLeft;
				btn_style.margin = new RectOffset(0, 0, 0, 0);
			}
		}
		void Draw_Body(GuiWindow wnd) {
			InitStyles();

			//GUILayout.Label("Threads:");
			string filter_tag = DebugGUI.Instance.wndLogs.filterTag;
			foreach (string tag in all_thread_tags) {
				bool is_tag = (tag == filter_tag);
				bool use_tag = GUILayout.Toggle(is_tag, tag, btn_style, GUILayout.ExpandWidth(false));
				if (use_tag && !is_tag) {
					filter_tag = tag;
				} else if (!use_tag && is_tag) {
					filter_tag = null;
				}
			}
			DebugGUI.Instance.wndLogs.filterTag = filter_tag;

			//GUILayout.Space(8);

			//GUILayout.Label("Variables:");
			foreach (var kv in global_vars) {
				GUILayout.Label(kv.Key+" := "+kv.Value);
			}

			//GUILayout.Space(8);
			
			//GUILayout.Label("Object variables:");
			foreach (var kv in global_obj_vars) {
				GUILayout.Label(kv.Key+" := "+kv.Value.name);
			}
		}
	}
}

public class ScenarioThread {
	public string name {get; private set;}
	public ScenarioStages.Stage root_stage {get; private set;}

	internal bool stop_scenario = false;
	internal string goto_label = "";
	internal bool goto_loop = false;

	public string tag {get; private set;}

	public ScenarioThread(string name, ScenarioStages.Stage stage) {
		this.name = name;
		root_stage = stage;

		tag = root_stage.scenario_player.name;
		if (!string.IsNullOrEmpty(name)) tag += " ("+name+")";

		if (!ScenarioPlayer.all_thread_tags.Contains(tag)) {
			ScenarioPlayer.all_thread_tags.Add(tag);
		}
	}

	public void SetGoto(string goto_label, bool goto_loop=false) {
		this.goto_label = goto_label;
		this.goto_loop = goto_loop;
	}
}

public static partial class ScenarioStages {
	public class StageInfo {
		public delegate Stage StageCreator(Stage stage, ScenarioEntry source);
		public StageCreator Creator = null;
		public System.Action OnScenarioInit = null;
		public System.Action OnLevelWasLoaded = null;
		public string CmdName = "";
		public bool Foldout = false;
	}
	static Dictionary<string, StageInfo> stage_infos = null;
	public static Dictionary<string, StageInfo> StageInfos {
		get {
			if (stage_infos == null) GatherStages();
			return stage_infos;
		}
	}

	public static bool IsStageFoldout(string val_data) {
		if (foldout_stages == null) GatherStages();
		foreach (var stage_name in foldout_stages) {
			if ((val_data == stage_name) || val_data.StartsWith(stage_name+" ")) {
				return true;
			}
		}
		return false;
	}
	static List<string> foldout_stages = null;

	static dlgT _GetStaticDelegate<dlgT>(System.Type t, string name) where dlgT : class {
		var flags = System.Reflection.BindingFlags.Public | System.Reflection.BindingFlags.Static;
		var method = t.GetMethod(name, flags);
		if (method == null) return null;
		return System.Delegate.CreateDelegate(typeof(dlgT), method, false) as dlgT;
	}

	static void GatherStages() {
		if (stage_infos != null) return;

		stage_infos = new Dictionary<string, StageInfo>();
		foldout_stages = new List<string>();

		foreach (var t in typeof(ScenarioStages).GetNestedTypes()) {
			if (!t.IsSubclassOf(typeof(Stage))) continue;

			var creator = _GetStaticDelegate<StageInfo.StageCreator>(t, "Create");
			if (creator == null) continue;

			var onScenarioInit = _GetStaticDelegate<System.Action>(t, "OnScenarioInit");
			var onLevelWasLoaded = _GetStaticDelegate<System.Action>(t, "OnLevelWasLoaded");

			foreach (var attr in t.GetCustomAttributes(true)) {
				var stage_attr = attr as StageAttribute;
				if (stage_attr == null) continue;

				var stage_info = new StageInfo();
				stage_info.Creator = creator;
				stage_info.OnScenarioInit = onScenarioInit;
				stage_info.OnLevelWasLoaded = onLevelWasLoaded;
				stage_info.CmdName = stage_attr.CmdName;
				stage_info.Foldout = stage_attr.Foldout;

				stage_infos.Add(stage_info.CmdName, stage_info);
				if (stage_info.Foldout) foldout_stages.Add(stage_info.CmdName);
			}
		}
	}

	[System.AttributeUsage(System.AttributeTargets.Class, AllowMultiple=true)]
	public class StageAttribute : System.Attribute {
		public string CmdName {get; private set;}
		public bool Foldout {get; private set;}
		public StageAttribute(string name, bool foldout=false) {
			CmdName = name;
			Foldout = foldout;
		}
	}
	
	public class Stage {
		public ScenarioPlayer scenario_player;
		public ScenarioThread scenario_thread;
		public Stage parent_stage;
		public ScenarioEntry source = null;
		public bool is_finished = true;

		public Dictionary<string, string> substitutes = null;
		public Dictionary<string, Object> obj_substitutes = null;

		public string GetVar(string name, string dflt="", bool add_brackets=false) {
			if (substitutes == null) return dflt;
			if (add_brackets) name = "["+name+"]";
			string ws;
			if (!substitutes.TryGetValue(name, out ws)) return dflt;
			return ws;
		}
		public int GetIntVar(string name, int dflt=0, bool add_brackets=false) {
			if (substitutes == null) return dflt;
			if (add_brackets) name = "["+name+"]";
			string ws;
			if (!substitutes.TryGetValue(name, out ws)) return dflt;
			int v;
			if (!int.TryParse(ws, out v)) return dflt;
			return v;
		}
		public float GetFloatVar(string name, float dflt=0f, bool add_brackets=false) {
			if (substitutes == null) return dflt;
			if (add_brackets) name = "["+name+"]";
			string ws;
			if (!substitutes.TryGetValue(name, out ws)) return dflt;
			float v;
			if (!float.TryParse(ws, out v)) return dflt;
			return v;
		}
		public bool GetBoolVar(string name, bool dflt=false, bool add_brackets=false) {
			if (substitutes == null) return dflt;
			if (add_brackets) name = "["+name+"]";
			string ws;
			if (!substitutes.TryGetValue(name, out ws)) return dflt;
			int v;
			if (!int.TryParse(ws, out v)) return dflt;
			return (v != 0);
		}
		public Object GetObjVar(string name, Object dflt=null, bool add_brackets=false) {
			if (obj_substitutes == null) return dflt;
			if (add_brackets) name = "{"+name+"}";
			Object obj;
			if (!obj_substitutes.TryGetValue(name, out obj)) return dflt;
			return obj;
		}
		
		public void SetVar(string name, string v, bool add_brackets=false) {
			if (add_brackets) name = "["+name+"]";
			if (substitutes == null) substitutes = new Dictionary<string, string>();
			substitutes[name] = v;
		}
		public void SetIntVar(string name, int v, bool add_brackets=false) {
			if (add_brackets) name = "["+name+"]";
			if (substitutes == null) substitutes = new Dictionary<string, string>();
			substitutes[name] = v.ToString();
		}
		public void SetFloatVar(string name, float v, bool add_brackets=false) {
			if (add_brackets) name = "["+name+"]";
			if (substitutes == null) substitutes = new Dictionary<string, string>();
			substitutes[name] = v.ToString();
		}
		public void SetBoolVar(string name, bool v, bool add_brackets=false) {
			if (add_brackets) name = "["+name+"]";
			if (substitutes == null) substitutes = new Dictionary<string, string>();
			substitutes[name] = (v ? 1 : 0).ToString();
		}
		public void SetObjVar(string name, Object v, bool add_brackets=false) {
			if (add_brackets) name = "{"+name+"}";
			if (obj_substitutes == null) obj_substitutes = new Dictionary<string, Object>();
			obj_substitutes[name] = v;
		}

		public Stage Setup(Stage stage, ScenarioEntry source) {
			parent_stage = stage;
			if (stage != null) {
				this.substitutes = stage.substitutes;
				this.obj_substitutes = stage.obj_substitutes;
				this.scenario_player = stage.scenario_player;
				this.scenario_thread = stage.scenario_thread;
			}
			this.source = source;
			is_finished = (source == null);
			return this;
		}

		public virtual void Init(string cmd_name, string[] args) {
		}

		public virtual void Update() {
		}

		public virtual void OnGUI() {
		}
		
		#region Objects gathering and substitution
		public Object SubstituteObj(ScenarioEntry entry) {
			foreach (var kv in obj_substitutes) {
				if (entry.val_data.Contains(kv.Key)) {
					return kv.Value;
				}
			}
			foreach (var kv in ScenarioPlayer.global_obj_vars) {
				if (entry.val_data.Contains(kv.Key)) {
					return kv.Value;
				}
			}
			return entry.obj_data;
		}

		public T MainObject<T>() where T : class {
			return SubstituteObj(source) as T;
		}

		public IEnumerable<T> GatherObjects<T>(bool include_this=true, bool include_nested=true, bool use_group=true) where T : class {
			if (source == null) yield break;
			
			if (include_this) {
				foreach (T element in IterateGroup<T>(SubstituteObj(source), use_group)) {
					yield return element;
				}
			}
			
			if (include_nested) {
				var subentries = source.subentries;
				for (int i = 0; i < subentries.Count; i++) {
					foreach (T element in IterateGroup<T>(SubstituteObj(subentries[i]), use_group)) {
						yield return element;
					}
				}
			}
		}

		public IEnumerable<T> IterateGroup<T>(Object obj, bool use_group=true) where T : class {
			if (obj == null) yield break;
			
			var tfm = obj as Transform;
			if (tfm == null) {
				var gameobj = obj as GameObject;
				if (gameobj != null) tfm = gameobj.transform;
			}
			if (tfm == null) {
				var component = obj as Component;
				if (component != null) tfm = component.transform;
			}

			if ((tfm != null) && use_group) {
				var obj_group = tfm.GetComponent<GameObjectGroup>();
				if (obj_group != null) {
					foreach (var child in obj_group.FilteredChildren()) {
						yield return ObjOrTfmTo<T>(null, child);
					}
					yield break;
				}
			}

			yield return ObjOrTfmTo<T>(obj, tfm);
		}
		T ObjOrTfmTo<T>(Object obj, Transform tfm) where T : class {
			T T_obj = obj as T;
			if (T_obj != null) return T_obj;
			T_obj = tfm as T;
			if (T_obj != null) return T_obj;
			T_obj = tfm.gameObject as T;
			if (T_obj != null) return T_obj;
			T_obj = tfm.GetComponent(typeof(T)) as T;
			if (T_obj != null) return T_obj;
			return null;
		}
		#endregion

		#region Argument interpretation
		protected string ArgAt(string[] args, int i, string def_val="") {
			if (args == null) return def_val;
			if ((args == null) || (i >= args.Length)) return def_val;
			return args[i];
		}
		protected int IntArgAt(string[] args, int i, int def_val=0) {
			return ParseIntDefault(ArgAt(args, i), def_val);
		}
		protected float FloatArgAt(string[] args, int i, float def_val=0f) {
			return ParseFloatDefault(ArgAt(args, i), def_val);
		}

		protected string FindArg(string[] args, string name, string def_val="") {
			if (args == null) return def_val;
			string prefix = name+"=";
			for (int i = 0; i < args.Length; i++) {
				var arg_val = args[i];
				if (arg_val.StartsWith(prefix)) {
					return arg_val.Substring(prefix.Length);
				}
			}
			return def_val;
		}
		protected int FindIntArg(string[] args, string name, int def_val=0) {
			return ParseIntDefault(FindArg(args, name), def_val);
		}
		protected float FindFloatArg(string[] args, string name, float def_val=0f) {
			return ParseFloatDefault(FindArg(args, name), def_val);
		}

		protected int ParseIntDefault(string ws, int def_val=0) {
			int result;
			if (ParseInt(ws, out result)) return result;
			return def_val;
		}
		protected bool ParseInt(string ws, out int result) {
			int i_split = ws.IndexOf(":");
			if (i_split == -1) {
				return int.TryParse(ws, out result);
			} else {
				string part0 = ws.Substring(0, i_split);
				string part1 = ws.Substring(i_split+1);

				int val0, val1;
				result = 0;

				if (!int.TryParse(part0, out val0)) return false;
				result = val0;

				if (!int.TryParse(part1, out val1)) return true;
				val0 = Mathf.Min(val0, val1);
				val1 = Mathf.Max(result, val1);
				// min is inclusive, max is exclusive!
				result = Random.Range(val0, val1+1);

				return true;
			}
		}

		protected float ParseFloatDefault(string ws, float def_val=0f) {
			float result;
			if (ParseFloat(ws, out result)) return result;
			return def_val;
		}
		protected bool ParseFloat(string ws, out float result) {
			int i_split = ws.IndexOf(":");
			if (i_split == -1) {
				return float.TryParse(ws, out result);
			} else {
				string part0 = ws.Substring(0, i_split);
				string part1 = ws.Substring(i_split+1);

				float val0, val1;
				result = 0f;

				if (!float.TryParse(part0, out val0)) return false;
				result = val0;

				if (!float.TryParse(part1, out val1)) return true;
				result = Random.Range(val0, val1);

				return true;
			}
		}

		protected bool ArgsContain(string[] args, string key, int i0=0) {
			if (args == null) return false;
			if ((i0 < 0) || (i0 >= args.Length)) return false;
			int i = System.Array.IndexOf<string>(args, key, i0);
			return i != -1;
		}
		#endregion

		public void AssignVariable(string source_data, ScenarioEntry source) {
			AssignVariable(source_data, source.obj_data);
		}
		public void AssignVariable(string source_data, Object obj) {
			// This is a macro definition / variable assignment
			int assign_idx = source_data.IndexOf(":=");
			string var_name = source_data.Substring(0, assign_idx).Trim();
			string var_value = source_data.Substring(assign_idx+2);
			
			// Is this a global var or local var?
			var str_vars = ScenarioPlayer.global_vars;
			var obj_vars = ScenarioPlayer.global_obj_vars;
			if (var_name.EndsWith("$")) {
				var_name = var_name.Substring(0, var_name.Length-1).Trim();
				str_vars = substitutes;
				obj_vars = obj_substitutes;
			}
			
			if (var_name.StartsWith("{") && var_name.EndsWith("}")) {
				// This is an object variable
				var gObj = obj as GameObject;
				if (gObj != null) {
					var_value = var_value.Trim();
					if (!string.IsNullOrEmpty(var_value)) {
						// Misha says it's better to NOT search by part of name
						if (!var_value.StartsWith("/")) var_value = "/" + var_value;
						if (!var_value.EndsWith("/")) var_value = var_value + "/";
						var child = gObj.transform.FindPartial(var_value);
						//var child = gObj.transform.Find(var_value);
						obj = (child != null) ? child.gameObject : null;
					}
				}
				obj_vars[var_name] = obj;
			} else {
				#region Parse value
				if (var_value.StartsWith("=")) {
					// Exact/literal value
					var_value = var_value.Substring(1);
				} else {
					// Evaluate
					var_value = var_value.Trim();
					bool this_is_math = false;
					string operatorAB = "";
					double operandA = 0.0;
					double operandB = 0.0;
					var toks = var_value.Split(' ');
					for (int i = 0; i < toks.Length; i++) {
						string tok = toks[i];
						if (@"+-*/".Contains(tok)) {
							operatorAB = tok;
						} else {
							if (TryParseNumber(ref tok, ref operandB)) {
								if (operatorAB == @"+") {
									operandB = operandA + operandB;
									this_is_math = true;
								} else if (operatorAB == @"-") {
									operandB = operandA - operandB;
									this_is_math = true;
								} else if (operatorAB == @"*") {
									operandB = operandA * operandB;
									this_is_math = true;
								} else if (operatorAB == @"/") {
									operandB = operandA / operandB;
									this_is_math = true;
								}
								operandA = operandB;
							}
						}
						toks[i] = tok;
					}
					if (this_is_math) {
						var_value = operandB.ToString();
					} else {
						var_value = string.Join(" ", toks);
					}

//					if (var_value.Contains(":") && !var_value.Contains(" ")) {
//						if (var_value.Contains(".")) {
//							float tmp;
//							if (ParseFloat(var_value, out tmp)) {
//								var_value = tmp.ToString();
//							}
//						} else {
//							int tmp;
//							if (ParseInt(var_value, out tmp)) {
//								var_value = tmp.ToString();
//							}
//						}
//					}
					
					str_vars[var_name] = var_value;
				}
				#endregion
			}
		}
		bool TryParseNumber(ref string var_value, ref double result) {
			if (var_value.Contains(".")) {
				float tmp;
				if (ParseFloat(var_value, out tmp)) {
					var_value = tmp.ToString();
					result = tmp;
					return true;
				}
			} else {
				int tmp;
				if (ParseInt(var_value, out tmp)) {
					var_value = tmp.ToString();
					result = tmp;
					return true;
				}
			}
			return false;
		}
	}

	[Stage("nested", true)]
	[Stage("if", true)]
	public class Stage_Nested : Stage {
		public int stage_id = 0;
		Stage stage = null;
		int loop_count = 1;

		int exit_mode = 0;
		bool is_if = false;

		internal static char[] delimiters = {' ', '\r', '\n', '\t'};
		const string ignore_entry_symbol = "\"";
		const string continue_symbol = "<";
		const string break_symbol = ">";
		const string return_symbol = "^";

		// Format/special characters/tokens:
		// " -- ignore the entry
		// ^ -- return
		// < -- continue
		// > -- break
		// A:B -- a random number in range [A, B]
		// (...) -- ignore what's inside the brackets
		// [name]=defaultvalue -- macro parameter declaration (default value is optional)
		// [name] -- (in macro body) substitute this with the corresponding parameter value
		// {} -- main object of the macro-invoking entry
		// {name} -- the correspondingly labelled object of the macro-invoking entry
		// @name -- a label for goto

		public static Stage Create(Stage stage, ScenarioEntry source) {
			return (new Stage_Nested()).Setup(stage, source);
		}
		override public void Init(string cmd_name, string[] args) {
			if (cmd_name == "if") {
				is_if = true;
				bool condition = false;
				if (args.Length == 1) {
					condition = (ParseFloatDefault(args[0], 0) != 0);
				} else if (args.Length == 3) {
					string arg1 = args[0];
					string op = args[1];
					string arg2 = args[2];
					switch (op) {
					case "==": condition = (arg1 == arg2); break;
					case "!=": condition = (arg1 != arg2); break;
					case "=": condition = (ParseFloatDefault(arg1, 0) == ParseFloatDefault(arg2, 0)); break;
					case "<>": condition = (ParseFloatDefault(arg1, 0) != ParseFloatDefault(arg2, 0)); break;
					case "<": condition = (ParseFloatDefault(arg1, 0) < ParseFloatDefault(arg2, 0)); break;
					case "<=": condition = (ParseFloatDefault(arg1, 0) <= ParseFloatDefault(arg2, 0)); break;
					case ">": condition = (ParseFloatDefault(arg1, 0) > ParseFloatDefault(arg2, 0)); break;
					case ">=": condition = (ParseFloatDefault(arg1, 0) >= ParseFloatDefault(arg2, 0)); break;
					default: condition = false; break;
					}
				}
				is_finished = !condition;
			} else {
				loop_count = IntArgAt(args, 0, 1); // negative -> loop until break
				
				string new_thread_name = FindArg(args, "thread", "");
				if (!string.IsNullOrEmpty(new_thread_name)) {
					parent_stage = null; // to not go out of thread on exit
					scenario_thread = new ScenarioThread(new_thread_name, this);
					scenario_player.threads.Add(scenario_thread);
				}
			}
		}

		override public void Update() {
			if (is_finished) return;
			
			if (source.subentries_count == 0) return;

			if (scenario_thread.stop_scenario) {
				// Exit until we reach the root scenario stage
				if (parent_stage == null) {
					scenario_thread.stop_scenario = false;
				}
				stage_id = source.subentries_count;
				is_finished = true;
				return;
			}

			// Initialization at the start of the loop
			if (stage == null) stage = ParseStage(source, stage_id);

			while (stage != null) {
				stage.Update();
				if (!stage.is_finished) break;

				// A break/continue/return directive has been encountered
				if ((exit_mode != 0) || scenario_thread.stop_scenario) {
					stage_id = source.subentries_count;
					if (exit_mode != 1) loop_count = 0;
					exit_mode = 0;
				}

				stage_id += 1;
				stage = ParseStage(source, stage_id);
			}
			
			if (stage == null) {
				bool has_goto = !string.IsNullOrEmpty(scenario_thread.goto_label);
				bool goto_loop = scenario_thread.goto_loop;
				bool has_parent = (parent_stage != null);
				if (has_goto && !has_parent && goto_loop) {
					stage_id = 0;
				} else {
					// This iteration has finished
					if (loop_count > 0) loop_count--;
					if (loop_count == 0) is_finished = true;
					if (!is_finished) stage_id = 0;
				}
			}
		}

		override public void OnGUI() {
			if (is_finished) return;

			if (source.subentries_count == 0) return;

			if (stage != null) stage.OnGUI();
		}

		static Regex obj_subs_regex = new Regex(@"\{[^{}]*\}");
		string PremodifyData(string source_data, ScenarioEntry source) {
			// String/number macro arguments
			foreach (var kv in substitutes) {
				source_data = source_data.Replace(kv.Key, kv.Value);
			}
			foreach (var kv in ScenarioPlayer.global_vars) {
				source_data = source_data.Replace(kv.Key, kv.Value);
			}

			// Object macro arguments
			foreach (var kv in obj_substitutes) {
				if (source_data.Contains(kv.Key)) {
					source_data = source_data.Replace(kv.Key, " ");
					source.obj_data = kv.Value;
				}
			}
			foreach (var kv in ScenarioPlayer.global_obj_vars) {
				if (source_data.Contains(kv.Key)) {
					source_data = source_data.Replace(kv.Key, " ");
					source.obj_data = kv.Value;
				}
			}

			// Remove any leftover object substitutes (in the expected format)
			source_data = obj_subs_regex.Replace(source_data, " ");

			return source_data;
		}

		static Regex comments_regex = new Regex(@"\([^()]*\)");
		string RemoveComments(string source_data) {
			if (scenario_player.UseBracketComments) {
				source_data = comments_regex.Replace(source_data, " ").Trim();
			}
			return source_data;
		}

		void PropagateExitMode() {
			if (!is_if) return;
			var parent_nested = parent_stage as Stage_Nested;
			if (parent_nested == null) return;
			parent_nested.exit_mode = exit_mode;
			parent_nested.PropagateExitMode();
		}
		void DetectExitMode(ref string stage_type, ref string[] stage_args) {
			if (stage_args == null) return;
			
			string last_token = stage_type;
			if (stage_args.Length != 0) last_token = stage_args[stage_args.Length-1];

			switch (last_token) {
			case continue_symbol: exit_mode = 1; PropagateExitMode(); break;
			case break_symbol: exit_mode = 2; PropagateExitMode(); break;
			case return_symbol: scenario_thread.stop_scenario = true; break;
			}

			if ((exit_mode != 0) || scenario_thread.stop_scenario) {
				// Remove the continue/break/return symbol from the command
				if (stage_args.Length != 0) {
					System.Array.Resize<string>(ref stage_args, stage_args.Length-1);
				} else {
					stage_type = "";
				}
			}
		}

		Stage ParseStage(ScenarioEntry parent, int id) {
			if ((id < 0) || (id >= parent.subentries_count)) return null;

			var source = parent.subentries[id];
			string source_data = source.val_data.Trim();

			// Ignore this entry if a specific symbol is found in it
			if (source_data.Contains(ignore_entry_symbol)) {
				return new Stage();
			}

//			if (parent_stage == null) {
//				bool _is_nested = (source_data == "nested") || source_data.StartsWith("nested ");
//				bool _is_macro = (source_data == "macro") || source_data.StartsWith("macro ");
//				if (!(_is_nested || _is_macro)) {
//					source_data = "nested "+source_data;
//				}
//			}
			
			// Substitute macro arguments and remove the comments
			int assign_idx = source_data.IndexOf(":=");
			if (assign_idx != -1) {
				assign_idx += 2; // length of ":="
				string tok0 = source_data.Substring(0, assign_idx);
				string tok1 = source_data.Substring(assign_idx);
				source_data = tok0 + PremodifyData(tok1, source);
			} else {
				source_data = PremodifyData(source_data, source);
			}
			string log_source_data = source_data;
			source_data = RemoveComments(source_data);

			// We are searching for goto label, skip until it's found
			if (!string.IsNullOrEmpty(scenario_thread.goto_label)) {
				if (source_data.StartsWith(scenario_thread.goto_label+" ")) {
					source_data = source_data.Substring(scenario_thread.goto_label.Length+1);
					scenario_thread.goto_label = "";
				} else if (source_data == scenario_thread.goto_label) {
					scenario_thread.goto_label = "";
					return new Stage(); // nothing else to do here
				} else if (source_data.StartsWith("nested ") || (source_data == "nested")) {
					// we need to search inside the nested
				} else {
					return new Stage();
				}
			}

			if (scenario_player.PrintTrace) {
				DebugGUI.LogTag = scenario_thread.tag;
				if (Debug.isDebugBuild) Debug.Log(log_source_data.AddTime());
			}
			
			if (assign_idx != -1) {
				AssignVariable(source_data, source);
				return new Stage();
			}
			
			// Split the command into command name and command arguments
			var tokens = source_data.Split(delimiters,
				System.StringSplitOptions.RemoveEmptyEntries);
			string stage_type = "";
			string[] stage_args = null;
			if ((tokens != null) && (tokens.Length != 0)) {
				stage_type = tokens[0];
				stage_args = new string[tokens.Length-1];
				System.Array.Copy(tokens, 1, stage_args, 0, tokens.Length-1);

				// Find if we should continue/break/return after this stage
				DetectExitMode(ref stage_type, ref stage_args);

				// Search in predefined commands...
				StageInfo stage_info = null;
				if (ScenarioStages.StageInfos.TryGetValue(stage_type, out stage_info)) {
					Stage stage = stage_info.Creator(this, source);
					stage.Init(stage_type, stage_args);
					// Skip (don't wait) if this is a "start new thread" stage
					if (stage.scenario_thread != scenario_thread) return new Stage();
					return stage;
				}
	
				// Search in macros...
				ScenarioEntry macro_entry = null;
				if (Stage_Macro.macros.TryGetValue(stage_type, out macro_entry)) {
					var stage = Stage_Nested.Create(this, macro_entry);
					stage.substitutes = BuildSubstitutes(macro_entry, stage_args);
					stage.obj_substitutes = BuildObjSubstitutes(source);
					return stage;
				}
			}

			// Unknown stage, will be skipped
			return new Stage();
		}

		static Dictionary<string, string> BuildSubstitutes(ScenarioEntry source, string[] stage_args) {
			var tokens = source.val_data.Split(delimiters,
				System.StringSplitOptions.RemoveEmptyEntries);
			//var macro_name = tokens[1];
			var macro_args = new string[tokens.Length-2];
			System.Array.Copy(tokens, 2, macro_args, 0, tokens.Length-2);

			var substitutes = new Dictionary<string, string>();
			for (int i = 0; i < macro_args.Length; i++) {
				string[] arg_toks = macro_args[i].Split('=');
				string arg_name = arg_toks[0].Trim();
				string arg_value = "";
				if (i < stage_args.Length) {
					arg_value = stage_args[i];
				} else if (arg_toks.Length > 1) {
					arg_value = arg_toks[1];
				}
				substitutes.Add(arg_name, arg_value);
			}

			return substitutes;
		}

		static Dictionary<string, Object> BuildObjSubstitutes(ScenarioEntry source) {
			var obj_substitutes = new Dictionary<string, Object>();

			obj_substitutes.Add("{}", source.obj_data);

			var subentries = source.subentries;
			for (int i = 0; i < subentries.Count; i++) {
				var subentry = subentries[i];
				obj_substitutes.Add(subentry.val_data.Trim(), subentry.obj_data);
			}

			return obj_substitutes;
		}
	}

	[Stage("macro", true)]
	public class Stage_Macro : Stage {
		public static Dictionary<string, ScenarioEntry> macros = new Dictionary<string, ScenarioEntry>();

		public static Stage Create(Stage stage, ScenarioEntry source) {
			return (new Stage_Macro()).Setup(stage, source);
		}
		override public void Init(string cmd_name, string[] args) {
			if ((args != null) && (args.Length != 0)) {
				string macros_name = args[0];
				if (!string.IsNullOrEmpty(macros_name)) {
					if (macros.ContainsKey(macros_name)) {
						macros.Remove(macros_name);
					}
					macros.Add(macros_name, source);
				}
			}
			is_finished = true;
		}
	}

	[Stage("print")]
	public class Stage_Print : Stage {
		public static Stage Create(Stage stage, ScenarioEntry source) {
			return (new Stage_Print()).Setup(stage, source);
		}
		override public void Init(string cmd_name, string[] args) {
			string txt = string.Join(" ", args);

			var main_obj = MainObject<Object>();
			if (main_obj != null) {
				if (txt.Length != 0) txt += " ";
				var gObj = main_obj as GameObject;
				if (gObj != null) {
					txt += gObj.transform.HierarchyPath();
				} else {
					txt += main_obj.ToString();
				}
			}

			DebugGUI.LogTag = scenario_thread.tag;
			if (Debug.isDebugBuild) Debug.Log(txt);

			is_finished = true;
		}
	}

	[Stage("goto")]
	public class Stage_Goto : Stage {
		public static Stage Create(Stage stage, ScenarioEntry source) {
			return (new Stage_Goto()).Setup(stage, source);
		}
		override public void Init(string cmd_name, string[] args) {
			if (args.Length != 0) {
				string goto_label = args[0];
				bool goto_loop = ArgsContain(args, "loop", 1);
				var match = Regex.Match(goto_label, @"^([^0-9]*)([0-9]+:[0-9]+)([^0-9]*)$");
				if (match.Success) {
					goto_label = match.Groups[1].Value + 
						ParseIntDefault(match.Groups[2].Value, 0).ToString() + 
						match.Groups[3].Value;
				}
				scenario_thread.SetGoto(goto_label, goto_loop);
			}
			is_finished = true;
		}
	}

	[Stage("wait")]
	public class Stage_Wait : Stage {
		float start_time = 0;
		float delay = 0;
		
		public static Stage Create(Stage stage, ScenarioEntry source) {
			return (new Stage_Wait()).Setup(stage, source);
		}
		override public void Init(string cmd_name, string[] args) {
			delay = FloatArgAt(args, 0, 0f);
			start_time = Time.time;
		}

		override public void Update() {
			if (Time.time > (start_time + delay)) is_finished = true;
		}
	}

	[Stage("send")]
	public class Stage_Send : Stage {
		public static Stage Create(Stage stage, ScenarioEntry source) {
			return (new Stage_Send()).Setup(stage, source);
		}
		override public void Init(string cmd_name, string[] args) {
			string msg_name = ArgAt(args, 0, "");
			if (!string.IsNullOrEmpty(msg_name)) {
				string msg_value = ArgAt(args, 1, "");
				foreach (var obj in GatherObjects<GameObject>()) {
					obj.SendMessage(msg_name, msg_value);
				}
			}
			is_finished = true;
		}
	}

	[Stage("broadcast")]
	public class Stage_Broadcast : Stage {
		public static Stage Create(Stage stage, ScenarioEntry source) {
			return (new Stage_Broadcast()).Setup(stage, source);
		}
		override public void Init(string cmd_name, string[] args) {
			string msg_name = ArgAt(args, 0, "");
			if (!string.IsNullOrEmpty(msg_name)) {
				switch (args.Length) {
				case 1: Messenger.Broadcast(msg_name); break;
				case 2: Messenger.Broadcast(msg_name, ArgAt(args, 1, "")); break;
				case 3: Messenger.Broadcast(msg_name, ArgAt(args, 1, ""), ArgAt(args, 2, "")); break;
				case 4: Messenger.Broadcast(msg_name, ArgAt(args, 1, ""), ArgAt(args, 2, ""), ArgAt(args, 3, "")); break;
				case 5: Messenger.Broadcast(msg_name, ArgAt(args, 1, ""), ArgAt(args, 2, ""), ArgAt(args, 3, ""), ArgAt(args, 4, "")); break;
				default: Messenger.Broadcast(msg_name, ArgAt(args, 1, ""), ArgAt(args, 2, ""), ArgAt(args, 3, ""), ArgAt(args, 4, ""), ArgAt(args, 5, "")); break;
				}
			}
			is_finished = true;
		}
	}

	[Stage("gccollect")]
	public class Stage_GCCollect : Stage {
		public static Stage Create(Stage stage, ScenarioEntry source) {
			return (new Stage_GCCollect()).Setup(stage, source);
		}
		override public void Init(string cmd_name, string[] args) {
			RuntimeAssets.GCCollect();
			System.GC.Collect();
			Resources.UnloadUnusedAssets();
			is_finished = true;
		}
	}

	[Stage("pressbtn")]
	public class Stage_PressBtn : Stage {
		bool prev_UserInput;
		int update_counter = 0;
		
		public static Stage Create(Stage stage, ScenarioEntry source) {
			return (new Stage_PressBtn()).Setup(stage, source);
		}
		public override void Init(string cmd_name, string[] args) {
			is_finished = true;
			
			var mainObj = MainObject<GameObject>();
			if (mainObj == null) return;
			var widget = mainObj.GetComponent<SpriteGUIWidget>();
			if (widget == null) return;
			
			SpriteGUIController.Instance.EmulateMouseCollision(widget, 1);
			
			var mouse_emulator = MouseEmulator.Instance;
			mouse_emulator.SetButton(0, true);
			mouse_emulator.SetButtonDelta(0, 1);
			
			prev_UserInput = mouse_emulator.UserInput;
			mouse_emulator.UserInput = false;
			
			is_finished = false;
		}
		
		public override void Update() {
			if (is_finished) return;
			
			update_counter++;
			
			if (update_counter > 1) {
				var mouse_emulator = MouseEmulator.Instance;
				mouse_emulator.UserInput = prev_UserInput;
				
				is_finished = true;
			}
		}
	}

	[Stage("show")]
	[Stage("hide")]
	public class Stage_ShowHide : Stage {
		public static Stage Create(Stage stage, ScenarioEntry source) {
			return (new Stage_ShowHide()).Setup(stage, source);
		}
		override public void Init(string cmd_name, string[] args) {
			bool all = ArgsContain(args, "all");
			bool nonbehaviors = (FindIntArg(args, "NB", 1) != 0);
			bool behaviors = (FindIntArg(args, "B", 1) != 0);
			bool state = (cmd_name == "show");
			foreach (var obj in GatherObjects<GameObject>()) {
				obj.EnableAllComponents(state, all, nonbehaviors, behaviors);
			}
			is_finished = true;
		}
	}
	
	[Stage("setactive")]
	public class Stage_SetActive : Stage {
		public static Stage Create(Stage stage, ScenarioEntry source) {
			return (new Stage_SetActive()).Setup(stage, source);
		}
		override public void Init(string cmd_name, string[] args) {
			bool state = (IntArgAt(args, 0, 1) != 0);
			foreach (var obj in GatherObjects<GameObject>()) {
				obj.SetActive(state);
			}
			is_finished = true;
		}
	}
	
	[Stage("syncto")]
	public class Stage_SyncTo : Stage {
		public static Stage Create(Stage stage, ScenarioEntry source) {
			return (new Stage_SyncTo()).Setup(stage, source);
		}
		override public void Init(string cmd_name, string[] args) {
			bool to_gametime = ArgsContain(args, "gametime");
			bool to_realtime = ArgsContain(args, "realtime");
			string sound_name = FindArg(args, "sound");
			bool to_sound = !string.IsNullOrEmpty(sound_name);

			if (to_gametime) {
				ScenarioPlayer.SyncTimeRealtime = false;
			} else if (to_realtime) {
				ScenarioPlayer.SyncTimeRealtime = true;
			}

			if (to_sound) {
				var cam = Camera.main;
				if (cam != null) {
					var sound_tfm = cam.transform.Find(sound_name);
					if (sound_tfm == null) sound_tfm = cam.transform.Find("Audio:" + sound_name);
					if (sound_tfm != null) {
						ScenarioPlayer.SyncTimeSound = sound_tfm.GetComponent<AudioSource>();
					}
				}
			}

			// Update the time
			ScenarioPlayer.SetFloatVar("[Time.time]", ScenarioPlayer.SyncTime);

			is_finished = true;
		}
		public static void OnScenarioInit() {
			//ScenarioPlayer.SyncTimeSound = null;
		}
	}

	[Stage("timescale")]
	public class Stage_TimeScale : Stage {
		public static Stage Create(Stage stage, ScenarioEntry source) {
			return (new Stage_TimeScale()).Setup(stage, source);
		}
		override public void Init(string cmd_name, string[] args) {
			Time.timeScale = FloatArgAt(args, 0);
			is_finished = true;
		}
		public static void OnScenarioInit() {
			Time.timeScale = 1f;
		}
		public static void OnLevelWasLoaded() {
			// Well, OnLevelWasLoaded seems to be called only if object
			// already/still exists at the moment of new level loading
			Time.timeScale = 1f;
		}
	}
	
	[Stage("animspeed")]
	public class Stage_Animspeed : Stage {
		public static Stage Create(Stage stage, ScenarioEntry source) {
			return (new Stage_Animspeed()).Setup(stage, source);
		}
		override public void Init(string cmd_name, string[] args) {
			ScenarioUtils.AnimSpeed = FloatArgAt(args, 0);
			is_finished = true;
		}
		public static void OnScenarioInit() {
			ScenarioUtils.AnimSpeed = 1f;
		}
		public static void OnLevelWasLoaded() {
			// Well, OnLevelWasLoaded seems to be called only if object
			// already/still exists at the moment of new level loading
			ScenarioUtils.AnimSpeed = 1f;
		}
	}
	
	[Stage("soundvolume")]
	public class Stage_Soundvolume : Stage {
		public static Stage Create(Stage stage, ScenarioEntry source) {
			return (new Stage_Soundvolume()).Setup(stage, source);
		}
		override public void Init(string cmd_name, string[] args) {
			ScenarioUtils.SoundVolume = FloatArgAt(args, 0);
			is_finished = true;
		}
		public static void OnScenarioInit() {
			ScenarioUtils.SoundVolume = 1f;
		}
		public static void OnLevelWasLoaded() {
			// Well, OnLevelWasLoaded seems to be called only if object
			// already/still exists at the moment of new level loading
			ScenarioUtils.SoundVolume = 1f;
		}
	}

	[Stage("anim")]
	[Stage("anim0")]
	[Stage("anim0p")]
	[Stage("anim0r")]
	[Stage("anim0s")]
	[Stage("anim0pr")]
	[Stage("anim0ps")]
	[Stage("anim0rs")]
	public class Stage_Anim : Stage {
		List<AnimBasic> anims = new List<AnimBasic>();
		bool all_anims_finished = false;
		float interloop_time = 0f;

		string anim_name = "";
		int loops = 1;
		string speed_arg = "";
		string time_arg = "";
		string pause_arg = "";
		string time_start_arg = "";

		float endtime = -1f;

		string sound_volume_arg = "";
		bool align_sound=true;

		bool reset_position = false;
		bool reset_rotation = false;
		bool reset_scale = false;

		public static Stage Create(Stage stage, ScenarioEntry source) {
			return (new Stage_Anim()).Setup(stage, source);
		}
		override public void Init(string cmd_name, string[] args) {
			#region Command parameters
			// Parameters: reset flags
			if (cmd_name == "anim0") {
				reset_position = true;
				reset_rotation = true;
				reset_scale = true;
			} else if (cmd_name.StartsWith("anim0")) {
				string anim0_params = cmd_name.Substring("anim0".Length);
				reset_position = anim0_params.Contains("p");
				reset_rotation = anim0_params.Contains("r");
				reset_scale = anim0_params.Contains("s");
			}
			
			anim_name = ArgAt(args, 0);

			loops = Mathf.Max(FindIntArg(args, "L", 1), 0);
			speed_arg = FindArg(args, "S", "");
			time_arg = FindArg(args, "T", "");
			pause_arg = FindArg(args, "P", "");
			time_start_arg = FindArg(args, "From", "");
			
			string time0_arg = FindArg(args, "T0", "");
			float time0_scale = FindFloatArg(args, "T0_scale", 1f);
			string time1_arg = FindArg(args, "T1", "");
			float time1_scale = FindFloatArg(args, "T1_scale", 1f);
			if (!string.IsNullOrEmpty(time1_arg)) {
				//float curr_time = Time.time;
				float curr_time = ScenarioPlayer.SyncTime;
				float time0 = ParseFloatDefault(time0_arg, curr_time/time0_scale) * time0_scale;
				float time1 = ParseFloatDefault(time1_arg, (curr_time-time0)/time1_scale) * time1_scale;
				endtime = time0 + time1;
			}

			sound_volume_arg = FindArg(args, "V", "");
			align_sound = (FindIntArg(args, "align_sound", 1) != 0);

			bool silent = ArgsContain(args, "silent", 1);

			bool async = ArgsContain(args, "async", 1);
			bool stop = ArgsContain(args, "stop", 1);

			// Parameters: wrap mode
			bool wrap_mode_specified = true;
			WrapMode wrap_mode = WrapMode.Default;
			if (ArgsContain(args, "loop", 1)) {
				wrap_mode = WrapMode.Loop;
				async = true;
			} else if (ArgsContain(args, "pingpong", 1)) {
				wrap_mode = WrapMode.PingPong;
				async = true;
			} else if (ArgsContain(args, "clamp", 1) || ArgsContain(args, "once", 1)) {
				wrap_mode = WrapMode.Once; // Once and Clamp are synonymous
			} else if (ArgsContain(args, "clampforever", 1)) {
				wrap_mode = WrapMode.ClampForever;
				async = true;
			} else if (ArgsContain(args, "default", 1)) {
				wrap_mode = WrapMode.Default;
			} else {
				wrap_mode_specified = false;
				wrap_mode = WrapMode.Default;
			}
			#endregion

			foreach (var obj in GatherObjects<GameObject>()) {
				var anim = AnimBasic.Create(obj, anim_name);
				if (anim == null) continue;

				if (silent) {
					anim.enabled = true;
				} else {
					obj.EnableAllComponents(true, false);
				}

				if (stop && !wrap_mode_specified) {
					anim.Stop();
				} else {
					anim.wrapMode = wrap_mode;
					anims.Add(anim);
				}
			}

			ResetController();

			if (!stop) PlayWithSpeed();

			if (async || stop) is_finished = true;
		}

		void PlayWithSpeed() {
			bool speed_is_time = false;
			float anim_speed = 1f;

			if (endtime >= 0) {
				//float curr_time = Time.time;
				float curr_time = ScenarioPlayer.SyncTime;
				// ATTENTION! infinite speed results in lots of errors!
				anim_speed = Mathf.Max(loops, 1) / Mathf.Max(endtime - curr_time, float.Epsilon*100);
				speed_is_time = true;
			} else if (!string.IsNullOrEmpty(time_arg)) {
				anim_speed = 1f / ParseFloatDefault(time_arg, 1f);
				speed_is_time = true;
			} else {
				anim_speed = ParseFloatDefault(speed_arg, 1f) * ScenarioUtils.AnimSpeed;
			}
			
			if (Mathf.Abs(anim_speed) > 1000f) {
				anim_speed = 1000f * Mathf.Sign(anim_speed);
			}

			float? time_start = null;
			if (!string.IsNullOrEmpty(time_start_arg)) {
				time_start = ParseFloatDefault(time_start_arg, 1f);
			}

			float anim_length = 0;
			for (int i = 0; i < anims.Count; i++) {
				var anim = anims[i];
				anim.Play(anim_speed, speed_is_time);
				if (time_start != null) anim.time = (float)time_start;
				if (speed_is_time) {
					anim_length = Mathf.Max(anim_length, Mathf.Abs(1f / anim_speed));
				} else {
					anim_length = Mathf.Max(anim_length, anim.length / Mathf.Abs(anim_speed));
				}
			}
			anim_length *= Mathf.Sign(anim_speed);

			float sound_volume = ParseFloatDefault(sound_volume_arg, 1f);
			if (align_sound) {
				ScenarioUtils.PlaySound(anim_name, sound_volume, anim_length, true);
			} else {
				ScenarioUtils.PlaySound(anim_name, sound_volume);
			}
			// If there's no animation to synchronize the sound to,
			// then sound wouldn't play. That's intentional (signifies a scenario bug).
		}

		void ResetController() {
			if (!reset_position && !reset_rotation && !reset_scale) return;
			for (int i = 0; i < anims.Count; i++) {
				var anim = anims[i];
				var tfm = anim.transform;
				var parent = tfm.parent;
				if (parent == null) continue;
				if (reset_position) {
					parent.position = tfm.position;
					tfm.localPosition = Vector3.zero;
				}
				if (reset_rotation) {
					parent.rotation = tfm.rotation;
					tfm.localRotation = Quaternion.identity;
				}
				if (reset_scale) {
					parent.localScale = tfm.localScale;
					tfm.localScale = Vector3.one;
				}
			}
		}

		override public void Update() {
			if (is_finished) return;

			if (!all_anims_finished) {
				bool is_playing = false;
				for (int i = 0; i < anims.Count; i++) {
					var anim = anims[i];
					is_playing |= anim.isPlaying;
				}
				if (!is_playing) {
					all_anims_finished = true;
					// can be random each time
					interloop_time = Time.time + ParseFloatDefault(pause_arg, 0f);
				}
			}

			if (all_anims_finished && (Time.time >= interloop_time)) {
				ResetController();
				if (loops > 0) loops--;
				if (loops == 0) {
					is_finished = true;
				} else {
					PlayWithSpeed();
					all_anims_finished = false;
				}
			}
		}
	}

	[Stage("sound")]
	public class Stage_Sound : Stage {
		AudioSource audio_source;
		float sync_time = -1f;
		float prev_time = 0f;
		float loop_time = 0f;
		int loop_counter = 0;
		public static Stage Create(Stage stage, ScenarioEntry source) {
			return (new Stage_Sound()).Setup(stage, source);
		}
		override public void Init(string cmd_name, string[] args) {
			var audio_clip = MainObject<AudioClip>();
			string audio_path = ArgAt(args, 0);

			float volume = FindFloatArg(args, "V", 1f);

			string speed_arg = FindArg(args, "S", "");
			string time_arg = FindArg(args, "T", "");
			string time_start_arg = FindArg(args, "From", "");

			string sync_arg = FindArg(args, "sync", "");
			sync_time = ParseFloatDefault(sync_arg, -1);

			bool loop = ArgsContain(args, "loop", 1);

			bool speed_is_time = false;
			float speed = 1f;
			
			if (!string.IsNullOrEmpty(time_arg)) {
				speed = 1f / ParseFloatDefault(time_arg, 1f);
				speed_is_time = true;
			} else {
				speed = ParseFloatDefault(speed_arg, 1f);
				speed *= ScenarioUtils.AnimSpeed;
			}

			float? time_start = null;
			if (!string.IsNullOrEmpty(time_start_arg)) {
				time_start = ParseFloatDefault(time_start_arg, 1f);
			}

			if (audio_clip == null) {
				audio_source = ScenarioUtils.PlaySound(audio_path, volume, speed, speed_is_time, loop, time_start);
			} else {
				audio_source = ScenarioUtils.PlaySound(audio_clip, volume, speed, speed_is_time, loop, time_start);
			}

			is_finished = (sync_time < 0) || (audio_source == null);
		}
		override public void Update() {
			if (is_finished) return;

			if (audio_source == null) is_finished = true;
			if (!audio_source.isPlaying) is_finished = true;

			float norm_time = audio_source.time / audio_source.clip.length;
			if (!audio_source.loop && (norm_time == 1)) is_finished = true;

			if (norm_time < prev_time) loop_counter++;
			prev_time = norm_time;
			loop_time = loop_counter + norm_time;

			scenario_thread.root_stage.SetFloatVar("[SoundSyncTime]", loop_time * audio_source.clip.length);

			if (loop_time > sync_time) {
				is_finished = true;
			}
		}
	}

	[Stage("soundfade")]
	public class Stage_SoundFade : Stage {
		public static Stage Create(Stage stage, ScenarioEntry source) {
			return (new Stage_SoundFade()).Setup(stage, source);
		}
		override public void Init(string cmd_name, string[] args) {
			var cam = Camera.main;
			if (cam == null) return;

			string name = ArgAt(args, 0);
			var sound_tfm = cam.transform.Find(name);
			if (sound_tfm == null) sound_tfm = cam.transform.Find("Audio:" + name);

			SoundFader.SetFade(sound_tfm, FloatArgAt(args, 1));

			is_finished = true;
		}
	}

	[Stage("destroy")]
	public class Stage_Destroy : Stage {
		public static Stage Create(Stage stage, ScenarioEntry source) {
			return (new Stage_Destroy()).Setup(stage, source);
		}
		override public void Init(string cmd_name, string[] args) {
			string child_path = ArgAt(args, 0);

			if (string.IsNullOrEmpty(child_path)) {
				foreach (var obj in GatherObjects<Object>(true)) {
					if (obj != null) Object.Destroy(obj);
				}
			} else {
				foreach (var obj in GatherObjects<GameObject>(true)) {
					var child = obj.transform.Find(child_path);
					if (child != null) GameObject.Destroy(child.gameObject);
				}
			}
			
			is_finished = true;
		}
	}

	[Stage("parent", true)]
	public class Stage_Parent : Stage {
		public static Stage Create(Stage stage, ScenarioEntry source) {
			return (new Stage_Parent()).Setup(stage, source);
		}
		override public void Init(string cmd_name, string[] args) {
			var parent_obj = MainObject<GameObject>();

			Transform parent = null;
			if (parent_obj != null) parent = parent_obj.transform;

			foreach (var obj in GatherObjects<GameObject>(false)) {
				obj.transform.parent = parent;
			}

			is_finished = true;
		}
	}

	[Stage("getparent", true)]
	public class Stage_GetParent : Stage {
		public static Stage Create(Stage stage, ScenarioEntry source) {
			return (new Stage_GetParent()).Setup(stage, source);
		}
		override public void Init(string cmd_name, string[] args) {
			var gameobj = MainObject<GameObject>();
			string varname = ArgAt(args, 0);
			bool is_globalvar = (IntArgAt(args, 1, 1) != 0);

			var tfm = (gameobj != null ? gameobj.transform : null);
			var parent = (tfm != null ? tfm.parent : null);
			var parentobj = (parent != null ? parent.gameObject : null);

			if (is_globalvar) {
				ScenarioPlayer.SetObjVar(varname, parentobj, true);
			} else {
				parent_stage.SetObjVar(varname, parentobj, true);
			}

			is_finished = true;
		}
	}
	
	[Stage("getscenario", true)]
	public class Stage_GetScenario : Stage {
		public static Stage Create(Stage stage, ScenarioEntry source) {
			return (new Stage_GetScenario()).Setup(stage, source);
		}
		override public void Init(string cmd_name, string[] args) {
			string varname = ArgAt(args, 0);
			bool is_globalvar = (IntArgAt(args, 1, 1) != 0);

			var scenario_obj = scenario_player.gameObject;

			if (is_globalvar) {
				ScenarioPlayer.SetObjVar(varname, scenario_obj, true);
			} else {
				parent_stage.SetObjVar(varname, scenario_obj, true);
			}

			is_finished = true;
		}
	}

	[Stage("clone", true)]
	public class Stage_Clone : Stage {
		public static Stage Create(Stage stage, ScenarioEntry source) {
			return (new Stage_Clone()).Setup(stage, source);
		}
		override public void Init(string cmd_name, string[] args) {
			var reset_modes = ArgAt(args, 0, "");
			bool reset_pos = reset_modes.Contains("P");
			bool reset_rot = reset_modes.Contains("R");
			bool reset_scl = reset_modes.Contains("S");
			bool reset_name = reset_modes.Contains("N");
			bool reset_tag = reset_modes.Contains("T");
			bool reset_layer = reset_modes.Contains("L");
			bool copy_anims = reset_modes.Contains("A");
			bool children = ArgsContain(args, "children");

			var dst_objs = new List<GameObject>();
			var dst_vars = new List<string>();
			for (int i = 0; i < source.subentries.Count; i++) {
				var subentry = source.subentries[i];
				var dst_obj = subentry.obj_data;
				string key = subentry.val_data.Trim();
				if (!key.Contains(":=")) dst_obj = SubstituteObj(subentry);
				int n = dst_objs.Count;
				foreach (var element in IterateGroup<GameObject>(dst_obj)) {
					dst_objs.Add(element);
				}
				n = dst_objs.Count - n;
				for (int j = 0; j < n; j++) dst_vars.Add(key);
			}

			var src_obj = MainObject<GameObject>();
			for (int i = 0; i < dst_objs.Count; i++) {
				var dst_obj = dst_objs[i];
				var dst_var = dst_vars[i];

				var clone = GameObject.Instantiate(src_obj) as GameObject;
				if (reset_name) {
					clone.name = dst_obj.name;
				} else {
					// Instantiate() adds "(Clone)" to object name
					clone.name = src_obj.name;
				}
				if (reset_tag) clone.tag = dst_obj.tag;
				if (reset_layer) clone.layer = dst_obj.layer;

				if (copy_anims) {
					var clone_anim = clone.GetComponent<Animation>();
					var dst_anim = dst_obj.GetComponent<Animation>();
					if (dst_anim != null) {
						if (clone_anim == null) {
							clone_anim = clone.AddComponent<Animation>();
							clone_anim.animatePhysics = dst_anim.animatePhysics;
							clone_anim.cullingType = dst_anim.cullingType;
							clone_anim.playAutomatically = dst_anim.playAutomatically;
							clone_anim.wrapMode = dst_anim.wrapMode;
						}
						foreach (AnimationState animstate in dst_anim) {
							clone_anim.AddClip(dst_anim[animstate.name].clip, animstate.name);
						}
					}
					var clone_animator = clone.GetComponent<Animator>();
					var dst_animator = dst_obj.GetComponent<Animator>();
					if (dst_animator != null) {
						if (clone_animator == null) {
							clone_animator = clone.AddComponent<Animator>();
							clone_animator.updateMode = dst_animator.updateMode;
							clone_animator.applyRootMotion = dst_animator.applyRootMotion;
							clone_animator.avatar = dst_animator.avatar;
							clone_animator.bodyPosition = dst_animator.bodyPosition;
							clone_animator.bodyRotation = dst_animator.bodyRotation;
							clone_animator.cullingMode = dst_animator.cullingMode;
							clone_animator.fireEvents = dst_animator.fireEvents;
							clone_animator.rootPosition = dst_animator.rootPosition;
							clone_animator.rootRotation = dst_animator.rootRotation;
							clone_animator.speed = dst_animator.speed;
						}
					}
				}

				var clone_tfm = clone.transform;
				var dst_tfm = dst_obj.transform;

				if (children) {
					clone_tfm.parent = dst_tfm;
					if (reset_pos) clone_tfm.localPosition = Vector3.zero;
					if (reset_rot) clone_tfm.localRotation = Quaternion.identity;
					if (reset_scl) clone_tfm.localScale = Vector3.one;
				} else {
					clone_tfm.parent = dst_tfm.parent;
					if (reset_pos) clone_tfm.localPosition = dst_tfm.localPosition;
					if (reset_rot) clone_tfm.localRotation = dst_tfm.localRotation;
					if (reset_scl) clone_tfm.localScale = dst_tfm.localScale;
					GameObject.Destroy(dst_obj);
				}

				if (dst_var.Contains(":=")) {
					AssignVariable(dst_var, clone);
				}
			}

			is_finished = true;
		}
	}

	[Stage("filter")]
	public class Stage_Filter : Stage {
		public static Stage Create(Stage stage, ScenarioEntry source) {
			return (new Stage_Filter()).Setup(stage, source);
		}
		override public void Init(string cmd_name, string[] args) {
			string name = null;
			string tag = null;
			LayerMask? layers = null;
			bool? children = null;
			bool? inverse = null;
			bool? regex = null;
			bool? path = null;

			for (int i = 0; i < args.Length; i++) {
				var ws = args[i];
				if (ws.StartsWith("name=")) {
					name = ws.Substring("name=".Length);
				} else if (ws.StartsWith("tag=")) {
					tag = ws.Substring("tag=".Length);
				} else if (ws.StartsWith("layers=")) {
					string ws2 = ws.Substring("layers=".Length);
					string[] toks = ws2.Split('+');
					for (int j = 0; j < toks.Length; j++) {
						layers |= LayerMask.NameToLayer(toks[j]);
					}
				} else if (ws.StartsWith("children=")) {
					string ws2 = ws.Substring("children=".Length);
					children = (ParseIntDefault(ws2, 0) != 0);
				} else if (ws.StartsWith("inverse=")) {
					string ws2 = ws.Substring("inverse=".Length);
					inverse = (ParseIntDefault(ws2, 0) != 0);
				} else if (ws.StartsWith("regex=")) {
					string ws2 = ws.Substring("regex=".Length);
					regex = (ParseIntDefault(ws2, 0) != 0);
				} else if (ws.StartsWith("path=")) {
					string ws2 = ws.Substring("path=".Length);
					path = (ParseIntDefault(ws2, 0) != 0);
				}
			}

			foreach (var obj_group in GatherObjects<GameObjectGroup>(true, true, false)) {
				if (name != null) obj_group.FilterName = name;
				if (tag != null) obj_group.FilterTag = tag;
				if (layers != null) obj_group.FilterLayers = (LayerMask)layers;
				if (children != null) obj_group.FilterChildren = (bool)children;
				if (inverse != null) obj_group.FilterInverse = (bool)inverse;
				if (regex != null) obj_group.FilterRegex = (bool)regex;
				if (path != null) obj_group.FilterPath = (bool)path;
			}

			is_finished = true;
		}
	}

	[Stage("throw_error")]
	public class Stage_Throw_Error : Stage {
		public static Stage Create(Stage stage, ScenarioEntry source) {
			return (new Stage_Throw_Error()).Setup(stage, source);
		}
		override public void Init(string cmd_name, string[] args) {
			throw new System.Exception(ArgAt(args, 0, "throw_error"));
		}
	}

	[Stage("animdev")]
	public class Stage_AnimDev : Stage
	{
		Stage_Anim anim = null;
		private static int _devIs4_3 = -1;
		public static int devIs4_3
		{
			get
			{
				if (_devIs4_3 >= 0) return _devIs4_3;
				float aspect = (float)Screen.width / (float)Screen.height;
				if (Screen.height > Screen.width)
					_devIs4_3 = aspect > 0.74f ? 1 : 0;				
				else
					_devIs4_3 = aspect < 1.48f ? 1 : 0;
				return _devIs4_3;
			}
		}
		
		public static Stage Create(Stage stage, ScenarioEntry source) {
			return (new Stage_AnimDev()).Setup(stage, source);
		}
		override public void Init(string cmd_name, string[] args) 
		{
			if(args == null) return;

			if (devIs4_3 == 1)
				if(args.Length>0)	args[0] += "_ipad";
			// Call [anim] command with right clip name
			anim = (Stage_Anim)Stage_Anim.Create(parent_stage, source);
			anim.Init("anim", args);
		}

		override public void OnGUI() 
		{
		}
		
		override public void Update() 
		{
			anim.Update();
			is_finished = anim.is_finished;
		}
	}	
	
	[Stage("components_add")]
	public class Stage_Components_Add : Stage {
		public static Stage Create(Stage stage, ScenarioEntry source) {
			return (new Stage_Components_Add()).Setup(stage, source);
		}
		override public void Init(string cmd_name, string[] args) {
			foreach (var obj in GatherObjects<GameObject>(true)) {
				for (int i = 0; i < args.Length; i++) {
					var name = args[i];
					var component = obj.GetComponent(name);
					if (component != null) continue;
					// name has to be in the Assembly Qualified Name format
					// https://msdn.microsoft.com/en-us/library/system.type.assemblyqualifiedname(v=vs.110).aspx
					var component_type = System.Type.GetType(name, false, false);
					if (component_type != null) obj.AddComponent(component_type);
				}
			}
			is_finished = true;
		}
	}
	
	[Stage("components_remove")]
	public class Stage_Components_Remove : Stage {
		public static Stage Create(Stage stage, ScenarioEntry source) {
			return (new Stage_Components_Remove()).Setup(stage, source);
		}
		override public void Init(string cmd_name, string[] args) {
			foreach (var obj in GatherObjects<GameObject>(true)) {
				for (int i = 0; i < args.Length; i++) {
					var name = args[i];
					var component = obj.GetComponent(name);
					if (component == null) continue;
					Object.Destroy(component);
				}
			}
			is_finished = true;
		}
	}
	
	[Stage("components_enable")]
	[Stage("components_disable")]
	public class Stage_Components_Enable : Stage {
		public static Stage Create(Stage stage, ScenarioEntry source) {
			return (new Stage_Components_Enable()).Setup(stage, source);
		}
		override public void Init(string cmd_name, string[] args) {
			bool enabled = cmd_name.EndsWith("enable");
			foreach (var obj in GatherObjects<GameObject>(true)) {
				for (int i = 0; i < args.Length; i++) {
					var name = args[i];
					var component = obj.GetComponent(name);
					if (component == null) continue;
					ObjectUtils.SetComponentEnabled(component, enabled);
					// Unity seems to not support the dynamic type
					//if (!TrySettingPropertyValue(component, "enabled", enabled.ToString())) {
					//	DebugGUI.LogTag = scenario_thread.tag;
					//	if (Debug.isDebugBuild) Debug.LogError("Component '"+name+"' has no 'enabled' property");
					//}
				}
			}
			is_finished = true;
		}
		//static bool TrySettingPropertyValue(Object target, string property, string value) {
		//	if (target == null) return false;
		//	try {
		//		var prop = target.GetType().GetProperty(property);
		//		if (prop == null) return false;
		//		if (value == null) prop.SetValue(target, null, null);
		//		var convertedValue = System.Convert.ChangeType(value, prop.PropertyType);
		//		prop.SetValue(target, convertedValue, null);
		//		return true;
		//	} catch {
		//		return false;
		//	}
		//}
	}

	[Stage("touch", true)]
	public class Stage_Touch : Stage {
		public static Stage Create(Stage stage, ScenarioEntry source) {
			return (new Stage_Touch()).Setup(stage, source);
		}
		override public void Init(string cmd_name, string[] args) {
			var cam = Camera.main;
			string cam_name = FindArg(args, "cam", null);
			if (!string.IsNullOrEmpty(cam_name)) {
				var cam_obj = ScenarioPlayer.GetObjVar(cam_name, null, true) as GameObject;
				cam_obj = GetObjVar(cam_name, cam_obj, true) as GameObject;
				if (cam_obj != null) {
					var explicit_cam = cam_obj.GetComponent<Camera>();
					if (explicit_cam != null) cam = explicit_cam;
				}
			}
			
			var ray = cam.ScreenPointToRay(InputMT.mousePosition);
			var distance = cam.farClipPlane - cam.nearClipPlane;

			var var_names = new HashSet<string>();

			string var_name0 = FindArg(args, "var", "touch");
			bool recursive0 = ArgsContain(args, "all");
			float alpha0 = FindFloatArg(args, "alpha", -1f);

			ScenarioPlayer.SetBoolVar(var_name0, false, true);
			var_names.Add(var_name0);

			DetectTouch_Entry(source, alpha0, ray, distance, var_name0, recursive0);

			for (int i = 0; i < source.subentries.Count; i++) {
				var subentry = source.subentries[i];
				//var dst_obj = subentry.obj_data; // not used
				var val_data = subentry.val_data;

				args = val_data.Split(Stage_Nested.delimiters, System.StringSplitOptions.RemoveEmptyEntries);
				string var_name = FindArg(args, "var", var_name0);
				bool recursive = ArgsContain(args, "all");
				float alpha = FindFloatArg(args, "alpha", -1f);

				if (!var_names.Contains(var_name)) {
					ScenarioPlayer.SetBoolVar(var_name, false, true);
					var_names.Add(var_name);
				}

				DetectTouch_Entry(subentry, alpha, ray, distance, var_name, recursive);
			}

			is_finished = true;
		}
		void DetectTouch_Entry(ScenarioEntry entry, float alpha, Ray ray, float distance, string var_name, bool recursive) {
			if (!InputMT.GetMouseButtonDown(0)) return; // or mouse emulator?
			var obj = SubstituteObj(entry) as GameObject;
			if (obj == null) {
				ScenarioPlayer.SetBoolVar(var_name, true, true);
			} else {
				foreach (var element in IterateGroup<GameObject>(obj)) {
					DetectTouch(element, alpha, ray, distance, var_name, recursive);
				}
			}
		}

		public static void DetectTouch(GameObject obj, float alpha, string var_name, bool recursive) {
			if (obj == null) return;
			if (DetectTouch(obj.transform, alpha, recursive)) {
				ScenarioPlayer.SetBoolVar(var_name, true, true);
			}
		}
		public static bool DetectTouch(Transform tfm, float alpha, bool recursive) {
			//var renderer = tfm.renderer;
			//if ((renderer == null) || !renderer.enabled) return false;
			if (ObjectUtils.DetectSpriteCollision(tfm, alpha)) return true;
			if (recursive) {
				for (int i = 0; i < tfm.childCount; i++) {
					if (DetectTouch(tfm.GetChild(i), alpha, recursive)) return true;
				}
			}
			return false;
		}

		public static void DetectTouch(GameObject obj, float alpha, Ray ray, float distance, string var_name, bool recursive) {
			if (obj == null) return;
			if (DetectTouch(obj.transform, alpha, ray, distance, recursive)) {
				ScenarioPlayer.SetBoolVar(var_name, true, true);
			}
		}
		static Bounds dummy_bounds = new Bounds();
		public static bool DetectTouch(Transform tfm, float alpha, Ray ray, float distance, bool recursive) {
			//var renderer = tfm.renderer;
			//if ((renderer == null) || !renderer.enabled) return false;
			if (ObjectUtils.DetectSpriteCollision(tfm, alpha, ray, distance, dummy_bounds, true)) return true;
			if (recursive) {
				for (int i = 0; i < tfm.childCount; i++) {
					if (DetectTouch(tfm.GetChild(i), alpha, ray, distance, recursive)) return true;
				}
			}
			return false;
		}
	}
}
