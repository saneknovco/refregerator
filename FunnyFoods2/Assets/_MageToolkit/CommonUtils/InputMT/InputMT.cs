﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using Bini.Utils;

namespace UnityEngine {
	/// <summary>
	/// Multi-touch -> mouse emulator
	/// </summary>
	[MonoBehaviourSingleton(true, true, true)]
	public class InputMT : MonoBehaviourSingleton<InputMT> {
		void Start() {
			StartCoroutine(EndFrameCoroutine());

			trackingMode = TrackingMode.Last;

			switchingCondition = SwitchingCondition.OnCollider;
		}

		void FixedUpdate() {
			if (this == Instance) Refresh();
		}

		void Update() {
			if (this == Instance) Refresh();
		}

		IEnumerator EndFrameCoroutine() {
			while (true) {
				yield return new WaitForEndOfFrame();
				frame_count++;
			}
		}

		static int frame_count = 0;

		public enum TrackingMode {
			Unity, Any, First, Last
		}
		public static TrackingMode trackingMode {get; set;}

		public enum SwitchingCondition {
			Always, NotWhileDrag, OnCollider
		}
		public static SwitchingCondition switchingCondition {get; set;}
		
		public enum RaycastPreference {
			Closest, Collider2D, Collider3D
		}
		public static RaycastPreference raycastPreference {get; set;}

		static int last_frame = -1;
		
		static bool tracking = false;
		static int tracking_id = -1;

		static GameObject prev_hit_obj = null;
		static GameObject drag_obj = null;

		static void Refresh() {
			int frame = frame_count; // Time.frameCount; // Time.renderedFrameCount
			if (frame == last_frame) return;
			last_frame = frame;

			//trackingMode = TrackingMode.Last;

			bool use_default = !(Input.touchSupported && Input.multiTouchEnabled);
			use_default |= (trackingMode == TrackingMode.Unity);

			//use_default &= (trackingMode == TrackingMode.Unity);

			bool reset_tracking = false;
			
			#region Mouse position & button presses
			if (use_default) {
				_mousePosition = Input.mousePosition;
				_mouseButton0.Read(0);
				_mouseButton1.Read(1);
				_mouseButton2.Read(2);
				tracking = false;
				tracking_id = -1;
			} else {
				if (Input.touchCount == 0) {
					tracking = false;
					tracking_id = -1;
				} else if (!tracking) {
					bool found_old = false, found_new = false;
					Touch touch_old = default(Touch), touch_new = default(Touch);
					
					for (int i = 0; i < Input.touchCount; i++) {
						var touch = Input.GetTouch(i);
						if (touch.phase == TouchPhase.Began) {
							found_new = true;
							touch_new = touch;
						} else if ((touch.phase == TouchPhase.Moved)||(touch.phase == TouchPhase.Stationary)) {
							if (!found_old) {
								found_old = true;
								touch_old = touch;
							}
						}
					}
					
					if (trackingMode == TrackingMode.Any) {
						tracking = found_old || found_new;
						if (tracking) tracking_id = (found_old ? touch_old : touch_new).fingerId;
					} else {
						tracking = found_new;
						if (tracking) tracking_id = touch_new.fingerId;
					}
				} else if (trackingMode == TrackingMode.Last) {
					if ((switchingCondition != SwitchingCondition.NotWhileDrag) || (!drag_obj)) {
						for (int i = 0; i < Input.touchCount; i++) {
							var touch = Input.GetTouch(i);
							if (touch.phase == TouchPhase.Began) {
								if ((switchingCondition != SwitchingCondition.OnCollider) || Raycast(touch.position)) {
									tracking = true;
									tracking_id = touch.fingerId;
									// "unpress" mouse for 1 frame
									// (So that at current frame MouseUp is generated with previous touch coords,
									// and on next frame MouseDown is generated with current touch coords)
									reset_tracking = true;
								}
							}
						}
					}
				}
				
				if (tracking & !reset_tracking) {
					for (int i = 0; i < Input.touchCount; i++) {
						var touch = Input.GetTouch(i);
						if (touch.fingerId == tracking_id) {
							_mousePosition = new Vector3(touch.position.x, touch.position.y, 0f);
							if ((touch.phase == TouchPhase.Canceled) || (touch.phase == TouchPhase.Ended)) {
								tracking = false;
								tracking_id = -1;
								break;
							}
						}
					}

					_mouseButton0.Press();
				} else {
					_mouseButton0.Unpress();
				}
				
				_mouseButton1.Unpress();
				_mouseButton2.Unpress();
			}

			_prev_count_held_buttons = _count_held_buttons;
			_count_held_buttons = CountHeldButtons();
			#endregion

			#region Mouse events
			// Note: Input.simulateMouseWithTouches doesn't affect OnMouse* events!

			var hit_obj = Raycast(mousePosition);

			if (prev_hit_obj != hit_obj) {
				TrySendMessage(prev_hit_obj, "OnMouseExitMT");
				TrySendMessage(hit_obj, "OnMouseEnterMT");
			}

			if (hit_obj) {
				TrySendMessage(hit_obj, "OnMouseOverMT");
				if (_GetMouseButtonDown()) {
					drag_obj = hit_obj;
					TrySendMessage(drag_obj, "OnMouseDownMT");
				}
			} else {
				hit_obj = null; // just in case
			}

			if (drag_obj) {
				if (_GetMouseButtonUp() || !_GetMouseButton()) {
					// For Bini's PlayMaker buttons, this order of events is necessary
					// (if MouseUp transition is activated first, the FSM will return
					// to the waiting state instead of the button reaction state)
					if ((drag_obj == hit_obj) & !reset_tracking) {
						// If finger switching occured (reset_tracking), we don't
						// want to interpret old finger's MouseUp as a button click
						TrySendMessage(drag_obj, "OnMouseUpAsButtonMT");
					}
					TrySendMessage(drag_obj, "OnMouseUpMT");
					drag_obj = null;
				} else {
					TrySendMessage(drag_obj, "OnMouseDragMT");
				}
			} else {
				drag_obj = null; // just in case
			}

			prev_hit_obj = hit_obj;
			#endregion
		}

		static GameObject Raycast(Vector3 screen_pos) {
			foreach (var cam in IterCameras(screen_pos)) {
				var ray = cam.ScreenPointToRay(screen_pos);

				GameObject hit_obj = null;

				var hit = default(RaycastHit);
				if (Physics.Raycast(ray, out hit)) {
					hit_obj = hit.transform.gameObject;
				}

				var hit2d = Physics2D.GetRayIntersection(ray);
				if (hit2d.transform) {
					if (hit_obj == null) {
						hit_obj = hit2d.transform.gameObject;
					} else {
						if (raycastPreference == RaycastPreference.Closest) {
							if (hit2d.distance < hit.distance) {
								hit_obj = hit2d.transform.gameObject;
							}
						} else if (raycastPreference == RaycastPreference.Collider2D) {
							hit_obj = hit2d.transform.gameObject;
						}
					}
				}

				if (hit_obj) return hit_obj;
			}

			return null;
		}

		static IEnumerable<Camera> IterCameras(Vector3 pos) {
			List<Camera> res_cameras = null;

			var cameras = Camera.allCameras;
			for (int i = 0; i < cameras.Length; i++) {
				var cam = cameras[i];

#if UNITY_5
				if (cam.cameraType != CameraType.Game) continue;
#endif
				if (!cam.isActiveAndEnabled) continue;
				if (cam.targetTexture != null) continue;
				if (!cam.pixelRect.Contains(pos)) continue;

				if (res_cameras == null) res_cameras = new List<Camera>();
				res_cameras.Add(cam);
			}

			if (res_cameras == null) yield break;

			res_cameras.Sort((cam0, cam1) => (-cam0.depth.CompareTo(cam1.depth)));

			for (int i = 0; i < res_cameras.Count; i++) {
				var cam = res_cameras[i];
				yield return cam;

				switch (cam.clearFlags) {
				case CameraClearFlags.Skybox: yield break;
				case CameraClearFlags.SolidColor: yield break;
				}
			}
		}

		static void TrySendMessage(GameObject obj, string msg) {
			if (!obj) return;
			obj.SendMessage(msg, SendMessageOptions.DontRequireReceiver);
#if !MAGE_NO_PM
			//support multiple component
			var fsms = obj.GetComponents<PlayMakerFSM>();
			if (fsms.Length < 1) return;
			string fsmMsg = @"MouseMT/" + msg.Substring(7, msg.Length - 9); // Extract * from "OnMouse*MT"
			for (int i = 0; i < fsms.Length; i++) {
				if (fsms[i] != null) fsms[i].SendEvent(fsmMsg);
			}
#endif
		}

		struct ButtonState {
			public bool held, down, up;
			public void Read(int button) {
				held = Input.GetMouseButton(button);
				down = Input.GetMouseButtonDown(button);
				up = Input.GetMouseButtonUp(button);
			}
			public void Press() {
				up = false;
				down = !held;
				held = true;
			}
			public void Unpress() {
				up = held;
				down = false;
				held = false;
			}
		}
		
		static Vector3 _mousePosition;
		static ButtonState _mouseButton0;
		static ButtonState _mouseButton1;
		static ButtonState _mouseButton2;
		
		static int _count_held_buttons = 0;
		static int _prev_count_held_buttons = 0;
		static int CountHeldButtons() {
			int n = 0;
			if (_mouseButton0.held) n++;
			if (_mouseButton1.held) n++;
			if (_mouseButton2.held) n++;
			return n;
		}
		
		// ============================================ //
		public static Vector3 mousePosition {
			get {
				Instance.Update();
				return _mousePosition;
			}
		}
		
		public static bool GetMouseButton(int button) {
			Instance.Update();
			switch (button) {
			case 0: return _mouseButton0.held;
			case 1: return _mouseButton1.held;
			case 2: return _mouseButton2.held;
			}
			return false;
		}
		// Returns whether ANY button is held
		public static bool _GetMouseButton() {
			return _mouseButton0.held|_mouseButton1.held|_mouseButton2.held;
		}
		public static bool GetMouseButton() { // not in the original API
			Instance.Update();
			return _GetMouseButton();
		}
		
		public static bool GetMouseButtonDown(int button) {
			Instance.Update();
			switch (button) {
			case 0: return _mouseButton0.down;
			case 1: return _mouseButton1.down;
			case 2: return _mouseButton2.down;
			}
			return false;
		}
		// Returns whether FIRST button is pressed
		public static bool _GetMouseButtonDown() {
			if (_prev_count_held_buttons != 0) return false;
			return _mouseButton0.down|_mouseButton1.down|_mouseButton2.down;
		}
		public static bool GetMouseButtonDown() { // not in the original API
			Instance.Update();
			return _GetMouseButtonDown();
		}
		
		public static bool GetMouseButtonUp(int button) {
			Instance.Update();
			switch (button) {
			case 0: return _mouseButton0.up;
			case 1: return _mouseButton1.up;
			case 2: return _mouseButton2.up;
			}
			return false;
		}
		// Returns whether LAST button is released
		public static bool _GetMouseButtonUp() {
			if (CountHeldButtons() != 0) return false;
			return _mouseButton0.up|_mouseButton1.up|_mouseButton2.up;
		}
		public static bool GetMouseButtonUp() { // not in the original API
			Instance.Update();
			return _GetMouseButtonUp();
		}
	}
}
