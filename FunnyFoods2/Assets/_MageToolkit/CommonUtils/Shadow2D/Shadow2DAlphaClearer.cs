﻿using UnityEngine;
using System.Collections.Generic;
using Bini.Utils;

public class Shadow2DAlphaClearer : MonoBehaviour {
	public Material AlphaClearMaterial {get; set;}

	public Color ClearColor = Color.clear;

	void OnPostRender() {
		var cam = GetComponent<Camera>();
		var pz = (cam.nearClipPlane + cam.farClipPlane)*0.5f;

		AlphaClearMaterial.color = ClearColor;
		AlphaClearMaterial.SetPass(0);

		GL.Begin(GL.QUADS);
		GL.Vertex(cam.ViewportToWorldPoint(new Vector3(0, 0, pz)));
		GL.Vertex(cam.ViewportToWorldPoint(new Vector3(0, 1, pz)));
		GL.Vertex(cam.ViewportToWorldPoint(new Vector3(1, 1, pz)));
		GL.Vertex(cam.ViewportToWorldPoint(new Vector3(1, 0, pz)));
		GL.End();

//		GL.PushMatrix();
//		GL.LoadOrtho();
//		GL.Begin(GL.QUADS);
//		GL.TexCoord2(0, 0); GL.Vertex3(0, 0, 0);
//		GL.TexCoord2(0, 1); GL.Vertex3(0, 1, 0);
//		GL.TexCoord2(1, 1); GL.Vertex3(1, 1, 0);
//		GL.TexCoord2(1, 0); GL.Vertex3(1, 0, 0);
//		GL.End();
//		GL.PopMatrix();
	}
}
