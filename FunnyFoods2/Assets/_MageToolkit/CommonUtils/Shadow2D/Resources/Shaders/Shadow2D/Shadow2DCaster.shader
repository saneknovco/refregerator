﻿Shader "Shadow2D/Caster" {
	Properties {
		_Color ("Color Tint", Color) = (1,1,1,1)
		_MainTex ("Base (RGB) Alpha (A)", 2D) = "white" {}
		_AlphaMap ("Alpha (A)", 2D) = "white" {}
		//_MainTex_Alpha ("Alpha (A)", 2D) = "white" {}
	}
	SubShader {
		Tags { "Queue"="Transparent" "IgnoreProjector"="True" "RenderType"="Transparent" }
		Lighting Off
		//Cull Off
		ZTest Always
		ZWrite Off
		Fog { Mode Off }
		
		Pass {
			BlendOp Max
			Blend Zero One, One One
			
			CGPROGRAM
			#pragma vertex vert
			#pragma fragment frag
			
			#include "UnityCG.cginc"
			
			#define _MainTex_Alpha _AlphaMap
			
			fixed4 _Color;
			sampler2D _MainTex;
			uniform float4 _MainTex_ST;
			sampler2D _MainTex_Alpha;
			
			struct appdata {
				float4 vertex : POSITION;
				float2 uv : TEXCOORD0;
			};
			
			struct v2f {
				float4 pos : SV_POSITION;
				float2 uv : TEXCOORD0;
				float factor : TEXCOORD1;
			};
			
			v2f vert(appdata v) {
				v2f o;
				o.pos = mul(UNITY_MATRIX_MVP, v.vertex);
				o.uv = TRANSFORM_TEX(v.uv, _MainTex);
				o.factor = (1 - saturate(o.pos.z)) * _Color.a;
				return o;
			}
			
			fixed4 frag(v2f i) : COLOR0 {
				return min(tex2D(_MainTex, i.uv), tex2D(_MainTex_Alpha, i.uv)) * i.factor;
			}
			ENDCG
		}
	}
	SubShader {
		Tags { "Queue"="Transparent" "IgnoreProjector"="True" "RenderType"="Transparent" }
		Lighting Off
		//Cull Off
		ZTest Always
		ZWrite Off
		Fog { Mode Off }
		
		Pass {
			//Some android devices have no GL_EXT_blend_minmax extension
			Blend Zero One, OneMinusDstAlpha One
			
			CGPROGRAM
			#pragma vertex vert
			#pragma fragment frag
			
			#include "UnityCG.cginc"
			
			#define _MainTex_Alpha _AlphaMap
			
			fixed4 _Color;
			sampler2D _MainTex;
			uniform float4 _MainTex_ST;
			sampler2D _MainTex_Alpha;
			
			struct appdata {
				float4 vertex : POSITION;
				float2 uv : TEXCOORD0;
			};
			
			struct v2f {
				float4 pos : SV_POSITION;
				float2 uv : TEXCOORD0;
				float factor : TEXCOORD1;
			};
			
			v2f vert(appdata v) {
				v2f o;
				o.pos = mul(UNITY_MATRIX_MVP, v.vertex);
				o.uv = TRANSFORM_TEX(v.uv, _MainTex);
				o.factor = (1 - saturate(o.pos.z)) * _Color.a;
				return o;
			}
			
			fixed4 frag(v2f i) : COLOR0 {
				return min(tex2D(_MainTex, i.uv), tex2D(_MainTex_Alpha, i.uv)) * i.factor;
			}
			ENDCG
		}
	}
}
