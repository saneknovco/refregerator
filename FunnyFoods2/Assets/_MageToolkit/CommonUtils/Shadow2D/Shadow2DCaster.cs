﻿using UnityEngine;
using System.Collections.Generic;
using Bini.Utils;

public class Shadow2DCaster : MonoBehaviour {
	public Shadow2DPlane ShadowPlane = null;
	public float DistanceToPlane = 0;

	public void SetShadowPlane(Shadow2DPlane shadow_plane) {
		if (ShadowPlane != null) ShadowPlane.RemoveCaster(this);
		ShadowPlane = shadow_plane;
		if (ShadowPlane != null) ShadowPlane.AddCaster(this);
	}

	public bool ShadowPossible(float z_min, float z_max, float z0, float alpha, Shadow2DManager shadow_manager, ref Bounds bounds, ref int vertex_count, Matrix4x4 matrix) {
		float z = transform.position.z;
		bool shadow_possible = ((z > z_min) & (z < z_max)) && CanCast;
		if (shadow_possible & (vertex_count >= 0)) { // vertex_count < 0 --> don't calculate bounds
			shadow_calculated = false;
			ToShadow(z0, alpha, shadow_manager);
			UpdateBounds(ref bounds, ref vertex_count, matrix, transform);
			FromShadow(false);
			shadow_possible = (vertex_count > 0);
		}
		return shadow_possible;
	}

	public bool CanCast {
		get { return (DistanceToPlane > 0) && this.enabled && gameObject.activeInHierarchy; }
	}

	Vector3 save_pos, save_scale; // pos is local
	Vector3 shadow_pos, shadow_scale; // pos is global
	bool shadow_calculated = false;

	public void CalcShadowPosScale(float z0, float alpha, Shadow2DManager shadow_manager) {
		save_pos = transform.localPosition;
		save_scale = transform.localScale;
		
		Vector3 pos = transform.position;
		Vector3 scale = save_scale;
		
		//float shadow_fade = shadow_manager.ShadowFading * DistanceToPlane;
		float shadow_fade = 1f - Mathf.Pow(2f, -DistanceToPlane * shadow_manager.ShadowFading) * alpha;
		shadow_fade = Mathf.Clamp01(Mathf.Max(shadow_fade, 0.001f));
		
		Vector3 lightpos = Vector3.zero;
		float k = 1f;
		if (shadow_manager.LightObject != null) {
			lightpos = shadow_manager.LightObject.position;
			float LightDistanceToPlane = -lightpos.z - z0;
			float DeltaDistance = LightDistanceToPlane - DistanceToPlane;
			k = LightDistanceToPlane / DeltaDistance;
			if (DeltaDistance <= 0f) shadow_fade = -1f; // hide shadow
		}
		
		if (shadow_manager.LightObject != null) {
			if (shadow_fade >= 0) { // or: k is not infinity
				pos.x = lightpos.x + (pos.x - lightpos.x) * k;
				pos.y = lightpos.y + (pos.y - lightpos.y) * k;
			}
		} else {
			pos.x += shadow_manager.ShadowShifting.x * DistanceToPlane;
			pos.y += shadow_manager.ShadowShifting.y * DistanceToPlane;
		}
		pos.z = z0 + shadow_manager.ShadowSpaceDepth * shadow_fade;
		shadow_pos = pos;
		
		if (shadow_manager.LightObject != null) {
			if (shadow_fade >= 0) { // or: k is not infinity
				scale.x = scale.x * k;
				scale.y = scale.y * k;
			}
		}
		scale.z = 0;
		shadow_scale = scale;

		shadow_calculated = true;
	}

	public void ToShadow(float z0, float alpha, Shadow2DManager shadow_manager) {
		if (!shadow_calculated) CalcShadowPosScale(z0, alpha, shadow_manager);
		transform.position = shadow_pos;
		transform.localScale = shadow_scale;
	}
	public void FromShadow(bool reset=false) {
		transform.localPosition = save_pos;
		transform.localScale = save_scale;
		if (reset) shadow_calculated = false;
	}

	void Start() {
		// Do this on Start() so that ShadowPlane may be specified at runtime
		if (ShadowPlane != null) ShadowPlane.AddCaster(this);
	}

	void OnDestroy() {
		if (ShadowPlane != null) ShadowPlane.RemoveCaster(this);
	}

	#region Bounds calculation
	static void UpdateBounds(ref Bounds bounds, ref int count, Matrix4x4 matrix, Transform tfm) {
		var skin = tfm.GetComponent<SkinnedMeshRenderer>();
		if (skin) {
			UpdateBounds(ref bounds, ref count, matrix, skin);
		} else {
			var mesh_filter = tfm.GetComponent<MeshFilter>();
			if (mesh_filter) UpdateBounds(ref bounds, ref count, matrix, mesh_filter);
		}

		for (int i = 0; i < tfm.childCount; i++) {
			var child = tfm.GetChild(i);
			UpdateBounds(ref bounds, ref count, matrix, child);
		}
	}

	static Matrix4x4[] bone_matrices = new Matrix4x4[64];
	static void UpdateBounds(ref Bounds bounds, ref int count, Matrix4x4 matrix, SkinnedMeshRenderer skin) {
		var mesh = skin.sharedMesh;
		if (!mesh) return;
		var vertices = mesh.vertices;
		if (vertices == null) return;
		if (vertices.Length == 0) return;
		var bindposes = mesh.bindposes;
		if (bindposes == null) return;
		var boneWeights = mesh.boneWeights;
		if (boneWeights == null) return;
		var bones = skin.bones;
		if (bones == null) return;
		var tfm = skin.transform;

		if (bones.Length > bone_matrices.Length) bone_matrices = new Matrix4x4[bones.Length];

		for (int i = 0; i < bones.Length; i++) {
			if (bones[i]) {
				bone_matrices[i] = matrix * (bones[i].localToWorldMatrix * bindposes[i]);
			} else {
				bone_matrices[i] = matrix * (tfm.localToWorldMatrix * bindposes[i]);
			}
		}

		for (int i = 0; i < vertices.Length; i++) {
			BoneWeight bw = boneWeights[i];
			Vector3 src_pos = vertices[i];
			Vector3 dst_pos = Vector3.zero;
			dst_pos += bone_matrices[bw.boneIndex0].MultiplyPoint3x4(src_pos) * bw.weight0;
			dst_pos += bone_matrices[bw.boneIndex1].MultiplyPoint3x4(src_pos) * bw.weight1;
			dst_pos += bone_matrices[bw.boneIndex2].MultiplyPoint3x4(src_pos) * bw.weight2;
			dst_pos += bone_matrices[bw.boneIndex3].MultiplyPoint3x4(src_pos) * bw.weight3;

			if (count == 0) {
				bounds = new Bounds(dst_pos, Vector3.zero);
				count = 1;
			} else {
				bounds.Encapsulate(dst_pos);
				count++;
			}
		}
	}

	static void UpdateBounds(ref Bounds bounds, ref int count, Matrix4x4 matrix, MeshFilter mesh_filter) {
		var mesh = mesh_filter.sharedMesh;
		if (!mesh) return;
		var vertices = mesh.vertices;
		if (vertices == null) return;
		if (vertices.Length == 0) return;
		var tfm = mesh_filter.transform;

		matrix = matrix * tfm.localToWorldMatrix;

		for (int i = 0; i < vertices.Length; i++) {
			var dst_pos = matrix.MultiplyPoint(vertices[i]);

			if (count == 0) {
				bounds = new Bounds(dst_pos, Vector3.zero);
				count = 1;
			} else {
				bounds.Encapsulate(dst_pos);
				count++;
			}
		}
	}
	#endregion
}
