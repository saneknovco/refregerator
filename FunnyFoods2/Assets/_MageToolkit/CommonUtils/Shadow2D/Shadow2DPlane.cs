﻿using UnityEngine;
using System.Collections.Generic;
using Bini.Utils;

public class Shadow2DPlane : MonoBehaviour {
	[HideInInspector]
	public HashSet<Shadow2DCaster> Casters = new HashSet<Shadow2DCaster>();
	public void AddCaster(Shadow2DCaster caster) {
		Casters.Add(caster);
	}
	public void RemoveCaster(Shadow2DCaster caster) {
		Casters.Remove(caster);
	}

	public Camera cam {get; private set;}
	Texture2D tex_2d = null;
	RenderTexture tex_rt = null;
	public bool use_rt = false; // public -- for testing

	public Color ShadowColor = new Color(0, 0, 0, 0.5f);

	public float MaxResolution = 1f;

	public bool Fullscreen = false;

	Renderer plane_renderer = null;
	MeshFilter plane_meshfilter = null;
	public Mesh PlaneMesh {
		get { return (plane_meshfilter != null ? plane_meshfilter.sharedMesh : null); }
	}

	public Shader CasterShader = null;
	public string CasterShaderTag = null;

	Material TexturedMaterial, FilterMaterial, AlphaClearMaterial;

	#region Setup & Cleanup
	void SetupRenderer(Shadow2DManager shadow_manager) {
		if (plane_renderer != null) return;
		TexturedMaterial = new Material(shadow_manager.PlaneShader);
		FilterMaterial = new Material(shadow_manager.FilterShader);
		AlphaClearMaterial = new Material(shadow_manager.AlphaClearShader);
		plane_meshfilter = gameObject.EnsureComponent<MeshFilter>();
		plane_meshfilter.sharedMesh = MeshUtils.SpriteMesh(new Rect(-1, -1, 2, 2));
		plane_renderer = gameObject.EnsureComponent<MeshRenderer>();
	}

	bool ValidateTexture(Texture tex, int width, int height) {
		if (tex == null) return false;
		if ((tex.width == width) && (tex.height == height)) return true;
		UnityEngine.Object.Destroy(tex);
		return false;
	}
	void SetupTexture(Shadow2DManager shadow_manager, int width, int height) {
		if ((width > 0) && (height > 0) && !shadow_manager.AsFilter) {
			if (use_rt) {
				// Somewhy RT is buggy, even in editor, and always returns false for IsCreated()
				if (!ValidateTexture(tex_rt, width, height)) {
					tex_rt = new RenderTexture(width, height, 24, RenderTextureFormat.ARGB32);
					tex_rt.wrapMode = TextureWrapMode.Clamp;
				}
				use_rt = (tex_rt != null);
			}
			if (!use_rt) {
				if (!ValidateTexture(tex_2d, width, height)) {
					// ARGB32 is required for framebuffer grabbing
					tex_2d = new Texture2D(width, height, TextureFormat.ARGB32, false);
					tex_2d.wrapMode = TextureWrapMode.Clamp;
				}
			}

			if (use_rt) {
				if (tex_2d != null) tex_2d = ObjectUtils.Dispose(tex_2d);
			} else {
				if (tex_rt != null) tex_rt = ObjectUtils.Dispose(tex_rt);
			}
		} else {
			if (tex_2d != null) tex_2d = ObjectUtils.Dispose(tex_2d);
			if (tex_rt != null) tex_rt = ObjectUtils.Dispose(tex_rt);
		}

		var used_tex = (use_rt ? (Texture)tex_rt : (Texture)tex_2d);
		TexturedMaterial.mainTexture = used_tex;
		
		cam.targetTexture = (use_rt ? tex_rt : null);
		if (used_tex != null) {
			cam.pixelRect = new Rect(0, 0, used_tex.width, used_tex.height);
		}
	}
	
	void Setup(int width=0, int height=0) {
		var shadow_manager = Shadow2DManager.Instance;
		if (cam == null) cam = this.EnsureComponent<Camera>();
		SetupRenderer(shadow_manager);
		SetupTexture(shadow_manager, width, height);
	}

	void Cleanup() {
		if (tex_rt != null) tex_rt = ObjectUtils.Dispose(tex_rt);
		if (tex_2d != null) tex_2d = ObjectUtils.Dispose(tex_2d);

		if (plane_renderer != null) {
			ObjectUtils.Dispose(plane_renderer.gameObject);
			plane_renderer = null;
		}
	}
	#endregion

	void OnEnable() {
		var shadow_manager = Shadow2DManager.Instance;
		if (shadow_manager != null) shadow_manager.AddPlane(this);
	}
	void OnDisable() {
		var shadow_manager = Shadow2DManager.Instance;
		if (shadow_manager != null) shadow_manager.RemovePlane(this);
	}

	void Awake() {
		Setup();
	}
	
	void OnDestroy() {
		var shadow_manager = Shadow2DManager.Instance;
		if (shadow_manager != null) shadow_manager.RemovePlane(this);
		Cleanup();
	}

	public bool UpdatePlane(Shadow2DManager shadow_manager, float z_min, float z_max) {
		var main_cam = Camera.main;
		
		Bounds proj_bounds = default(Bounds); // in main camera's space
		int vertex_count = ((Fullscreen || shadow_manager.AsFilter) ? -1 : 0); // AsFilter makes sense only fullscreen
		Matrix4x4 matrix = main_cam.projectionMatrix * main_cam.worldToCameraMatrix;

		// In Filter mode we cannot apply shadow alpha in shader, so we "bake" it beforehand
		float alpha = (shadow_manager.AsFilter ? ShadowColor.a : 1f);
		float z0 = transform.position.z;
		bool shadow_possible = false;
		foreach (var caster in Casters) {
			shadow_possible |= caster.ShadowPossible(z_min, z_max, z0, alpha, shadow_manager, ref proj_bounds, ref vertex_count, matrix);
		}

		var screen_bounds = main_cam.pixelRect;
		if (vertex_count >= 0) {
			screen_bounds = ProjToScreenBounds(main_cam, proj_bounds);
			shadow_possible &= (screen_bounds.width > 0.5f) & (screen_bounds.height > 0.5f);
		}

		cam.enabled = shadow_possible;
		plane_renderer.enabled = shadow_possible;

		if (!shadow_possible) return shadow_possible;

		if (vertex_count >= 0) {
			var planes = ScreenBoundsToPlanes(main_cam, screen_bounds);
			float zNear = 0;
			float zFar = shadow_manager.ShadowSpaceDepth;
			cam.projectionMatrix = Matrix4x4.Ortho(planes.x, planes.y, planes.z, planes.w, zNear, zFar);
		}

		CalculateSizes(main_cam, screen_bounds);
		
		plane_renderer.sharedMaterial = (shadow_manager.AsFilter ? FilterMaterial : TexturedMaterial);
		plane_renderer.sharedMaterial.color = ShadowColor;

		return shadow_possible;
	}

	Rect ProjToScreenBounds(Camera main_cam, Bounds proj_bounds) {
		float w = main_cam.pixelWidth;
		float h = main_cam.pixelHeight;
		float x0 = Mathf.Floor((proj_bounds.min.x + 1f) * w * 0.5f);
		float y0 = Mathf.Floor((proj_bounds.min.y + 1f) * h * 0.5f);
		float x1 = Mathf.Ceil((proj_bounds.max.x + 1f) * w * 0.5f);
		float y1 = Mathf.Ceil((proj_bounds.max.y + 1f) * h * 0.5f);
		x0 = Mathf.Max(x0, 0);
		y0 = Mathf.Max(y0, 0);
		x1 = Mathf.Min(x1, w);
		y1 = Mathf.Min(y1, h);
		return new Rect(x0, y0, x1-x0, y1-y0);
	}

	Vector4 ScreenBoundsToPlanes(Camera main_cam, Rect screen_bounds) {
		float w = main_cam.pixelWidth;
		float h = main_cam.pixelHeight;
		float orthoSizeX = main_cam.orthographicSize * (w / h);
		float orthoSizeY = main_cam.orthographicSize;
		float xL = ((screen_bounds.xMin / w) * 2f - 1f) * orthoSizeX; // left
		float xR = ((screen_bounds.xMax / w) * 2f - 1f) * orthoSizeX; // right
		float yB = ((screen_bounds.yMin / h) * 2f - 1f) * orthoSizeY; // bottom
		float yT = ((screen_bounds.yMax / h) * 2f - 1f) * orthoSizeY; // top
		return new Vector4(xL, xR, yB, yT);
	}

	void CalculateSizes(Camera main_cam, Rect screen_bounds) {
		int rectW = Mathf.CeilToInt(screen_bounds.width);
		int rectH = Mathf.CeilToInt(screen_bounds.height);
		int maxWH = Mathf.Max(rectW, rectH);
		int budget = Mathf.Min(ResolutionBudget(main_cam), maxWH);
		float size_norm = budget / (float)maxWH;
		int sizeX = Mathf.Min(Mathf.CeilToInt(size_norm * rectW), budget);
		int sizeY = Mathf.Min(Mathf.CeilToInt(size_norm * rectH), budget);
		Setup(sizeX, sizeY);

		var tfm = transform;
		var local_pos = tfm.localPosition;
		var main_cam_tfm = main_cam.transform;
		tfm.position = main_cam_tfm.position;
		tfm.rotation = main_cam_tfm.rotation;
		tfm.localPosition = new Vector3(tfm.localPosition.x, tfm.localPosition.y, local_pos.z);
	}

	int ResolutionBudget(Camera main_cam) {
		float max_screen_size = Mathf.Max(main_cam.pixelWidth, main_cam.pixelHeight);
		float budget = (MaxResolution > 1f ? MaxResolution : MaxResolution * max_screen_size);
		return Mathf.CeilToInt(Mathf.Min(budget, max_screen_size));
	}

	Vector3[] plane_vertices = new Vector3[4];
	void UpdatePlaneVertices(float dz=0f) {
		var main_cam = Camera.main;
		var tfm = transform;
		float rz = tfm.position.z - main_cam.transform.position.z;
		plane_vertices[0] = ProjectVertex(main_cam, tfm, 0, 0, rz, dz);
		plane_vertices[1] = ProjectVertex(main_cam, tfm, 0, 1, rz, dz);
		plane_vertices[2] = ProjectVertex(main_cam, tfm, 1, 1, rz, dz);
		plane_vertices[3] = ProjectVertex(main_cam, tfm, 1, 0, rz, dz);
		plane_meshfilter.sharedMesh.vertices = plane_vertices;
		plane_meshfilter.sharedMesh.bounds = default(Bounds); // always visible anyway
	}
	Vector3 ProjectVertex(Camera main_cam, Transform tfm, float x, float y, float z, float dz) {
		var v = cam.ViewportToWorldPoint(new Vector3(x, y, z));
		v = tfm.InverseTransformPoint(v);
		v.z = dz;
		return v;
	}

	Transform save_parent;
	void OnPreCull() {
		var shadow_manager = Shadow2DManager.Instance;

		save_parent = transform.parent;
		transform.parent = null; // shadow plane can be a child of caster

		// In Filter mode we cannot apply shadow alpha in shader, so we "bake" it beforehand
		float alpha = (shadow_manager.AsFilter ? ShadowColor.a : 1f);
		float z0 = transform.position.z;
		foreach (var caster in Casters) {
			if (!caster.CanCast) continue;
			caster.ToShadow(z0, alpha, shadow_manager);
		}

		// Disabling renderer here makes it invisible in main camera pass too!
		UpdatePlaneVertices(float.NegativeInfinity); // "hide" from shadow camera

		Shader.SetGlobalColor("_Color", Color.white);
		Shader.SetGlobalTexture("_MainTex", Texture2D.whiteTexture);
		Shader.SetGlobalTexture("_AlphaMap", Texture2D.whiteTexture);
		Shader.SetGlobalTexture("_MainTex_Alpha", Texture2D.whiteTexture);
	}
	void OnPostRender() {
		if (tex_2d != null) { // ReadPixels must be called from inside the rendering callback
			// At least in editor (5.2.2), for some reason we have to offset by 2x2 pixels to get the correct result
			tex_2d.ReadPixels(new Rect(2, 2, tex_2d.width, tex_2d.height), 0, 0, false);
			//tex_2d.ReadPixels(new Rect(0, 0, tex_2d.width, tex_2d.height), 0, 0, false);
			tex_2d.Apply(false);
		}

		UpdatePlaneVertices();

		foreach (var caster in Casters) {
			if (!caster.CanCast) continue;
			caster.FromShadow(true);
		}

		transform.parent = save_parent;

		// Must be done AFTER UpdatePlaneVertices()
		cam.rect = new Rect(0, 0, 1, 1);
		cam.ResetProjectionMatrix();
		cam.ResetWorldToCameraMatrix();
	}
}
