﻿using UnityEngine;
using System.Collections.Generic;
using Bini.Utils;

[MonoBehaviourSingleton(OnlyActive=false, AutoCreate=true)]
public class Shadow2DManager : MonoBehaviourSingleton<Shadow2DManager> {
	[HideInInspector]
	public List<Shadow2DPlane> Planes = new List<Shadow2DPlane>();
	public void AddPlane(Shadow2DPlane plane) {
		Planes.Add(plane);
	}
	public void RemovePlane(Shadow2DPlane plane) {
		Planes.Remove(plane);
	}

	public float ShadowSpaceDepth = 0.01f;

	public Vector2 ShadowShifting = Vector2.one;
	public float ShadowFading = 0f;
	public Transform LightObject = null;
	
	public LayerMask ShadowLayers = -1;

	float farClipDistance = -1;
	CameraClearFlags clear_flags = CameraClearFlags.SolidColor;

	bool WasFilter = false;
	public bool AsFilter = false;
	public Shader FilterShader = null;
	public Shader AlphaClearShader = null;
	
	public Shader PlaneShader = null;

	public Shader CasterShader = null;
	public string CasterShaderTag = null;

	class PlaneSlice {
		public Shadow2DPlane plane;
		public Camera slice_cam;
	}
	List<PlaneSlice> PlaneSlices = new List<PlaneSlice>();
	Camera CreateSliceCamera() {
		var gameObj = new GameObject("SliceCamera");
		gameObj.transform.ResetToParent(transform);
		return gameObj.AddComponent<Camera>();
	}

	Material AlphaClearMaterial = null;

	void LateUpdate() {
		if (AlphaClearMaterial == null) AlphaClearMaterial = new Material(AlphaClearShader);

		var main_cam = Camera.main;
		var main_cam_pos = main_cam.transform.position;
		var main_cam_rot = main_cam.transform.rotation;
		float z_min, z_max;

		if (AsFilter) {
			if (!WasFilter) {
				clear_flags = main_cam.clearFlags;
				farClipDistance = main_cam.farClipPlane;
			}
			z_min = main_cam_pos.z + main_cam.nearClipPlane;
			z_max = main_cam_pos.z + farClipDistance;
		} else {
			if (WasFilter) {
				main_cam.clearFlags = clear_flags;
				main_cam.farClipPlane = farClipDistance;
			}
			z_min = main_cam_pos.z + main_cam.nearClipPlane;
			z_max = main_cam_pos.z + main_cam.farClipPlane;
		}
		WasFilter = AsFilter;

		Planes.Sort((p0, p1) => {return -p0.transform.position.z.CompareTo(p1.transform.position.z);});

		int shadowplanes_count = 0;
		for (int i = 0; i < Planes.Count; i++) {
			var shadow_plane = Planes[i];
			if (!shadow_plane.UpdatePlane(this, z_min, z_max)) continue;

			if (PlaneSlices.Count <= shadowplanes_count) PlaneSlices.Add(new PlaneSlice());
			var slice = PlaneSlices[shadowplanes_count];
			slice.plane = shadow_plane;

			shadowplanes_count++;
		}

		for (int i = shadowplanes_count; i < PlaneSlices.Count; i++) {
			var slice = PlaneSlices[i];
			slice.plane = null;
			if (slice.slice_cam != null) slice.slice_cam.enabled = false;
		}

		float slice_z0 = z_min, slice_z1 = z_max;
		for (int i = 0; i < shadowplanes_count; i++) {
			int order = shadowplanes_count - (i+1);
			var slice = PlaneSlices[i];

			var cam = slice.plane.cam;
			cam.backgroundColor = Color.clear;
			cam.clearFlags = (AsFilter ? CameraClearFlags.Nothing : CameraClearFlags.SolidColor);
			cam.cullingMask = ShadowLayers;
			cam.nearClipPlane = 0;
			cam.farClipPlane = ShadowSpaceDepth;
			cam.orthographic = true;
			cam.orthographicSize = main_cam.orthographicSize;
			cam.aspect = main_cam.aspect;
			if (AsFilter) cam.rect = main_cam.rect; // otherwise, texture's size is used
			cam.depth = main_cam.depth - ((order+1f) / (shadowplanes_count+1f));
			var caster_shader = slice.plane.CasterShader;
			if (caster_shader == null) caster_shader = CasterShader;
			var caster_shader_tag = slice.plane.CasterShaderTag;
			if (string.IsNullOrEmpty(caster_shader_tag)) caster_shader_tag = CasterShaderTag;
			if ((caster_shader != null) || !string.IsNullOrEmpty(caster_shader_tag)) {
				cam.SetReplacementShader(caster_shader, caster_shader_tag);
			} else {
				cam.ResetReplacementShader();
			}

			slice_z0 = cam.transform.position.z;

			cam = slice.slice_cam;
			if (AsFilter) {
				if (cam == null) {
					cam = CreateSliceCamera();
					slice.slice_cam = cam;
					var alpha_clearer = cam.gameObject.AddComponent<Shadow2DAlphaClearer>();
					alpha_clearer.AlphaClearMaterial = AlphaClearMaterial;
				}
				cam.transform.position = main_cam_pos;
				cam.transform.rotation = main_cam_rot;
				cam.enabled = true;
				cam.backgroundColor = main_cam.backgroundColor;
				cam.clearFlags = (i == 0 ? clear_flags : CameraClearFlags.Nothing);
				cam.cullingMask = main_cam.cullingMask;
				cam.nearClipPlane = slice_z0 - main_cam_pos.z;
				cam.farClipPlane = slice_z1 - main_cam_pos.z;
				cam.orthographic = true;
				cam.orthographicSize = main_cam.orthographicSize;
				cam.aspect = main_cam.aspect;
				cam.rect = main_cam.rect;
				cam.depth = main_cam.depth - ((order+1.5f) / (shadowplanes_count+1f));
			} else {
				if (cam != null) cam.enabled = false;
			}

			slice_z1 = slice_z0;
		}
		if (AsFilter) {
			main_cam.clearFlags = CameraClearFlags.Nothing;
			main_cam.farClipPlane = slice_z0 - main_cam_pos.z;
		}
	}
}
