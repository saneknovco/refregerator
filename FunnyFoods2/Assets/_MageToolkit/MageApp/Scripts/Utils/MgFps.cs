﻿using UnityEngine;

public class MgFps : MonoBehaviour {

#if UNITY_EDITOR
    [Tooltip("Script working just in MAGE_DEBUG mode")]
#endif
    public bool isShowFps = true;
	public float updateInterval = 0.25F;
	public int fontSize = 32;
    public Color fontColor = Color.red;
    
#if MAGE_DEBUG

    public string fps
	{
		get { return _fps.ToString("f1"); }
	}
        	
	private float _lastInterval;
	private float _accum = 0; // FPS accumulated over the interval
	private int _frames = 0; // Frames drawn over the interval
	private float _timeleft; // Left time for current interval
	private float _fps;    
	private GUIStyle _labelStyle; 

    void Start () 
	{
		Application.targetFrameRate = 60;
		_timeleft = updateInterval;
		_labelStyle = new GUIStyle();
		_labelStyle.fontSize = fontSize;
		_labelStyle.normal.textColor = fontColor;	
	}
	
	// Update is called once per frame
	void Update () 
	{
		_timeleft -= Time.deltaTime;
		_accum += Time.timeScale / Time.deltaTime;
		++_frames;

		if (_timeleft <= 0.0)
		{
			// display two fractional digits (f2 format)
			_fps = _accum / _frames;			
			_timeleft = updateInterval;
			_accum = 0.0F;
			_frames = 0;
		}	
	}
        	
	void OnGUI()
	{
		if(isShowFps)
			GUILayout.Label("  " + _fps.ToString("f1") + " fps", _labelStyle);
	}
#endif
}
