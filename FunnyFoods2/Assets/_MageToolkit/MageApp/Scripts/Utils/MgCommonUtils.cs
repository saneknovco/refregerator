﻿using UnityEngine;

public static class MgCommonUtils
{
	#region Aspect
    public const int CountTypeAspect = 4;

    public static void ResetAspect()
    {
        _baseAspect = -1.0f;
        _baseOrtographicSizeToUI = -1.0f;
    }

    private static float _baseAspect = -1.0f;
    public static float baseAspect
    {
        get 
        {
            if (_baseAspect <=0.0f)
            {
                if ((float)Screen.width / (float)Screen.height > 1.0f) _baseAspect = 4.0f / 3.0f;
                else _baseAspect = 3.0f / 4.0f;
            }
            return MgCommonUtils._baseAspect; 
        }      
    }

    private static float _baseOrtographicSizeToUI = -1.0f;
    public static float baseOrtographicSizeToUI
    {
        get 
        {
            if(_baseOrtographicSizeToUI<= 0.0f)
            {
                if ((float)Screen.width / (float)Screen.height > 1.0f) MgCommonUtils._baseOrtographicSizeToUI = 768.0f;
                else MgCommonUtils._baseOrtographicSizeToUI = 1024.0f;
            }
            return MgCommonUtils._baseOrtographicSizeToUI;
        }
    }

	public static MageAppAspectRatio GetAspectRatio(Camera camera = null)
	{
		float aspect;
		if (camera == null) aspect = (float)Screen.width / (float)Screen.height;
		else aspect = camera.aspect;
        if (aspect > 1.0f)//landscape		
		{
			if (aspect >= 1.7f) return MageAppAspectRatio.Aspect16by9;
			if (aspect >= 1.59f) return MageAppAspectRatio.Aspect16by10;
			if (aspect >= 1.49f) return MageAppAspectRatio.Aspect3by2;
			//WARNING!!!!
			if (aspect >= 1.28f) return MageAppAspectRatio.Aspect4by3;
			if (aspect >= 1.20f) return MageAppAspectRatio.Aspect5by4;
			//WARNING!!!!
			//if (aspect >= 1.3f) return MageAppAspectRatio.Aspect4by3;
			//if (aspect >= 1.24f) return MageAppAspectRatio.Aspect5by4;
		}
        else //portrait
		{
			if (aspect >= 0.79f) return MageAppAspectRatio.Aspect5by4;
			if (aspect >= 0.74f) return MageAppAspectRatio.Aspect4by3;
			if (aspect >= 0.65f) return MageAppAspectRatio.Aspect3by2;
			if (aspect >= 0.61f) return MageAppAspectRatio.Aspect16by10;
			//WARNING
			if (aspect >= 0.545f) return MageAppAspectRatio.Aspect16by9;
			//WARNING
			//if (aspect >= 0.55f) return MageAppAspectRatio.Aspect16by9;
		}
		return MageAppAspectRatio.AspectOthers;
	}

	public static int GetAspectRatioToIndex(Camera camera = null)
	{
		int idx = (int)GetAspectRatio(camera);
		if (idx < 0 || idx >= CountTypeAspect)
		{
			if(Debug.isDebugBuild) Debug.LogError("Not supported MgCommonUtils.GetAspectRatioIndex:index: " + idx);
			idx = 0;
		}
		return idx;
	}
    #endregion

	#region language
	public static bool IsFullSupportLang(MageAppUILang lang)
	{
#if UNITY_IOS
        return true;
#else
		var brand = PrjSettings.instance.settings.brand;
		if(brand == MageAppBrand.PUBLISHER)
			return lang == MageAppUILang.cn ? true: false;//LEAD JOY
		else
			return lang == MageAppUILang.en || lang == MageAppUILang.ru ? true: false;
#endif
    }

#endregion

#region GUI

    public static float CalcCamSizeToUIView(Camera camera = null)
    {
        if (camera == null) camera = Camera.main;
        var baseUICamSize = MgCommonUtils.baseOrtographicSizeToUI;//768 or 1024
        MageAppAspectRatio aspect = MgCommonUtils.GetAspectRatio(camera);
        switch (aspect)
        {
			/// WARNING - NO TESTED!!!!
			case MageAppAspectRatio.Aspect5by4:
				{

					if (camera.aspect < 1.0f) baseUICamSize = (0.75f / camera.aspect) * baseUICamSize;
					else baseUICamSize = (1.33333f / camera.aspect) * baseUICamSize;	
					break;
				}

			
			case MageAppAspectRatio.Aspect3by2:
				{
					if (camera.aspect < 1.0f) baseUICamSize = 1152.0f;//909.0f;//960;//WARNING
                    else baseUICamSize = 682.0f;
                    break;
                }

			case MageAppAspectRatio.Aspect16by10:
                {
                    if (camera.aspect < 1.0f) baseUICamSize = 1152.0f;//1282.0f;//WARNING
                    else baseUICamSize = 682.0f;//640.0f;//752.0f;
                    break;
                } 

            case MageAppAspectRatio.Aspect16by9:
                {
			        if (camera.aspect < 1.0f) baseUICamSize = 1152.0f;//909.0f;//1365//WARNING
                    else baseUICamSize = 682.0f;
                    break;
                }

            
        	case MageAppAspectRatio.AspectOthers:
				{
					if(camera.aspect < 1.0f )
					{
						if(camera.aspect<0.55f)
							baseUICamSize = (camera.aspect/0.5625f) * baseUICamSize;
					}
					else
					{
						if (camera.aspect < 1.25f)
							baseUICamSize = (1.33333f / camera.aspect) * baseUICamSize;						
					}
					break;
				}
        }
        return baseUICamSize;
    }

    public static void SetObjectPlane(GameObject plane, bool beforeCamera = true, Camera camera = null, float planeZ = 0.5f)
    {
        if (camera == null) camera = Camera.main;
        if (beforeCamera)
        {
            var pos = plane.transform.position;
            pos.z = camera.transform.position.z + planeZ;
            plane.transform.position = pos;
        }
        else
        {
            plane.transform.position = new Vector3(plane.transform.position.x, plane.transform.position.y,
                plane.transform.position.z + 2.0f * camera.farClipPlane);
        }
    }

    public static float ScaleUnitToPoint(Camera camera = null )
    {
        if (camera == null) camera = Camera.main;
        return camera.orthographicSize / baseOrtographicSizeToUI;
    }

    public static float ScaleUnitToPoint34(Camera camera = null)
    {
        if (camera == null) camera = Camera.main;
        var scl = camera.orthographicSize / baseOrtographicSizeToUI;
        if(camera.aspect > 1.0f)
        {
            if (GetAspectRatio(camera) > MageAppAspectRatio.Aspect4by3)
                scl = scl * 9f / 8f; //(3:2/4:3) 
        }
        return scl;
    }

	public static Vector3 CalcPositionToUIView(Camera camera = null, float planeZ = 10.0f)
    {
        if (camera == null) camera = Camera.main;
        var offsetY = MgCommonUtils.baseOrtographicSizeToUI - camera.orthographicSize;
        var position = camera.transform.position;
        return new Vector3(position.x, position.y - offsetY, position.z + planeZ);
	}

	public static Vector3 CalcPositionToUIView(Vector3 basePosition, Camera camera = null, float planeZ = 10.0f)
 	{
        if (camera == null) camera = Camera.main;
        var offsetY = MgCommonUtils.baseOrtographicSizeToUI - camera.orthographicSize;
        return new Vector3(basePosition.x, basePosition.y - offsetY, basePosition.z + planeZ);
 	}

    #endregion


    #region Communication

	public static string GetObjectTypeToPlayerPrefs(Object obj)
	{
		return AppConfig.prefixMageToolkit + obj.GetType().Name;
	}

	public static void OpenUriWithAudit( Object sender, string uri)
	{
		string objectName = GetObjectTypeToPlayerPrefs( sender );
		int countOpening = PlayerPrefs.GetInt (objectName, 0) + 1;
		//Debug.Log (objectName);
		PlayerPrefs.SetInt ( objectName, countOpening );
		PlayerPrefs.Save();
		Application.OpenURL ( uri );
	}

    static public string GetAppLocalUri(string appId)
	{
#if UNITY_IOS || UNITY_TVOS
       return AppConfig.prefixCustomUrlApp + appId + @"://";
#else
        return appId;
#endif
	}

	static public string GetAppStoreUri(string appId)
	{
#if UNITY_ANDROID
        //string uri = @"https://play.google.com/store/apps/details?id=" + appId;

#if UNITY_EDITOR
        string uri = @"https://play.google.com/store/apps/details?id=" + appId;
#else
		string uri = @"market://details?id=" + appId;
#endif

#elif UNITY_WSA
        string uri = @"http://apps.microsoft.com/windows/app/" + appId);
#else
		string uri = string.Format("https://itunes.apple.com/app/id{0}?ls=1&mt=8", appId);
#endif
		return uri;
	}

	static public void GetAppInStore(string appId)
	{
		Application.OpenURL(GetAppStoreUri(appId));
	}

	static public bool IsAppInstalled(string appId)
	{
#if UNITY_IOS || UNITY_TVOS
		// BIGGG RAKE!!!!!
        Debug.LogWarning("MgCommonUtils.IsAppInstalled : Disabled into iOS 9+ !!!");
		return false;
#else
        return SysUtilsProxy.AppIsInstalled(GetAppLocalUri(appId));
#endif
	}

    #endregion

    #region Metadata & runtime-behaviour
#if !UNITY_IOS && !UNITY_TVOS && !UNITY_ANDROID
	public static void InitFromDefaultValueAttribute (System.Object component)
    {
        // for support DefaultValue

        foreach (System.ComponentModel.PropertyDescriptor property in System.ComponentModel.TypeDescriptor.GetProperties(component))
        {
            System.ComponentModel.DefaultValueAttribute myAttribute =
                (System.ComponentModel.DefaultValueAttribute)property.Attributes[typeof(System.ComponentModel.DefaultValueAttribute)];
            if (myAttribute != null) property.SetValue(component, myAttribute.Value);
        }
    }
#endif
    #endregion

}
