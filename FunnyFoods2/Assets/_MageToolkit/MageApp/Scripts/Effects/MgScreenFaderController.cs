﻿using UnityEngine;
using System.Collections;
using Mage.Effects;

public class MgScreenFaderController : GlobalSingletonBehaviour<MgScreenFaderController>
{
	#region Static

	public static void Stop()
	{      
		AppState.instance.UIViewMode &= ~MageAppUIViewMode.FadingView;
        Release(false);
	}

	public static void Run(FadingParams aFadingParams)
	{
		if (aFadingParams.isAutoSetUIViewMode)	AppState.instance.UIViewMode |= MageAppUIViewMode.FadingView;
		var fading = MgScreenFaderController.instance;
		if (fading != null)
		{
			fading.fadingParams = aFadingParams.ShallowCopy();
			fading.OnFading(fading.fadingParams.mode);
		}
		else if (aFadingParams.isAutoSetUIViewMode) AppState.instance.UIViewMode &= ~MageAppUIViewMode.FadingView;
	}

    public static void Run(MageAppFadingMode mode, float duration =-1f)
    {
		if (AppSettings.instance.Effect.fading.isAutoSetUIViewMode)
			AppState.instance.UIViewMode |= MageAppUIViewMode.FadingView;
        //if (Debug.isDebugBuild) Debug.LogWarning("ScreenFading:Run:uiViewMode:".AddTime() + AppState.Instance.uiViewMode);
        var fading = MgScreenFaderController.instance;
        if (fading != null )
        {
            if(duration > 0.0f)fading.fadingParams.duration = duration;
            fading.OnFading(mode);
        }
        else if (AppSettings.instance.Effect.fading.isAutoSetUIViewMode) AppState.instance.UIViewMode &= ~MageAppUIViewMode.FadingView;
    }

	#endregion

	public FadingParams fadingParams;// = new FadingParams();

	private GameObject _hFader = null;

	private void OnFading(MageAppFadingMode fadingMode)
	{
		if(_hFader == null)
		{
			var prefab = Resources.Load<GameObject>("UI/MgScreenFader");
			if (prefab != null) _hFader = GameObject.Instantiate(prefab) as GameObject;
			if (_hFader == null) return;
			_hFader.name = AppConfig.prefixFVTAutoObject + prefab.name;
		}
		var cmp = _hFader.GetComponent<MgScreenFader>();
		if (cmp == null) return;
		fadingParams.mode = fadingMode;
		cmp.Fading(fadingParams);		
	}

	protected override void DoAwake()
    {
        base.DoAwake();       
       
        fadingParams = AppSettings.instance.Effect.fading.ShallowCopy();       
        Messenger.AddListener<MageAppFadingMode>(AppState.EventTypes.uiRunScreenFading, OnFading);
	}

    protected override void DoDestroy()
    {       
        if (Messenger.eventTable.ContainsKey(AppState.EventTypes.uiRunScreenFading))
            Messenger.RemoveListener<MageAppFadingMode>(AppState.EventTypes.uiRunScreenFading, OnFading);
    }
}
