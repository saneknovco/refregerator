﻿using UnityEngine;

namespace Mage.Effects
{

	/// <summary>
	/// enum modes of fading
	/// </summary>
	[System.Serializable]
	public enum MageAppFadingMode
	{
		/// <summary>
		/// from hidden to visible
		/// </summary>
		In,
		InContinued,
		/// <summary>
		/// from visible to hidden 
		/// </summary>
		Out,
		OutContinued
	}

	[System.Serializable]
	public class FadingParams
	{
		public float duration = 0.5f;
		public MageAppFadingMode mode = MageAppFadingMode.OutContinued;
		public Color color = Color.white;
		public bool isAutoSetUIViewMode = true;

		public FadingParams ShallowCopy()
		{
			return (FadingParams)MemberwiseClone();
		}

		public void SetDefaultValues()
		{
			duration = 0.5f;
			mode = MageAppFadingMode.OutContinued;
			isAutoSetUIViewMode = true;
		}
	}

    [System.Serializable]
    public class BackScreenParams
    {
        public Color color = new Color(0, 0, 0, 0.67f);

        public BackScreenParams ShallowCopy()
        {
            return (BackScreenParams)MemberwiseClone();
        }

        public void SetDefaultValues()
        {
            color = new Color(0, 0, 0, 0.67f);
        }

    }

}
