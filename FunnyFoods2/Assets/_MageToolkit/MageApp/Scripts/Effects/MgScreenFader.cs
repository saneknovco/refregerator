﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using Mage.Effects;

public class MgScreenFader : MonoBehaviour 
{
	public FadingParams _fading;
	private GUITexture _guiTexture;

	private bool _suspend;
	private bool _isDone = false;
	private bool _start = false;

	private float _startTime;
	private float _endTime;
	private float _duration;
	private Color _colorFadeStart;
	private Color _colorFadeEnd;

	private AppState _appState;

	public void Fading(FadingParams fadingParam)
	{
		_fading = fadingParam.ShallowCopy();
		Color clrTransparent = _fading.color;
		clrTransparent.a = 0.0f;

		if(_fading.mode == MageAppFadingMode.Out || 
		   _fading.mode == MageAppFadingMode.OutContinued) {
			_colorFadeStart = clrTransparent;
			_colorFadeEnd = _fading.color;
			_guiTexture.color = clrTransparent;
		}
		else {			
			_colorFadeStart = _fading.color;
			_colorFadeEnd = clrTransparent;
			_guiTexture.color = _fading.color;			
		}

		//_startTime = Time.time;
		//_endTime = _startTime + _fading.duration;
		//_duration = _endTime - _startTime;
		//Debug.Log("START:  " +_fading.mode + " " + _duration + " " + _endTime + " " + _colorFadeStart + " " + _colorFadeEnd);
		
		if (!_guiTexture.enabled) _guiTexture.enabled = true;
		if (_fading.isAutoSetUIViewMode) _appState.UIViewMode |= MageAppUIViewMode.FadingView;
		_suspend = false;
		_start = true;
	}
	
    void Awake()
    {        
		_guiTexture = gameObject.GetComponent<GUITexture>();
		_guiTexture.pixelInset = new Rect(0f, 0f, Screen.width, Screen.height);
		_guiTexture.color =  Color.clear;
		_guiTexture.enabled = false;	
		_suspend = true;
		_isDone = false;
		_appState = AppState.instance;
    }

    void Update()
    {
		if(_suspend) return;	
		if(_start)
		{
			_start = false;
			_startTime = Time.time;
			_endTime = _startTime + _fading.duration;
			_duration =  _endTime - _startTime;
			//Debug.Log("START UPD:  " + _fading.mode + " " + _duration + " " + _endTime + " " + _colorFadeStart + " " + _colorFadeEnd);
		}	
		float t = Time.time;
		if( t > _endTime)
		{
			_guiTexture.color = _colorFadeEnd;
			_suspend = true;
			if (_fading.isAutoSetUIViewMode) _appState.UIViewMode &= ~MageAppUIViewMode.FadingView;
			if (_fading.mode == MageAppFadingMode.In || _fading.mode == MageAppFadingMode.Out ) _isDone = true;
		}
		else
		{
			_guiTexture.color = Color.Lerp(_colorFadeStart, _colorFadeEnd, (t - _startTime) / _duration);
		}
		//Debug.Log(" ".AddTime() + _guiTexture.color + " " + (t - _startTime) / _duration);
		if (_isDone)
		{
			//Destroy(this.gameObject)
			_guiTexture.enabled = false;
			_isDone = false;
		}
    }    
}   
