﻿using UnityEngine;
using System.Collections;

/// <summary>
/// Эффект наложения прозрачности для группы объектов
/// </summary>
[AddComponentMenu("Mage/Effects/Transparent")]
public class MgTransparentEffect : MonoBehaviour
{
	#region Members
	public bool autoZ = false;
	public float speed= 1.0f;
	public bool isApply = true;

	/// <summary>
	/// родительский объект для применения эффекта (alpha)
	/// </summary>
	public GameObject targetEffect = null;
	/// <summary>
	/// родительский объект для применения инвертированного эффекта (1.0-alpha)
	/// </summary>
	public GameObject targetInvertedEffect = null;
	
	private bool _noEffect = true;
	private float _duration;	
	private float _delta;
	private float _startAlpha = 0.0f;
	private float _endAlpha = 1.0f;
	private float _alpha = 0.0f;
	private float _origZ;

	private string _eventType = @"AE_TransparentEffect";

	/// <summary>
	/// задается группе дочерних объектов начиная с target уровень прозрачности alpha,
	/// если шейдер в материале поддерживает изменения уровня прозрачности 
	/// </summary>
	/// <param name="alpha"></param>
	/// <param name="target"></param>
	private void _SetTransparent(float alpha, GameObject target)
	{
		var rnd = target.GetComponent<Renderer>();
		var renderers = target.GetComponentsInChildren<Renderer>();
		if (rnd !=null)	{
			if (rnd.material != null) {
				if (rnd.material.HasProperty("_Color"))	{
					Color clr = rnd.material.color;
					clr.a = alpha;
					rnd.material.color = clr;
				}
			}
		}
		if (renderers != null) {
			foreach (var item in renderers)
			{
				if (item.material != null) {
					if (item.material.HasProperty("_Color")) {
						Color clr = item.material.color;
						clr.a = alpha;
						item.material.color = clr;
					}
				}
			}
		}
	}
	
	/// <summary>
	/// применения эффекта прозрачности
	/// </summary>
	/// <param name="alpha"></param>
	private void SetTransparent(float alpha)
	{
		if (targetEffect == null) return;
		_SetTransparent(alpha, targetEffect);

		if(targetInvertedEffect)
			_SetTransparent(1.0f - alpha, targetInvertedEffect);	
	}
	
	/// <summary>
	/// вызов эффекта прозрачности из скрипта
	/// </summary>
	/// <param name="alphaEnd"></param>
	/// <param name="frameCount"></param>
	public void OnTransparent(float alphaEnd, int frameCount)
	{		
		//Lock twice call
		if(!_noEffect || ! isApply) return;		
		if (frameCount <= 0)
		{
			SetTransparent(alphaEnd);
			if(autoZ && Mathf.Approximately(alphaEnd,0.0f))
			{
				var pos = targetEffect.transform.position;
				pos.z = _origZ;
				targetEffect.transform.position  = pos;
			}
			return;
		}
		if(autoZ)
		{
			var pos = targetEffect.transform.position;
			pos.z = Camera.main.transform.position.z + 0.5f;
			targetEffect.transform.position = pos;
		}
		_noEffect = false;
		_duration = (float)frameCount / (speed*60.0f);
		_endAlpha = alphaEnd;
		_startAlpha = 1.0f - _endAlpha;
		_alpha = _startAlpha;
		_delta = Time.fixedDeltaTime * speed / _duration;
		if (_startAlpha > _endAlpha) _delta *= -1.0f;		
	}

	/// <summary>
	/// вызов эффекта прозрачности при срабатывании события AnimationEvent в AnimationClip
	/// </summary>
	/// <param name="ae"></param>
	public void OnTransparent(AnimationEvent ae)
	{
		//Debug.Log (@"start transparent: " + this.name);
		//проверка какому контроллеру послано событие о применении эффекта
		if (this.name != ae.stringParameter) return;
		OnTransparent(ae.floatParameter, ae.intParameter);
	}
	#endregion

	#region Unity event functions

	void Awake()
	{

		_noEffect = true;		
		if (targetEffect == null) targetEffect = this.gameObject;	
		_origZ = targetEffect.transform.position.z;
		Messenger.AddListener<AnimationEvent>(_eventType, OnTransparent);
	}

	void Update()
	{		
		if (_noEffect) return;
		if (_delta < 0.0f ? (_endAlpha - _alpha) >= 0.0f : (_endAlpha - _alpha) <= 0.0f)
		{
			SetTransparent(_endAlpha);
			_noEffect = true;
			if(autoZ && Mathf.Approximately(_endAlpha,0.0f))
			{
				var pos = targetEffect.transform.position;
				pos.z = _origZ;
				targetEffect.transform.position  = pos;
			}
		}
		else
			SetTransparent(_alpha);
		_alpha += _delta;	
	}

	void OnDestroy()
	{		
		if(Messenger.eventTable.ContainsKey(_eventType))
		Messenger.RemoveListener<AnimationEvent>(_eventType, OnTransparent);
	}

	#endregion

}
