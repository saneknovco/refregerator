﻿using UnityEngine;
using System.Collections;
using System;


[System.Serializable]
public enum PlaneFaderEffect
{    
    FadeIntro,
    FadeOut,
    FadeBoth
};

public class MgPlaneFader : MonoBehaviour {
  
    public static GameObject instance = null;   

    public static void StartPlaneFader(Action<MonoBehaviour> faderBackAction = null)
    {
        GameObject fader = Resources.Load("UI/Effects/planeFader") as GameObject;
        if (fader)
        {            
            instance = GameObject.Instantiate(fader) as GameObject;
            if (faderBackAction!=null)
                instance.GetComponent<MgPlaneFader>().faderBackAction += faderBackAction;
        }
    }
    
    public Camera workCamera = null;
    public PlaneFaderEffect fadeEffect = PlaneFaderEffect.FadeBoth;
    public float planeZ = 0.1f;

    public float startAlpha = 0.0f;
    public float endAlpha = 0.85f;
    
    public int frameCount = 60;
    public int frameDelay = 20;
    public float speed = 1.0f;

    public bool autoDestroy = true;
    public bool isBackAction = true;

    public Action<MonoBehaviour> faderBackAction;    
    
    private bool _halfEffect = true;
    private bool _isApply = false;
    private bool _noEffect = true;
    private float _duration;
    private float _delta;
    private float _startAlpha = 0.0f;
    private float _endAlpha = 1.0f;
    private float _alpha = 0.0f;
    private int _delay = 0;

	private Renderer _renderer = null;
	private bool _hasColor = false;

    private IEnumerator DoBackgroundAction()
    {
        if(!isBackAction) yield break;
        float wait = (float)frameCount / (speed * 60.0f);
        if (fadeEffect != PlaneFaderEffect.FadeBoth) wait *= 0.5f;        
        if (wait < 0.1f) wait = 0.1f;
        yield return new WaitForSeconds(wait);
        if (faderBackAction != null)faderBackAction(this);
    }

    private void _SetTransparent(float alpha)
    {
		if (_hasColor)
		{
			Color clr = _renderer.material.color;
			clr.a = alpha;
			_renderer.material.color = clr;
		}
    }

   private void DoApplyEffect()
    {
        //Lock twice call
        if (!_noEffect) return;
        if (fadeEffect != PlaneFaderEffect.FadeBoth) _isApply = true;
        else if (!_halfEffect)
        {
            _delay = frameDelay;
            _isApply = true;
        } 
        _noEffect = false;
        _duration = (float)frameCount / (speed * 60.0f);
        _endAlpha = endAlpha;
        _startAlpha = startAlpha;
        _alpha = _startAlpha;
        _delta = Time.fixedDeltaTime * speed / _duration;
        if (_startAlpha > _endAlpha) _delta *= -1.0f;    
    }

    void Awake()
    {
        if (workCamera == null) workCamera = Camera.main;
        MgCommonUtils.SetObjectPlane(gameObject, true, workCamera, planeZ);
		_renderer = GetComponent<Renderer>();
		if (_renderer != null )
			if(_renderer.material !=null ) _hasColor = _renderer.material.HasProperty("_Color");
    }

	// Use this for initialization
    void Start() 
    {     
        StartCoroutine(DoBackgroundAction());
        DoApplyEffect();  
	}    

    void FixedUpdate()
    {
        if (_noEffect) return;
        if(_delay>0) {
            _delay--;
            return;
        }

        _SetTransparent(_alpha);
        if (_delta < 0.0f ? (_endAlpha - _alpha) >= 0.0f : (_endAlpha - _alpha) <= 0.0f)
        {
            _SetTransparent(_endAlpha);
            _noEffect = true;
            if (fadeEffect == PlaneFaderEffect.FadeBoth && _halfEffect) {
                var swap = startAlpha;
                startAlpha = endAlpha;
                endAlpha = swap;
                _halfEffect = false;
                DoApplyEffect();
            }
            else MgCommonUtils.SetObjectPlane(gameObject, false, workCamera);            
        } 
        else _alpha += _delta;
    }

    void LateUpdate()
    {
        if(autoDestroy && _noEffect && _isApply) {
            GameObject.Destroy(gameObject);
            instance = null;
        }
    }

}
