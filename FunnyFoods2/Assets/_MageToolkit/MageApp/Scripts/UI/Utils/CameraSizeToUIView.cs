﻿using UnityEngine;

public class CameraSizeToUIView : MonoBehaviour {

	Camera _cam = null;

	void Awake()
	{  
        _cam = gameObject.GetComponent<Camera>();
		if(_cam == null) _cam = Camera.main;
        _cam.orthographicSize = MgCommonUtils.CalcCamSizeToUIView(_cam);
	}	
}
