﻿using UnityEngine;

//public class AdaptUIContainer : FullInspector.BaseBehavior<FullInspector.FullSerializerSerializer>
public class AdaptUIContainer : MonoBehaviour //FullInspector.BaseBehavior !!!!
{
	[Header("Камера, к которой привязан scale & pos.")]    
    public Camera workCamera;
	public float baseUICamSize =768.0f;
    public float sensitivityCamSize = 0.1f;
    [Tooltip("Если true то применяется алгоритм позиционирования контейнера")]
    public bool shiftAtDeltaCamSize = true;// = MgCommonUtils.baseOrtographicSizeToUI - baseUICamSize; 	
	[Tooltip("в приложении адаптивное изменение камеры для аспектов")]
	public bool adaptiveCamSize = true;
	[Tooltip("в приложении адаптивное изменение позиции камеры для аспектов")]
	public bool adaptiveShiftCamSize = true;

    Vector3 _sclOrig;
	Vector3 _posOrig;
    float _sclAdapt;
	float camSize_prev = 0;
    bool _waitInit = true;  

	private void OnAdaptScale()
	{
        // необходим если в реал тайме меняется aspect Rate
        //baseUICamSize = MgCommonUtils.CalcCamSizeToUIView(workCamera); 

		camSize_prev = workCamera.orthographicSize;
		_sclAdapt = workCamera.orthographicSize / baseUICamSize;
        transform.localScale = _sclAdapt * _sclOrig;
		//Debug.Log(workCamera.aspect);
		/// PATCH FOR ANDROID
		if (workCamera.aspect > 1.79f)
		{
			transform.localScale = (workCamera.aspect / 1.79f) * transform.localScale;
		}
	}

    private void OnCameraSizeToUIView()
    {
        baseUICamSize = MgCommonUtils.CalcCamSizeToUIView(workCamera);
        if(_waitInit == false) OnAdaptScale();
    }

	void Awake() // protected override void Awake()
	{        
        if (workCamera == null) workCamera = Camera.main;
        _sclOrig = transform.localScale;
        Messenger.AddListener(AppState.EventTypes.uiCameraSizeToUIView, OnCameraSizeToUIView);
	}

	void Start () 
    {
        baseUICamSize = MgCommonUtils.CalcCamSizeToUIView(workCamera);
        if (shiftAtDeltaCamSize)
        {
			var posOrig = transform.localPosition;
			//Debug.Log ("posit " + transform.position + " " + transform.localPosition + " " + workCamera.transform.localPosition + " " + workCamera.transform.position);
			float shift = adaptiveCamSize ? baseUICamSize - MgCommonUtils.baseOrtographicSizeToUI : workCamera.orthographicSize - MgCommonUtils.baseOrtographicSizeToUI;
			if(adaptiveShiftCamSize){
				//this is @Magic@ - в разных приложениях как попало двигают камеру - где локал, где глобал! 
				//надеюсь эта формул ВЕРНА!!!
				shift += workCamera.transform.position.y - workCamera.transform.localPosition.y;
			}
			transform.localPosition = new Vector3(posOrig.x, posOrig.y + shift,	posOrig.z);
		};

        StartCoroutine(MgCoroutineHelper.WaitEndOfFrameThenCallback(
            () => { OnAdaptScale(); _waitInit = false; }));	
	}	
	
	void LateUpdate () 
    {
        if (_waitInit) return;
        if (Mathf.Abs(camSize_prev - workCamera.orthographicSize) > sensitivityCamSize)	
            OnAdaptScale();	
	}

    void OnDestroy()
    {
        if(Messenger.eventTable.ContainsKey(AppState.EventTypes.uiCameraSizeToUIView))
            Messenger.RemoveListener(AppState.EventTypes.uiCameraSizeToUIView, OnCameraSizeToUIView);	
    }
}
