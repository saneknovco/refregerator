﻿using UnityEngine;


public class JustToAppType : MonoBehaviour {

    public MageAppType appType = MageAppType.Lite;

    void Awake()
    {
        if (AppSettings.instance.appInfo.activeAppType != appType)
        {
            gameObject.SetActive(false);
            GameObject.Destroy(gameObject);
        }
    }
}
