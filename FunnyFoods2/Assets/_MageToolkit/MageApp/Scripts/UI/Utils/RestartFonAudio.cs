﻿using UnityEngine;
using UnityEngine.SceneManagement;

/// <summary>
/// проигрывание\остановка фоновой музыки при смене сцены
/// (объект не удаляется при переходе на другие сцены для плавности звучания)
/// </summary>
public class RestartFonAudio : MonoBehaviour {

    public int foneAudioLevel = 1;

    private AudioSource _audio = null;
    private bool _statePause = false;
    private bool _statePrevPlaying = false;
    private Scene _activeScene;

    void Awake()
    {
        _audio = GetComponent<AudioSource>();
        _statePause = false;
        _statePrevPlaying = false;
    }    
   
	// Update is called once per frame
	void Update () 
    {
        if (_audio == null) _audio = GetComponent<AudioSource>();
        else
        {
            _activeScene = SceneManager.GetActiveScene();
            if ( (_statePause == false) && (foneAudioLevel != _activeScene.buildIndex))
            {
                _statePause = true;
                _statePrevPlaying = _audio.isPlaying;
                _audio.Pause();
                return;
            }
            if(_statePause && (foneAudioLevel == _activeScene.buildIndex) )
            {
                _statePause = false;
                if (_statePrevPlaying) _audio.Play();
            }
        }
	}
}
