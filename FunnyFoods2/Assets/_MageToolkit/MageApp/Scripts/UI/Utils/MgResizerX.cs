﻿using UnityEngine;
using System.Collections;

/// <summary>
/// scale by x texture by aspect/// 
/// </summary>
/// <example>Bini: prefab - CPUIViewControl\gameobject - panelTop</example>
public class MgResizerX : MonoBehaviour {

    public Camera workCamera;
    public MageAppAspectRatio[] aspectPosCorrect;
    public MageAppAspectRatio[] aspectScaleCorrect;
	private float baseUICamSize = 768;
    private float baseUIScale = 3.0f/2.0f;

	private void GetBaseCamSize()
	{
		if(workCamera== null) workCamera = Camera.main;	
        baseUICamSize = MgCommonUtils.CalcCamSizeToUIView(workCamera);
	}

    void Awake()
    {
        var v = transform.localPosition;
        var asp = AppState.instance.aspect;

        GetBaseCamSize();
        if (aspectPosCorrect != null)
        {
            for (int i = 0; i < aspectPosCorrect.Length; i++)
                if (asp == aspectPosCorrect[i])
                {
                    transform.localPosition = new Vector3(v.x, v.y - (MgCommonUtils.baseOrtographicSizeToUI - baseUICamSize), v.z);
                    break;
                }
        }

        if (aspectScaleCorrect != null)
        {
            for (int i = 0; i < aspectScaleCorrect.Length; i++)
                if (asp == aspectScaleCorrect[i])
                {
                    float scale = workCamera.aspect / baseUIScale;
                    v = transform.localScale;
                    v.x = scale * v.x;
                    transform.localScale = v;
                    break;
                }
        }
    }
}
