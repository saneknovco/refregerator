﻿using System.Collections.Generic;
using UnityEngine;

[DisallowMultipleComponent]
public class UriOpenButton : BaseOpenUrlButton
{
	[FullInspector.InspectorCategory("Action")]
	[FullInspector.InspectorMargin(12)]
	[FullInspector.InspectorOrder(1)]
	public string defaultUri;

	[SerializeField]
	[FullInspector.InspectorHidePrimary]
	private Dictionary<RuntimePlatform, string> _uri;
	[FullInspector.ShowInInspector]
	[FullInspector.InspectorCategory("Action")]
	[FullInspector.InspectorOrder(2)]
	public Dictionary<RuntimePlatform, string> uri
	{
		get
		{
			if (_uri == null) _uri = new Dictionary<RuntimePlatform, string>();
			if (_uri.Count < 1)
			{
				_uri = new Dictionary<RuntimePlatform, string>() {
						{RuntimePlatform.IPhonePlayer, defaultUri},
						{RuntimePlatform.Android, defaultUri}
					};
			}					
			return _uri;
		}
		private set { _uri = value; }
	}	

	public override string GetUri()
	{
#if UNITY_EDITOR
		var platform = MgUnityHelper.GetRuntimePlatformByActiveBuildTarget();
		return uri.SafeGet(platform, defaultUri);
#else
		return uri.SafeGet(Application.platform, defaultUri);
#endif

	}	
	
}
