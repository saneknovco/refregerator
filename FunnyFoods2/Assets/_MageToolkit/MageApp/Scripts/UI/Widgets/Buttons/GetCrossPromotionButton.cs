﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using Mage.Effects;

[DisallowMultipleComponent]
public class GetCrossPromotionButton : ButtonWidget {
	[FullInspector.InspectorCategory("Action")]
	[FullInspector.InspectorHeader("Настройки создания UIView")]
	[FullInspector.InspectorName("UIContainer")]
	[FullInspector.InspectorTooltip("Родительский объект для загружаемого префаба UIView")]
	public GameObject UIContainer;
	[FullInspector.InspectorCategory("Action")]
	[FullInspector.InspectorName("Auto Bind UIContainer")]
	public bool autoBindUIContainer = true;
	private bool isUIContainer { get { return (UIContainer != null) || autoBindUIContainer; } }

	//[FullInspector.InspectorCategory("Action")]
	//[FullInspector.InspectorComment("Список объектов которые отключаются при показе UIView Cross-promotion (для повышения fps)")]
	//public GameObject[] deactivateObjects = new GameObject[0];

	[FullInspector.InspectorCategory("Action")]
	[FullInspector.InspectorComment(FullInspector.CommentType.None, "Эффект перехода")]
	public bool fading = true;
	[FullInspector.InspectorCategory("Action"), FullInspector.InspectorShowIf("fading")]
	public float fadingOutTime = 0.5f;
	[FullInspector.InspectorCategory("Action"), FullInspector.InspectorShowIf("fading")]
	public float fadingInTime = 0.5f;

	[FullInspector.InspectorCategory("Action"), FullInspector.InspectorShowIf("usingPlayMaker")]
	[FullInspector.InspectorTooltip("Отпралять сообщение в PM: " + AppState.PMEvents.cpOpenUIView)]
	public bool broadcastDoTouchPMEvent = false;

    [FullInspector.InspectorCategory("Action")]    
    [FullInspector.InspectorComment(FullInspector.CommentType.None, "Parental Gate на банере для других языков")]
    public bool useParentalGateForNoneSupportLang = true;

    private AppState _appState;

	private void OnShowUIView(bool isShow)
	{
		if (isShow == false && widgetSettings.canExecute == false)
		{			
			widgetSettings.canExecute = true;			
		}
	}

	protected void OnActiveMainObjects(bool isActive)
	{
		//// выключаем до деактивации объектов
		//if (!isActive)
		//{
		//	if (Messenger.eventTable.ContainsKey(AppState.EventTypes.uiEnableMainScenario))
		//		Messenger.Broadcast<bool>(AppState.EventTypes.uiEnableMainScenario, false);
		//}
		//if (deactivateObjects != null)
		//{
		//	for (int i = 0; i < deactivateObjects.Length; i++)
		//		if (deactivateObjects[i]!=null) deactivateObjects[i].SetActive(isActive);
		//}

		////включаем после активации объектов
		//if (isActive)
		//{
		//	if (Messenger.eventTable.ContainsKey(AppState.EventTypes.uiEnableMainScenario))
		//		Messenger.Broadcast<bool>(AppState.EventTypes.uiEnableMainScenario, true);
		//}
	}

	void OnAfterCloseUIView()
	{
		widgetSettings.canExecute = true;		
	}


	protected void OnAction()
	{
		widgetSettings.canExecute = false;
#if MAGE_ANALYTICS
		UnityEngine.Analytics.Analytics.CustomEvent(this.GetType().Name, new Dictionary<string, object>
			{
				{"action", "open"}
			});
#endif

#if !MAGE_NO_PM
		if (broadcastDoTouchPMEvent) 
			PlayMakerFSM.BroadcastEvent(AppState.PMEvents.cpOpenUIView);
#endif
		if (fading == false)
		{
			if (Messenger.eventTable.ContainsKey(AppState.EventTypes.cpCreateUIView))
				Messenger.Broadcast<GameObject>(AppState.EventTypes.cpCreateUIView, UIContainer);
			return;
		}

		MgScreenFaderController.Run(MageAppFadingMode.OutContinued, fadingOutTime);
		StartCoroutine(RoutineAction());
	}

	IEnumerator RoutineAction()
	{
		yield return new WaitForSeconds(fadingOutTime + 0.1f);
		if (Messenger.eventTable.ContainsKey(AppState.EventTypes.cpCreateUIView))
				Messenger.Broadcast<GameObject>(AppState.EventTypes.cpCreateUIView, UIContainer);
		yield return new WaitForSeconds(0.1f);
		MgScreenFaderController.Run(MageAppFadingMode.In, fadingInTime);			
	}

	protected override void DoAwake()
	{
		base.DoAwake();

		_appState = AppState.instance;		
		if (UIContainer == null && autoBindUIContainer) UIContainer = _appState.UIContainer;
		
		Messenger.AddListener<bool>(AppState.EventTypes.cpShowUIView, OnShowUIView);
		Messenger.AddListener(AppState.EventTypes.cpAfterCloseUIView, OnAfterCloseUIView);		
		Messenger.AddListener<bool>(AppState.EventTypes.uiActiveMainSceneObjects, OnActiveMainObjects);

	}

	protected override bool DoBreakTouch()
	{
		bool isBreak = (!widgetSettings.canExecute || _appState.IsShowUIView || _appState.lockWidgets);
		return isBreak;
	}

	protected override void DoTouch()
	{
		if (!widgetSettings.canExecute || _appState.IsShowUIView || _appState.lockWidgets) return;

#if MAGE_ANALYTICS
		UnityEngine.Analytics.Analytics.CustomEvent(this.GetType().Name, new Dictionary<string, object>
			{
				{"action", "touch"}
			});
#endif

#if UNITY_ANDROID
		var lang = _appState.CurrentLang;
		if(!MgCommonUtils.IsFullSupportLang(lang))
		{
            if (useParentalGateForNoneSupportLang)
            {
                _appState.parentalGateSvc.CreateUIView(
                    p =>
                    {
#if MAGE_ANALYTICS
		            	UnityEngine.Analytics.Analytics.CustomEvent(this.GetType().Name, new Dictionary<string, object>
				        {
					        {"action", "openStore"}
				        });
#endif
                        Application.OpenURL(AppSettings.instance.appInfo.GetAppsDevUri());

                    }
                );
            }
            else
            {
#if MAGE_ANALYTICS
        		UnityEngine.Analytics.Analytics.CustomEvent(this.GetType().Name, new Dictionary<string, object>
				{
					{"action", "openStore"}
				});
#endif
                Application.OpenURL(AppSettings.instance.appInfo.GetAppsDevUri());
            }           
			return;
		}
#endif
		if(CPStateController.instance.IsEmptyAppListForCurrentLang)
		{
			if (useParentalGateForNoneSupportLang)
			{
				_appState.parentalGateSvc.CreateUIView(
					p =>
					{
					#if MAGE_ANALYTICS
					UnityEngine.Analytics.Analytics.CustomEvent(this.GetType().Name, new Dictionary<string, object>
					                                            {
						{"action", "openStore"}
					});
					#endif
					Application.OpenURL(AppSettings.instance.appInfo.GetAppsDevUri());
					
				}
				);
			}
			else
			{
				#if MAGE_ANALYTICS
				UnityEngine.Analytics.Analytics.CustomEvent(this.GetType().Name, new Dictionary<string, object>
				                                            {
					{"action", "openStore"}
				});
				#endif
				Application.OpenURL(AppSettings.instance.appInfo.GetAppsDevUri());
			}           
			return;
		}		
	
        if (Messenger.eventTable.ContainsKey(AppState.EventTypes.cpBeforeCreateUIView))
			Messenger.Broadcast(AppState.EventTypes.cpBeforeCreateUIView);

		if (widgetSettings.parentalGate)
		{
			_appState.parentalGateSvc.CreateUIView(
			   p => { OnAction(); },
			   r =>
			   {
				   if (Messenger.eventTable.ContainsKey(AppState.EventTypes.cpCancelCreateUIView))
					   Messenger.Broadcast(AppState.EventTypes.cpCancelCreateUIView);
			   }
		   );
		}
		else OnAction();
	}

	protected override void DoDestroy()
	{
		if (Messenger.eventTable.ContainsKey(AppState.EventTypes.cpShowUIView))
			Messenger.RemoveListener<bool>(AppState.EventTypes.cpShowUIView, OnShowUIView);
		if (Messenger.eventTable.ContainsKey(AppState.EventTypes.cpAfterCloseUIView))
			Messenger.RemoveListener(AppState.EventTypes.cpAfterCloseUIView, OnAfterCloseUIView);
		if (Messenger.eventTable.ContainsKey(AppState.EventTypes.uiActiveMainSceneObjects))
			Messenger.RemoveListener<bool>(AppState.EventTypes.uiActiveMainSceneObjects, OnActiveMainObjects);
	}
}