﻿using UnityEngine;

[DisallowMultipleComponent]
public class SupportButton : BaseOpenUrlButton
{

    private string _subject = string.Empty; 
    private string _body = string.Empty;   

    public override string GetUri()
	{        
        if (string.IsNullOrEmpty(_subject))
        {
            _subject = "Support for : " + BundleInfoBindings.bundleProductName;             
            _subject = System.Uri.EscapeDataString(_subject);
        }
        if (string.IsNullOrEmpty(_body))
        {
            string newLine = System.Environment.NewLine;
            string newLine2 = ";" + newLine;
			string separatorTechMessage = "--------------------------------------";

            var appInfo = AppSettings.instance.appInfo;
           
            System.Text.StringBuilder builder = new System.Text.StringBuilder();

            builder.Append( separatorTechMessage + newLine );
            {

                if (AppState.instance.CurrentLang == MageAppUILang.ru)
                    builder.Append(@"ТЕХНИЧЕСКАЯ ИНФОРМАЦИЯ ДЛЯ СЛУЖБЫ ПОДДЕРЖКИ:" + newLine);
                else
                    builder.Append(@"TECHNICAL INFORMATION FOR SUPPORT:" + newLine);

                builder.Append(" - BUNDLE: " + BundleInfoBindings.bundleIdentifier + newLine2);
                builder.Append(" - INTERNAL_NAME: " + appInfo.bundleIdentifier + " (" + appInfo.bundleVersion + ")" + newLine2);
                builder.Append(" - OS: " + SystemInfo.operatingSystem).Append("; " + SystemInfo.deviceModel + newLine2);
                builder.Append(" - CPU: " + SystemInfo.processorType + newLine2);
                builder.Append(" - GPU: " + SystemInfo.graphicsDeviceName).Append("; GPU_VER: " + SystemInfo.graphicsDeviceVersion + newLine2);
                builder.Append(" - RAM: " + SystemInfo.systemMemorySize).Append("; GPU_RAM: " + SystemInfo.graphicsMemorySize + newLine2);
                builder.Append(" - UNITY_VER: " + Application.unityVersion + newLine2);
#if (UNITY_IOS || UNITY_TVOS) && !MAGE_NO_IAP
                // Just iOS with IAP
                builder.Append(@" - DUID_" + SystemInfo.deviceUniqueIdentifier + newLine2);
#endif
            }
			builder.Append( separatorTechMessage );

			if (AppState.instance.CurrentLang == MageAppUILang.ru)
				builder.Append(newLine + @"ТЕКСТ СООБЩЕНИЯ:" + newLine + newLine);			
			else
				builder.Append(newLine+ @"ENTER YOUR MESSAGE:" + newLine + newLine);
			
            _body = builder.ToString();                
            _body = System.Uri.EscapeDataString(_body);
        }        
        return "mailto:" + AppConfig.emailFeedback + "?subject=" + _subject + "&body=" + _body;
	}	
}

