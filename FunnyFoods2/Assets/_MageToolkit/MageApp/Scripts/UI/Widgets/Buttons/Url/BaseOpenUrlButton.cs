﻿using UnityEngine;
using System.Collections.Generic;

/// <summary>
/// базовый класс для button beheviour  реализующих переход по ссылке (Application.OpenURL)
/// </summary>
public class BaseOpenUrlButton : ButtonWidget 
{
	[FullInspector.InspectorMargin(16)]
	[FullInspector.InspectorCategory("Action")]
	[FullInspector.InspectorTooltip("Если false то позволяет блокировать обработку Touch через AppState.Instance.lockWidgets")]
	public bool ignoreLockWidget = false;

	public virtual string GetUri()
	{
		return string.Empty;
	}

#if MAGE_ANALYTICS
    private string _fullName = "empty";
    protected override void DoAwake()
    {
        _fullName = gameObject.GetFullName();
        UnityEngine.Analytics.Analytics.CustomEvent(this.GetType().Name, new Dictionary<string, object>
            {
                {"action", "create"},
                {"sender", _fullName}
            });
        //Debug.Log("event " + this.GetType().Name);
        //Debug.Log("source " + _fullName);
    }

    protected override void DoDestroy()
    {
        UnityEngine.Analytics.Analytics.CustomEvent(this.GetType().Name, new Dictionary<string, object>
            {
                {"action", "destroy"},
                {"sender", _fullName}
            });
    }
#endif

    protected override bool DoBreakTouch()
	{
		var result = (!ignoreLockWidget && AppState.instance.lockWidgets) || !widgetSettings.canExecute;
		return result;
	}

    protected override void DoTouch()
    {
        var uri = GetUri();
        if (string.IsNullOrEmpty(uri)) return;

#if MAGE_ANALYTICS
        UnityEngine.Analytics.Analytics.CustomEvent(this.GetType().Name, new Dictionary<string, object>
            {
                {"action", "touch"},
                {"sender", _fullName}
            });
#endif
        if (widgetSettings.parentalGate)
        {
            AppState.instance.parentalGateSvc.CreateUIView(p =>
            {
#if MAGE_ANALYTICS
                UnityEngine.Analytics.Analytics.CustomEvent(this.GetType().Name, new Dictionary<string, object>
                    {
                        {"action", "open"},
                        {"sender", _fullName}
                    });
#endif
                MgCommonUtils.OpenUriWithAudit(this, uri);
            });
        }
        else
        {
#if MAGE_ANALYTICS
            UnityEngine.Analytics.Analytics.CustomEvent(this.GetType().Name, new Dictionary<string, object>
                {
                    {"action", "open"},
                    {"sender", _fullName}
                });
#endif
            MgCommonUtils.OpenUriWithAudit(this, uri);
        }
	}
}
