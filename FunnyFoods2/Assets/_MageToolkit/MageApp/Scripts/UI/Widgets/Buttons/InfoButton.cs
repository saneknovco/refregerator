﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using Mage.Effects;

[DisallowMultipleComponent]
public class InfoButton :  ButtonWidget  {

	[System.Serializable]
	public class InfoButtonSettings : FullInspector.BaseObject
	{
		[FullInspector.InspectorOrder(1)]
		public bool customInfoUIView = true;
		[FullInspector.InspectorShowIf("customInfoUIView")]
		[FullInspector.InspectorComment(FullInspector.CommentType.Info, 
			"Необходимо сбрасывать значение флага infoUIViewMode при закрытии infoUIView, например скриптом CloseUIView")]
		[FullInspector.InspectorOrder(2)]
		public MageAppUIViewMode infoUIViewMode = MageAppUIViewMode.InfoView;		

		[SerializeField]
		[FullInspector.InspectorOrder(5.5), FullInspector.InspectorHidePrimary]
		private Dictionary<MageAppUILang, string> _uriPrefabs;
		[FullInspector.InspectorShowIf("customInfoUIView")]
		[FullInspector.InspectorOrder(3), FullInspector.ShowInInspector]
		public Dictionary<MageAppUILang, string> UriPrefabs
		{
			get
			{
				if (_uriPrefabs == null || _uriPrefabs.Count < 1)
				{
					_uriPrefabs = new Dictionary<MageAppUILang, string>()
                    {                       
						{MageAppUILang.en, @"UI/InfoUIViewControl"},
                        {MageAppUILang.ru, @"UI/InfoUIViewControl"}
                    };
				}
				return _uriPrefabs;
			}
			set { _uriPrefabs = value; }
		}
		[FullInspector.InspectorOrder(4)]
		public GameObject parentUIView = null;
		[FullInspector.InspectorOrder(5.1f)]
		public float fadingOutTime = 0.5f;
		[FullInspector.InspectorOrder(5.2f)]
		public float fadingInTime = 0.5f;
	}

	[FullInspector.InspectorCategory("Action")]
	[FullInspector.InspectorHeader("Настройки InfoView"), FullInspector.InspectorMargin(18)]
	public InfoButtonSettings infoSettings;  

	private bool CreateUIView(GameObject parentUIView)
	{
		var dic =infoSettings.UriPrefabs;
		string uri = dic.SafeGetByDefKey(AppState.instance.CurrentLang, AppSettings.instance.UI.defaultLang);	
		if (Screen.width < Screen.height) uri += "_P";
	
		var go = Resources.Load(uri) as GameObject;
		if (go == null) return false;

		var viewInstance = GameObject.Instantiate(go) as GameObject;
		AppState.instance.UIViewMode |= infoSettings.infoUIViewMode;
		viewInstance.name = go.name;
		if (parentUIView)
		{
			viewInstance.transform.parent = parentUIView.transform;
			viewInstance.transform.localPosition = go.transform.localPosition;
			viewInstance.transform.localScale = go.transform.localScale;
		}
		return true;
	}
	protected void OnAction()
	{
		MgScreenFaderController.Run(MageAppFadingMode.OutContinued, infoSettings.fadingOutTime);
		StartCoroutine(MgCoroutineHelper.WaitThenCallback(infoSettings.fadingOutTime + 0.25f, () =>
		{
			if (infoSettings.customInfoUIView)
			{
				CreateUIView(infoSettings.parentUIView);
			}
			else
			{
				if (Messenger.eventTable.ContainsKey(AppState.EventTypes.cpCreateUIView))
					Messenger.Broadcast<GameObject>(AppState.EventTypes.cpCreateUIView, infoSettings.parentUIView);
			}
			MgScreenFaderController.Run(MageAppFadingMode.In, infoSettings.fadingInTime);
		}));	
	}


	protected override void DoStart()
	{
		if (infoSettings.parentUIView == null)
		{
			var go = AppState.instance.UIContainer;
			if (go) infoSettings.parentUIView = go;
		}
	}

	protected override bool DoBreakTouch ()
	{
		return (!widgetSettings.canExecute || AppState.instance.IsShowUIView || AppState.instance.lockWidgets);    
	}

	protected override void DoTouch ()
	{
       
		if (Messenger.eventTable.ContainsKey(AppState.EventTypes.cpStampAction))
			Messenger.Broadcast<CPStampAction, bool>(AppState.EventTypes.cpStampAction, CPStampAction.Show, false);

		if (widgetSettings.parentalGate)
		{
			AppState.instance.parentalGateSvc.CreateUIView(
				p => { 
				OnAction(); 
			},
			r =>
			{
				//Cancel
				if (Messenger.eventTable.ContainsKey(AppState.EventTypes.cpStampAction))
					Messenger.Broadcast<CPStampAction, bool>(AppState.EventTypes.cpStampAction, CPStampAction.Show, true);
			}
			);
		}
		else OnAction();
	}
	
}
