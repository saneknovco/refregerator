﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

[DisallowMultipleComponent]
public class AudioModeButton : ButtonWidget
{ 
	[FullInspector.InspectorCategory("Action")]
	public string  uriAudioObject;
	[FullInspector.InspectorCategory("Action")]
    [FullInspector.InspectorComment("Время в сек. через которое запускается музыка при первом старте сцены")]
    public float delayStartAudio = 0.5f;
	[FullInspector.InspectorCategory("Action")]
    public float pauseTimeWait = 15.0f;
    [FullInspector.InspectorDivider]    
	[FullInspector.InspectorCategory("Action")]
    public Texture2D off;
	[FullInspector.InspectorCategory("Action")]
    public Texture2D on;

    private int _mode = 1;

	private Renderer _widgetRenderer = null;

    private static AudioSource _audio = null;
    private static bool isFirstRun = true;
    private static System.DateTime _startTime = System.DateTime.Now;

    protected override bool DoBreakTouch()
    {
        return (AppState.instance.IsShowUIView || AppState.instance.lockWidgets);
    }  

	private bool PreparedAudio
	{
		get {
			if (_audio == null || _widgetRenderer == null) return false;
			if (_audio.clip == null) return false;
			if (string.IsNullOrEmpty(_audio.clip.name)) return false;
			return true;
		}
	}

    protected override void DoTouch()
    {
        _mode = (_mode + 1) % 2;
				
		if (!PreparedAudio) return;

		if (_mode == 1) _widgetRenderer.material.mainTexture = on;
		else _widgetRenderer.material.mainTexture = off;

		PlayerPrefs.SetInt(_audio.clip.name, _mode);
		PlayerPrefs.Save();
		if (_mode == 1)
		{
			if (pauseTimeWait < (float)(System.DateTime.Now - _startTime).TotalSeconds)
			{
				_audio.Stop();
			}
			_audio.Play();
		}
		else
		{
			_audio.Pause();
			_startTime = System.DateTime.Now;
		}
    }

    protected override  void DoStart()
    {
        base.DoStart();
		_widgetRenderer = widgetSettings.widget.GetComponent<Renderer>();
        if (_audio == null)
        {
            var audioObject = Resources.Load(uriAudioObject) as GameObject;
            if (audioObject != null)
            {
                var nameObj = AppConfig.prefixFVTAutoObject + audioObject.name;
                audioObject = GameObject.Instantiate(audioObject) as GameObject;
                audioObject.name = nameObj;
                GameObject.DontDestroyOnLoad(audioObject);
                _audio = audioObject.GetComponent<AudioSource>();
            }  
        }
		if (!PreparedAudio) return;
		_mode = PlayerPrefs.GetInt(_audio.clip.name, 1); 
		if (_mode == 1) _widgetRenderer.material.mainTexture = on;
		else _widgetRenderer.material.mainTexture = off;
        if (isFirstRun)
        {
            _startTime = System.DateTime.Now;
            isFirstRun = false;
			if (_mode == 1)
			{
				if (delayStartAudio > 0.01f)
					StartCoroutine(MgCoroutineHelper.WaitThenCallback(delayStartAudio,
						() => { _audio.Play(); }));
				else _audio.Play();
			}
        }
    }
}
