﻿using UnityEngine;
using System.Collections;

[DisallowMultipleComponent]
public class ReviewButton : BaseOpenUrlButton
{

#if UNITY_ANDROID	
	#if UNITY_EDITOR
	const string UrlRate = @"https://play.google.com/store/apps/details?id={0}&reviewId=0";
	#else
	const string UrlRate = @"market://details?id={0}&reviewId=0";
	#endif
#else
	const string UrlRate = @"http://itunes.apple.com/WebObjects/MZStore.woa/wa/viewContentsUserReviews?pageNumber=0&sortOrdering=1&type=Purple+Software&mt=8&id={0}";
#endif

	public override string GetUri()
	{	
		return string.Format(UrlRate, AppSettings.instance.appInfo.appId);
	}
}
