﻿using UnityEngine;
using System.Collections;

public class UrlHomeButton : BaseOpenUrlButton 
{
    public override string GetUri()
    {
        return AppSettings.instance.UI.UrlHome;
    }
	
}
