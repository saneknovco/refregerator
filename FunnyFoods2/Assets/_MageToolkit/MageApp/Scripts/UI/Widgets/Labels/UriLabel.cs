﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class UriLabel : MonoBehaviour {

	[FullInspector.ShowInInspector]
	[FullInspector.InspectorHeader("Список uri для платформ"), FullInspector.InspectorMargin(16)]
	public UriLabelSettings uriSettings;		
	
	public string GetUri()
	{
#if UNITY_EDITOR
		var platform = MgUnityHelper.GetRuntimePlatformByActiveBuildTarget();
		return uriSettings.uri.SafeGet(platform, uriSettings.defaultUri);
#else
		return uriSettings.uri.SafeGet(Application.platform, uriSettings.defaultUri);
#endif
	}

	#region Nested types

	[System.Serializable]
	public class UriLabelSettings: FullInspector.BaseObject
	{		
		[FullInspector.InspectorMargin(12)]
		[FullInspector.InspectorOrder(1)]
		public string defaultUri;

		[SerializeField]
		[FullInspector.InspectorHidePrimary]
		private Dictionary<RuntimePlatform, string> _uri;
		[FullInspector.ShowInInspector]		
		[FullInspector.InspectorOrder(2)]
		public Dictionary<RuntimePlatform, string> uri
		{
			get
			{
				if (_uri == null) _uri = new Dictionary<RuntimePlatform, string>();
				if (_uri.Count < 1)
				{
					_uri = new Dictionary<RuntimePlatform, string>() {
						{RuntimePlatform.IPhonePlayer, defaultUri},
						{RuntimePlatform.Android, defaultUri}
					};
				}
				return _uri;
			}
			private set { _uri = value; }
		}
	}

	#endregion

}
