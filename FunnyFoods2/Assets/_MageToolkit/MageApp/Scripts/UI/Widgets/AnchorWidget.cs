﻿using UnityEngine;
using System.Collections.Generic;

public class AnchorWidget : FullInspector.BaseBehavior {

	[System.Serializable]
    public enum AnchorType
    {
        None,
        TopLeft,
        TopMidle,
        TopRight,
        BottomLeft,
        BottomMidle,
        BottomRight,
        Center,
        LeftMidle,
        RightMidle,
        RelativeLeft,
        RelativeRight,
        RelativeBottom,
        RelativeTop
    }

    [FullInspector.InspectorOrder(1),
    FullInspector.InspectorTooltip("кол-во кадров ожидания перед изменением размера. Hеобходим для синхронизации с корректным значением камеры!")]
    public int waitFrameToResize = 1;
    [FullInspector.InspectorOrder(1.1)]
    public GameObject relativeWidget = null;
    [FullInspector.InspectorOrder(2)]
    public AnchorType anchor = AnchorType.TopRight;   
	[SerializeField, HideInInspector]
    private  Dictionary<MageAppAspectRatio, Vector3> _offset;
    [FullInspector.InspectorOrder(3), FullInspector.ShowInInspector]
    public Dictionary<MageAppAspectRatio, Vector3> offset
    {
        get 
        {
           if (_offset == null)_offset = new Dictionary<MageAppAspectRatio, Vector3>();
            if (!_offset.ContainsKey(MageAppAspectRatio.AspectOthers))
                _offset.Add(MageAppAspectRatio.AspectOthers, Vector3.zero);
            return _offset;
        }
        set { _offset = value;}
    }

	public bool applyScaleCoef = false;
	[SerializeField, HideInInspector]
	private  Dictionary<MageAppAspectRatio, Vector3> _scaleCoef;
	[FullInspector.InspectorOrder(3), FullInspector.ShowInInspector, FullInspector.InspectorShowIf("applyScaleCoef")]
	public Dictionary<MageAppAspectRatio, Vector3> scaleCoef
	{
		get 
		{
			if (_scaleCoef == null)_scaleCoef = new Dictionary<MageAppAspectRatio, Vector3>();
			if (!_scaleCoef.ContainsKey(MageAppAspectRatio.AspectOthers))
				_scaleCoef.Add(MageAppAspectRatio.AspectOthers, Vector3.one);
			return _scaleCoef;
		}
		set { _scaleCoef = value;}
	}

    private Camera _workCamera = null;
    [FullInspector.InspectorOrder(4)]
    public Camera WorkCamera
    {
        get 
        { 
            if(_workCamera == null) _workCamera = Camera.main;
            return _workCamera;
        }
        private set 
        { 
            _workCamera = value;
        }
    }

    private bool __isResize = false;
    private void _ResizeWidget()
    {
        Vector3 pos;
        MageAppAspectRatio asp = MgCommonUtils.GetAspectRatio(WorkCamera);
        if (!offset.ContainsKey(asp)) asp = MageAppAspectRatio.AspectOthers;
        var offs = offset[asp];       
        var v = transform.position;
        Vector3 p = Vector3.zero;
        AnchorWidget relativeCmp = null;
        Renderer rnd = GetComponent<Renderer>();
        Renderer relativeRnd = null; 
        if (relativeWidget)
        {
            relativeCmp = relativeWidget.GetComponent<AnchorWidget>();
            if (relativeCmp)
            {
                relativeCmp.ResizeWidget();
                relativeRnd = relativeWidget.GetComponent<Renderer>();
            }
            p = relativeWidget.transform.position;            
        }

        if (rnd)// якорь только для объектов с renderer-ом
        {
            switch (anchor)
            {
                case AnchorType.None:
                    break;
                case AnchorType.BottomLeft:
                    pos = WorkCamera.ScreenToWorldPoint(Vector3.zero);
                    v.x = pos.x + 0.5f * rnd.bounds.size.x;
                    v.y = pos.y + 0.5f * rnd.bounds.size.y;
                    break;
                case AnchorType.BottomMidle:
                    pos = WorkCamera.ScreenToWorldPoint((0.5f * Screen.width) * Vector3.right);
                    v.x = pos.x;
                    v.y = pos.y + 0.5f * rnd.bounds.size.y;
                    break;

                case AnchorType.BottomRight:
                    pos = WorkCamera.ScreenToWorldPoint((float)Screen.width * Vector3.right);
                    v.x = pos.x - 0.5f * rnd.bounds.size.x;
                    v.y = pos.y + 0.5f * rnd.bounds.size.y;
                    break;

                case AnchorType.TopLeft:
                    pos = WorkCamera.ScreenToWorldPoint((float)Screen.height * Vector3.up);
                    v.x = pos.x + 0.5f * rnd.bounds.size.x;
                    v.y = pos.y - 0.5f * rnd.bounds.size.y;
                    break;

                case AnchorType.TopMidle:
                    pos = WorkCamera.ScreenToWorldPoint(new Vector3(0.5f * Screen.width, Screen.height, 0.0f));
                    v.x = pos.x;
                    v.y = pos.y - 0.5f * rnd.bounds.size.y;
                    break;

                case AnchorType.TopRight:
                    pos = WorkCamera.ScreenToWorldPoint(new Vector3(Screen.width, Screen.height, 0.0f));
                    v.x = pos.x - 0.5f * rnd.bounds.size.x;
                    v.y = pos.y - 0.5f * rnd.bounds.size.y;
                    break;
                case AnchorType.Center:
                    pos = WorkCamera.ScreenToWorldPoint(new Vector3(0.5f * Screen.width, 0.5f * Screen.height, 0.0f));
                    v.x = pos.x;
                    v.y = pos.y;
                    break;

                case AnchorType.LeftMidle:
                    pos = WorkCamera.ScreenToWorldPoint((0.5f * Screen.height) * Vector3.up);
                    v.x = pos.x + 0.5f * rnd.bounds.size.x;
                    v.y = pos.y;
                    break;

                case AnchorType.RightMidle:
                    pos = WorkCamera.ScreenToWorldPoint(new Vector3(Screen.width, 0.5f * Screen.height, 0.0f));
                    v.x = pos.x - 0.5f * rnd.bounds.size.x;
                    v.y = pos.y;
                    break;

                case AnchorType.RelativeLeft:
                    if (relativeWidget)
                    {
                        v.x = p.x + 0.5f * (relativeRnd.bounds.size.x + rnd.bounds.size.x);
                        v.y = p.y;
                    }
                    break;

                case AnchorType.RelativeRight:
                    if (relativeWidget && relativeRnd)
                    {
                        v.x = p.x - 0.5f * (relativeRnd.bounds.size.x + rnd.bounds.size.x);
                        v.y = p.y;
                    }
                    break;

                case AnchorType.RelativeBottom:
                    if (relativeWidget && relativeRnd)
                    {
                        v.x = p.x;
                        v.y = p.y - 0.5f * (relativeRnd.bounds.size.y + rnd.bounds.size.y);
                    }
                    break;

                case AnchorType.RelativeTop:
                    if (relativeWidget && relativeRnd)
                    {
                        v.x = p.x;
                        v.y = p.y + 0.5f * (relativeRnd.bounds.size.y + rnd.bounds.size.y);
                    }
                    break;

                default:
                    break;
            }
        }

        transform.position = v;
        transform.localPosition = transform.localPosition + offs;
		if(applyScaleCoef)
		{
			asp = MgCommonUtils.GetAspectRatio(WorkCamera);
			if (!scaleCoef.ContainsKey(asp)) asp = MageAppAspectRatio.AspectOthers;
			var scl = scaleCoef[asp];   
			transform.localScale = Vector3.Scale(transform.localScale, scl);
		}
        __isResize = true;
    }

    public void ResizeWidget()
    {
        if (__isResize) return;
        _ResizeWidget();      
    }   

	void Start () 
    {        
        StartCoroutine(MgCoroutineHelper.WaitThenCallback(waitFrameToResize, _ResizeWidget));
	}    
}
