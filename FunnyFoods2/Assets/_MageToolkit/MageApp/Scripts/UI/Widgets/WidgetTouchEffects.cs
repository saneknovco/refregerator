﻿using UnityEngine;

[System.Serializable]
public class TouchEffectParameters
{
    #region Wobble effect
    public float duration = 0.3f;
    public int cycles = 1;
    public float powerDecay = 1f;
    public float amplitude = 0.05f;
    public Vector3 extraScale = Vector3.one;
    #endregion
    #region Swirl
    public float speed = 1.0f;
    #endregion

    public Vector3 selectedScale = new Vector3(1.01f, 1.01f, 1f);

	public Vector3 initialScale = Vector3.zero;

	//Preset 0 :  {0.6, 3,1 0.05} 
    //Preset 1 (small jelly button): {0.5,5,1 0.125} 
    //Preset 2 (  button): {0.3, 2, 3,  0.125} 
    //Preset 3 (  BIG button): {0.3, 2, 3,  0.05} 
    //preset 4 (smart button) {0.25, 1, 3, 0.125}
    //preset 5 (smart big button) {0.25, 1, 3, 0.07}
    //preset 6 (smart big button) {0.25, 1, 3, 0.05}

    /// <summary>
    /// ЭФФЕКТ БОУНСИНГА
    /// </summary>
    /// <param name="t"></param>
    /// <param name="scale"></param>
    /// <returns></returns>
    public bool WobbleReaction(float t, ref Vector3 scale)
    {
        if (t < 1f){
            float phase = (0.5f + t * (cycles + 0.5f)) * Mathf.PI;
            float tp = Mathf.Pow(t, powerDecay);
            float a = (amplitude * (1f - tp)) * Mathf.Sin(phase);
            scale.x *= (1f + a);
            scale.y *= (1f - a);            
            return true;
        }
        else return false;
    }

    /// <summary>
    /// эффект вращения
    /// </summary>
    /// <param name="t"></param>
    /// <param name="eulerAngles"></param>
    /// <returns></returns>
    public bool SwirlReaction(float t, ref Vector3 eulerAngles)
    {
        if (t < 1f)
        {
            float tp = Mathf.Pow(t, powerDecay);
            float delta = speed * (1f - tp) * Time.smoothDeltaTime;
            var angles = eulerAngles;
            angles.z += delta;
            eulerAngles = angles;
            return true;
        }
        else return false;
    }
}
