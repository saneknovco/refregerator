﻿using UnityEngine;
using System.Collections;

public class BaseSelectedButtonWidget : MonoBehaviour
{
    public Renderer renderObject;
    public Color selectingColor = new Color(0.5f,0.5f,0.5f,1.0f);
    public int waitFrameBeforeInit = 0;

    Material _mat;
    Color _baseColor = Color.white;
    bool _isLeaveMouse = false;

    //protected static bool __isTouchAction = false;
    //public static void ResetTouchInProcess()
    //{
    //    __isTouchAction = false;
    //}

    protected void SetSelectedState(bool isLeaveMouse)
    {
       
        _isLeaveMouse = isLeaveMouse;

        if (_mat == null) return;
        if (!_mat.HasProperty("_Color")) return;

        if (isLeaveMouse)
        {
            _mat.SetColor("_Color", selectingColor);           
        }
        else
        {
            _mat.SetColor("_Color", _baseColor);
        }
    }

    protected virtual void DoAction() { }

    void TouchAction()
    {
        MgAudioHelper.Click();
        DoAction();
        SetSelectedState(false);
    }

    private void Init()
    {
        if (renderObject == null)
        {
            renderObject = this.GetComponent<Renderer>();
        }
        if (renderObject != null)
        {
#if UNITY_EDITOR
            _mat = renderObject.sharedMaterial;
#else
            _mat = renderObject.material;
#endif
        }
        else _mat = null;

        if (_mat != null)
        {
            if (_mat.HasProperty("_Color"))
                _baseColor = _mat.GetColor("_Color");
        }
    }

    // Use this for initialization
    protected virtual void Awake ()
    {
        if (waitFrameBeforeInit <= 0) Init();
        else StartCoroutine(MgCoroutineHelper.WaitThenCallback(waitFrameBeforeInit, () => { Init(); }));
    } 

#if MAGE_MT
    void OnMouseDownMT()
#else
    void OnMouseDown()
#endif
    {
        SetSelectedState(true);
    }

#if MAGE_MT
    void OnMouseExitMT()
#else
    void OnMouseExit()
#endif
    {
        SetSelectedState(false);
    }

#if MAGE_MT
	void OnMouseUpAsButtonMT()
#else
    void OnMouseUpAsButton ()
#endif
    {
        if (_isLeaveMouse ) TouchAction();
    }
}
