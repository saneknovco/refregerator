﻿using UnityEngine;
using System.Collections;

public class ButtonWidgetStd : MonoBehaviour
{
	[System.Serializable]
	public class ButtonWidgetSettings : FullInspector.BaseObject
	{
		[FullInspector.InspectorOrder(1)]
		public GameObject widget = null;
		[FullInspector.InspectorOrder(1.1)]
		public bool parentalGate = true;
		[FullInspector.InspectorOrder(1.2)]
		public bool isAudioEffect = true;
		[FullInspector.InspectorOrder(1.3)]
		public bool useMouseDown = false;

		[System.NonSerialized, HideInInspector]
		public Collider widgetCollider = null;
		[SerializeField, FullInspector.InspectorHidePrimary]
		protected bool _canExecute = true;
		[FullInspector.InspectorOrder(1.4), FullInspector.ShowInInspector]
		public bool canExecute
		{
			get { return _canExecute; }
			set
			{
				_canExecute = value;
				if (widgetCollider != null)
					widgetCollider.enabled = value;
			}
		}

		[FullInspector.InspectorOrder(1.5), FullInspector.InspectorMargin(8)]
		[FullInspector.InspectorComment("Wobble effect при нажатии")]
		public bool isTouchEffect = true;
		[FullInspector.InspectorOrder(1.6), FullInspector.InspectorShowIf("isTouchEffect")]
		public TouchEffectParameters touchEffect;// = new TouchEffectParameters();

		//[FullInspector.InspectorDivider]    
		[FullInspector.InspectorOrder(2), FullInspector.InspectorMargin(12)]
		[FullInspector.InspectorComment(FullInspector.CommentType.None, "Дополнительные параметры синхронизации touch")]
		public bool showAdvncedMode = false;

		[FullInspector.InspectorOrder(2.1), FullInspector.InspectorShowIf("showAdvncedMode")]
		public int afterTouchSkipFrames = 0;
		[FullInspector.InspectorOrder(2.2), FullInspector.InspectorShowIf("showAdvncedMode")]
		public bool ignoreMultiTouch = true;
		[FullInspector.InspectorOrder(2.3), FullInspector.InspectorShowIf("showAdvncedMode")]
		public bool asyncTouch = false;
		[FullInspector.InspectorOrder(2.35), FullInspector.InspectorShowIf("showAdvncedMode")]
		public int asyncFrameRunTouch = 2;
	}

	protected static int __countTouchInProcess = 0;
	public static void ResetTouchInProcess()
	{
		__countTouchInProcess = 0;
	}
	public static int countTouchInProcess { get { return __countTouchInProcess; } }

	public ButtonWidgetSettings widgetSettings;

	public bool isWidgetTouch
	{
		get { return _isWidgetTouch; }
	}
	public bool isWidgetTouchComplete
	{
		get { return _isWidgetTouchComplete; }
	}

	protected bool _isWidgetTouch = false;//internal flag for signal
	protected bool _isWidgetTouchComplete = true;//internal flag for signal
	protected int _countFrame = 0;

	private bool _isReaction = false;
	private float _startTime = 0f;
	private Vector3 _initialScale = Vector3.one;
	private Vector3 _selectedScale = Vector3.one;
	private Vector3 _deltaScale = Vector3.zero;
	//private float _deltaTimeScale = 0.0f;

	public void RefreshWidgetScale()
	{
		_initialScale = widgetSettings.widget.transform.localScale;
		_selectedScale = _initialScale;
		_deltaScale = Vector3.zero;
	}

	public void UpdateWidgetScale(Vector3 widgetLocalScale)
	{
		widgetSettings.widget.transform.localScale = widgetLocalScale;
		RefreshWidgetScale();
	}

	/// <summary>
	/// инициализация - call in Awake
	/// </summary>
	protected virtual void DoAwake() { }
	/// <summary>
	/// инициализация - call in Start
	/// </summary>
	protected virtual void DoStart() { }
	/// <summary>
	/// if need interrupt DoTouch
	/// </summary>
	/// <returns></returns>
	protected virtual bool DoBreakTouch() { return !widgetSettings.canExecute; }
	/// <summary>
	/// //реакция на нажатие
	/// </summary>
	protected virtual void DoTouch() { }
	/// <summary>
	/// call in OnDestroy
	/// </summary>
	protected virtual void DoDestroy() { }

	// эффект нажатия - вызывается в MouseDown/Up
	protected void Touch()
	{
		if (widgetSettings.ignoreMultiTouch && __countTouchInProcess > 0) return;
		if (_isWidgetTouch && !_isWidgetTouchComplete) return;
		if (DoBreakTouch()) return;

		if (widgetSettings.isAudioEffect) MgAudioHelper.Click();

		_isWidgetTouch = true;
		_isWidgetTouchComplete = false;
		__countTouchInProcess++;
		if (widgetSettings.isTouchEffect)
		{
			_initialScale = widgetSettings.widget.transform.localScale;
			_startTime = Time.time;
			_isReaction = true;
			if (widgetSettings.asyncTouch)
			{
				if (widgetSettings.asyncFrameRunTouch <= 0) DoTouch();
				else StartCoroutine(MgCoroutineHelper.WaitThenCallback(widgetSettings.asyncFrameRunTouch, () =>
				{
					DoTouch();
				}));
			}
		}
		else
		{
			DoTouch();
			_isWidgetTouchComplete = true;
			__countTouchInProcess--;
			if (__countTouchInProcess < 0) __countTouchInProcess = 0;
		}
	}

	protected void ReactionEffect()
	{
		//Vector3 scale = Vector3.Scale(_selectedScale, widgetSettings.touchEffect.extraScale);
		Vector3 scale = Vector3.Scale(_initialScale, widgetSettings.touchEffect.extraScale);
		float t = (Time.time - _startTime) / widgetSettings.touchEffect.duration;
		_isReaction = widgetSettings.touchEffect.WobbleReaction(t, ref scale);
		if (_isReaction)
		{
			if (widgetSettings.widget) widgetSettings.widget.transform.localScale = scale;
		}
		else
		{
			//_isWidgetTouchComplete = true;
			//if (widgetSettings.widget) widgetSettings.widget.transform.localScale = _initialScale;
			_deltaScale = widgetSettings.widget.transform.localScale - _initialScale;
			//_deltaTimeScale = 0.0f;
			if (widgetSettings.afterTouchSkipFrames > 0)
			{
				_deltaScale = (1.0f / widgetSettings.afterTouchSkipFrames) * _deltaScale;
				//_deltaTimeScale = ((float)widgetSettings.afterTouchSkipFrames / (float)Application.targetFrameRate);

			}

			if (!widgetSettings.asyncTouch) DoTouch();
			//MOD 2015.11.01
			_isWidgetTouchComplete = true;
			__countTouchInProcess--;
			if (__countTouchInProcess < 0) __countTouchInProcess = 0;       
		}
	}

	#region UNITY EVENT
	void Awake()
	{
		_countFrame = 0;
		_isWidgetTouch = false;
		_isWidgetTouchComplete = true;

		if (widgetSettings.widget == null) widgetSettings.widget = gameObject;
		widgetSettings.widgetCollider = gameObject.GetComponent<Collider>(); ;

		//========
		_initialScale = widgetSettings.widget.transform.localScale;
		_selectedScale = _initialScale;
		//========

		// for children Awake action
		DoAwake();
	}

	void Start()
	{
		_initialScale = widgetSettings.widget.transform.localScale;
		_selectedScale = _initialScale;
		DoStart();
	}

	private bool _isLeaveMouse = false;
#if MAGE_MT
	void OnMouseDownMT()
#else
	void OnMouseDown()
#endif
	{
		if (!widgetSettings.canExecute) return;
		if (widgetSettings.useMouseDown) Touch();
		else
		{
			_selectedScale = Vector3.Scale(_initialScale, widgetSettings.touchEffect.selectedScale);
			if (widgetSettings.widget) widgetSettings.widget.transform.localScale = _selectedScale;
		}
		_isLeaveMouse = true;
	}

#if MAGE_MT
	void OnMouseExitMT()
#else
	void OnMouseExit()
#endif  
	{
		if (!widgetSettings.useMouseDown && !_isReaction)
			if (widgetSettings.widget) widgetSettings.widget.transform.localScale = _initialScale;
		_isLeaveMouse = false;
	}

#if MAGE_MT
	void OnMouseUpAsButtonMT()
#else
	void OnMouseUp()
#endif    
	{
		if (!widgetSettings.useMouseDown && _isLeaveMouse)
		{
			if (widgetSettings.widget) widgetSettings.widget.transform.localScale = _initialScale;//если Touch break'
			Touch();
		}
	}

	void LateUpdate()
	{
		if (_isWidgetTouch && _isWidgetTouchComplete)
		{
			if (_countFrame >= widgetSettings.afterTouchSkipFrames)
			{
				_countFrame = 0;
				_isWidgetTouch = false;
				if (widgetSettings.widget) widgetSettings.widget.transform.localScale = _initialScale;
			}
			else
			{
				widgetSettings.widget.transform.localScale -= _deltaScale;
				//widgetSettings.widget.transform.localScale =Vector3.Slerp(widgetSettings.widget.transform.localScale, _initialScale, Time.deltaTime);
				_countFrame++;
			}
		}
		if (_isReaction) ReactionEffect();
	}

	//MOD 2015.11.01
	void OnDestroy()
	{
		if (!_isWidgetTouchComplete)
		{
			//_isWidgetTouchComplete = true;
			__countTouchInProcess--;
			if (__countTouchInProcess < 0) __countTouchInProcess = 0;
		}

		DoDestroy();
	}

	#endregion
}