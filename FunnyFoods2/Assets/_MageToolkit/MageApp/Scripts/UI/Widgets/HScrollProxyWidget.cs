using UnityEngine;
using System.Collections;
using System.Collections.Generic;

[DisallowMultipleComponent]
public class HScrollProxyWidget :MonoBehaviour
{
    const float PI05 = Mathf.PI * 0.5f;

    #region Nested types
    /// <summary>
    /// типы движения
    /// </summary>
    [System.Serializable]
    public enum TypeMoving
    {
        Power       = 0,// универсальный несимметричный закон
        PowerInOut  = 1,// универсальный симметричный закон движения
        Sin         = 2,// универсальный симметричный закон движения
        //3..6 зарезервированы для симметричных кривых типа sin

        // анимационные законы
        CircIn      = 7,
        CircOut     = 8,        
        CircInOut   = 9,

        SineIn      = 10,
        SineOut     = 11,
        SineInOut   = 12,

		BackIn		= 13,
		BackOut		= 14,
		BackInOut	= 15
    };

    /// <summary>
    /// параметры дыижения объектов в скролле
    /// </summary>
    [System.Serializable]
    public class MovingData : FullInspector.BaseObject
    {        
		private bool isPower
		{
			get { return  ( type  == TypeMoving.Power  || type == TypeMoving.PowerInOut);}
		}

		private bool isSin 
		{
			get { return  ( type  == TypeMoving.Sin );}
		}

		private bool isBack
		{
			get { return  ( type  == TypeMoving.BackIn || type == TypeMoving.BackOut || type == TypeMoving.BackInOut) ; }
		}

		public TypeMoving type = TypeMoving.CircOut;
        public float minTime = 0.4f;
        public float maxTime = 0.8f;
        public float stepTime = 0.1f;

        [FullInspector.InspectorTooltip("Степень для законов движения: 'Power'"),
		 FullInspector.InspectorShowIf("isPower")]
        public float power = 0.5f;//when TypeMoving == Power
        [FullInspector.InspectorTooltip("Амплитуда для законов движения: 'Sin'"),
		 FullInspector.InspectorShowIf("isSin")]
        public float amplitude = 1.0f;
        [FullInspector.InspectorTooltip("Коэффициент для закона движения 'Back'"),
		 FullInspector.InspectorShowIf("isBack")]
        public float backCoef = 1.5f;        
    };

    /// <summary>
    /// объекты для тапов
    /// </summary>
    [System.Serializable]
    public class TapData : FullInspector.BaseObject
    {
        public GameObject[] tapLeft;
        public GameObject[] tapCenter;
        public GameObject[] tapRight;
    };

    [System.Serializable]
    public class BoundFrameData : FullInspector.BaseObject
    {
        public Dictionary<MageAppAspectRatio, Bounds> toPanFrame;        
        public Dictionary<MageAppAspectRatio, Bounds> toTapFrame;        
    };

    [System.Serializable]
    public class HScrollSettings: FullInspector.BaseObject
    {
#if MAGE_NO_PM
        const bool usingPlayMaker = false;
#else
        const bool usingPlayMaker = true;
#endif
		[FullInspector.InspectorOrder(0.1f), FullInspector.InspectorShowIf("usingPlayMaker")]
		[FullInspector.InspectorName("PMUsingMultiplyItemsInEventPaterns")]
		[FullInspector.InspectorTooltip("playMakerUsingMultiplyItemsInEventPaterns")]
		public bool playMakerUsingMyltiplyItemsInEventPaterns = true;

        [FullInspector.InspectorOrder(0.2f), FullInspector.InspectorShowIf("usingPlayMaker")]
        [FullInspector.InspectorName("PMTapEventPatern")]
        [FullInspector.InspectorTooltip("playMakerTapEventPatern")]
        public string playMakerTapEventPatern = @"UI_SP_TAP_ACTIVE_OBJECT_";
        [FullInspector.InspectorOrder(0.3f), FullInspector.InspectorShowIf("usingPlayMaker")]
        [FullInspector.InspectorName("PMMoveItemsEventPatern")]
        [FullInspector.InspectorTooltip("playMakerMoveItemsEventPatern")]
        public string playMakerMoveItemsEventPatern = "UI_SP_MoveItems_";



		[FullInspector.InspectorOrder(0.4f), FullInspector.InspectorShowIf("sendMoveFinishedEventPM")]
		[FullInspector.InspectorName("PMMoveFinishedEvent")]
		[FullInspector.InspectorTooltip("playMakerMoveFinishedEvent")]
		public string playMakerMoveFinishedEvent = "UI_SP_MoveFinished";
        
		[FullInspector.InspectorOrder(0.9f)]
		[FullInspector.InspectorTooltip("Отпралять сообщение, когда движение завершено")]
		public bool sendMoveFinishedEvent = false;
		private bool sendMoveFinishedEventPM { get {return sendMoveFinishedEvent && usingPlayMaker; } }

        [FullInspector.InspectorOrder(1f), FullInspector.InspectorMargin(18)]
        [FullInspector.InspectorComment(FullInspector.CommentType.None, "Закон перемещения объектов")]
        [FullInspector.InspectorTooltip( "Тип и параметры анимирования перемещения объектов")]
        public MovingData moving;
      
        [FullInspector.InspectorOrder(2f), FullInspector.InspectorMargin(18)]
        [FullInspector.InspectorComment(FullInspector.CommentType.None, "Объекты для тапа")]
        [FullInspector.InspectorTooltip("Объекты для тапа")]
        public TapData tapObjects;

        [FullInspector.InspectorOrder(2.5f), FullInspector.InspectorMargin(18)]
        [FullInspector.InspectorComment(FullInspector.CommentType.None, "Области BoundaryFrame для TouchKit")]
        public BoundFrameData boundFrameData;

        [FullInspector.InspectorOrder(3f), FullInspector.InspectorMargin(18)]
        [FullInspector.InspectorComment(FullInspector.CommentType.Info, "Список объектов, коллайдеры которых создают зоны игнорируемые TouchKit")]
        [FullInspector.InspectorTooltip("Список объектов, коллайдеры которых создают зоны игнорируемые TouchKit")]
        public GameObject[] tkIgnoreBounds;
        [FullInspector.InspectorOrder(4f), FullInspector.InspectorMargin(10)]
        [FullInspector.InspectorComment(FullInspector.CommentType.Info, "Чувствительность PanTouchKit к короткому толканию для перемещения одного элемента")]
        public float thresholdShortPush = 12f;

        [FullInspector.InspectorOrder(4.1f), FullInspector.InspectorMargin(12)]
        [FullInspector.InspectorComment(FullInspector.CommentType.Info, "Коэф-нт замедления скорости swipe TouchKit")]
        public float scaleSwipeVelocity = 0.5f;
        [FullInspector.InspectorOrder(4.2f)]
        [FullInspector.InspectorComment(FullInspector.CommentType.None, "Макс. время для swipe который будет обрабатыватся")]
        public float timeSwipeReaction = 0.25f;

        [FullInspector.InspectorComment(FullInspector.CommentType.Info, "Время через которое срабатывает LongTap (пролистывание при зажатом контроле)")]
        [FullInspector.InspectorOrder(4.3f), FullInspector.InspectorMargin(12)]
        public float durationLongTap = 0.5f;
        [FullInspector.InspectorComment(FullInspector.CommentType.None, "Коэф-нт скорости движения при LongTap для TouchKit")]
        [FullInspector.InspectorOrder(4.4f)]
        public float scaleLongTapVelocity = 2f;

        [FullInspector.InspectorOrder(5f), FullInspector.InspectorMargin(10)]
        [FullInspector.InspectorComment(FullInspector.CommentType.Info, "Обрабатывать состояние флага appState.lockWidgets")]
        public bool detectLockWidgets = true;

        [FullInspector.InspectorOrder(6f), FullInspector.InspectorMargin(12)]
        [FullInspector.InspectorComment(FullInspector.CommentType.Warning, "Ширина элемента в ленте scroll")]
        public float itemWidth = 600.0f;
        [SerializeField, HideInInspector]
        private Dictionary<MageAppAspectRatio, float> _itemDistanceByAspect;
        [FullInspector.InspectorOrder(6.1f), FullInspector.ShowInInspector, FullInspector.InspectorMargin(18)]
        [FullInspector.InspectorComment(FullInspector.CommentType.Info, "Расстояние между элементами в ленте")]
        [FullInspector.InspectorTooltip("Расстояние между элементами в ленте")]        
        public Dictionary<MageAppAspectRatio, float> ItemDistanceByAspect
        {
            get
            {
                if (_itemDistanceByAspect == null) _itemDistanceByAspect = new Dictionary<MageAppAspectRatio, float>();
                if (!_itemDistanceByAspect.ContainsKey(MageAppAspectRatio.AspectOthers))
                    _itemDistanceByAspect.Add(MageAppAspectRatio.AspectOthers, _itemDistance);
                return _itemDistanceByAspect;
            }
            set { _itemDistanceByAspect = value; }
        }

        private float _itemDistance = 0.0f;
    }
    #endregion

	#region Fields

	[Tooltip("Камера для расчетов позиций")]    
    public Camera workCamera;
    [Header("Параметры HScroll:")]
    public HScrollSettings settings;
    
    private AppState _appState;
    private MageAppAspectRatio _activeAspect;
    System.Func<Vector3, Vector3, float, Vector3> _Fmoving;

    private Vector3 _zeroPosition = Vector3.zero;

    private float _itemDistance = 0.0f;
    private float _itemWFull;

    private TKTapRecognizer _tapRecognizer = null;
    private TKSwipeRecognizer _swipeRecognizer = null;
    private TKPanRecognizer _panRecognizer = null;
    private TKLongPressRecognizer _longPressRecognizer = null;

    //for detected tap in active box
    private float _colliderDistance = 1000.0f;
    //Prevent tap from other UIView tap
    private bool _tapLock = false;

    private bool _moveItems = false;
    private bool _itemMoving = false;
    private bool _smoothMoving = false;
    private bool _longTapMoving = false;

	private List<Collider> _tkIgnoreBoundsCollider = new List<Collider>();
	private List<Collider> _tapLeftCollider = new List<Collider>();
	private List<Collider> _tapCenterCollider = new List<Collider>();
	private List<Collider> _tapRightCollider = new List<Collider>();

	#endregion

	#region  detect touch area
	//кэшированные переменные для TouchInLockArea
    private Vector3 __touchLocation = Vector3.zero;
    private bool TouchInLockArea(Vector2 touchLocation)
    {        
		__touchLocation.x = touchLocation.x;
        __touchLocation.y = touchLocation.y;
		RaycastHit hit;
        Ray ray = workCamera.ScreenPointToRay(__touchLocation);
        for (int i = 0; i < _tkIgnoreBoundsCollider.Count; i++)            
			if (_tkIgnoreBoundsCollider[i].Raycast(ray, out hit, _colliderDistance)) return true;
        return false;
    }
    #endregion

    #region config & intit TouchKit

    private Vector3 _v_cached;
    private Bounds _boundScreen;

    private void RemoveRecognizers()
    {
        if (_tapRecognizer != null)
        {
            TouchKit.removeGestureRecognizer(_tapRecognizer);
            _tapRecognizer = null;
        }
        if (_panRecognizer != null)
        {
            TouchKit.removeGestureRecognizer(_panRecognizer);
            _panRecognizer = null;
        }
        if (_swipeRecognizer != null)
        {
            TouchKit.removeGestureRecognizer(_swipeRecognizer);
            _swipeRecognizer = null;
        }
        if (_longPressRecognizer != null)
        {
            TouchKit.removeGestureRecognizer(_longPressRecognizer);
            _longPressRecognizer = null;
        }
    }

    private TKRect GetBoundaryFrame(Dictionary<MageAppAspectRatio,Bounds> bounds, Vector2 multiplyUnits)
    {
        var aspect = _activeAspect;        
        if (!bounds.ContainsKey(aspect)) aspect = MageAppAspectRatio.AspectOthers;        
        Bounds bound = bounds.ContainsKey(aspect)? bounds[aspect] : _boundScreen;
		//Debug.Log(" 0 " + multiplyUnits.x);
        Vector2 posCenter = workCamera.WorldToScreenPoint(bound.center);
        return (bound.size != Vector3.zero) ?
			new TKRect(bound.size.x * multiplyUnits.x,
                       bound.size.y * multiplyUnits.y,
                       posCenter) :
            new TKRect(0, 0, Screen.width, Screen.height);
    }

    private void AddRecognizers()
    {
        RemoveRecognizers();

        #region Config gestureRecognized
        float allowableMovement = 1.1f;
        float swipeStartTime = Time.time;
        float velocity = TouchKit.instance.pixelsToUnityUnitsMultiplier.x;
        float pixelIsPerCm = TouchKit.instance.ScreenPixelsPerCm;
        float swipeVelocity = 0f;
        Vector2 multiplyUnits = new Vector2(Screen.width / (2.0f * workCamera.orthographicSize * workCamera.aspect),
                                            Screen.height / (2.0f * workCamera.orthographicSize));
        var frameData = settings.boundFrameData;
        float threshold = settings.thresholdShortPush / pixelIsPerCm;

#if MAGE_FILE_LOG
        //
        Debug.Log("HScrollProxyWidget: ");
        Debug.Log("velocity: " + velocity);
        Debug.Log("multiplyUnits: " + multiplyUnits);
        Debug.Log("runtimeDistanceModifier: " + TouchKit.instance.runtimeDistanceModifier);
        Debug.Log("runtimeScaleModifier: " + TouchKit.instance.runtimeScaleModifier);
        Debug.Log("ScreenPixelsPerCm: " + pixelIsPerCm);
        Debug.Log("HScrollProxyWidget: end");
        //
#endif

        //обработка Tap       
        _tapRecognizer = new TKTapRecognizer(1f, allowableMovement);       
        _tapRecognizer.boundaryFrame = GetBoundaryFrame(frameData.toTapFrame, multiplyUnits);       
        _tapRecognizer.gestureRecognizedEvent += (r) =>
        {
            swipeVelocity = 0.0f;
            var v = r.touchLocation();
            OnTap(new Vector3(v.x, v.y, 0.0f));
        };

        //обработка Swipe       
		_swipeRecognizer = new TKSwipeRecognizer(TKSwipeDirection.Horizontal, 1, 1.5f);
        _swipeRecognizer.boundaryFrame = GetBoundaryFrame(frameData.toPanFrame, multiplyUnits);        
        _swipeRecognizer.gestureRecognizedEvent += (r) =>
        {
            swipeVelocity = 0.0f;
            if (lockScroll) return;
            //изменяем скорость только если swipe  завершен
            if ((r.state == TKGestureRecognizerState.Recognized))
            {
                swipeVelocity = velocity * r.swipeVelocity * pixelIsPerCm;
                swipeStartTime = Time.time;
                if (r.completedSwipeDirection == TKSwipeDirection.Left) swipeVelocity = -swipeVelocity;
            }
        };

        //обработка Pan
        _panRecognizer = new TKPanRecognizer();               
        _panRecognizer.boundaryFrame = GetBoundaryFrame(frameData.toPanFrame, multiplyUnits);       
        _panRecognizer.gestureRecognizedEvent += (r) =>
        {
            if (lockScroll || TouchInLockArea(r.touchLocation())) return;
            _smoothMoving = false;
            _v_cached = transform.localPosition;
            _v_cached.x = _v_cached.x + velocity * r.deltaTranslation.x;            
            transform.localPosition = _v_cached;
        };
        
        _panRecognizer.gestureCompleteEvent += r =>
        {
            //Debug.Log("pan:  velocity: ".AddTime() + swipeVelocity);
            if (_itemMoving || _smoothMoving || lockScroll || !gameObject.activeSelf)
            {
                swipeVelocity = 0.0f;// всегда сбрасываем скорость swipe!!!
                return;
            }
            
            _v_cached = transform.localPosition;
            int offset = 0;
            var dt = Time.time - swipeStartTime;
            // расчет перемешщения по swipe 
            if (dt <= settings.timeSwipeReaction) offset = Mathf.RoundToInt(settings.scaleSwipeVelocity * swipeVelocity / _itemWFull);
            swipeVelocity = 0.0f;// всегда сбрасываем скорость swipe!!!            
            // короткое "толкание"
            //if (offset == 0 && (Mathf.Abs(r.deltaTranslation.x) > threshold))
            if (offset == 0 && (Mathf.Abs(r.deltaTranslationCm) > threshold))
                offset = r.deltaTranslation.x >= 0 ? 1 : -1;
            // время движения (варьирование скорости)
            float timeMove = Mathf.Clamp(settings.moving.minTime + settings.moving.stepTime * offset, settings.moving.minTime, settings.moving.maxTime);
            // перерасчет смещения и запуск перемещения			
            _v_cached.x = (CurrentItemByPos(_v_cached.x) + offset) * _itemWFull;
            _smoothMoving = true;
            //Debug.Log(" ==== offset: ".AddTime() + offset + " x: " + r.deltaTranslation.x + " Cm: " + r.deltaTranslationCm);
            StartCoroutine(AsyncMoveBetweenPoints(transform.localPosition, _v_cached, timeMove));
        };

        if (settings.durationLongTap > 0.35f)
        {            
			_longPressRecognizer = new TKLongPressRecognizer(settings.durationLongTap, allowableMovement, -1);
            _longPressRecognizer.boundaryFrame = GetBoundaryFrame(frameData.toTapFrame, multiplyUnits);
            _longPressRecognizer.gestureRecognizedEvent += (r) =>
            {
                if (_longTapMoving) return;
                _longTapMoving = true;
                Vector2 v = r.touchLocation();
                OnLongTap(new Vector3(v.x, v.y, 0.0f));
            };
            _longPressRecognizer.gestureCompleteEvent += (r) =>
            {
                _longTapMoving = false;
            };
        }
        else _longPressRecognizer = null;

        // register swipe&scroll&tap					
        //-- old TouchKit
        //TouchKit.addGestureRecognizer(_tapRecognizer);
        //TouchKit.addGestureRecognizer(_swipeRecognizer);
        //TouchKit.addGestureRecognizer(_panRecognizer);
        //if (_longPressRecognizer != null) TouchKit.addGestureRecognizer(_longPressRecognizer);
        // -- new TouchKit
        TouchKit.addGestureRecognizer(_panRecognizer);
		TouchKit.addGestureRecognizer(_swipeRecognizer);
		TouchKit.addGestureRecognizer(_tapRecognizer);
		if (_longPressRecognizer != null) TouchKit.addGestureRecognizer(_longPressRecognizer);
        // --
#endregion
    }
#endregion

#region ВСПОМОГАТЕЛЬНЫЕ методы
    /// <summary>
    /// определяет текущий активный элемент
    /// </summary>
    /// <param name="x"></param>
    /// <returns></returns>
    public int CurrentItemByPos(float x)
    {
        return (int)Mathf.Round(Mathf.Abs(x) / _itemWFull) * (int)Mathf.Sign(x);
    }
    /// <summary>
    /// дополнительные условия блокировки обработки touch
    /// </summary>
    private bool lockScroll
    {
        get
        {
            return (_tapLock || _appState.IsShowUIView || _appState.IsShowDummyView || (settings.detectLockWidgets && _appState.lockWidgets)
                || (ButtonWidget.countTouchInProcess > 0) || (ButtonWidgetStd.countTouchInProcess > 0) || !gameObject.activeSelf);
        }
    }

    private int DetectDirMove(Vector3 mousePos)
    {
        RaycastHit hit;
		for(int i=0; i< _tapLeftCollider.Count; i++)
			if (_tapLeftCollider[i].Raycast(workCamera.ScreenPointToRay(mousePos), out hit, _colliderDistance))
				return -1;

		for(int i=0; i< _tapRightCollider.Count; i++)
			if (_tapRightCollider[i].Raycast(workCamera.ScreenPointToRay(mousePos), out hit, _colliderDistance))
				return 1;
        
        return 0;
    }
#endregion

#region Обработка Команд

    public void MoveItems(int countItem, float timeInSecond = 0.5f, int moveType = 0)
    {
        if (_smoothMoving || _moveItems || (countItem == 0) || !gameObject.activeSelf) return;
        _tapLock = true;
        _itemMoving = true;
        Vector3 v = transform.localPosition;
        v.x = moveType == 0 ? _itemWFull * (CurrentItemByPos(v.x) + countItem) : v.x + _itemWFull * countItem;
        _moveItems = true;
        _smoothMoving = true;
    
        if (Messenger.eventTable.ContainsKey(AppState.EventTypes.uiSPTapNavItem))
            Messenger.Broadcast<int, int>(AppState.EventTypes.uiSPTapNavItem,
                CurrentItemByPos(transform.localPosition.x), countItem);
        StartCoroutine(AsyncMoveItem(transform.localPosition, v, timeInSecond));
#if !MAGE_NO_PM
		if(settings.playMakerUsingMyltiplyItemsInEventPaterns)
			PlayMakerFSM.BroadcastEvent(settings.playMakerMoveItemsEventPatern + countItem);
		else
			PlayMakerFSM.BroadcastEvent(settings.playMakerMoveItemsEventPatern);
#endif
        return;
    }

    private void OnLongTap(Vector3 mousePos)
    {
        if (_itemMoving || _smoothMoving || !gameObject.activeSelf) return;
        if (lockScroll) return;
        var dirMove = DetectDirMove(mousePos);
        if (dirMove != 0) StartCoroutine(AsyncMoveLongTapItems(dirMove, settings.moving.minTime));
    }

    private void OnTap(Vector3 mousePos)
    {
        // если придыдущее движение ленты то прерываем, 
        // т.к. НЕОБХОДИМО ТОЧНОЕ ПОЗИЦИОНИРОВАНИЕ ЦЕНТРА
        if (_itemMoving || _smoothMoving || lockScroll || !gameObject.activeSelf) return;
        if (settings.tapObjects.tapCenter == null) return;
        RaycastHit hit;

		for (int i = 0; i < _tapCenterCollider.Count; i++)
        {
			if (_tapCenterCollider[i].Raycast(workCamera.ScreenPointToRay(mousePos), out hit, _colliderDistance))
            {
                
#if !MAGE_NO_PM
				if(settings.playMakerUsingMyltiplyItemsInEventPaterns)
					PlayMakerFSM.BroadcastEvent(settings.playMakerTapEventPatern + i.ToString());
				else
					PlayMakerFSM.BroadcastEvent(settings.playMakerTapEventPatern);
#endif
                if (Messenger.eventTable.ContainsKey(AppState.EventTypes.uiSPTapActiveItem))
                    Messenger.Broadcast<int, int>(AppState.EventTypes.uiSPTapActiveItem,
                        CurrentItemByPos(transform.localPosition.x), i);

                return;
            }
        }
        // moving
        int dirMove = DetectDirMove(mousePos);
        if (dirMove != 0) MoveItems(dirMove, settings.moving.minTime);// + moving.stepTime);
    }

#endregion

#region Законы перемещения

    // закон движения для easeIn:
    //result = (to - from) * delta(progress) + from
    // запись формулы в контексте параметров скрипта: result = move*delta(time) + start;
    // тогда 
    //easeOut: delta = 1 - delta(1 - progress)
    //context easeOut: delta = 1.0f-delta(1.0f-time);
    //easeInOut: delta =  progress<0.5? delta(2 * progress) / 2 : (2 - delta(2 * (1 - progress))) / 2
    //context easeInOut: delta =  time<0.5f? 0.5f*delta(2.0f * time) : (1.0f - 0.5f*delta(2.0f * (1.0 - time)))

    float _Pow(float t)
    {
        return Mathf.Pow(t, settings.moving.power);
    }
    private Vector3 MovingPow(Vector3 start, Vector3 move, float time)
    {
        return move * _Pow(time) + start;
    }    
    private Vector3 MovingPowInOut(Vector3 start, Vector3 move, float time)
    {
        float delta = (time < 0.5f) ? 
            0.5f * _Pow(2.0f * time) : 1.0f - 0.5f * _Pow(2.0f * (1.0f - time));
        return move * delta + start;
    }

    private float _Sin(float t)
    {
        return settings.moving.amplitude * Mathf.Sin(t * Mathf.PI);
    }

    private Vector3 MovingSin(Vector3 start, Vector3 move, float time)
    {		
		return move * _Sin(time) + start;
    }

    // лук- отвод тетивы и выстрел
	private float _Back(float t)
	{
		float c = settings.moving.backCoef;
		return  (t*t*(c+1.0f)*t-c);
	}

    private Vector3 MovingBackIn(Vector3 start, Vector3 move, float time)
    {       
		return move * _Back(time) + start;
    }

	private Vector3 MovingBackOut(Vector3 start, Vector3 move, float time)
	{       
		return move * (1.0f - _Back(1.0f - time)) + start;
	}

	private Vector3 MovingBackInOut(Vector3 start, Vector3 move, float time)
	{       
		float delta = (time < 0.5f) ?
			0.5f * _Back(2.0f * time) : 1.0f - 0.5f * _Back(2.0f * (1.0f - time));
		return move * delta + start;
	}

    private float _Circ(float t)
    {
        return 1.0f - Mathf.Sin(Mathf.Acos(t));
    }

    private Vector3 MovingCircIn(Vector3 start, Vector3 move, float time)
    {
        return move * _Circ(time) + start;
    }
    
    private Vector3 MovingCircOut(Vector3 start, Vector3 move, float time)
    {
        //return move * Mathf.Sin(Mathf.Acos(1.0f - time)) + start;
		return move * (1.0f - _Circ(1.0f - time)) + start;
    }   
    
    private Vector3 MovingCircInOut(Vector3 start, Vector3 move, float time)
    {
        float delta = (time < 0.5f) ?
            0.5f * _Circ(2.0f * time) : 1.0f - 0.5f * _Circ(2.0f * (1.0f - time));
        return move * delta + start;
    }
    private float _Sine(float t)
    {
       return 1.0f - Mathf.Sin((1.0f - t) * PI05);
    }

    private Vector3 MovingSineIn(Vector3 start, Vector3 move, float time)
    {
        return move * _Sine(time) + start;
    }

    private Vector3 MovingSineOut(Vector3 start, Vector3 move, float time)
    {        
        return move * (1.0f - _Sine(1.0f - time)) + start;
    }

    private Vector3 MovingSineInOut(Vector3 start, Vector3 move, float time)
    {
        float delta = (time < 0.5f) ?
            0.5f * _Sine(2.0f * time) : 1.0f - 0.5f * _Sine(2.0f * (1.0f - time));
        return move * delta + start;
    }

    //============================== Long Tap ==============================
    private Vector3 MovingForLongTap(Vector3 start, Vector3 move, float time)
    {
        return move * time + start;
    }
    private Vector3 MovingForLongTapEnd(Vector3 start, Vector3 move, float time)
    {
        return move * (1.0f - Mathf.Pow(1.0f - time, 2)) + start;
    }
    //============================== Long Tap ===============================

#endregion

#region Перемещение скролла

    // перемещение скролл-контрола в нужную позицию
    private void UpdateScrollBox(Vector3 newPosition)
    {
        transform.localPosition = newPosition;
    }

    private IEnumerator _AsyncMove(Vector3 start, Vector3 end, float time, bool checkSmooth = true,
         System.Func<Vector3, Vector3, float, Vector3> Fmoving = null)
    {
        UpdateScrollBox(start);
        {
            if (Fmoving == null) Fmoving = _Fmoving;
            float t = 0.0f;
            Vector3 move = end - start;
            Vector3 v;
            float f = 1.0f / time;//frequency
            bool left = (end.x - start.x) < 0.0f;
            if (left)
            {
                while (t < 1.0f)
                {
                    if (checkSmooth && !_smoothMoving) yield break;
                    t += Time.smoothDeltaTime * f;
                    v = Fmoving(start, move, t);
                    if (v.x < end.x)
                    {
                        Vector3 deltaJump = 0.5f * (end - transform.localPosition);
                        for (int i = 0; i < 2; i++)
                        {
                            UpdateScrollBox(transform.localPosition + deltaJump);
                            yield return null;
                            if (checkSmooth && !_smoothMoving) yield break;
                        }
                        break;
                    }
                    UpdateScrollBox(v);
                    yield return null;
                }
            }
            else
            {
                while (t < 1.0f)
                {
                    if (checkSmooth && !_smoothMoving) yield break;
                    t += Time.smoothDeltaTime * f;
                    v = Fmoving(start, move, t);
                    if (v.x > end.x)
                    {
                        Vector3 deltaJump = 0.5f * (end - transform.localPosition);
                        for (int i = 0; i < 2; i++)
                        {
                            UpdateScrollBox(transform.localPosition + deltaJump);
                            yield return null;
                            if (checkSmooth && !_smoothMoving) yield break;
                        }
                        break;
                    }
                    UpdateScrollBox(v);
                    yield return null;
                }
            }
        }
        UpdateScrollBox(end);
    }

    private IEnumerator AsyncMoveBetweenPoints(Vector3 start, Vector3 end, float time)
    {
        if (_smoothMoving == false) yield break;
        var asyncMove = _AsyncMove(start, end, time, true);
        while (asyncMove.MoveNext()) yield return asyncMove.Current;
        _smoothMoving = false;

		if(settings.sendMoveFinishedEvent)
		{
			if (Messenger.eventTable.ContainsKey(AppState.EventTypes.uiSPMoveFinished))
				Messenger.Broadcast(AppState.EventTypes.uiSPMoveFinished);
#if !MAGE_NO_PM
			PlayMakerFSM.BroadcastEvent(settings.playMakerMoveFinishedEvent);
#endif
		}
        yield break;
    }

    private IEnumerator AsyncMoveItem(Vector3 start, Vector3 end, float time)
    {
        if (!_smoothMoving) {
            _moveItems = false;
            yield break;
        }
        _tapLock = true;
        _itemMoving = true;
        var asyncMove = _AsyncMove(start, end, time, false);
        while (asyncMove.MoveNext()) yield return asyncMove.Current;
        _tapLock = false;
        _itemMoving = false;
        _moveItems = false;
        _smoothMoving = false;
		if(settings.sendMoveFinishedEvent)
		{
			if (Messenger.eventTable.ContainsKey(AppState.EventTypes.uiSPMoveFinished))
				Messenger.Broadcast(AppState.EventTypes.uiSPMoveFinished);
#if !MAGE_NO_PM
			PlayMakerFSM.BroadcastEvent(settings.playMakerMoveFinishedEvent);
#endif
		}
        yield break;
    }

	private IEnumerator AsyncMoveLongTapItems(int countItem, float timeInSecond)
	{
		if (_smoothMoving || _moveItems || (countItem == 0)) yield break;
		Vector3 v = transform.localPosition;
		float time = settings.moving.minTime;// +moving.stepTime;
		float velocity = TouchKit.instance.pixelsToUnityUnitsMultiplier.x * settings.scaleLongTapVelocity;
		float speed = velocity * ((float)(countItem * _itemWFull) / time);
		float f = 1f / time;
		float t = 2f;
		do
		{
			if (t < 1.0f)
			{
				t += Time.smoothDeltaTime * f;
				v.x += speed * Time.smoothDeltaTime;
				UpdateScrollBox(v);
			}
			else
			{
				t = 0.0f;
				if (Messenger.eventTable.ContainsKey(AppState.EventTypes.uiSPLongTapNavItem))
					Messenger.Broadcast<int, int>(
						AppState.EventTypes.uiSPLongTapNavItem,
						CurrentItemByPos(transform.localPosition.x),
						countItem);
#if !MAGE_NO_PM
				if(settings.playMakerUsingMyltiplyItemsInEventPaterns)
					PlayMakerFSM.BroadcastEvent(settings.playMakerMoveItemsEventPatern + countItem);
				else
					PlayMakerFSM.BroadcastEvent(settings.playMakerMoveItemsEventPatern);
#endif
			}
			yield return null;
		} while (_longTapMoving);

		v = transform.localPosition;
		v.x = _itemWFull * (CurrentItemByPos(v.x) + countItem);
		_smoothMoving = true;
		_moveItems = true;
		if (Messenger.eventTable.ContainsKey(AppState.EventTypes.uiSPLongTapNavItem))
			Messenger.Broadcast<int, int>(AppState.EventTypes.uiSPLongTapNavItem,
				CurrentItemByPos(transform.localPosition.x), countItem);
		StartCoroutine(AsyncLongTapMoveItem(transform.localPosition, v, time + settings.moving.stepTime, true));
#if !MAGE_NO_PM
		if(settings.playMakerUsingMyltiplyItemsInEventPaterns)
			PlayMakerFSM.BroadcastEvent(settings.playMakerMoveItemsEventPatern + countItem);
		else 
		   PlayMakerFSM.BroadcastEvent(settings.playMakerMoveItemsEventPatern);
#endif
		yield break;
	}

    private IEnumerator AsyncLongTapMoveItem(Vector3 start, Vector3 end, float time, bool lastItem = false)
    {
        if (!_smoothMoving) {
            _moveItems = false;
            yield break;
        }
        System.Func<Vector3, Vector3, float, Vector3> Fmov = MovingForLongTap;
        if (lastItem) Fmov = MovingForLongTapEnd;
        _tapLock = true;
        _itemMoving = true;
        var asyncMove = _AsyncMove(start, end, time, false, Fmov);
        while (asyncMove.MoveNext()) yield return asyncMove.Current;
        _tapLock = false;
        _itemMoving = false;
        _moveItems = false;
        _smoothMoving = false;

		if(settings.sendMoveFinishedEvent)
		{
			if (Messenger.eventTable.ContainsKey(AppState.EventTypes.uiSPMoveFinished))
				Messenger.Broadcast(AppState.EventTypes.uiSPMoveFinished);
#if !MAGE_NO_PM
			PlayMakerFSM.BroadcastEvent(settings.playMakerMoveFinishedEvent);
#endif
		}
		yield break;
    }
#endregion

#region Unity event functions

    void Start()
    {
        _appState = AppState.instance;
        _activeAspect = _appState.aspect;

        if (workCamera == null) workCamera = Camera.main;
        _colliderDistance += workCamera.farClipPlane;
        if (settings.tkIgnoreBounds == null) settings.tkIgnoreBounds = new GameObject[0];
        if (settings.tkIgnoreBounds.Length < 1)
            settings.tkIgnoreBounds = GameObject.FindGameObjectsWithTag("TKIgnoreBound");
        else 
        {
        	if (settings.tkIgnoreBounds.Length == 1 && settings.tkIgnoreBounds[0] == null)
        		settings.tkIgnoreBounds = GameObject.FindGameObjectsWithTag("TKIgnoreBound");
    	}

		Collider clldr;
		if( settings.tkIgnoreBounds  != null)
		{
			for(int i =0; i< settings.tkIgnoreBounds.Length; i++)
			{
				clldr = settings.tkIgnoreBounds[i].GetComponent<Collider>();
				if(clldr!= null) _tkIgnoreBoundsCollider.Add(clldr);
			}
		}

		if( settings.tapObjects.tapLeft != null)
		{
			for(int i =0; i< settings.tapObjects.tapLeft.Length; i++)
			{
				clldr = settings.tapObjects.tapLeft[i].GetComponent<Collider>();
				if(clldr!= null) _tapLeftCollider.Add(clldr);
			}
		}

		if( settings.tapObjects.tapCenter != null)
		{
			for(int i =0; i< settings.tapObjects.tapCenter.Length; i++)
			{
				clldr = settings.tapObjects.tapCenter[i].GetComponent<Collider>();
				if(clldr!= null) _tapCenterCollider.Add(clldr);
			}
		}

		if( settings.tapObjects.tapRight != null)
		{
			for(int i =0; i< settings.tapObjects.tapRight.Length; i++)
			{
				clldr = settings.tapObjects.tapRight[i].GetComponent<Collider>();
				if(clldr!= null) _tapRightCollider.Add(clldr);
			}
		}
        _activeAspect = _appState.aspect;
        var asp = _activeAspect;
        if (!settings.ItemDistanceByAspect.ContainsKey(asp)) asp = MageAppAspectRatio.AspectOthers;
        _itemDistance = settings.ItemDistanceByAspect[asp];
        _itemWFull = settings.itemWidth + _itemDistance;
        transform.localPosition = _zeroPosition;

        switch (settings.moving.type)
        {
            case TypeMoving.Power:
                _Fmoving = MovingPow;
                break;
            case TypeMoving.PowerInOut:
                _Fmoving = MovingPowInOut;
                break;

            case TypeMoving.Sin:
                _Fmoving = MovingSin;
                break;

            case TypeMoving.CircIn:
                _Fmoving = MovingCircIn;
                break;
            case TypeMoving.CircOut:
                _Fmoving = MovingCircOut;
                break;
            case TypeMoving.CircInOut:
                _Fmoving = MovingCircInOut;
                break;

            case TypeMoving.SineIn:
                _Fmoving = MovingSineIn;
                break;
            case TypeMoving.SineOut:
                _Fmoving = MovingSineOut;
                break;
            case TypeMoving.SineInOut:
                _Fmoving = MovingSineInOut;
                break;

			case TypeMoving.BackIn:
				_Fmoving = MovingBackIn;
				break;
			case TypeMoving.BackOut:
				_Fmoving = MovingBackOut;
				break;
			case TypeMoving.BackInOut:
				_Fmoving = MovingBackInOut;
				break;

            default:
                _Fmoving = MovingPowInOut;
                break;
        }
		_boundScreen = new Bounds(Vector3.zero, Vector3.zero);
        AddRecognizers();        
    }

    void OnDestroy()
    {
        RemoveRecognizers();
    }
#endregion
}
