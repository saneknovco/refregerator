﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class OpenAppLinkButton : ButtonWidget 
{
	[FullInspector.InspectorMargin(16)]
	[FullInspector.InspectorCategory("Action")]	
	public string appKey;	

	[FullInspector.InspectorCategory("Action")]
	[FullInspector.InspectorTooltip("Сделать актиным GameObject, когда AppLink локальная(приложение установленно)")]
	public GameObject localAppLink;

	[FullInspector.InspectorCategory("Action")]
	[FullInspector.InspectorTooltip("Сделать актиным GameObject, когда AppLink магазтна(приложение не установленно)")]
	public GameObject storeAppLink;

	[FullInspector.InspectorCategory("Action")]
	public bool addListenerRefreshAppLink = true;

	[FullInspector.InspectorMargin(16)]
	[FullInspector.InspectorCategory("Action")]
	[FullInspector.InspectorTooltip("Если false то позволяет блокировать обработку Touch через AppState.Instance.lockWidgets")]
	public bool ignoreLockWidget = false;

	private MageAppType _appType;
	private bool _appIsInstalled = false;
	protected bool AppIsInstalled
	{
		set
		{
			_appIsInstalled = value;
			if (localAppLink != null) localAppLink.SetActive(_appIsInstalled);
			if (storeAppLink != null) storeAppLink.SetActive(!_appIsInstalled);
			widgetSettings.parentalGate = !_appIsInstalled;
#if UNITY_ANDROID
			widgetSettings.parentalGate = widgetSettings.parentalGate && (_appType == MageAppType.Full);
#endif
		}
	}

	private void OnRefreshAppLink(string a_appKey, bool isInstalled)
	{
		if(appKey == a_appKey) AppIsInstalled = isInstalled;		
	}

	protected override void DoAwake()
	{
		_appType = AppSettings.instance.appInfo.activeAppType;
		if(addListenerRefreshAppLink )
			Messenger.AddListener<string, bool>(AppState.EventTypes.adChangeStateAppLink, OnRefreshAppLink);
	}	

	protected override void DoStart()
	{
		AppIsInstalled = AppDomainController.instance.AppIsInstalled(appKey);		
	}

	protected override bool DoBreakTouch()
	{		
		return  (!ignoreLockWidget && AppState.instance.lockWidgets) || !widgetSettings.canExecute;		
	}

	protected override void DoTouch()
	{
		AppIsInstalled = AppDomainController.instance.AppIsInstalled(appKey,true);

#if MAGE_ANALYTICS
				UnityEngine.Analytics.Analytics.CustomEvent("AppLink", new Dictionary<string, object>
				{
					{"action", "touch"},
					{"app", appKey}
				});
#endif

		if (widgetSettings.parentalGate)
		{
			AppState.instance.parentalGateSvc.CreateUIView(p =>
			{
				AppDomainController.instance.GoAppLink(appKey);
			});
		}
		else AppDomainController.instance.GoAppLink(appKey);
	}

	protected override void DoDestroy()
	{
		if(addListenerRefreshAppLink )
		{
			if (Messenger.eventTable.ContainsKey(AppState.EventTypes.adChangeStateAppLink))
				Messenger.RemoveListener<string, bool>(AppState.EventTypes.adChangeStateAppLink, OnRefreshAppLink);	
		}
	}
}