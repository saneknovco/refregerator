﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class AppDomainController : GlobalSingletonBehaviour<AppDomainController>
{
	private Dictionary<string, AppSettings.AppDomainSettings> _appDomains;

	#region AppLinks
	private Dictionary<string, bool> _appLinkLocal;
	private bool _refreshStatus = false;

	#region Public methods
	/// <summary>
	/// Проверка установлено ли одно из приложений группы
	/// </summary>
	/// <param name="appKey">ключ группы приложений (Full/Lite)</param>
	/// <param name="forceCheck"></param>
	/// <returns></returns>
	public bool AppIsInstalled(string appKey, bool forceCheck = false)
	{
		if(forceCheck) CheckAppLink( appKey );
		if( _appLinkLocal.ContainsKey(appKey) ) return _appLinkLocal[appKey];
		return false;
	}

	/// <summary>
	/// Проверить наличие приложений на устройстве по AppLinks
	/// </summary>
	public void RefreshAppLinks()
	{
		foreach(var appKey in _appLinkLocal.Keys)
		{
			var state = _appLinkLocal[appKey];
			CheckAppLink(appKey);
			if(state!=_appLinkLocal[appKey])
			{
				if(Messenger.eventTable.ContainsKey(AppState.EventTypes.adChangeStateAppLink ))
					Messenger.Broadcast<string, bool>(AppState.EventTypes.adChangeStateAppLink, appKey, _appLinkLocal[appKey]);
			}
		}
	}

	/// <summary>
	/// перейти по активной AppLink (adaptive algorithm)
	/// </summary>
	/// <param name="appKey"></param>
	/// <returns></returns>
	public bool GoAppLink(string appKey)
	{
		if (string.IsNullOrEmpty(appKey)) return false;
		if (!_appDomains.ContainsKey(appKey)) return false;
		var appDomain = _appDomains[appKey];
		var appLinks = appDomain.appLinks;
		string appId;
		string appIdFull;
		string appUrl;		
		if (appLinks.ContainsKey(MageAppType.Freemium))
		{
			appId = appLinks[MageAppType.Freemium].appId;
			appUrl = MgCommonUtils.GetAppLocalUri(appId);
			if (SysUtilsProxy.AppIsInstalled(appUrl))
			{
				_appLinkLocal[appKey] = true;

#if MAGE_ANALYTICS
				UnityEngine.Analytics.Analytics.CustomEvent("AppLink", new Dictionary<string, object>
				{
					{"action", "open"},
					{"app", appKey}					
				});
#endif
				SysUtilsProxy.OpenApp(appUrl);
			}
			else
			{
				_appLinkLocal[appKey] = false;

#if MAGE_ANALYTICS
				UnityEngine.Analytics.Analytics.CustomEvent("AppLink", new Dictionary<string, object>
				{
					{"action", "openStore"},
					{"app", appKey}
				});
#endif
				MgCommonUtils.GetAppInStore(appId);
			}
			return true;
		}
		else
		{
			if (appLinks.ContainsKey(MageAppType.Full))
			{
				appId = appLinks[MageAppType.Full].appId;
				appUrl = MgCommonUtils.GetAppLocalUri(appId);
				if (SysUtilsProxy.AppIsInstalled(appUrl))
				{
					_appLinkLocal[appKey] = true;
					SysUtilsProxy.OpenApp(appUrl);
				}
				else
				{
					if (appLinks.ContainsKey(MageAppType.Lite))
					{
						appIdFull = appId;
						appId = appLinks[MageAppType.Lite].appId;
						appUrl = MgCommonUtils.GetAppLocalUri(appId);
						if (SysUtilsProxy.AppIsInstalled(appUrl))
						{
							_appLinkLocal[appKey] = true;
							SysUtilsProxy.OpenApp(appUrl);
						}
						else
						{
							_appLinkLocal[appKey] = false;
							if (AppSettings.instance.appInfo.activeAppType == MageAppType.Full) MgCommonUtils.GetAppInStore(appIdFull);
							else MgCommonUtils.GetAppInStore(appId);
						}
					}
					else
					{
						_appLinkLocal[appKey] = false;
						MgCommonUtils.GetAppInStore(appId);
					}
				}
				return true;
			}
			else
			{
				if (appLinks.ContainsKey(MageAppType.Lite))
				{
					appId = appLinks[MageAppType.Lite].appId;
					appUrl = MgCommonUtils.GetAppLocalUri(appId);
					if (SysUtilsProxy.AppIsInstalled(appUrl))
					{
						_appLinkLocal[appKey] = true;
						SysUtilsProxy.OpenApp(appUrl);
					}
					else
					{
						_appLinkLocal[appKey] = false;
						MgCommonUtils.GetAppInStore(appId);
					}
				}
			}
		}
		return false;
	}
	#endregion

	bool CheckAppLink(string appKey)
	{	
		if (string.IsNullOrEmpty(appKey)) return false;
		if (!_appDomains.ContainsKey(appKey)) return false;
		var appDomain = _appDomains[appKey];
		var appLinks = appDomain.appLinks;
		string appId = string.Empty;
		string appUrl;		
		if (appLinks.ContainsKey(MageAppType.Freemium))
		{
			appId = appLinks[MageAppType.Freemium].appId;
			appUrl = MgCommonUtils.GetAppLocalUri(appId);
			_appLinkLocal[appKey] = SysUtilsProxy.AppIsInstalled(appUrl);			
			return true;
		}
		else
		{
			if (appLinks.ContainsKey(MageAppType.Full))
			{
				appId = appLinks[MageAppType.Full].appId;
				appUrl = MgCommonUtils.GetAppLocalUri(appId);
				if (SysUtilsProxy.AppIsInstalled(appUrl))
				{
					_appLinkLocal[appKey] = true;
				}
				else
				{
					if (appLinks.ContainsKey(MageAppType.Lite))
					{						
						appId = appLinks[MageAppType.Lite].appId;
						appUrl = MgCommonUtils.GetAppLocalUri(appId);
						_appLinkLocal[appKey] = SysUtilsProxy.AppIsInstalled(appUrl);
					}
					else 
					{
						_appLinkLocal[appKey] = false;
					}
				}
				return true;
			}
			else
			{
				if (appLinks.ContainsKey(MageAppType.Lite))
				{
					appId = appLinks[MageAppType.Lite].appId;
					appUrl = MgCommonUtils.GetAppLocalUri(appId);
					_appLinkLocal[appKey] = SysUtilsProxy.AppIsInstalled(appUrl);					
				}
			}
		}
		return false;
	}
	#endregion

	#region Unity event functions

	protected override void DoAwake() 
	{ 
		base.DoAwake();
		_appDomains = AppSettings.instance.appDomain;
		_appLinkLocal = new Dictionary<string, bool>();
		_refreshStatus = AppSettings.instance.refreshAppLinksInResume;		
		foreach(var item in _appDomains.Keys)
		{
			_appLinkLocal.Add(item, false);
			CheckAppLink(item);
		}
	}

	void Start()
	{
		MgCoroutineHelper.WaitThenCallback(AppConfig.frameWaitAsync +4, () =>
		{
			RefreshAppLinks();
		});
	}

	void OnApplicationPause(bool pauseStatus)
	{
		if (pauseStatus || !_refreshStatus) return;
		MgCoroutineHelper.WaitThenCallback(AppConfig.frameWaitAsync, () =>
		{
			RefreshAppLinks();
		});
	}
	#endregion
}
