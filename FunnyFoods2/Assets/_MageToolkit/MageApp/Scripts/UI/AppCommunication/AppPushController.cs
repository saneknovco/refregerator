﻿using UnityEngine;
using System.Collections.Generic;

public class AppPushController : GlobalSingletonBehaviour<AppPushController>
{
#if MAGE_ANALYTICS
    public const string handleNotificationEvent = "PushHandle";
    public const string pushInitEvent = "PushInit";
    private static float _timePause = 0;
#endif

    // Gets called when the user opens the notification or gets one while in your app.
    // The name of the method can be anything as long as the signature matches.
    // Method must be static or this object should be marked as DontDestroyOnLoad
    private static void HandleNotification(string message, Dictionary<string, object> additionalData, bool isActive)
	{
		//print("GameControllerExample:HandleNotification:message" + message);
		if (_instance != null) _instance.DoNotification(message, additionalData, isActive);

#if MAGE_ANALYTICS && !MAGE_NO_PUSH
        float rts = Time.realtimeSinceStartup;
        UnityEngine.Analytics.Analytics.CustomEvent(handleNotificationEvent, new Dictionary<string, object>
		    {
			    {"message", message },
			    {"isActive", isActive },
			    {"timeSinceStartup", rts},
                {"timeSincePause", (rts - _timePause)}
            });
#if MAGE_FILE_LOG
        float t = Time.realtimeSinceStartup;
        Debug.Log("HandleNotification:start...".AddTime());
        Debug.Log("HandleNotification:message: " + message);
        Debug.Log("HandleNotification:isActive: " + isActive);       
        Debug.Log("HandleNotification:timeSinceStartup: " + t);
        Debug.Log("HandleNotification:timeSincePause: " + (t - _timePause));     
        Debug.Log("HandleNotification:end...".AddTime());
#endif
#endif               
    }

    protected void DoNotification(string message, Dictionary<string, object> additionalData, bool isActive)
	{
		if (Messenger.eventTable.ContainsKey(AppState.EventTypes.pnsNotification))
			Messenger.Broadcast<string, Dictionary<string, object>, bool>(AppState.EventTypes.pnsNotification,
				message, additionalData, isActive);
	}

	void Shutdown()
	{
		Release(false);
	}   

    void Start()
	{
#if MAGE_NO_PUSH
    Invoke("Shutdown", 1);
#else
#if MAGE_PUSH_DEBUG
        // Enable line below to debug issues with setuping OneSignal. (logLevel, visualLogLevel)
        //OneSignal.SetLogLevel(OneSignal.LOG_LEVEL.INFO, OneSignal.LOG_LEVEL.INFO);
        //OneSignal.SetLogLevel(OneSignal.LOG_LEVEL.DEBUG, OneSignal.LOG_LEVEL.DEBUG);
        //OneSignal.SetLogLevel(OneSignal.LOG_LEVEL.VERBOSE, OneSignal.LOG_LEVEL.VERBOSE);
#endif

        // The only required method you need to call to setup OneSignal to receive push notifications.
        // Call before using any other methods on OneSignal.
        // Should only be called once when your app is loaded.
        // OneSignal.Init(OneSignal_AppId, GoogleProjectNumber, NotificationReceivedHandler(optional));		
        var pnsSettings = AppSettings.instance.PNS;
		if (pnsSettings.enabled && !string.IsNullOrEmpty(pnsSettings.appId))
		{
#if UNITY_IOS || UNITY_TVOS
			OneSignal.Init(pnsSettings.appId, null, HandleNotification);
#else
            OneSignal.Init(pnsSettings.appId, pnsSettings.googleProjectNumber, HandleNotification);
#endif
            // Shows a Native iOS/Android alert dialog when the user is in your app when a notification comes in.
            //OneSignal.EnableInAppAlertNotification(pnsSettings.inAppAlertNotification);
            OneSignal.EnableInAppAlertNotification(false);
            OneSignal.EnableNotificationsWhenActive(true);
#if MAGE_ANALYTICS
            UnityEngine.Analytics.Analytics.CustomEvent(pushInitEvent, new Dictionary<string, object>
			    {
				    { "timeSinceStartup", Time.realtimeSinceStartup }
			    });
#endif
		}
		else Invoke("Shutdown", 7);	        
#endif
    }

#if MAGE_ANALYTICS && !MAGE_NO_PUSH
    void OnApplicationFocus(bool paused)
    {
        if(paused == false)
            _timePause = Time.realtimeSinceStartup;        
    }
#endif

#if MAGE_PUSH_DEBUG && !MAGE_NO_PUSH
	private Rect _rectTagSendButton = new Rect(8, 10, 210, 32);	
	private Rect _rectTagSendButton2 = new Rect(222, 10, 124, 32);	
	private Rect _rectTagSendButton3 = new Rect(364, 10, 150, 32);	
	void OnGUI()
	{
		GUIStyle customTextSize = new GUIStyle("button");
		customTextSize.fontSize = 22;
		if (GUI.Button(_rectTagSendButton, "Send TestDev Tag", customTextSize)) 
		{
			OneSignal.SendTag("TestDevice", "yes");	
            // OneSignal.DeleteTags(new List<string>() {"UnityTestKey2", "UnityTestKey3" });
		}
		if (GUI.Button(_rectTagSendButton2, "Subscribe", customTextSize))
			OneSignal.SetSubscription(true);
		if (GUI.Button(_rectTagSendButton3, "UnSubscribe", customTextSize))
			OneSignal.SetSubscription(false);
	}
#endif
}
