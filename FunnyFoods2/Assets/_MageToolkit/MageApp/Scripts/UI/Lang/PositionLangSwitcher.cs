﻿using UnityEngine;
using System.Collections.Generic;

[DisallowMultipleComponent]
public class PositionLangSwitcher:BaseLangSwitcher 
{
    #region fields
    public PositionLangSwitcherSettings localPositions;
	Vector3 _defLocalPosition = Vector3.zero;
    #endregion

    #region override methods
    protected override void OnChangeLangUI(MageAppUILang newLang)
	{
		transform.localPosition = localPositions.positions.SafeGet(newLang, _defLocalPosition);
	}
	
	protected override void DoInitAwake()
	{
		if(localPositions.positions.ContainsKey(MageAppUILang.none)) 
			_defLocalPosition = localPositions.positions[MageAppUILang.none];
		transform.localPosition = localPositions.positions.SafeGet(AppState.instance.CurrentLang,_defLocalPosition);
	}
    #endregion

    #region nested types

    [System.Serializable]
    public class PositionLangSwitcherSettings : FullInspector.BaseObject
    {
        [FullInspector.InspectorComment(FullInspector.CommentType.None, "localPosition для активного языка. None - как значение поумолчанию.")]
        public Dictionary<MageAppUILang, Vector3> positions;
    }
    #endregion
}
