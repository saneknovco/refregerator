﻿using UnityEngine;

public class BaseLangSwitcher : MonoBehaviour {

	protected virtual void OnChangeLangUI(MageAppUILang newLang) { }
	protected virtual void DoInitAwake() { }

	void Awake()
	{
		DoInitAwake();
		OnChangeLangUI(AppState.instance.CurrentLang);
        Messenger.AddListener<MageAppUILang>(AppState.EventTypes.langUI, OnChangeLangUI);
	}	
	
	void OnDestroy()
	{
        if (Messenger.eventTable.ContainsKey(AppState.EventTypes.langUI))
            Messenger.RemoveListener<MageAppUILang>(AppState.EventTypes.langUI, OnChangeLangUI);
	}
}
