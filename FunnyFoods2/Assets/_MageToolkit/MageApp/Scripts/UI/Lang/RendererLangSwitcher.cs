﻿using UnityEngine;
using System.Collections.Generic;

[DisallowMultipleComponent]
public class RendererLangSwitcher :BaseLangSwitcher
{
	[FullInspector.InspectorHeader("Параметры Lang Switcher:")]
	[FullInspector.InspectorMargin(16)]
	public RendererLangSwitcherSettings settings;

	[System.NonSerialized]
	private Dictionary<MageAppUILang, Renderer> _activateRenderers = new Dictionary<MageAppUILang, Renderer>();

	[System.NonSerialized]
	private Dictionary<MageAppUILang, Renderer> _deactivateRenderers = new Dictionary<MageAppUILang, Renderer>();

	protected override void OnChangeLangUI(MageAppUILang newLang)
	{
		foreach (var item in _activateRenderers)
			item.Value.enabled = item.Key == newLang;		
		foreach (var item in _deactivateRenderers)
			item.Value.enabled = item.Key != newLang;	
	}

	protected override void DoInitAwake()
	{
		Renderer rnd;

		if (settings.activateRendererCurrLang != null)
		{
			foreach (var item in settings.activateRendererCurrLang)
			{
				if (item.Value != null)
				{
					rnd = item.Value.GetComponent<Renderer>();
					if (rnd != null) _activateRenderers.Add(item.Key, rnd);
				}
			}
		}

		if (settings.deactivateRendererCurrLang != null)
		{
			foreach (var item in settings.deactivateRendererCurrLang)
			{
				if (item.Value != null)
				{
					rnd = item.Value.GetComponent<Renderer>();
					if (rnd != null) _deactivateRenderers.Add(item.Key, rnd);
				}
			}
		}
	}

    #region nested types
    [System.Serializable]
    public class RendererLangSwitcherSettings : FullInspector.BaseObject
    {
        [FullInspector.InspectorComment(FullInspector.CommentType.None, "Словарь объектов из которых активируется для активного языка")]
        public Dictionary<MageAppUILang, GameObject> activateRendererCurrLang;
        [FullInspector.InspectorComment(FullInspector.CommentType.None, "Словарь объектов из которого активируются для неактивных языков")]
        public Dictionary<MageAppUILang, GameObject> deactivateRendererCurrLang;
    }
    #endregion
}
