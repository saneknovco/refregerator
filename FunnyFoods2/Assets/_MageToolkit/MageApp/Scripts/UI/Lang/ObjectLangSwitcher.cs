﻿using UnityEngine;
using System.Collections.Generic;

[DisallowMultipleComponent]
public class ObjectLangSwitcher :MonoBehaviour
{
    [System.Serializable]
    public class ObjectLangSwitcherSettings : FullInspector.BaseObject
    {
        [FullInspector.InspectorMargin(18)]
        [FullInspector.InspectorComment("словарь локализации объектов")]
        public Dictionary<MageAppUILang, GameObject> langObjects;
    }

    public ObjectLangSwitcherSettings settings;

    private MageAppUILang _curentLang = MageAppUILang.none;

    void OnChangeLangUI(MageAppUILang newLang)
    {
        if (settings.langObjects == null) return;
        if (_curentLang == newLang) return;
        _curentLang = settings.langObjects.ContainsKey(newLang) ? newLang : AppSettings.instance.UI.defaultLang;
        foreach (var item in settings.langObjects.Keys)
		{
            if (settings.langObjects[item] != null)
                settings.langObjects[item].SetActive(_curentLang == item);        
		}
    }
    
    void  Awake()
    {   
        OnChangeLangUI(AppState.instance.CurrentLang);
        Messenger.AddListener<MageAppUILang>(AppState.EventTypes.langUI, OnChangeLangUI);
    }    

    void OnDestroy()
    {        
        if (Messenger.eventTable.ContainsKey(AppState.EventTypes.langUI))
            Messenger.RemoveListener<MageAppUILang>(AppState.EventTypes.langUI, OnChangeLangUI);
    }

}
