﻿using UnityEngine;
using System.Collections;

[DisallowMultipleComponent]
public class MeshLangSwitcher : BaseLangSwitcher
{
    public string rootTextureLocation = @"UI/Textures";
    public string rootMeshLocation = @"UI/Meshes";

    [Tooltip("Использовать режим выбора текстур и мешей с префиксом _uni для языков с неполной локализацией")]
    public bool uniMode = false;

    private Renderer _renderer = null;

    protected override void DoInitAwake()
    {
        _renderer = GetComponent<Renderer>();
    }

    protected override void OnChangeLangUI(MageAppUILang newLang)
    {
        string uri = string.Empty;
        string lang = System.Enum.GetName(newLang.GetType(), newLang);
        if (uniMode)
        {
            if (!MgCommonUtils.IsFullSupportLang(newLang)) lang = AppConfig.langToUniAssets;
        }        

        if (!string.IsNullOrEmpty(rootMeshLocation))
        {
            uri = rootMeshLocation + "/" + this.name + "_" + lang + AppConfig.sufixToMesh;
            var mesh = Resources.Load<Mesh>(uri);
            if(mesh == null)
            {
                if (uniMode && lang != AppConfig.langToUniAssets)
                {
                    lang = AppConfig.langToUniAssets;
                    uri = rootMeshLocation + "/" + this.name + "_" + lang + AppConfig.sufixToMesh;
                    mesh = Resources.Load<Mesh>(uri);
                }
                if (mesh == null)
                {
                    lang = AppSettings.instance.UI.defaultLangStr;
                    uri = rootMeshLocation + "/" + this.name + "_" + lang + AppConfig.sufixToMesh;
                    mesh = Resources.Load<Mesh>(uri);
                }
            }
            if (mesh != null)
            {
                MeshFilter meshFilter = GetComponent<MeshFilter>();
                if (meshFilter == null) meshFilter = gameObject.AddComponent<MeshFilter>();
                meshFilter.sharedMesh = mesh;
            }           
        }
        if (!string.IsNullOrEmpty(rootTextureLocation))
        {            
            uri = rootTextureLocation + "/" + this.name + "_" + lang;
            var texture = Resources.Load<Texture2D>(uri);
            if (texture == null)
            {
                if (uniMode && lang != AppConfig.langToUniAssets)
                {
                    lang = AppConfig.langToUniAssets;
                    uri = rootTextureLocation + "/" + this.name + "_" + lang;
                    texture = Resources.Load<Texture2D>(uri);
                }
                if(texture == null)
                {
                    lang = AppSettings.instance.UI.defaultLangStr;
                    uri = rootTextureLocation + "/" + this.name + "_" + lang;
                    texture = Resources.Load<Texture2D>(uri);
                }
            }
			uri = rootTextureLocation + "/" + this.name + "_" + lang + AppConfig.sufixToAlphaTexture;
            var textureAlpha = Resources.Load<Texture2D>(uri);  
            if (texture != null)
            {
                if (_renderer == null) _renderer = GetComponent<Renderer>();
                if (_renderer != null)
                {
                    var mat = _renderer.material;
                    mat.mainTexture = texture;
					//set alpha mask
					if (textureAlpha != null)
					{
						for (int i = 0; i < AppConfig.propAlphaTextureInShader.Length; i++) 
						{
							if (mat.HasProperty (AppConfig.propAlphaTextureInShader [i])) 
							{
								mat.SetTexture (AppConfig.propAlphaTextureInShader [i], textureAlpha);
								break;
							}						
						}
					}
                }
            }
        }
    }
}
