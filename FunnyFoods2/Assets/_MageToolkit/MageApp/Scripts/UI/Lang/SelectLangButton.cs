﻿using UnityEngine;

[DisallowMultipleComponent]
public class SelectLangButton : ButtonWidget 
{
	[FullInspector.InspectorCategory("Action")]
	[FullInspector.InspectorHeader("Параметры Select Lang:")]
	[FullInspector.InspectorMargin(16)]
	public MageAppUILang lang;
	
	private AppState _appState;
	
	protected override void DoAwake()
	{        	
		_appState = AppState.instance;
	}

	protected override bool DoBreakTouch ()
	{
		return lang == _appState.CurrentLang;
	}

	protected override void DoTouch()
	{
		_appState.CurrentLang = lang;	
	}
}
