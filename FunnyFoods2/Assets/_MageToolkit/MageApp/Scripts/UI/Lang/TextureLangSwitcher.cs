﻿using UnityEngine;

[DisallowMultipleComponent]
public class TextureLangSwitcher : BaseLangSwitcher
{
	public string rootLocation = @"UI/Textures";

	[Tooltip("Использовать режим выбора текстур с префиксом _uni для языков с неполной локализацией")]
	public bool uniMode = false;

	private Renderer _renderer = null;

	protected override void DoInitAwake()
	{
		_renderer = GetComponent<Renderer>();
	}

	protected override void OnChangeLangUI(MageAppUILang newLang)
	{
        if (string.IsNullOrEmpty(rootLocation)) return;
        
        string lang = System.Enum.GetName(newLang.GetType(), newLang);
		if (uniMode)
		{
			if (!MgCommonUtils.IsFullSupportLang(newLang)) lang = AppConfig.langToUniAssets;
		}

		var uri = rootLocation + "/" + this.name + "_" + lang;
		var texture = Resources.Load<Texture2D>(uri);
        if (texture == null)
        {
            if(uniMode && lang != AppConfig.langToUniAssets)
            {
                lang = AppConfig.langToUniAssets;
                uri = rootLocation + "/" + this.name + "_" + lang;
                texture = Resources.Load<Texture2D>(uri);
            }
            if (texture == null)
            {
                lang = AppSettings.instance.UI.defaultLangStr;
                uri = rootLocation + "/" + this.name + "_" + lang;
                texture = Resources.Load<Texture2D>(uri);                
            }
        }       
      
		uri = rootLocation + "/" + this.name + "_" + lang + AppConfig.sufixToAlphaTexture;
		var textureAlpha = Resources.Load<Texture2D>(uri);
		if (texture != null)
		{
			if (_renderer == null) _renderer = GetComponent<Renderer>();
			if (_renderer != null)
			{
				var mat = _renderer.material;
				mat.mainTexture = texture;
				if (textureAlpha != null)
				{
					for (int i = 0; i < AppConfig.propAlphaTextureInShader.Length; i++) 
					{
						if (mat.HasProperty (AppConfig.propAlphaTextureInShader [i])) 
						{
							mat.SetTexture (AppConfig.propAlphaTextureInShader[i], textureAlpha);
							break;
						}						
					}				
				}
			}
		} 
	}
}
