﻿using UnityEngine;
using System.Collections;

[DisallowMultipleComponent]
[RequireComponent(typeof(Collider))]
public class FVTSwitchAppButton : MonoBehaviour
{
	#region Fields	

    public Camera workCamera;
    public float inactivityTime = 2.0f;
    public bool enabledEffect = true;
    [Header("Wobble effect кнопки:")]
    public TouchEffectParameters touchEffect = new TouchEffectParameters();

    private float _startTime = 0f;
    private Vector3 _initialScale = Vector3.one;

    private TKTapRecognizer      _recognizerTap = null;
    private TKAnyTouchRecognizer _recognizer = null;

    private bool __multiTouchState = false;
    private bool _isDestroying = false;
    private bool _isLeaveTouch = false;

    private Vector2 _mousePos2D;
    private Vector3 _mousePos3D;
    private Collider _collider;
    private Ray _ray;

    #endregion

    #region Action

    private IEnumerator AsyncInitRecognizer()
    {
        if(_recognizerTap != null) yield break;

        yield return null;
        yield return null;

        #region TouchKit config
        //enabled multiTouch state      
        __multiTouchState = Input.multiTouchEnabled;
        Input.multiTouchEnabled = true;

        //recognizer for TouchKit
        var boundary = new TKRect(0, 0, Screen.width, Screen.height);

        _recognizer = new TKAnyTouchRecognizer(boundary);
        _recognizer.onEnteredEvent += (r) =>
         {
             _isLeaveTouch = true;
             __asyncReaction = false;
         };

        _recognizerTap = new TKTapRecognizer(0.7f, 6f);
        _recognizerTap.boundaryFrame = boundary;
        _recognizerTap.gestureRecognizedEvent += (r) =>
        {
            if (_isDestroying) return;

            _mousePos2D = r.touchLocation();
            _mousePos3D = new Vector3(_mousePos2D.x, _mousePos2D.y, 0.0f);
            _ray = workCamera.ScreenPointToRay(_mousePos3D);
            RaycastHit hit;
            if (_collider.Raycast(_ray, out hit, 10000f))
            {
                if (r.state == TKGestureRecognizerState.Recognized)
                {
                    _isLeaveTouch = true;
                    MgAudioHelper.Click();
                    if (Messenger.eventTable.ContainsKey(AppState.EventTypes.fvtAction))
                        Messenger.Broadcast<FVTAction>(AppState.EventTypes.fvtAction, FVTAction.OpenApp);
                }
            }
            else
            {
                if (r.state == TKGestureRecognizerState.Recognized)
                {
                    _isDestroying = true;
                    _isLeaveTouch = true;
                    if (Messenger.eventTable.ContainsKey(AppState.EventTypes.fvtAction))
                        Messenger.Broadcast<FVTAction>(AppState.EventTypes.fvtAction, FVTAction.CloseUIView);
#if !UNITY_IOS
                    else Application.Quit();
#endif
                }
            }
        };

        TouchKit.addGestureRecognizer(_recognizerTap);
        TouchKit.addGestureRecognizer(_recognizer);

        //FIX: temp stub for cradle & etc.
        _recognizer.enabled = true;
        _recognizerTap.enabled = true;
        #endregion

    }

    private bool __asyncReaction = false;
    private IEnumerator AsyncReactionEffect()
    {       
        if (__asyncReaction) yield break;

        __asyncReaction = true;
        bool isReaction = false;		
        while (__asyncReaction)
        {           
            float tw = Random.Range(inactivityTime, inactivityTime + 2.1f);
            yield return new WaitForSeconds(tw);
            _startTime = Time.time;
            isReaction = true;
           
            while (isReaction && !_isLeaveTouch)
            {
                Vector3 scale = Vector3.Scale(_initialScale, touchEffect.extraScale);
                float t = (Time.time - _startTime) / touchEffect.duration;
                isReaction = touchEffect.WobbleReaction(t, ref scale);
                if (isReaction) transform.localScale = scale;                
                yield return null;
            }
            isReaction = false;
			if (_isLeaveTouch) transform.localScale = touchEffect.selectedScale;
            else transform.localScale = _initialScale;           
        }		
        yield break;
    }

    #endregion

    #region Unity event functions

    void Awake()
    {      
        _initialScale = transform.localScale;
        _collider = GetComponent<Collider>();
        if (workCamera == null) {            
            if(Camera.allCamerasCount>=1)
                workCamera = Camera.allCameras[0];
        }

        StartCoroutine(AsyncInitRecognizer());
        if (enabledEffect) StartCoroutine(AsyncReactionEffect());
    }

    void OnDestroy()
    {       
        //restore multiTouch state
        Input.multiTouchEnabled = __multiTouchState;

        if (_recognizer != null)
            TouchKit.removeGestureRecognizer(_recognizer);
        if (_recognizerTap != null)
            TouchKit.removeGestureRecognizer(_recognizerTap);
        _recognizer = null;
        _recognizerTap = null;        
    }

    #endregion
}
