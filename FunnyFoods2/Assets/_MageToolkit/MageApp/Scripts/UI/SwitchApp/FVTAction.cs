﻿/// <summary>
/// действия(команды) для отслеживание полной версии
/// <para>implementation: FVTSwitchAppController </para>
/// </summary>
[System.Serializable]
public enum FVTAction 
{
    None = 0,
    /// <summary>
    /// Создание диалога выбора полной версии, например при старте приложения или при покупки и т.п.
    /// </summary>
    CreateUIView = 1,
    /// <summary>
    /// Закрытие диалога выбора полной версии приложения.
    /// <para>сalled when the user has touch close button</para>
    /// </summary>
    CloseUIView = 2,
    /// <summary>
    /// Запуск полной версии приложения
    /// </summary>
    OpenApp = 3
}
