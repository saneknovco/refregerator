﻿using UnityEngine;
using System.Collections;
using System.Linq;
using System.Collections.Generic;

/// <summary>
/// контроллер отслеживания полной версии приложения в системе
/// </summary>
public class FVTSwitchAppController : GlobalSingletonBehaviour<FVTSwitchAppController>
{
    static private bool __firstParce = true;

    GameObject _uiView = null;
    string _afvUrl = string.Empty;

    #region methods of Controller

#if UNITY_ANDROID && MAGE_FORCE_QUIT
    private static int __cnt = 0;
#endif
    private void RestartLevel()
    {
        if (__firstParce) __firstParce = false;
        
        if (Messenger.eventTable.ContainsKey(AppState.EventTypes.sysRestartApp))
            Messenger.Broadcast(AppState.EventTypes.sysRestartApp);

        CleanAppObject();

        Camera tmpCamera = new GameObject("TmpCamera", typeof(Camera)).GetComponent<Camera>();
        tmpCamera.cullingMask = 0;
        tmpCamera.backgroundColor = Color.white;
		tmpCamera.clearFlags = CameraClearFlags.SolidColor;// !!!!!!!!! Иначе будет мелькать вид по умолчанию с горизонтом

        StartCoroutine(MgCoroutineHelper.WaitThenCallback(AppConfig.frameWaitAsync, () =>
        {
            if (tmpCamera != null) {
                tmpCamera.gameObject.SetActive(false);
                GameObject.Destroy(tmpCamera);
                tmpCamera = null;
            }
            var hook = AppState.instance;
            hook.ResetState();
            UnityEngine.SceneManagement.SceneManager.LoadScene(0);
#if UNITY_ANDROID && MAGE_FORCE_QUIT
			__cnt++;
            if (__cnt > 1)Application.Quit();
#endif
        }));
    }

    private void CleanAppObject()
    {
        var go_all = GameObject.FindObjectsOfType<Transform>();
        if (go_all== null) return;
        var gos = (from item in go_all
                       where (
                                (item.parent == null) && (item.gameObject != gameObject)
								&& (item.gameObject.name != @"prime[31]") 
                                && (item.gameObject.name != @"MessengerHelper")								                              
								&& (item.gameObject.name != @"_TaskManager")
								&& (item.gameObject.name != @"MobileAppTracker")
								&& (item.gameObject.name != @"[DOTween]")
                                && (!item.gameObject.name.StartsWith(AppConfig.prefixAutoObject))
                            )
                       select item.gameObject).ToArray<GameObject>();

        if(gos!=null) {
            int n = gos.Length;
            for(int i=0; i<n; i++) {
               gos[i].SetActive(false);
               GameObject.Destroy(gos[i]);
               gos[i] = null;
            }
        }
    }

    IEnumerator AsyncTrackApp( int waitFrames )
    {        
        if (_uiView != null) yield break;
        if (!SysUtilsProxy.AppIsInstalled(_afvUrl)) yield break;
        for (int i = 0; i < waitFrames; i++) yield return null;
        CleanAppObject();            
        yield return null;        
        DoCreateUIView();
        yield break;
    }

    #endregion

    #region FVT_MESSAGE

    private void OnAction(FVTAction action)
    {		
		switch (action)
        {
            case FVTAction.CreateUIView:                
                StartCoroutine(AsyncTrackApp(1));
                break;
            case FVTAction.CloseUIView:
                DoCloseUIView();
                break;
            case FVTAction.OpenApp:
                DoOpenApp();
                break;
            default:
                Debug.LogError("FVTSwitchAppController:OnAction not support " + action);
                break;
        }
    }

    private void DoCreateUIView()
    {
        if (_uiView != null) return;
        string uri = @"UI/SwitchAppUIView";        
        if (Screen.width < Screen.height) uri += AppConfig.sufixToPortrait;
        var hRes = Resources.Load(uri);
        if (hRes == null) return;
#if MAGE_ANALYTICS
		UnityEngine.Analytics.Analytics.CustomEvent(this.GetType().Name, new Dictionary<string, object>
			{
				{"action", "create"},
				{ "timeSinceStartup", Time.realtimeSinceStartup}
			});
#endif
		_uiView = GameObject.Instantiate(hRes, Vector3.zero, Quaternion.identity) as GameObject;
        if (_uiView == null) DoCloseUIView();        
    }

    private void DoCloseUIView()
    {   
#if MAGE_ANALYTICS
		UnityEngine.Analytics.Analytics.CustomEvent(this.GetType().Name, new Dictionary<string, object>
			{
				{"action", "close"},
				{ "timeSinceStartup", Time.realtimeSinceStartup}
			});
#endif
		_uiView = null;
		RestartLevel();
    }

    private void DoOpenApp()
    {
		if (SysUtilsProxy.AppIsInstalled(_afvUrl))
		{
#if MAGE_ANALYTICS
		UnityEngine.Analytics.Analytics.CustomEvent(this.GetType().Name, new Dictionary<string, object>
			{
				{"action", "open"},
				{ "timeSinceStartup", Time.realtimeSinceStartup}
			});
#endif
			SysUtilsProxy.OpenApp(_afvUrl);
		}
		else DoCloseUIView();
    }   

    #endregion

    #region override

    protected override void DoAwake()
    {
        base.DoAwake();

		var appId = AppSettings.instance.appInfo.fullVersionAppId;
        if (string.IsNullOrEmpty(appId) || appId.Length < 9){
            GameObject.Destroy(this.gameObject);
            return;
        }
        Messenger.AddListener<FVTAction>(AppState.EventTypes.fvtAction, OnAction);
		_afvUrl = AppSettings.instance.appInfo.GetFullVersionAppUrl();
        if (__firstParce) {
            if (SysUtilsProxy.AppIsInstalled(_afvUrl)) 
                StartCoroutine(AsyncTrackApp(AppConfig.fvtFrameWaitAsync));
        }
    }

    protected override void DoDestroy()
    {
        if (Messenger.eventTable.ContainsKey(AppState.EventTypes.fvtAction))
            Messenger.RemoveListener<FVTAction>(AppState.EventTypes.fvtAction, OnAction);
        __firstParce = true;
        base.DoDestroy();
    }

    #endregion

    #region Unity event functions   

    void OnApplicationPause(bool pauseStatus)
    {
        if (pauseStatus) return;		
        if (SysUtilsProxy.AppIsInstalled(_afvUrl)) StartCoroutine(AsyncTrackApp(0));
		else {
			if(_uiView != null) DoCloseUIView();
		}
    }
    #endregion
      
}
