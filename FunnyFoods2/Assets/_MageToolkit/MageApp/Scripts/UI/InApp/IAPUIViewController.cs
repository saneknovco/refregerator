﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

[DisallowMultipleComponent]
public class IAPUIViewController : MonoBehaviour
{
	#region Fields
	public GameObject textIAPMessage;
    private TextMesh _text;

    #endregion

    #region IAP Text messages

    [System.NonSerialized]
    private Dictionary<MageAppUILang, string[]> _textMessages = new Dictionary<MageAppUILang, string[]>()
                { 
                    { 
                        MageAppUILang.ru,
                        new string[]
                        {
#if UNITY_ANDROID
                            "Подключение к Google Play!\r\nПожалуйста, подождите...",
                            "Не удается подключиться к Google Play!\r\nУбедитесь, что Ваше сетевое соединение активно,\r\nи повторите попытку",
#else
                            "Подключение к iTunes App Store!\r\nПожалуйста, подождите...",
	                        "Не удается подключиться к iTunes App Store!\r\nУбедитесь, что Ваше сетевое соединение активно,\r\nи повторите попытку",
#endif
                            "На устройстве недостаточно\r\nсвободного места для загрузки\r\nинформации о IN-APP PURCHASES!",
                            "Пожалуйста, подождите...",
							"ПОКУПКА ПРЕРВАНА! ",
                            "Обновление покупок..."
                        }
                    },
                    { 
                        MageAppUILang.en,
                        new string[]
                        {
#if UNITY_ANDROID
                            "Connecting to Google Play!\r\nPlease wait...",
                            "Cannot connect to Google Play!\r\nPlease check your internet connection\r\nand try again!",
#else
                            "Connecting to iTunes App Store!\r\nPlease wait...",
							"Cannot connect to iTunes App Store!\r\nPlease check your internet connection\r\nand try again!",
#endif
                            "THE DEVICE DOES NOT HAVE\r\nENOUGH FREE SPACE FOR DOWNLOADING\r\nIN-APP PURCHASES INFORMATION!",
                            "Please wait...",
							"PURCHASES CANCELED! ",
                            "Updating purchases..."  
                        }
                    }
                };   

	private string[] _curTextMessages;
	private void UpdateTextMessage(int textMessageId)
	{
		if(_text) {
			_text.text = string.Empty;
			if (textMessageId >=0 && textMessageId < _curTextMessages.Length )
				_text.text = _curTextMessages[textMessageId];
		}
	}

#endregion

#region IAP: ACTION & MESSANGER CALLBACKS
	private void AutoClose ( int textMessageId = 1, float waitTime = 3f)
	{		
        UpdateTextMessage(textMessageId);
        //блокируем all  buttons
        Messenger.Broadcast<List<IAPAction>, bool>(AppState.EventTypes.iapUICommandCanExecute,
            new List<IAPAction>() { IAPAction.Purchase, IAPAction.PurchaseAll, IAPAction.PurchaseFull, IAPAction.Restore, IAPAction.Close }, false);
		StartCoroutine(MgCoroutineHelper.WaitThenCallback(waitTime, ()=>{
			DoClose();
		}));
    }

    private bool _didOpen = false;
    private void OnRequestProductComplete(bool didSucceed)
    {        
        _inProcess = false;
        _didOpen = didSucceed;
        if (didSucceed) {
            if (_text) _text.text = string.Empty;
            Messenger.Broadcast<List<IAPAction>, bool>(AppState.EventTypes.iapUICommandCanExecute,
                new List<IAPAction>() { IAPAction.Purchase, IAPAction.PurchaseAll, IAPAction.Restore }, true);
        }
        else AutoClose(1, 3f);
    }

    private bool _inProcess = false;
    //private bool __OneCallRestoreComplete = true;   
    private bool _didRefreshing = false;
    void OnActionComplete(IAPAction action, string productId, bool didSucceed, string error)
    {        
        switch (action)
        {
            case IAPAction.Restore:
                    _inProcess = false;
                    _didRefreshing = true;
                    if (_text) _text.text = string.Empty;					
                    DoClose();
                break;
               
            case IAPAction.Purchase:
			case IAPAction.PurchasePick:
			case IAPAction.PurchaseAll:                
				_inProcess = false;
                if (didSucceed) {
                    _didRefreshing = true;
                    if (_text) _text.text = string.Empty;
                    DoClose();
                }
                else {
                    UpdateTextMessage(4);
                    //WARNING: доп инфо
                    if (_text) _text.text += "\r\n" + error;                    
                    Messenger.Broadcast<List<IAPAction>, bool>(AppState.EventTypes.iapUICommandCanExecute,
						new List<IAPAction> { IAPAction.Purchase, IAPAction.PurchaseAll, IAPAction.PurchaseFull, IAPAction.Restore, IAPAction.Close }, true);
                }
                break;                
        }
    }
   
	private void OnUICommand(IAPAction action, string productId)
    {      
		if(_inProcess && (action != IAPAction.Close)) return;
        switch (action)
        {
            case IAPAction.Close:
                DoClose();
                break;

            case IAPAction.Restore:
            case IAPAction.Purchase:
			case IAPAction.PurchaseAll:
			case IAPAction.PurchasePick:
				DoAction(action, productId);
                break;

			default: 
                Debug.LogError("IAPUIViewController:OnUICommandAction: not supported command: ".AddTime() + action);	
				break;            
        }
    }

    private bool _isClosing = false;
    void DoClose()
    {
        // == WARNING ===
		//if (_inProcess || _isClosing) return;
        if (_isClosing) return;

        _isClosing = true;
        UpdateTextMessage(5);
        //блокируем все кнопки перед удалением префаба
        Messenger.Broadcast<List<IAPAction>, bool>(AppState.EventTypes.iapUICommandCanExecute,
           new List<IAPAction> { IAPAction.Purchase, IAPAction.PurchaseAll, IAPAction.PurchaseFull, IAPAction.Restore, IAPAction.Close }, 
           false);  

        StartCoroutine(MgCoroutineHelper.WaitThenCallback(5, () => { 
            if (Messenger.eventTable.ContainsKey(AppState.EventTypes.iapCloseUIView))
                Messenger.Broadcast<bool>(AppState.EventTypes.iapCloseUIView, _didRefreshing);
        }));
    }

    void DoAction( IAPAction action, string productId )
    {
        if (!_didOpen || _inProcess || _isClosing) return;        
        _inProcess = true;
        UpdateTextMessage(3);
        Messenger.Broadcast<List<IAPAction>, bool>(AppState.EventTypes.iapUICommandCanExecute,
            new List<IAPAction> { IAPAction.Purchase, IAPAction.PurchaseAll, IAPAction.PurchaseFull, IAPAction.Restore },
            false);
        Messenger.Broadcast<IAPAction, string>(AppState.EventTypes.iapAction, action, productId);
    }

#endregion

#region UNITY EDITOR

#if UNITY_EDITOR
	//void Reset()
    //{
    //    // предотвращает создание второго экземпляра скрипта если он есть у дочерних или родит. объектов
    //    bool lockNewInstance = GetComponentInParent<IAPUIViewController>() != null;
    //    if (lockNewInstance == false) lockNewInstance = GetComponentInChildren<IAPUIViewController>() != null;
    //    if (lockNewInstance) Invoke("DestroyThis", 0);        
    //}

    //void DestroyThis()
    //{
    //    Debug.LogError("ERROR: IAPUIViewController already exist at scene!".AddTime());
    //    DestroyImmediate(this);
    //}
#endif

#endregion

#region Unity event functions

    void Awake()
    {
        Messenger.AddListener<bool>(AppState.EventTypes.iapRequestProductDataComplete, OnRequestProductComplete);
        Messenger.AddListener<IAPAction, string, bool, string>(AppState.EventTypes.iapActionComplete, OnActionComplete);
        Messenger.AddListener<IAPAction, string>(AppState.EventTypes.iapUICommand, OnUICommand);

        if (textIAPMessage == null) textIAPMessage = gameObject.FindChild(@"TextIAPMessage");
        if (textIAPMessage) _text = textIAPMessage.GetComponent<TextMesh>();

        _curTextMessages = _textMessages.SafeGetByDefKey(AppState.instance.CurrentLang,AppSettings.instance.UI.defaultLang);
    }

	void Start ()
    {
        UpdateTextMessage(0);        
        ///// ================        
        _didRefreshing = false;               
        _inProcess = false;
        _isClosing = false;
        ///// ================  

		if( Application.internetReachability == NetworkReachability.NotReachable) {
			AutoClose(1, 3f);
			return;		
		}
        if (SysUtilsProxy.FreeDiskSpace < InAppProducts.instance.minFreeSpace) {
            AutoClose(2, 5f);
            return;
        }

        Messenger.Broadcast<List<IAPAction>, bool>(AppState.EventTypes.iapUICommandCanExecute,
												new List<IAPAction>() { IAPAction.Purchase, IAPAction.PurchaseAll, IAPAction.Restore }, false);
        Messenger.Broadcast<List<IAPAction>, bool>(AppState.EventTypes.iapUICommandCanExecute,
                                                    new List<IAPAction>() {IAPAction.PurchaseFull, IAPAction.Close }, true);
       
        _inProcess = true;
        _didOpen = false;      
        Messenger.Broadcast(AppState.EventTypes.iapRequestProductData);
	}  

    void OnDestroy()
    {       
        if (Messenger.eventTable.ContainsKey(AppState.EventTypes.iapRequestProductDataComplete))
            Messenger.RemoveListener<bool>(AppState.EventTypes.iapRequestProductDataComplete, OnRequestProductComplete);
        if (Messenger.eventTable.ContainsKey(AppState.EventTypes.iapActionComplete))
            Messenger.RemoveListener<IAPAction, string, bool, string>(AppState.EventTypes.iapActionComplete, OnActionComplete);
        if (Messenger.eventTable.ContainsKey(AppState.EventTypes.iapUICommand))
            Messenger.RemoveListener<IAPAction, string>(AppState.EventTypes.iapUICommand, OnUICommand);
    }

#endregion
}
