﻿using UnityEngine;
using System.Collections;

public class IAPUILinkedLock : MonoBehaviour 
{
	public GameObject lockButton;
	public string     productId;

	private bool __unlockProduct = false;

	void DoUnlockProductComplete (string p_productId, GameObject p_lockButton)
	{
		if (__unlockProduct) return;
		if(!string.IsNullOrEmpty(productId))
		{
			if(productId == p_productId) GameObject.Destroy(gameObject);
		}
		else if(lockButton != null)
		{
			if(lockButton == p_lockButton) GameObject.Destroy(gameObject);
		}
	}

	void DoCheckUnlockProduct()
	{
		if (__unlockProduct) return;
		if (!string.IsNullOrEmpty(productId))
		{
			__unlockProduct = InAppProducts.instance.GetUnlock(productId);
			if (__unlockProduct)
			{
				// сразу делаем неактивным
				if (gameObject != null)
				{
					gameObject.SetActive(false);
					// затем удаляем
					Destroy(gameObject);
					//Destroy(gameObject, AppConfig.timeWaitFinishing);
				}
			}
		}
	}

	void Awake()
	{
        Messenger.AddListener<string, GameObject>(AppState.EventTypes.iapUnlockProductComplete, DoUnlockProductComplete);
        DoCheckUnlockProduct();		
	}

    void OnEnable()
    {
        DoCheckUnlockProduct();
    }

    void OnDestroy()
	{
		if (Messenger.eventTable.ContainsKey(AppState.EventTypes.iapUnlockProductComplete))
			Messenger.RemoveListener<string, GameObject>(AppState.EventTypes.iapUnlockProductComplete, DoUnlockProductComplete);
	}
}
