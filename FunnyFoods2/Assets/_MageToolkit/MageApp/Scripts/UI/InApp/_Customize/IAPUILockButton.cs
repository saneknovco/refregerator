﻿using UnityEngine;
using System.Collections;

/// <summary>
///Cтандартная реализация для "замка" на некупленном продукте.
///<para>Для создание customize логики разблокировки:  </para>
///<para>в переодпределить метод DoUnlockProduct</para>
///</summary>
public class IAPUILockButton : ButtonWidget
{    
    [FullInspector.InspectorCategory("IAP")]
    [FullInspector.InspectorHeader("Параметры IAP:")]
    [FullInspector.InspectorMargin(16)]
    public string productId;
    [FullInspector.InspectorMargin(12)]
    [FullInspector.InspectorCategory("IAP")]
    public GameObject parentInAppUIView = null;
    [FullInspector.InspectorCategory("IAP")]
    public bool forceUIContainer = true;
    private InAppProducts _products;
    private bool __unlockProduct = false;

    private bool _iapEnable = true;
	private string _beforeCreateUIView = string.Empty;
	private string _cancelCreateUIView = string.Empty;

	private AppSettings.IAPSettings _iapSettings;

    /// <summary>
    /// Рекция на разблокировку контента
    /// <para>Виртуальный метод! Перегружается в кастомном наследники</para>
    /// </summary>
    /// <param name="productRefreshed"></param>
    protected  virtual void DoUnlockProduct(bool productRefreshed)
    {
        if (__unlockProduct || !productRefreshed) return;
        __unlockProduct = _products.GetUnlock(productId);
        if (__unlockProduct)
        {
			//see:IAPUILinkedLock.cs for example
			if (Messenger.eventTable.ContainsKey(AppState.EventTypes.iapUnlockProductComplete))
				Messenger.Broadcast<string, GameObject>(AppState.EventTypes.iapUnlockProductComplete, productId, gameObject);
			#if !MAGE_NO_PM
			if (_iapSettings.broadcastPMEvents)
			{
				//Debug.Log("pm: ".AddTime() + AppState.PMEvents.iapUnlockProductComplete);
				PlayMakerFSM.BroadcastEvent(AppState.PMEvents.iapUnlockProductComplete);
			}
			#endif	
			// сразу делаем неактивным
			if (gameObject != null)
			{
				gameObject.SetActive(false);
				// затем удаляем
				Destroy(gameObject, AppConfig.timeWaitFinishing);
			}
        }      
    }

    /// <summary>
    /// вызывается когда проходит транзакция IAP (покупка, etc)
    /// </summary>
    /// <param name="command"></param>
    /// <param name="a_productId"></param>
    /// <param name="didSucceed"></param>
    /// <param name="error"></param>
    private void OnActionComplete(IAPAction command, string a_productId, bool didSucceed, string error)
    {
		if ( !(enabled && gameObject.activeSelf) ) return;
		if (didSucceed == false) return;
		if (productId != a_productId)
		{
			if(_products.CanIapPurchaseAll && (a_productId != _products.ProductIdPurchaseAll)) return;
		}

		// < MOD 26.11!!!!
		if (command == IAPAction.None || command == IAPAction.Close) return;
        // Mod />		
		DoUnlockProduct(true);		
    }

    /// <summary>
    /// вызывается когда завершается восстановление
    /// </summary>
    /// <param name="a_productId"></param>
    /// <param name="didSucceed"></param>
    /// <param name="error"></param>
    private void OnIdleRestored(bool checkProductId, string a_productId, bool didSucceed, string error)
    {
		if (!(enabled && gameObject.activeSelf)) return;
		if (didSucceed == false) return;

		if ( checkProductId && (productId != a_productId) )
		{
			if(_products.CanIapPurchaseAll && (a_productId != _products.ProductIdPurchaseAll)) return;
		}  
        DoUnlockProduct(true);
    }    

    protected override void DoAwake()
    {
        base.DoAwake();
        __unlockProduct = false;

		_iapSettings = AppSettings.instance.IAP;
		_iapEnable = _iapSettings.enabled;

		_beforeCreateUIView = _iapEnable ? AppState.EventTypes.iapBeforeCreateUIView : AppState.EventTypes.gfvBeforeCreateUIView;
		_cancelCreateUIView = _iapEnable ? AppState.EventTypes.iapCancelCreateUIView : AppState.EventTypes.gfvCancelCreateUIView;
	
        _products = InAppProducts.instance;        
        DoUnlockProduct(true);
        if (__unlockProduct) return;

		// обработка результата завершения асинхронных команд InApp
        if (_iapEnable)
        {
            Messenger.AddListener<bool>(AppState.EventTypes.iapAfterCloseUIView, DoUnlockProduct);
            Messenger.AddListener<IAPAction, string, bool, string>(AppState.EventTypes.iapActionComplete, OnActionComplete);
            Messenger.AddListener<bool,string, bool, string>(AppState.EventTypes.iapIdleRestored, OnIdleRestored);
        }

        if (parentInAppUIView == null && forceUIContainer) parentInAppUIView = AppState.instance.UIContainer;
    }

	void OnEnable()
	{
        if (!__unlockProduct) DoUnlockProduct(true);
	}


	public bool DependencyDoTouch()
	{
		if( DoBreakTouch() ) return false;
		DoTouch ();
		return true;
	}

    protected override bool DoBreakTouch()
    {
        var state = AppState.instance;
        return (state.IsShowUIView || state.lockWidgets || __unlockProduct || string.IsNullOrEmpty(productId));
    }

	private void DoCreateUIView()
	{
        if (_iapEnable)
        {
			if (Messenger.eventTable.ContainsKey(AppState.EventTypes.iapCreateUIView))
                Messenger.Broadcast<string, GameObject>(AppState.EventTypes.iapCreateUIView, productId, parentInAppUIView);
            else Debug.LogError((gameObject.name + ":IAPUILockButton: Not config InApp!").AddTime());
        }
        else
        {
            if (Messenger.eventTable.ContainsKey(AppState.EventTypes.gfvCreateUIView))
                Messenger.Broadcast<GameObject>(AppState.EventTypes.gfvCreateUIView, parentInAppUIView);
            else Debug.LogError((gameObject.name + ":IAPUILockButton: Not config Get Full Version !").AddTime());
        }
        
	}

    protected override void DoTouch()
    {
#if MAGE_IAP_DEBUG        
        Debug.Log((GetInstanceID().ToString() + " IAPUILockButton:DoTouch: ").AddTime() + productId);
#endif

        if (Messenger.eventTable.ContainsKey(_beforeCreateUIView))Messenger.Broadcast(_beforeCreateUIView);
#if !MAGE_NO_PM
		if (_iapSettings.broadcastPMEvents)
		{
			//Debug.Log("pm: ".AddTime() + AppState.PMEvents.iapOpenUIView);
			PlayMakerFSM.BroadcastEvent(AppState.PMEvents.iapOpenUIView);
		}
#endif
        if (widgetSettings.parentalGate) {
			AppState.instance.parentalGateSvc.CreateUIView(
                p => {
				    DoCreateUIView();
			    },
			    r => {
					//Cancel action - no show prefab!
#if !MAGE_NO_PM
					if (_iapSettings.broadcastPMEvents)
					{
						//Debug.Log("pm: ".AddTime() + AppState.PMEvents.iapCloseUIView);
						PlayMakerFSM.BroadcastEvent(AppState.PMEvents.iapCloseUIView);
					}
#endif
					if (Messenger.eventTable.ContainsKey(_cancelCreateUIView)) Messenger.Broadcast(_cancelCreateUIView);
			});
		}
		else DoCreateUIView();
    }
    
    protected override void DoDestroy()
    {
        if (_iapEnable)
        {
            if (Messenger.eventTable.ContainsKey(AppState.EventTypes.iapAfterCloseUIView))
                Messenger.RemoveListener<bool>(AppState.EventTypes.iapAfterCloseUIView, DoUnlockProduct);
            if (Messenger.eventTable.ContainsKey(AppState.EventTypes.iapActionComplete))
                Messenger.RemoveListener<IAPAction, string, bool, string>(AppState.EventTypes.iapActionComplete, OnActionComplete);
            if (Messenger.eventTable.ContainsKey(AppState.EventTypes.iapIdleRestored))
                Messenger.RemoveListener<bool,string, bool, string>(AppState.EventTypes.iapIdleRestored, OnIdleRestored);
        }
    }  
}
