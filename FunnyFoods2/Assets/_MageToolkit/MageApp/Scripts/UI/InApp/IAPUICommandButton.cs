﻿using UnityEngine;
using System.Collections.Generic;

public class IAPUICommandButton : ButtonWidget
{
    [FullInspector.InspectorCategory("IAP")]
    [FullInspector.InspectorHeader("Параметры IAP:")]
    [FullInspector.InspectorMargin(16)]
    public IAPAction action = IAPAction.None;    

	[FullInspector.InspectorCategory("IAP")]
	[FullInspector.InspectorShowIf("IsPickProduct")]
	public string  productId = string.Empty;

	[FullInspector.InspectorCategory("IAP")]
	[FullInspector.InspectorShowIf("IsPickProduct")]
	public bool checkIdleUnlockProduct = false;

	private bool __unlockProduct = false;
	private bool IsPickProduct
	{
		get { return action == IAPAction.PurchasePick;}
	}

	protected void DoCheckUnlockProduct()
	{
		if (__unlockProduct) return;		
		__unlockProduct = InAppProducts.instance.GetUnlock(productId);
		if (__unlockProduct)
		{
			// сразу делаем неактивным
			if (gameObject != null)
			{
				gameObject.SetActive(false);
				// затем удаляем
				Destroy(gameObject);					
			}
		}
	}

    protected virtual void OnCommandCanExecute(List<IAPAction> actions, bool aCanExecute)
    {
        if (actions.Contains(this.action)) widgetSettings.canExecute = aCanExecute;
    }

	private void OnIdleRestored(bool checkProductId, string a_productId, bool didSucceed, string error)
	{
		if (didSucceed == false || (checkProductId && (productId != a_productId))) return;
		if (!enabled || !gameObject.activeSelf) return;
		DoCheckUnlockProduct();
	}    

    protected override bool DoBreakTouch()
    {
        bool result = !widgetSettings.canExecute;
        if (action != IAPAction.Close) result |= AppState.instance.lockWidgets;		
        return result;
    }

    protected override void DoTouch()
    {        
        switch (action)
        {
            case IAPAction.Close:
				 Messenger.Broadcast<IAPAction, string>(AppState.EventTypes.iapUICommand, action, string.Empty);
                 break;               

            case IAPAction.Purchase:
			case IAPAction.PurchaseAll:
            case IAPAction.Restore:
                 if (widgetSettings.parentalGate)
                 {
                    AppState.instance.parentalGateSvc.CreateUIView(p => {
                        Messenger.Broadcast<IAPAction, string>(AppState.EventTypes.iapUICommand, action, string.Empty);
                    });
                }
                else Messenger.Broadcast<IAPAction, string>(AppState.EventTypes.iapUICommand, action, string.Empty);
                break;

			case IAPAction.PurchasePick:
				{
					if (widgetSettings.parentalGate)
					{
						AppState.instance.parentalGateSvc.CreateUIView(p => {
							Messenger.Broadcast<IAPAction, string>(AppState.EventTypes.iapUICommand, action, productId);
						});
					}
					else Messenger.Broadcast<IAPAction, string>(AppState.EventTypes.iapUICommand, action, productId);
					break;
				}
                
            case IAPAction.PurchaseFull:
                {
                    if (widgetSettings.parentalGate)
                    {
                        AppState.instance.parentalGateSvc.CreateUIView(p => {
							Application.OpenURL(AppSettings.instance.appInfo.GetFullVersionUri());
							Messenger.Broadcast<IAPAction, string>(AppState.EventTypes.iapUICommand, IAPAction.Close, string.Empty);
                        });
                    }
                    else {
						Application.OpenURL(AppSettings.instance.appInfo.GetFullVersionUri());
						Messenger.Broadcast<IAPAction, string>(AppState.EventTypes.iapUICommand, IAPAction.Close, string.Empty);
                    }
                    break;
                }
		}
	}

	//MAY BE BEST IF DoAwake() & DoDestroy
	//void OnEnable()
	protected override void DoAwake()
    {
        base.DoAwake();
		if ((action == IAPAction.PurchaseAll) && !InAppProducts.instance.CanIapPurchaseAll)
		{
			gameObject.SetActive(false);
			GameObject.Destroy(gameObject);
			return;
		}
		if (action == IAPAction.PurchasePick)
		{
			DoCheckUnlockProduct();
			if (checkIdleUnlockProduct)
			{
				Messenger.AddListener<bool,string, bool, string>(AppState.EventTypes.iapIdleRestored, OnIdleRestored);
			}
		}
		Messenger.AddListener<List<IAPAction>, bool>(AppState.EventTypes.iapUICommandCanExecute, OnCommandCanExecute);		
    }

    //void OnDisable()
	protected override void DoDestroy()
    {        
		if (Messenger.eventTable.ContainsKey(AppState.EventTypes.iapUICommandCanExecute))
            Messenger.RemoveListener<List<IAPAction>, bool>(AppState.EventTypes.iapUICommandCanExecute, OnCommandCanExecute);
		if ((action == IAPAction.PurchasePick) && checkIdleUnlockProduct)
		{
			if (Messenger.eventTable.ContainsKey(AppState.EventTypes.iapIdleRestored))
				Messenger.RemoveListener<bool, string, bool, string>(AppState.EventTypes.iapIdleRestored, OnIdleRestored);			
		}
    }	
}
