﻿using UnityEngine;
using System.Collections.Generic;

/// <summary>
/// Information about products is described into instance of asset
/// </summary>
public class InAppProducts : FullInspector.BaseScriptableObject<FullInspector.FullSerializerSerializer> 
//public class InAppProducts :  FullInspector.BaseScriptableObject
{
    #region Nested types
    [System.Serializable]
    protected class ProductInfo //: FullInspector.BaseObject
    {
        public string productId;
        [FullInspector.InspectorTooltip("Если isFree, ТО ПРОДУКТ НЕ РЕГИСТРИРУЕМ в iTunesConnect или GooglePlay!")]
        public bool isFree;
		[System.NonSerialized]
        [FullInspector.InspectorHidePrimary]
        public bool isBought = false;
    }
    #endregion

    #region UNITY EDITOR

#if UNITY_EDITOR
    [UnityEditor.MenuItem("Assets/Create/Mage/InAppProducts")]
    public static void CreateAsset()
    {
        ScriptableObjectUtility.CreateAsset<InAppProducts>(PrjSettings.GetAssetNameToActivePlatform(""));
    }
#endif

    #endregion

    #region  InAppProducts instance

    const string assetName = "InAppProducts";
    const string subPath = "Settings/";

    private static InAppProducts _instance;
    public static InAppProducts instance
    {
        get
        {
            if (_instance == null) _instance = MgUnityHelper.CreateFromResources<InAppProducts>(subPath, PrjSettings.GetAssetNameToActivePlatform(assetName));
            return _instance;
        }
    }
    [System.Obsolete("use InAppProducts.instance")]
    public static InAppProducts Instance
    {
        get
        {            
            return instance;
        }
    }

    #endregion

    #region Members

    [SerializeField,  FullInspector.ShowInInspector]
    [FullInspector.InspectorOrder(1),FullInspector.InspectorMargin(20)]
    [FullInspector.InspectorHeader("Список единичных продуктов")]   
    private List<ProductInfo> _products;

    [SerializeField, FullInspector.ShowInInspector]
    [FullInspector.InspectorOrder(2)]
    [FullInspector.InspectorComment(FullInspector.CommentType.Warning, "Оптовая покупка через InApp")]
	private bool _canIapPurchaseAll;
	public bool CanIapPurchaseAll
	{
		get { return _canIapPurchaseAll; }		
	}

    [SerializeField, FullInspector.ShowInInspector, FullInspector.InspectorShowIf("_canIapPurchaseAll")]
    [FullInspector.InspectorOrder(2.5), FullInspector.InspectorMargin(20)]
    [FullInspector.InspectorHeader("ProductId оптововой покупки (купить все)")]
	private string _productIdPurchaseAll = string.Empty;
	public string ProductIdPurchaseAll
	{
		get { return _productIdPurchaseAll; }		
	}

    [FullInspector.InspectorOrder(3), FullInspector.InspectorMargin(12), FullInspector.InspectorDivider]
    [FullInspector.InspectorTooltip("Мин. свободное место(Mb) на устройстве, необходимое для запуска диалога встроеных покупок!")]	 
    public int  minFreeSpace= 15;

	#endregion

	#region Work with product (unlock purchases content)

	public bool GetUnlock(string productId)
    {   
		if (AppSettings.instance.appInfo.activeAppType == MageAppType.Full) return true;

        if (string.IsNullOrEmpty(productId)) return false;
        if (_products != null) {
            foreach (var item in _products)
                if (item.productId == productId) return item.isFree || item.isBought;
        }
        return false;
    }

    private List<string> __iapProductIds = null;
    public List<string> GetInAppProductIds()
    {
        if (__iapProductIds == null) __iapProductIds = new List<string>(_products.Count);
        if (__iapProductIds.Count < 1) {
            if (_canIapPurchaseAll && !string.IsNullOrEmpty(_productIdPurchaseAll)) __iapProductIds.Add(_productIdPurchaseAll);
            if (_products != null)
                foreach (var item in _products) if (!item.isFree) __iapProductIds.Add(item.productId);            
        }
        return new List<string>(__iapProductIds);  
    }

	public void UpdateBoughtProduct(string productId)
	{
        if (_products == null) return;
        if (_canIapPurchaseAll && !string.IsNullOrEmpty(_productIdPurchaseAll) && (productId == _productIdPurchaseAll))
        {  
            for (int i = 0; i < _products.Count; i++) _products[i].isBought = true;			
        }
        else {
            for (int i = 0; i < _products.Count; i++) {
                if (_products[i].productId == productId) {
                    _products[i].isBought = true;
                    break;
                }
            }
        }
	}

	public int GetCountBoughtProducts()
	{
		int count =0;
		for(int i = 0; i < _products.Count; i++)
			if(_products[i].isBought )count++;
		return count;
	}

    public void UpdateBoughtProducts(List<string> productIds)
    {
        if (_products == null) return;

        if (_canIapPurchaseAll && !string.IsNullOrEmpty(_productIdPurchaseAll)) {
            if (productIds.Exists((p) => { return p == _productIdPurchaseAll; })){
                UpdateBoughtProduct(_productIdPurchaseAll);
                return;
            }
        }
        foreach (var item in _products){
            if (productIds.Exists((p) => { return p == item.productId; }))          
                item.isBought = true;            
        }
    }
    #endregion
}
