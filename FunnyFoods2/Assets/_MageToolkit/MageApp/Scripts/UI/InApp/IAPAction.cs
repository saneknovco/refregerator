﻿
[System.Serializable]
public enum IAPAction
{
	None            = 0,
	/// <summary>
	/// close InApp View
	/// </summary>
	Close           = 1,
	/// <summary>
	/// restore purchases
	/// </summary>
	Restore         = 2,
	/// <summary>
	/// Purchase InApp by productId (active product Id)
	/// </summary>
	Purchase        = 3,
	/// <summary>
	/// Get Full Version
	/// </summary>
	PurchaseFull    = 4,
	/// <summary>
	/// Purchase All InApp Product by productIdPurchaseAll (special InApp by special price!)
	/// </summary>
	PurchaseAll		= 5,
	/// <summary>
	/// The purchase pick InApp product (by pick product Id).
	/// </summary>
	PurchasePick	= 6
};
