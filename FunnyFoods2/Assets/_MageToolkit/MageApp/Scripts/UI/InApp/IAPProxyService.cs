﻿
using UnityEngine;
using System.Collections.Generic;

#if (UNITY_IOS || UNITY_TVOS || UNITY_ANDROID) && !UNITY_EDITOR && !MAGE_NO_IAP 
using Prime31;
#endif

public class IAPProxyService : GlobalSingletonBehaviour<IAPProxyService> 
{
#region Static
	
	static List<string> savedIdentifiers
    {
        get
        {
            var result = new List<string>();

#if (UNITY_IOS || UNITY_TVOS) && !UNITY_EDITOR && !MAGE_NO_IAP
#if MAGE_IAP_DEBUG
            Debug.Log("=== IAPProxyService:SAVED TRANSACTION === ".AddTime());
#endif

			var transactions = StoreKitBinding.getAllSavedTransactions();			
			if( transactions != null && transactions.Count > 0 ) {	

				foreach (StoreKitTransaction tran in transactions )	{
#if MAGE_IAP_DEBUG
			        Debug.Log(("=== TRAN: " + tran).AddTime());
#endif
				    if(!(string.IsNullOrEmpty(tran.productIdentifier) || result.Contains(tran.productIdentifier)
				         && (tran.transactionState !=  StoreKitTransactionState.Failed)) )
					    result.Add(tran.productIdentifier);				
			    }
			}
#if MAGE_IAP_DEBUG
				Debug.Log("SAVED PRODUCT: ".AddTime());
				foreach(var item in result)	Debug.Log(item.AddTime());
				Debug.Log("=== IAPProxyService:SAVED TRANSACTION END === ".AddTime());
#endif
#endif
            return result;
//#endif
        }
    }    

    static void UpdateProducts()
    {
        InAppProducts.instance.UpdateBoughtProducts(savedIdentifiers);
    }

    static void UpdateProduct( string productId )
    {
        InAppProducts.instance.UpdateBoughtProduct( productId );
    }

#endregion

#region IAP: IAPCombo Methods

    string __activeProductId = string.Empty;
    bool __didOpen = false;
    bool __underRequest = false;
	bool __requestUpdatePurchase = true;
    /// <summary>
    /// request STRONGLY ALL IN-APP (КОТОРЫЕ ХОТИМ ПРОДАВАТЬ)
    /// Запрос инициализации - выполняется до начала покупки
    /// </summary>
    /// <param name="productIds"></param>
    private void RequestProducts(string[] productIds)
    {        
		if (AppSettings.instance.appInfo.activeAppType == MageAppType.Full) __didOpen = true;       
        if (__didOpen) {           
            if (Messenger.eventTable.ContainsKey(AppState.EventTypes.iapRequestProductDataComplete))
                Messenger.Broadcast<bool>(AppState.EventTypes.iapRequestProductDataComplete, __didOpen);
            return;
        }

#if (UNITY_IOS || UNITY_TVOS || UNITY_ANDROID) && !UNITY_EDITOR && !MAGE_NO_IAP
        __underRequest = true; 		
        //request STRONGLY ALL IN-APP (КОТОРЫЕ ХОТИМ ПРОДАВАТЬ)
		IAP.requestProductData(productIds, productIds, productList => {
                __underRequest = false;

#if MAGE_IAP_DEBUG
                Debug.Log( "IAPProxyService:RequestProducts:Product list received:".AddTime());
                //Prime31.Utils.logObject( productList );
#endif

                if (productList != null) __didOpen = (productList.Count > 0);  

#if UNITY_ANDROID

#if MAGE_IAP_DEBUG
                Debug.Log("IAPProxyService:RequestProducts:__requestUpdatePurchase: ".AddTime() + __requestUpdatePurchase);               
#endif
				if(__requestUpdatePurchase)
				{
					var purchasedItems = Prime31.IAP.androidPurchasedItems;
					if(purchasedItems != null)
					{
						__requestUpdatePurchase = false;
						foreach(var item in purchasedItems)
						{
							if(item.purchaseState == Prime31.GooglePurchase.GooglePurchaseState.Purchased)
                            {                                
#if MAGE_IAP_DEBUG
                                Debug.Log("IAPProxyService:RequestProducts:UpdateProduct: ".AddTime() + item.productId);
#endif
                                UpdateProduct(item.productId);
                                //if (Messenger.eventTable.ContainsKey(AppState.EventTypes.iapIdleRestored))
                                //    Messenger.Broadcast<bool, string, bool, string>(AppState.EventTypes.iapIdleRestored, true, item.productId, true, string.Empty);
                            }
						}						
                        if (Messenger.eventTable.ContainsKey(AppState.EventTypes.iapIdleRestored))
                             Messenger.Broadcast<bool, string, bool, string>(AppState.EventTypes.iapIdleRestored, false, string.Empty, true, string.Empty);
                        // WARNING - NOT FULL SUPPORTED
						//if (Messenger.eventTable.ContainsKey(AppState.EventTypes.iapActionComplete))
						//	Messenger.Broadcast<IAPAction, string, bool, string>(AppState.EventTypes.iapActionComplete, IAPAction.Restore, string.Empty, true, "forceFlag");
					}
				}	
#endif
                
                if (Messenger.eventTable.ContainsKey(AppState.EventTypes.iapRequestProductDataComplete))
                    Messenger.Broadcast<bool>(AppState.EventTypes.iapRequestProductDataComplete, __didOpen);
            });
#else
        __didOpen = true;
        __underRequest = false;
        if (Messenger.eventTable.ContainsKey(AppState.EventTypes.iapRequestProductDataComplete))
            Messenger.Broadcast<bool>(AppState.EventTypes.iapRequestProductDataComplete, __didOpen);
#endif
    }

    bool __underRestore = false;
    /// <summary>
    /// восстановления покупок
    /// </summary>
    void RestoreProducts()
    {
        if (__underRestore || __underRequest) return;
        __underRestore = true;

#if (UNITY_IOS || UNITY_TVOS) && !UNITY_EDITOR && !MAGE_NO_IAP
		IAP.restoreCompletedTransactions( productId => {

#if MAGE_IAP_DEBUG
			Debug.Log( "it is restore purchased product: ".AddTime() + productId );
#endif
			if(!string.IsNullOrEmpty(productId)){
                UpdateProduct(productId);
                if (Messenger.eventTable.ContainsKey(AppState.EventTypes.iapIdleRestored))
                    Messenger.Broadcast<bool, string, bool, string>(AppState.EventTypes.iapIdleRestored, true, productId, true, string.Empty);
            }
		});
#else
        var prds = InAppProducts.instance.GetInAppProductIds();
        if (prds.Count > 0) {
            foreach (var item in prds) UpdateProduct(item);   
        }          
#endif
		// вынуждены принудительно сбрасывать, т.к. IAP.restoreCompletedTransactions 
        // не во всех случаях вызывает делегат завершения команды восстановления
		if (__underRestore) {
			__underRestore = false;
			if (Messenger.eventTable.ContainsKey(AppState.EventTypes.iapActionComplete))
				Messenger.Broadcast<IAPAction, string, bool, string>(AppState.EventTypes.iapActionComplete, IAPAction.Restore, string.Empty, true, "forceFlag");
		}        
    }

    private bool __underPurchase = false;
    /// <summary>
    /// purchase nonconsumable product
    /// </summary>
    /// <param name="productId"></param>
    private void PurchaseNCP(string productId)
    {
#if (UNITY_IOS || UNITY_TVOS || UNITY_ANDROID) && !UNITY_EDITOR && !MAGE_NO_IAP
        if ( __underPurchase || __underRequest ) return;
	    __underPurchase = true;
        IAP.purchaseNonconsumableProduct(productId, (didSucceed, error) => {
            __underPurchase = false;
			if(didSucceed)UpdateProduct( productId );
            if (Messenger.eventTable.ContainsKey(AppState.EventTypes.iapActionComplete))
				Messenger.Broadcast<IAPAction, string, bool, string>(AppState.EventTypes.iapActionComplete, IAPAction.Purchase, productId, didSucceed, error);
        });
#else
        UpdateProduct(productId);
        if (Messenger.eventTable.ContainsKey(AppState.EventTypes.iapActionComplete))
            Messenger.Broadcast<IAPAction, string, bool, string>(AppState.EventTypes.iapActionComplete, IAPAction.Purchase, productId, true, string.Empty);
#endif
    }

#endregion

#region IAP: MESSANGER CALLBACKS
    private void OnRequestProductData()
    {
        RequestProducts(InAppProducts.instance.GetInAppProductIds().ToArray());
    }

    private void OnCreateUIView(string activeProductId, GameObject parentUIView)
    {      
        bool isShow = false;
        if (CreateInAppUIView(activeProductId, parentUIView)) {
            __activeProductId = activeProductId;
            isShow = true;
        }
		if(Messenger.eventTable.ContainsKey(AppState.EventTypes.iapShowUIView))
			Messenger.Broadcast<bool>(AppState.EventTypes.iapShowUIView, isShow);
    }

    private void OnCloseUIView(bool refreshBoughtProducts)
    {
        CloseInAppUIView(refreshBoughtProducts);
    }

    private void OnAction(IAPAction cmd, string productId)
    {        
        if (__underRestore || __underPurchase || __underRequest || !__didOpen) return;        
        switch (cmd) {
            case IAPAction.Restore:
                    RestoreProducts();
                    break;
         	case IAPAction.Purchase:
                    PurchaseNCP(__activeProductId);
                    break;
			case IAPAction.PurchasePick:
					PurchaseNCP(productId);
					break;
			case IAPAction.PurchaseAll:
					if (InAppProducts.instance.CanIapPurchaseAll)PurchaseNCP(InAppProducts.instance.ProductIdPurchaseAll);
					break;
        }
    }
#endregion

#region GlobalSingletonBehaviourStd: override methods

	private void ResetState()
	{
		__underRestore = false;
		__underPurchase = false;
	}

    protected override void DoAwake()
    {
        base.DoAwake();
        
        ResetState();

#if UNITY_ANDROID && !UNITY_EDITOR && !MAGE_NO_IAP
        //your public key from the Android developer portal here
        IAP.init( AppSettings.instance.appInfo.publicKey );
#endif

        IAPProxyService.UpdateProducts();

        Messenger.AddListener(AppState.EventTypes.iapRequestProductData, OnRequestProductData);
        Messenger.AddListener<string, GameObject>(AppState.EventTypes.iapCreateUIView, OnCreateUIView);
        Messenger.AddListener<bool>(AppState.EventTypes.iapCloseUIView, OnCloseUIView);
        Messenger.AddListener<IAPAction, string>(AppState.EventTypes.iapAction, OnAction);
    }

    protected override void DoDestroy()
    {
        if (Messenger.eventTable.ContainsKey(AppState.EventTypes.iapRequestProductData))
            Messenger.RemoveListener(AppState.EventTypes.iapRequestProductData, OnRequestProductData);
        if (Messenger.eventTable.ContainsKey(AppState.EventTypes.iapCreateUIView))
            Messenger.RemoveListener<string, GameObject>(AppState.EventTypes.iapCreateUIView, OnCreateUIView);
        if (Messenger.eventTable.ContainsKey(AppState.EventTypes.iapCloseUIView))
            Messenger.RemoveListener<bool>(AppState.EventTypes.iapCloseUIView, OnCloseUIView);
        if (Messenger.eventTable.ContainsKey(AppState.EventTypes.iapAction))
            Messenger.RemoveListener<IAPAction, string>(AppState.EventTypes.iapAction, OnAction);
 
        ///////////////////////////////////////////////
        var inAppProducts = InAppProducts.instance;
        int count = inAppProducts.GetCountBoughtProducts ();

        if(PlayerPrefs.GetInt("iapBoughtProduct", 0) != count)
        {
            int unlockPrd = 0;
            var prds = inAppProducts.GetInAppProductIds();        
            if (prds.Count > 0) 
            {
                foreach (var item in prds) {
                    unlockPrd = inAppProducts.GetUnlock(item) ? 1:0;
                    //Debug.Log (item + " " + unlockPrd);
                    PlayerPrefs.SetInt( item, unlockPrd );
               }
            }
            //Debug.Log ("iapBoughtProduct" + " " + count);
            PlayerPrefs.SetInt ("iapBoughtProduct", count);
            PlayerPrefs.Save ();
        }
        ////////////////////////////////////////////////
        base.DoDestroy();
    }
#endregion

#region Unity event functions

    void Start()
    {
        OnRequestProductData();	
    }

#endregion

#region UIView: work with prefab: create/delete with broadcat messages

	//private MageAppUIViewMode _iapUIViewMode = MageAppUIViewMode.None;
	private MageAppUIViewMode _iapUIViewMode = 0;
	public MageAppUIViewMode iapUIViewMode
	{
		get { return _iapUIViewMode; }       
	}
	
	private GameObject _iapUIViewInstance = null;

	//it is property uncomment if realy need access
	//public GameObject inAppViewInstance
	//{
	//    get { return _iapUIViewInstance; }
	//}	
	
	private bool CreateInAppUIView(string productId, GameObject parentUIView)
	{
        //=== hook for FVT ===
        var settings = AppSettings.instance;
		if ( (settings.appInfo.activeAppType == MageAppType.Lite)
		    && SysUtilsProxy.AppIsInstalled(settings.appInfo.GetFullVersionAppUrl()) ) {
             if (Messenger.eventTable.ContainsKey(AppState.EventTypes.fvtAction))
                 Messenger.Broadcast<FVTAction>(AppState.EventTypes.fvtAction,FVTAction.CreateUIView);
             return false;
        }
        //=== hook for FVT ===
        
        //if (_iapUIViewInstance != null || _iapUIViewMode != MageAppUIViewMode.None) return false;
		if (_iapUIViewInstance != null || (int)_iapUIViewMode != 0) return false;
       
        var dic = AppSettings.instance.IAP.UriPrefabs;
        string uri = dic.SafeGetByDefKey(AppState.instance.CurrentLang, AppSettings.instance.UI.defaultLang);      
		
		var go = Resources.Load(uri) as GameObject;
		if (go == null) return false;
		_iapUIViewInstance = GameObject.Instantiate(go) as GameObject;		
		
		_iapUIViewMode = AppSettings.instance.IAP.UIViewMode;
		AppState.instance.UIViewMode |= _iapUIViewMode;
		_iapUIViewInstance.name = go.name;
		if (parentUIView) {
			_iapUIViewInstance.transform.parent = parentUIView.transform;
			_iapUIViewInstance.transform.localPosition = go.transform.localPosition;
			_iapUIViewInstance.transform.localScale = go.transform.localScale;
		}
		__activeProductId = productId;
		return true;
	}
	
	private bool __isclosing =false;
	/// <summary>
	/// изменение состояния при закрытии InApp
	/// </summary>
    private void CloseInAppUIView(bool refreshedPurchases)
	{
		if(__isclosing)return;
		__isclosing = true;
        if (gameObject.activeSelf == false) gameObject.SetActive(true);
		
		if (Messenger.eventTable.ContainsKey(AppState.EventTypes.iapBeforeCloseUIView))
            Messenger.Broadcast<bool>(AppState.EventTypes.iapBeforeCloseUIView, refreshedPurchases);
		var settings = AppSettings.instance.IAP;
#if !MAGE_NO_PM
		//Debug.Log("pm: ".AddTime() + AppState.PMEvents.iapBeforeCloseUIView);
		if (settings.broadcastPMEvents)
		{
			//Debug.Log("pm: ".AddTime() + AppState.PMEvents.iapBeforeCloseUIView);
			PlayMakerFSM.BroadcastEvent(AppState.PMEvents.iapBeforeCloseUIView);
		}
#endif
		//WARNING WAIT
		StartCoroutine(MgCoroutineHelper.WaitThenCallback(settings.timeWaitBeforeClose, () =>
		{			
			//if ( RealaseInAppUIView() )	
			RealaseInAppUIView();
			{ 
				if(Messenger.eventTable.ContainsKey(AppState.EventTypes.iapAfterCloseUIView))
                    Messenger.Broadcast<bool>(AppState.EventTypes.iapAfterCloseUIView, refreshedPurchases);
#if !MAGE_NO_PM
				if (settings.broadcastPMEvents)
				{
					//Debug.Log("pm: ".AddTime() + AppState.PMEvents.iapCloseUIView);
					PlayMakerFSM.BroadcastEvent(AppState.PMEvents.iapCloseUIView);
				}
#endif
			}
            __isclosing = false;			
		}));
	}

	private bool RealaseInAppUIView ( )
	{
        ResetState();
        var _appState = AppState.instance;
		_appState.UIViewMode &= ~_iapUIViewMode;
		var result = _iapUIViewMode == 0 ? true : false;
		_iapUIViewMode = 0;
		if (_iapUIViewInstance != null) {
			GameObject.Destroy(_iapUIViewInstance);
            _iapUIViewInstance = null;
		}
        return result;
	}

#endregion
}
