using Mage.Effects;
using UnityEngine;

/// <summary>
/// Наследник ButtonWidget для закрытия  экземпляра UIView.
/// Интегрирован в систему сообщений MageApp
/// </summary>
[DisallowMultipleComponent]
public class CloseUIView : ButtonWidget
{
	[FullInspector.InspectorCategory("Action")]
	[FullInspector.InspectorHeader("Настройки закрытия UIView")]
	[FullInspector.InspectorMargin(16)]
	public GameObject UIView = null;
	[FullInspector.InspectorCategory("Action")]
	public MageAppUIViewMode UIViewMode;
	[FullInspector.InspectorCategory("Action")]
	public bool callUnloadAssets = false;

	[FullInspector.InspectorCategory("Action")]
	[FullInspector.InspectorComment(FullInspector.CommentType.None, "Обрабатывать Esc")]
	public bool isHookEsc = false;

	[FullInspector.InspectorCategory("Action")]
	[FullInspector.InspectorComment(FullInspector.CommentType.None, "Эффект перехода")]
	public bool fading = false;
	[FullInspector.InspectorCategory("Action"), FullInspector.InspectorShowIf("fading")]
	public float fadingOutTime = 0.5f;
	[FullInspector.InspectorCategory("Action"), FullInspector.InspectorShowIf("fading")]
	public float fadingInTime = 0.5f;

	//[System.Obsolete("use Messenger listrne (Enum.GetName(typeof(MageAppUIEvent), MageAppUIEvent.UIViewClose)",false)]	
	public System.Action<MonoBehaviour> beforePrefabCloseEvent;

	protected override void DoAwake()
	{		
		//if not explicity set - init implicity
		if (UIView == null) UIView = transform.parent.gameObject;	
	}

	protected void OnAction()
	{
		if (UIView != null)
		{	
			GameObject.Destroy(UIView);
			UIView = null;
		}	
	}

	protected override bool DoBreakTouch()
	{
		var result = base.DoBreakTouch();
		if(result)
		{
			if (Messenger.eventTable.ContainsKey(AppState.EventTypes.breakCloseUIView))
				Messenger.Broadcast<MageAppUIViewMode>(AppState.EventTypes.breakCloseUIView, UIViewMode);
		}
		return result;
	}

	protected override void DoTouch()
	{
		if (UIView == null) return;
		if(fading == false)
		{
			if (beforePrefabCloseEvent != null) beforePrefabCloseEvent(this);
			if (Messenger.eventTable.ContainsKey(AppState.EventTypes.closeUIView))
					Messenger.Broadcast<MageAppUIViewMode>(AppState.EventTypes.closeUIView, UIViewMode);
			OnAction();
			if (Messenger.eventTable.ContainsKey(AppState.EventTypes.afterCloseUIView))
				Messenger.Broadcast<MageAppUIViewMode>(AppState.EventTypes.afterCloseUIView, UIViewMode);	
			return;
		}

		if (beforePrefabCloseEvent != null) beforePrefabCloseEvent(this);
		if (Messenger.eventTable.ContainsKey(AppState.EventTypes.closeUIView))
				Messenger.Broadcast<MageAppUIViewMode>(AppState.EventTypes.closeUIView, UIViewMode);			
		widgetSettings.canExecute = false;
		var lockState = AppState.instance.lockWidgets;
		var mode = UIViewMode;
		AppState.instance.lockWidgets = true;		
		MgScreenFaderController.Run(MageAppFadingMode.OutContinued, fadingOutTime);
		StartCoroutine(MgCoroutineHelper.WaitThenCallback(fadingOutTime + AppConfig.timeWaitFinishing, () =>
		{
			widgetSettings.canExecute = true; 
			AppState.instance.lockWidgets = lockState;
			//if (Messenger.eventTable.ContainsKey(AppState.EventTypes.afterCloseUIView))
			//	Messenger.Broadcast<MageAppUIViewMode>(AppState.EventTypes.afterCloseUIView, mode);	
			OnAction();
			// NEED TEST !!!
			if (Messenger.eventTable.ContainsKey(AppState.EventTypes.afterCloseUIView))
				Messenger.Broadcast<MageAppUIViewMode>(AppState.EventTypes.afterCloseUIView, mode);	
		}));
	}
	
	protected override void DoDestroy()
	{		
		if( callUnloadAssets ) Resources.UnloadUnusedAssets();		
	}


#if !UNITY_IOS
	void Update()
	{
		if (isHookEsc == false) return;
		
		if (Input.GetKeyUp(KeyCode.Escape))
		{
			if (!AppState.instance.IsShowParentalGate)
			{
				Input.ResetInputAxes();					
				Touch();
			}
		}
	}
#endif
}
