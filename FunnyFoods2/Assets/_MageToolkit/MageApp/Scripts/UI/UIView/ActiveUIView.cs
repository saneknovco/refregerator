﻿using UnityEngine;
using Mage.Effects;

/// <summary>
/// Показывает/Прячет UIView. Наследник ButtonWidget
/// <para>Интегрирован в систему событий MageApp</para>
/// </summary>
[DisallowMultipleComponent]
public class ActiveUIView : ButtonWidget 
{
	[FullInspector.InspectorCategory("Action")]
	[FullInspector.InspectorHeader("Параметры Active UIView")]
    [FullInspector.InspectorOrder(1), FullInspector.InspectorMargin(16)]
	public MageAppUIViewMode modeUIView;

	[FullInspector.InspectorCategory("Action")]
    [FullInspector.InspectorOrder(1.1)]
	public GameObject UIView = null;

	[FullInspector.InspectorCategory("Action")]
    [FullInspector.InspectorOrder(1.2)]
	[FullInspector.InspectorComment(FullInspector.CommentType.Info,"Если makeActive == true то UIView делается активным")]
	public bool makeActive = true;

	[FullInspector.InspectorCategory("Action")]
	[FullInspector.InspectorComment(FullInspector.CommentType.None, "Эффект перехода")]
	public bool fading = false;
	[FullInspector.InspectorCategory("Action"), FullInspector.InspectorShowIf("fading")]
	public float fadingOutTime = 0.5f;
	[FullInspector.InspectorCategory("Action"), FullInspector.InspectorShowIf("fading")]
	public float fadingInTime = 0.5f;

    private AppState _appState;
	protected override void DoAwake()
	{
		base.DoAwake();
        _appState = AppState.instance;
	}

    protected override bool DoBreakTouch()
    {
		return (!widgetSettings.canExecute || _appState.lockWidgets);
    }

	protected void DoAction()
	{
		if (UIView!=null)
		{
			//if (makeActive) _appState.UIViewMode |= modeUIView;
			//else _appState.UIViewMode &= ~modeUIView;
			UIView.SetActive(makeActive);
		}
	}

	protected void OnAction()
	{
		if (fading == false)
		{
			DoAction();
			return;
		}
		widgetSettings.canExecute = false;
		MgScreenFaderController.Run(MageAppFadingMode.OutContinued, fadingOutTime);
		StartCoroutine(MgCoroutineHelper.WaitThenCallback(fadingOutTime + AppConfig.timeWaitFinishing, () =>
		{
			DoAction();
			MgScreenFaderController.Run(MageAppFadingMode.In, fadingInTime);
			StartCoroutine(MgCoroutineHelper.WaitThenCallback(1, () =>
			{
				StartCoroutine(MgCoroutineHelper.WaitThenCallback(fadingInTime, () =>
				{ 
					widgetSettings.canExecute = true; 
				}));
			}));
		}));
	}

	protected override void DoTouch()
	{
		if (UIView == null) return;
		//// NEED TEST!
		if (!widgetSettings.canExecute || _appState.lockWidgets) return;
        ////

		//устанавливаем флаг
		if (makeActive)
		{
			_appState.UIViewMode |= modeUIView;
			//signal: создаем UIView
			if (Messenger.eventTable.ContainsKey(AppState.EventTypes.beforeCreateUIView))
				Messenger.Broadcast<MageAppUIViewMode>(AppState.EventTypes.beforeCreateUIView, modeUIView);
		}
		else
		{
			_appState.UIViewMode &= ~modeUIView;
			//signal: закрытие UIView
			if (Messenger.eventTable.ContainsKey(AppState.EventTypes.closeUIView))
				Messenger.Broadcast<MageAppUIViewMode>(AppState.EventTypes.closeUIView, modeUIView);
		}
		
		if (widgetSettings.parentalGate)
		{
			_appState.parentalGateSvc.CreateUIView(
			   p => { OnAction(); },
			   r =>
			   {
				   if (makeActive && Messenger.eventTable.ContainsKey(AppState.EventTypes.cancelCreateUIView))
				   {
					   _appState.UIViewMode &= ~modeUIView;
					   Messenger.Broadcast<MageAppUIViewMode>(AppState.EventTypes.cancelCreateUIView, modeUIView);
				   }
			   }
		   );
		}
		else OnAction();       
	}
}
