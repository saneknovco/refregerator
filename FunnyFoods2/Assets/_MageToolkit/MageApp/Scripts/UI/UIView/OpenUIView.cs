﻿using UnityEngine;
using Mage.Effects;

/// <summary>
/// Наследник ButtonWidget для создания экземпляра UIView из префаба.
/// Интегрирован в систему сообщений MageApp
/// </summary>
[DisallowMultipleComponent]
public class OpenUIView : ButtonWidget {

	[FullInspector.InspectorCategory("Action")]
	[FullInspector.InspectorHeader("Настройки создания UIView")]
	[FullInspector.InspectorName("UIContainer")]
	[FullInspector.InspectorTooltip("Родительский объект для загружаемого префаба UIView")]
	public GameObject UIContainer;
	[FullInspector.InspectorCategory("Action")]
	[FullInspector.InspectorName("Auto Bind UIContainer")]
	public bool autoBindUIContainer = true; 
	private bool isUIContainer { get { return (UIContainer != null) || autoBindUIContainer; } }

	[FullInspector.InspectorCategory("Action")]
	[FullInspector.InspectorComment(FullInspector.CommentType.None, "Режим отображения UIView")]	
	public MageAppUIViewMode modeUIView;

	[FullInspector.InspectorCategory("Action")]
	public string uriUIView = @"UI/infoUIView";
	//public string GetUriUIView() 
	//{
	//	if (AppSettings.Instance.appInfo.ActiveAppType == MageAppType.Lite)
	//		return uriUIView + System.Enum.GetName(typeof(MageAppType), MageAppType.Lite);
	//	return uriUIView;
	//}	

	[FullInspector.InspectorCategory("Action"), FullInspector.InspectorShowIf("isUIContainer")]
	[FullInspector.InspectorTooltip("Относительно родительский объект UIContainer")]
	[FullInspector.InspectorName("Position UIView")]
	public Vector3 positionUIView = Vector3.zero;
	[FullInspector.InspectorCategory("Action"), FullInspector.InspectorShowIf("isUIContainer")]
	[FullInspector.InspectorTooltip("Относительно родительский объект UIContainer")]
	[FullInspector.InspectorName("Scale UIView")]
	public Vector3 scaleUIView = Vector3.one;
	[FullInspector.InspectorCategory("Action"), FullInspector.InspectorShowIf("isUIContainer")]
	[FullInspector.InspectorTooltip("Относительно родительский объект UIContainer")]
	[FullInspector.InspectorName("Rotation UIView")]
	public Quaternion rotationUIView = Quaternion.identity;

	[FullInspector.InspectorCategory("Action")]
	[FullInspector.InspectorComment(FullInspector.CommentType.None, "Эффект перехода")]
	public bool fading = false;
	[FullInspector.InspectorCategory("Action"), FullInspector.InspectorShowIf("fading")]
	public float fadingOutTime = 0.5f;
	[FullInspector.InspectorCategory("Action"), FullInspector.InspectorShowIf("fading")]
	public float fadingInTime = 0.5f;

	private GameObject _handleView = null;
	private AppState _appState;

	/// <summary>
	/// адаптивная загрузка префаба для Full & Lite version
	/// </summary>
	/// <returns></returns>
	private GameObject GetHandleUIView()
	{
		if (_handleView != null) return _handleView;
		// separate UIView for Lite:
		if (AppSettings.instance.appInfo.activeAppType == MageAppType.Lite)
		{
			string uriHandle = uriUIView + System.Enum.GetName(typeof(MageAppType), MageAppType.Lite);
			_handleView = Resources.Load<GameObject>(uriHandle);
			//loading Lite version with none excplicity prefab ("Lite" sufix)
			if (_handleView == null) _handleView = Resources.Load<GameObject>(uriUIView);
		}
		else
		{
			//loading for Full
			_handleView = Resources.Load<GameObject>(uriUIView);
		}
		return _handleView;
	}

	private void OnCloseUIView(MageAppUIViewMode mode)
	{
		if (mode != modeUIView) return;
		_appState.UIViewMode &= ~modeUIView;
	}

	protected override void DoAwake() 
	{
		base.DoAwake();

		_appState = AppState.instance;        
		Messenger.AddListener<MageAppUIViewMode>(AppState.EventTypes.closeUIView, OnCloseUIView);
		if (UIContainer == null && autoBindUIContainer ) UIContainer = GameObject.Find("UIContainer");
	}

	protected override bool DoBreakTouch ()
	{
        return (!widgetSettings.canExecute || _appState.IsShowUIView || _appState.lockWidgets);    
	}

	protected void DoAction()
	{
		if (_handleView == null) _handleView = GetHandleUIView();
		
		bool isShow = _handleView != null;
		if (isShow)
		{
			var obj = GameObject.Instantiate(_handleView) as GameObject;
			if (UIContainer)
			{
				obj.transform.parent = UIContainer.transform;
				obj.transform.localPosition = positionUIView;
				obj.transform.localRotation = rotationUIView;
				obj.transform.localScale = scaleUIView;				
			}
		}
		//signal: UIView  успешно или не успешно создано
		if (Messenger.eventTable.ContainsKey(AppState.EventTypes.showUIView))
			Messenger.Broadcast<MageAppUIViewMode, bool>(AppState.EventTypes.showUIView, modeUIView, isShow);
	}

	protected void OnAction()
	{
		if(fading == false)
		{
			DoAction();
			return;
		}
		widgetSettings.canExecute = false;
		MgScreenFaderController.Run(MageAppFadingMode.OutContinued, fadingOutTime);
		StartCoroutine(MgCoroutineHelper.WaitThenCallback(fadingOutTime + AppConfig.timeWaitFinishing, () =>
		{
			DoAction();
			MgScreenFaderController.Run(MageAppFadingMode.In, fadingInTime);
			StartCoroutine(MgCoroutineHelper.WaitThenCallback(1, () => 
				{
					StartCoroutine(MgCoroutineHelper.WaitThenCallback(fadingInTime, ()=>
						{ widgetSettings.canExecute = true;}
					));
				}));			
		}));
	}

	protected override void DoTouch()
	{
		if (!widgetSettings.canExecute || _appState.IsShowUIView || _appState.lockWidgets) return;
		//устанавливаем флаг
        _appState.UIViewMode |= modeUIView;
        //signal: создаем UIView
		if (Messenger.eventTable.ContainsKey(AppState.EventTypes.beforeCreateUIView))
            Messenger.Broadcast<MageAppUIViewMode>(AppState.EventTypes.beforeCreateUIView, modeUIView);
		if (widgetSettings.parentalGate)
		{
			_appState.parentalGateSvc.CreateUIView(
			   p => { 
				   OnAction(); 
			   },
			   r => {
				   _appState.UIViewMode &= ~modeUIView;
				   if (Messenger.eventTable.ContainsKey(AppState.EventTypes.cancelCreateUIView))
						  Messenger.Broadcast<MageAppUIViewMode>(AppState.EventTypes.cancelCreateUIView, modeUIView);
			   }
		   );
		}
		else OnAction();
	}

	protected override void  DoDestroy()
	{
        if (Messenger.eventTable.ContainsKey(AppState.EventTypes.closeUIView))
            Messenger.RemoveListener<MageAppUIViewMode>(AppState.EventTypes.closeUIView, OnCloseUIView);	
	}
}
