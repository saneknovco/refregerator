﻿
using UnityEngine;
using System.Collections;
using Mage.Effects;

public class CPStateController : GlobalSingletonBehaviour<CPStateController>
{
	#region Nested Types
	public enum RemoteTypeConfig
    {
        App  = 0,
        Cat  = 1,// category
        Com  = 2 // common
    }
	#endregion

	#region const
#if UNITY_ANDROID
	const string cpConfigName = @"cp31a";
    const string cpConfigFile = @"/cp31a.json";
#else
	const string cpConfigName = @"cp31";
	const string cpConfigFile = @"/cp31.json";
#endif
    const string cpSubFolder = @"/UI/CP";
    const string cpResFolder = @"UI/CP";
    const string cpSubFolderWithSlash = cpSubFolder + @"/";
    const string cpResFolderWithSplash = cpResFolder + @"/";
    #endregion

	#region Members
	public bool isBackgroundWorker = true;
    public bool isAccessToRunUpdate = true;
  
	private  CP31AppDataHolder _appDataHolder = null;
	public CP31AppDataHolder appDataHolder
	{
		get 
		{
			if (_appDataHolder == null)	{
                _appDataHolder = new CP31AppDataHolder(localCPConfigUri, ActiveUpdate == 0);
			}
			return _appDataHolder; 
		}
		private set { _appDataHolder = value; }
	}	
	private CP31AppDataHolder _appDataHolderNew = null;
	public CP31AppDataHolder appDataHolderNew
	{
		get 
		{
			if (_appDataHolderNew == null){
                _appDataHolderNew = new CP31AppDataHolder();
			}
			return _appDataHolderNew; 
		}
		private set { _appDataHolderNew = value; }
	}

	public bool IsEmptyAppListForCurrentLang
	{
		get
		{
			if (appDataHolder.GetSequenceApp(AppState.instance.CurrentLang).Count < 2) return true;
			return false;
		}
	}

	public CPStampState stampState;
   
    /// <summary>
    /// 0 or 1 or 2
    /// </summary>
    private int _activeUpdate = -1;
    public  int ActiveUpdate
    {
        get
        {
            if (_activeUpdate < 0) _activeUpdate = PlayerPrefs.GetInt(@"mg_CP31_ActiveUpdate", 0);
            return _activeUpdate;
        }
        set
        { 
            if (value >= 0 && value <= 2) {
                if (_activeUpdate != value) {
                    _activeUpdate = value;
                    PlayerPrefs.SetInt(@"mg_CP31_ActiveUpdate", _activeUpdate);
                    PlayerPrefs.Save();
                }
            }
            else Debug.LogError("cpActiveUpdate is bad " + _activeUpdate);
        }
    }

    /// <summary>
    /// 1 or 2
    /// </summary>
    public int NewUpdate
    {
        get
        {
            return ActiveUpdate % 2 + 1;
        }
    }

	private System.DateTime _lastTimeUpdated = System.DateTime.Now;

    private static DLCLoader.LoadingState _updateState = DLCLoader.LoadingState.None;
    public static DLCLoader.LoadingState UpdateState
    {
        get
        {
            if (_updateState == DLCLoader.LoadingState.None)
            {
                _updateState =
                (DLCLoader.LoadingState)System.Enum.Parse(_updateState.GetType(),
                        PlayerPrefs.GetString(AppState.EventTypes.cpRemoteUpdateData,
                        System.Enum.GetName(_updateState.GetType(), DLCLoader.LoadingState.Ready)));
            }
            return _updateState;
        }
        set
        {
            var prev = _updateState;
            _updateState = value;
		
            if (_updateState != prev)
            {
                PlayerPrefs.SetString(
                    AppState.EventTypes.cpRemoteUpdateData,
                    System.Enum.GetName(value.GetType(), value));
                PlayerPrefs.Save();
            }
        }
    }

    private string _remoteCPUri = string.Empty;
    public string remoteCPUri
    {
        get { return _remoteCPUri; }
    }

    private string _remoteCPAppConfigUri = string.Empty;
    private string _remoteCPCatConfigUri = string.Empty;
    private string _remoteCPConfigUri = string.Empty;

    private string _localCPAppConfigUri;
    private string _localCPResConfigUri;

    public string localCPConfigUri
    {
        get
        {
            int aUpd = ActiveUpdate;
            if (aUpd > 0) return _localCPAppConfigUri + aUpd.ToString() + cpConfigFile;
            else return _localCPResConfigUri;
        }
    }
    public string localNewCPUri
    {
        get
        { 
            return _localCPAppConfigUri + NewUpdate.ToString();
        }
    }
    public string localNewCPConfigUri
    {
        get
        {
            return localNewCPUri + cpConfigFile;
        }
    }

    public string GetRemoteCPConfigUri(RemoteTypeConfig typeConfig = RemoteTypeConfig.Com)
    {
        if (typeConfig == RemoteTypeConfig.App) return _remoteCPAppConfigUri;
        if (typeConfig == RemoteTypeConfig.Cat) return _remoteCPCatConfigUri;
        //common config
        return _remoteCPConfigUri;
    }

    public string GetLocalCPUri(string imageName)
    {
        int aUpd = ActiveUpdate;
        if (aUpd > 0) return _localCPAppConfigUri + aUpd.ToString() + "/" + imageName + AppConfig.imageFileExt;
        else return cpResFolderWithSplash + imageName;
    }

    public Texture2D SmartLoadImage(string imageName)
    {
        var uri = GetLocalCPUri(imageName);
        if (_activeUpdate == 0) return Resources.Load<Texture2D>(uri);
#if UNITY_ANDROID
		return DLCUtils.LocalLoadPNGTexture(uri);
#else
		return  DLCUtils.LocalLoadPVR4Texture(uri);        
#endif
    }
   

    private int _updateTimeInterval = 3 * 3600;// 3 * 1 hour
    private int _checkInterval = 900;// 15 min
    private AppSettings.CPSettings _settings;  

    #endregion       

    #region Task

    internal bool ResetUpdates()
    {
        ActiveUpdate = 0;
        if (_appDataHolder == null) _appDataHolder = new CP31AppDataHolder();
        return appDataHolder.ParseJSON(localCPConfigUri, true);
    }

    private IEnumerator BackgroundWorker()
	{
		yield return new WaitForEndOfFrame();

		//работает все время в фоне
		while (isBackgroundWorker)
		{			
			// ждем соединения с интернетом
			while (Application.internetReachability == NetworkReachability.NotReachable)
			    yield return new WaitForSeconds(120);			
			// ждем статуса готовности данных
            while (UpdateState != DLCLoader.LoadingState.Ready)
				yield return new WaitForSeconds(15);           
            // запускаем новый загрузчик
            _lastTimeUpdated = System.DateTime.Now;
			if( isAccessToRunUpdate &&
                (_settings.denyRunUpdateFromLevel <= -1 || UnityEngine.SceneManagement.SceneManager.GetActiveScene().buildIndex < _settings.denyRunUpdateFromLevel))
			{
#if MAGE_DEBUG && MAGE_DLC_DEBUG
                Debug.Log ("Run update cp: ".AddTime());
#endif                
                _lastTimeUpdated = System.DateTime.Now;
                UpdateState = DLCLoader.LoadingState.Run;
                CPAppInfoLoader.Run(GetRemoteCPConfigUri(RemoteTypeConfig.App), 
                    GetRemoteCPConfigUri(RemoteTypeConfig.Cat),
                    GetRemoteCPConfigUri(RemoteTypeConfig.Com));               
            }	
		
			// Infinite cycle for updates every UPDATE_INTERVAL
            while (((System.DateTime.Now - _lastTimeUpdated).TotalSeconds <= _updateTimeInterval))
                yield return new WaitForSeconds(_checkInterval);			
		}
		yield break;
	}

    #endregion

    #region CP: MESSENGER EVENTS & CALLBACKS

    private void DoRemoteUpdateConfig(DLCLoader.ActionState actionState)
    {
        _lastTimeUpdated = System.DateTime.Now;
        switch (actionState)
        {           
                
            case DLCLoader.ActionState.AfterBreak:                
                UpdateState = DLCLoader.LoadingState.Ready;

                break;

            case DLCLoader.ActionState.AfterDone:
#if MAGE_DEBUG && MAGE_DLC_DEBUG
            Debug.Log("CPAppInfoLoader:DoAfterLoadingPackage is start");
#endif
                var holderNew = appDataHolderNew;
                if (!holderNew.ParseJSON(localNewCPConfigUri, false)) {
                    UpdateState = DLCLoader.LoadingState.Ready;
#if MAGE_DEBUG && MAGE_DLC_DEBUG
                    Debug.LogWarning("DoAfterLoadingPackage: failed config parsing");
#endif
                    return;
                }
                //nested loader
                CPAppImagesLoader.Run();
                break;
            default:
                Debug.LogError("DoRemoteUpdateConfig: do not support state ".AddTime() + actionState);
                break;
        }
    }

    private void DoRemoteUpdateImages(DLCLoader.ActionState actionState)
    {
        _lastTimeUpdated = System.DateTime.Now;
        switch (actionState)
        {
            case DLCLoader.ActionState.AfterBreak:
                UpdateState = DLCLoader.LoadingState.Ready;
                break;

            case DLCLoader.ActionState.AfterDone:
                if (appDataHolderNew.SafeParseNewJSON())
                {
                    //// CP31 =====
                    // чистим старые данные
                    if(_appDataHolder!=null)_appDataHolder.ClearAppData();
                    _appDataHolder = null;//явно в null (для Unity 4.6.3+)
                    //трюк с указателями
                    _appDataHolder = _appDataHolderNew;
                    _appDataHolderNew = null;                   
                    //// CP31 =====

                    //CP31: WARNING  возможен сбой (NEED TEST)
                    UpdateState = DLCLoader.LoadingState.Ready;

                    if (Messenger.eventTable.ContainsKey(AppState.EventTypes.cpStampAction))
                        Messenger.Broadcast<CPStampAction, bool>(AppState.EventTypes.cpStampAction, CPStampAction.RemoteUpdateData, true);
                }
                else {
                    UpdateState = DLCLoader.LoadingState.Stop;
                    ActiveUpdate = 0;
                }
                break;

            default:
                Debug.LogError("DoRemoteUpdateImages: do not support state ".AddTime() + actionState);
                break;
        }
    }

    private void OnRemoteUpdateData(DLCLoader.ActionState actionState, string actionName)
    {
#if MAGE_DEBUG && MAGE_DLC_DEBUG        
        Debug.LogWarning("OnRemoteUpdateData: ".AddTime() + actionState + " " + actionName);
#endif
        switch (actionName)
        {
            case CPAppInfoLoader.actionName: 
                DoRemoteUpdateConfig(actionState);
                break;
            case CPAppImagesLoader.actionName:
                DoRemoteUpdateImages(actionState);
                break;
            default:
                Debug.LogError("OnRemoteUpdateData: do not support action: ".AddTime() + actionName);
                break;
        }
    }

    private void OnSwitchLangUI(MageAppUILang lang)
    {
        if (Messenger.eventTable.ContainsKey(AppState.EventTypes.cpStampUpdate))
            Messenger.Broadcast(AppState.EventTypes.cpStampUpdate);
    }

    private void OnStampAction(CPStampAction action, bool activeState)
    {
        switch (action)
        {
            case CPStampAction.Enabled:
                stampState.enabled = activeState;
                break;
            case CPStampAction.Show:
                stampState.show = activeState;
                break;
            case CPStampAction.RemoteUpdateData:
                if(activeState && Messenger.eventTable.ContainsKey(AppState.EventTypes.cpStampUpdate))
                    Messenger.Broadcast(AppState.EventTypes.cpStampUpdate);
                break;
        }
    }

    #endregion

    #region Unity event functions

    protected override void DoAwake()
	{
        base.DoAwake();
		
		_settings = AppSettings.instance.CP;

        stampState.enabled = false;
        stampState.show = true;

        if(_settings.testMode) {
            _updateTimeInterval = 150;//2.5 min
            _checkInterval = 60;// 1 min
            if (_settings.resetActiveUpdate) ActiveUpdate = 0;
        }
        
        var dlc_set = AppSettings.instance.DLC;
        var appInfo_set = AppSettings.instance.appInfo;

        _remoteCPUri = dlc_set.suffixContentUri + cpSubFolder;
        _remoteCPAppConfigUri = dlc_set.suffixContentUri + @"/"
            + appInfo_set.shortBundleId + cpSubFolderWithSlash + Application.platform + cpConfigFile;
        _remoteCPCatConfigUri = dlc_set.suffixContentUri + @"/Cat/"
			+ appInfo_set.productCategory + cpSubFolderWithSlash + Application.platform + cpConfigFile;
        _remoteCPConfigUri = _remoteCPUri + @"/" + Application.platform + cpConfigFile;
		
		_localCPAppConfigUri = Application.persistentDataPath + @"/" + appInfo_set.activeAppTypeStr + cpSubFolderWithSlash;
        _localCPResConfigUri = cpResFolder + "/" + cpConfigName;
        // DO NOT MOVING NEXT LINE!
        UpdateState = DLCLoader.LoadingState.Ready;

	    /// WARNING !!!!
        _appDataHolder = new CP31AppDataHolder();
		bool successInit = _appDataHolder.ParseJSON(localCPConfigUri, ActiveUpdate == 0);
		if (!successInit)
		{
			ActiveUpdate = 0;
			successInit = _appDataHolder.ParseJSON(localCPConfigUri, true);			
		}

		isBackgroundWorker = successInit && !_settings.disable;
		stampState.enabled = isBackgroundWorker;		

        Messenger.AddListener<DLCLoader.ActionState, string>(AppState.EventTypes.cpRemoteUpdateData, OnRemoteUpdateData);
        Messenger.AddListener<CPStampAction, bool>(AppState.EventTypes.cpStampAction, OnStampAction);

        Messenger.AddListener<GameObject>(AppState.EventTypes.cpCreateUIView, OnCreateUIView);
        Messenger.AddListener(AppState.EventTypes.cpCloseUIView, OnCloseUIView);

        //подписка на изменение языка интерфейса
        if (AppSettings.instance.UI.isMultiLang)
            Messenger.AddListener<MageAppUILang>(AppState.EventTypes.langUI, OnSwitchLangUI);
	}
	
	// Use this for initialization
	void Start ()
	{
		StartCoroutine(BackgroundWorker());
	}   

    protected override void DoDestroy()
    {
        _updateState = DLCLoader.LoadingState.None;

        if (AppSettings.instance.UI.isMultiLang)
		{
			if (Messenger.eventTable.ContainsKey(AppState.EventTypes.langUI))
				Messenger.RemoveListener<MageAppUILang>(AppState.EventTypes.langUI, OnSwitchLangUI);
		}

        if (Messenger.eventTable.ContainsKey(AppState.EventTypes.cpStampAction))
            Messenger.RemoveListener<CPStampAction, bool>(AppState.EventTypes.cpStampAction, OnStampAction);

        if (Messenger.eventTable.ContainsKey(AppState.EventTypes.cpRemoteUpdateData))
            Messenger.RemoveListener<DLCLoader.ActionState, string>(AppState.EventTypes.cpRemoteUpdateData, OnRemoteUpdateData);
       
        if (Messenger.eventTable.ContainsKey(AppState.EventTypes.cpCreateUIView))
            Messenger.RemoveListener< GameObject>(AppState.EventTypes.cpCreateUIView, OnCreateUIView);
        if (Messenger.eventTable.ContainsKey(AppState.EventTypes.cpCloseUIView))
            Messenger.RemoveListener(AppState.EventTypes.cpCloseUIView, OnCloseUIView);
        
        base.DoDestroy();
    }
  
	#endregion

    #region CP UIView: work with prefab: create/delete with broadcat messages

	
	public readonly string cpUIViewTypeName = typeof(CPUIViewManager).Name;

	private MageAppUIViewMode _cpUIViewMode = 0;//MageAppUIViewMode.None;
    public MageAppUIViewMode cpUIViewMode
    {
        get { return _cpUIViewMode; }
    }

    private GameObject _cpUIViewInstance = null;

    //uncomment this is property if realy need access
    //public GameObject cpViewInstance
    //{
    //    get { return _cpUIViewInstance; }
    //}	

    private void OnCreateUIView(GameObject parentUIView)
    {        
        bool isShow = CreateUIView(parentUIView);
        if (Messenger.eventTable.ContainsKey(AppState.EventTypes.cpShowUIView))
            Messenger.Broadcast<bool>(AppState.EventTypes.cpShowUIView, isShow);
        if(isShow)
        {
            if (Messenger.eventTable.ContainsKey(AppState.EventTypes.uiActiveMainSceneObjects))
                Messenger.Broadcast<bool>(AppState.EventTypes.uiActiveMainSceneObjects, false);
        }
    }
    
    private bool CreateUIView( GameObject parentUIView)
    { 
        //if (_cpUIViewInstance != null || _cpUIViewMode != MageAppUIViewMode.None) return false;
		if (_cpUIViewInstance != null || (int)_cpUIViewMode != 0) return false;
        var dic = AppSettings.instance.CP.UriPrefabs;
        string uri = dic.SafeGetByDefKey(AppState.instance.CurrentLang, AppSettings.instance.UI.defaultLang);
        if(AppSettings.instance.CP.alwaysLandscape == false)
        {
            if (Screen.width < Screen.height) uri += "_P";
        }
        var go = Resources.Load(uri) as GameObject;
        if (go == null) return false;

        _cpUIViewInstance = GameObject.Instantiate(go) as GameObject;

        _cpUIViewMode = AppSettings.instance.CP.UIViewMode;
        AppState.instance.UIViewMode |= _cpUIViewMode;
        _cpUIViewInstance.name = go.name;
        if (parentUIView)
        {
            _cpUIViewInstance.transform.parent = parentUIView.transform;
            _cpUIViewInstance.transform.localPosition = go.transform.localPosition;
            _cpUIViewInstance.transform.localScale = go.transform.localScale;
        }
        return true;
    }

    private bool __isclosing = false;

	public bool IsClosingUIView
	{
		get { return __isclosing; }
	}
    /// <summary>
    /// Закрытие CrossPromo через фейдинг (обработка команды close)
    /// </summary>
    private void OnCloseUIView()
    {
        if (__isclosing || _cpUIViewInstance == null) return;// Warning !!!
        __isclosing = true;
		var appState = AppState.instance;
		var lockState = appState.lockWidgets;
        appState.lockWidgets = true;

        var fadingOutTime = 0.25f;
        var fadingInTime = 0.5f;		
        MgScreenFaderController.Run(MageAppFadingMode.OutContinued, fadingOutTime);
        StartCoroutine(MgCoroutineHelper.WaitThenCallback(fadingOutTime + 0.1f, () =>
        {            
            appState.UIViewMode &= ~_cpUIViewMode;           
			_cpUIViewMode = 0;
            if (_cpUIViewInstance != null)
            {
                GameObject.Destroy(_cpUIViewInstance);
                _cpUIViewInstance = null;
            }            
            MgScreenFaderController.Run(MageAppFadingMode.In, fadingInTime);
            __isclosing = false;
			appState.lockWidgets = lockState;

            if (Messenger.eventTable.ContainsKey(AppState.EventTypes.uiActiveMainSceneObjects))
                Messenger.Broadcast<bool>(AppState.EventTypes.uiActiveMainSceneObjects, true);
            if (Messenger.eventTable.ContainsKey(AppState.EventTypes.cpAfterCloseUIView))
                Messenger.Broadcast(AppState.EventTypes.cpAfterCloseUIView);

#if !MAGE_NO_PM
			//if (broadcastDoTouchPMEvent) 
			PlayMakerFSM.BroadcastEvent(AppState.PMEvents.cpCloseUIView);
#endif
        } ));
    }

    public void ResetUIViewState()
    {
        AppState.instance.UIViewMode &= ~_cpUIViewMode;        
		_cpUIViewMode = 0;        
        if (_cpUIViewInstance != null) {
            GameObject.Destroy(_cpUIViewInstance);
            _cpUIViewInstance = null;
        }
    }

    #endregion     
}
