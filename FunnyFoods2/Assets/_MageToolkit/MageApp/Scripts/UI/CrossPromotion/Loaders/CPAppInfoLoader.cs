﻿using UnityEngine;
using System.Collections.Generic;

public class CPAppInfoLoader : DLCLoader
{
    #region static
    public const string actionName = @"CPAppInfoLoader";

    private const string sectionCommonConfig = @"config";
    private const string sectionCatConfig = @"catconfig";
    private const string sectionAppConfig = @"appconfig";

    internal static void Run(string uriAppConfig, string uriCatConfig, string uriCommonConfig)
    {
        //Debug.Log("Run: ".AddTime() + uriAppConfig + " === " + uriCatConfig+" === " + uriCommonConfig);       
        Dictionary<string, string> prms = new Dictionary<string, string>(3);
        prms.Add(sectionAppConfig, uriAppConfig);
        prms.Add(sectionCatConfig, uriCatConfig);
        prms.Add(sectionCommonConfig, uriCommonConfig);
        DLCLoader.RunLoader<CPAppInfoLoader>(prms, AppConfig.timeWaitFinishing);    
    }

    #endregion

    private bool _skipedContent = true;

	protected override void DoAfterLoadingItem(string key, WWW www)
	{ 		   
        if (_isBreakPackRequest || !_skipedContent) return;

        bool isCommonConfig = key == sectionCommonConfig;
		try
		{		
			//  ПРОВЕРЯЕМ НА ОШИБКУ ЗАГРУЗКИ ЧЕРЕЗ WWW
            if( www == null || !string.IsNullOrEmpty(www.error)) {
                if (isCommonConfig) _isRetryLoad = true;
                return;
            }
            var controller = CPStateController.instance;
            //чистим
            string dir = controller.localNewCPUri;
			if (System.IO.Directory.Exists(dir)) System.IO.Directory.Delete(dir, true);				
			System.IO.Directory.CreateDirectory(dir);

			//грузим			
			//.... Insert decode & md5
            var jsonBytes = www.bytes;
#if MAGE_DEBUG && MAGE_DLC_DEBUG
                Debug.Log("json:" + System.Text.UTF8Encoding.UTF8.GetString(jsonBytes));
#endif
            var strData = System.Text.UTF8Encoding.UTF8.GetString(jsonBytes);
            var appDataNew = CP31AppDataHolder.CreateInstance(strData);
            if(appDataNew == null)
            {
                if (isCommonConfig) _isRetryLoad = true;
                return;
            }

            if (appDataNew.BundleId != controller.appDataHolder.BundleId) 
            {
                var uriFile = controller.localNewCPConfigUri;
				System.IO.File.WriteAllBytes(uriFile, jsonBytes);
#if UNITY_IOS
#if UNITY_5
                UnityEngine.iOS.Device.SetNoBackupFlag(uriFile);
#else
				iPhone.SetNoBackupFlag(uriFile);
#endif				
#endif
				_skipedContent = false;
                _isBreakPackRequest = true;
                _isRetryLoad = false;
#if MAGE_DEBUG && MAGE_DLC_DEBUG
				Debug.Log("CPAppInfoLoader:save: ".AddTime() + uriFile);
#endif
            }
            else _skipedContent = true;            
		}
		catch
		{
            if (isCommonConfig) _isRetryLoad = true;
		}
	}

    //после загрузки конфиг файла принимаем решении о запуске загрузчика images content
	protected override void DoAfterLoadingPackage()
	{        
        bool breakLoading = _skipedContent || _isRetryLoad;
        if (breakLoading == false){
            breakLoading = SysUtilsProxy.FreeDiskSpace < (long)32 * 1024 * 1024;
            if (breakLoading) Debug.LogError("CPAppInfoLoader:DoAfterLoadingPackage: NOT ENOUGH DISK SPACE!");
        }
        DLCLoader.ActionState actionState = breakLoading ? DLCLoader.ActionState.AfterBreak : DLCLoader.ActionState.AfterDone;
        
        if (Messenger.eventTable.ContainsKey(AppState.EventTypes.cpRemoteUpdateData))
            Messenger.Broadcast<DLCLoader.ActionState, string>(AppState.EventTypes.cpRemoteUpdateData, 
                actionState, actionName);
	}
}
