﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

/// <summary>
/// Загрузчик png  файлов  без оптимизации
/// (ПОКА ГРУЗИМ ВСЕ)
/// </summary>
public class CPAppImagesLoader : DLCLoader
{
    public const string actionName = @"CPAppImagesLoader";	

    internal static void Run()
    {
        // обновляем  графический контент
        var remoteUri = CPStateController.instance.remoteCPUri;
        var holderNew = CPStateController.instance.appDataHolderNew;
        Dictionary<string, string> prms = new Dictionary<string, string>(CP31AppDataHolder.capacityAppItem);
        foreach (var item in holderNew.LoadingShortBundleIds)
        {
            //prms.Add(item, remoteUri + @"/" + item + extImageFile);
            prms.Add(item, remoteUri + @"/Textures/" + item + AppConfig.imageFileExt);
#if MAGE_DEBUG && MAGE_DLC_DEBUG
                Debug.Log(("DoAfterLoadingPackage: " + item + " : " + remoteUri + @"/Textures/" + item + AppConfig.imageFileExt).AddTime());
#endif
        }
        DLCLoader.RunLoader<CPAppImagesLoader>(prms, AppConfig.timeWaitFinishing, 5, 5.0f, AppSettings.instance.CP.waitBeforeCompleteImagesLoading, 0);
    }

	private Dictionary<string, byte[]> _imagesCache = new Dictionary<string, byte[]>();
    private string _localUri = string.Empty;
	protected override void DoBeforeRetryLoadingPackage()
	{
		base.DoBeforeRetryLoadingPackage();
		_imagesCache.Clear();
	}
	
	protected override void DoAfterLoadingItem(string key, WWW www)
	{   
		try
		{
            if (string.IsNullOrEmpty(_localUri)) _localUri = CPStateController.instance.localNewCPUri;
			
            if (string.IsNullOrEmpty(www.error))
            {
				byte [] buffer = new byte[www.bytes.Length];
				www.bytes.CopyTo(buffer,0);
				_imagesCache.Add(key, buffer);
#if MAGE_DEBUG && MAGE_DLC_DEBUG
				var uriFile = _localUri + @"/" + key + AppConfig.imageFileExt;				
                Debug.Log("CPAppItemLoader: " + uriFile + " www: " + www.size);
#endif
            }
            else
            {
                _isRetryLoad = true;
				_imagesCache.Clear();
            }
        }
		catch
		{			
			_isRetryLoad = true;
            _isBreakPackRequest = true;
			_imagesCache.Clear();
		}
	}


	protected override void DoAfterLoadingPackage()
	{
		//_localUri = string.Empty;
		if (string.IsNullOrEmpty(_localUri)) _localUri = CPStateController.instance.localNewCPUri;
		Foundation.Tasks.UnityTask.Run(() =>
		{
#if MAGE_DEBUG && MAGE_DLC_DEBUG
			Debug.Log("CPAppImagesLoader:DoAfterLoadingPackage:Thread start...".AddTime());			
#endif

            string uriFile;
			foreach (var item in _imagesCache)
			{
				uriFile = _localUri + @"/" + item.Key + AppConfig.imageFileExt;
				System.IO.File.WriteAllBytes(uriFile, item.Value);
#if MAGE_ASYNC
				Foundation.Tasks.UnityTask.Delay(400);
#endif
			}

			Foundation.Tasks.UnityTask.RunOnMain( () => 
			{
#if MAGE_DEBUG && MAGE_DLC_DEBUG
			    Debug.Log("CPAppImagesLoader:DoAfterLoadingPackage:MainThread sync operation start...".AddTime());
#endif

#if UNITY_IOS
				if (string.IsNullOrEmpty(_localUri)) _localUri = CPStateController.Instance.localNewCPUri;
				foreach (var item in _imagesCache)
				{
					uriFile = _localUri + @"/" + item.Key + AppConfig.imageFileExt;
#if UNITY_5
					UnityEngine.iOS.Device.SetNoBackupFlag(uriFile);
#else
					UnityEngine.iPhone.SetNoBackupFlag(uriFile);
#endif
				}
#endif
                _localUri = string.Empty;
				_imagesCache.Clear();
				DLCLoader.ActionState actionState = _isRetryLoad || _isBreakPackRequest ? ActionState.AfterBreak : ActionState.AfterDone;
				if (Messenger.eventTable.ContainsKey(AppState.EventTypes.cpRemoteUpdateData))
					Messenger.Broadcast<DLCLoader.ActionState, string>(AppState.EventTypes.cpRemoteUpdateData,
						actionState, CPAppImagesLoader.actionName);
#if MAGE_DEBUG && MAGE_DLC_DEBUG
				Debug.Log("CPAppImagesLoader:DoAfterLoadingPackage:MainThread sync operation done...".AddTime());
#endif
            });
			Foundation.Tasks.UnityTask.Delay(700);
#if MAGE_DEBUG && MAGE_DLC_DEBUG
			Debug.Log("CPAppImagesLoader:DoAfterLoadingPackage:Thread done...".AddTime());			
#endif
        });
	}	
}
