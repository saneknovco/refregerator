﻿using UnityEngine;
using System.Collections.Generic;

/// классы для данных Cross-promotion.
/// формат двных json версии 3.1 (1.31.x.x) (cp31.json)
/// 

/// <summary>
/// текстовые данные отображаемые в Cross-promotion как в appItem так и в hot.
/// используется в CP31LangAppInfo
/// </summary>
[System.Serializable]
public class CP31TextInfo
{
    #region Text

    public string h1 = string.Empty;
    public Vector3 h1Position = Vector3.zero;

    public string h2 = string.Empty;
    public Vector3 h2Position = Vector3.zero;

    #endregion

    #region JSON CONVERT

    public JSONObject ToJSON()
    {
        JSONObject json = JSONObject.obj;
        json.AddField(@"h1", h1);
        json.AddField(@"h1Pos", JSONTemplates.FromVector3(h1Position));
        json.AddField(@"h2", h2);
        json.AddField(@"h2Pos", JSONTemplates.FromVector3(h2Position));
        return json;
    }
    public static CP31TextInfo FromJSON(JSONObject json)
    {
        var vData = new CP31TextInfo();
        if (json != null) {
            if (json.keys.Contains(@"h1")) vData.h1 = json[@"h1"].str;            
            if (json.keys.Contains(@"h1Pos")) vData.h1Position = JSONTemplates.ToVector3(json.GetField(@"h1Pos"));           
            if (json.keys.Contains(@"h2")) vData.h2 = json[@"h2"].str;           
            if (json.keys.Contains(@"h2Pos")) vData.h2Position = JSONTemplates.ToVector3(json.GetField(@"h2Pos"));           
        }
        return vData;
    }

    #endregion
}

/// <summary>
/// данные на текущем языке UI для отображения в cross
/// используется в CP31AppInfo
/// </summary>
[System.Serializable]
public class CP31LangAppInfo
{
    public int parsedCode = 0;

    public string iconLabel = string.Empty;//Free: {"f1","f2",...}
    #region Text Info
    public CP31TextInfo bannerText;
    public CP31TextInfo appText;
    #endregion

    #region JSON CONVERT

    public JSONObject ToJSON()
    {
        JSONObject json = JSONObject.obj;        
        json.AddField(@"iLabel", iconLabel);
        if (bannerText != null) json.AddField(@"hot", bannerText.ToJSON());
        if (appText != null) json.AddField(@"app", appText.ToJSON());
        return json;
    }

    public static CP31LangAppInfo FromJSON(JSONObject json)
    {
        var vData = new CP31LangAppInfo();       
        vData.parsedCode = 0;

        if (json != null)
        {
            vData.parsedCode = 1;            
            if (json.keys.Contains(@"iLabel")) vData.iconLabel = json[@"iLabel"].str;
            else vData.iconLabel = string.Empty;

            if (json.keys.Contains(@"hot"))vData.bannerText = CP31TextInfo.FromJSON(json.GetField(@"hot"));
            else {
                vData.bannerText = new CP31TextInfo();
                vData.parsedCode = 2;
            };

            if (json.keys.Contains(@"app")) vData.appText = CP31TextInfo.FromJSON(json.GetField(@"app"));
            else {
                vData.appText = new CP31TextInfo();
                vData.parsedCode = 3;
            }            
        }
        return vData;
    }

    public static CP31LangAppInfo FromJSON(string shortBundleId,JSONObject parentJson)
    {
        JSONObject json = null;
        if (parentJson.keys.Contains(shortBundleId)) json =  parentJson.GetField(shortBundleId);      
        return FromJSON(json);
    }

    #endregion
}

/// <summary>
/// информация о приложении для отображения в Cross-promotion
/// используется в CP31CfgInfo
/// </summary>
[System.Serializable]
public class CP31AppInfo
{
    #region const
    public static readonly string stampName = @"CPAppStamp";
    public static readonly string appItemName = @"CPAppItem";
    public static readonly string bannerName = @"AppHot";
    public static readonly string bannerSuffix = "_hot";
    public static readonly string screenshotSuffix = "_shot";
    #endregion
    
    public string appId = string.Empty;
    public string shortBundleId = string.Empty;
    public MageAppType appType = MageAppType.Lite;
    public CP31LangAppInfo langInfo;

    public string uriInStore
    {
        get 
        {
			return AppSettings.instance.appInfo.GetUriInStore(appId);
        }
    }

    #region icon
    public string bannerIcon
    {
        get { return shortBundleId + bannerSuffix; }
    }
    
    public string screenshot
    {
        get { return shortBundleId + screenshotSuffix; }
    }
    #endregion

    public JSONObject ToJSON(JSONObject parent = null)
    {
        JSONObject json = JSONObject.obj;
        if (parent == null || parent.IsNull || !parent.IsObject)
        {
            json.AddField(@"appId", appId);
            json.AddField(@"appTp", System.Enum.GetName(typeof(MageAppType), appType));
            json.AddField(@"sbId", shortBundleId);
        }
        else
        {
            json.AddField(@"appId", appId);
            json.AddField(@"appTp", System.Enum.GetName(typeof(MageAppType), appType));
            if (parent.keys.Contains(shortBundleId)) parent.SetField(shortBundleId, json);
            else parent.AddField(shortBundleId, json);
        }
        return json;
    }    

    public bool UpdateLangInfo(CP31LangAppInfo obj)
    {       
        //if (obj.shortBundleId != shortBundleId || obj.parsedCode <= 0) return false;
        langInfo = obj;
        return true;
    }

    public static CP31AppInfo FromJSON(string shortBundleId, JSONObject json )
    {        
        var vData = new CP31AppInfo();
        vData.shortBundleId = shortBundleId;
        vData.langInfo = new CP31LangAppInfo();
        if (json != null) {
            if (json.keys.Contains(@"appId")) vData.appId = json[@"appId"].str;     
            vData.appType = MageAppType.Lite;
            if (json.keys.Contains(@"appTp")) {
                if (json[@"appTp"].str == "Single")
                    vData.appType = MageAppType.Freemium;
                else
                    vData.appType = (MageAppType)System.Enum.Parse(typeof(MageAppType), json[@"appTp"].str);
            }
        }
        return vData;
    }

    public static CP31AppInfo FromJSON(string shortBundleId, JSONObject json, JSONObject jsonLang)
    {
        var vData = FromJSON(shortBundleId, json );
        vData.langInfo = CP31LangAppInfo.FromJSON(jsonLang);        
        return vData;
    }
}

/// <summary>
/// информация о последовательности отображаемых приложений для локализации
/// </summary>
[System.Serializable]
public class CP31LangCfgInfo
{
    public bool useDefaultLang = true;
    public bool useDefaultSeq = true; 

    public List<string> sequence;

    public JSONObject ToJSON()
    {
        JSONObject json = JSONObject.obj;
        if(useDefaultLang == false) json.AddField(@"defLang", useDefaultLang);
        if(useDefaultSeq == false) json.AddField(@"defSeq", useDefaultSeq);

        var jsonArr = JSONObject.arr;
        json.AddField(@"seq", jsonArr);
        jsonArr = json.GetField(@"seq");        
        for (int i = 0; i < sequence.Count; i++) jsonArr.list.Add(JSONObject.StringObject(sequence[i]));

        return json;
    }

    public static CP31LangCfgInfo FromJSON(JSONObject json)
    {
        var vData = new CP31LangCfgInfo();
        vData.sequence = new List<string>();
        if (json != null)
        {
            if (json.keys.Contains(@"defLang")) vData.useDefaultLang = json[@"defLang"].b;
            else vData.useDefaultLang = true;

            if (json.keys.Contains(@"defSeq")) vData.useDefaultSeq = json[@"defSeq"].b;
            else vData.useDefaultSeq = true;

            if (vData.useDefaultSeq == false)
            {
                if (json.keys.Contains(@"seq"))
                {
                    var jsn = json.GetField(@"seq");
                    if (jsn != null)
                        for (int i = 0; i < jsn.list.Count; i++) vData.sequence.Add(jsn.list[i].str);
                }
                if (vData.sequence.Count < 1) vData.useDefaultSeq = true;
            }
        }
        return vData;
    }
}
/// <summary>
/// информация о последовательности отображаемых приложений и 
/// отображаемые данные с учетом выбранного языка UI и конфигурации для локализации 
/// (т.е. например для France может отображатся English но в другой последовательности)
/// </summary>
[System.Serializable]
public class CP31CfgInfo
{
	public int parsedCode = 0;

	public List<string> sequence;
    public List<CP31AppInfo> apps;	

	public JSONObject ToJSON()
	{
		JSONObject json = JSONObject.obj;
       
		var jsonArr = JSONObject.arr;
        if (sequence != null && sequence.Count>0)
        {
            json.AddField(@"seq", jsonArr);
            jsonArr = json.GetField(@"seq");
            for (int i = 0; i < sequence.Count; i++) jsonArr.list.Add(JSONObject.StringObject(sequence[i]));
        }

        if (apps != null && apps.Count > 0)
        {
            jsonArr = JSONObject.arr;
            json.AddField(@"apps", jsonArr);
            jsonArr = json.GetField(@"apps");
            for (int i = 0; i < apps.Count; i++)
                jsonArr.AddField(apps[i].shortBundleId, apps[i].ToJSON());            
            jsonArr = JSONObject.arr;
        }

		return json;
	}

    public static CP31CfgInfo FromJSON(JSONObject json)
	{
        var vData = new CP31CfgInfo();
        JSONObject jsn;
		vData.sequence = new List<string>();
		vData.parsedCode = 1;
		if (json != null) {
			if ( json.keys.Contains(@"seq") ) {
				jsn = json.GetField(@"seq");		
				if (jsn != null) for(int i =0; i<jsn.list.Count; i++) vData.sequence.Add(jsn.list[i].str);
				else {
                    vData.parsedCode = -1;
                    return new CP31CfgInfo();
                }
			}            
            //smart algo without data verified
            if (vData.apps == null) vData.apps = new List<CP31AppInfo>();
            else vData.apps.Clear();          

            if (json.keys.Contains(@"apps")) {
                jsn = json.GetField(@"apps");
                //Debug.Log("parse apps:".AddTime() + jsn);
                if (jsn != null)
                {                  
                    for (int i = 0; i < jsn.keys.Count; i++ )
                    {
                        var o = jsn.GetField(jsn.keys[i]);
                        if (o != null || o.IsObject) vData.apps.Add(CP31AppInfo.FromJSON(jsn.keys[i], o));
                    }                 
                }
                else
                {
                    vData.parsedCode = -2;
                    return new CP31CfgInfo();
                }
            }

		} else vData.parsedCode = 2;
		return vData;
	}

    internal void Clear()
    {
        if (sequence != null) sequence.Clear();
        if (apps != null) apps.Clear();
    }
}
