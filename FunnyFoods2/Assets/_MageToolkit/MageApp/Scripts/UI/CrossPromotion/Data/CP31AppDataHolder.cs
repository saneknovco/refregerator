﻿using UnityEngine;
using System.Collections.Generic;

public class CP31AppDataHolder
{
    #region constants
    const int MIN_SEQ = 2;
    const int MAX_APP_TO_STAMP = 4;
    const string SEC_APPS = @"apps";
    const string SEC_CFG_INFO = @"cfgInfo";
    const string SEC_LANG_CFG_INFO = @"langCfgInfo";

    public const string unknownBundleId = "1.31.0.0";
    public const int capacityAppItem = 42;
    #endregion

    #region static
    static public CP31AppDataHolder CreateInstance(string  jsonData)
    {
        var result = new CP31AppDataHolder();       
        if (result.ParseJSON(jsonData)) return result;
        return null;
    }
    #endregion

    #region Constructor

    public CP31AppDataHolder() { }

    public CP31AppDataHolder(string uri, bool fromResources)
    {
       ParseJSON(uri, fromResources);
    }
    #endregion

    #region private members - state data

    private JSONObject _jsonData = null;//raw data - cp31.json
    private MageAppUILang _currentLang = MageAppUILang.none;// язык для которого распарсены данные
    private CP31CfgInfo _cfgInfo;// информация о Cross-promotion для текущей локализации
    private List<string> _seqBundleId; //последоватенльность shortBundleId для отображения приложений в Cross-promotion
    private int _countRandomStamp = 1;//колличество сменяемых иконок <=MAX_APP_TO_STAMP

    #endregion

    #region Prpoerties
    string _bundleId = string.Empty;
    public string BundleId
    {
        get { return _bundleId; }       
    }
    
    private List<string> _stampBundles;
    /// <summary>
    /// список shortBundleId приложений для показа в марке
    /// </summary>
    public List<string> StampBundles
    {
        get
        {
            if (_currentLang != AppState.instance.CurrentLang) ParserRawJson();
			if(_stampBundles == null)_stampBundles = new List<string>();
            if (_stampBundles.Count < 1) {
                _stampBundles.Add(string.Empty);//dummy - предотвращаем потенциальный exception
                DisabledStamp();
            }
            return 	_stampBundles;
        }
    }

    private int _currentStampIdx = 0;
    /// <summary>
    /// случайный выбор следующей иконки для показа
    /// </summary>
    /// <param name="reset"></param> 
    public void SetNextStampIdx(bool reset = false)
    {
        if( reset ) {
            _currentStampIdx = 0;
            return;
        }
        
        if (_currentStampIdx == 0 && _stampBundles != null) {
            _countRandomStamp = _stampBundles.Count;
            if(_countRandomStamp > 1) _currentStampIdx = Random.Range(1, _countRandomStamp);
        }
        else _currentStampIdx = 0;
    }

    /// <summary>
    /// какую иконку показывать
    /// </summary> 
    public string ActiveStampBundle
    {
        get {
            var stamps = StampBundles;
            if (_currentStampIdx >= stamps.Count) _currentStampIdx = 0;
            return stamps[_currentStampIdx];
        }
    } 

    /// <summary>
    /// Банер (хот)
    /// </summary>
    public CP31AppInfo ActiveBanner
    {
        get { return Apps[0]; }        
    }

    private List<CP31AppInfo> _apps;
    /// <summary>
    /// приложения для Cross-promotion
    /// </summary>
    public List<CP31AppInfo> Apps
    {
        get
        {
            if (_currentLang != AppState.instance.CurrentLang) 
                if(!ParserRawJson()) DisabledStamp();
                
            if (_apps == null) _apps = new List<CP31AppInfo>();
            if (_apps.Count < 1) _apps.Add(new CP31AppInfo());
            return _apps;
        }
    }

	
    /// <summary>
    /// копия списка последовательности shortBundleId, которая отображается в Cross
    /// </summary>
    /// <param name="lang"></param>
    /// <returns></returns> 
    public List<string> GetSequenceApp(MageAppUILang lang)
    {        
        if (lang == MageAppUILang.none) lang = AppState.instance.CurrentLang;
		var defLang = AppSettings.instance.UI.defaultLang;
        var langs = AppSettings.instance.UI.supportedLang;
        var isSupport = langs.SafeGet(lang, false);

        //if (!isSupport) return new List<string>();
		if (!isSupport) lang = AppSettings.instance.UI.defaultLang;

        JSONObject json = _jsonData.GetField(System.Enum.GetName(lang.GetType(), lang));
        //if (json == null) return new List<string>();
		if (json == null)
		{
			if (lang != defLang)
			{
				lang = defLang;				
				json = _jsonData.GetField(System.Enum.GetName(lang.GetType(), lang));
			}
			if (json == null) return new List<string>();
		}
		if (!json.keys.Contains(SEC_LANG_CFG_INFO))
		{
			if (lang != AppSettings.instance.UI.defaultLang)
			{
				lang = AppSettings.instance.UI.defaultLang;				
				json = _jsonData.GetField( System.Enum.GetName(lang.GetType(), lang) );
				if (json == null) return new List<string>();
				if (!json.keys.Contains(SEC_LANG_CFG_INFO)) return new List<string>();
			}
			else return new List<string>();
		}
        json = json.GetField(SEC_LANG_CFG_INFO);
        if (json == null || json.IsNull) return new List<string>();

        var langCfgInfo = CP31LangCfgInfo.FromJSON(json);
        List<string> seq = null;
        if(!langCfgInfo.useDefaultSeq)
        {
            if (langCfgInfo.sequence != null) seq = new List<string>(langCfgInfo.sequence);            
        }
        if (seq == null) seq = new List<string>(_cfgInfo.sequence);
        return seq;
    }

    //список для загрузки - содержит все языки appitem/hot/screenshot
    private List<string> _loadingShortBundleIds;
    /// <summary>
    /// список для загрузки - содержит appitem, hots, screenshots
    /// </summary>
    public List<string> LoadingShortBundleIds
    {
        get {
            if (_loadingShortBundleIds == null) _loadingShortBundleIds = new List<string>();
            return _loadingShortBundleIds; 
        }       
    }
    #endregion

    #region Broadcast Message
    private void DisabledStamp()
    {
        if (Messenger.eventTable.ContainsKey(AppState.EventTypes.cpStampAction))
            Messenger.Broadcast<CPStampAction, bool>(AppState.EventTypes.cpStampAction, CPStampAction.Enabled, false);
    }
    #endregion

    #region  Подготовка списка файлов изображений для удаленной загрузки

    private void AddHotItemToLoadList(string shortBundelId, MageAppType appType, List<string> sequenceApp, List<string> hots)
    {
        if (sequenceApp == null) return;
        if (sequenceApp.Count < 1) return;

		var appInfo = AppSettings.instance.appInfo;
		var allApps = _cfgInfo.apps;
        int countApp = sequenceApp.Count;


        //обрабатываем как lite & Full (как правило в seq сначало full затем lite ) или как Freemium
        for (int i = 0; i < countApp; i++)
        {
            var o = allApps.Find(x => x.shortBundleId == sequenceApp[i]);
            if (o == null) continue;
//#if UNITY_IOS
//            if ((o.shortBundleId != shortBundelId) && ((o.appType == appType) || (o.appType == MageAppType.Freemium)
//                || (appType == MageAppType.Freemium && o.appType != MageAppType.Full )))
//#else			
			
//			if(!appInfo.IsCurrentAppProduct(o.shortBundleId)
//				    && (o.appType == appType || o.appType == MageAppType.Lite || o.appType == MageAppType.Freemium))

//#endif
            if (!appInfo.IsCurrentAppProduct(o.shortBundleId)
                && ((o.appType == appType || o.appType == MageAppType.Freemium) || (appType == MageAppType.Freemium && o.appType == MageAppType.Lite)))

            {
                //добавляем если нет
                if (!hots.Contains(sequenceApp[i] + CP31AppInfo.bannerSuffix))
                {
                    hots.Add(sequenceApp[i] + CP31AppInfo.bannerSuffix);
#if MAGE
                    hots.Add(sequenceApp[i] + CP31AppInfo.screenshotSuffix);
#endif
                }
                // останавливаем парсинг так как элементы уже есть
                break;
            }
        }
    }

    private void PrepareAppInfoToLoadList()
    {
		var appInfo = AppSettings.instance.appInfo;
		var appType = appInfo.activeAppType;
		var sbid =  appInfo.shortBundleId;
	
        var allApps = _cfgInfo.apps;

        if (_loadingShortBundleIds == null) _loadingShortBundleIds = new List<string>();
        else _loadingShortBundleIds.Clear();
        foreach (var item in allApps)
        {
//#if UNITY_IOS
//			if ((item.shortBundleId != sbid) && ( (item.appType == appType || item.appType == MageAppType.Freemium)
//                    || (appType == MageAppType.Freemium && item.appType != MageAppType.Full))) 
//                _loadingShortBundleIds.Add(item.shortBundleId);
//#else
//			//if ( ((item.shortBundleId != sbid) && (item.shortBundleId != fullver_sbid) && (item.shortBundleId != litever_sbid)) 
//			if ( !appInfo.IsCurrentAppProduct(item.shortBundleId) 
//			    && (item.appType == appType || item.appType == MageAppType.Lite || item.appType == MageAppType.Freemium) ) 
//				//&& ( (item.appType == appType || item.appType == MageAppType.Freemium)
//				//                                 || (appType == MageAppType.Freemium && item.appType != MageAppType.Full))) 
//				_loadingShortBundleIds.Add(item.shortBundleId);
//#endif

            if (!appInfo.IsCurrentAppProduct(item.shortBundleId)
                && ((item.appType == appType || item.appType == MageAppType.Freemium) || (appType == MageAppType.Freemium && item.appType == MageAppType.Lite)))
                _loadingShortBundleIds.Add(item.shortBundleId);
        }

        var hots = new List<string>();
        AddHotItemToLoadList(sbid, appType, _cfgInfo.sequence, hots);
        /// == try Избыточно, но более надежная защита от ошибок в json file
        try
        {
            JSONObject json = null;
            List<string> seq = null;
            var langs = AppSettings.instance.UI.supportedLang;
            foreach (var langItem in langs)
            {
                if (langItem.Value == false || langItem.Key == MageAppUILang.none) continue;
                string langStr = System.Enum.GetName(langItem.Key.GetType(), langItem.Key);                
                json = _jsonData.GetField(langStr);
                if (json == null) continue;
                if (!json.keys.Contains(SEC_LANG_CFG_INFO)) continue;
                json = json.GetField(SEC_LANG_CFG_INFO);
                if (json == null || json.IsNull) continue;
                
                seq = null;
                var langCfgInfo = CP31LangCfgInfo.FromJSON(json);
                if (!langCfgInfo.useDefaultSeq)
                {                    
                    if (langCfgInfo.sequence != null)// && langCfgInfo.sequence.Count >= MIN_SEQ)
                    {
                        seq = langCfgInfo.sequence;
                        AddHotItemToLoadList(sbid, appType, seq, hots);
                    }
                }
            }
        }
        catch
        {
            Debug.LogError("PrepareAppInfoToLoadList: parsing hots is failed!");
        }
        /// == try Избыточно, но более надежная защита от ошибок в json file
        
        for (int i = 0; i < hots.Count; i++) 
            _loadingShortBundleIds.Add(hots[i]);            
    }

    #endregion 

    #region Parsing json file

    /// <summary>
    /// парсер raw(json) данных в структуру данных для Cross-promotion
    /// </summary>
    /// <returns></returns>
    private bool ParserRawJson()
	{
        if(_jsonData == null) return false;
        JSONObject json = null;        
        if(_cfgInfo == null) {
            if (!_jsonData.keys.Contains(SEC_CFG_INFO)) return false;
            json = _jsonData.GetField(SEC_CFG_INFO);
			if(json == null) return false;            
            _cfgInfo = CP31CfgInfo.FromJSON(json);
            if (_cfgInfo.parsedCode <= 0)return false;
		}
        // проверяем возможность парсинга активного языка UI
        json = null;  
		_currentLang = AppState.instance.CurrentLang;       
        string langStr = System.Enum.GetName(typeof(MageAppUILang), _currentLang);
        //Debug.Log("first parse: ".AddTime() + langStr);
        if (_jsonData.keys.Contains(langStr)) {
            json = _jsonData.GetField(langStr);
            if ((json == null) || (!json.keys.Contains(SEC_LANG_CFG_INFO))) _currentLang = AppSettings.instance.UI.defaultLang;               
        }
        else _currentLang = AppSettings.instance.UI.defaultLang;       
        langStr = System.Enum.GetName(typeof(MageAppUILang), _currentLang);

        if ( (json == null) || (_currentLang == AppSettings.instance.UI.defaultLang) ) json = _jsonData.GetField(langStr);
        if ( (json == null) || !json.keys.Contains(SEC_LANG_CFG_INFO)) return false;

        json = json.GetField(SEC_LANG_CFG_INFO);
        if (json == null) return false;
        //задание последовательности отображения приложений в списке(for stamp, banner & appItems)
        var langCfgInfo = CP31LangCfgInfo.FromJSON(json); // информация о  приложениях для текущего языка UI       
        _seqBundleId = _cfgInfo.sequence;
        if (!langCfgInfo.useDefaultSeq){
            if (langCfgInfo.sequence != null && langCfgInfo.sequence.Count >= MIN_SEQ)
                _seqBundleId = langCfgInfo.sequence;
        }

        //какой язык в конце концов парсим       
        var parsedLang = langCfgInfo.useDefaultLang ? AppSettings.instance.UI.defaultLang : _currentLang;
        _currentLang = AppState.instance.CurrentLang;
        langStr = System.Enum.GetName(parsedLang.GetType(), parsedLang);
        //Debug.Log(" final lang :  ".AddTime() + langStr);
        json = null;
        if (_jsonData.keys.Contains(langStr)) json = _jsonData.GetField(langStr);      
        if (json == null) return false;
        // берем инфу для заполнения 
        if (json.keys.Contains(SEC_LANG_CFG_INFO)) json = json.GetField(SEC_LANG_CFG_INFO);
        if (json == null) return false;       
        if (json.keys.Contains(SEC_APPS)) json = json.GetField(SEC_APPS);// from parsed lang info
        else return false;
        if (json == null || !json.IsObject) return false;

        //делаем нужный список
		var appInfo = AppSettings.instance.appInfo;
		var appType = appInfo.activeAppType;

#if UNITY_IOS
		var sbid = appInfo.shortBundleId;
#endif
        if(_apps == null) _apps = new List<CP31AppInfo>();
        else _apps.Clear();
		bool firstApp = true;
        for(int i = 0; i< _seqBundleId.Count; i++ )
        {
            var o = _cfgInfo.apps.Find(x => x.shortBundleId == _seqBundleId[i]);
            if (o != null) {
                //#if UNITY_IOS
                //                if ( (o.shortBundleId != sbid) 
                //                    && ( ( appType == MageAppType.Freemium && o.appType!= MageAppType.Full ) 
                //                       ||( o.appType == appType || o.appType == MageAppType.Freemium)) )
                //#else
                //			if (!appInfo.IsCurrentAppProduct(o.shortBundleId)
                //					&& (o.appType == appType || o.appType == MageAppType.Lite || o.appType == MageAppType.Freemium)) 
                //#endif

                ////if (!appInfo.IsCurrentAppProduct(o.shortBundleId)
                ////		&& (o.appType == appType || o.appType == MageAppType.Freemium)) 

                if (!appInfo.IsCurrentAppProduct(o.shortBundleId)
                    && ((o.appType == appType || o.appType == MageAppType.Freemium) || (appType == MageAppType.Freemium && o.appType == MageAppType.Lite)))
                    
                {
					if ( firstApp || !MgCommonUtils.IsAppInstalled(o.appId) )
					{
						o.UpdateLangInfo(CP31LangAppInfo.FromJSON(_seqBundleId[i], json));
						_apps.Add(o);
						firstApp = false;
					}
                }
            }
        }
        // делаем список для отображения в рекламной марке
        if (_stampBundles == null) _stampBundles = new List<string>();
        else _stampBundles.Clear();
        _countRandomStamp = _apps.Count < MAX_APP_TO_STAMP ? _apps.Count : MAX_APP_TO_STAMP;
        for (int i = 0; i < _countRandomStamp;i++ )
            _stampBundles.Add(_apps[i].shortBundleId);        

        return true;
	}

    public bool ParseJSON(string data)
    {
        ClearAppData();        
        _jsonData = new JSONObject(data);
        if (_jsonData == null || _jsonData.IsNull) return false;

        var oldId = _bundleId;
        if (_jsonData.HasField(@"BundleId")) _bundleId = _jsonData[@"BundleId"].str;
        else _bundleId = unknownBundleId;

        var result = ParserRawJson();
        if (result) PrepareAppInfoToLoadList();
        else _bundleId = oldId;//revert state
        return result;
    }
    
    public bool ParseJSON(string uri, bool fromResources)
   {
        string data = string.Empty;
        try
        {
            if (fromResources)
            {
                var asset = Resources.Load<TextAsset>(uri);
                if (asset != null) data = System.Text.UTF8Encoding.UTF8.GetString(asset.bytes);
            }
            else
            {
                var bytes = System.IO.File.ReadAllBytes(uri);
                data = System.Text.UTF8Encoding.UTF8.GetString(bytes);
            }
            if (string.IsNullOrEmpty(data)) return false;
            else return ParseJSON(data);
        }
        catch
        {
            return false;
        }
   }

    public bool SafeParseNewJSON()
    {
        var controller = CPStateController.instance;
        CPStateController.UpdateState = DLCLoader.LoadingState.Finalize;
        
        try
        {
            int curUpdate = controller.ActiveUpdate;
            controller.ActiveUpdate = controller.NewUpdate;

            if (!ParseJSON(controller.localCPConfigUri, controller.ActiveUpdate == 0)) {     
                controller.ActiveUpdate = curUpdate;
                if (!ParseJSON(controller.localCPConfigUri, controller.ActiveUpdate == 0)) {
                    controller.ActiveUpdate = 0;
                    if (!ParseJSON(controller.localCPConfigUri, true)) {
                        DisabledStamp();
                        return false;
                    }
                }
            }
        }
        catch
        {
            // CP31_WARNING
            if(controller!=null)controller.ActiveUpdate = 0;
            Debug.LogError(@"SafeParseNewJSON exception: ".AddTime());
            DisabledStamp();
            return false;            
        }        
        return true;
    }

    #endregion

    #region reset state
    public void ClearAppData()
    {   
        if (_cfgInfo != null) {
            _cfgInfo.Clear();
            _cfgInfo = null;
        }

        if(_jsonData != null) {
            _jsonData.Clear();
            _jsonData = null;
        }
        //Warning!
        _currentLang = MageAppUILang.none;
    }
    #endregion
}
