﻿
[System.Serializable]
public enum CPStampAction 
{
    None                = 0,    
    Enabled             = 1, 
    Show                = 2,
    RemoteUpdateData    = 3
};

[System.Serializable]
public struct CPStampState
{
    public bool enabled;// = true;
    public bool show;// = true;
}
