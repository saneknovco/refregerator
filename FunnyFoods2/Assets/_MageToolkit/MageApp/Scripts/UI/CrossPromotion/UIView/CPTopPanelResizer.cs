﻿using UnityEngine;

/// <summary>
/// скрипт для изменения длины верхней панели (scale x)
/// для аспектов больше чем 3:2 и меньше чем 4:3
/// </summary>
[DisallowMultipleComponent]
public class CPTopPanelResizer : MonoBehaviour
{
    public Camera workCamera;

    void Awake()
    {   
        if (workCamera == null) workCamera = Camera.main;
        var aspect = workCamera.aspect;
        //just landscape
        if (aspect < 1.0f) return;

        float coefScale = 1.0f;       
        if (aspect >= 1.49f) coefScale =  2.0f / 3.0f; //if device >= 16:10       
        else if(aspect < 1.28f) coefScale = 3.0f / 4.0f; //if device < 4:3
        if (Mathf.Approximately(coefScale, 1.0f)) return;

        float scale = coefScale * aspect;
        Vector3 v = transform.localScale;
        v.x = scale * v.x;
        transform.localScale = v;
    }
}
