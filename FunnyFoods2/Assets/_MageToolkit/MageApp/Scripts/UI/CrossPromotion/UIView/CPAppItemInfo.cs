﻿using UnityEngine;
using System.Collections;

[DisallowMultipleComponent]
public class CPAppItemInfo : MonoBehaviour
{
    public string shortBundleId;	
    public string uriInStore;
    public string[] iconLabelNames = { "f1", "f2", "f3", "f4" }; 

	private GameObject _goIcon;
    private GameObject[] _goLabels = new GameObject[0];
	//private GameObject _goFlag;

	private GameObject _goTextTitle;
	private GameObject _goTextDescript;
    

	public int UpdateItemInfo(int indexItem)
	{
        int result = 0;
        gameObject.SetActive(true);

        if (_goIcon == null) _goIcon = gameObject.FindChild(@"icon");
        if (_goTextTitle == null) _goTextTitle = gameObject.FindChild(@"textTitle");
		if (_goTextDescript == null) _goTextDescript = gameObject.FindChild(@"textDescript");
        if (_goIcon == null || _goTextTitle == null || _goTextDescript == null)
        {
            if(Debug.isDebugBuild)Debug.LogError("CrossPromotion: not config app item");
            return 1;
        }
        _goIcon.SetActive(true);

        //if (_goFlag == null) _goFlag = gameObject.FindChild(@"flag");
        //if (_goFlag == null) return;

        if (iconLabelNames != null)
        {
            if (_goLabels==null || _goLabels.Length!=iconLabelNames.Length)
                _goLabels = new GameObject[iconLabelNames.Length];
            for(int i=0; i< iconLabelNames.Length; i++)
            {
                if (_goLabels[i] == null) _goLabels[i] = gameObject.FindChild(iconLabelNames[i]);
            }
        }

		var holder = CPStateController.instance.appDataHolder;
        if (holder == null || holder.Apps == null)
        {
            gameObject.SetActive(false);// not tested
            return -10;
        }
        
		CP31AppInfo item = null;
		if (indexItem >= 0 && indexItem < holder.Apps.Count) item = holder.Apps[indexItem];
        if (item == null)
        {
            gameObject.SetActive(false);// not tested
            return -11;
        }
      
        uriInStore = item.uriInStore;
		shortBundleId = item.shortBundleId;

        Texture2D tex2D = CPStateController.instance.SmartLoadImage(item.shortBundleId);
        _goIcon.GetComponent<Renderer>().material.mainTexture = tex2D;
        if (tex2D == Texture2D.blackTexture)
        {
            var sdw = _goIcon.transform.parent;
            if (sdw) sdw.gameObject.SetActive(false);
            else _goIcon.SetActive(false);
            result = -1;
        }

		//_goFlag.SetActive(item.flag);
        if( item != null && item.langInfo != null)
        {
            for (int i = 0; i < _goLabels.Length; i++)
            {
                if (_goLabels[i] != null) _goLabels[i].SetActive(_goLabels[i].name == item.langInfo.iconLabel);
            }

            if (item.langInfo.appText != null)
            {
                var textMesh = _goTextTitle.GetComponent<TextMesh>();
                if (textMesh)
                {
                    textMesh.text = item.langInfo.appText.h1;
                    _goTextTitle.transform.localPosition = item.langInfo.appText.h1Position;
                    if (string.IsNullOrEmpty(textMesh.text)) result = -2;
                }
                textMesh = _goTextDescript.GetComponent<TextMesh>();
                if (textMesh)
                {
                    textMesh.text = item.langInfo.appText.h2;
                    _goTextDescript.transform.localPosition = item.langInfo.appText.h2Position;
                }
            }
			#if MAGE
			var flagGo = gameObject.FindChild("flag");
			if(flagGo != null)
				if (item.appType != MageAppType.Full) flagGo.SetActive(true);			
			#endif
        }

        return result;
	}	

}
