﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

[DisallowMultipleComponent]
public class CPAppHotInfo : ButtonWidget
{
	#region Members

	[FullInspector.InspectorCategory("Action")]
    [FullInspector.InspectorHeader("Параметры Cross Promo:")]
    [FullInspector.InspectorMargin(16)]
    public string shortBundleId;

    [FullInspector.InspectorCategory("Action")]    
	public string uriInStore;

	public string GetUri()
	{
		return uriInStore;
	}

	private CPStateController _cpState;

	public int UpdateAppInfo()
	{
		_cpState = CPStateController.instance;

		var  _goIcon = gameObject.FindChild(@"iconHot");
		if (_goIcon == null) return 1;
        var _goText = gameObject.FindChild(@"textHot");
        if (_goText == null) return 2;
        var _goDescript = gameObject.FindChild(@"textDescript");

        _goIcon.SetActive(true);
		var holder = _cpState.appDataHolder;
		//if(holder == null) return;
        var banner = holder.ActiveBanner;
        if (banner == null) return -10;

        int result = 0;

        uriInStore = banner.uriInStore;
		shortBundleId = banner.shortBundleId;

		Texture2D tex2D = _cpState.SmartLoadImage(banner.bannerIcon);
        _goIcon.GetComponent<Renderer>().material.mainTexture = tex2D;
        if(tex2D == Texture2D.blackTexture)
        {   
            var sdw = _goIcon.transform.parent;
            if (sdw) sdw.gameObject.SetActive(false);
            else _goIcon.SetActive(false);
            result = -1;
        }

        if (banner.langInfo != null && banner.langInfo.bannerText != null)
        {
            var textMesh = _goText.GetComponent<TextMesh>();
            if (textMesh)
            {
                textMesh.text = banner.langInfo.bannerText.h1;
                _goText.transform.localPosition = banner.langInfo.bannerText.h1Position;
                if(string.IsNullOrEmpty(textMesh.text)) result = -2;               
            }
            if (_goDescript)
            {
                textMesh = _goDescript.GetComponent<TextMesh>();
                if (textMesh)
                {
                    textMesh.text = banner.langInfo.bannerText.h2;
                    _goDescript.transform.localPosition = banner.langInfo.bannerText.h2Position;
                    //if (string.IsNullOrEmpty(textMesh.text)) result = -3;
                }
            }
#if MAGE
            var _goScreenshot = gameObject.FindChild(@"screenshot");
            if (_goScreenshot)
            {
				tex2D = _cpState.SmartLoadImage(banner.screenshot);
                if (tex2D != Texture2D.blackTexture)
                {
                    _goScreenshot.GetComponent<Renderer>().material.mainTexture = tex2D;
                    //_goScreenshot.transform.position = banner.langInfo.
                }
                else
                {
                    _goScreenshot.SetActive(false);
                    result = -4;
                }

            }
#endif
        }
        return result;
	}

	#endregion

	#region Action

	
	protected override void DoAwake()
	{		
		if(_cpState == null) _cpState = CPStateController.instance;

		#if UNITY_ANDROID 
		if(AppSettings.instance.appInfo.activeAppType != MageAppType.Full)
			widgetSettings.parentalGate = false;
		#endif
	}

	protected override bool DoBreakTouch()
	{
		var result =  base.DoBreakTouch();
		if (result == false) result = _cpState.IsClosingUIView;
		return result;
	}

	protected  override void DoTouch()
	{
#if MAGE_ANALYTICS
		//UnityEngine.Analytics.Analytics.CustomEvent(this.GetType().Name, new Dictionary<string, object>
		//	{
		//		{"action",  "touch"},
		//		{"bundleId", shortBundleId }
		//	});
		UnityEngine.Analytics.Analytics.CustomEvent(_cpState.cpUIViewTypeName, new Dictionary<string, object>
		    {
				 {"action", "touchHot"},
                 {"bundleId",shortBundleId }
			});
#endif

		if (widgetSettings.parentalGate)
        {
            AppState.instance.parentalGateSvc.CreateUIView(p =>
            {
#if MAGE_ANALYTICS
				//UnityEngine.Analytics.Analytics.CustomEvent(this.GetType().Name, new Dictionary<string, object>
				//	{
				//		{"action",  "open"},
				//		{"bundleId", shortBundleId }
				//	});
				UnityEngine.Analytics.Analytics.CustomEvent(_cpState.cpUIViewTypeName, new Dictionary<string, object>
					{
						 {"action", "openHot"},
						 {"bundleId",shortBundleId }
					});
#endif
				Application.OpenURL(GetUri());
            });
        }
        else
        {
#if MAGE_ANALYTICS
			//UnityEngine.Analytics.Analytics.CustomEvent(this.GetType().Name, new Dictionary<string, object>
			//	{
			//		{"action",  "open"},
			//		{"bundleId", shortBundleId }
			//	});
			UnityEngine.Analytics.Analytics.CustomEvent(_cpState.cpUIViewTypeName, new Dictionary<string, object>
				{
					 {"action", "openHot"},
					 {"bundleId",shortBundleId }
				});
#endif
			Application.OpenURL(GetUri());
        }
	}

	#endregion
	
}
