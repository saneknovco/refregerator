﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using FullInspector;

[DisallowMultipleComponent]
public class CPUIViewManager : FullInspector.BaseBehavior
{
	#region Members

	public System.Action<MonoBehaviour> parentalGateReject;

    [InspectorOrder(0.1)]
    public bool parentalGate = false;

    [InspectorOrder(1)]
	public Camera workCamera = null;
    [InspectorOrder(2)]
	public GameObject appItemController = null;
    [InspectorOrder(2.1)]
	public GameObject appItemFirst = null;
    [InspectorOrder(2.2)]
	public GameObject appHotController = null;
	//[InspectorOrder(2.3)]
	//public GameObject supportWidget;
    //[InspectorOrder(2.4)]
	//public GameObject reviewUIView;
    [InspectorOrder(3)]
    public GameObject[] tkIgnoreBounds;

    [InspectorOrder(3.2)]
    public float startYAppItem = 0.0f;//-448
    [InspectorOrder(3.3)]
    public int countAppItemInPanelTile = 2;

    [InspectorOrder(4)]
	public float delayTimeToStartTouch = 1.2f;
    [InspectorOrder(4.1)]
    public int waitFrameByResize = 1;

	#region parameters to scrolling with smooth 
    [InspectorOrder(5)]
	public float scrollDeltaBound = 96.0f;
    [InspectorOrder(5.1)]
    public float smoothSensivity = 60.0f;//70.0f
    [InspectorOrder(5.2)]
    public float smoothSpeed = 10.0f;//7

	//region to recognizer
	private float _minY0;
	private float _maxY0;
	private float _minY;
	private float _maxY;
	
	private Vector3 _smoothDelta;
	private bool _smoothIsMoving = false;

	private float _colliderDistance = 10000.0f;
	private float _appItemH = 244.0f;

    const int baseMinAppItem = 4;
    private int _minAppItem = baseMinAppItem;
	
	#endregion

	//private SupportButton _supportBtn = null;
	private List<GameObject> _cpAppItemList = new List<GameObject>(42);
	private List<GameObject> _appItemList = new List<GameObject>(42);    
   
    private Transform _itemControllerTfm;

	private List<Collider> __tkIgnoreBoundsCollider = new List<Collider>();
	private List<Collider> __appItemListCollider = new List<Collider>();

	private Vector2 __blackPinch = new Vector2(-8192, -8192);
	private bool __isPanComplete = false;
	private bool _parentalGateToAppItem = false;


	#endregion

	#region  Detect touch

	//Prevent tap from other UIView tap
	private bool _lockTap = true;
	/// <summary>
	/// дополнительные условия блокировки обработки touch
	/// </summary>
	private bool lockAppItemController
	{
		get
		{
			if (_lockTap) return true;
			//if (_supportBtn != null)
			//	if (_supportBtn.isWidgetTouch) return true;
			//if (reviewUIView != null)
			//	if (reviewUIView.activeSelf) return true;

			if (CPStateController.instance.IsClosingUIView) return true;
			if (ButtonWidget.countTouchInProcess > 0) return true;
			return false;
		}
	}

	//кэшированные переменные для TouchInLockArea
	private Vector3 __touchLocation = Vector3.zero;
	private bool TouchInLockArea(Vector2 touchLocation)
	{
		if (tkIgnoreBounds == null) return false;
		RaycastHit hit;
		__touchLocation.x = touchLocation.x;
		__touchLocation.y = touchLocation.y;
		__touchLocation.z = 0.0f;
		Ray ray = workCamera.ScreenPointToRay(__touchLocation);
		for (int i = 0; i < __tkIgnoreBoundsCollider.Count; i++)
			if (__tkIgnoreBoundsCollider[i].Raycast(ray, out hit, _colliderDistance)) return true;
		return false;
	}	

	/// <summary>
	/// определение нажатие на appItem в scroll boxe
	/// </summary>
	/// <param name="mousePos"></param>
	private void DoClickAppLabel(Vector3 mousePos)
	{
		if (lockAppItemController) return;

		var ray = workCamera.ScreenPointToRay(mousePos);
		RaycastHit hit;
		for (int i = 0; i < __tkIgnoreBoundsCollider.Count; i++)
			if (__tkIgnoreBoundsCollider[i].Raycast(ray, out hit, _colliderDistance)) return;

		foreach (var item in __appItemListCollider)
		{
			if (item.Raycast(ray, out hit, _colliderDistance))
			{
				var uriContainer = item.gameObject.GetComponent<CPAppItemInfo>();
				if (uriContainer != null)
				{
					MgAudioHelper.Click();
#if MAGE_ANALYTICS
                    UnityEngine.Analytics.Analytics.CustomEvent(this.GetType().Name, new Dictionary<string, object>
                        {
                            {"action", "touch"},
                            {"bundleId", uriContainer.shortBundleId }
                        });
#endif
                    StartCoroutine(MgCoroutineHelper.WaitEndOfFrameThenCallback(() =>
					{
						if (_parentalGateToAppItem)
                        {
                            if ((AppState.instance.UIViewMode & MageAppUIViewMode.ParentalGateView)
                                != MageAppUIViewMode.ParentalGateView)
                            {
                                AppState.instance.parentalGateSvc.CreateUIView(p =>
                                {

#if MAGE_ANALYTICS
                                    UnityEngine.Analytics.Analytics.CustomEvent(this.GetType().Name, new Dictionary<string, object>
                                        {
                                            {"action",  "open"},
                                            {"bundleId", uriContainer.shortBundleId }
                                        });
#endif
                                    Application.OpenURL(uriContainer.uriInStore);
                                },
                                parentalGateReject);
                            }
                        }
                        else
                        {

#if MAGE_ANALYTICS
                            UnityEngine.Analytics.Analytics.CustomEvent(this.GetType().Name, new Dictionary<string, object>
                                {
                                    {"action",  "open"},
                                    {"bundleId", uriContainer.shortBundleId }
                                });
#endif

                            Application.OpenURL(uriContainer.uriInStore);
                        }
					}));
					return;
				}
			}
		}
	}

	#endregion

	#region Touch Kit Recognizers

	private IEnumerator AsyncResizeUIWidgets()
	{
		Vector3 v1 = transform.position;
		float hide_plane = 2.0f * workCamera.farClipPlane;
		v1.z += hide_plane;
		transform.position = v1;
		for (int i = 0; i < waitFrameByResize; i++) yield return null;
		ResizeUIWidgets();
		v1.z -= hide_plane;
		transform.position = v1;
	}

	private TKPanRecognizer _recognizer = null;
	private TKTapRecognizer _recognizerTap = null;
	private void ResizeUIWidgets()
	{ 
		#region Config gestureRecognized
        
        int visibleRow = _appItemList.Count - _minAppItem;
        if (countAppItemInPanelTile == 1) visibleRow += (_minAppItem - baseMinAppItem + 2);

        if (visibleRow < 1) visibleRow = 1;
        _minY = _itemControllerTfm.localPosition.y;
        _maxY = _minY + _appItemH * visibleRow;    
		_minY0 = _minY - scrollDeltaBound;
		_maxY0 = _maxY + scrollDeltaBound;       

        RemoveRecognizers();

		float velocity = TouchKit.instance.pixelsToUnityUnitsMultiplier.x;
        //scroll
		Vector2 touchLocation = __blackPinch;		
		_recognizer = new TKPanRecognizer();		
        _recognizer.boundaryFrame = new TKRect(0, 0, Screen.width, Screen.height);
		_recognizer.gestureRecognizedEvent += (r) =>
		{
			touchLocation = r.touchLocation();			
			if (lockAppItemController || TouchInLockArea(touchLocation)) return;            
            _smoothIsMoving = false;
            var v = _itemControllerTfm.localPosition;
            _itemControllerTfm.localPosition = new Vector3(v.x,
                Mathf.Clamp(v.y + velocity*r.deltaTranslation.y, _minY0, _maxY0),v.z);
		};
		_recognizer.gestureCompleteEvent += r =>
		{
			//Debug.Log("pan complete: " + touchLocation + " " + r.touchLocation() + " " + r.startTouchLocation() + " " + r.deltaTranslation + " " + r.deltaTranslationCm);
			__isPanComplete = true;
			if (lockAppItemController || TouchInLockArea(touchLocation)) {
				touchLocation = __blackPinch;
				return;
			}                      
			_smoothDelta = _itemControllerTfm.localPosition;
			_smoothDelta.y = r.deltaTranslation.y > 0.0f ?
				Mathf.Clamp(_smoothDelta.y +  velocity*r.deltaTranslation.y + smoothSensivity, _minY, _maxY) :
				Mathf.Clamp(_smoothDelta.y +  velocity*r.deltaTranslation.y - smoothSensivity, _minY, _maxY);
			_smoothIsMoving = true;			
			touchLocation = __blackPinch;
		};
		
		_recognizerTap = new TKTapRecognizer(0.7f, 1f);	//30 (div 5 into new version)
        _recognizerTap.boundaryFrame = new TKRect(0, 0, Screen.width, Screen.height);      
		_recognizerTap.gestureRecognizedEvent += (r) =>
		{
			if(__isPanComplete) {
				__isPanComplete = false;
				return;
			}			
			//Debug.Log("tap " +touchLocation + " " +  r.touchLocation() + " " + r.startTouchLocation() + " " + r.state);			
			if (r.state == TKGestureRecognizerState.Recognized ) {				
				Vector2 v = r.touchLocation();				
				_smoothIsMoving = false;				
				DoClickAppLabel(new Vector3(v.x, v.y, 0.0f));			
            }
		};
        // register scroll&tap
		//_recognizerTap.zIndex = 0;
		//_recognizer.zIndex = 101;        
		TouchKit.addGestureRecognizer(_recognizer);
		TouchKit.addGestureRecognizer(_recognizerTap);

		#endregion
    }

    private void RemoveRecognizers()
    {
        if (_recognizer != null) {
            TouchKit.removeGestureRecognizer(_recognizer);
            _recognizer = null;
        }
        if (_recognizerTap != null)
        {
            TouchKit.removeGestureRecognizer(_recognizerTap);
            _recognizerTap = null;
        }
    }
	

	#endregion

	#region Unity event functions

	// если будуь битые текстуры или текст то после закрытия сбрасываем флаг!
    private bool __resetActiveUpdate = false;	
	protected override void Awake()
	{
        base.Awake();
        		
        if(workCamera == null) workCamera = Camera.main;		
		
		if (tkIgnoreBounds == null) tkIgnoreBounds = new GameObject[0];
		if ( tkIgnoreBounds.Length  == 0 ) tkIgnoreBounds = GameObject.FindGameObjectsWithTag("TKIgnoreBound"); 
        else
        {
            if(tkIgnoreBounds.Length >= 1 && tkIgnoreBounds[0] == null)
                tkIgnoreBounds = GameObject.FindGameObjectsWithTag("TKIgnoreBound"); 
        }
        
		Collider cll;
		for (int i=0; i< tkIgnoreBounds.Length; i++)
		{
			if(tkIgnoreBounds[i]!=null)
			{
				cll = tkIgnoreBounds[i].GetComponent<Collider>();
				if(cll!=null)__tkIgnoreBoundsCollider.Add(cll);
			}
		}

        #region config buttons
		//if (supportWidget == null) supportWidget = gameObject.FindChild(@"crossBtnSupport");
        //if (supportWidget != null) _supportBtn = supportWidget.GetComponent<SupportButton>();
        //if (reviewUIView == null) reviewUIView = gameObject.FindChild(@"reviewUIView");
        #endregion

        if (appHotController == null) appHotController = gameObject.FindChild(@"AppHotController");
        if (appHotController != null)
        {
            var appHotInfo = appHotController.FindChild(CP31AppInfo.bannerName).GetComponent<CPAppHotInfo>();
            if (appHotInfo != null)
            {
                if (appHotInfo.UpdateAppInfo() < 0) __resetActiveUpdate = true;
            }
        }

        if (appItemController == null) appItemController = gameObject.FindChild(@"AppItemController");
        _itemControllerTfm = appItemController.transform;

        if (appItemFirst == null) appItemFirst = gameObject.FindChild(CP31AppInfo.appItemName);
//#if MAGE
//        var flagGo = appItemFirst.FindChild("flag");
//        if(flagGo != null)
//            if (AppSettings.Instance.appInfo.ActiveAppType != MageAppType.Full) flagGo.SetActive(true);
//        
//#endif
		_colliderDistance += workCamera.farClipPlane;
        //set position & data info for AppItems        
        var bg = appItemController.FindChild(@"bgItem");
        if (bg != null) {
            var panelParent = bg.transform.parent;
            var bgH = bg.GetComponent<Renderer>().bounds.size.y;
            _appItemH = bgH / (float)countAppItemInPanelTile;            
            var bgpos = bg.transform.localPosition;
            bgpos.y += bgH;
            var go_bk = GameObject.Instantiate(bg, Vector3.zero, Quaternion.identity) as GameObject;
            go_bk.name = bg.name + (0).ToString();
            go_bk.transform.parent = panelParent.transform;
            go_bk.transform.localPosition = bgpos;
            bgpos.y -= 2 * bgH;
            _minAppItem = (int)(0.5f + MgCommonUtils.baseOrtographicSizeToUI / bgH);            
            int countTile = CPStateController.instance.appDataHolder.Apps.Count / countAppItemInPanelTile + (int)(Mathf.Abs(startYAppItem) / bgH) + 2;//2 is magic            
            if (countTile <= _minAppItem) countTile = _minAppItem + (int)(Mathf.Abs(startYAppItem) / bgH) + 2;
            for (int i = 0; i < countTile; i++)
            {
                go_bk = GameObject.Instantiate(bg, Vector3.zero, Quaternion.identity) as GameObject;
                go_bk.name = bg.name + (i + 2).ToString();
                go_bk.transform.parent = panelParent.transform;
                go_bk.transform.localPosition = bgpos;
                bgpos.y -= bgH;
            }
            bg.name = bg.name + (1).ToString();
        }
        else if (Debug.isDebugBuild) Debug.LogError("CPUIViewManager: not detected appItem texture!");

        int count = CPStateController.instance.appDataHolder.Apps.Count;        
        var pos = appItemFirst.transform.position;
        pos.y += startYAppItem;//Сдвигаем если надо
        for (int i = 0; i < count; i++)
        {          
            var go = GameObject.Instantiate(appItemFirst, Vector3.zero, Quaternion.identity) as GameObject;
            go.name = appItemFirst.name + i.ToString();
            _cpAppItemList.Add(go);
            var childItem = go.FindChild(@"AppItem");
            if (childItem) {
               var component = childItem.GetComponent<CPAppItemInfo>();
               if (component) {
                   if (component.UpdateItemInfo(i) < 0) __resetActiveUpdate = true;
               };               
            }
            go.transform.parent = appItemController.transform;
            go.transform.position = pos;
            pos.y -= _appItemH;
        }
        foreach (var item in _cpAppItemList) {
            var go = item.FindChild(@"AppItem");
            if (go!=null) {
				_appItemList.Add(go);
				cll = go.GetComponent<Collider>();
				if(cll != null) __appItemListCollider.Add(cll);
			}
        }
		GameObject.Destroy(appItemFirst);
		if(_cpAppItemList.Count>0) appItemFirst = _cpAppItemList[0];


		_parentalGateToAppItem = parentalGate;
		#if UNITY_ANDROID 
		if(AppSettings.instance.appInfo.activeAppType != MageAppType.Full)
			_parentalGateToAppItem = false;
		#endif
	}
    
    void Start()
	{  
       StartCoroutine(MgCoroutineHelper.WaitThenCallback(delayTimeToStartTouch,
           () => { _lockTap = false; }));
       StartCoroutine(AsyncResizeUIWidgets());
	}
	
	// Update is called once per frame
	void Update () 
	{        
        //эффект пружины и сглаживание движения
        if (_smoothIsMoving)
		{
            _itemControllerTfm.localPosition = Vector3.Lerp(_itemControllerTfm.localPosition, _smoothDelta, 
                smoothSpeed * Time.smoothDeltaTime);
			if (Mathf.Approximately(_itemControllerTfm.localPosition.y, _smoothDelta.y))
			{
				_smoothIsMoving = false;
				__isPanComplete = false;
			}
		}
	}

	void OnDestroy()
	{
		RemoveRecognizers(); 
        if(__resetActiveUpdate) CPStateController.instance.ResetUpdates();
	}

	#endregion
}
