﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

[DisallowMultipleComponent]
public class CPUIViewClose : ButtonWidget
{
    [FullInspector.InspectorCategory("Action")]
    [FullInspector.InspectorHeader("Закрытие Cross-Promo UIView:")]
    [FullInspector.InspectorMargin(16)]
    public bool callUnloadAssets = false;

    [FullInspector.InspectorCategory("Action")]    
    public bool ignoreLockWidget = false;

    protected override bool DoBreakTouch()
    {
         return !widgetSettings.canExecute || (AppState.instance.lockWidgets && !ignoreLockWidget);
    }
 
	protected override void DoTouch()
	{
        if (Messenger.eventTable.ContainsKey(AppState.EventTypes.cpCloseUIView))
        {			
#if MAGE_ANALYTICS
			UnityEngine.Analytics.Analytics.CustomEvent(CPStateController.instance.cpUIViewTypeName, new Dictionary<string, object>
		    {
				 {"action", "close"},
                 {"bundleId","none" }
			});
#endif
			Messenger.Broadcast(AppState.EventTypes.cpCloseUIView);
            if (ignoreLockWidget) widgetSettings.canExecute = false;
        }
	}

	protected override void DoDestroy()
	{		
		if( callUnloadAssets ) Resources.UnloadUnusedAssets();		
	}

#if !UNITY_IOS
	void Update()
	{		
		if (Input.GetKeyUp(KeyCode.Escape))
		{
			if (!AppState.instance.IsShowParentalGate)
			{
				Input.ResetInputAxes();				
				Touch();
			}
		}
	}
#endif

}
