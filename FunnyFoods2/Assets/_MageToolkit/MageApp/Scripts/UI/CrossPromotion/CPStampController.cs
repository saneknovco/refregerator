﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using Mage.Effects;

[DisallowMultipleComponent]
public class CPStampController : ButtonWidget
{
    // параметры аниматора
    const string animIsShowing = "isShowing";
    const string animIsBouncing = "isBouncing";
    const string animIsDisable = "isDisable";

    #region Параметры поведения Марки
    [FullInspector.InspectorCategory("Stamp")]
    [FullInspector.InspectorHeader("Параметры поведения Марки:")]
	[FullInspector.InspectorOrder(10),FullInspector.InspectorMargin(16)]
    public GameObject stamp;
    [FullInspector.InspectorCategory("Stamp")]
	[FullInspector.InspectorOrder(10.1f)]
	[FullInspector.InspectorComment(FullInspector.CommentType.None, "Родительский объект для UIView Cross-promotion (UIContainer)")]
	public GameObject parentUIView = null;
    [FullInspector.InspectorCategory("Stamp")]
	[FullInspector.InspectorOrder(10.2f)]
	[FullInspector.InspectorComment(FullInspector.CommentType.Info, "если parentUIView == null, инициализировать UIContainer, как родительский объект для Cross-promotion")]
    public bool forceUIContainer = true;

    [FullInspector.InspectorCategory("Stamp")]
	[FullInspector.InspectorOrder(10.3f)]
	[FullInspector.InspectorComment(FullInspector.CommentType.Info, "Список объектов которые отключаются при показе UIView Cross-promotion (для повышения fps)")]
	public GameObject[] deactivateObjects = new GameObject[0];

    [FullInspector.InspectorCategory("Stamp")]
	[FullInspector.InspectorOrder(10.4f)]
    public Camera workCamera;

    [FullInspector.InspectorCategory("Stamp")]
	[FullInspector.InspectorOrder(10.5f)]
    public bool leftAnchor = true;
    
	[System.NonSerialized]
    private Vector3 _currentOffset;
	[SerializeField, HideInInspector]
	private Dictionary<MageAppAspectRatio, Vector3> _offset;
	[FullInspector.InspectorCategory("Stamp")]
	[FullInspector.InspectorOrder(10.6f), FullInspector.ShowInInspector, FullInspector.InspectorMargin(16)]
	[FullInspector.InspectorComment(FullInspector.CommentType.None,"Смещении позиции префаба относительно угла отображения и уровень по Z")]
	public Dictionary<MageAppAspectRatio, Vector3> offset
	{
		get
		{
			if (_offset == null) _offset = new Dictionary<MageAppAspectRatio, Vector3>();
			if (!_offset.ContainsKey(MageAppAspectRatio.AspectOthers))
#if MAGE
				_offset.Add(MageAppAspectRatio.AspectOthers, new Vector3(-12.0f, 74.0f, 20.0f));
#else
				_offset.Add(MageAppAspectRatio.AspectOthers, new Vector3(0.0f, 48.0f, 20.0f));
#endif
			return _offset;
		}
		set { _offset = value; }
	}

    [FullInspector.InspectorCategory("Stamp")]
	[FullInspector.InspectorOrder(11f)]
	[FullInspector.InspectorComment(FullInspector.CommentType.None, "Задержка при старте (в фреймах) перед инициализацией и запуском банера CPAppBanner")]
    public int waitFramesInInit = 30;//Задержка при старте
    [FullInspector.InspectorCategory("Stamp")]
	[FullInspector.InspectorOrder(11.1f)]
    public float fadingOutTime = 0.5f;
    [FullInspector.InspectorCategory("Stamp")]
	[FullInspector.InspectorOrder(11.2f)]
    public float fadingInTime = 0.5f;
    [FullInspector.InspectorCategory("Stamp")]
	[FullInspector.InspectorOrder(11.3f)]
    public float timeInShown = 15.0f;
    [FullInspector.InspectorCategory("Stamp")]
	[FullInspector.InspectorOrder(11.4f)]
    public float timeInHidden = 0.5f;

    [FullInspector.InspectorCategory("Stamp")]
	[FullInspector.InspectorOrder(12)]
    public bool resetIconInStampEvent = true;

    [FullInspector.InspectorCategory("Stamp")]
    [FullInspector.InspectorOrder(12.5f)]
    [FullInspector.InspectorComment(FullInspector.CommentType.None, "Parental Gate на банере для других языков")]
    public bool useParentalGateForNoneSupportLang = true;

	[FullInspector.InspectorCategory("Stamp")]
	[FullInspector.InspectorOrder(12.6f)]
	[FullInspector.InspectorComment(FullInspector.CommentType.None, "Всегда переходить на страницу разработчика")]
	public bool allwaysOpenDeveloperPage = false;


    [FullInspector.InspectorCategory("Stamp"), FullInspector.InspectorShowIf("usingPlayMaker")]
	[FullInspector.InspectorTooltip("Отпралять сообщение в PM: " + AppState.PMEvents.cpOpenUIView)]
	[FullInspector.InspectorOrder(14)]
	public bool broadcastDoTouchPMEvent = false;
    #endregion

    private GameObject _goIcon;
	private Transform _trfMove;	
	private Vector3 _visiblePosition;	

    private AppState _appState;
    private CPStateController _stateController;
    private Animator _animator;

    private bool _stampEnabled = true;
    private bool _stampShow = true;
    
    private bool _alwaysLandscape = true;

	private Renderer _goIconRenderer;
    
    private IEnumerator BackgroundStampUpdater()
    {
		float timeBounce =1f;
		bool flag;
        yield return new WaitForSeconds(AppConfig.timeWaitFinishing);
        _stampShow = CPStateController.instance.stampState.show;
      
        while (true)
        {
            if (_animator && _animator.enabled && _stampShow)
            {
                // если режим марки доступен то  запускаем логику изменения иконки
                if (_stampEnabled)
                {
                    flag = _stateController.stampState.show && _stateController.stampState.enabled;
                    _animator.SetBool(animIsShowing, flag);
					if (flag)
                    {
                        timeBounce = Random.Range(0.3f * timeInShown, 0.71f * timeInShown);
                        yield return new WaitForSeconds(timeBounce);
                        if (_animator)
                        {
                            if (!_animator.enabled || !_stampShow)
                            {                                
                                yield return null;
                                //yield return new WaitForSeconds(timeInShown - timeBounce);
                                continue;
                            }
                            if (_animator && _stampEnabled)
                            {
                                _animator.SetTrigger(animIsBouncing);
                                yield return null;
                                yield return new WaitForSeconds(timeInShown - timeBounce);
                            }
                        }
                    }
                    else yield return new WaitForSeconds(timeInHidden);//1.0f//timeInShown
                    
                    if (_animator)
                    {
                        if (!_animator.enabled) continue;
                        if (_stampEnabled && _stateController.stampState.enabled)
                        {
                            _animator.SetBool(animIsShowing, false);

                            yield return new WaitForSeconds(timeInHidden);
                            if (_stampEnabled && _stateController.stampState.enabled)
                            {
                                if (_animator.enabled && _stampShow) // пофиг если иконка меняется в поле видимости && (_animator.transform.localPosition.y < -390.0f))
                                {
                                    _stateController.appDataHolder.SetNextStampIdx();
                                    DoUpdateIcon();
                                }
                            }
                        }
                    }
                }
                // иначе ждем обновления состояния
                else
                {
                    yield return new WaitForSeconds(0.5f);
                }
            }
            else yield return null;
        }  
    } 

    #region CP_MESSAGE

	private Dictionary<string, Texture2D> _iconCache = new Dictionary<string, Texture2D>();	
    private void DoUpdateIcon()
	{		
		if (_goIcon == null) return;
        if(_animator != null && _animator.enabled)
        {
           if (_goIconRenderer != null)
            {
                if (_goIconRenderer.material.mainTexture != null)
                {
                    var currentState = _animator.GetCurrentAnimatorStateInfo(0);
                    if (!currentState.IsName("Base Layer.hiddenStamp") && !currentState.IsName("Base Layer.disabledStamp")) return;
                }
            }
           
        }
		// parsed Json data
        var holder = _stateController.appDataHolder;		
		if (holder == null) return;
		//shortBundle app (name icon)
		string activeStampBundle = holder.ActiveStampBundle;
		if (string.IsNullOrEmpty(activeStampBundle)) return;

		Texture2D txt = null;
		if (_iconCache.ContainsKey(activeStampBundle))	txt = _iconCache[activeStampBundle];
		if(txt == null )
		{
			txt = _stateController.SmartLoadImage(activeStampBundle);
			if (txt == Texture2D.blackTexture || txt == null)
			{
				if (_stateController.ActiveUpdate > 0)
				{
					//HotFix: unknow case or update failure!						
#if MAGE_DEBUG
					Debug.LogError(@"DoUpdateIcon: reset active updates".AddTime());                    
#endif
					if (_stateController.ResetUpdates()) {
						holder = _stateController.appDataHolder;
						txt = _stateController.SmartLoadImage(holder.ActiveStampBundle);
					} 
					else {
						GameObject.Destroy(gameObject);
						return;
					}						
				}
			}
			else _iconCache.Add(activeStampBundle,txt);				
		}
		if (_goIconRenderer != null  && txt != null ) _goIconRenderer.material.mainTexture = txt;
		
	}    
    /// <summary>
    /// message from CPStateController when new data
    /// </summary>
    private void OnStampUpdate()
    {		
		if (__noneInitStamp) return; //rake

		//чистим кэш
		_iconCache.Clear();
		//

		//_обновляем иконку  только когда она спрятана
        //CASE: вохможно двойная генерация апдейта - второй в корутине       
		if(_stampEnabled && (_animator!=null)) DoUpdateIcon();//всегда меняем иконку!!
    }

	private bool __canAsyncUpdateIcon = true;
    private void OnStampAction(CPStampAction action, bool activeState)
    {		
		//if (__noneInitStamp) return; //rake	

		switch (action)
        {          
            case CPStampAction.Show:
                if(_stampShow!=activeState)
                {
                    _stampShow = activeState;
					if (__noneInitStamp) return; //rake	

                    _animator.SetBool(animIsShowing, _stampShow);
                    _stateController.appDataHolder.SetNextStampIdx(activeState && resetIconInStampEvent);
                    if (_stampShow) DoUpdateIcon();
                    else
                    {
						if (__canAsyncUpdateIcon)
						{
							__canAsyncUpdateIcon = false;
							StartCoroutine(MgCoroutineHelper.WaitThenCallback(20, () =>
							{
								DoUpdateIcon();
								__canAsyncUpdateIcon = true;
							}));
						}
                    }
                }                
                break;
            case CPStampAction.Enabled:
                if (__noneInitStamp) return; //rake	
                _animator.SetBool(animIsDisable, !activeState);
                break;
        }
    }

    private void OnShowUIView(bool isShow)
    {		
		if (__noneInitStamp) return; // rake		

		if (isShow)
        {            
            if(_stateController.stampState.enabled) {               
                _stampEnabled = false;
                if(_animator!=null)_animator.SetBool(animIsShowing, false);
            }
        }
        else
        {
            if (_alwaysLandscape)
            {
                Screen.orientation = ScreenOrientation.Portrait;
                workCamera.ResetAspect();
                _appState.ResetAspect();
            }
            widgetSettings.canExecute = true;
            if (_stampEnabled == false) {
                _stampEnabled = true;                
                if(_animator!=null)_animator.SetBool(animIsShowing, true);
            }
        }
    }

    protected void OnActiveMainObjects(bool isActive)
    {
		if (__noneInitStamp) return;
		// выключаем до деактивации объектов
        if (!isActive)
		{
            if (Messenger.eventTable.ContainsKey(AppState.EventTypes.uiEnableMainScenario))
                Messenger.Broadcast<bool>(AppState.EventTypes.uiEnableMainScenario, false);
        }
        if (deactivateObjects != null) 
		{
            for (int i = 0; i < deactivateObjects.Length; i++)
				if (deactivateObjects[i] != null ) deactivateObjects[i].SetActive(isActive);
        }

        //включаем после активации объектов
        if (isActive)
		{
            if (Messenger.eventTable.ContainsKey(AppState.EventTypes.uiEnableMainScenario))
                Messenger.Broadcast<bool>(AppState.EventTypes.uiEnableMainScenario, true);
        }
    }

    void OnAfterCloseUIView()
    {
        widgetSettings.canExecute = true;
		if (_stampEnabled == false)
		{
			_stampEnabled = true;
			if(_animator!=null)_animator.SetBool(animIsShowing, true);
		}

        if (_alwaysLandscape)
		{
            Screen.orientation = ScreenOrientation.Portrait;
			StartCoroutine(MgCoroutineHelper.WaitThenCallback(0.2f, ()=>
			{
				_appState.ResetAspect();
				if (Messenger.eventTable.ContainsKey(AppState.EventTypes.uiCameraSizeToUIView))
					Messenger.Broadcast(AppState.EventTypes.uiCameraSizeToUIView);
			}));
        }
    }
    #endregion

    #region Touch action   
    protected override bool DoBreakTouch ()
	{
        bool isBreak = _appState.IsShowUIView || _appState.lockWidgets;
        if(isBreak == false)
        {  
            if (_animator != null)
                isBreak = _animator.GetBool(animIsBouncing) || !_animator.GetCurrentAnimatorStateInfo(0).IsName("shownStamp");
			if(isBreak == false) {
				if (_stateController.IsEmptyAppListForCurrentLang) isBreak = true;
			}
        }
        return isBreak;
	}    

	protected override void DoTouch()
	{
        // double check - избыточное но требует full testing - поэтому не удалять!
        if (!widgetSettings.canExecute || _appState.IsShowUIView || _appState.lockWidgets) return;
#if MAGE_ANALYTICS
				UnityEngine.Analytics.Analytics.CustomEvent("CPStamp", new Dictionary<string, object>
				{
					{"action", "touch"}
				});
#endif
		if(allwaysOpenDeveloperPage)
		{
			_appState.parentalGateSvc.CreateUIView(
				p =>
				{
					#if MAGE_ANALYTICS
					UnityEngine.Analytics.Analytics.CustomEvent(this.GetType().Name, new Dictionary<string, object>
					{
						{"action", "openStore"}
					});
					#endif

					Debug.LogWarning("dev page: " + AppSettings.instance.appInfo.GetAppsDevUri());
					Application.OpenURL(AppSettings.instance.appInfo.GetAppsDevUri());

				}
			);
			return;
		}

#if UNITY_ANDROID
		var lang = _appState.CurrentLang;
		if(!MgCommonUtils.IsFullSupportLang(lang))
		{
            if (useParentalGateForNoneSupportLang)
            {
                _appState.parentalGateSvc.CreateUIView(
                    p =>
                    {
#if MAGE_ANALYTICS
		            	UnityEngine.Analytics.Analytics.CustomEvent(this.GetType().Name, new Dictionary<string, object>
				        {
					        {"action", "openStore"}
				        });
#endif
                        Application.OpenURL(AppSettings.instance.appInfo.GetAppsDevUri());

                    }
                );
            }
            else
            {
#if MAGE_ANALYTICS
        		UnityEngine.Analytics.Analytics.CustomEvent(this.GetType().Name, new Dictionary<string, object>
				{
					{"action", "openStore"}
				});
#endif
                Application.OpenURL(AppSettings.instance.appInfo.GetAppsDevUri());
            }           
			return;
		}
#endif

        if (Messenger.eventTable.ContainsKey(AppState.EventTypes.cpBeforeCreateUIView))
			Messenger.Broadcast(AppState.EventTypes.cpBeforeCreateUIView);
#if !MAGE_NO_PM
		if (broadcastDoTouchPMEvent) 
			PlayMakerFSM.BroadcastEvent(AppState.PMEvents.cpOpenUIView);
#endif

        if (widgetSettings.parentalGate)
        {
            if(_animator != null)_animator.enabled = false;
            _appState.parentalGateSvc.CreateUIView(
                p =>
                {
                    if (_animator != null)
                    {
                        _animator.enabled = true;
                        DoCreateCPView();
                    }
                },
                r =>
                { 
					if (_animator != null) _animator.enabled = true;
                    if (Messenger.eventTable.ContainsKey(AppState.EventTypes.cpCancelCreateUIView))
                        Messenger.Broadcast(AppState.EventTypes.cpCancelCreateUIView);
                }
            );
        }
        else DoCreateCPView();        
	}

    private void DoCreateCPView()
    {
        widgetSettings.canExecute = false;

        if (_stateController.stampState.enabled)
        {
            _stampEnabled = false;
            if(_animator!=null)_animator.SetBool(animIsShowing, false);
		}

#if MAGE_ANALYTICS
		UnityEngine.Analytics.Analytics.CustomEvent("CPStamp", new Dictionary<string, object>
		{
			{"action", "open"}
		});
#endif

		MgScreenFaderController.Run(MageAppFadingMode.OutContinued, fadingOutTime);
        if (_alwaysLandscape)
            StartCoroutine(MgCoroutineHelper.WaitThenCallback(fadingOutTime + 0.05f, () =>
            {               
                Screen.orientation = ScreenOrientation.Landscape;
            }));

		StartCoroutine(RoutineAction());
    }


	IEnumerator RoutineAction()
	{
		yield return new WaitForSeconds(fadingOutTime + 0.1f);
		if (_alwaysLandscape)
		{
			_appState.ResetAspect();
			if (Messenger.eventTable.ContainsKey(AppState.EventTypes.uiCameraSizeToUIView))
				Messenger.Broadcast(AppState.EventTypes.uiCameraSizeToUIView);
			yield return null; //WARNING - NEED TEST!
			//yield return null;
		}
		if (Messenger.eventTable.ContainsKey(AppState.EventTypes.cpCreateUIView))
			Messenger.Broadcast<GameObject>(AppState.EventTypes.cpCreateUIView, parentUIView);
		yield return new WaitForSeconds(0.1f);
		MgScreenFaderController.Run(MageAppFadingMode.In, fadingInTime);
	}


    #endregion

	private bool __noneInitStamp = true;
    /// <summary>
    /// Inits the stamp.
    /// </summary>	
    private void InitStamp()
    {
		__noneInitStamp = false;
		if (leftAnchor)
        {
            _visiblePosition = workCamera.ScreenToWorldPoint(new Vector3(0, Screen.height, 0.0f));
            _visiblePosition.x = _visiblePosition.x + _currentOffset.x;
            _visiblePosition.y = _visiblePosition.y + _currentOffset.y - 2.0f * workCamera.transform.position.y;
            _visiblePosition.z = workCamera.transform.position.z + _currentOffset.z;
            _visiblePosition.Set(_visiblePosition.x + 0.5f * stamp.GetComponent<Renderer>().bounds.size.x,
                                 -_visiblePosition.y + 0.5f * stamp.GetComponent<Renderer>().bounds.size.y,
                                 _visiblePosition.z);
        }
        else
        {
            _visiblePosition = workCamera.ScreenToWorldPoint(new Vector3(Screen.width, Screen.height, 0.0f));
            _visiblePosition.x = _visiblePosition.x - _currentOffset.x;
            _visiblePosition.y = _visiblePosition.y + _currentOffset.y - 2.0f * workCamera.transform.position.y;
            _visiblePosition.z = workCamera.transform.position.z + _currentOffset.z;
            _visiblePosition.Set(_visiblePosition.x - 0.5f * stamp.GetComponent<Renderer>().bounds.size.x,
                                 -_visiblePosition.y + 0.5f * stamp.GetComponent<Renderer>().bounds.size.y,
                                 _visiblePosition.z);
        }

        _trfMove.position = _visiblePosition;
        _stampEnabled = _stateController.stampState.enabled;

        if (_animator != null)
        {
            _animator.SetBool(animIsDisable, !_stampEnabled);
            _animator.enabled = true;
        }
    }

    #region Unity event functions

    protected override void DoAwake()
    {
        base.DoAwake();

        _appState = AppState.instance;
        _stateController = CPStateController.instance;
        _alwaysLandscape = AppSettings.instance.CP.alwaysLandscape;
		// WARNING!
		_stateController.ResetUIViewState();

        _trfMove = transform.parent.gameObject.transform;
        _trfMove.position = new Vector3(8192, -8192, 0);
        _goIcon = gameObject.FindChild(@"icon");
		if(_goIcon != null) _goIconRenderer = _goIcon.GetComponent<Renderer>();
        
        if (workCamera == null) workCamera = Camera.main;
        if (parentUIView == null && forceUIContainer) parentUIView = _appState.UIContainer;

		if(stamp == null) stamp = gameObject.FindChild(@"stamp");

		// === add listners ===
        Messenger.AddListener(AppState.EventTypes.cpStampUpdate, OnStampUpdate);
        Messenger.AddListener<bool>(AppState.EventTypes.cpShowUIView, OnShowUIView);
		Messenger.AddListener(AppState.EventTypes.cpAfterCloseUIView, OnAfterCloseUIView);
        Messenger.AddListener<CPStampAction, bool>(AppState.EventTypes.cpStampAction, OnStampAction);
		Messenger.AddListener<bool>(AppState.EventTypes.uiActiveMainSceneObjects, OnActiveMainObjects);
		//подписка на изменение языка интерфейса
		
		// ===

        _animator = GetComponent<Animator>();	

		if (AppSettings.instance.CP.disable)
		{
			Destroy(this.gameObject);
		}
		else
		{
			MageAppAspectRatio asp = MgCommonUtils.GetAspectRatio(workCamera);
			if (!offset.ContainsKey(asp)) asp = MageAppAspectRatio.AspectOthers;
			_currentOffset = _offset[asp];

			StartCoroutine(MgCoroutineHelper.WaitThenCallback(waitFramesInInit, () =>
				{
					InitStamp();
					DoUpdateIcon();
					StartCoroutine(BackgroundStampUpdater());
				}));
			// MOD 2015/03/11 - NEED TEST
			if (_alwaysLandscape)
			{
				// только для широких устройст
				_alwaysLandscape = _alwaysLandscape && (_appState.aspect != MageAppAspectRatio.Aspect4by3);
				// проверяем блокировки поварота - если аутоповороты запрещены
				_alwaysLandscape = _alwaysLandscape &&
					!(Screen.autorotateToLandscapeLeft || Screen.autorotateToLandscapeRight || Screen.autorotateToPortrait);
			}
		}
    }

	protected override void DoDestroy()
	{
		if (Messenger.eventTable.ContainsKey(AppState.EventTypes.cpStampUpdate))
            Messenger.RemoveListener(AppState.EventTypes.cpStampUpdate, OnStampUpdate);
        if (Messenger.eventTable.ContainsKey(AppState.EventTypes.cpShowUIView))
            Messenger.RemoveListener<bool>(AppState.EventTypes.cpShowUIView, OnShowUIView);
		if (Messenger.eventTable.ContainsKey(AppState.EventTypes.cpAfterCloseUIView))
			Messenger.RemoveListener(AppState.EventTypes.cpAfterCloseUIView, OnAfterCloseUIView);

        if (Messenger.eventTable.ContainsKey(AppState.EventTypes.cpStampAction))
            Messenger.RemoveListener<CPStampAction, bool>(AppState.EventTypes.cpStampAction, OnStampAction);

        if (Messenger.eventTable.ContainsKey(AppState.EventTypes.uiActiveMainSceneObjects))
            Messenger.RemoveListener<bool>(AppState.EventTypes.uiActiveMainSceneObjects, OnActiveMainObjects);
	}

	#endregion
}
