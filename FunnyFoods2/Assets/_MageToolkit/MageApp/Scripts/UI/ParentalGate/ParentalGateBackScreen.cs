﻿using UnityEngine;

[DisallowMultipleComponent]
[RequireComponent(typeof (Material)) ]
public class ParentalGateBackScreen : MonoBehaviour
{
    void Awake()
    {
        var renderer = GetComponent<Renderer>();
        if (renderer != null)
        {
            if (renderer.material.HasProperty("_Color"))
                renderer.material.color = AppSettings.instance.Effect.parentalGateBackScreen.color;

        }
    }	
}
