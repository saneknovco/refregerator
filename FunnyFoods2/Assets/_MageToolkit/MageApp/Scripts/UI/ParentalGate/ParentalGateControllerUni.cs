﻿using UnityEngine;

[DisallowMultipleComponent]
public class ParentalGateControllerUni : BaseParentalGateController
{
	const int CountVariant = 3;	
	
	public GameObject question;
	public GameObject answers;
	public GameObject answersPool;

	public void SelectAnswer(string answer)
	{		
		if (_answerDone || _isDestroying) return;

		MgAudioHelper.Click();
		_answerDone = true;		
		if (answer == _answer)
		{
			//Signal: destroy  UIView: prevent lock other UIView
            // MOD 12.12!!!!
			//AppState.Instance.UIViewMode &= ~MageAppUIViewMode.ParentalGateView;
			OnParentalGateSuccess(this);	
		}
		else
		{
			OnParentalGateReject(this);
		}

		// destroy this UIView
		GameObject.Destroy(this.gameObject);
	}

	private string _answer;
	private bool _answerDone = false;
	void Awake()
	{
		Random.seed = (int)Time.time;
		
		//Signal: create UIView;
		AppState.instance.UIViewMode |= MageAppUIViewMode.ParentalGateView;
		//init		
		if (question == null) question = gameObject.FindChild("question");
		if (answers == null) answers = gameObject.FindChild("answers");
		if (answersPool == null) answersPool = gameObject.FindChild("answersPool");

		//select quetions
		int questionIndex = UnityEngine.Random.Range(0, CountVariant);
		GameObject go = question.transform.GetChild(questionIndex).gameObject;
		go.GetComponent<Renderer>().enabled = true;
		_answer = go.name.Substring(go.name.Length - 1, 1);

        Vector3 goPos = new Vector3(0, 0, -0.1f);
		// renderer answers
		for (int i =0; i< CountVariant; i++)
		{
			go = answers.transform.GetChild(i).gameObject;
			go = answersPool.transform.GetChild(Random.Range(0, CountVariant - i)).gameObject;
			go.transform.parent = answers.transform.GetChild(i);
			go.transform.localPosition = goPos;            
			go.transform.localScale = Vector3.one;
			go.GetComponent<Renderer>().enabled = true;
		}

        TouchKit.enabledGestureRecognizers(false);
    }

	void OnDestroy()
	{
		//Signal: destroy UIView
		AppState.instance.UIViewMode &= ~MageAppUIViewMode.ParentalGateView;
        TouchKit.enabledGestureRecognizers(true);
#if !MAGE_NO_PM		
		PlayMakerFSM.BroadcastEvent(AppState.PMEvents.pgCloseUIView);
#endif
    }
}
