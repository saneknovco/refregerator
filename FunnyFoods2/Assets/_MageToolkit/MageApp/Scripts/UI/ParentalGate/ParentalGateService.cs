﻿using UnityEngine;
using System;

public class ParentalGateService : IParentalGateService
{
	private const string UriParentalPrefab = @"UI/ParentalGateUIView";	

	private string safeUri
	{
		get
		{			
			string uriPrefab = UriParentalPrefab;
			if (Screen.width > Screen.height) return uriPrefab;
			else return uriPrefab + AppConfig.sufixToPortrait;
		}
	}

	private string uri
	{
		get
		{
			var lang = AppState.instance.CurrentLang;
			string uriPrefab = UriParentalPrefab;

			if (!(lang == MageAppUILang.en || lang == MageAppUILang.ru))
				uriPrefab = uriPrefab + AppConfig.sufixToUniAssets;
			if (Screen.width > Screen.height) return uriPrefab;
			else return uriPrefab + AppConfig.sufixToPortrait;
		}
	}

	private GameObject _uiView = null;

	public bool CreateUIView(Action<MonoBehaviour> parentalGateSuccess)
	{
        if (AppState.instance.IsShowParentalGate) return false;
        var cam = Camera.main;
       	_uiView = Resources.Load<GameObject>(uri);
		if (_uiView == null) _uiView = Resources.Load<GameObject>(safeUri);       
        if(_uiView != null)		
		{
			_uiView = GameObject.Instantiate(_uiView) as GameObject;
			//ставим сразу перед  камерой для блокировки других объектов
            _uiView.transform.position = cam.transform.position + Vector3.forward;		
            _uiView.transform.localScale = MgCommonUtils.ScaleUnitToPoint(cam) * _uiView.transform.localScale;
			_uiView.GetComponent<BaseParentalGateController>().parentalGateSuccessEvent += parentalGateSuccess;			
		}

		bool result = _uiView != null;
		if (Messenger.eventTable.ContainsKey(AppState.EventTypes.pgShowUIView))
			Messenger.Broadcast<bool>(AppState.EventTypes.pgShowUIView, result);

#if !MAGE_NO_PM
		if (result) //&& (broadcastDoTouchPMEvent) 
			PlayMakerFSM.BroadcastEvent(AppState.PMEvents.pgOpenUIView);
#endif
		return result;
	}

	public bool CreateUIView(Action<MonoBehaviour> parentalGateSuccess, Vector3 positionUIView)
	{
        if (AppState.instance.IsShowParentalGate) return false;
        _uiView = Resources.Load<GameObject>(uri);
		if (_uiView == null) _uiView = Resources.Load<GameObject>(safeUri);        
		if (_uiView != null)
		{
			_uiView = GameObject.Instantiate(_uiView) as GameObject;
			// === Hook - for very smart  baby
            //Signal: create UIView 
            //AppState.Instance.UIViewMode |= MageAppUIViewMode.ParentalGateView;
            // === Hook - for very smart  baby --<
            _uiView.transform.position = positionUIView;
			_uiView.GetComponent<BaseParentalGateController>().parentalGateSuccessEvent += parentalGateSuccess;			
		}

		bool result = _uiView != null;
		if (Messenger.eventTable.ContainsKey(AppState.EventTypes.pgShowUIView))
			Messenger.Broadcast<bool>(AppState.EventTypes.pgShowUIView, result);

#if !MAGE_NO_PM
		if (result)
			PlayMakerFSM.BroadcastEvent(AppState.PMEvents.pgOpenUIView);
#endif
		return result;
	}

	public bool CreateUIView(Action<MonoBehaviour> parentalGateSuccess, Vector3 positionUIView, Vector3 localScaleUIView)
	{
        if (AppState.instance.IsShowParentalGate) return false;
        _uiView = Resources.Load<GameObject>(uri);
		if (_uiView == null) _uiView = Resources.Load<GameObject>(safeUri);
        if (_uiView != null)
		{
			_uiView = GameObject.Instantiate(_uiView) as GameObject;
			_uiView.transform.position = positionUIView;
			_uiView.transform.localScale = localScaleUIView;
			_uiView.GetComponent<BaseParentalGateController>().parentalGateSuccessEvent += parentalGateSuccess;			
		}

		bool result = _uiView != null;
		if (Messenger.eventTable.ContainsKey(AppState.EventTypes.pgShowUIView))
			Messenger.Broadcast<bool>(AppState.EventTypes.pgShowUIView, result);

#if !MAGE_NO_PM
		if (result)
			PlayMakerFSM.BroadcastEvent(AppState.PMEvents.pgOpenUIView);
#endif
		return result;
	}


	public bool CreateUIView(Action<MonoBehaviour> parentalGateSuccess, Action<MonoBehaviour> parentalGateReject)
	{
        if (AppState.instance.IsShowParentalGate) return false;
        var cam = Camera.main;
        _uiView = Resources.Load<GameObject>(uri);
		if (_uiView == null) _uiView = Resources.Load<GameObject>(safeUri);
		if (_uiView != null)
		{
			_uiView = GameObject.Instantiate(_uiView) as GameObject;
			//ставим сразу перед камерой для блокировки других объектов
			_uiView.transform.position = cam.transform.position + Vector3.forward;
            _uiView.transform.localScale = MgCommonUtils.ScaleUnitToPoint(cam) * _uiView.transform.localScale;
			_uiView.GetComponent<BaseParentalGateController>().parentalGateSuccessEvent += parentalGateSuccess;
			if (parentalGateReject != null)
			_uiView.GetComponent<BaseParentalGateController>().parentalGateRejectEvent += parentalGateReject;			
		}
		bool result = _uiView != null;
		if (Messenger.eventTable.ContainsKey(AppState.EventTypes.pgShowUIView))
			Messenger.Broadcast<bool>(AppState.EventTypes.pgShowUIView, result);
#if !MAGE_NO_PM
		if (result)
			PlayMakerFSM.BroadcastEvent(AppState.PMEvents.pgOpenUIView);
#endif
		return result;
	}

	public bool CreateUIView(Action<MonoBehaviour> parentalGateSuccess, Action<MonoBehaviour> parentalGateReject,
		Vector3 positionUIView, Vector3 localScaleUIView)
	{
        if (AppState.instance.IsShowParentalGate) return false;
        _uiView = Resources.Load<GameObject>(uri);
		if (_uiView == null) _uiView = Resources.Load<GameObject>(safeUri);
        if (_uiView != null)		
		{
			_uiView = GameObject.Instantiate(_uiView) as GameObject;
			_uiView.transform.position = positionUIView;
			_uiView.transform.localScale = localScaleUIView;
			_uiView.GetComponent<BaseParentalGateController>().parentalGateSuccessEvent += parentalGateSuccess;
			if (parentalGateReject != null)
			_uiView.GetComponent<BaseParentalGateController>().parentalGateRejectEvent += parentalGateReject;
		}

		bool result = _uiView != null;
		if (Messenger.eventTable.ContainsKey(AppState.EventTypes.pgShowUIView))
			Messenger.Broadcast<bool>(AppState.EventTypes.pgShowUIView, result);
#if !MAGE_NO_PM
		if (result)
			PlayMakerFSM.BroadcastEvent(AppState.PMEvents.pgOpenUIView);
#endif
		return result;
	}

    public bool CreateUIView2(Action<MonoBehaviour> parentalGateSuccess, GameObject parentUIView = null, Action<MonoBehaviour> parentalGateReject = null)
    {
        if (AppState.instance.IsShowParentalGate) return false;
        if (parentUIView == null) return CreateUIView(parentalGateSuccess, parentalGateReject);

        _uiView = Resources.Load<GameObject>(uri);
		if (_uiView == null) _uiView = Resources.Load<GameObject>(safeUri);
        if (_uiView != null)        
        {
			_uiView = GameObject.Instantiate(_uiView) as GameObject;
			_uiView.transform.parent = parentUIView.transform;
            //сдвигаем на -1 по Z относительно парента!
            _uiView.transform.position = Vector3.back;            
            _uiView.transform.localScale = Vector3.one;
            _uiView.GetComponent<BaseParentalGateController>().parentalGateSuccessEvent += parentalGateSuccess;
            if (parentalGateReject != null)
                _uiView.GetComponent<BaseParentalGateController>().parentalGateRejectEvent += parentalGateReject;			
        }
		bool result = _uiView != null;
		if (Messenger.eventTable.ContainsKey(AppState.EventTypes.pgShowUIView))
			Messenger.Broadcast<bool>(AppState.EventTypes.pgShowUIView, result);
#if !MAGE_NO_PM
		if ( result )
			PlayMakerFSM.BroadcastEvent(AppState.PMEvents.pgOpenUIView);
#endif
		return result;
    }

	public bool CloseUIView()
	{
		if(_uiView == null) return false;
		var cmp = _uiView.GetComponent<BaseParentalGateController>();
		if(cmp == null) return false;
		cmp.RejectParentalGate();
		return true;
	}
}
