﻿using UnityEngine;

public class ParentalGateSelectAnswer : BaseSelectedButtonWidget
{
	public GameObject parentalGateController;
	private ParentalGateControllerUni _controller;
    private string _answer = string.Empty;
  
    protected override void DoAction()
    {
        if (_controller != null)
            _controller.SelectAnswer(_answer);
    }

    void TouchAction()
    {
        MgAudioHelper.Click();
        DoAction();
    }   

    void Start()
    {
        if (parentalGateController != null)
            _controller = parentalGateController.GetComponent<ParentalGateControllerUni>();
        if (gameObject.transform.childCount > 0)
        {
            var child = gameObject.transform.GetChild(0);
            if (child != null)
            {
                if (renderObject == null)
                    renderObject = child.GetComponent<Renderer>();
                _answer = child.gameObject.name;
            }
        }
    }
}
