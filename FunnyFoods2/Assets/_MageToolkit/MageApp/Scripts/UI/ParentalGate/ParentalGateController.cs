﻿using UnityEngine;
using System.Collections;

/// <summary>
/// Контролер для работы с ParentalGateUIView
/// </summary>
/// History:
/// 2015.04.05: new method BindingRejectEvent - call into Start (early in Awake)
[DisallowMultipleComponent]
public class ParentalGateController : BaseParentalGateController
{
    public Camera workCamera = null;    
    public BoxCollider closeBtnBound;

    private bool _isMoving = false;
	private float _startPosY;
	private int _magnitudeMoving; //- величина перемешения пальцев
	private TKPanRecognizer _recognizer = null;
	private TKTapRecognizer _recognizerTap = null;   

    private bool __multiTouchState = false; 
	void Awake()
	{
		_magnitudeMoving = Screen.height / 30;		
		if (_magnitudeMoving < 16) _magnitudeMoving = 16;
		else if (_magnitudeMoving > 32) _magnitudeMoving = 32;

		//Signal: create UIView;
		AppState.instance.UIViewMode |= MageAppUIViewMode.ParentalGateView;
        if (workCamera == null) workCamera = Camera.main;
        if (closeBtnBound == null) closeBtnBound = GetComponent<BoxCollider>();
        StartCoroutine(AsyncInit());
	}

	private IEnumerator AsyncInit()
	{
        yield return null;

		#region TouchKit config

		__multiTouchState = Input.multiTouchEnabled;
		Input.multiTouchEnabled = true;
		_recognizer = new TKPanRecognizer();
#if (UNITY_IOS || UNITY_ANDROID) && !UNITY_EDITOR
		//Two fingers:
		_recognizer.minimumNumberOfTouches = 2;
#endif
		_recognizer.gestureRecognizedEvent += (r) =>
		{
			if (_isMoving) return;
			_isMoving = true;
			_startPosY = Input.mousePosition.y;			
		};

		_recognizer.gestureCompleteEvent += (r) =>
		{
			if (_isDestroying) return;
			if (_isMoving)
			{
				_isMoving = false;
				if (((_startPosY - Input.mousePosition.y) > _magnitudeMoving))
				{
                    DoAction(true);
					return;
				}				
			}
			_startPosY = 0.0f;
		};

        _recognizerTap = new TKTapRecognizer(0.7f, 6f);	//30 (div 5 into new version)
		_recognizerTap.boundaryFrame = new TKRect(0, 0, Screen.width, Screen.height);
		_recognizerTap.gestureRecognizedEvent += (r) =>
		{
			if (_isDestroying) return;

            Vector2 v = r.touchLocation();
           if(closeBtnBound.bounds.IntersectRay(workCamera.ScreenPointToRay(new Vector3(v.x, v.y))))
            {
                if (r.state == TKGestureRecognizerState.Recognized) DoAction(false);
                return;
            } 
#if !UNITY_EDITOR
            if (Input.touchCount != 1)
            {
            	r.state = TKGestureRecognizerState.FailedOrEnded;
            	return;
            }
#endif
           if (r.state == TKGestureRecognizerState.Recognized) DoAction(false);      
        };

        // Disable all others recogonizers
        TouchKit.enabledGestureRecognizers(false);
        //add recognizer
        TouchKit.addGestureRecognizer(_recognizer);
		TouchKit.addGestureRecognizer(_recognizerTap);        
		//FIX:  temp stub for cradle & etc.
		_recognizer.enabled = true;
		_recognizerTap.enabled = true;        
		#endregion		        
	}
	
	void OnDestroy()
	{
		//Signal: destroy UIView
		AppState.instance.UIViewMode &= ~MageAppUIViewMode.ParentalGateView;

        //TouchKit: освобождение подписчика 
        if (_recognizerTap != null)
		{
			TouchKit.removeGestureRecognizer(_recognizerTap);
			_recognizerTap = null;
		}
		if (_recognizer != null)
		{
			TouchKit.removeGestureRecognizer(_recognizer);
			_recognizer = null;
		}		
		//restore multiTouch state
		Input.multiTouchEnabled = __multiTouchState;
		//enable all others recogonizers
		TouchKit.enabledGestureRecognizers(true);
#if !MAGE_NO_PM		
		PlayMakerFSM.BroadcastEvent(AppState.PMEvents.pgCloseUIView);
#endif
	}
}
