﻿using UnityEngine;
using System;

public interface IParentalGateService
{
	bool CreateUIView(Action<MonoBehaviour> parentalGateSuccess);	
	bool CreateUIView(Action<MonoBehaviour> parentalGateSuccess, Vector3 positionUIView);
	bool CreateUIView(Action<MonoBehaviour> parentalGateSuccess, Vector3 positionUIView, Vector3 localScaleUIView);

	#region ver 1.1

	/// <summary>
	/// 
	/// </summary>
	/// <param name="parentalGateSuccess"></param>
	/// <param name="parentalGateReject"></param>
	/// <returns></returns>
	bool CreateUIView(Action<MonoBehaviour> parentalGateSuccess, Action<MonoBehaviour> parentalGateReject);	
	/// <summary>
	/// 
	/// </summary>
	/// <param name="parentalGateSuccess"></param>
	/// <param name="parentalGateReject"></param>
	/// <param name="positionUIView"></param>
	/// <param name="localScaleUIView"></param>
	/// <returns></returns>
	bool CreateUIView(Action<MonoBehaviour> parentalGateSuccess, Action<MonoBehaviour> parentalGateReject,
			Vector3 positionUIView, Vector3 localScaleUIView);

	#endregion

    #region ver 1.2
    bool CreateUIView2(Action<MonoBehaviour> parentalGateSuccess, GameObject parentUIView = null, Action<MonoBehaviour> parentalGateReject = null);	
	bool CloseUIView();	
    #endregion
}

