﻿using UnityEngine;
using System.Collections;

public class BaseParentalGateController : MonoBehaviour
{
	/// <summary>
	/// Это событие позволяет выполнить код, который  попадает под
	/// Parental Gate контроль
	/// </summary>
	public event System.Action<MonoBehaviour> parentalGateSuccessEvent;
	/// <summary>
	/// Это событие позволяет выполнить код, который  попадает под  Parental Gate контроль
	/// когда пользователь не прошел контроль
	/// </summary>
	public event System.Action<MonoBehaviour> parentalGateRejectEvent;
    
	protected CloseUIView _closeUIView;
	protected bool _isDestroying = false;

	public bool IsDestroying
    {
		get { return _isDestroying;	}
	}

    public void DoAction (bool passParentalGate)
    {
        AppState.instance.UIViewMode &= ~MageAppUIViewMode.ParentalGateView;        
        if (passParentalGate)
        {
            OnParentalGateSuccess(this);
        }
        else
        {
            MgAudioHelper.Click();
            OnParentalGateReject(this);
        }
        _isDestroying = true;
        GameObject.Destroy(this.gameObject);
    }

	public void RejectParentalGate()
	{
		if(_isDestroying) return;

		AppState.instance.UIViewMode &= ~MageAppUIViewMode.ParentalGateView;
		OnParentalGateReject(this);
		_isDestroying = true;
		MgAudioHelper.Click();
		GameObject.Destroy(this.gameObject);		
	}	
	
	protected void OnParentalGateSuccess(MonoBehaviour sender)
	{
		if (parentalGateSuccessEvent != null)
			parentalGateSuccessEvent(sender);
		if(Messenger.eventTable.ContainsKey(AppState.EventTypes.pgSuccess))
			Messenger.Broadcast(AppState.EventTypes.pgSuccess);
#if !MAGE_NO_PM		
	    PlayMakerFSM.BroadcastEvent(AppState.PMEvents.pgSuccess);
#endif
	}

	protected void OnParentalGateReject(MonoBehaviour sender)
	{
		if (parentalGateRejectEvent != null)
            parentalGateRejectEvent(this);		
		if(Messenger.eventTable.ContainsKey(AppState.EventTypes.pgReject))
			Messenger.Broadcast(AppState.EventTypes.pgReject);
#if !MAGE_NO_PM		
		PlayMakerFSM.BroadcastEvent(AppState.PMEvents.pgReject);
#endif
	}
    
#if !UNITY_IOS
	void Update()
	{
		if (Input.GetKeyUp(KeyCode.Escape))
		{
			Input.ResetInputAxes();
			RejectParentalGate();
		}
	}
#endif

}
