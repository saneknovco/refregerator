﻿using UnityEngine;
using System.Collections;

[DisallowMultipleComponent]
public class GFVCloseView : ButtonWidget
{
    [FullInspector.InspectorCategory("Action")]
    [FullInspector.InspectorHeader("Закрытие Get Full Version UIView:")]
    [FullInspector.InspectorMargin(16)]
    public bool callUnloadAssets = false;

	protected override bool DoBreakTouch()
	{
		var result =  base.DoBreakTouch();
		if (!result)
			result = GFVController.instance.isClosingView;
		return result;
	}
    protected override void DoTouch()
    {	
		if (GFVController.instance.isClosingView) return;
		if (Messenger.eventTable.ContainsKey(AppState.EventTypes.gfvCloseUIView))
			Messenger.Broadcast(AppState.EventTypes.gfvCloseUIView);
		
    }

	protected override void DoDestroy()
    {
        if (callUnloadAssets) Resources.UnloadUnusedAssets();
	}

#if UNITY_ANDROID
	void Update()
	{
		if (Input.GetKeyUp(KeyCode.Escape))
		{
			if (!AppState.instance.IsShowParentalGate)
			{
				Input.ResetInputAxes();
				Touch();
			}
		}
	}
#endif
}
