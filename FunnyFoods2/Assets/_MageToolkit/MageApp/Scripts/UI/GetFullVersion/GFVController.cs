﻿using UnityEngine;
using System.Collections;

/// <summary>
/// Контролер вызова UIView полной версии приложения.
/// Активен если режим InApp отключен
/// </summary>
public class GFVController : GlobalSingletonBehaviour<GFVController>
{
    private GameObject _gfvUIViewInstance = null;    
	private MageAppUIViewMode _gfvUIViewMode = 0;
  
    private bool CreateUIView(GameObject parentUIView)
    {        
		if (_gfvUIViewInstance != null || (int)_gfvUIViewMode != 0) return false;

        var dic = AppSettings.instance.IAP.GetFullVersionUriPrefabs;
        string uri = dic.SafeGetByDefKey(AppState.instance.CurrentLang, AppSettings.instance.UI.defaultLang);     
        var go = Resources.Load(uri) as GameObject;
        if (go == null) return false;

        _gfvUIViewInstance = GameObject.Instantiate(go) as GameObject;
        _gfvUIViewMode = AppSettings.instance.IAP.GetFullVersionViewMode;

        AppState.instance.UIViewMode |= _gfvUIViewMode;
        _gfvUIViewInstance.name = go.name;
        if (parentUIView)
        {
            _gfvUIViewInstance.transform.parent = parentUIView.transform;
            _gfvUIViewInstance.transform.localPosition = go.transform.localPosition;
            _gfvUIViewInstance.transform.localScale = go.transform.localScale;
        }
        return true;
    }

    private void OnCreateUIView(GameObject parentUIView)
    {
        bool isShow = CreateUIView(parentUIView);
        if (Messenger.eventTable.ContainsKey(AppState.EventTypes.gfvShowUIView))
            Messenger.Broadcast<bool>(AppState.EventTypes.gfvShowUIView, isShow);        
    }

    private bool __isclosing = false;
	public bool isClosingView
	{
		get { return __isclosing; }
	}

    private void OnCloseUIView()
    {
        if (__isclosing || _gfvUIViewInstance == null) return;// Warning !!!
        __isclosing = true;
		var settings = AppSettings.instance.IAP;

#if !MAGE_NO_PM
		if (settings.broadcastPMEvents)
			PlayMakerFSM.BroadcastEvent(AppState.PMEvents.gfvBeforeCloseUIView);
#endif
		StartCoroutine(MgCoroutineHelper.WaitThenCallback(settings.timeWaitBeforeClose, () =>
		{
			if (RealaseUIView())
			{
				if (Messenger.eventTable.ContainsKey(AppState.EventTypes.gfvAfterCloseUIView))
					Messenger.Broadcast(AppState.EventTypes.gfvAfterCloseUIView);
#if !MAGE_NO_PM
				if (settings.broadcastPMEvents)
					PlayMakerFSM.BroadcastEvent(AppState.PMEvents.gfvCloseUIView);
#endif
			}
			__isclosing = false;
		}));     
   
    }

	public bool RealaseUIView()
    {
        AppState.instance.UIViewMode &= ~_gfvUIViewMode;

		var result = _gfvUIViewMode == 0 ? false : true;
		_gfvUIViewMode = 0;
        if (_gfvUIViewInstance != null)
        {
            GameObject.Destroy(_gfvUIViewInstance);
            _gfvUIViewInstance = null;
        }
		return result;
    }

    protected override void DoAwake()
    { 
        Messenger.AddListener<GameObject>(AppState.EventTypes.gfvCreateUIView, OnCreateUIView);       
        Messenger.AddListener(AppState.EventTypes.gfvCloseUIView, OnCloseUIView);        
    }

    protected override void DoDestroy()
    {      
        if (Messenger.eventTable.ContainsKey(AppState.EventTypes.gfvCreateUIView))
            Messenger.RemoveListener<GameObject>(AppState.EventTypes.gfvCreateUIView, OnCreateUIView);
        if (Messenger.eventTable.ContainsKey(AppState.EventTypes.gfvCloseUIView))
            Messenger.RemoveListener(AppState.EventTypes.gfvCloseUIView, OnCloseUIView);       
    }    
}
