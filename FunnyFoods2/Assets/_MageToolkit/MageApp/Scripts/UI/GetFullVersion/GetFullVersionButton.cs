﻿using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// Переход в магазин приложений для покупки полной версии
/// </summary>
[DisallowMultipleComponent]
public class GetFullVersionButton: ButtonWidget
{
	[FullInspector.InspectorCategory("Action")]
	[FullInspector.InspectorHeader("Переход в стор на полную версию приложения")]
	[FullInspector.InspectorMargin(16)]
	public bool checkShowUIView = false;
	//Mod 07.09
	[FullInspector.InspectorCategory("Action")]//, FullInspector.InspectorShowIf("isNoAndroid")]
    public bool checkFullVersion = true;
	[FullInspector.InspectorCategory("Action")]
	[FullInspector.InspectorTooltip("Если false то позволяет блокировать обработку Touch через AppState.Instance.lockWidgets")]
    public bool ignoreLockWidget = false;

	[FullInspector.InspectorCategory("Action"), FullInspector.InspectorShowIf("usingPlayMaker")]
	[FullInspector.InspectorTooltip("Отпралять сообщение в PM: " + AppState.PMEvents.gfvGetButtonDoTouch)]
	public bool broadcastDoTouchPMEvent = false;
	

    /// <summary>
    /// проверка в лайт версии установлена ли на устройстве полная версия
    /// </summary>
    /// <returns></returns>
    private bool StartFVT()
    {        
        var settings = AppSettings.instance;
		if ((settings.appInfo.activeAppType == MageAppType.Lite)
		    && SysUtilsProxy.AppIsInstalled(settings.appInfo.GetFullVersionAppUrl()))
        {
            if (Messenger.eventTable.ContainsKey(AppState.EventTypes.fvtAction))
                Messenger.Broadcast<FVTAction>(AppState.EventTypes.fvtAction, FVTAction.CreateUIView);
            return true;
        }        
        return false;
    }

    protected override bool DoBreakTouch()
    {
        var state = AppState.instance;
        bool breakTouch = !widgetSettings.canExecute || (checkShowUIView && state.IsShowUIView) || 
            (state.lockWidgets &&!ignoreLockWidget);        
        if (breakTouch) return true;		
		if ( checkFullVersion ) return StartFVT();
        return false;
    }   

    protected override void DoTouch()
    {
        //print(this.GetType().Name);
#if MAGE_ANALYTICS
        UnityEngine.Analytics.Analytics.CustomEvent(this.GetType().Name, new Dictionary<string, object>
            {
                {"action", "touch"}
            });
#endif
		if (widgetSettings.parentalGate)
        {
            AppState.instance.parentalGateSvc.CreateUIView(p =>
            {

#if MAGE_ANALYTICS
				UnityEngine.Analytics.Analytics.CustomEvent(this.GetType().Name, new Dictionary<string, object>
                    {
                        {"action", "open"}
                    });
#endif
				MgCommonUtils.OpenUriWithAudit (this, AppSettings.instance.appInfo.GetFullVersionUri ());				


                if (Messenger.eventTable.ContainsKey(AppState.EventTypes.gfvCloseUIView))
                    Messenger.Broadcast(AppState.EventTypes.gfvCloseUIView);
#if !MAGE_NO_PM
				if( broadcastDoTouchPMEvent ) PlayMakerFSM.BroadcastEvent(AppState.PMEvents.gfvGetButtonDoTouch);
#endif
			});
        }
        else
        {

#if MAGE_ANALYTICS
			UnityEngine.Analytics.Analytics.CustomEvent(this.GetType().Name, new Dictionary<string, object>
                {
                    {"action", "open"}
                });
#endif
			MgCommonUtils.OpenUriWithAudit (this, AppSettings.instance.appInfo.GetFullVersionUri ());            

            if (Messenger.eventTable.ContainsKey(AppState.EventTypes.gfvCloseUIView))
                Messenger.Broadcast(AppState.EventTypes.gfvCloseUIView);
#if !MAGE_NO_PM
			if( broadcastDoTouchPMEvent ) PlayMakerFSM.BroadcastEvent(AppState.PMEvents.gfvGetButtonDoTouch);
#endif
		}
    }

}
