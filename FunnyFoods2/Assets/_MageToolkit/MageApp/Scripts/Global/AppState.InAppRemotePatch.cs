﻿using UnityEngine;

public partial class AppState
{
    #region HotFix: In App purchases Patch
    [System.NonSerialized]
    private int _checkDUID = -1;
    public int checkDUID
    {
        get
        {
            if (_checkDUID < 0)
            {
                _checkDUID = PlayerPrefs.GetInt(@"mg_CheckDUID", 1);
            }
            return _checkDUID;
        }
        set
        {
            _checkDUID = value;
            PlayerPrefs.SetInt(@"mg_CheckDUID", _checkDUID);
            PlayerPrefs.Save();
        }
    }

    [System.NonSerialized]
    private int _inAppPatch = -1;
    public int inAppPatch
    {
        get
        {
            if (_inAppPatch < 0) ReadInAppPatch();
            return _inAppPatch;
        }
    }
    public void ApplyInAppPatch(int patch = 1)
    {
        _inAppPatch = patch;
        PlayerPrefs.SetInt(@"mg_InAppPatch", _inAppPatch);
        PlayerPrefs.Save();
    }
    public void ReadInAppPatch()
    {
        _inAppPatch = PlayerPrefs.GetInt(@"mg_InAppPatch", 0);
    }

    //сбросить патч - для тестирования
    public void ResetInAppPatch()
    {
        ApplyInAppPatch(0);
        checkDUID = 1;
    }

    #endregion
}