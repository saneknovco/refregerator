﻿
#region Enums

[System.Serializable]
public enum MageAppBrand
{
    MAGE = 1,
    BINI = 2,
    WOOOW = 3,
    PUBLISHER = 4	
}

[System.Serializable]
public enum MageAppStore
{
    Default = 1,
    Apple = 2,
    Google = 3,
    Microsoft = 4,
    Amazon = 5,
    Yandex = 6,
    SamsungGalaxy = 7
};

/// <summary>
/// Тип приложения
/// </summary>
[System.Serializable]
public enum MageAppType
{
    Freemium = 0,
    Full = 1,
    Lite = 2
};

/// <summary>
/// Stores for Android app
/// </summary>
[System.Serializable]
public enum MageAppAndroidStore
{
    GooglePlayMarket = 0,
    AmazonAppstore = 1,
    YandexStore = 2,
    SamsungGalaxyApps = 3
};

/// <summary>
/// категория приложения, для более гибкой настройки Cross Promotion & etc.
/// </summary>
[System.Serializable]
public enum MageAppCategory
{
    None = 0,
    Books = 1,
    Education = 2,
    Games = 3,
    Music = 4,
    Kids = 5
};

/// <summary>
/// языки интерфейса
/// </summary>
[System.Serializable]
public enum MageAppUILang
{
    none = 0, //none	
    en = 1,
    ru = 2,
    jp = 3,
    fr = 4,
    sp = 5,
    it = 6,
    pt = 7,
    de = 8,
    cn = 9,
    kr = 10,
    tr = 11
};

/// <summary>
/// аспект устройства
/// </summary>
[System.Serializable]
public enum MageAppAspectRatio
{
    /// <summary>
    /// аспект не задан или не определён, часто используется как значение по умолчанию
    /// </summary>
    AspectOthers = -2,
    /// <summary>
    /// Rare android device
    /// </summary>
    Aspect5by4 = -1,
    /// <summary>
    /// iPad Default - all designs made to this is aspect
    /// </summary>
    Aspect4by3 = 0,//
    /// <summary>
    /// iPhone 4/4s
    /// </summary>
    Aspect3by2 = 1,
    /// <summary>
    /// just android devices
    /// </summary>
    Aspect16by10 = 2,
    /// <summary>
    /// iPhone 5+
    /// </summary>
    Aspect16by9 = 3
};

/// <summary>
/// режимы отображения UIView в приложении
/// </summary>
[System.Flags]
[System.Serializable]
public enum MageAppUIViewMode
{
    //None = 0,	// BUG IN UNITY INSPECTOR!!!
    /// <summary>
    /// для диалога info
    /// </summary>
    InfoView = 1 << 0,
    /// <summary>
    /// view help
    /// </summary>
    HelpView = 1 << 1,
    /// <summary>
    /// view настроек(settings)
    /// </summary>
    SettingsView = 1 << 2,
    /// <summary>
    /// View Cross-promotion
    /// </summary>
    CrossPromotionView = 1 << 3,
    /// <summary>
    /// Родитеьский контроль
    /// </summary>
    ParentalGateView = 1 << 4,
    /// <summary>
    /// диалог покупки полной версчии (переход в стор)
    /// </summary>
    BuyFullVersionView = 1 << 5,

    /// <summary>
    /// View для Fading эффекта
    /// </summary>
    FadingView = 1 << 6,

    /// <summary>
    /// view отзыв 
    /// </summary>
    ReviewView = 1 << 7,

    /// <summary>
    /// Info with InApp (exotic: for support old projects)
    /// </summary>
    InfoWithInAppView = 1 << 8,

    /// <summary>
    /// View for IAP
    /// </summary>
    InAppView = 1 << 9,
    /// <summary>
    /// View for IAP: InApp по акции (скидки и т.п.)
    /// </summary>
    InAppActionView = 1 << 10,

    /// <summary>
    /// для mainSceneObject, etc.
    /// </summary>
    SceneObjectView = 1 << 11,

    /// <summary>
    /// View, которое создается через PlayMaker для общего назначения.
    /// </summary>
    PlayMakerView = 1 << 12,

    /// <summary>
    /// пустышка для мульти view и т.п.
    /// </summary>
    DummyView = 1 << 13,

    /// <summary>
    /// для диалога AppLinks связаных приложений
    /// </summary>
    AppLinksView = 1 << 14
};

/// <summary>
/// типы событий для UIView
/// </summary>
[System.Serializable]
public enum MageAppUIViewEvent
{
    None = 0x0,
    /// <summary>
    /// событие для отслеживания переключение языка интерфейса
    /// </summary>
    LangUI = 0x1,
    /// <summary>
    /// Сообщение "создание  UIView" ( before parental gate)
    /// </summary>
    BeforeCreateUIView = 0x5,
    /// <summary>
    /// Сообщение "отмена создания  UIView" (reject parental gate event)
    /// </summary>
    CancelCreateUIView = 0x6,
    /// <summary>
    /// сигнал на создание UIView (происходит после прохождения parental gate)
    /// </summary>
    CreateUIView = 0x2,
    /// <summary>
    /// Сообщение "Открытие UIView": для контроля успешности загрузки UIView из prefab и т.п.
    /// </summary>
    ShowUIView = 0x3,
    /// <summary>
    /// закрытие UIView отображающегося в одном из режимов MageAppUIViewMode.
    /// <para>Происходит по нажатию кнопки Close и т.п.</para>
    /// </summary>
    CloseUIView = 0x4,
    ///<summary>
    /// закрытие UIView: происходит после очистки ресурсов, fading еффектоф и т.п.
    ///</summary>
    AfterCloseUIView = 0x7
};
#endregion

