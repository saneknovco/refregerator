﻿
public partial class AppState
{
    #region Nested Types: List of Name for Event Types(Messenger).
    
    /// <summary>
    /// Список имен событий MageApp для Messenger.
    /// </summary>
    public static class EventTypes
    {
        #region SYS
        /// <summary>
        /// Перезапуск приложения 
        /// </summary>
        public static readonly string sysRestartApp = @"mg_SysRestartApp";

        #endregion

        #region common UI
        /// <summary>
        /// событие изменения состояний UIView (для отслеживания состояния переменной uiViewMode )
        /// </summary>
        public static readonly string UIViewMode = typeof(MageAppUIViewMode).Name;

        /// <summary>
        /// событие переключение языка интерфейса (для отслеживание состояния языка UI в приложение)
        /// </summary>
        public static readonly string langUI = System.Enum.GetName(typeof(MageAppUIViewEvent), MageAppUIViewEvent.LangUI);

        #region Event for generic UIView

        /// <summary>
        /// Сообщение "создание  UIView" ( before parental gate)
        /// <para> call format: </para>
        /// <para>Messenger.Broadcast&lt;MageAppUIViewMode&gt;(AppState.EventTypes.beforeCreateUIView, UIViewMode)</para>
        /// </summary>
        public static readonly string beforeCreateUIView = System.Enum.GetName(typeof(MageAppUIViewEvent), MageAppUIViewEvent.BeforeCreateUIView);

        /// <summary>
        /// Сообщение "отмена создания UIView" (reject parental gate event)
        /// <para> call format: </para>
        /// <para>Messenger.Broadcast&lt;MageAppUIViewMode&gt;(AppState.EventTypes.cancelCreateUIView, UIViewMode)</para>
        /// </summary>
        public static readonly string cancelCreateUIView = System.Enum.GetName(typeof(MageAppUIViewEvent), MageAppUIViewEvent.CancelCreateUIView);

        /// <summary>
        /// сообщение создания UIView
        /// </summary>
        public static readonly string createUIView = System.Enum.GetName(typeof(MageAppUIViewEvent), MageAppUIViewEvent.CreateUIView);

        /// <summary>
        /// Сообщение "Открытие UIView": для контроля успешности загрузки UIView из prefab и т.п.
        /// <para>Call Format: </para>
        /// Messenger.Broadcast&lt;MageAppUIViewMode, bool&gt;((AppState.EventTypes.showUIView, UIViewMode, isShow);
        /// isShow == true when Create UIView is success
        /// </summary>
        public static readonly string showUIView = System.Enum.GetName(typeof(MageAppUIViewEvent), MageAppUIViewEvent.ShowUIView);

        /// <summary>
        /// сообщение отмены закрытия UIView.
        ///<para>Происходит при нажатии кнопки Close и т.п.</para>
        /// <para> call format: </para>
		/// <para>Messenger.Broadcast&lt;MageAppUIViewMode&gt;(AppState.EventTypes.breakCloseUIView, UIViewMode)</para>
        /// </summary>
        public static readonly string breakCloseUIView = "break" + System.Enum.GetName(typeof(MageAppUIViewEvent), MageAppUIViewEvent.CloseUIView);

        /// <summary>
        /// сообщение закрытия UIView.
        ///<para>Происходит по нажатию кнопки Close и т.п.</para>
        /// <para> call format: </para>
        /// <para>Messenger.Broadcast&lt;MageAppUIViewMode&gt;(AppState.EventTypes.closeUIView, UIViewMode)</para>
        /// </summary>
        public static readonly string closeUIView = System.Enum.GetName(typeof(MageAppUIViewEvent), MageAppUIViewEvent.CloseUIView);

        /// <summary>
        /// Закрытие UIView: происходит после очистки ресурсов, fading еффектоф и т.п.
        /// <para> call format: </para>
        /// <para>Messenger.Broadcast&lt;MageAppUIViewMode&gt;(AppState.EventTypes.afterCloseUIView, UIViewMode)</para>
        /// </summary>
        public static readonly string afterCloseUIView = System.Enum.GetName(typeof(MageAppUIViewEvent), MageAppUIViewEvent.AfterCloseUIView);

        #endregion

        /// <summary>
        /// сообщение для пересчета базового orthographicSize для маштабироваания UIView
        /// <para>Call Format: void OnCommandCanExecute(List&lt;IAPCommand&gt;commands, bool aCanExecute)</para>
        /// <para>sender: CPStampController or other scripts</para>
        /// <para>target: AdaptUIContainer </para> 
        ///</summary>
        public const string uiCameraSizeToUIView = @"mg_uiCamSizeToUIView";

        /// <summary>
        /// включение-отключение основных объектов на сцене(при загрузке вспомогательного контента)
        /// </summary>
        public const string uiActiveMainSceneObjects = @"mg_uiActiveMSObjects";

        /// <summary>
        /// сообщение включение-отключение сценария
        /// </summary>
        public const string uiEnableMainScenario = @"mg_uiEnableMainScenario";

        /// <summary>
        /// Запуск ScreenFading 
        /// </summary>
        public const string uiStartScreenFading = @"mg_uiStartFading";

        /// <summary>
        /// ScreenFading
        /// </summary>
        public const string uiRunScreenFading = @"mg_uiRunFading";

        /// <summary>
        /// завершение ScreenFading
        /// </summary>
        public const string uiEndScreenFading = @"mg_uiEndFading";

        #region ScrollProxyWidgets : HScrollProxyWidget or other implementation
        /// <summary>
        /// событие генерируемое при тапе на центральный объект(или на области заданные списком колайдеров) в HScrollProxyWidget
        /// <para>Call Format: </para>
        /// <para> Messenger.Broadcast &lt;int,int&gt;(@"mg_uiSPTapActiveItem", CurrentItemByPos(transform.localPosition.x), TapBoxIndex) </para> 
        /// <para>sender: HScrollProxyWidget or other implementation.</para>
        /// <para>target: custom script</para>
        /// </summary>        
        public const string uiSPTapActiveItem = @"mg_uiSPTapActiveItem";

        /// <summary>
        /// событие генерируемое при Tap на управляющие элементы (стрелки)
        /// </summary>
        public const string uiSPTapNavItem = @"mg_uiSPTapNavItem";
        /// <summary>
        /// событие генерируемое при Long Tap на управляющие элементы (стрелки)
        /// </summary>
        public const string uiSPLongTapNavItem = @"mg_uiSPLongTapNavItem";

        /// <summary>
        /// событие генерируемое при остановке перемещения виджета
        /// </summary>
        public const string uiSPMoveFinished = "mg_uiSPMoveFinished";
        #endregion

        #endregion

        #region Parental Gate (pg)

        /// <summary>
        /// Сообщение "Открытие диалога parentalGate"//
        /// </summary>
        public static readonly string pgShowUIView = @"mg_pgShowUIView";

        /// <summary>
        /// Сообщение "ParentalGate пройден"
        /// </summary>
        public static readonly string pgSuccess = "mg_pgSuccess";

        /// <summary>
        /// Сообщение "прохождение ParentalGate отклонено"
        /// </summary>
        public static readonly string pgReject = "mg_pgReject";

        #endregion

        #region Cross Promotion (format 3.1)

        /// <summary>
        /// <para>Событие сигнализирующее о обновлении контента для Cross-promotion </para>
        /// <para>Внутри движка Cross-promotion.</para>
        /// </summary>
        public static readonly string cpRemoteUpdateData = "mg_cpRemoteUpdateData";

        /// <summary>
        /// управление доступностью и показом марки Cross-promotion(Stamp)!
        /// <para>Call Format: </para>
        /// <para>Messenger.Broadcast&lt;CPStampAction, bool&gt;(AppState.EventTypes.cpStampAction, CPStampAction.Show, enabled);</para>
        /// <para>sender: custom scripts when management visible effects and engine cross when updating remote data</para>
        /// <para>target: CPStampController, CPStateController</para>
        /// </summary>
        /// <example>
        /// Messenger.Broadcast&lt;CPStampAction, bool&gt;(AppState.EventTypes.cpStampAction, CPStampAction.Show, enabled);
        /// </example>
        public static readonly string cpStampAction = "mg_cpStampAction";

        /// <summary>
        /// Обновление контента.
        /// <para>Внутри движка Cross-promotion.</para>
        /// </summary>
        public static readonly string cpStampUpdate = "mg_cpStampUpdate";


        /// <summary>
        /// Сообщение "создание диалога Cross-promotion": - before parental gate
        /// </summary>
        public static readonly string cpBeforeCreateUIView = @"mg_cpBeforeCreateUIView";
        /// <summary>
        /// Сообщение "отмена создания диалога Cross-promotion": - reject parental gate event
        /// </summary>
        public static readonly string cpCancelCreateUIView = @"mg_cpCancelCreateUIView";

        /// <summary>
        /// сообщение "Создание UIView Cross-promotion" 
        /// <para>Call Format: </para>
        /// <para>sender: CPStampController and other custom scripts</para>
        /// <para>target: CPStateController</para>
        /// </summary>
        public static readonly string cpCreateUIView = @"mg_cpCreateUIView";

        /// <summary>
        /// Сообщение "Открытие диалога Cross-promotion": для контроля успешности загрузки UIView
        /// <para>Call Format: </para>
        /// Messenger.Broadcast&lt;bool&gt;(@"mg_cpShowUIView", isShow);
        /// isShow == true when Create UIView is success 
        /// <para>sender: CPStateController</para>
        /// <para>target: custom scripts</para>
        /// </summary>
        public static readonly string cpShowUIView = @"mg_cpShowUIView";

        /// <summary>
        /// Обработка команды "Закрытие диалога Cross-promotion".
        /// <para>Внутри движка.</para>
        /// <para>Call Format: </para>
        /// <para>sender: </para>
        /// <para>target: CPStateController</para>
        /// </summary>
        public static readonly string cpCloseUIView = @"mg_cpCloseUIView";
        /// <summary>
        /// Сообщение "После закрытия диалога Cross-promotion"
        /// <para>Call Format: </para>
        /// <para>Messenger.Broadcast(AppState.EventTypes.cpAfterCloseUIView)</para>
        /// <para>sender: CPStateController</para>
        /// <para>target: CPStampController and other custom scripts</para>
        /// </summary>
        public static readonly string cpAfterCloseUIView = @"mg_cpAfterCloseUIView";

        #endregion

        #region Full Version tracking (fvt) - отслеживание полной версии/switch app

        /// <summary>
        /// Отправка сообщений для команд FVTSwitchAppController
        /// <para>Call Format: void OnAction(FVTAction action) </para> 
        /// <para>sender: FVTSwitchAppButton, custom script</para>
        /// <para>target: FVTSwitchAppController</para>
        /// </summary>
        public static readonly string fvtAction = "mg_fvtAction";

        #endregion

        #region Get Full Version (gfv)

        /// <summary>
        /// Сообщение "создание диалога GetFullVersion": - before parental gate
        /// </summary>
        public static readonly string gfvBeforeCreateUIView = @"mg_gfvBeforeCreateUIView";
        /// <summary>
        /// Сообщение "отмена создания диалога GetFullVersion": - reject parental gate event
        /// </summary>
        public static readonly string gfvCancelCreateUIView = @"mg_gfvCancelCreateUIView";

        /// <summary>
        /// сообщение "Создание UIView GetFullVersion" 
        /// <para>Call Format: </para>
        /// <para>sender: and other custom scripts</para>
        /// <para>target: </para>
        /// </summary>
        public static readonly string gfvCreateUIView = @"mg_gfvCreateUIView";

        /// <summary>
        /// Сообщение "Открытие диалога GetFullVersion": для контроля успешности загрузки UIView
        /// <para>Call Format: </para>
        /// Messenger.Broadcast&lt;bool&gt;(@"mg_cpShowUIView", isShow);
        /// isShow == true when Create UIView is success 
        /// <para>sender: CPStateController</para>
        /// <para>target: custom scripts</para>
        /// </summary>
        public static readonly string gfvShowUIView = @"mg_gfvShowUIView";

        /// <summary>
        /// Обработка команды "Закрытие диалога GetFullVersion".
        /// <para>Внутри движка.</para>
        /// <para>Call Format: </para>
        /// <para>sender: </para>
        /// <para>target: CPStateController</para>
        /// </summary>
        public static readonly string gfvCloseUIView = @"mg_gfvCloseUIView";
        /// <summary>
        /// Сообщение "После закрытия диалога GetFullVersion"
        /// <para>Call Format: </para>
        /// <para>Messenger.Broadcast(AppState.EventTypes.fvAfterCloseUIView)</para>
        /// <para>sender: </para>
        /// <para>target: and other custom scripts</para>
        /// </summary>
        public static readonly string gfvAfterCloseUIView = @"mg_gfvAfterCloseUIView";
        #endregion

        #region IAP

        public static readonly string iapRequestProductData = @"mg_iapReqProduct";
        public static readonly string iapRequestProductDataComplete = @"mg_iapReqProductComplete";

        /// <summary>
        /// Внутреннее: обработка действий(purchase/restore) для IAPProxyService 
		/// <para>Call Format: void OnAction(IAPCommand command, string productId) </para> 
        /// <para>sender: IAPUIViewController</para>
        /// <para>target: IAPProxyService</para>
        /// </summary>
        public static readonly string iapAction = @"mg_iapAction";

        /// <summary>
        /// Завершение асинхронных действий iapAction (вызов из плагина Prime31 IAPCombo )
        /// <para>see IAPUILockButton.cs</para>
        /// <para>        
        /// call format: void OnActionComplete ( IAPAction action, string productId, bool didSucceed, string error )
        /// </para>
        /// <para>sender: IAPProxyService</para>
        /// <para>target: IAPUIViewController and custom script if need and know what is do</para>
        /// </summary>
        public static readonly string iapActionComplete = @"mg_iapActionComplete";

        /// <summary>
        /// Внутреннее ТОЛЬКО iOS: посылка сообщения при восстановлении продукта
        /// <para>ВСЕГДА НУЖЕН СЛУШАТЕЛЬ на ОБЪЕКТАХ "ЗАМОК"</para>
        /// <para>see IAPUILockButton.cs</para>
        /// </summary>
        public static readonly string iapIdleRestored = "mg_iapIdleRestored";

        /// <summary>
        /// <para>Сообщение перед вызовом диалога IAP.</para>
        /// <para>Используется для изменения состояния UI при прохождением ParentalGate перед созданием InApp UIView</para>
        /// <para>sender: IAPUILockButton or other custom script</para>        
        /// </summary>
        public static readonly string iapBeforeCreateUIView = @"mg_iapBeforeCreateUIView";

        /// <summary>
        /// Сообщение "отмена создания диалога IAP" (reject parental gate event)
        /// </summary>
        public static readonly string iapCancelCreateUIView = @"mg_iapCancelCreateUIView";

        /// <summary>
        /// Сообщение "Создание диалога IAP" 
        /// <para>see IAPUILockButton.cs</para>
        /// <para>Call Format: </para>
        /// <para>sender: IAPUILockButton or other custom script</para>
        /// <para>target: IAPProxyService</para>
        /// </summary>
        public static readonly string iapCreateUIView = @"mg_iapCreateUIView";

        /// <summary>
        /// Отправка сообщения "Открытия диалога IAP":
        /// Нужен, если надо обрабатывать статус создания UIView
        /// <para>Call Format: </para>
        /// Messenger.Broadcast&lt;bool&gt;(iapShowUIView, isShow);
        /// <para>isShow == true when Create UIView is success </para> 
        /// <para>sender: IAPProxyService</para>
        /// <para>target: custom script</para>
        /// </summary>
        public static readonly string iapShowUIView = @"mg_iapShowUIView";

        /// <summary>
        /// Обработка команды "Закрытие диалога IAP" (нажатие кнопки)
        /// Внутри движка IAP.
        /// <para>Call Format: </para>
        /// <para>sender: IAPUIViewController</para>
        /// <para>target: IAPProxyService</para>
        /// </summary>
        public static readonly string iapCloseUIView = @"mg_iapCloseUIView";

        /// <summary>
        /// Отправка сообщения перед "Закрытием диалога IAP"
        /// <para>Call Format: </para>        
        /// <para>sender: </para>
        /// <para>target: custom Listner для спец эффектов (если очень нужно)</para>
        /// </summary>
        public static readonly string iapBeforeCloseUIView = @"mg_iapUIViewBeforeClose";

        /// <summary>
        /// Отправка сообщения после "Закрытия диалога IAP"
        /// <para>see IAPUILockButton.cs</para>
        /// <para>Call Format: </para>
        /// <para>sender: </para>
        /// <para>target: рекомендуется добавлять custom Listner для custom action (дополнительные изменения состояния) </para>
        /// </summary>
        public static readonly string iapAfterCloseUIView = @"mg_iapUIViewAfterClose";

        /// <summary>
        /// Сообщение команды при нажатии кнопки пользователем в InAppUIView 
        /// <para>Call Format: void OnUICommandAction(IAPAction action, string productId)</para>  
        /// <para>sender: InAppCommandButton or other custom script</para>
        /// <para>target: InAppUIViewController</para>        
        /// </summary>
        public static readonly string iapUICommand = @"mg_iapUICommand";

        /// <summary>
        /// сообщение изменения состояние доступности команд IAP (для блокировки и визуального изменения кнопок)
        /// <para>Call Format: void OnCommandCanExecute(List&lt;IAPCommand&gt;commands, bool aCanExecute)</para>
        /// <para>sender: InAppUIViewController </para>
        /// <para>target: InAppCommandButton or other custom script </para> 
        ///</summary>
        public static readonly string iapUICommandCanExecute = @"mg_iapUICmdCanExecute";

        /// <summary>
        /// The IAP product unlock complete.
        /// </summary>
        public static readonly string iapUnlockProductComplete = @"mg_iapUnlockProductComplete";

        #endregion

        #region Push Notification
        /// <summary>
        /// 
        /// <para>Call Format: void OnNotification(string message, Dictionary&lt;string, Object&gt; additionalData, bool isActive)</para>
        /// </summary>
        public static readonly string pnsNotification = @"mg_pnsNotification";

        #endregion

        #region Application communication - AppDomain & AppLinks
        public const string adChangeStateAppLink = @"mg_adChangeStateAppLink";
        #endregion
    }

    #endregion
}
