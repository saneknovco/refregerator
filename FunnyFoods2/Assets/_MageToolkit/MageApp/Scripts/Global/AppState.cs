﻿using System.Collections;
using UnityEngine;

/// <summary>
/// Класс содержащий параметры для обмена между 
/// <para>разными модулями и сценами приложения.</para> 
/// <para>доступ к Singleton  через instance: AppState.instance</para> 
/// </summary>
public partial class AppState : GlobalSingletonBehaviour<AppState>
{
	#region Cached var

	private AppSettings _appSettings;

	#endregion

	#region Instance Services

	public IParentalGateService parentalGateSvc { get; private set; }

	#endregion

	#region Unity event functions

	#if MAGE_FILE_LOG
    public static string appFileLogTAG = "AppFileLogTag";
    private MageFileLogHandler _mageFileLogHandler;
#endif
	protected override void DoAwake()
	{
		base.DoAwake();
		_appSettings = AppSettings.instance;

#if MAGE_FILE_LOG
        _mageFileLogHandler = new MageFileLogHandler();
        Debug.logger.Log(appFileLogTAG, "=== Start..... ===".AddTime());
#endif
		Init();		      
	}

	IEnumerator Start()
	{
		#if MAGE_DEBUG
		Debug.Log("AppState: Start: Begin ...".AddTime());
		#endif

		// регистрация сервиса Parental Gate
		MgSvcFactory<IParentalGateService>.RegisterClass<ParentalGateService>();
		//создание экземпляра сервиса Parental Gate
		parentalGateSvc = MgSvcFactory<IParentalGateService>.instance;

#if UNITY_IOS
        // Setup AudioSession
#if MAGE_DEBUG
        Debug.Log("AppState: Start: SetupAudioSession...".AddTime());
#endif
        SysUtilsProxy.SetupAudioSession();
        yield return null;
#endif

        // запуск службы Push Notification
#if MAGE_DEBUG
        Debug.Log("AppState: Start: AppPushController run...".AddTime());
#endif
		if (_appSettings.PNS.enabled) AppPushController.Run();
		yield return null;

		// запуск службы AppDomain: AppLinks
#if MAGE_DEBUG
		Debug.Log("AppState: Start: AppDomainController run...".AddTime());
#endif
		AppDomainController.Run();
		yield return null;

		//запуск служб лайт версии
		if (_appSettings.appInfo.activeAppType != MageAppType.Full)
		{            
			if (_appSettings.IAP.enabled)
			{
#if MAGE_DEBUG
				Debug.Log("AppState: Start: IAPProxyService run...".AddTime());
#endif
				IAPProxyService.Run(); 
			}
			else
			{
#if MAGE_DEBUG
				Debug.Log("AppState: Start: GFVController run...".AddTime());
#endif
				GFVController.Run();
			}
			yield return null;

			if (_appSettings.appInfo.activeAppType == MageAppType.Lite && _appSettings.IAP.trackFullVersion)
			{
#if MAGE_DEBUG
				Debug.Log("AppState: Start: FVTSwitch run...".AddTime());
#endif
				FVTSwitchAppController.Run();
			}
		}

		yield return null;

#if MAGE_MT
		// MultiTouch_MT
		if (_appSettings.usingInputMT)
		{
#if MAGE_DEBUG
			Debug.Log("AppState: Start: InputMT run...".AddTime());
#endif
			Input.multiTouchEnabled = true;
            var inputMT = InputMT.Instance;            
            //GameObject.DontDestroyOnLoad(inputMT.gameObject);            
			yield return null;
		}
#endif

#if MAGE_DEBUG
		Debug.Log("AppState: Start: CPStateController run...".AddTime());
#endif
		//запуск службы Cross-promotion (в конце!) !!!
		CPStateController.Run();
#if MAGE_DEBUG
        Debug.Log("AppState: Start: End...".AddTime());
#endif
		yield break;
	}

	protected override void DoDestroy()
	{
#if MAGE_FILE_LOG
        Debug.logger.Log(appFileLogTAG, "=== End..... ===".AddTime());
#endif
		base.DoDestroy();
	}

#endregion

#region Init Methods	
    
	/// <summary>
	/// Инициализация глобальных переменных! (Вызывается при старте приложения)
	/// </summary>
	private void Init()
	{
#if UNITY_IOS || UNITY_TVOS || UNITY_ANDROID
		Application.targetFrameRate = 60;
#endif
		Screen.sleepTimeout = _appSettings.UI.sleepTimeout;
		loadingSceneIndex = _appSettings.Scene.MainIndex;

#if MAGE_ANALYTICS
        int isdevInfo = PlayerPrefs.GetInt("UA_DeviceInfoShort", 0);
        if (isdevInfo == 0)
        {           
            PlayerPrefs.SetInt("UA_DeviceInfoShort", 1);

            float inch = SysUtilsProxy.screenSize;
            UnityEngine.Analytics.Analytics.CustomEvent("DeviceInfoShort", new System.Collections.Generic.Dictionary<string, object>
                {
                    { "API", SysUtilsProxy.apiLevel},
                    { "CPU", SystemInfo.processorType},                   
                    { "RAM", SystemInfo.systemMemorySize},
                    { "GPURAM", SystemInfo.graphicsMemorySize},
                    { "SCREENSIZE", inch.ToString("0.0") + " inch"}
                });
#if MAGE_FILE_LOG
            Debug.Log("screen dpi: " + SysUtilsProxy.dpi + "dioganal inch: " + inch.ToString("0.0"));
#endif
            PlayerPrefs.Save();
        }
#endif

	}

#endregion

#region Scenes

	[System.NonSerialized]
	public int loadingSceneIndex = -1;

	public void LoadScene(int sceneIndex)
	{
		loadingSceneIndex = sceneIndex;
		if (loadingSceneIndex < 0)
			loadingSceneIndex = 1;
		UnityEngine.SceneManagement.SceneManager.LoadScene(loadingSceneIndex);
	}

#endregion

#region Context State

	/// <summary>
	/// Используется для сброса состояния отображаемых UIView.
	/// Может использоваться при повторном рестарте главной сцены.
	/// </summary>
	public void ResetState()
	{
		loadingSceneIndex = _appSettings.Scene.MainIndex;
		lockWidgets = false;
		//_uiViewMode = MageAppUIViewMode.None;// WARNING: direct reset. Do not event send!
		_uiViewMode = 0;
		ResetAspect();
	}

	public void ResetAspect()
	{
		_aspect = MageAppAspectRatio.AspectOthers;
		_aspectIndex = -1;
		MgCommonUtils.ResetAspect();
	}

#endregion

#region Camera

	[System.NonSerialized]    
	private MageAppAspectRatio _aspect = MageAppAspectRatio.AspectOthers;

	public MageAppAspectRatio aspect
	{
		get
		{
			if (_aspect == MageAppAspectRatio.AspectOthers)
				_aspect = MgCommonUtils.GetAspectRatio();
			return _aspect;
		}
	}

	[System.Obsolete("use property aspect!")]
	public MageAppAspectRatio Aspect
	{
		get { return aspect; }        
	}

	[System.NonSerialized]
	private int _aspectIndex = -1;

	public int aspectIndex
	{
		get
		{
			if (_aspectIndex < 0)
				_aspectIndex = MgCommonUtils.GetAspectRatioToIndex();
			return _aspectIndex;
		}
	}

	[System.Obsolete("use property aspectIndex!")]
	public int AspectIndex
	{
		get { return aspectIndex; }
	}

#endregion

#region UIView

	/// <summary>
	/// флаг неявного создания UIContainer
	/// </summary>
	[System.NonSerialized]
	public bool forceCreateUIContainer = true;
	[System.NonSerialized]
	public Vector3 UIContainerLocalPos = new Vector3(0.0f, 0.0f, 10.0f);
	private GameObject _UIContainer = null;

	/// <summary>
	/// Контайнер для экземпляров UIVIew, привязанный к камере. 
	/// Для UIContainer положение и скейл управляются через скрипт AdaptUIContainer.
	/// </summary>
	public GameObject UIContainer
	{
		get
		{
			if (_UIContainer == null)
			{
				_UIContainer = Camera.main.gameObject.FindChild(AppConfig.nameUIContainer);
				if (_UIContainer == null && forceCreateUIContainer)
				{
					_UIContainer = new GameObject(AppConfig.nameUIContainer);
					_UIContainer.transform.parent = Camera.main.transform;
					_UIContainer.transform.localPosition = UIContainerLocalPos;
					_UIContainer.transform.localScale = Vector3.one;
					_UIContainer.AddComponent<AdaptUIContainer>();
				}
			}
			return _UIContainer;
		}
	}

	/// <summary>
	/// Блокировка обработки widgets (tap ButtonWidget, etc. )
	/// </summary>
	[System.NonSerialized] 
	public bool lockWidgets = false;

	private MageAppUIViewMode _uiViewMode = 0;

	/// <summary>
	/// Битовый флаг состояния активных UIView
	/// </summary>
	public MageAppUIViewMode UIViewMode
	{
		get { return _uiViewMode; }
		set
		{
			if (_uiViewMode != value)
			{
				_uiViewMode = value;
				if (Messenger.eventTable.ContainsKey(EventTypes.UIViewMode))
					Messenger.Broadcast<MageAppUIViewMode>(EventTypes.UIViewMode, _uiViewMode);
			}
		}
	}

	/// <summary>
	/// Активен ли UIView, кроме DummyView
	/// </summary>
	public bool IsShowUIView
	{
		get
		{
			if ((_uiViewMode & MageAppUIViewMode.DummyView) == MageAppUIViewMode.DummyView)
				return _uiViewMode != MageAppUIViewMode.DummyView;
			//return (_uiViewMode != MageAppUIViewMode.None);
			return ((int)_uiViewMode != 0);
		}
	}

	/// <summary>
	/// Активен ли ParentalGate
	/// </summary>
	public bool IsShowParentalGate
	{
		get { return ((_uiViewMode & MageAppUIViewMode.ParentalGateView) == MageAppUIViewMode.ParentalGateView); }
	}

	/// <summary>
	/// Активен ли Cross
	/// </summary>
	public bool IsShowCrossPromotion
	{
		get { return ((_uiViewMode & MageAppUIViewMode.CrossPromotionView) == MageAppUIViewMode.CrossPromotionView); }
	}

	/// <summary>
	/// Gets a value indicating whether this instance is show get full version.
	/// </summary>
	/// <value><c>true</c> if this instance is show get full version; otherwise, <c>false</c>.</value>
	public bool IsShowBuyFullVersion
	{
		get { return ((_uiViewMode & AppSettings.instance.IAP.GetFullVersionViewMode) == AppSettings.instance.IAP.GetFullVersionViewMode); }
	}

	/// <summary>
	/// Активно ли UIView с флагами InApp
	/// </summary>
	public bool IsShowInAppUIView
	{
		get
		{
			return ((_uiViewMode & MageAppUIViewMode.InAppView) == MageAppUIViewMode.InAppView) ||
			((_uiViewMode & MageAppUIViewMode.InAppActionView) == MageAppUIViewMode.InAppActionView) ||
			((_uiViewMode & MageAppUIViewMode.InfoWithInAppView) == MageAppUIViewMode.InfoWithInAppView);
		}
	}

	/// <summary>
	/// Активно ли UIView с флагом DummyView
	/// </summary>
	public bool IsShowDummyView
	{
		get { return ((_uiViewMode & MageAppUIViewMode.DummyView) == MageAppUIViewMode.DummyView); }
	}

	/// <summary>
	/// Активен ли фейдинг
	/// </summary>
	public bool IsShowFadingView
	{
		get { return ((_uiViewMode & MageAppUIViewMode.FadingView) == MageAppUIViewMode.FadingView); }
	}

	public bool ApplyPlayMakerUIView()
	{
		if (IsShowUIView)
			return false;
		_uiViewMode |= MageAppUIViewMode.PlayMakerView;
		return true;
	}

	public void ResetPlayMakerUIView()
	{
		_uiViewMode &= ~MageAppUIViewMode.PlayMakerView;
	}

	/// <summary>
	/// Определения языка по умолчанию, по настройкам AppSettings and Application.systemLanguage
	/// </summary>
	public MageAppUILang DefaultLang
	{
		get
		{
			if (_appSettings.UI.isMultiLang)
			{
				MageAppUILang lang = MageAppUILang.none;
				switch (Application.systemLanguage)
				{
					case SystemLanguage.English:
						lang = MageAppUILang.en;
						break;
					case SystemLanguage.Russian:
						lang = MageAppUILang.ru;
						break;
					case SystemLanguage.Japanese:
						lang = MageAppUILang.jp;
						break;
					case SystemLanguage.French:
						lang = MageAppUILang.fr;
						break;
					case SystemLanguage.Spanish:
						lang = MageAppUILang.sp;
						break;
					case SystemLanguage.Italian:
						lang = MageAppUILang.it;
						break;
					case SystemLanguage.Portuguese:
						lang = MageAppUILang.pt;
						break;
					case SystemLanguage.German:
						lang = MageAppUILang.de;
						break;
					case SystemLanguage.Chinese:
						lang = MageAppUILang.cn;
						break;
					case SystemLanguage.Korean:
						lang = MageAppUILang.kr;
						break;
					case SystemLanguage.Turkish:
						lang = MageAppUILang.tr;
						break;

					default:
						{
							lang = _appSettings.UI.defaultLang;
							if (lang == MageAppUILang.none)
								lang = MageAppUILang.en;
							break;
						}
				}
				var dic = _appSettings.UI.supportedLang;
				if (dic.ContainsKey(lang))
				if (dic[lang])
					return lang;

				lang = _appSettings.UI.defaultLang;
				if (lang == MageAppUILang.none)
					lang = MageAppUILang.en;
				return lang;
			}
			else
			{
				var defLang = _appSettings.UI.defaultLang;
				if (defLang == MageAppUILang.none)
					return MageAppUILang.ru;
				return defLang;
			}
		}
	}

	[System.NonSerialized]
	private MageAppUILang _currentLang = MageAppUILang.none;

	/// <summary>
	/// Текуший язык, интерфейса приложения
	/// </summary>
	public MageAppUILang CurrentLang
	{
		get
		{          
			//load setting if not set
			if (_currentLang == MageAppUILang.none)
			{
				MageAppUILang df = DefaultLang;
				if (_appSettings.UI.isMultiLang)
				{
					_currentLang = (MageAppUILang)System.Enum.Parse(_currentLang.GetType(),
						PlayerPrefs.GetString(EventTypes.langUI, System.Enum.GetName(df.GetType(), df)));
				}
				else
					return df;

			}
			return _currentLang;
		}
		set
		{
			if (value == MageAppUILang.none)
			{
				Debug.LogError("CurrentLang: attempt set ".AddTime() + value);
				return;
			}
			var prevLang = _currentLang;
			_currentLang = value;            
			//SEND MESSAGE FOR CHANGE OF UI	            
			if (Messenger.eventTable.ContainsKey(EventTypes.langUI))
				Messenger.Broadcast<MageAppUILang>(EventTypes.langUI, _currentLang);
			//Save setting
			if (_currentLang != prevLang)
			{
				PlayerPrefs.SetString(EventTypes.langUI, System.Enum.GetName(_currentLang.GetType(), _currentLang));
				PlayerPrefs.Save();
			}
		}
	}

	/// <summary>
	/// <para>Текуший язык, интерфейса приложения, приведенный к строковому типу.</para>
	/// <para>Упрощает работу через PM</para>
	/// </summary>
	public string CurrentLangStr
	{
		get { return System.Enum.GetName(_currentLang.GetType(), CurrentLang); }
		set { CurrentLang = (MageAppUILang)System.Enum.Parse(typeof(MageAppUILang), value); }
	}

#endregion
}
