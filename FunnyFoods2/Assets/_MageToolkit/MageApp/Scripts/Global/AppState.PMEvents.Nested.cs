﻿
public partial class AppState
{
    /// <summary>
	/// Список имен событий  MageApp для PlayMaker
	/// </summary>
	public static class PMEvents
    {
        #region Get Full Version Events

        /// <summary>
        /// Обработана комманда GetFullVersion
        /// </summary>
        public const string gfvGetButtonDoTouch = @"MG_GFV_BUTTON_DO_TOUCH";

        /// <summary>
        /// Отправка сообщения перед "Закрытием UIView GetFullVersion" (для дополнительной логики )		
        /// </summary>
        public const string gfvBeforeCloseUIView = @"MG_GFV_BEFORE_CLOSE_UIVIEW";

        /// <summary>
        /// Обработки команды "Закрытие UIView GetFullVersion" (удаление GameObject)
        /// </summary>
        public const string gfvCloseUIView = @"MG_GFV_CLOSE_UIVIEW";

        #endregion

        #region Cross Promotion Events

        /// <summary>
        /// Open UIView Кросса 
        /// </summary>
        public const string cpOpenUIView = @"MG_CP_OPEN_UIVIEW";

        /// <summary>
        /// Close UIView Кросса 
        /// </summary>
        public const string cpCloseUIView = @"MG_CP_CLOSE_UIVIEW";

        #endregion

        #region InApp Purchase Events

        /// <summary>
        /// Open UIView IAP 
        /// </summary>
        public const string iapOpenUIView = @"MG_IAP_OPEN_UIVIEW";

        /// <summary>
        /// Отправка сообщения перед "Закрытием UIView IAP" (для дополнительной логики )
        /// </summary>
        public const string iapBeforeCloseUIView = @"MG_IAP_BEFORE_CLOSE_UIVIEW";

        /// <summary>
        /// завершение обработки команды "Закрытие UIView IAP" 
        /// </summary>
        public const string iapCloseUIView = @"MG_IAP_CLOSE_UIVIEW";

        public const string iapUnlockProductComplete = @"MG_IAP_UNLOCK_PRODUCT_COMPLETE";

        #endregion

        #region Parental Gate Events
        /// <summary>
        /// Open UIView ParentalGate
        /// </summary>
        public const string pgOpenUIView = @"MG_PG_OPEN_UIVIEW";

        /// <summary>
        /// Parse ParentalGate 
        /// </summary>
        public const string pgSuccess = @"MG_PG_SUCCESS";

        /// <summary>
        /// Redject ParentalGate 
        /// </summary>
        public const string pgReject = @"MG_PG_REJECT";

        /// <summary>
        /// Close UIView ParentalGate
        /// </summary>
        public const string pgCloseUIView = @"MG_PG_CLOSE_UIVIEW";

        #endregion
    }
}
