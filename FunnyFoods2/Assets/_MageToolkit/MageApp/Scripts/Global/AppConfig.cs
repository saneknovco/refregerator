/// ===== LIST DEFINES SYMBOLS FOR MageToolKit (СПИСОК define которые задаются в настройках проекта) =====
//#define MAGE                  // config for brand Mage
//#define BINI                  // config for brand Bini
//#define WOOOW                 // config for brand WOOOW

// config Unity packages
//#define MAGE_ANALYTICS        // включен сбор данных для Unity Analitics

//config Mage packages
//#define MAGE_MT				// поддержка InputMT!!!

// config 3rd party packages
//#define MAGE_NO_IAP           // в проекте отсутствует плагин IAP (Prime IAPCombo)
//#define MAGE_NO_PM            // в проекте отсутствует плагин PM (PlayMaker)
//#define MAGE_NO_UNI2D         // в проекте отсутствует плагин Uni2d
//#define MAGE_NO_PUSH          // в проекте отсутствует плагин OneSignal
//#define MAGE_GS               // в проект включен плагин GalleryScreenshot
//#define MAGE_OBB              // в проекте используется Obb (UnityOBBDownloader)
//#define MAGE_OBB_LITE			// библиотеку UnityOBBDownloader переключить для ключа(PUBLIC KEY) лайт версии!!!

// config Debug settings
//#define MAGE_FILE_LOG         //запись сообщений unity console (Debug.Log& etc.) в файл Application.persistentDataPath/AppLogs.txt
//#define MAGE_DEBUG            //расширенный вывод сообщений в консоль(Debug.Log).
//#define MAGE_IAP_DEBUG        //тестовый режим для проверки IAP
//#define MAGE_DLC_DEBUG        //тестовый режим для проверки компонентов DLC

/// ===== LIST DEFINE FOR MageApp (СПИСОК define  которые задаются в настройках проекта) ===== />

/// <summary>
/// Класс конфигурации настраиваемых параметров приложения 
/// и содержащий общие константы
/// </summary>
static public class AppConfig
{
    #region Legacy

    /// <summary>
    /// Legacy values
    /// </summary>
    static public class Legacy
    {
        /// <summary>
        /// project with before ver. 1.0.1
        /// </summary>
        static public class V100
        {
#if UNITY_IOS || UNITY_TVOS            
            public const string iOS_prefixCustomUrlApp_MAGE = @"mg";
            public const string iOS_prefixCustomUrlApp_BINI = @"bb";

            public const string iOS_defaultPrefixBundleId_MAGE = @"com.mage-app.a";
            public const string iOS_defaultPrefixBundleId_BINI = @"com.BiniBambini";            
#endif
        }
    }

    #endregion
    
    #region PlayerPrefs
    /// <summary>
    /// The prefix mage toolkit to PlayerPrefs.
    /// </summary>
    public const string prefixMageToolkit = @"mgtk_";
    #endregion

    #region  Brand dependency constants   

    #region Communications

    public const string defaultProductName = @"dummy";

#if UNITY_IOS || UNITY_TVOS    
    public const string prefixCustomUrlApp = @"id";
#endif

#if MAGE
    //mobile
    public const string defaultCompanyName = @"ARROWSTAR LIMITED";
    public const string defaultPrefixBundleId = @"com.arrowstar";

    //web
    public const string defaultUrlHome = @"http://mage-app.com";
    public const string defaultSuffixContentUri = @"http://u.mage-app.com";    
    public const string emailFeedback = @"support@mage-app.com";

#elif BINI     
    //mobile
    public const string defaultCompanyName = @"Bini Bambini Ltd.";
    public const string defaultPrefixBundleId = @"com.binibambini";

    //web
    public const string defaultUrlHome = @"http://binibambini.com";
    public const string defaultSuffixContentUri = @"http://u.mage-app.com/bini";
    public const string emailFeedback = "feedback@binibambini.com";

#elif WOOOW    
    //mobile
    public const string defaultCompanyName = @"ARROWSTAR LIMITED";
    public const string defaultPrefixBundleId = @"com.arrowstar";// ??? !!!

    //web
    public const string defaultUrlHome = @"http://wooowkids.com";
    public const string defaultSuffixContentUri = @"http://u.mage-app.com/wooow";
    public const string emailFeedback = "feedback@wooowkids.com";

#else //PUBLISHER    
    //mobile    
    public const string defaultCompanyName = @"Publisher Ltd.";
    public const string defaultPrefixBundleId = @"com.publisher";

    //web
    public const string defaultUrlHome = @"http://publisher.com";
    public const string defaultSuffixContentUri = @"http://unity3d.publisher.com/";
    public const string emailFeedback = @"feedback@publisher.com";
#endif

    #endregion// Communications

    #region Developer page
#if UNITY_IOS || UNITY_TVOS
    public const string prefixAppsDevUri = @"https://itunes.apple.com/us/developer/";

#if MAGE
    public const string idAppsDevUri = @"mage/id675097120";

#elif BINI
    public const string idAppsDevUri = @"bini-bambini/id820544171";

#elif WOOOW
    public const string idAppsDevUri = @"mage/id675097120"; //MAGE!!!

#else //PUBLISHER
    public const string idAppsDevUri = @"publisher/id000000000";
#endif

#elif UNITY_ANDROID
    public const string prefixAppsDevUri = @"https://play.google.com/store/apps/dev?id=";

#if MAGE
    public const string idAppsDevUri = @"7367075006174028940";

#elif BINI
    public const string idAppsDevUri = @"7849383909134155368";

#elif WOOOW
    public const string idAppsDevUri = @"7367075006174028940"; //MAGE !!!!

#else //PUBLISHER
    public const string idAppsDevUri = @"0000000000000000000";
#endif

#else
    public const string prefixAppsDevUri = @"https://itunes.apple.com/us/developer/";
#if MAGE
    public const string idAppsDevUri = @"mage/id675097120";

#elif BINI
    public const string idAppsDevUri = @"bini-bambini/id820544171";

#elif WOOOW
    public const string idAppsDevUri = @"mage/id675097120"; //MAGE!!!

#else //PUBLISHER
    public const string idAppsDevUri = @"publisher/id000000000";
#endif
#endif

    #endregion //Developer page

    #endregion //Brand

    /// <summary>
    /// Секция для констант общих компонентов библиотеки MageApp
    /// </summary>
    #region Common Section for config MageApp


    #region Game Objects

    public const string nameUIContainer = @"UIContainer";
    /// <summary>
    /// игнорируются компонентом AppSwitcher в lite версии
    /// </summary>
    public const string prefixAutoObject = @"_auto_";
    /// <summary>
    /// удаляются компонентом AppSwitcher для lite версии
    /// </summary>
    public const string prefixFVTAutoObject = @"_fvtAuto_";

    /// <summary>
	/// имя языка, которое подставляется в суффикс имен ассетов не зависящих от языка (для группы языков)
	/// </summary>
	public const string langToUniAssets = @"uni";
    /// <summary>
    /// суффикс в именах ассетов не зависящих от языка (для группы языков)
    /// </summary>
    public const string sufixToUniAssets = @"_" + langToUniAssets;

    /// <summary>
    /// суффикс для Alpha Texture
    /// </summary>
    public const string sufixToAlphaTexture = @"_Alpha";
    
	/// <summary>
	/// свойство шейдера для Alpha Texture (ETC compression)
	/// </summary>
	public static readonly string [] propAlphaTextureInShader = { @"_MainTex_Alpha", @"_AlphaMap"};


    /// <summary>
    /// окончание для mesh asset
    /// </summary>
    public const string sufixToMesh = @"_mesh";

    /// <summary>
    ///суффикс в именах ассетов(prefabs, etc) разработанные для Portrait ориентации
    /// </summary>
    public const string sufixToPortrait = @"_P";

#endregion

#region System

    public const float timeWaitQuantium = 0.05f; // 50ms (1 frame in 20 FPS)
	public const float timeWaitFinishing = 4f * timeWaitQuantium; //200 ms
	public const float timeWaitUnload = 5f * timeWaitQuantium; //250 ms = эмпирический интервал
	public const float timeWaitFrame = 0.12f;// 120 ms - один фрейм разряженной анимации
    /// <summary>
    /// задержка для отработки корутины по запуску сервисов MageToolKit
    /// </summary>
    public const float timeWaitRunServices = 1.0f;

    public const int frameWaitAsync = 3;
    public const int frameWaitAsyncShort = 1;
    public const int fvtFrameWaitAsync = 5; //wait in awake (first run)


#if UNITY_IOS || UNITY_TVOS
    public const string imageFileExt = @".pvr";    
#else
    public const string imageFileExt = @".png";
#endif

#endregion

#endregion
}