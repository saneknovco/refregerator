﻿using UnityEngine;

public class GlobalSingletonBehaviour<T>:  MonoBehaviour where T : GlobalSingletonBehaviour<T>
{
    #region Static
    protected static T _instance;	
    protected static bool _destroyed;
#if UNITY_EDITOR	
	protected static HideFlags _objectHideFlags = HideFlags.None;
#else
	protected static HideFlags _objectHideFlags = HideFlags.None;
#endif
       
    public static T instance
	{
		get 
		{
            return GlobalSingletonBehaviour<T>._instance != null ||
            _destroyed ? GlobalSingletonBehaviour<T>._instance : GlobalSingletonBehaviour<T>.Load(); 
		}
        set { GlobalSingletonBehaviour<T>._instance = value; }
	}

    [System.Obsolete("use property: instance!")]
    public static T Instance
    {
        get
        {
            return GlobalSingletonBehaviour<T>._instance != null ||
            _destroyed ? GlobalSingletonBehaviour<T>._instance : GlobalSingletonBehaviour<T>.Load();
        }
        set { GlobalSingletonBehaviour<T>._instance = value; }
    }


    public static bool Run(bool forceActivateObject = false)
    {
        var dummy = instance;
        if(forceActivateObject)
        {
            if (dummy.gameObject.activeSelf == false) dummy.gameObject.SetActive(true);
        }
        return dummy!=null;    
    }

    public static void Release(bool justComponent = false)
    {
        if (_instance != null && !_destroyed) {
            if (justComponent) GameObject.Destroy(_instance);
            else GameObject.Destroy(_instance.gameObject);            
        }
    }

	private static T Load()
	{
        var instance = FindObjectOfType<T>();
		if (instance == null) {
			var obj = new GameObject(AppConfig.prefixAutoObject + typeof(T).Name);            
			instance = obj.AddComponent<T>();
            if (_objectHideFlags != HideFlags.None)
                obj.hideFlags = _objectHideFlags;
		}
		instance.Awake();
		return instance;
	}
    #endregion

    #region UNITY EDITOR

#if UNITY_EDITOR
	/// _objectHideFlags = HideFlags.DontSaveInEditor; - resolve this is issue
	void Reset()
	{
		// предотвращает создание второго экземпляра скрипта
		//проверяем у текущего объекта
		bool lockNewInstance = GetComponents<GlobalSingletonBehaviour<T>>().Length > 1;
		//  проверяем на всей сцене
		if (!lockNewInstance)
		{
			var scripts = Resources.FindObjectsOfTypeAll<T>();//FindObjectsOfType<T>();
			if (scripts != null) lockNewInstance = scripts.Length > 1;
		}
		if (lockNewInstance) Invoke("DestroyThis", 0);
	}

	void DestroyThis()
	{
		DestroyImmediate(this);
	}
#endif

    #endregion       
   
    #region Unity event functions

    [System.NonSerialized]//???
    private bool _awaken;

    protected virtual void DoAwake() { }
    protected virtual void DoDestroy() { }  

    void Awake()
	{
        //--- 1 ---
        //lock multiple calls
        if (this._awaken) return;
        this._awaken = true;

        //--- 2 ---
        if (GlobalSingletonBehaviour<T>._instance != null && GlobalSingletonBehaviour<T>._instance != this) {
			Object.Destroy(this.gameObject);
			return;
		}
        //--- 3 ---        
        GlobalSingletonBehaviour<T>._instance = (T)this;
		Object.DontDestroyOnLoad(this.gameObject);
        // --- 4 --- child awake
        DoAwake();
	}

	void OnDestroy()
	{
        //!!! MOD === 2015.0402 ===
        //lock multiple calls
        if (_destroyed) return;
        //!!! MOD === 2015.0402 ===

        if (GlobalSingletonBehaviour<T>._instance == this) {
			_destroyed = true;
			this.DoDestroy();
            GlobalSingletonBehaviour<T>._instance = null;
            _destroyed = false;
		}
    }

    #endregion
}
