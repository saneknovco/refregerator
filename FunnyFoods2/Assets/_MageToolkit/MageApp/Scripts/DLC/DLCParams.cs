﻿using UnityEngine;
using System.Collections.Generic;

/// <summary>
/// Параметры для наследника DLCLoader
/// </summary>
public class DLCParams : MonoBehaviour 
{
	public Dictionary<string, string> loaderParams = new Dictionary<string, string>();
	/// <summary>
	/// время задержки после вызовом MakeRequest
	/// </summary>
	public float waitBetweenRequest = 0.0f;
    /// <summary>
    /// время задержки перед вызовом DoAfterLoadingPackage
    /// </summary>
    public float waitBeforeComplete = 0.0f;
    /// <summary>
    /// время задержки после вызовом DoAfterLoadingPackage
    /// </summary>
	public float waitAfterComplete = 0.0f;
    /// <summary>
    /// время задержки перед повторной попыткой закачки
    /// </summary>
    public float waitAfterWWWerror = 5.0f;
    /// <summary>
    /// колличество попыток закачек контента
    /// </summary>
	public int maxRetryLoadCount = 5;
}
