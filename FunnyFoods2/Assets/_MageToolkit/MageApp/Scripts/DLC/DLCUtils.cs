﻿using UnityEngine;
using System.Linq;

public class DLCUtils 
{    
	const int headerSizeMipMap = 52;
    const int headerSize = headerSizeMipMap + 15;//67;//52+15(metadata)

	/// <summary>
	/// агрузка RGB32 текстуры с локального накопителя данных из png file
	/// </summary>
	/// <returns>The load image.</returns>
	/// <param name="imagePath">Image path.</param>
	public static Texture2D LocalLoadPNGTexture(string uri, bool makeNoLongerReadable = true)
	{
		try
		{
			Texture2D targetTex = null;
			var bytes = System.IO.File.ReadAllBytes(uri);
			if (bytes != null)
			{
				targetTex = new Texture2D(1, 1);//new Texture2D(1, 1);
				if (!targetTex.LoadImage(bytes))
				{
					GameObject.Destroy(targetTex);
					targetTex = Texture2D.blackTexture;
				}
				else targetTex.Apply(false, makeNoLongerReadable);				
			}
			return targetTex;
		}
		catch
		{
			return Texture2D.blackTexture;
		}
	}

    /// <summary>
    /// загрузка PVR текстуры с локального накопителя данных
    /// </summary>
    /// <param name="uri"></param>
    /// <param name="textureSize"></param>
    /// <param name="textureFormat"></param>
    /// <param name="mipmap"></param>
    /// <param name="makeNoLongerReadable"></param>
    /// <returns></returns>
    public static Texture2D LocalLoadPVRTexture( string uri, int textureSize = 256, 
                                                  TextureFormat textureFormat = TextureFormat.PVRTC_RGBA4, 
                                                  bool mipmap = false, 
                                                  bool makeNoLongerReadable = true)
    {
		try
		{
			byte[] buffer = System.IO.File.ReadAllBytes(uri);
			if (buffer == null) return Texture2D.blackTexture;

			//int bufSize = buffer.GetLength(0);
			//byte[] texMem = new byte[bufSize - headerSize];
			//System.Buffer.BlockCopy(buffer, headerSize, texMem, bufSize  - headerSize,0);

			var header = mipmap ? headerSizeMipMap : headerSize;
			buffer = buffer.Skip(header).ToArray();
			Texture2D targetTex = new Texture2D(textureSize, textureSize, textureFormat, mipmap);
			targetTex.LoadRawTextureData(buffer);
			targetTex.Apply(mipmap, makeNoLongerReadable);
			return targetTex;
		}
		catch
		{
			return Texture2D.blackTexture;
		}
    }    

    /// <summary>
	/// загрузка PVR4 текстуры с локального накопителя данных
    /// </summary>
    /// <param name="uri"></param>
    /// <param name="makeNoLongerReadable"></param>
    /// <returns></returns>
	public static Texture2D LocalLoadPVR4Texture( string uri, bool makeNoLongerReadable = true)
	{
        try
        {
            byte[] buffer = System.IO.File.ReadAllBytes(uri);
            if (buffer == null) return Texture2D.blackTexture;
            int textureSize = (int)Mathf.Sqrt(2 * (buffer.GetLength(0) - headerSize));
            if (!Mathf.IsPowerOfTwo(textureSize)) textureSize = 256;         
            buffer = buffer.Skip(headerSize).ToArray();
            Texture2D targetTex = new Texture2D(textureSize, textureSize, TextureFormat.PVRTC_RGBA4, false);
            targetTex.LoadRawTextureData(buffer);
            targetTex.Apply(false, makeNoLongerReadable);
            return targetTex;
        }
        catch
        {           
            return Texture2D.blackTexture;
        }
	}

    public static string LocalLoadStringData(string uri)
    {
        try
        {
            byte[] buffer = System.IO.File.ReadAllBytes(uri);
            if (buffer == null) return string.Empty;
            return System.Text.UTF8Encoding.UTF8.GetString(buffer);
        }
        catch
        {
            return string.Empty;
        }
    }

    public static bool LocalSaveStringData(string uri, string stringData)
    {
        bool result = false;
        try
        {            
            byte[] buffer = System.Text.UTF8Encoding.UTF8.GetBytes(stringData);            
            System.IO.File.WriteAllBytes(uri, buffer);
            result = true;
        }
        catch
        {
            result =  false;
        }
        return result;        
    }
}

//public class LoadPVRTexture : MonoBehaviour
//{
//    public Material targetMat = null;
//    private Texture2D targetTex = null;

//#if UNITY_IOS && !UNITY_EDITOR
//    private const int _HeaderSize = 52;
//#else
//    private const int _HeaderSize = 0;
//#endif

//    void Start()
//    {
//#if UNITY_IOS && !UNITY_EDITOR
//        targetTex = new Texture2D(128, 128, TextureFormat.PVRTC_RGBA4, true);
//#else
//        targetTex = new Texture2D(128, 128);
//#endif

//        StartCoroutine(LoadTexture());
//        targetMat.mainTexture = targetTex;
//    }

//    IEnumerator LoadTexture()
//    {
//#if UNITY_IOS && !UNITY_EDITOR
//        string filePath = System.IO.Path.Combine(Application.streamingAssetsPath, "Test_UnityLogoLarge.pvr");
//#else
//        string filePath = System.IO.Path.Combine(Application.streamingAssetsPath, "Test_UnityLogoLarge.png");
//#endif

//        byte[] texMem = null;
//        if (filePath.Contains("://"))
//        {
//            WWW www = new WWW(filePath);
//            yield return www;
//            texMem = www.bytes.Skip(_HeaderSize).ToArray();
//        }
//        else
//        {
//            texMem = System.IO.File.ReadAllBytes(filePath).Skip(_HeaderSize).ToArray();
//        }

//#if UNITY_IOS && !UNITY_EDITOR
//        targetTex.LoadRawTextureData(texMem);
//        targetTex.Apply(true, true);
//#else
//        targetTex.LoadImage(texMem);
//#endif

//        yield break;
//    }
//}