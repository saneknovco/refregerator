﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

/// <summary>
/// Загрузчик динамического контента. 
/// Для обработки загружаемого контента необходимо создать наследника 
/// и перегрузить метод DoAfterLoadingItem &amp; DoAfterLoadingPackage
/// </summary>
public abstract class DLCLoader : MonoBehaviour
{
	#region Nested types: Enums
	/// <summary>
    ///         Машина состояний
    /// Init!-&gt;Ready&lt;--------&gt;Run-&gt;Finalize-&gt;Stop  
    ///              |&lt;--Wait--&lt;|
    /// </summary>
    [System.Serializable]
    public enum LoadingState
    {
        None = 0,
        Init,
        Ready,
        Run,
        Wait,
        Finalize,
        Stop
    };

    [System.Serializable]
    public enum ActionState
    {
        None            = 0,
        BeforeBreak     = 1,
        BeforeDone      = 2,
        BeforeFailure   = 3,
        AfterBreak      = 4,
        AfterDone       = 5,
        AfterFailure    = 6
    }

	#endregion

	#region Static
	/// <summary>
	/// старт процесса загрузки динамического контента
	/// </summary>
	/// <typeparam name="T">наследник MgDCLoader</typeparam>
	/// <param name="loaderParams"></param>
	/// <example>
    ///		Dictionary&lt;string, string&gt; prms = new Dictionary&lt;string, string&gt;();
	///		prms.Add(@"duid", @"http://u.m-a.com/udid.list");
    ///		MgDCLoader.RunLoader&lt;CheckDUID&gt;(prms); 
	///	</example>	
	public static void RunLoader<T>(Dictionary<string, string> loaderParams, float waitAfterComplete = AppConfig.timeWaitFinishing, int maxRetryLoadCount = 5,
		float waitAfterWWWerror = 5.0f, float waitBeforeComplete = 0.0f, float waitBetweenRequest = 0.0f) where T : DLCLoader
	{
		var component = new GameObject(AppConfig.prefixAutoObject + typeof(T).Name, typeof(DLCParams)).GetComponent<DLCParams>();
		if (component) {
#if MAGE_DEBUG && MAGE_DLC_DEBUG
                Debug.Log("run " + typeof(T).Name);
#endif	
			GameObject.DontDestroyOnLoad(component.gameObject);
			component.loaderParams = loaderParams;
			component.waitAfterComplete = waitAfterComplete;
			component.maxRetryLoadCount = maxRetryLoadCount;
            component.waitAfterWWWerror = waitAfterWWWerror;
            component.waitBeforeComplete = waitBeforeComplete;
			component.waitBetweenRequest = waitBetweenRequest;
			component.gameObject.AddComponent<T>();  
		} 
	}

    #endregion

    #region Members

    protected Dictionary<string, string> _loaderParams;
    protected bool _isBreakPackRequest = false;
    protected bool _isRetryLoad = false;
    private int _maxRetryLoadCount = 1;
    private int _retryLoadCount = 0;
    private float _waitAfterWWWerror = 5.0f;
    private float _waitAfterComplete = AppConfig.timeWaitFinishing;
    private float _waitBeforeComplete = 0.0f;
	private float _waitBetweenRequest = 0.0f;

    #endregion

    #region Methods
    private IEnumerator MakeRequest(string key, string uri)
	{

#if MAGE_DLC_DEBUG        
            Debug.Log((@"DLCLoader: BeforeMakeRequest: " + uri).AddTime());
#endif
        WWW www = new WWW(uri);
		www.threadPriority = ThreadPriority.Low;
		yield return www;
        DoAfterLoadingItem(key, www);

#if MAGE_DLC_DEBUG
            Debug.LogWarning((@"DLCLoader:MakeRequest: " + uri).AddTime());
            if(!string.IsNullOrEmpty(www.error))Debug.LogWarning(("DLCLoader:MakeRequest:www.error: " + www.error).AddTime());        
#endif
        if (string.IsNullOrEmpty(www.error) && (_waitBetweenRequest >= AppConfig.timeWaitQuantium) )		
			yield return new WaitForSeconds(_waitBetweenRequest);		
		//www.Dispose();
		yield break;
	}

    protected virtual void DoBeforeLoadingPackage()
    {
        return;
    }

    protected virtual void DoBeforeRetryLoadingPackage()
    {
        return;
    }

	protected virtual void DoAfterLoadingItem(string key, WWW www)
	{
		return;
	}

	protected virtual void DoAfterLoadingPackage()
	{
		return;
	}

    #endregion

	#region Unity event functions

	IEnumerator Start()
	{
        if (Application.internetReachability == NetworkReachability.NotReachable){
			GameObject.Destroy(this.gameObject);
			yield break;
		}
		var cmp = gameObject.GetComponent<DLCParams>();
        if(cmp) {		    
		    _loaderParams = cmp.loaderParams;
			_waitBeforeComplete = cmp.waitBeforeComplete;
			_maxRetryLoadCount = cmp.maxRetryLoadCount;
			_waitAfterWWWerror = cmp.waitAfterWWWerror;
		    _waitAfterComplete = cmp.waitAfterComplete;            
			_waitBetweenRequest = cmp.waitBetweenRequest;			
        }        
		_retryLoadCount = 0;
        DoBeforeLoadingPackage();
	    bool isRetry = false;
		do {
            _isRetryLoad = false;
            _isBreakPackRequest = false;
            _retryLoadCount++;

            if (_loaderParams == null) break;
			foreach (var item in _loaderParams) {
				var request = MakeRequest(item.Key, item.Value);
				while (request.MoveNext()) yield return request.Current;
                if (_isBreakPackRequest || _isRetryLoad) break;
			}
			yield return new WaitForEndOfFrame();
            isRetry = _isRetryLoad && (_retryLoadCount < _maxRetryLoadCount);
            if (isRetry)
            {
                DoBeforeRetryLoadingPackage();//JUST BEFORE RETRY DOWNLOADING
                yield return new WaitForSeconds(_waitAfterWWWerror);
            }
		}
        while ( isRetry );

        if (_waitBeforeComplete > 0.0f) yield return new WaitForSeconds(_waitBeforeComplete);	
        DoAfterLoadingPackage(); // JUST ONE CALL!		
		if (_waitAfterComplete > 0.0f) Destroy(this.gameObject, _waitAfterComplete);
		else Destroy(this.gameObject);

		yield break;
    }

    #endregion
}
