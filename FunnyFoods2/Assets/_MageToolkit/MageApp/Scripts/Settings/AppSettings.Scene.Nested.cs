﻿using UnityEngine;
using System.Collections.Generic;

public partial class AppSettings
{
    /// <summary>
    /// Параметры конфигурации сцен
    /// </summary>
    [FullInspector.InspectorCategory("Scene")]
    [FullInspector.InspectorOrder(50), FullInspector.ShowInInspector]
    [FullInspector.InspectorHeader("Конфигурация сцен"),FullInspector.InspectorMargin(20),FullInspector.InspectorDivider]
    [FullInspector.InspectorTooltip("Параметры конфигурации сцен")]    
    public SceneSettings Scene;

	#region Nested types

	#region  SceneSettings: Конфигурация сцен
	[System.Serializable]
	public class SceneSettings : FullInspector.BaseObject
    {
        [FullInspector.ShowInInspector]          
        public int LogoIndex { get { return 0; } }

		[SerializeField, FullInspector.InspectorHidePrimary]
		private int _mainIndex = 1;
		[FullInspector.ShowInInspector]             
		public int MainIndex { get { return _mainIndex; }  set { _mainIndex = value;}}

        [FullInspector.ShowInInspector]             
        public int ContentIndex { get { return 2; } }

        [FullInspector.InspectorComment(FullInspector.CommentType.None, "Splash scene (custom Logotype)"),FullInspector.InspectorMargin(6)]        
        public int SplahIndex { get; private set; }

        [FullInspector.InspectorComment(FullInspector.CommentType.None, "Очистка ресурсов"), FullInspector.InspectorMargin(6)]
		[FullInspector.InspectorTooltip("Сцена для очистки ресурсов")]        
        public int CleanerIndex {get; private set;}
    }
    #endregion	

	#endregion
}
