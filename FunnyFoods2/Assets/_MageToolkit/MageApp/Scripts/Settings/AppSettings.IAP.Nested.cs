﻿using UnityEngine;
using System.Collections.Generic;

// Настройки модулей приложения.
// Реализация как Nested классы класса AppSettings
public partial class AppSettings
{
    /// <summary>
    /// Параметры конфигурации InApp
    /// </summary>
    [FullInspector.InspectorCategory("IAP")]
    [FullInspector.InspectorOrder(25), FullInspector.ShowInInspector]
    [FullInspector.InspectorHeader("Параметры конфигурации InApp"), FullInspector.InspectorMargin(20)]    
    public IAPSettings IAP;
    
    #region Nested types

    #region  IAPSettings: Параметры конфигурации InApp
    /// <summary>
    /// класс параметров конфигурации InApp
    /// </summary>
    [System.Serializable]
    public class IAPSettings : FullInspector.BaseObject
    {
        [FullInspector.InspectorOrder(1)]
        public bool enabled = false;
        private bool isGetFullVersion { get { return !enabled; } }

		[FullInspector.InspectorOrder(2), FullInspector.InspectorShowIf("enabled")]
		[FullInspector.InspectorName("UIView Mode")]
		public MageAppUIViewMode UIViewMode = MageAppUIViewMode.InAppView;

		[FullInspector.InspectorOrder(2.1)]
		[FullInspector.InspectorName("Broadcast PlayMaker Events")]
		public bool broadcastPMEvents = true;

        [SerializeField]
        [FullInspector.InspectorOrder(3), FullInspector.InspectorHidePrimary]
        private Dictionary<MageAppUILang, string> _uriPrefabs;
        [FullInspector.InspectorOrder(3.1), FullInspector.ShowInInspector, FullInspector.InspectorShowIf("enabled")]
        public Dictionary<MageAppUILang, string> UriPrefabs
        {
            get 
            {
                if(_uriPrefabs == null || _uriPrefabs.Count<1) {
                    _uriPrefabs = new Dictionary<MageAppUILang, string>()
                    {                       
						{MageAppUILang.en, @"UI/InAppUIViewControl"},
						{MageAppUILang.ru, @"UI/InAppUIViewControl"}
                    };
                }
                return _uriPrefabs; 
            }
            set { _uriPrefabs = value; }
        }

        [FullInspector.InspectorOrder(2), FullInspector.InspectorShowIf("isGetFullVersion")]
        public MageAppUIViewMode GetFullVersionViewMode = MageAppUIViewMode.BuyFullVersionView;

        [SerializeField]
        [FullInspector.InspectorOrder(3), FullInspector.InspectorHidePrimary]
        private Dictionary<MageAppUILang, string> _uriGfVPrefabs;
        [FullInspector.InspectorOrder(3.1), FullInspector.ShowInInspector, FullInspector.InspectorShowIf("isGetFullVersion")]
        public Dictionary<MageAppUILang, string> GetFullVersionUriPrefabs
        {
            get
            {
                if (_uriGfVPrefabs == null || _uriGfVPrefabs.Count < 1)
                {
                    _uriGfVPrefabs = new Dictionary<MageAppUILang, string>()
                    {                       
						{MageAppUILang.en, @"UI/GetFullVersionViewControl"},
						{MageAppUILang.ru, @"UI/GetFullVersionViewControl"}
                    };
                }
                return _uriGfVPrefabs;
            }
            set { _uriGfVPrefabs = value; }
        }
				
		[FullInspector.InspectorOrder(4.1), FullInspector.InspectorRange(AppConfig.timeWaitFinishing, 2.0f)]
		[FullInspector.InspectorTooltip("Задержка перед закрытием UIView InApp/GetFullVersion для создания эффектов")]
		public float timeWaitBeforeClose = AppConfig.timeWaitFinishing;      

		[FullInspector.InspectorOrder(6)]
		[FullInspector.InspectorHeader("Отслеживать наличие полной врсии на устройстве"), FullInspector.InspectorMargin(12)]
		public bool trackFullVersion = true;
		
    }
    #endregion

    #endregion
}
