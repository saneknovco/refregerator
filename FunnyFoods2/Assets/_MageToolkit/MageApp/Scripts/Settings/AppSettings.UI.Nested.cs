﻿using UnityEngine;
using System.Collections.Generic;

// Настройки модулей приложения.
// Реализация как Nested классы класса AppSettings
public partial class AppSettings
{
    /// <summary>
    /// Параметры конфигурации UI: язык, Resources, UIView
    /// </summary>
    [FullInspector.InspectorCategory("UI")]
    [FullInspector.InspectorOrder(30), FullInspector.ShowInInspector, FullInspector.InspectorMargin(20)]
    [FullInspector.InspectorHeader("Параметры UI: язык, Resources, UIView")]
    public UISettings UI;

	/// <summary>
	/// Параметры конфигурации Effects: fading
	/// </summary>
	[FullInspector.InspectorCategory("UI")]
	[FullInspector.InspectorOrder(30.1), FullInspector.ShowInInspector, FullInspector.InspectorMargin(20)]
	[FullInspector.InspectorHeader("Параметры Effects: fading")]
	public EffectSettings Effect; 

    #region Nested types

    #region UISettings: Параметры конфигурации UI: язык, Resources, UIView
    [System.Serializable]
    public class UISettings : FullInspector.BaseObject
    {

        const int LangCount = 12;

        [SerializeField, FullInspector.InspectorHidePrimary]
        private bool _isMultiLang = true;
        [FullInspector.ShowInInspector]
        [FullInspector.InspectorComment(FullInspector.CommentType.None, "!R")]
        public bool isMultiLang
        {
            get { return _isMultiLang; }
            private set {
                _isMultiLang = value;
                if(_isMultiLang == true)
                {
                    _defaultLang = MageAppUILang.en;
                    _defaultLangStr = string.Empty;
                }
            }
        }

        [SerializeField, FullInspector.InspectorHidePrimary]
        private MageAppUILang _defaultLang = MageAppUILang.en;
        [FullInspector.ShowInInspector]
        [FullInspector.InspectorComment(FullInspector.CommentType.None, "!R")]
        public MageAppUILang defaultLang
        {
            get { return _defaultLang; }
            private set 
            {
                if (_isMultiLang)
                {
                    if (value == MageAppUILang.en) _defaultLang = value;
                    else Debug.LogError("AppSettings:DefaultLang: '".AddTime() + value + "' no suppoerted value!");
                }
                else
                {
                    _defaultLang = value;
                    _defaultLangStr = string.Empty;
                    //if (value == MageAppUILang.en || value == MageAppUILang.ru || value == MageAppUILang.cn)
                    //    _defaultLang = value;
                    //else Debug.LogError("AppSettings:DefaultLang: '".AddTime() + value + "' no suppoerted value!");
                }
            }
        }

        [System.Obsolete("AppSettings - use property defaultLang")]
        public MageAppUILang DefaultLang
        {
            get { return _defaultLang; }           
        }

        private string _defaultLangStr = string.Empty;
        public string defaultLangStr
        {
            get
            {
                if (string.IsNullOrEmpty(_defaultLangStr))
                    _defaultLangStr = System.Enum.GetName(typeof(MageAppUILang), defaultLang);
                return _defaultLangStr;
            }
        }

        [System.Obsolete("AppSettings - use property defaultLangStr")]
        public string DefaultLangStr
        {
            get
            {
                return defaultLangStr;
            }
        }

        [SerializeField, FullInspector.InspectorHidePrimary]
        private Dictionary<MageAppUILang, bool> _supportedLang;
        [FullInspector.ShowInInspector, FullInspector.InspectorShowIf("_isMultiLang")]
        public Dictionary<MageAppUILang, bool> supportedLang
        {
            get
            {
                if (_supportedLang == null || _supportedLang.Count < LangCount)
                {
                    if (_supportedLang != null) _supportedLang.Clear();
                    _supportedLang = new Dictionary<MageAppUILang, bool>()
                        {                        
						    {MageAppUILang.none, false},
                            {MageAppUILang.en, true},
						    {MageAppUILang.ru, true},
                            {MageAppUILang.jp, false},
						    {MageAppUILang.fr, false},
                            {MageAppUILang.sp, false},
                            {MageAppUILang.it, false},
                            {MageAppUILang.pt, false},
                            {MageAppUILang.de, false},
                            {MageAppUILang.cn, false},
                            {MageAppUILang.kr, false},
                            {MageAppUILang.tr, false}
                        };
                }
                return _supportedLang;
            }
            set
            {
                if (value != null)
                    if (value.Count == LangCount) _supportedLang = value;
            }
        }

        [System.Obsolete("AppSettings - use property supportedLang")]
        public Dictionary<MageAppUILang, bool> SupportedLang
        {
            get { return supportedLang; }
        }

        [FullInspector.ShowInInspector, FullInspector.InspectorMargin(12)]
        [FullInspector.InspectorComment(FullInspector.CommentType.None, "Сайт: домашняя страница")]
        public string UrlHome { get { return AppConfig.defaultUrlHome; } }
        
        //[FullInspector.ShowInInspector]
        //[FullInspector.InspectorComment(FullInspector.CommentType.None, "Сайт: страница поддержки")]
        //public string UrlSupport { get { return AppConfig.defaultUrlSupport; } }

        [FullInspector.ShowInInspector]
        [FullInspector.InspectorComment(FullInspector.CommentType.None, "Путь к текстовым assets в Resources")]
        public string UriTexts { get { return @"UI/Texts/"; } }

        [FullInspector.ShowInInspector]
        [FullInspector.InspectorComment(FullInspector.CommentType.None, "Путь к текстурам в Resources")]
        public string UriTextures { get { return @"UI/Textures/"; } }

        [FullInspector.ShowInInspector]
        [FullInspector.InspectorComment(FullInspector.CommentType.None, "Путь к звуковым эффектам в Resources")]
        public string UriSounds { get { return @"UI/Sounds/"; } }

        [System.NonSerialized]
        private string _clickName = @"SingleClick";
        [FullInspector.ShowInInspector]
        [FullInspector.InspectorComment(FullInspector.CommentType.None, "Имя аудиофайла клика")]
        public string clickName
        {
            get { return _clickName; }
            private set
            {
                if (!string.IsNullOrEmpty(value)) _clickName = value;
                else _clickName = "SingleClick";
            }
        }

        [FullInspector.ShowInInspector]
        public string UriClick { get { return UriSounds + clickName; } }

		[FullInspector.InspectorComment(FullInspector.CommentType.None, "Screen SleepTimeout.NeverSleep = -1 or SleepTimeout.SystemSetting = -2")]
		public int sleepTimeout = SleepTimeout.NeverSleep;

    }
    #endregion

	#region EffectSettings: параметры для fade эффекта в кроссе, и т.п.
	[System.Serializable]
	public class EffectSettings : FullInspector.BaseObject
	{
		[FullInspector.InspectorTooltip("Параметры по умолчанию для ScreenFading")]
		public Mage.Effects.FadingParams fading;

        [FullInspector.InspectorTooltip("Параметры по умолчанию для фона затемнения Parental Gate")]
        public Mage.Effects.BackScreenParams  parentalGateBackScreen;
    }
	#endregion

    #endregion
}
