﻿using UnityEngine;
using System;
#if UNITY_EDITOR
using UnityEditor;
#endif
using System.Collections.Generic;

////////////////////////////////////////////////////
/// <summary>
/// Настройки приложения и значения для инициализации переменных.
/// <para>!R - readonly property.</para>
/// <para>!D - dependency by other properties &amp; members property.</para> 
/// <para>!C - computed property.</para>
/// </summary>
/// ////////////////////////////////////////////////

public partial class AppSettings : FullInspector.BaseScriptableObject<FullInspector.FullSerializerSerializer>
{
    #region UNITY EDITOR

#if UNITY_EDITOR
    [UnityEditor.MenuItem("Assets/Create/Mage/AppSettings")]
    public static void CreateAsset()
    {
        ScriptableObjectUtility.CreateAsset<AppSettings>(assetNamePrefix);
    }
#endif

    #endregion

    #region  AppSetings main code
    const string appConfigFolderPath = @"Assets/AppConfig/";

    const string assetName = @"AppSettings";
    const string subPath = @"Settings/";   
	const string bundleVersionPattern = @"^([\d\.]+)$";

    private static string assetNamePrefix
    {
        get
        {
            return PrjSettings.GetAssetNameToActivePlatform("");
        }
    }

	public static string assetFullName
	{
		get {
            return PrjSettings.GetAssetNameToActivePlatform(assetName);   
		}
	}

    private static AppSettings _instance;
    [System.Obsolete("use AppSettings.instance")]
    public static AppSettings Instance
    {
        get { return instance; }
    }

    public static AppSettings instance
    {
        get
        {
            try
            {
                if (_instance == null)
                {                    
					_instance = MgUnityHelper.CreateFromResources<AppSettings>(subPath, assetFullName);
                }
            }
            catch
            {
                _instance = ScriptableObject.CreateInstance<AppSettings>();
            }
            return _instance;
        }
    }
    
    private PrjSettings.PrjPlatformSettings _prjActivePlatformSettings = null;
    [FullInspector.InspectorCategory("Main")]
    [FullInspector.InspectorOrder(48), FullInspector.ShowInInspector, FullInspector.InspectorMargin(12)]
    [FullInspector.InspectorDivider()]
    [FullInspector.InspectorHeader("Multi Touch"), FullInspector.InspectorTooltip("для поддержки проектов без мультитач")]
    public bool usingInputMT
    {
        get
        {
            if (_prjActivePlatformSettings == null)
                _prjActivePlatformSettings = PrjSettings.instance.settings.activePlatformSettings;            
            return _prjActivePlatformSettings.usingInputMT;            
        }       
    }    

    [FullInspector.InspectorCategory("Main")]
	[FullInspector.InspectorCategory("ACS")]
	[FullInspector.InspectorCategory("DLC")]
	[FullInspector.InspectorOrder(49), FullInspector.ShowInInspector, FullInspector.InspectorMargin(12)]
    [FullInspector.InspectorDivider()]
    [FullInspector.InspectorHeader("Active Runtime Platform")]
	public RuntimePlatform activePlatform
	{
		get
        {            
            return PrjSettings.instance.settings.activeRuntimePlatform;
        }
	}

    [FullInspector.InspectorCategory("Main")]
    [FullInspector.InspectorOrder(1), FullInspector.ShowInInspector]
    [FullInspector.InspectorHeader("Параметры Application"), FullInspector.InspectorMargin(16)]
	public AppInformation appInfo;

	#endregion
    
    #region Customize/AppSettings.MyApp.Nested.cs: MyApp Nested AppSettings. Just only specific types for MyApp
    /// <summary>
    /// see Customize/readme.txt
    /// </summary>
    #endregion

    #region Nested types

    #region Application information (App type, shortBundleId, productName, etc)

    [System.Serializable]
    public class AppInformation : FullInspector.BaseObject
    {


        enum AppConfigAssetsGroup
        {
            None,
            Icons,
            SplashImage,
            Manifest
        }

        string GetAppConfigUriPath(AppConfigAssetsGroup assetsGroup, bool commonConfig = false, string subPath = null )
        {
            string path = string.Empty;
#if UNITY_EDITOR
            var platform = MgUnityHelper.GetActiveBuildTargetGroup();
            path = appConfigFolderPath + System.Enum.GetName(typeof(UnityEditor.BuildTargetGroup), platform) + @"/";
            if (assetsGroup != AppConfigAssetsGroup.None)
                path = path + System.Enum.GetName(typeof(AppConfigAssetsGroup), assetsGroup) + @"/";                       
            if (!commonConfig) path = path + System.Enum.GetName(typeof(MageAppType), _activeAppType) + @"/";
            if (!string.IsNullOrEmpty(subPath)) path = path + subPath + @"/";

#endif
            return path;
        }
       
        [FullInspector.InspectorOrder(1.1), FullInspector.ShowInInspector]
        public string companyName
        {
            get 
            {
#if UNITY_EDITOR
                if (UnityEditor.PlayerSettings.companyName != AppConfig.defaultCompanyName)
                    UnityEditor.PlayerSettings.companyName = AppConfig.defaultCompanyName;                
#endif                
                return AppConfig.defaultCompanyName; 
            }
        }

        [SerializeField]
        [FullInspector.InspectorOrder(1.1), FullInspector.InspectorHidePrimary]
        private Dictionary<MageAppType, string> _productNames;        
        [FullInspector.InspectorOrder(1.11), FullInspector.ShowInInspector]
        public string productName
        {
            get
            {
                if (_productNames == null) _productNames = new Dictionary<MageAppType, string>();
                return _productNames.SafeGet(_activeAppType, AppConfig.defaultProductName);
            }
            set
            {
                if (_productNames == null) _productNames = new Dictionary<MageAppType, string>();
                _productNames.SafeSet<MageAppType, string>(_activeAppType, value);               
#if UNITY_EDITOR
                UnityEditor.PlayerSettings.productName = value;
#endif
            }
        }

        [SerializeField]
        [FullInspector.InspectorOrder(1.2),FullInspector.InspectorHidePrimary]
        private MageAppType _activeAppType = MageAppType.Full;
        [FullInspector.InspectorOrder(1.21), FullInspector.ShowInInspector, FullInspector.InspectorMargin(12)]
        public MageAppType activeAppType
        {
            get
            {
                return _activeAppType;
            }
            set
            {
                _activeAppType = value;
#if UNITY_EDITOR
                _ConfigAppTypeForActivePlatform();
#endif
            }
        }
#if UNITY_EDITOR
        private bool _ActiveInstanceIsPlatfornInstance()
        {             
            if (Selection.activeInstanceID != AppSettings.instance.GetInstanceID())
            {
                Debug.LogWarning("WARNING: ATTEMPT CONFIGURE NONE ACTIVE PLATFORM INSTANCE!");
                return false;
            }
            return true;
        }

        private void _ConfigAppTypeForActivePlatform()
        {
            if (!_ActiveInstanceIsPlatfornInstance()) return;

            UnityEditor.PlayerSettings.productName = productName;

            var platform = MgUnityHelper.GetActiveBuildTargetGroup();
            string path = GetAppConfigUriPath(AppConfigAssetsGroup.Icons);
            Texture2D[] txts = new Texture2D[1];

            txts[0] = UnityEditor.AssetDatabase.LoadAssetAtPath(path + "1024.png", typeof(Texture2D)) as Texture2D;

#if UNITY_ANDROID
			if(txts[0] == null || txts[0] == Texture2D.blackTexture || txts[0] == Texture2D.whiteTexture)
				txts[0] = UnityEditor.AssetDatabase.LoadAssetAtPath(path + "512.png", typeof(Texture2D)) as Texture2D;
#endif
            UnityEditor.PlayerSettings.SetIconsForTargetGroup(UnityEditor.BuildTargetGroup.Unknown, txts);

            var szIcons = UnityEditor.PlayerSettings.GetIconSizesForTargetGroup(platform);
            txts = new Texture2D[szIcons.Length];
            for (int i = 0; i < szIcons.Length; i++)
            {
                txts[i] = UnityEditor.AssetDatabase.LoadAssetAtPath(path + szIcons[i].ToString() + ".png",
                    typeof(Texture2D)) as Texture2D;
            }
            UnityEditor.PlayerSettings.SetIconsForTargetGroup(platform, txts);

#if UNITY_IOS || UNITY_TVOS
            UnityEditor.PlayerSettings.iOS.prerenderedIcon = true;
#endif
        //path = GetAppConfigUriPath(AppConfigAssetsGroup.SplashImage);
        //UnityEditor.PlayerSettings.showUnitySplashScreen = false; 

#if UNITY_ANDROID
                    /// Config manifest!
#if !MAGE_NO_PUSH
                    /// One Signal config
                    _ConfigPush(path);
#endif

//#if MAGE_OBB
                    /// OBB Downloader config
                    _ConfigOBBDownloader();
//#endif
#endif
    }

    private void _ConfigPush(string iconPath)
        {
//#if !MAGE_NO_PUSH
            string icon192 = iconPath + "192.png";
            AssetDatabase.DeleteAsset("Assets/Plugins/Android/OneSignalConfig/res/drawable-xxhdpi-v11/ic_onesignal_large_icon_default.png");
            AssetDatabase.Refresh();
            AssetDatabase.CopyAsset(icon192, "Assets/Plugins/Android/OneSignalConfig/res/drawable-xxhdpi-v11/ic_onesignal_large_icon_default.png");

            string pathOneSignalManifest = GetAppConfigUriPath(AppConfigAssetsGroup.None, true, "OneSignal") + "OneSignalManifest.xml";
            string oneSignalManifest = DLCUtils.LocalLoadStringData(pathOneSignalManifest);
            oneSignalManifest = oneSignalManifest.Replace("${manifestApplicationId}", bundleIdentifier);
            DLCUtils.LocalSaveStringData("Assets/Plugins/Android/OneSignalConfig/AndroidManifest.xml", oneSignalManifest);
            UnityEditor.AssetDatabase.Refresh(UnityEditor.ImportAssetOptions.ForceUpdate);
//#endif
        }

        private void _ConfigOBBDownloader()
        {
#if UNITY_ANDROID
            //========= ВКЛЮЧАЕМ И ПЕРЕКЛЮЧАЕМ OBB DOWNLOADER на нужный ключ!!! ===========
            if (UnityEditor.PlayerSettings.Android.useAPKExpansionFiles)
            {
                PrjSettings.instance.settings.activePlatformSettings.usingOBB = true;
                MgUnityHelper.UpdateScriptingDefineSymbols("MAGE_OBB_LITE", (_activeAppType == MageAppType.Lite));                
            }
            else
            {
                PrjSettings.instance.settings.activePlatformSettings.usingOBB = false;
                MgUnityHelper.RemoveScriptingDefineSymbols("MAGE_OBB_LITE", UnityEditor.BuildTargetGroup.Android);
            }
            //==================================================================
#endif
        }

#endif

    [System.Obsolete("use property activeAppType")]
        [FullInspector.InspectorHidePrimary]
        public MageAppType ActiveAppType
        {
            get
            {
                return _activeAppType;
            }
        }

        public string activeAppTypeStr
        {
            get
            {
                return System.Enum.GetName(typeof(MageAppType), _activeAppType);
            }
        }
        [System.Obsolete("use property activeAppTypeStr")]
        public string ActiveAppTypeStr
        {
            get
            {
                return System.Enum.GetName(typeof(MageAppType), _activeAppType);
            }
        }

        private System.Version GetVersion(string ver)
		{
			System.Version result = new System.Version(1, 0, 0, 1);
			if (System.Text.RegularExpressions.Regex.IsMatch(ver, bundleVersionPattern))
			{
				var match = System.Text.RegularExpressions.Regex.Match(ver, bundleVersionPattern);
				try
				{
					var version = new System.Version(match.Groups[0].Value);
					var major = version.Major < 0 ? 1 : version.Major;
					var minor = version.Minor < 0 ? 0 : version.Minor;
					var build = version.Build < 0 ? 0 : version.Build;
					var revision = version.Revision < 0 ? 0 : version.Revision;
					result = new System.Version(major, minor, build, revision);					
				}
				catch
				{ }
			}
			return result;
		}

		private int GetBundleCode(System.Version version)
		{
			return 1000 * (1000 * version.Major + 100 * version.Minor + version.Build) + version.Revision;	
		}

        [SerializeField]
        [FullInspector.InspectorOrder(1.3), FullInspector.InspectorHidePrimary]
        private Dictionary<MageAppType, string> _bundleVersions;     
        [FullInspector.InspectorOrder(1.31), FullInspector.ShowInInspector]// FullInspector.InspectorMargin(12)]
        public string bundleVersion
        {
            get
            {
                if (_bundleVersions == null) _bundleVersions = new Dictionary<MageAppType, string>();
#if UNITY_EDITOR
				var ver = _bundleVersions.SafeGet(_activeAppType, @"1.0.0.0");
				var version = GetVersion(ver);

#if UNITY_IOS || UNITY_TVOS
#if UNITY_4_6_9 || UNITY_4_7 || UNITY_4_7_1
					UnityEditor.PlayerSettings.shortBundleVersion = version.Major.ToString() + "." + version.Minor.ToString() + "." + version.Build.ToString();
					UnityEditor.PlayerSettings.bundleVersion = version.Revision.ToString();
#else
					//UnityEditor.PlayerSettings.bundleVersion = ver;
                    UnityEditor.PlayerSettings.bundleVersion = version.Major.ToString() + "." + version.Minor.ToString() + "." + version.Build.ToString();
                    PlayerSettings.iOS.buildNumber = version.Revision.ToString();
#endif
#endif

#if UNITY_ANDROID

                UnityEditor.PlayerSettings.Android.bundleVersionCode = GetBundleCode(version);
				UnityEditor.PlayerSettings.bundleVersion = version.Major.ToString() + "." + version.Minor.ToString() +
					"." + version.Build.ToString() + "." + version.Revision.ToString();
#if UNITY_4_6_9 || UNITY_4_7 || UNITY_4_7_1
				UnityEditor.PlayerSettings.shortBundleVersion = version.Major.ToString() + "." + version.Minor.ToString() + "." + version.Build.ToString();
#endif
#endif
                return ver;
#else
				return _bundleVersions.SafeGet(_activeAppType, @"1.0.0.0");
#endif
            }
            private set
            {
                if (_bundleVersions == null) _bundleVersions = new Dictionary<MageAppType, string>();
                _bundleVersions.SafeSet(_activeAppType, value);
				var bundleVer = value;
				var version = GetVersion(bundleVer);

#if UNITY_EDITOR // Editor

                //iOS
#if UNITY_IOS || UNITY_TVOS
#if UNITY_4_6_9 || UNITY_4_7 || UNITY_4_7_1
				UnityEditor.PlayerSettings.shortBundleVersion = version.Major.ToString() + "." + version.Minor.ToString() + "." + version.Build.ToString();
				UnityEditor.PlayerSettings.bundleVersion = version.Revision.ToString();                
               
#else
                UnityEditor.PlayerSettings.bundleVersion = _bundleVersions.SafeGet(_activeAppType, @"1.0.0.0");
#endif
#endif

                //Android
#if UNITY_ANDROID
                UnityEditor.PlayerSettings.Android.bundleVersionCode = GetBundleCode(version);
                UnityEditor.PlayerSettings.bundleVersion = version.Major.ToString() + "." + version.Minor.ToString() +
                    "." + version.Build.ToString() + "." + version.Revision.ToString();
#if UNITY_4_6_9 || UNITY_4_7 || UNITY_4_7_1
				UnityEditor.PlayerSettings.shortBundleVersion = version.Major.ToString() + "." + version.Minor.ToString() + "." + version.Build.ToString();
#endif
#endif

#endif //Editor
            }
        }      

        public void UpdateBuildVersion(string newVersion, bool updatePlayerSetings = true)
        {			
			bundleVersion = newVersion;
        }

        [SerializeField]
        [FullInspector.InspectorOrder(2.1), FullInspector.InspectorHidePrimary]
        private MageAppCategory _productCategory = MageAppCategory.None;
        [FullInspector.InspectorOrder(2.11), FullInspector.ShowInInspector, FullInspector.InspectorMargin(12), 
        FullInspector.InspectorDivider]
        public MageAppCategory productCategory
        {
            get { return _productCategory; }
            private set { _productCategory = value; }
        }

        [SerializeField]
        [FullInspector.InspectorOrder(3.1), FullInspector.InspectorHidePrimary]
        private Dictionary<MageAppType, string> _appIds;      
        [FullInspector.InspectorHeader("Application ID:")]
        [FullInspector.InspectorOrder(3.11), FullInspector.ShowInInspector, FullInspector.InspectorMargin(12), FullInspector.InspectorIndent,
        FullInspector.InspectorDivider]
        [FullInspector.InspectorTooltip("App ID приложения используется при поиске в AppStore, etc.")]
        public string appId
        {
            get
            {
#if UNITY_ANDROID
				return bundleIdentifier;				
#else
				if (_appIds == null) _appIds = new Dictionary<MageAppType, string>();
                return _appIds.SafeGet(_activeAppType, string.Empty);
#endif
			}
            private set
            {
#if !UNITY_ANDROID
				if (_appIds == null) _appIds = new Dictionary<MageAppType, string>();
                _appIds.SafeSet(_activeAppType, value);
#endif
            }
        }
        /// <summary>
        /// custom url scheme for call app into iOS
        /// </summary>       
        public string appUrlScheme
        {
#if UNITY_IOS || UNITY_TVOS
			get { return AppConfig.prefixCustomUrlApp + appId; }
#else
            get { return appId; }
#endif
        }

        [FullInspector.InspectorOrder(3.2), FullInspector.InspectorMargin(12), FullInspector.ShowInInspector, FullInspector.InspectorIndent]
        [FullInspector.InspectorComment(FullInspector.CommentType.Warning, "App ID полной версии приложения!")]
        [FullInspector.InspectorTooltip("Используется в Lite версии: GetFullVersion, etc.!")]
        public string fullVersionAppId
        {
            get
            {
#if UNITY_ANDROID
				return (activeAppType == MageAppType.Freemium) ? GetBundleIdentifier(MageAppType.Freemium) : GetBundleIdentifier(MageAppType.Full);
#else
				if (_appIds == null) return string.Empty;
                if (activeAppType == MageAppType.Freemium) return _appIds.SafeGet(MageAppType.Freemium, string.Empty);
                return _appIds.SafeGet(MageAppType.Full, string.Empty);
#endif
            }
            private set { }
        }

		[FullInspector.InspectorOrder(3.21), FullInspector.InspectorMargin(12), FullInspector.ShowInInspector, FullInspector.InspectorIndent]
		[FullInspector.InspectorComment(FullInspector.CommentType.Warning, "App ID лайт версии приложения!")]		
		public string liteVersionAppId
		{
			get
			{
#if UNITY_ANDROID
				return (activeAppType == MageAppType.Freemium) ? GetBundleIdentifier(MageAppType.Freemium) : GetBundleIdentifier(MageAppType.Lite);
#else
				if (_appIds == null) return string.Empty;
                if (activeAppType == MageAppType.Freemium) return _appIds.SafeGet(MageAppType.Freemium, string.Empty);
                return _appIds.SafeGet(MageAppType.Lite, string.Empty);
#endif
			}
			private set { }
		}

        public string GetFullVersionUri()
        {
#if UNITY_ANDROID			
	#if UNITY_EDITOR
			string uri = @"https://play.google.com/store/apps/details?id=" + GetBundleIdentifier(MageAppType.Full);
	#else
			string uri = @"market://details?id=" + GetBundleIdentifier(MageAppType.Full);
	#endif
#elif UNITY_WSA
            string uri = @"http://apps.microsoft.com/windows/app/" + GetBundleIdentifier(MageAppType.Full);
#else
            string uri = string.Format("https://itunes.apple.com/app/id{0}?ls=1&mt=8", fullVersionAppId);
#endif
			return uri;
        }

        public string GetUriInStore(string appId = null)
        {
            if (string.IsNullOrEmpty(appId)) appId = this.appId;
#if UNITY_ANDROID 

	#if UNITY_EDITOR
			string uri = @"https://play.google.com/store/apps/details?id=" + appId;
	#else
			string uri = @"market://details?id=" + appId;
	#endif

#elif UNITY_WSA
			string uri = @"http://apps.microsoft.com/windows/app/" + appId;
#else
            string uri = string.Format("https://itunes.apple.com/app/id{0}?ls=1&mt=8", appId);
#endif
			return uri;
        }

        //[SerializeField, FullInspector.InspectorHidePrimary]
        [System.NonSerialized]
        private string _fullVersionAppUrlScheme = string.Empty;
        [FullInspector.NotSerialized]
        public string fullVersionAppUrlScheme
        {
            get
			{
#if UNITY_IOS
                if (string.IsNullOrEmpty(_fullVersionAppUrlScheme) ||
                    !_fullVersionAppUrlScheme.StartsWith(AppConfig.prefixCustomUrlApp))
                    _fullVersionAppUrlScheme = AppConfig.prefixCustomUrlApp + fullVersionAppId;
#else
				if (string.IsNullOrEmpty(_fullVersionAppUrlScheme))
					_fullVersionAppUrlScheme = fullVersionAppId;
#endif
                return _fullVersionAppUrlScheme;
            }
        }

        /// <summary>
        /// Gets the URL for run full version app into iOS
        /// </summary>
        /// <returns>The custom URL full version app.</returns>
        public string GetFullVersionAppUrl()
        {
#if UNITY_IOS || UNITY_TVOS
			return fullVersionAppUrlScheme + @"://";
#else
            return fullVersionAppUrlScheme;
#endif
        }

		public string GetAppsDevUri()
		{
            return AppConfig.prefixAppsDevUri + AppConfig.idAppsDevUri;
		}

        [SerializeField]
        [FullInspector.InspectorOrder(3.3), FullInspector.InspectorHidePrimary]
        private Dictionary<MageAppType, string> _shortBundleIds;     
        [FullInspector.InspectorOrder(3.31), FullInspector.ShowInInspector, FullInspector.InspectorMargin(8), FullInspector.InspectorDivider]
        public string shortBundleId
        {
            get
            {
                if (_shortBundleIds == null) _shortBundleIds = new Dictionary<MageAppType, string>();
                return _shortBundleIds.SafeGet(_activeAppType, AppConfig.defaultProductName);
            }
            private set
            {
                if (_shortBundleIds == null) _shortBundleIds = new Dictionary<MageAppType, string>();
                _shortBundleIds.SafeSet(_activeAppType, value);
#if UNITY_EDITOR
                UnityEditor.PlayerSettings.bundleIdentifier = bundleIdentifier;
#endif
            }
        }

		public string fullVersionShortBundleId
		{
			get
			{
				if (_shortBundleIds == null) _shortBundleIds = new Dictionary<MageAppType, string>();
				if(_activeAppType == MageAppType.Freemium) return _shortBundleIds.SafeGet(_activeAppType, AppConfig.defaultProductName);
				return _shortBundleIds.SafeGet(MageAppType.Full, AppConfig.defaultProductName);
			}
		}

		public string liteVersionShortBundleId
		{
			get
			{
				if (_shortBundleIds == null) _shortBundleIds = new Dictionary<MageAppType, string>();
				return _shortBundleIds.SafeGet(MageAppType.Lite, AppConfig.defaultProductName);
			}
		}

		public bool IsCurrentAppProduct(string shortBundleId)
		{

			if (_shortBundleIds == null) _shortBundleIds = new Dictionary<MageAppType, string>();

			var bundleId = _shortBundleIds.SafeGet(MageAppType.Lite, AppConfig.defaultProductName);
			if(bundleId == shortBundleId) return true;

			bundleId = _shortBundleIds.SafeGet(MageAppType.Full, AppConfig.defaultProductName);
			if(bundleId == shortBundleId) return true;

			bundleId = _shortBundleIds.SafeGet(MageAppType.Freemium, AppConfig.defaultProductName);
			if(bundleId == shortBundleId) return true;

			return false;
		}

        [SerializeField]
        [FullInspector.InspectorOrder(3.35), FullInspector.InspectorHidePrimary]
        private string _prefixBundleId = AppConfig.defaultPrefixBundleId;

        [FullInspector.ShowInInspector]
        [FullInspector.InspectorOrder(3.36)]       
        public string prefixBundleId
        {
            get
            {
#if UNITY_EDITOR
                if (string.IsNullOrEmpty(_prefixBundleId))
                    _prefixBundleId = AppConfig.defaultPrefixBundleId;
#endif
                return _prefixBundleId;
            }
            private set
            {
                _prefixBundleId = value;
#if UNITY_EDITOR
                if (string.IsNullOrEmpty(_prefixBundleId))
                    _prefixBundleId = AppConfig.defaultPrefixBundleId;
#endif
            }
        }


        public string GetLowerInvariantPrefixBundleId()
        {
            return _prefixBundleId.ToLowerInvariant();
        }

        /// <summary>
        /// !Computed: BundleIdentifier = prefixBundleId + ShortBundleId;
        /// </summary>
        [FullInspector.NotSerialized]
        [FullInspector.InspectorHeader("Application Bundle Identifier:")]
        [FullInspector.InspectorOrder(3.4), FullInspector.InspectorMargin(12), FullInspector.ShowInInspector, FullInspector.InspectorIndent, FullInspector.InspectorDivider]
        [FullInspector.InspectorTooltip("!C BundleIdentifier = prefixBundleId + shortBundleId")]
        public string bundleIdentifier
        {
            get
            {                
                var s = shortBundleId;// _shortBundleIds.SafeGet(_activeAppType, string.Empty);
                if (string.IsNullOrEmpty(s)) s= prefixBundleId + ".*";
                else s= prefixBundleId + "." + s;
#if UNITY_EDITOR
                if (UnityEditor.PlayerSettings.bundleIdentifier!=s)
                    UnityEditor.PlayerSettings.bundleIdentifier = s;
#endif
                return s;
            }
        }

        public string GetBundleIdentifier(MageAppType appType)
        {
            if (_shortBundleIds == null) return _prefixBundleId + ".*";// for prevent exception in FI 2.6+

            var s = _shortBundleIds.SafeGet(appType, string.Empty);
            if (string.IsNullOrEmpty(s)) return _prefixBundleId + ".*";
            else return _prefixBundleId + "." + s;
        }

		#region Android
        [FullInspector.NotSerialized]
		public bool isAndroid 
		{ 
			get 
			{ 
#if UNITY_ANDROID
				return true;
#else
				return false;
#endif				
			} 
		}

        [FullInspector.NotSerialized]
        public bool isAndroidOBB 
		{ 
			get 
			{ 
#if UNITY_ANDROID && MAGE_OBB
				return true;
#else
				return false;
#endif			
			} 
		}

#if UNITY_EDITOR

#endif
        [FullInspector.ShowInInspector]
		[FullInspector.InspectorOrder(7), FullInspector.InspectorShowIf("isAndroid")]
		[FullInspector.InspectorHeader("Android settings"), FullInspector.InspectorMargin(12), FullInspector.InspectorDivider]
		[FullInspector.InspectorComment(FullInspector.CommentType.Info,"Магазин распространения:")]
		public MageAppAndroidStore androidStore = MageAppAndroidStore.GooglePlayMarket;

        [FullInspector.InspectorOrder(7.1), FullInspector.InspectorMargin(12)]//,FullInspector.InspectorTextArea(32f)]
        [FullInspector.InspectorShowIf("isAndroid")]
        [FullInspector.InspectorTooltip("public key from the Android developer portal")]
        public string publicKey = string.Empty;

        [SerializeField, FullInspector.InspectorHidePrimary]
		private Dictionary<MageAppType, bool> _splitBinary;
		[FullInspector.ShowInInspector]
		[FullInspector.InspectorHeader("APK Expansion"), FullInspector.InspectorMargin(10)]
		[FullInspector.InspectorOrder(7.2), FullInspector.InspectorShowIf("isAndroid")]		
		public bool splitBinary
		{
			get
			{
				if(_splitBinary == null) {
					_splitBinary = new Dictionary<MageAppType, bool>() {						
						{MageAppType.Full, false},
						{MageAppType.Lite, false},
						{MageAppType.Freemium, false}						
					};
				}

				var result = _splitBinary.SafeGet(_activeAppType, false);// && isAndroidOBB;
#if UNITY_EDITOR
                if(_ActiveInstanceIsPlatfornInstance())
                {
                    UnityEditor.PlayerSettings.Android.useAPKExpansionFiles = result;
                    _ConfigOBBDownloader();
                }
#endif
				return result;
			}
			private set
			{
				if (_splitBinary == null) {
					_splitBinary = new Dictionary<MageAppType, bool>() {						
						{MageAppType.Full, false},
						{MageAppType.Lite, false},
						{MageAppType.Freemium, false}						
					};
				}
				
				_splitBinary.SafeSet(_activeAppType, value);
#if UNITY_EDITOR
                if (_ActiveInstanceIsPlatfornInstance())
                {
                    UnityEditor.PlayerSettings.Android.useAPKExpansionFiles = value;
                    _ConfigOBBDownloader();
                }
#endif
			}
		}		

		[SerializeField, FullInspector.InspectorHidePrimary]
		private Dictionary<MageAppType, bool> _usePatchOBB;
		[FullInspector.ShowInInspector]
		[FullInspector.InspectorOrder(7.21), FullInspector.InspectorShowIf("isAndroidOBB")]
		public bool usePatchOBB
		{
			get
			{
				if(_usePatchOBB == null) {
					_usePatchOBB = new Dictionary<MageAppType, bool>() {						
						{MageAppType.Full, false},
						{MageAppType.Lite, false},
						{MageAppType.Freemium, false}						
					};
				}

				var result =_usePatchOBB.SafeGet(_activeAppType, false);

				return result && splitBinary;
			}
			private set
			{
				if (_usePatchOBB == null) {
					_usePatchOBB = new Dictionary<MageAppType, bool>() {						
						{MageAppType.Full, false},
						{MageAppType.Lite, false},
						{MageAppType.Freemium, false}						
					};
				}	
				var flag = value;
				_usePatchOBB.SafeSet(_activeAppType, flag);
			}
		}
		#endregion

	}
    #endregion

    #endregion
}

