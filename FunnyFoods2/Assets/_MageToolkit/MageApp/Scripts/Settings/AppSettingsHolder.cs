﻿using UnityEngine;
using System.Collections;
using FullInspector;

public class AppSettingsHolder : FullInspector.BaseBehavior<FullInspector.FullSerializerSerializer> 
{
    [FullInspector.InspectorTooltip("Ассет для настройки параметров приложения")]	
	public AppSettings appSettings;

	protected override void Awake ()
	{
		base.Awake ();
		if(appSettings == null) appSettings = AppSettings.instance;
    }
}
