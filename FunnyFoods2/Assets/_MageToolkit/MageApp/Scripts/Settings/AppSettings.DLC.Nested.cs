﻿using UnityEngine;
using System.Collections.Generic;

// Настройки модулей приложения.
// Реализация как Nested классы класса AppSettings
public partial class AppSettings
{
    /// <summary>
    /// Downloadable content (DLC) engine settings.
    /// </summary>  
    [FullInspector.InspectorCategory("DLC")]
    [FullInspector.InspectorOrder(10), FullInspector.ShowInInspector, FullInspector.InspectorMargin(20)]
    [FullInspector.InspectorHeader("Параметры загружаемого контента (DLC)")]
    public DLCSettings DLC;

    /// <summary>
    /// Cross-promotion settings
    /// </summary>
    [FullInspector.InspectorCategory("DLC")]
    [FullInspector.InspectorOrder(10.1), FullInspector.ShowInInspector, FullInspector.InspectorMargin(20)]
    [FullInspector.InspectorHeader("Параметры конфигурации cross-promotion (CP)")]
    public CPSettings CP;

    #region Nested types

    #region DLCSettings: Параметры конфигурации загружаемого контента (DLC - Downloadable content)
    [System.Serializable]
    public class DLCSettings : FullInspector.BaseObject
    {
        [FullInspector.InspectorOrder(2)]
        [FullInspector.ShowInInspector]
        public string suffixContentUri
        {
            get { return AppConfig.defaultSuffixContentUri; }
        }
    }

    #endregion

    #region CPSettings: Параметры конфигурации Cross-promotion
    [System.Serializable]
    public class CPSettings : FullInspector.BaseObject
    {
        [FullInspector.InspectorOrder(1),
        FullInspector.InspectorComment(FullInspector.CommentType.Warning, "TestMode: тестовый режим.")]
        public bool testMode = false;

        [FullInspector.InspectorOrder(2),
        FullInspector.InspectorComment(FullInspector.CommentType.None, "!D TestMode: Сброс сохраненных настроек.")]
        [FullInspector.InspectorTooltip("Применяется при testMode == true. Если true то сбрасывает сохраненные настройки!")]
        [FullInspector.InspectorShowIf("testMode")]
        public bool resetActiveUpdate = true;

        [FullInspector.InspectorOrder(4),
        FullInspector.InspectorComment(FullInspector.CommentType.None, "Индекс сцены с которой отключается фоновая загрузка контента."),
        FullInspector.InspectorTooltip("Индекс сцены с которой отключается фоновая загрузка контента. Если -1, то работает всегда.")]
        public int denyRunUpdateFromLevel = -1;

        [FullInspector.InspectorOrder(5)]
        public MageAppUIViewMode UIViewMode = MageAppUIViewMode.CrossPromotionView;//NoneView because bug into FI 

        [FullInspector.InspectorOrder(5.2),
        FullInspector.InspectorComment(FullInspector.CommentType.Warning, "alwaysLandscape: в проекте только префаб для landscape UIView")]
        public bool alwaysLandscape = false;

        [SerializeField]
        [FullInspector.InspectorOrder(5.5),FullInspector.InspectorHidePrimary]
        private Dictionary<MageAppUILang, string> _uriPrefabs;
        [FullInspector.InspectorOrder(5.51),FullInspector.ShowInInspector]
        public Dictionary<MageAppUILang, string> UriPrefabs
        {
            get
            {
                if (_uriPrefabs == null || _uriPrefabs.Count < 1)
                {
                    _uriPrefabs = new Dictionary<MageAppUILang, string>()
                    {        
						{MageAppUILang.en, @"UI/CPUIViewControl"},
                        {MageAppUILang.ru, @"UI/CPUIViewControl"}
                    };
                }
                return _uriPrefabs;
            }
            set { _uriPrefabs = value; }
        }

		[FullInspector.InspectorOrder(6.0),
		FullInspector.InspectorComment(FullInspector.CommentType.None, "Пауза перед завершением задачи загрузки изображений. (>=0.25 sec)")]
		public float waitBeforeCompleteImagesLoading = 0.25f;

		[FullInspector.InspectorOrder(0.1),
		FullInspector.InspectorComment(FullInspector.CommentType.Warning, "Отключить кросс промоушен")]		
		public bool disable = false;
    }
    #endregion

    #endregion
}
