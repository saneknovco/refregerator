﻿using UnityEngine;
using System;
using System.Collections.Generic;
#if UNITY_EDITOR
using UnityEditor;
#endif

/// <summary>
/// Настройки проекта: brand, плагины, отладочный режим. 
/// Cписок настраиваемых define symbols смотри в AppConfig.cs
/// </summary>
public class PrjSettings : ScriptableObject
{
    #region UNITY EDITOR

    #if UNITY_EDITOR

    [UnityEditor.MenuItem("Assets/Create/Mage/PrjSettings")]
    public static void CreateAsset()
    {
        ScriptableObjectUtility.CreateAsset<PrjSettings>();
    }   

#endif

    #endregion

    #region  static

    const string assetName = @"PrjSettings";
    const string subPath = @"Settings/";    

    private static PrjSettings _instance;    
    public static PrjSettings instance
    {
        get
        {
            try
            {
                if (_instance == null) _instance = MgUnityHelper.CreateFromResources<PrjSettings>(subPath, assetName);
            }
            catch
            {
                _instance = ScriptableObject.CreateInstance<PrjSettings>();
            }
            return _instance;
        }
    }

    public static string GetAssetNameToActivePlatform(string assetName)
    {
        var prjSettings = instance.settings;
        return assetName + "_" + prjSettings.activeRuntimePlatformStr + "_" + prjSettings.activePlatformSettings.appStore;
    }
    #endregion
    [FullInspector.InspectorOrder(2)]
    [Header("Параметры настройки проетка")]
    public PrjSettingsContainer settings;

    #region Nested types

    [System.Serializable]
    public class PrjSettingsContainer : FullInspector.BaseObject
    {
#if UNITY_EDITOR
        void RefreshValuesProperies(System.Object obj)
        {
            Type objType = obj.GetType();
            var collection = objType.GetProperties();
            List<FullInspector.InspectedMember> members = new List<FullInspector.InspectedMember>();

            foreach (var item in collection)
                members.Add(new FullInspector.InspectedMember(new FullInspector.InspectedProperty(item)));            
            FullInspector.InspectedType.StableSort(members, (a, b) => {
                return Math.Sign(FullInspector.InspectorOrderAttribute.GetInspectorOrder(a.MemberInfo) -
                    FullInspector.InspectorOrderAttribute.GetInspectorOrder(b.MemberInfo));
            });
            // call method Set in properies!!!
            foreach (var item in members)
                item.Property.Write(obj, item.Property.Read(obj));            
        }

        /// <summary>
        /// Обновления зависимостей в проекте: Scripting Define Symbols, etc.
        /// </summary>
        [FullInspector.InspectorOrder(1.5)]
        [FullInspector.InspectorButton]
        [FullInspector.InspectorName("Refresh Settings")]
        void RefreshValueSettings()
        {            
            brand = _brand;
            RefreshValuesProperies(activePlatformSettings);
        }    
#endif

        [SerializeField]
        [FullInspector.InspectorOrder(1), FullInspector.InspectorHidePrimary]
        MageAppBrand _brand = MageAppBrand.MAGE;
        [FullInspector.InspectorOrder(1.1), FullInspector.ShowInInspector]
        public MageAppBrand brand
        {
            get
            {
                return _brand;
            }
            set
            {
                _brand = value;
#if UNITY_EDITOR
                switch (_brand)
                {
                    case MageAppBrand.MAGE:
                        MgUnityHelper.UpdateScriptingDefineSymbols("MAGE", true);
                        MgUnityHelper.UpdateScriptingDefineSymbols("BINI", false);
                        MgUnityHelper.UpdateScriptingDefineSymbols("WOOOW", false);
                        
                        break;
                    case MageAppBrand.BINI:                        
                        MgUnityHelper.UpdateScriptingDefineSymbols("MAGE", false);
                        MgUnityHelper.UpdateScriptingDefineSymbols("BINI", true);
                        MgUnityHelper.UpdateScriptingDefineSymbols("WOOOW", false);
                        
                        break;
                    case MageAppBrand.WOOOW:
                        MgUnityHelper.UpdateScriptingDefineSymbols("MAGE", false);
                        MgUnityHelper.UpdateScriptingDefineSymbols("BINI", false);                        
                        MgUnityHelper.UpdateScriptingDefineSymbols("WOOOW", true);                        
                        break;                  
                    case MageAppBrand.PUBLISHER:
                        MgUnityHelper.UpdateScriptingDefineSymbols("MAGE", false);
                        MgUnityHelper.UpdateScriptingDefineSymbols("BINI", false);                        
                        MgUnityHelper.UpdateScriptingDefineSymbols("WOOOW", false);
                        break;
                }
#endif
            }
        }

        [FullInspector.InspectorOrder(1.2)]
        [FullInspector.NotSerialized, FullInspector.ShowInInspector]
        public RuntimePlatform activeRuntimePlatform
        {
            get
            {
                return MgUnityHelper.GetRuntimePlatform();
            }
        }

        [FullInspector.NotSerialized]
        public string activeRuntimePlatformStr
        {
            get
            {
                return System.Enum.GetName(typeof(RuntimePlatform), activeRuntimePlatform);
            }
        }
       
        [SerializeField]
        [FullInspector.InspectorOrder(2), FullInspector.InspectorHidePrimary]
        private Dictionary<RuntimePlatform, PrjPlatformSettings> _platformSettings = null;
        
        [FullInspector.InspectorOrder(2.1), FullInspector.ShowInInspector]
        [FullInspector.InspectorHeader("Platform dependency settings")]
        [FullInspector.InspectorMargin(16), FullInspector.InspectorDivider]
        public  PrjPlatformSettings activePlatformSettings
        {
            get
            {
                var platform = activeRuntimePlatform;
                if (_platformSettings == null) _platformSettings = new Dictionary<RuntimePlatform, PrjPlatformSettings>()
                    {
                        { platform, _defaultSettings.ShallowCopy()}
    			    };                
                if (!_platformSettings.ContainsKey(platform))
                    _platformSettings.Add(platform, _defaultSettings.ShallowCopy());

                return _platformSettings[platform];
            }
            private set
            {
                var platform = activeRuntimePlatform;
                if (_platformSettings == null) _platformSettings = new Dictionary<RuntimePlatform, PrjPlatformSettings>()
                    {
                        { platform,  _defaultSettings.ShallowCopy()}
                    };
                _platformSettings.SafeSet(platform, value);
            }
        }

        #region Default settings
        [SerializeField]
        [FullInspector.InspectorOrder(0.05)]
        [FullInspector.InspectorMargin(16), FullInspector.InspectorHidePrimary]
        private PrjPlatformSettings _defaultSettings = new PrjPlatformSettings();

#if UNITY_EDITOR
        [FullInspector.InspectorOrder(2.3)]
        [FullInspector.InspectorButton]
        [FullInspector.InspectorName("Apply default settings")]        
        void ApplyDefaultSettings()
        {
            activePlatformSettings = _defaultSettings.ShallowCopy();
            RefreshValueSettings();
        }

        [FullInspector.InspectorOrder(2.4)]
        [FullInspector.InspectorButton]
        [FullInspector.InspectorName("Set as default settings")]        
        void SetDefaultSettings()
        {
            _defaultSettings = activePlatformSettings.ShallowCopy();
        }
#endif
        #endregion

        #region Manage App settings

#if UNITY_EDITOR
        [FullInspector.InspectorOrder(3.1)]
        [FullInspector.InspectorButton]
        [FullInspector.InspectorName("Create AppSettings to active platform")]       
        void CreateAppSettings()
        {
            AppSettings.CreateAsset();
        }

        [FullInspector.InspectorOrder(3.2)]
        [FullInspector.InspectorButton]
        [FullInspector.InspectorName("Create InAppProducts to active platform")]        
        void CreateInAppProducts()
        {
            InAppProducts.CreateAsset();
        }
#endif

        #endregion

    }
    #region parametrs project settings
    [System.Serializable]
    public class PrjPlatformSettings : FullInspector.BaseObject
    {
        public PrjPlatformSettings ShallowCopy()
        {
            return (PrjPlatformSettings)MemberwiseClone();
        }

        [SerializeField]
        [FullInspector.InspectorOrder(1),FullInspector.InspectorHidePrimary]
        MageAppStore _appStore = MageAppStore.Default;
        [FullInspector.InspectorOrder(1.1), FullInspector.ShowInInspector]
        public MageAppStore appStore
        {
            get
            {
                return _appStore;
            }
            set
            {
                _appStore = value;
            }
        }

        [SerializeField, FullInspector.InspectorOrder(2), FullInspector.InspectorHidePrimary]
        bool _usingUnityAnalytics = false;        
        [FullInspector.ShowInInspector]
        [FullInspector.InspectorOrder(2.1)]
        [FullInspector.InspectorHeader("Unity packages"), FullInspector.InspectorMargin(4)]
        [FullInspector.InspectorTooltip(@"MAGE_ANALYTICS - Включение/Отключение Unity Analytics")]
        public bool usingUnityAnalytics
        {
            get
            {
                return _usingUnityAnalytics;
            }
            set
            {
                _usingUnityAnalytics = value;
#if UNITY_EDITOR
                MgUnityHelper.UpdateScriptingDefineSymbols("MAGE_ANALYTICS", value);
                // https://feedback.unity3d.com/suggestions/programmatically-disable-submit-hw-statistics
                // WE WANT PlayerSettings.SubmitHWStatistics = false; !!!
#endif
            }
        }

        [SerializeField]
        [FullInspector.InspectorOrder(3), FullInspector.InspectorHidePrimary]
        bool _usingInputMT = true;
        [FullInspector.InspectorOrder(3.1)]
        [FullInspector.ShowInInspector]        
        [FullInspector.InspectorHeader("Mage packages"), FullInspector.InspectorMargin(8)]          
        [FullInspector.InspectorTooltip("MAGE_MT - InputMT для поддержки мультитач в проектах без мультитач")]
        public bool usingInputMT
        {
            get
            {
                return _usingInputMT;
            }
            set
            {
                _usingInputMT = value;
#if UNITY_EDITOR
                MgUnityHelper.UpdateScriptingDefineSymbols("MAGE_MT", value);
#endif
            }
        }
        
        [SerializeField]
        [FullInspector.InspectorOrder(4), FullInspector.InspectorHidePrimary]
        bool _usingIAP = false;
        [FullInspector.InspectorOrder(4.1)]
        [FullInspector.ShowInInspector]        
        [FullInspector.InspectorHeader("3rd party packages"), FullInspector.InspectorMargin(8)]
        [FullInspector.InspectorName("using IAP")]
        public bool usingIAP
        {
            get
            {
                return _usingIAP;
            }
            set
            {
                _usingIAP = value;
#if UNITY_EDITOR
                MgUnityHelper.UpdateScriptingDefineSymbols("MAGE_NO_IAP", !value);
#endif
            }
        }

        [SerializeField]
        [FullInspector.InspectorOrder(5), FullInspector.InspectorHidePrimary]
        bool _usingPlayMaker = true;
        [FullInspector.InspectorOrder(5.1)]
        [FullInspector.ShowInInspector]        
        public bool usingPlayMaker
        {
            get
            {
                return _usingPlayMaker;
            }
            set
            {
                _usingPlayMaker = value;
#if UNITY_EDITOR
                MgUnityHelper.UpdateScriptingDefineSymbols("MAGE_NO_PM", !value);
#endif
            }
        }

        [SerializeField]
        [FullInspector.InspectorOrder(6), FullInspector.InspectorHidePrimary]
        bool _usingUni2d = true;
        [FullInspector.InspectorOrder(6.1)]
        [FullInspector.ShowInInspector]        
        public bool usingUni2d
        {
            get
            {
                return _usingUni2d;
            }
            set
            {
                _usingUni2d = value;
#if UNITY_EDITOR
                MgUnityHelper.UpdateScriptingDefineSymbols("MAGE_NO_UNI2D", !value);
#endif
            }
        }

        [SerializeField]
        [FullInspector.InspectorOrder(7), FullInspector.InspectorHidePrimary]
        bool _usingPush = true;
        [FullInspector.InspectorOrder(7.1)]
        [FullInspector.ShowInInspector]        
        public bool usingPush
        {
            get
            {
                return _usingPush;
            }
            set
            {
                _usingPush = value;
#if UNITY_EDITOR
                MgUnityHelper.UpdateScriptingDefineSymbols("MAGE_NO_PUSH", !value);
#endif
            }
        }

        [SerializeField]
        [FullInspector.InspectorOrder(7.5), FullInspector.InspectorHidePrimary]
        bool _usingGalleryScreenshot = false;
        [FullInspector.InspectorOrder(7.51)]
        [FullInspector.ShowInInspector]
        public bool usingGalleryScreenshot
        {
            get
            {
                return _usingGalleryScreenshot;
            }
            set
            {
                _usingGalleryScreenshot = value;
#if UNITY_EDITOR
                MgUnityHelper.UpdateScriptingDefineSymbols("MAGE_GS", value);
#endif
            }
        }

        [SerializeField]
        [FullInspector.InspectorOrder(8), FullInspector.InspectorHidePrimary]
        bool _usingOBB = false;
        [FullInspector.ShowInInspector]
        [FullInspector.InspectorOrder(8.1)]      
        [FullInspector.InspectorName("using OBB")]
        public bool usingOBB
        {
            get
            {
                return _usingOBB;
            }
            set
            {
                _usingOBB = value;
#if UNITY_EDITOR
                MgUnityHelper.UpdateScriptingDefineSymbols("MAGE_OBB", value);
#endif
            }
        }

        [SerializeField]
        [FullInspector.InspectorOrder(20), FullInspector.InspectorHidePrimary]
        bool _fileLog = false;
        [FullInspector.InspectorOrder(20.1)]
        [FullInspector.ShowInInspector]
        [FullInspector.InspectorHeader("Debug settings"), FullInspector.InspectorMargin(8)]        
        [FullInspector.InspectorTooltip(@"MAGE_FILE_LOG - запись сообщений unity console в файл Application.persistentDataPath/AppLogs.txt")]
        public bool fileLog
        {
            get
            {
                return _fileLog;
            }
            set
            {
                _fileLog = value;
#if UNITY_EDITOR
                MgUnityHelper.UpdateScriptingDefineSymbols("MAGE_FILE_LOG", value);
#endif
            }
        }

        [SerializeField, FullInspector.InspectorOrder(21), FullInspector.InspectorHidePrimary]
        bool _mageDebug = false;
        [FullInspector.InspectorOrder(21.1)]
        [FullInspector.ShowInInspector]        
        [FullInspector.InspectorTooltip("MAGE_DEBUG - расширенный вывод сообщений в консоль")]
        public bool mageDebug
        {
            get
            {
                return _mageDebug;
            }
            set
            {
                _mageDebug = value;
#if UNITY_EDITOR
                MgUnityHelper.UpdateScriptingDefineSymbols("MAGE_DEBUG", value);
#endif
            }
        }

        [SerializeField, FullInspector.InspectorOrder(22), FullInspector.InspectorHidePrimary]
        bool _IAPDebug = false;
        [FullInspector.InspectorOrder(22.1)]
        [FullInspector.ShowInInspector]        
        [FullInspector.InspectorTooltip("MAGE_IAP_DEBUG - тестовый режим для проверки IAP")]
        [FullInspector.InspectorName("IAP Debug")]
        public bool IAPDebug
        {
            get
            {
                return _IAPDebug;
            }
            set
            {
                _IAPDebug = value;
#if UNITY_EDITOR
                MgUnityHelper.UpdateScriptingDefineSymbols("MAGE_IAP_DEBUG", value);
#endif
            }
        }

        [SerializeField, FullInspector.InspectorOrder(22), FullInspector.InspectorHidePrimary]
        bool _DLCDebug = false;
        [FullInspector.InspectorOrder(22.1)]
        [FullInspector.ShowInInspector]       
        [FullInspector.InspectorTooltip("MAGE_DLC_DEBUG - тестовый режим для проверки DLC")]
        [FullInspector.InspectorName("DLC Debug")]
        public bool DLCDebug
        {
            get
            {
                return _DLCDebug;
            }
            set
            {
                _DLCDebug = value;
#if UNITY_EDITOR
                MgUnityHelper.UpdateScriptingDefineSymbols("MAGE_DLC_DEBUG", value);
#endif
            }
        }

        [SerializeField, FullInspector.InspectorOrder(23), FullInspector.InspectorHidePrimary]
        bool _pushDebug = false;
        [FullInspector.ShowInInspector]
        [FullInspector.InspectorOrder(23.1)]
        [FullInspector.InspectorTooltip("MAGE_PUSH_DEBUG - тестовый режим для проверки push notification")]
        public bool pushDebug
        {
            get
            {
                return _pushDebug;
            }
            set
            {
                _pushDebug = value;
#if UNITY_EDITOR
                MgUnityHelper.UpdateScriptingDefineSymbols("MAGE_PUSH_DEBUG", value);
#endif
            }
        }
    }
    #endregion

    #endregion
}