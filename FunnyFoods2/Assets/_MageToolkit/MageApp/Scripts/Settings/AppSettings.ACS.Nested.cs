﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

// ==========================================
// Applications Communication Services (ACS):
// 1. Push
// 2. App Links (саязанные приложения)
// 3. Unity Analytics // not implemenation
// ==========================================
public partial class AppSettings
{
	#region Push Notification Service
	[SerializeField]
	[FullInspector.InspectorHidePrimary]
	[FullInspector.InspectorKeyWidth(0.1f)]
	private Dictionary<MageAppType, PNSSettings> _PNSList;
	/// <summary>
	/// Push Notification Service (PNS) settings for current appType.
	/// </summary>  	
	public PNSSettings PNS
	{
		get
		{
			if (_PNSList == null) _PNSList = new Dictionary<MageAppType, PNSSettings>()
				{
                    {MageAppType.Freemium, new PNSSettings()},
                    {MageAppType.Full, new PNSSettings()},
					{MageAppType.Lite, new PNSSettings()}					
				};
			var appType = appInfo.activeAppType;
			if (!_PNSList.ContainsKey(appType)) _PNSList.Add(appType, new PNSSettings());
			return _PNSList[appType];
		}
	}
	/// <summary>
	/// Push Notification Services (PNS) settings for all appType.
	/// </summary> 	
	[FullInspector.InspectorCategory("ACS")]
	[FullInspector.InspectorOrder(20),FullInspector.ShowInInspector, FullInspector.InspectorMargin(20)]
	[FullInspector.InspectorHeader("Push Notification Service (PNS)")]
	[FullInspector.InspectorKeyWidth(0.1f)]
	public Dictionary<MageAppType, PNSSettings> PNSList
	{
		get
		{
			if (_PNSList == null) _PNSList = new Dictionary<MageAppType, PNSSettings>()
			{
                {MageAppType.Freemium, new PNSSettings()},
                {MageAppType.Full, new PNSSettings()},
				{MageAppType.Lite, new PNSSettings()}
			};		
			return _PNSList;
		}
		set
		{
			if (value == null)
			{
				if (_PNSList == null) _PNSList = new Dictionary<MageAppType, PNSSettings>()
				{
                    {MageAppType.Freemium, new PNSSettings()},
                    {MageAppType.Full, new PNSSettings()},
					{MageAppType.Lite, new PNSSettings()}
				};
			}else _PNSList = value;
		}
	}

	#endregion

	#region App Domain
	[SerializeField]
	[FullInspector.InspectorHidePrimary]
	[FullInspector.InspectorKeyWidth(0.1f)]
	private Dictionary<string, AppDomainSettings> _appDomains;
	/// <summary>
	/// AppLinks for AppDomain.
	/// </summary> 
	[FullInspector.InspectorCategory("ACS")]
	[FullInspector.InspectorOrder(20.1), FullInspector.ShowInInspector, FullInspector.InspectorMargin(20)]
	[FullInspector.InspectorHeader("AppLinks для AppDomain активной платформы исполнения")]
	[FullInspector.InspectorKeyWidth(0.1f)]
	public Dictionary<string, AppDomainSettings> appDomain
	{
		get
		{
			if (_appDomains == null) _appDomains = new Dictionary<string, AppDomainSettings>();			
            return _appDomains;
		}
		set
		{
			if (value == null)
			{
				if (_appDomains == null) _appDomains = new Dictionary<string, AppDomainSettings>();
			}
			else
			{
				_appDomains= value;
			}
		}
	}
	[FullInspector.InspectorCategory("ACS")]
	[FullInspector.InspectorOrder(20.2), FullInspector.ShowInInspector]
	[FullInspector.InspectorTooltip("При выходе из фона обновлять статус AppLinks")]
	public bool refreshAppLinksInResume = true;

	#endregion

	#region Nested types

	#region  PNSSettings: Параметры конфигурации Push Notification Service
	/// <summary>
	/// класс параметров конфигурации Push Notification Service (PNS)
	/// </summary>
	[System.Serializable]
	public class PNSSettings : FullInspector.BaseObject
	{
		public bool enabled = false;
		[FullInspector.InspectorShowIf("enabled")]
		public string appId;
		[FullInspector.InspectorShowIf("enabled")]
		public string googleProjectNumber;
		//[FullInspector.InspectorShowIf("enabled")]
		//[FullInspector.InspectorTooltip("Показывать Push сообщения внутри приложения(когда приложение активно)")]
		//public bool inAppAlertNotification = false;
		
	}
	#endregion

	#region  AppDomainSettings: Параметры конфигурации Applications Domain

	/// <summary>
	/// параметры App Link
	/// </summary>
	[System.Serializable]
	public class AppLinkSettings : FullInspector.BaseObject
	{
		public string appId;
	}

	/// <summary>
	/// параметры конфигурации Application Domain
	/// </summary>
	[System.Serializable]
	public class AppDomainSettings : FullInspector.BaseObject
	{
		[SerializeField]
		[FullInspector.InspectorHidePrimary]
		[FullInspector.InspectorKeyWidth(0.1f)]
		private Dictionary<MageAppType, AppLinkSettings> _appLinks;

		/// <summary>
		/// Настройки App Links для AppDomain.
		/// </summary> 		
		[FullInspector.ShowInInspector, FullInspector.InspectorMargin(20)]
		[FullInspector.InspectorKeyWidth(0.1f)]
		[FullInspector.InspectorHeader("Список AppLinks для AppDomain")]
		public Dictionary<MageAppType, AppLinkSettings> appLinks
		{
			get
			{
				if (_appLinks == null) _appLinks = new Dictionary<MageAppType, AppLinkSettings>()
				{
                    {MageAppType.Freemium, new AppLinkSettings()},
                    { MageAppType.Full, new AppLinkSettings()},
					{MageAppType.Lite, new AppLinkSettings()}					
				};
				return _appLinks;
			}
			set
			{
				if (value == null)
				{
					if (_appLinks == null) _appLinks = new Dictionary<MageAppType, AppLinkSettings>()
					{
                        {MageAppType.Freemium, new AppLinkSettings()},
                        {MageAppType.Full, new AppLinkSettings()},
						{MageAppType.Lite, new AppLinkSettings()}						
					};
				}
				else _appLinks = value;
			}
		}
	}
	#endregion

	#endregion
}