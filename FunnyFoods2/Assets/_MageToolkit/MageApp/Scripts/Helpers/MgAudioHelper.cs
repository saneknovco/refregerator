﻿using UnityEngine;

static public class MgAudioHelper
{
	private const string defAudioObjectName = AppConfig.prefixAutoObject + @"AudioClick";//"auto_AudioClick"

	private static AudioSource _audioSourceClick = null;

    private static bool _InitAudioClick(string audioObjectName = defAudioObjectName, 
		string audioClip = "")
	{
        if (string.IsNullOrEmpty(audioClip))
            audioClip = AppSettings.instance.UI.UriClick;
        var go = GameObject.Find(audioObjectName);
		if (go == null)
		{
			go = new GameObject(audioObjectName, typeof(AudioSource));
            GameObject.DontDestroyOnLoad(go);
			_audioSourceClick = go.GetComponent<AudioSource>();
			_audioSourceClick.playOnAwake = false;
			_audioSourceClick.rolloffMode = AudioRolloffMode.Custom;
			_audioSourceClick.maxDistance = 1000000.0f;//for click !!!
			_audioSourceClick.clip = Resources.Load<AudioClip>(audioClip);
		}
		else _audioSourceClick = go.GetComponent<AudioSource>();

		return _audioSourceClick != null;
	}
	public static bool InitAudioClick(GameObject audioObject)
	{
		if (audioObject) _audioSourceClick = audioObject.GetComponent<AudioSource>();
		else _InitAudioClick();
		
		return _audioSourceClick != null;
	}

	public static void ResetAudioClick()
	{
		_audioSourceClick = null;
	}

	public static void Click(float volume = 1.0f, GameObject audioObject = null )
	{
		var play = true;
		if (!_audioSourceClick)	play = InitAudioClick(audioObject);
		if (play)
		{
			_audioSourceClick.volume = volume;
			_audioSourceClick.Play();
		}
	}

    /// <summary>
    /// Play AUDIOCLIP
    /// </summary>
    /// <param name="clip"></param>
    /// <param name="volume"></param>
    /// <param name="loop"></param>
    /// <param name="speed"></param>
    /// <param name="autoDestroy"></param>
    /// <param name="audioObject"></param>
    /// <param name="maxDistance"></param>
    /// <returns></returns>
    public static AudioSource Play(AudioClip clip, float volume = 1.0f, bool loop = false, float speed = 1.0f, bool autoDestroy = true,
        GameObject audioObject = null, float maxDistance = 1000000f)
    {
        if (clip == null) return null;       
        
        if (!audioObject) {
            var cam = Camera.main;
            if (cam == null) return null;
            audioObject = new GameObject(AppConfig.prefixAutoObject + @"_audio_" + clip.name);          
            audioObject.transform.parent = cam.transform;
            audioObject.transform.localPosition = Vector3.zero;
            audioObject.transform.localScale = Vector3.one;
            audioObject.transform.localRotation = Quaternion.identity;
        }

        var source = audioObject.GetComponent<AudioSource>();
        if (source == null) {
            audioObject.AddComponent<AudioSource>();
            source = audioObject.GetComponent<AudioSource>();
            _audioSourceClick.playOnAwake = false;
            _audioSourceClick.rolloffMode = AudioRolloffMode.Custom;
            _audioSourceClick.maxDistance = maxDistance;
        }

        source.clip = clip;
        source.volume = volume;
        float duration = clip.length / speed;
        source.loop = loop;
        source.pitch = speed;
        if (autoDestroy)GameObject.Destroy(audioObject, duration + 0.2f);

        source.Play();
        return source;
    }
}

