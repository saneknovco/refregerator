﻿using UnityEngine;
using System.IO;

namespace Mage.BuildUtils
{
    [System.Serializable]
    public class MageAppPlistData
    {
        #region static
        //[UnityEditor.MenuItem("Assets/Create/Mage/templateMageAppPlistData")]
        //public static void CreateAssetXXX()
        //{
        //      "1004562049",// FunnyFoods Lite(IAP)
        //      "991021617",// BabyWizar full
        //      "991022422",// BabyWizar lite      
        //    string path = System.IO.Path.Combine(Application.dataPath, @"AppConfig/iOS/MageAppPlistData.json");
        //    var obj = new MageAppPlistData();
        //    obj.LSApplicationQueriesSchemes = new string[] { "id1004562049", "id991021617", "id991022422" };
        //    obj.SaveToFile(path);
        //}

        public static MageAppPlistData CreateFromJSON()
        {           
            //Debug.Log(Path.Combine(Application.dataPath, "AppConfig/iOS/MageAppPlistData.json"));
            return CreateFromJSON(Path.Combine(Application.dataPath, "AppConfig/iOS/MageAppPlistData.json"));
        }
        public static MageAppPlistData CreateFromJSON(string uriAsset)
        {
            try
            {                
                //Debug.Log(File.ReadAllText(uriAsset));
                return JsonUtility.FromJson<MageAppPlistData>(File.ReadAllText(uriAsset));
            }
            catch
            {
                return new MageAppPlistData();
            }
        }
        #endregion

        #region Json Serialization

        public string[] LSApplicationQueriesSchemes;

        #endregion

        #region methods
        public bool SaveToFile(string uriAsset)
        {
            File.WriteAllText(uriAsset, JsonUtility.ToJson(this, true));
            return true;
        }
        #endregion
    }
}
