﻿using UnityEngine;
using UnityEditor;
#if UNITY_IOS || UNITY_TVOS
using UnityEditor.iOS.Xcode;
#endif
using UnityEditor.Callbacks;
//.NET
using System.IO;

namespace Mage.BuildUtils 
{
    public static class MageAppPostProcessBuild
    {
        private static System.Version _version = null;
        private static AppSettings _appSettingsInstance = null;
        public static AppSettings appSettingsInstance
        {
            get 
            {
                
                if (_appSettingsInstance == null)
					_appSettingsInstance = AssetDatabase.LoadAssetAtPath(@"Assets/Resources/Settings/" + AppSettings.assetFullName + ".asset", typeof(AppSettings)) as AppSettings;                         
                return _appSettingsInstance; 
            }            
        }
        
        [PostProcessBuild]
        public static void OnPostProcessBuild(BuildTarget target, string pathToProject)
        {
            _appSettingsInstance = null;
            AssetDatabase.Refresh();
            string newLine = System.Environment.NewLine;
            Debug.Log("=== MageApp: OnPostProcessBuild: START to platform : ".AddTime() + target.ToString() +
               newLine + "PATH TO BUILD: " + pathToProject + newLine + "...............");

            // increment build version
            AutoIncrementBuild(pathToProject);

#if UNITY_IOS || UNITY_TVOS
            //update plist
            iOSUpdatePList(pathToProject);
            //update project
            //iOSUpdatePBXProject(pathToProject);
#endif
            //
            Debug.Log("=== MageApp: OnPostProcessBuild: END to platform: ".AddTime() + target.ToString());
        }

        #region Common Post Process Build
        private static void AutoIncrementBuild(string path)
        {
            var appSet = appSettingsInstance;
            if (appSet == null)
            {
				Debug.LogError(@"Couldn't find Assets/Resources/Settings/" + AppSettings.assetFullName + ".asset");
                return;
            }

            string pattern = @"^([\d\.]+)$";
            bool success = false;            
			var bundleVer = appSet.appInfo.bundleVersion;
            
            _version = null;
            if (System.Text.RegularExpressions.Regex.IsMatch(bundleVer, pattern))
            {
                var match = System.Text.RegularExpressions.Regex.Match(bundleVer, pattern);
                _version = new System.Version(match.Groups[0].Value);
                var major = _version.Major < 0 ? 0 : _version.Major;
                var minor = _version.Minor < 0 ? 0 : _version.Minor;
                var build = _version.Build < 0 ? 0 : _version.Build;
                var revision = _version.Revision < 0 ? 0 : _version.Revision;
                _version = new System.Version(major, minor, build, revision + 1);
                success = true;

				appSet.appInfo.UpdateBuildVersion(_version.ToString(), true);
                #if (UNITY_IOS || UNITY_TVOS) && !UNITY_5 //for unity4 : ver4.6.9+
   				UnityEditor.PlayerSettings.shortBundleVersion = version.Major.ToString() + "." + version.Minor.ToString() + "." + version.Build.ToString();
				UnityEditor.PlayerSettings.bundleVersion = version.Revision.ToString();
                #else
                UnityEditor.PlayerSettings.bundleVersion = appSet.appInfo.bundleVersion;
                #endif
            }

            if (!success)
            {
                Debug.LogError(@"Couldn't find bundle version in ProjectSettings.asset");
                return;
            }
	
			appSet.SaveState();
			//!!! WARNING !!!
			AssetDatabase.SaveAssets();            
            AssetDatabase.Refresh();
        }

        #endregion

        #region Android Post Process Build
#if UNITY_ANDROID

        /// TO DO Android specific

#endif
        #endregion

        #region iOS Post Process Build
#if UNITY_IOS || UNITY_TVOS

        private static void iOSUpdatePList(string path )
        {   
            var plistPath = Path.Combine(path, @"Info.plist");
            if (!File.Exists(plistPath))
            {
                Debug.LogWarning(@"Couldn't find Info.plist in build output.");
                return;
            }

            var appSet = appSettingsInstance;
            //read plist.info
            var plist = new PlistDocument();
            plist.ReadFromFile(plistPath);
            
            //1. version & build
            if (_version != null)
            {
                plist.root.SetString(@"CFBundleShortVersionString", 
                    _version.Major.ToString() + "." + _version.Minor.ToString() + "." + _version.Build.ToString());
                plist.root.SetString(@"CFBundleVersion", _version.Revision.ToString());
            }
                        
            //2. CFBundleURLTypes
            //here's how the custom url scheme should end up looking
            /***************************************
             <key>CFBundleURLTypes</key>
             <array>
                 <dict>
                     <key>CFBundleURLSchemes</key>
                     <array>
                         <string>idYOUR_APP_ID</string>
                     </array>
                 </dict>
             </array>
            ***************************************/
            plist.root.CreateArray(@"CFBundleURLTypes").AddDict().CreateArray(@"CFBundleURLSchemes").AddString(appSet.appInfo.appUrlScheme);
            
            //3. CFBundleLocalizations
            //example:
            /***************************************
            < key > CFBundleLocalizations </ key >
            < array >
                < string > en </ string >
                < string > zh_CN </ string >
            </ array >
            ***************************************/
            var bundleLocalizations = plist.root.CreateArray(@"CFBundleLocalizations");
            var uiSet = appSet.UI;
            if ( uiSet.isMultiLang )
            {                
                foreach (var item in uiSet.supportedLang)
                {
                    if (item.Key == MageAppUILang.none) continue;

                    if (item.Key == MageAppUILang.cn)
                    {
                        if (item.Value) bundleLocalizations.AddString("zh_CN");
                        continue;
                    }
                    if (item.Key == MageAppUILang.sp)
                    {
                        if (item.Value) bundleLocalizations.AddString("es");
                        continue;
                    }
                    if (item.Key == MageAppUILang.jp)
                    {
                        if (item.Value) bundleLocalizations.AddString("ja");
                        continue;
                    }
                    if (item.Key == MageAppUILang.kr)
                    {
                        if (item.Value) bundleLocalizations.AddString("ko");
                        continue;
                    }
                    if (item.Value) bundleLocalizations.AddString(System.Enum.GetName(typeof(MageAppUILang), item.Key));
                }

            }
            else
            {
                if (uiSet.defaultLang == MageAppUILang.cn)
                    bundleLocalizations.AddString("zh_CN");
                else if (uiSet.defaultLang == MageAppUILang.sp)
                    bundleLocalizations.AddString("es");
                else if (uiSet.defaultLang == MageAppUILang.jp)
                    bundleLocalizations.AddString("ja");
                else if (uiSet.defaultLang == MageAppUILang.kr)
                    bundleLocalizations.AddString("ko");
                else
                    bundleLocalizations.AddString(System.Enum.GetName(typeof(MageAppUILang), uiSet.defaultLang));
            }

            //4. LSApplicationQueriesSchemes
            //example:
            /***************************************
            < key > LSApplicationQueriesSchemes </ key >
            < array >
                < string > mg886533398 </ string >
            </ array >
            ***************************************/
            var plistData = MageAppPlistData.CreateFromJSON();
            if (plistData.LSApplicationQueriesSchemes == null)
                Debug.LogError(@"MageApp: OnPostProcessBuild: LSApplicationQueriesSchemes is corruption!!!".AddTime());
            else
            {
                var applicationQueriesSchemes = plist.root.CreateArray(@"LSApplicationQueriesSchemes");
                for (int i = 0; i < plistData.LSApplicationQueriesSchemes.Length; i++)
                    applicationQueriesSchemes.AddString(plistData.LSApplicationQueriesSchemes[i]);
            }

            //save plist
            plist.WriteToFile(plistPath);
        }


        private const string organizationSectionGuid = @"29B97313FDCFA39411CA2CEA";
        private static void iOSUpdatePBXProject(string path)
        {
            var appSet = appSettingsInstance;
            /// https://gist.github.com/suakig/ffb64f48c800e9dea545
            ///
            // Create a new project object from build target
            string projPath = PBXProject.GetPBXProjectPath(path);            
            PBXProject proj = new PBXProject();
            proj.ReadFromString(File.ReadAllText(projPath));
            string unityTargetName = PBXProject.GetUnityTargetName();
            string target = proj.TargetGuidByName(unityTargetName);

            //9B97313FDCFA39411CA2CEA /* Project object */ = {
            //    isa = PBXProject;
            //    attributes = {
            //        ORGANIZATIONNAME = "ARROWSTAR LIMITED";
            //        TargetAttributes = {
            //            1D6058900D05DD3D006BFB54 = {
            //                DevelopmentTeam = 4WLDFKZGA7;
            //                SystemCapabilities = {
            //                    com.apple.BackgroundModes = {
            //                        enabled = 1;
            //                    };
            //                    com.apple.Push = {
            //                        enabled = 1;
            //                    };
            //                };
            //            };
            //            5623C57217FDCB0800090B9E = {
            //                TestTargetID = 1D6058900D05DD3D006BFB54;
            //            };
            //        };
            //    };

            /// - API DO NOT IMPLEMENTATION (((
            //proj.AddBuildPropertyForConfig(organizationSectionGuid, "ORGANIZATIONNAME", appSet.appInfo.companyName);                        

            // Finally save the xcode project            
            File.WriteAllText(projPath, proj.WriteToString());
        }
#endif
        #endregion        
	}
}