﻿Shader "Custom/ModifyHSV" {
	Properties {
		_H ("H", Float) = 0
		_S ("S", Float) = 0
		_V ("V", Float) = 0
		_MainTex ("Base (RGB) Alpha (A)", 2D) = "white" {}
	}
	SubShader {
		Tags { "Queue"="Transparent" "IgnoreProjector"="True" "RenderType"="Transparent" }
		Lighting Off
		//Cull Off
		ZTest Always
		ZWrite Off
		Fog { Mode Off }
		
		Pass {
			Blend SrcAlpha OneMinusSrcAlpha
			
			CGPROGRAM
			#pragma vertex vert
			#pragma fragment frag
			
			#include "UnityCG.cginc"
			
			float _H, _S, _V;
			sampler2D _MainTex;
			uniform float4 _MainTex_ST; // Needed for TRANSFORM_TEX(v.texcoord0, _MainTex)
			
			/////////////////////////////////////////////////////
			// (see also http://www.chilliant.com/rgb2hsv.html)
			// http://lolengine.net/blog/2013/07/27/rgb-to-hsv-in-glsl
			float3 rgb2hsv(float3 c) {
			    float4 K = float4(0.0, -1.0 / 3.0, 2.0 / 3.0, -1.0);
			    //float4 p = lerp(float4(c.bg, K.wz), float4(c.gb, K.xy), step(c.b, c.g));
			    //float4 q = lerp(float4(p.xyw, c.r), float4(c.r, p.yzx), step(p.x, c.r));
			    float4 p = c.g < c.b ? float4(c.bg, K.wz) : float4(c.gb, K.xy);
			    float4 q = c.r < p.x ? float4(p.xyw, c.r) : float4(c.r, p.yzx);
			    
			    float d = q.x - min(q.w, q.y);
			    float e = 1.0e-10;
			    return float3(abs(q.z + (q.w - q.y) / (6.0 * d + e)), d / (q.x + e), q.x);
			}
			float3 hsv2rgb(float3 c) {
			    float4 K = float4(1.0, 2.0 / 3.0, 1.0 / 3.0, 3.0);
			    float3 p = abs(frac(c.xxx + K.xyz) * 6.0 - K.www);
			    return c.z * lerp(K.xxx, saturate(p - K.xxx), c.y);
			}
			/////////////////////////////////////////////////////
			
			struct appdata {
				float4 vertex : POSITION;
				float2 texcoord0 : TEXCOORD0;
			};
			
			struct v2f {
				float4 pos : SV_POSITION;
				float2 texcoord0 : TEXCOORD0;
			};
			
			v2f vert(appdata v) {
				v2f o;
				o.pos = mul(UNITY_MATRIX_MVP, v.vertex);
				o.texcoord0 = TRANSFORM_TEX(v.texcoord0, _MainTex);
				return o;
			}
			
			fixed4 frag(v2f i) : COLOR0 {
				fixed4 texcol = tex2D(_MainTex, i.texcoord0);
				float3 tmp = rgb2hsv(texcol.rgb);
				tmp.x += _H;
				tmp.x -= floor(tmp.x);
				tmp.y = saturate(tmp.y + _S);
				tmp.z = saturate(tmp.z + _V);
				//tmp = saturate(tmp);
				texcol.rgb = hsv2rgb(tmp);
				return texcol;
			}
			ENDCG
		}
	}
	FallBack "Unlit/Transparent"
}
