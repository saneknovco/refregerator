﻿Shader "Custom/OutlineShader2" {
	Properties {
		_OutlineColor ("Outline Color", Color) = (1,1,1,1)
		_OutlineSize ("Outline Size", Float) = 1
		_AlphaMin ("Alpha Min", Float) = 0
		_AlphaMax ("Alpha Max", Float) = 1
		_Sharpness ("Sharpness", Float) = 1
		_TextureWidth ("Texture Width", Float) = 128
		_TextureHeight ("Texture Height", Float) = 128
		_TexInfluence ("Texture Influence", Range (0, 1)) = 1
		_Color ("Color Tint", Color) = (1,1,1,1)
		_MainTex ("Base (RGB) Alpha (A)", 2D) = "white" {}
	}
	SubShader {
		Tags { "Queue"="Transparent" "IgnoreProjector"="True" "RenderType"="Transparent" }
		Lighting Off
		//Cull Off
		ZTest Always
		ZWrite Off
		Fog { Mode Off }
		
		Pass {
			Blend One OneMinusSrcAlpha
			
			CGPROGRAM
			#pragma vertex vert
			#pragma fragment frag
			
			#pragma multi_compile SAMPLE_4 SAMPLE_8
			
			#include "UnityCG.cginc"
			
			fixed4 _OutlineColor;
			float _OutlineSize;
			float _AlphaMin;
			float _AlphaMax;
			float _Sharpness;
			float _TextureWidth;
			float _TextureHeight;
			fixed _TexInfluence;
			fixed4 _Color;
			sampler2D _MainTex;
			uniform float4 _MainTex_ST; // Needed for TRANSFORM_TEX(v.texcoord0, _MainTex)
			
			struct appdata {
				float4 vertex : POSITION;
				float2 texcoord0 : TEXCOORD0;
			};
			
			struct v2f {
				float4 pos : SV_POSITION;
				float2 texcoord0 : TEXCOORD0;
				float4 texcoordH : TEXCOORD1;
				float4 texcoordV : TEXCOORD2;
				#if SAMPLE_8
				float4 texcoordDP : TEXCOORD3;
				float4 texcoordDN : TEXCOORD4;
				#endif
			};
			
			v2f vert(appdata v) {
				v2f o;
				o.pos = mul(UNITY_MATRIX_MVP, v.vertex);
				float2 uv = TRANSFORM_TEX(v.texcoord0, _MainTex);
				float2 duv = float2(_OutlineSize / _TextureWidth, _OutlineSize / _TextureHeight);
				o.texcoord0 = uv;
				o.texcoordH = float4(uv.x - duv.x, uv.y, uv.x + duv.x, uv.y);
				o.texcoordV = float4(uv.x, uv.y - duv.y, uv.x, uv.y + duv.y);
				#if SAMPLE_8
				o.texcoordDP = float4(uv.x - duv.x, uv.y - duv.y, uv.x + duv.x, uv.y + duv.y);
				o.texcoordDN = float4(uv.x - duv.x, uv.y + duv.y, uv.x + duv.x, uv.y - duv.y);
				#endif
				return o;
			}
			
			fixed4 frag(v2f i) : COLOR0 {
				fixed min_a;
				fixed4 s;
				
				s = fixed4(tex2D(_MainTex, i.texcoordH.xy).a,
						   tex2D(_MainTex, i.texcoordH.zw).a,
						   tex2D(_MainTex, i.texcoordV.xy).a,
						   tex2D(_MainTex, i.texcoordV.zw).a) *
					fixed4((i.texcoordH.x > 0),
						   (i.texcoordH.z < 1),
						   (i.texcoordV.y > 0),
						   (i.texcoordV.w < 1));
				min_a = min(min(s.x, s.y), min(s.z, s.w));
				
				#if SAMPLE_8
				s = fixed4(tex2D(_MainTex, i.texcoordDP.xy).a,
						   tex2D(_MainTex, i.texcoordDP.zw).a,
						   tex2D(_MainTex, i.texcoordDN.xy).a,
						   tex2D(_MainTex, i.texcoordDN.zw).a) *
					fixed4((i.texcoordDP.x > 0) * (i.texcoordDP.y > 0),
						   (i.texcoordDP.z < 1) * (i.texcoordDP.w < 1),
						   (i.texcoordDN.x > 0) * (i.texcoordDN.y < 1),
						   (i.texcoordDN.z < 1) * (i.texcoordDN.w > 0));
				min_a = min(min(min(s.x, s.y), min(s.z, s.w)), min_a);
				#endif
				
				fixed4 texcol = tex2D(_MainTex, i.texcoord0);
				min_a = min(texcol.a, min_a);
				_OutlineColor.a *= pow(saturate((texcol.a - _AlphaMin) / (_AlphaMax - _AlphaMin)), _Sharpness);
				_OutlineColor.rgb *= _OutlineColor.a; // premultiply before interpolation!
				texcol = _Color * lerp(fixed4(1,1,1,texcol.a), texcol, _TexInfluence);
				texcol.rgb *= texcol.a; // premultiply before interpolation!
				return lerp(_OutlineColor, texcol, min_a);
			}
			ENDCG
		}
	}
	FallBack "Unlit/Transparent"
	CustomEditor "OutlineMaterialEditor"
}
