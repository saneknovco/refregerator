﻿Shader "Custom/SiluetteShader" {
	Properties 
    {
        _Color ("color", Color) = (0.5,0.5,0.5,1)
        _MainTex ("Atlas_tex", 2D) = "white" {}
    }
    SubShader 
    {
		Tags { "RenderType" = "Transparent" "IgnoreProjector"="True" "Queue" = "Transparent"}
        Pass
        {
            BindChannels 
            {
                Bind "Vertex", vertex
                Bind "texcoord", texcoord0
            }
            Lighting Off
            Blend SrcAlpha OneMinusSrcAlpha
//            ZWrite On
//            ZTest NotEqual
//            AlphaTest Greater 0.05
//            Blend DstColor Zero
            SetTexture [_MainTex] 
            {
            	constantColor [_Color]
            	combine constant , texture*constant
            }
        }        

    }
    FallBack "Diffuse"
}
