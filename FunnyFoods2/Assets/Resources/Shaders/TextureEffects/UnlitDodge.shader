﻿Shader "Unlit/UnlitDodge" {
	Properties {
		_Color ("Color Tint", Color) = (1,1,1,1)
		_MainTex ("Base (RGB) Trans (A)", 2D) = "white" {}
		_Opacity ("Opacity", Range(0, 1)) = 1.0
	}
	
	SubShader {
		Tags {"Queue"="Transparent" "IgnoreProjector"="True" "RenderType"="Transparent"}
		Lighting Off
		//Cull Off
		ZTest Off
		ZWrite Off
		Fog { Mode Off }
		Blend DstColor One, Zero One
		
		Pass {
			CGPROGRAM
			#pragma vertex vert
			#pragma fragment frag
			
			#include "UnityCG.cginc"
			
			fixed4 _Color;
			sampler2D _MainTex;
			float _Opacity;

			struct appdata {
				float4 vertex : POSITION;
				float4 texcoord0 : TEXCOORD0;
			};

			struct v2f {
				float4 pos : SV_POSITION;
				float2 texcoord0 : TEXCOORD0;
			};
			
			v2f vert(appdata v) {
				v2f o;
				o.pos = mul(UNITY_MATRIX_MVP, v.vertex);
				o.texcoord0 = v.texcoord0.xy;
				return o;
			}
			
			fixed4 frag(v2f i) : COLOR0 {
				half4 texcol = tex2D(_MainTex, i.texcoord0) * _Color;
   				
   				// premultiply RGB with alpha
				texcol.rgb *= (texcol.a * _Opacity);
				texcol.a = 1;
		        
		        // Actual dodge is impossible with conventional shader,
		        // since a color is divided by color (in other words,
		        // a color has to be multiplied by something greater than 1).
		        
		        // dodge: result = base / (1 - blend)
		        // pseudo-dodge: result = base + base * k, where 0 <= k <= 1
		        // base / (1 - blend) = base * (1 + k)
		        // 1 + k = 1 / (1 - blend)
		        // k = (1 - (1 - blend)) / (1 - blend)
		        // k = blend / (1 - blend)
				//texcol.rgb = texcol.rgb / (1 - texcol.rgb);
				texcol.rgb /= (1 - texcol.rgb);
				
				return texcol;
			}
			
			ENDCG
		}
	}
	FallBack "Unlit/Transparent"
}
