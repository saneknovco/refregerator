Shader "Unlit/UnlitMultiply" {
	Properties {
		_MainTex ("Base (RGB) Trans (A)", 2D) = "white" {}
	}

	SubShader {
		Tags {"Queue"="Transparent" "IgnoreProjector"="True" "RenderType"="Transparent"}
		Lighting Off
		//Cull Off
		ZTest Off
		ZWrite Off
		Fog { Mode Off }
		Blend DstColor Zero

		Pass {
			Lighting Off
			SetTexture [_MainTex] { combine texture }
		}
	}
	FallBack "Unlit/Transparent"
}
