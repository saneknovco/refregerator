﻿Shader "BlendMode/HardLight" {
	Properties {
		_Color ("Color Tint", Color) = (1,1,1,1)
		_MainTex ("Base (RGB) Alpha (A)", 2D) = "white" {}
	}
	SubShader {
		Tags { "Queue"="Transparent" "IgnoreProjector"="True" "RenderType"="Transparent" }
		Lighting Off
		//Cull Off
		ZTest Always
		ZWrite Off
		Fog { Mode Off }
		
		// overlay = lerp(multiply, screen, dst)
		// hard_light = lerp(multiply, screen, src)
		
		// multiply = src*dst
		// screen = 1 - (1-src)*(1-dst) = src + dst - src*dst = add - multiply
		
		// overlay or hard_light = f*src + f*dst - 2*f*src*dst + src*dst
		// hard_light = src*src + 2*src*dst*(1 - src)
		
		Pass {
			Blend Zero SrcColor
			
			CGPROGRAM
			#pragma vertex vert
			#pragma fragment frag
			
			#include "UnityCG.cginc"
			
			fixed4 _Color;
			sampler2D _MainTex;
			uniform float4 _MainTex_ST; // Needed for TRANSFORM_TEX(v.texcoord0, _MainTex)
			
			struct appdata {
				float4 vertex : POSITION;
				float2 texcoord0 : TEXCOORD0;
			};
			
			struct v2f {
				float4 pos : SV_POSITION;
				float2 texcoord0 : TEXCOORD0;
			};
			
			v2f vert(appdata v) {
				v2f o;
				o.pos = mul(UNITY_MATRIX_MVP, v.vertex);
				o.texcoord0 = TRANSFORM_TEX(v.texcoord0, _MainTex);
				return o;
			}
			
			fixed4 frag(v2f i) : COLOR0 {
				fixed4 texcol = tex2D(_MainTex, i.texcoord0) * _Color;
				texcol.rgb *= 2*(1 - texcol.rgb);
				texcol.rgb = lerp(float3(1,1,1), texcol.rgb, texcol.a);
				//texcol.rgb *= texcol.a;
				return texcol;
			}
			ENDCG
		}
		
		Pass {
			Blend One One
			
			CGPROGRAM
			#pragma vertex vert
			#pragma fragment frag
			
			#include "UnityCG.cginc"
			
			fixed4 _Color;
			sampler2D _MainTex;
			uniform float4 _MainTex_ST; // Needed for TRANSFORM_TEX(v.texcoord0, _MainTex)
			
			struct appdata {
				float4 vertex : POSITION;
				float2 texcoord0 : TEXCOORD0;
			};
			
			struct v2f {
				float4 pos : SV_POSITION;
				float2 texcoord0 : TEXCOORD0;
			};
			
			v2f vert(appdata v) {
				v2f o;
				o.pos = mul(UNITY_MATRIX_MVP, v.vertex);
				o.texcoord0 = TRANSFORM_TEX(v.texcoord0, _MainTex);
				return o;
			}
			
			fixed4 frag(v2f i) : COLOR0 {
				fixed4 texcol = tex2D(_MainTex, i.texcoord0) * _Color;
				texcol.rgb *= texcol.rgb;
				texcol.rgb *= texcol.a;
				return texcol;
			}
			ENDCG
		}
	}
	FallBack "Unlit/Transparent"
}
