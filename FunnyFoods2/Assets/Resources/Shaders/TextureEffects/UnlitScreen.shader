﻿Shader "Unlit/UnlitScreen" {
	Properties {
		_Color ("Color Tint", Color) = (1,1,1,1)
		_MainTex ("Base (RGB) Trans (A)", 2D) = "white" {}
	}
	
	SubShader {
		Tags {"Queue"="Transparent" "IgnoreProjector"="True" "RenderType"="Transparent"}
		Lighting Off
		//Cull Off
		ZTest Off
		ZWrite Off
		Fog { Mode Off }
		// These should be equivalent
		Blend OneMinusDstColor One
		//Blend One OneMinusSrcColor

		Pass {
			CGPROGRAM
			#pragma vertex vert
			#pragma fragment frag
			
			#include "UnityCG.cginc"
			
			fixed4 _Color;
			sampler2D _MainTex;

			struct appdata {
				float4 vertex : POSITION;
				float4 texcoord0 : TEXCOORD0;
			};

			struct v2f {
				float4 pos : SV_POSITION;
				float2 texcoord0 : TEXCOORD0;
			};
			
			v2f vert(appdata v) {
				v2f o;
				o.pos = mul(UNITY_MATRIX_MVP, v.vertex);
				o.texcoord0 = v.texcoord0.xy;
				return o;
			}

			fixed4 frag(v2f i) : COLOR0 {
				half4 texcol = tex2D(_MainTex, i.texcoord0) * _Color;
				// premultiply RGB with alpha
				texcol.rgb *= texcol.a;
				texcol.a = 1;
				return texcol;
			}
			
			ENDCG
		}
	}
	FallBack "Unlit/Transparent"
}
