Shader "Unlit/Siluet_MultiplyTransparent" {
	Properties {
		_MainTex ("Base (RGB) Trans (A)", 2D) = "white" {}
        _Color ("color", Color) = (0.5,0.5,0.5,1)		
	}

	SubShader {
		Tags {"Queue"="Transparent" "IgnoreProjector"="True" "RenderType"="Transparent"}
		Lighting Off
		//Cull Off
//		ZWrite Off
//		ZTest Off
//            ZWrite On
//            ZTest NotEqual
//            AlphaTest Greater 0.05
	
		Fog { Mode Off }
		Blend DstColor Zero

		Pass {
			CGPROGRAM
			#pragma vertex vert
			#pragma fragment frag
			
			#include "UnityCG.cginc"
			
			sampler2D _MainTex;
			half4 _Color;

			struct appdata {
				float4 vertex : POSITION;
				float4 texcoord0 : TEXCOORD0;
			};

			struct v2f {
				float4 pos : SV_POSITION;
				float2 texcoord0 : TEXCOORD0;
			};
			
			v2f vert(appdata v) {
				v2f o;
				o.pos = mul(UNITY_MATRIX_MVP, v.vertex);
				o.texcoord0 = v.texcoord0.xy;
				return o;
			}

			fixed4 frag(v2f i) : COLOR0 {
				half4 texcol = tex2D(_MainTex, i.texcoord0);
				// premultiply RGB with alpha
				texcol.xyz = lerp(float3(1,1,1), float3(0.5,0.5,0.5), texcol.w*_Color.a);
				//texcol.w = 1;//= 0;// _Color.a;
				return texcol;
			}
			
			ENDCG
		}
	}
	FallBack "Unlit/Transparent"
}
