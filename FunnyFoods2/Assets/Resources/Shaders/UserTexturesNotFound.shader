Shader "Custom/UserTexturesNotFound" {
	Properties {
		//_AA_Scale ("AA Scale", Float) = 1000000
		_AA_Size ("AA Size", Float) = 0
		_Color ("Global tint", Color) = (1,1,1,1)
		_ColorR ("Color R", Color) = (1,0,0,1)
		_ColorG ("Color G", Color) = (0,1,0,1)
		_ColorB ("Color B", Color) = (0,0,1,1)
		_ColorA ("Color A", Color) = (1,1,1,0)
		_MainTex ("Main tex", 2D) = "white" {}
	}
	SubShader {
		Tags { "Queue"="Transparent" "IgnoreProjector"="True" "RenderType"="Transparent" }
		Lighting Off
		//Cull Off
		ZTest Always
		ZWrite Off
		Fog { Mode Off }
		Blend SrcAlpha OneMinusSrcAlpha
		
		Pass {
			CGPROGRAM
			#pragma vertex vert
			#pragma fragment frag
			
			#pragma multi_compile VECTOR_ON VECTOR_AA
			
			#include "UnityCG.cginc"
			
			half _AA_Size;
			//fixed _AA_Size;
			fixed4 _Color;
			fixed4 _ColorR;
			fixed4 _ColorG;
			fixed4 _ColorB;
			fixed4 _ColorA;
			sampler2D _MainTex;

			struct appdata {
				float4 vertex : POSITION;
				float4 texcoord0 : TEXCOORD0;
			};
			
			struct v2f {
				float4 pos : SV_POSITION;
				float2 texcoord0 : TEXCOORD0;
			};
			
			v2f vert(appdata v) {
				v2f o;
				o.pos = mul(UNITY_MATRIX_MVP, v.vertex);
				o.texcoord0 = v.texcoord0.xy;
				return o;
			}
			
			fixed4 frag(v2f i) : COLOR0 {
				#if VECTOR_ON
				fixed4 texcol = tex2D(_MainTex, i.texcoord0);
				texcol = (texcol > 0.5);
				_ColorR.a *= texcol.r;
				_ColorB.a *= texcol.b;
				return lerp(_ColorR, _ColorB, _ColorB.a) * _Color;
				#endif
				#if VECTOR_AA
				fixed4 texcol = tex2D(_MainTex, i.texcoord0);
				//texcol = saturate((texcol - 0.5) / _AA_Size + 0.5);
				texcol.r = saturate((texcol.r - 0.5) / _AA_Size + 0.5);
				texcol.b = saturate((texcol.b - 0.5) / _AA_Size + 0.5);
				fixed aR = _ColorR.a * texcol.r;
				fixed aB = _ColorB.a * texcol.b;
				fixed aR1B = aR * (1 - aB); // = aR - aR*aB
				fixed a = aB + aR1B; // = aB + aR - aR*aB
				fixed3 rgb = lerp(_ColorB.rgb, _ColorR.rgb, aR1B);
				return fixed4(rgb, a) * _Color;
				#endif
			}
			
			ENDCG
		}
	}
	FallBack "Unlit/Transparent"
}
