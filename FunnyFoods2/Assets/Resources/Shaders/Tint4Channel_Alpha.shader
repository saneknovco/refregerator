Shader "Custom/Tint4Channel_Alpha" {
	Properties {
		_Color ("Global tint", Color) = (1,1,1,1)
		_ColorR ("Color R", Color) = (1,0,0,1)
		_ColorG ("Color G", Color) = (0,1,0,1)
		_ColorB ("Color B", Color) = (0,0,1,1)
		_ColorA ("Color A", Color) = (1,1,1,0)
		_MainTex ("Main tex", 2D) = "white" {}
	}
	SubShader {
		Tags { "Queue"="Transparent" "IgnoreProjector"="True" "RenderType"="Transparent" }
		Lighting Off
		//Cull Off
		ZTest Always
		ZWrite Off
		Fog { Mode Off }
		Blend SrcAlpha OneMinusSrcAlpha
		
		Pass {
			CGPROGRAM
			#pragma vertex vert
			#pragma fragment frag
			
			#pragma multi_compile VECTOR_OFF VECTOR_ON VECTOR_AA
			#pragma multi_compile PROPER_BLEND FAST_BLEND
			
			#if VECTOR_AA
			#pragma glsl
			#pragma target 3.0
			#endif
			
			#include "UnityCG.cginc"
			
			fixed4 _Color;
			fixed4 _ColorR;
			fixed4 _ColorG;
			fixed4 _ColorB;
			fixed4 _ColorA;
			sampler2D _MainTex;

			struct appdata {
				float4 vertex : POSITION;
				float4 texcoord0 : TEXCOORD0;
			};
			
			struct v2f {
				float4 pos : SV_POSITION;
				float2 texcoord0 : TEXCOORD0;
			};
			
			v2f vert(appdata v) {
				v2f o;
				o.pos = mul(UNITY_MATRIX_MVP, v.vertex);
				o.texcoord0 = v.texcoord0.xy;
				return o;
			}
			
			const half a_min = 1.0/255.0;
			
			fixed4 frag(v2f i) : COLOR0 {
				fixed4 texcol = tex2D(_MainTex, i.texcoord0);
				
				#if VECTOR_AA
				fixed4 dist = (texcol - 0.5);
				fixed4 derivX = ddx(dist);
				fixed4 derivY = ddy(dist);
				fixed4 L = sqrt(derivX*derivX + derivY*derivY);
				fixed4 aa = saturate(dist / (2 * L));
				texcol = lerp(fixed4(0,0,0,0), fixed4(1,1,1,1), aa);
				#endif
				#if VECTOR_ON
				texcol = (texcol > 0.5);
				#endif
				
				#if PROPER_BLEND
				fixed3 rgb;
				//fixed a, kIN, kBKG; // causes glitches on mobile
				half a, kIN, kBKG;
				
				rgb = _ColorR.rgb;
				a = _ColorR.a * texcol.r;
				
				kIN = _ColorG.a * texcol.g;
				kBKG = a * (1 - kIN);
				a = kIN + kBKG;
				rgb = (_ColorG.rgb*kIN + rgb*kBKG)/max(a, a_min);
				//rgb = lerp(_ColorG.rgb, rgb, saturate(kBKG/a));
				
				kIN = _ColorB.a * texcol.b;
				kBKG = a * (1 - kIN);
				a = kIN + kBKG;
				rgb = (_ColorB.rgb*kIN + rgb*kBKG)/max(a, a_min);
				//rgb = lerp(_ColorB.rgb, rgb, saturate(kBKG/a));
				
				kIN = _ColorA.a * texcol.a;
				kBKG = a * (1 - kIN);
				a = kIN + kBKG;
				rgb = (_ColorA.rgb*kIN + rgb*kBKG)/max(a, a_min);
				//rgb = lerp(_ColorA.rgb, rgb, saturate(kBKG/a));
				
				return fixed4(rgb, a) * _Color;
				#endif
				
				#if FAST_BLEND
				fixed3 rgb;
				fixed a, kIN, kBKG;
				
				a = _ColorR.a * texcol.r;
				rgb = _ColorR.rgb;
				
				kIN = (_ColorG.a * texcol.g);
				kBKG = a * (1 - kIN);
				a = kIN + kBKG;
				rgb = lerp(_ColorG.rgb, rgb, kBKG);
				
				kIN = (_ColorB.a * texcol.b);
				kBKG = a * (1 - kIN);
				a = kIN + kBKG;
				rgb = lerp(_ColorB.rgb, rgb, kBKG);
				
				kIN = (_ColorA.a * texcol.a);
				kBKG = a * (1 - kIN);
				a = kIN + kBKG;
				rgb = lerp(_ColorA.rgb, rgb, kBKG);
				
				return fixed4(rgb, a) * _Color;
				#endif
			}
			
			ENDCG
		}
	}
	FallBack "Unlit/Transparent"
}
