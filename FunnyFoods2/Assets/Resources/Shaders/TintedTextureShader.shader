Shader "Unlit/TintedTextureShader" {
	Properties {
		_Color ("Color Tint", Color) = (1,1,1,1)    
		_MainTex ("Base (RGB) Alpha (A)", 2D) = "white"
	}
	SubShader {
		Tags { "Queue"="Transparent" "IgnoreProjector"="True" "RenderType"="Transparent" }
		Lighting Off
		//Cull Off
		ZTest Always
		ZWrite Off
		Fog { Mode Off }
		Blend SrcAlpha OneMinusSrcAlpha
		
		Pass {
			SetTexture [_MainTex] {
				ConstantColor [_Color]
				Combine Texture * constant
			}
		}
	}
}