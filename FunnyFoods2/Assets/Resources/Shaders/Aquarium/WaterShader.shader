﻿Shader "Aquarium/WaterShader" {
	Properties {
		_Additivity1 ("Additivity 1", Float) = 0.0
		_Additivity0 ("Additivity 0", Float) = 0.0
		_Color1 ("Color 1", Color) = (1,1,1,1)
		_Color0 ("Color 0", Color) = (1,1,1,1)
		_WaterTex ("Water (ARGB)", 2D) = "white" {}
	}
	SubShader {
		Tags { "Queue"="Transparent" "IgnoreProjector"="True" "RenderType"="Transparent" }
		Lighting Off
		Cull Off
		ZTest Always
		ZWrite Off
		Fog { Mode Off }
		
		//Blend One OneMinusSrcAlpha
		Blend SrcAlpha SrcColor
		
		Pass {
			CGPROGRAM
			#pragma vertex vert
			#pragma fragment frag
			
			#include "UnityCG.cginc"
			
			float _Additivity1;
			float _Additivity0;
			fixed4 _Color1;
			fixed4 _Color0;
			sampler2D _WaterTex;
			
			struct a2v {
				float4 vertex : POSITION;
				float2 texcoord : TEXCOORD0;
				float4 color : COLOR;
			};
			
			struct v2f {
				float4 pos : SV_POSITION;
				float2 uv_water : TEXCOORD0;
				float additivity : TEXCOORD1;
				float4 color : COLOR;
			};
			
			float4 _WaterTex_ST;
			
			v2f vert (a2v v) {
				v2f o;
				o.pos = mul(UNITY_MATRIX_MVP, v.vertex);
				o.uv_water = TRANSFORM_TEX(mul(_Object2World, v.vertex).xy, _WaterTex);
				o.additivity = lerp(_Additivity0, _Additivity1, saturate(v.texcoord.y));
				o.color = v.color * lerp(_Color0, _Color1, saturate(v.texcoord.y));
				return o;
			}
			
			half4 frag (v2f i) : COLOR {
				half4 result = tex2D(_WaterTex, i.uv_water) * i.color;
				//result.rgb *= result.a;
				//result.a *= saturate(1.0-i.additivity);
				result.rgb = lerp(fixed3(1,1,1), result.rgb, result.a);
				result.a *= saturate(i.additivity);
				return result;
			}
			ENDCG
		}
	}
	FallBack "Unlit/Transparent"
}
