﻿Shader "Aquarium/SplashShader" {
	Properties {
		_Color ("Tint Color", Color) = (1,1,1,1)
		_MainTex ("Base (RGB)", 2D) = "white" {}
		_WaterTex ("Water (ARGB)", 2D) = "white" {}
	}
	SubShader {
		Tags { "Queue"="Transparent" "IgnoreProjector"="True" "RenderType"="Transparent" }
		Lighting Off
		Cull Off
		ZTest Always
		ZWrite Off
		Fog { Mode Off }
		
		Blend SrcAlpha OneMinusSrcAlpha
		//Blend One One
		
		Pass {
			CGPROGRAM
			#pragma vertex vert
			#pragma fragment frag
			
			#include "UnityCG.cginc"
			
			fixed4 _Color;
			sampler2D _MainTex;
			sampler2D _WaterTex;
			
			struct a2v {
				float4 vertex : POSITION;
				float2 texcoord : TEXCOORD0;
				float4 color : COLOR;
			};
			
			struct v2f {
				float4 pos : SV_POSITION;
				float2 uv_main : TEXCOORD0;
				float2 uv_water : TEXCOORD1;
				float4 color : COLOR;
			};
			
			float4 _MainTex_ST;
			float4 _WaterTex_ST;
			
			v2f vert (a2v v) {
				v2f o;
				o.pos = mul(UNITY_MATRIX_MVP, v.vertex);
				o.uv_main = TRANSFORM_TEX(v.texcoord, _MainTex);
				o.uv_water = TRANSFORM_TEX(mul(_Object2World, v.vertex).xy, _WaterTex);
				o.color = v.color * _Color;
				return o;
			}
			
			half4 frag (v2f i) : COLOR {
				half4 texcol = tex2D(_MainTex, i.uv_main);
				half4 watercol = tex2D(_WaterTex, i.uv_water);
				texcol = texcol*i.color*watercol;
				return texcol;
				//return watercol;
			}
			ENDCG
		}
	}
	FallBack "Unlit/Transparent"
}
