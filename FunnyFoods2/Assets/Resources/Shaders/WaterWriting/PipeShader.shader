﻿Shader "WaterWriting/PipeShader" {
	Properties {
		_FillFade ("Fill Fade", Float) = 0.1
		_FillAmount ("Fill Amount", Float) = 0.0
		_InflationScale ("Inflation Scale", Float) = 1.0
		_InflateEnd0 ("Inflate End 0", Float) = 0.0
		_InflateEnd1 ("Inflate End 1", Float) = 0.0
		_Colorization ("Colorization", Float) = 0.0
		_Color ("Tint Color", Color) = (1,1,1,1)
		_MainTex ("Base (RGB)", 2D) = "white" {}
		_WaterTex ("Water (ARGB)", 2D) = "white" {}
	}
	SubShader {
		Tags { "Queue"="Transparent" "IgnoreProjector"="True" "RenderType"="Transparent" }
		Lighting Off
		Cull Off
		ZTest Always
		ZWrite Off
		Fog { Mode Off }
		
		//Blend SrcAlpha OneMinusSrcAlpha
		Blend One OneMinusSrcAlpha
		
		Pass {
			CGPROGRAM
			#pragma vertex vert
			#pragma fragment frag
			
			#pragma multi_compile PARTIALLY_FILLED NOT_PARTIALLY_FILLED
			
			#include "UnityCG.cginc"
			
			float _FillFade;
			float _FillAmount;
			float _InflationScale;
			float _InflateEnd0;
			float _InflateEnd1;
			float _Colorization;
			fixed4 _Color;
			sampler2D _MainTex;
			sampler2D _WaterTex;
			
			struct a2v {
				float4 vertex : POSITION;
				float2 texcoord : TEXCOORD0;
				float2 texcoord1 : TEXCOORD1;
				float3 normal : NORMAL;
				float4 color : COLOR;
			};
			
			struct v2f {
				float4 pos : SV_POSITION;
				float2 uv_main : TEXCOORD0;
				float2 uv_water : TEXCOORD1;
				float2 deltas : TEXCOORD2;
				float4 params : TEXCOORD3;
			};
			
			float4 _MainTex_ST;
			float4 _WaterTex_ST;
			
			// EncodeFloatRGBA, DecodeFloatRGBA are defined in UnityCg.cginc
			// But they might have a buggy formula? (e.g. 160581375 instead of 16581375)
			float4 Encode_Float_RGBA(float v) {
				float4 enc = float4(1.0, 255.0, 65025.0, 16581375.0) * v;
				enc.yzw = frac(enc.yzw);
				enc -= enc.yzww * float4(1.0/255.0, 1.0/255.0, 1.0/255.0, 0.0);
				return enc;
			}
			float Decode_Float_RGBA(float4 rgba) {
				return dot(rgba, float4(1.0, 1.0/255.0, 1.0/65025.0, 1.0/16581375.0));
			}
			float Decode_Float_RGB(float3 rgb) {
				return dot(rgb, float3(1.0, 1.0/255.0, 1.0/65025.0));
			}
			float Decode_Float_RG(float2 rg) {
				return dot(rg, float2(1.0, 1.0/255.0));
			}
			
			v2f vert (a2v v) {
				v2f o;
				o.pos = mul(UNITY_MATRIX_MVP, v.vertex);
				#if NOT_PARTIALLY_FILLED
				o.uv_main = TRANSFORM_TEX(v.texcoord, _MainTex);
				o.uv_water = TRANSFORM_TEX(mul(_Object2World, v.vertex).xy, _WaterTex);
				o.deltas = float2(0,0);
				o.params = float4(0, v.texcoord1.y, 0, 0);
				#else
				float2 uv_main = TRANSFORM_TEX(v.texcoord, _MainTex);
				float2 uv_shift = clamp(normalize(v.normal.xy), -1, 1) * abs(v.texcoord1.x);
				float2 uv_added = TRANSFORM_TEX((v.texcoord + uv_shift), _MainTex);
				o.uv_main = uv_main;
				o.uv_water = TRANSFORM_TEX(mul(_Object2World, v.vertex).xy, _WaterTex);
				o.deltas = uv_added - uv_main;
				float rel_distance = Decode_Float_RGB(v.color.rgb);
				float rel_thickness = v.texcoord1.y;
				float rel_thickness_abs = 1.0 / (1.0 - v.color.a);
				float water_height = max(v.color.a, 0.001);
				o.params = float4(rel_distance, rel_thickness, 1/rel_thickness_abs, water_height);
				#endif
				return o;
			}
			
			half4 frag (v2f i) : COLOR {
				#if NOT_PARTIALLY_FILLED
				float diff = (_FillAmount > 0);
				float inflation_coef = 0;
				half profile = smoothstep(0.8, 1, abs(i.params.y));
				
				half4 texcol = tex2D(_MainTex, i.uv_main);
				#else
				float fade_1 = 1.0 / _FillFade;
				float fade_z = i.params.x * fade_1;
				float fade_fill = _FillAmount * fade_1;
				float diff = saturate(fade_fill - fade_z);
				
				float h = smoothstep(1, 0, abs(diff*2 - 1));
				
				// distance-from-pipe-ends factors
				float2 hAB = float2(fade_z, fade_1 - fade_z)*2;
				float2 tAB = float2(_InflateEnd0, _InflateEnd1);
				hAB = lerp(saturate(abs(hAB)), 1, tAB);
				float inflation_coef = h * min(hAB.x, hAB.y) * _InflationScale;
				
				half profile = abs(i.params.y) * lerp(1, i.params.z, inflation_coef);
				profile = smoothstep(0.8, 1, profile); // fade within pipe's borders
				
				half4 texcol = tex2D(_MainTex, i.uv_main + i.deltas * inflation_coef);
				#endif
				
				half4 watercol = tex2D(_WaterTex, i.uv_water) * _Color;
				texcol = lerp(texcol, texcol*watercol, diff*_Colorization);
				
				watercol.a *= (1 - profile)*diff;
				
				watercol.rgb *= watercol.a;
				texcol.rgb *= texcol.a;
				return lerp(watercol, texcol, texcol.a);
			}
			ENDCG
		}
	}
	FallBack "Unlit/Transparent"
}
