﻿Shader "WaterWriting/WaterDropsShader" {
	Properties {
		_FillFade ("Fill Fade", Float) = 0.1
		_FillAmount ("Fill Amount", Float) = 0.0
		_WaveAmpl ("Wave Amplitude", Float) = 1.0
		_WavePhase ("Wave Phase", Float) = 0.0
		_Color ("Tint Color", Color) = (1,1,1,1)
		_MainTex ("Base (RGB)", 2D) = "white" {}
		_WaterTex ("Water (ARGB)", 2D) = "white" {}
	}
	SubShader {
		Tags { "Queue"="Transparent" "IgnoreProjector"="True" "RenderType"="Transparent" }
		Lighting Off
		Cull Off
		ZTest Always
		ZWrite Off
		Fog { Mode Off }
		Blend SrcAlpha OneMinusSrcAlpha
		
		Pass {
			CGPROGRAM
			#pragma vertex vert
			#pragma fragment frag
			
			#pragma multi_compile PARTIALLY_FILLED NOT_PARTIALLY_FILLED
			#pragma multi_compile WAVE_ON WAVE_OFF
			
			#include "UnityCG.cginc"
			
			float _FillFade;
			float _FillAmount;
			float _WaveAmpl;
			float _WavePhase;
			fixed4 _Color;
			sampler2D _MainTex;
			sampler2D _WaterTex;
			
			struct a2v {
				float4 vertex : POSITION;
				float2 texcoord : TEXCOORD0;
				float4 color : COLOR;
			};
			
			struct v2f {
				float4 pos : SV_POSITION;
				float2 uv_main : TEXCOORD0;
				float2 uv_water : TEXCOORD1;
				float dist : TEXCOORD2;
			};
			
			float4 _MainTex_ST;
			float4 _WaterTex_ST;
			
			v2f vert (a2v v) {
				v2f o;
				float4 pos = v.vertex;
				o.dist = pos.z;
				pos.z = 0;
				#if WAVE_ON
				const float PI2 = 6.283185307179586476925286766559;
				pos.x += _WaveAmpl * v.color.x * cos(_WavePhase + v.color.z*PI2);
				pos.y += _WaveAmpl * v.color.y * sin(_WavePhase + v.color.w*PI2);
				#endif
				o.pos = mul(UNITY_MATRIX_MVP, pos);
				o.uv_main = TRANSFORM_TEX(v.texcoord, _MainTex);
				o.uv_water = TRANSFORM_TEX(mul(_Object2World, pos).xy, _WaterTex);
				return o;
			}
			
			half4 frag (v2f i) : COLOR {
				#if NOT_PARTIALLY_FILLED
				float diff = (_FillAmount > 0);
				#else
				float fade_1 = 1.0 / _FillFade;
				float fade_z = i.dist * fade_1;
				float fade_fill = _FillAmount * fade_1;
				float diff = saturate(fade_fill - fade_z);
				#endif
				
				half4 texcol = tex2D(_MainTex, i.uv_main);
				half4 watercol = tex2D(_WaterTex, i.uv_water) * _Color;
				
				texcol.a *= diff;
				
				return texcol * watercol;
			}
			ENDCG
		}
	}
	FallBack "Unlit/Transparent"
}
