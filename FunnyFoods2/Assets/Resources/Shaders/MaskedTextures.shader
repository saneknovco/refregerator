﻿Shader "Custom/MaskedTextures" {
	Properties {
		_Color ("Color Tint", Color) = (1,1,1,1)
		_MainTex ("Base (RGB) Alpha (A)", 2D) = "white" {}
		_AlphaMap ("Alpha (A)", 2D) = "white" {}
		_MaskedColor ("Masked Color Tint", Color) = (1,1,1,1)
		_MaskedTex ("Masked (RGB) Alpha (A)", 2D) = "black" {}
		_PosScale ("Pos & Scale", Vector) = (0,0,1,1)
		_Angle ("Angle", Float) = 0.0
	}
	SubShader {
		Tags { "Queue"="Transparent" "IgnoreProjector"="True" "RenderType"="Transparent" }
		Lighting Off
		//Cull Off
		ZTest Always
		ZWrite Off
		Fog { Mode Off }
		
		Pass {
			Blend SrcAlpha OneMinusSrcAlpha, One One
			
			CGPROGRAM
			#pragma vertex vert
			#pragma fragment frag
			
			#pragma multi_compile __ USE_SEPARATE_ALPHA_MAP
			
			#include "UnityCG.cginc"
			
			fixed4 _Color;
			sampler2D _MainTex;
			uniform float4 _MainTex_ST; // Needed for TRANSFORM_TEX(v.texcoord0, _MainTex)
			uniform float4 _MainTex_TexelSize;
			
			#if USE_SEPARATE_ALPHA_MAP
			sampler2D _AlphaMap;
			#endif
			
			fixed4 _MaskedColor;
			sampler2D _MaskedTex;
			uniform float4 _MaskedTex_ST; // Needed for TRANSFORM_TEX(v.texcoord0, _MainTex)
			uniform float4 _MaskedTex_TexelSize;
			
			float4 _PosScale;
			float _Angle;
			
			// Declare this next to your sampler:
			// float4 _MaskedTex_TexelSize;
			// Unity will populate it with (1/width, 1/height, width, height) for the texture.
			// If using D3D and anti-aliasing in an image effect, the y value will be negative
			// for _MainTex. You can use this to get the texel size/dimensions of any texture
			// assigned by Unity. Unfortunately, this feature has never been documented except
			// for a passing mention in Platform-specific Rendering Differences.
			
			struct appdata {
				float4 vertex : POSITION;
				float2 texcoord0 : TEXCOORD0;
			};
			
			struct v2f {
				float4 pos : SV_POSITION;
				float2 texcoord0 : TEXCOORD0;
				float2 texcoord1 : TEXCOORD1;
			};
			
			v2f vert(appdata v) {
				float4 abs_pos = mul(_Object2World, v.vertex);
				float4 rel_pos = mul(_World2Object, abs_pos);
				
				v2f o;
				o.pos = mul(UNITY_MATRIX_MVP, v.vertex);
				o.texcoord0 = TRANSFORM_TEX(v.texcoord0, _MainTex);
				
				float _sin, _cos;
				sincos(radians(_Angle), _sin, _cos);
				float2x2 texmat = { {_cos, _sin}, {-_sin, _cos} };
				float2 tex_xy = mul(texmat, rel_pos.xy - _PosScale.xy);
				o.texcoord1 = tex_xy / (_MaskedTex_TexelSize.zw * _PosScale.zw);
				
				return o;
			}
			
			const half a_min = 1.0/255.0;
			fixed4 frag(v2f i) : COLOR0 {
				#if USE_SEPARATE_ALPHA_MAP
				fixed mask_a = tex2D(_AlphaMap, i.texcoord0).a;
				fixed4 main_color = tex2D(_MainTex, i.texcoord0);
				main_color.a = mask_a;
				main_color *= _Color;
				#else
				fixed4 main_color = tex2D(_MainTex, i.texcoord0);
				fixed mask_a = main_color.a;
				main_color *= _Color;
				#endif
				
				fixed2 test = i.texcoord1;
				test.x = frac(test.x + 50);
				test.y = frac(test.y + 50);
				fixed4 masked_color = tex2D(_MaskedTex, test) * _MaskedColor;
				masked_color.a *= mask_a * (i.texcoord1.y >= 0) * (i.texcoord1.y < 1);
				
				fixed3 rgb;
				half a, kIN, kBKG;
				
				rgb = main_color.rgb;
				a = main_color.a;
				
				kIN = masked_color.a;
				kBKG = a * (1 - kIN);
				a = kIN + kBKG;
				rgb = (masked_color.rgb*kIN + rgb*kBKG)/max(a, a_min);
				
				return fixed4(rgb, a);
			}
			ENDCG
		}
	}
	FallBack "Unlit/Transparent"
	CustomEditor "AlphamapMaterialInspector"
}
