﻿Shader "Shadow2D/Filter" {
	Properties {
		_Color ("Color Tint", Color) = (1,1,1,1)
	}
	SubShader {
		Tags { "Queue"="Transparent" "IgnoreProjector"="True" "RenderType"="Transparent" }
		Lighting Off
		Cull Off
		ZTest Always
		ZWrite Off
		Fog { Mode Off }
		
		Pass {
			Color [_Color]
			Blend DstAlpha OneMinusDstAlpha
		}
	}
}
