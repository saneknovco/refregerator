﻿Shader "Shadow2D/AlphaClear" {
	Properties {
		_Color ("Color", Color) = (0,0,0,0)
	}
	SubShader {
		Tags { "Queue"="Overlay+1000" "IgnoreProjector"="True" "RenderType"="Transparent" }
		Lighting Off
		Cull Off
		ZTest Always
		ZWrite Off
		Fog { Mode Off }
		
		ColorMask A
		
		Pass {
			Color [_Color]
			Blend Off
		}
	}
}
