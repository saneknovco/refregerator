﻿Shader "Shadow2D/Caster" {
	Properties {
		_Color ("Color Tint", Color) = (1,1,1,1)
		_MainTex ("Base (RGB) Alpha (A)", 2D) = "white" {}
	}
	SubShader {
		Tags { "Queue"="Transparent" "IgnoreProjector"="True" "RenderType"="Transparent" }
		Lighting Off
		//Cull Off
		ZTest Always
		ZWrite Off
		Fog { Mode Off }
		
		ColorMask A
		
		Pass {
			BlendOp Max
			Blend One One
			
			CGPROGRAM
			#pragma vertex vert
			#pragma fragment frag
			
			#include "UnityCG.cginc"
			
			sampler2D _MainTex;
			uniform float4 _MainTex_ST; // Needed for TRANSFORM_TEX(v.texcoord0, _MainTex)
			
			struct appdata {
				float4 vertex : POSITION;
				float2 texcoord0 : TEXCOORD0;
			};
			
			struct v2f {
				float4 pos : SV_POSITION;
				float2 texcoord0 : TEXCOORD0;
				float shadow_fade : TEXCOORD1;
			};
			
			v2f vert(appdata v) {
				v2f o;
				o.pos = mul(UNITY_MATRIX_MVP, v.vertex);
				o.pos.z = UNITY_MATRIX_MVP[2][3];
				o.texcoord0 = TRANSFORM_TEX(v.texcoord0, _MainTex);
				o.shadow_fade = 1 - saturate(o.pos.z);
				return o;
			}
			
			fixed4 frag(v2f i) : COLOR0 {
				return tex2D(_MainTex, i.texcoord0) * i.shadow_fade;
			}
			ENDCG
		}
	}
	SubShader {
		Tags { "Queue"="Transparent" "IgnoreProjector"="True" "RenderType"="Transparent" }
		Lighting Off
		//Cull Off
		ZTest Always
		ZWrite Off
		Fog { Mode Off }
		
		ColorMask A
		
		Pass {
			BlendOp Max
			Blend One One
			
			CGPROGRAM
			#pragma vertex vert
			#pragma fragment frag
			
			#include "UnityCG.cginc"
			
			fixed4 _Color;
			sampler2D _MainTex;
			uniform float4 _MainTex_ST; // Needed for TRANSFORM_TEX(v.texcoord0, _MainTex)
			
			struct appdata {
				float4 vertex : POSITION;
				float2 texcoord0 : TEXCOORD0;
			};
			
			struct v2f {
				float4 pos : SV_POSITION;
				float2 texcoord0 : TEXCOORD0;
				float shadow_fade : TEXCOORD1;
			};
			
			v2f vert(appdata v) {
				v2f o;
				o.pos = mul(UNITY_MATRIX_MVP, v.vertex);
				o.pos.z = UNITY_MATRIX_MVP[2][3];
				o.texcoord0 = TRANSFORM_TEX(v.texcoord0, _MainTex);
				o.shadow_fade = 1 - saturate(o.pos.z);
				return o;
			}
			
			fixed4 frag(v2f i) : COLOR0 {
				return tex2D(_MainTex, i.texcoord0) * _Color * i.shadow_fade;
			}
			ENDCG
		}
	}
	FallBack "Unlit/Transparent"
}
