﻿Shader "Custom/MaskedTextures_A" {
	Properties {
		_Color ("Color Tint", Color) = (1,1,1,1)
		_MainTex ("Base (RGB) Alpha (A)", 2D) = "white" {}
		_MaskedColor ("Masked Color Tint", Color) = (1,1,1,1)
		_MaskedTex ("Masked (RGB) Alpha (A)", 2D) = "black" {}
		_PosScale ("Pos & Scale", Vector) = (0,0,1,1)
		_Angle ("Angle", Float) = 0.0
		_MaskedColor2 ("Masked Color Tint", Color) = (1,1,1,1)
		_MaskedTex2 ("Masked (RGB) Alpha (A)", 2D) = "black" {}
		_PosScale2 ("Pos & Scale", Vector) = (0,0,1,1)
		_Angle2 ("Angle", Float) = 0.0
		_MaskedColor3 ("Masked Color Tint", Color) = (1,1,1,1)
		_MaskedTex3 ("Masked (RGB) Alpha (A)", 2D) = "black" {}
		_PosScale3 ("Pos & Scale", Vector) = (0,0,1,1)
		_Angle3 ("Angle", Float) = 0.0
	}
	SubShader {
		Tags { "Queue"="Transparent" "IgnoreProjector"="True" "RenderType"="Transparent" }
		Lighting Off
		//Cull Off
		ZTest Always
		ZWrite Off
		Fog { Mode Off }
		
		Pass {
			Blend SrcAlpha OneMinusSrcAlpha, One One
			
			CGPROGRAM
			#pragma vertex vert
			#pragma fragment frag
			
			#include "UnityCG.cginc"
			
			fixed4 _Color;
			sampler2D _MainTex;
			uniform float4 _MainTex_ST;
			uniform float4 _MainTex_TexelSize;
			
			fixed4 _MaskedColor;
			sampler2D _MaskedTex;
			uniform float4 _MaskedTex_ST;
			uniform float4 _MaskedTex_TexelSize;
			
			float4 _PosScale;
			float _Angle;
			
			fixed4 _MaskedColor2;
			sampler2D _MaskedTex2;
			uniform float4 _MaskedTex2_ST;
			uniform float4 _MaskedTex2_TexelSize;
			
			float4 _PosScale2;
			float _Angle2;
			
			fixed4 _MaskedColor3;
			sampler2D _MaskedTex3;
			uniform float4 _MaskedTex3_ST;
			uniform float4 _MaskedTex3_TexelSize;
			
			float4 _PosScale3;
			float _Angle3;
			
			// Declare this next to your sampler:
			// float4 _MaskedTex_TexelSize;
			// Unity will populate it with (1/width, 1/height, width, height) for the texture.
			// If using D3D and anti-aliasing in an image effect, the y value will be negative
			// for _MainTex. You can use this to get the texel size/dimensions of any texture
			// assigned by Unity. Unfortunately, this feature has never been documented except
			// for a passing mention in Platform-specific Rendering Differences.
			
			struct appdata {
				float4 vertex : POSITION;
				float2 texcoord0 : TEXCOORD0;
			};
			
			struct v2f {
				float4 pos : SV_POSITION;
				float2 texcoord0 : TEXCOORD0;
				float2 texcoord1 : TEXCOORD1;
				float2 texcoord2 : TEXCOORD2;
				float2 texcoord3 : TEXCOORD3;
			};
			
			v2f vert(appdata v) {
				float4 abs_pos = mul(_Object2World, v.vertex);
				float4 rel_pos = mul(_World2Object, abs_pos);
				
				v2f o;
				o.pos = mul(UNITY_MATRIX_MVP, v.vertex);
				o.texcoord0 = TRANSFORM_TEX(v.texcoord0, _MainTex);
				
				float _sin, _cos;
				sincos(radians(_Angle), _sin, _cos);
				float2x2 texmat = { {_cos, _sin}, {-_sin, _cos} };
				float2 tex_xy = mul(texmat, rel_pos.xy - _PosScale.xy);
				o.texcoord1 = tex_xy / (_MaskedTex_TexelSize.zw * _PosScale.zw);
				
				float _sin2, _cos2;
				sincos(radians(_Angle2), _sin2, _cos2);
				float2x2 texmat2 = { {_cos2, _sin2}, {-_sin2, _cos2} };
				float2 tex_xy2 = mul(texmat2, rel_pos.xy - _PosScale2.xy);
				o.texcoord2 = tex_xy2 / (_MaskedTex2_TexelSize.zw * _PosScale2.zw);
				
				float _sin3, _cos3;
				sincos(radians(_Angle3), _sin3, _cos3);
				float2x2 texmat3 = { {_cos3, _sin3}, {-_sin3, _cos3} };
				float2 tex_xy3 = mul(texmat3, rel_pos.xy - _PosScale3.xy);
				o.texcoord3 = tex_xy3 / (_MaskedTex3_TexelSize.zw * _PosScale3.zw);
//				fixed2 mt = o.texcoord1;
//				mt.x = frac(mt.x + 50);
				//mt.y = frac(mt.y + 10);
//				o.texcoord1 = mt;
				return o;
			}
			
			const half a_min = 1.0/255.0;
			fixed4 frag(v2f i) : COLOR0 {
				fixed4 main_color = tex2D(_MainTex, i.texcoord0);
				fixed mask_a = main_color.a;
				main_color *= _Color;
				
				fixed2 uv = fixed2(frac(i.texcoord1.x + 50), i.texcoord1.y);
				fixed4 masked_color = tex2D(_MaskedTex, uv) * _MaskedColor;
				masked_color.a *= mask_a * (i.texcoord1.y >= 0) * (i.texcoord1.y < 1);
				
				fixed2 uv2 = fixed2(frac(i.texcoord2.x + 50), i.texcoord2.y);
				fixed4 masked_color2 = tex2D(_MaskedTex2, uv2) * _MaskedColor2;
				masked_color2.a *= mask_a * (i.texcoord2.y >= 0) * (i.texcoord2.y < 1);
				
				fixed2 uv3 = fixed2(frac(i.texcoord3.x + 50), i.texcoord3.y);
				fixed4 masked_color3 = tex2D(_MaskedTex3, uv3) * _MaskedColor3;
				masked_color3.a *= mask_a * (i.texcoord3.y >= 0) * (i.texcoord3.y < 1);
				
				fixed3 rgb;
				half a, kIN, kBKG, kIN2, kBKG2, kIN3, kBKG3;
				
				rgb = main_color.rgb;
				a = main_color.a;
				
				kIN = masked_color.a;
				kBKG = a * (1 - kIN);
				a = kIN + kBKG;
				rgb = (masked_color.rgb * kIN + rgb * kBKG) / max(a, a_min);
				
				kIN2 = masked_color2.a;
				kBKG2 = a * (1 - kIN2);
				a = kIN2 + kBKG2;
				rgb = (masked_color2.rgb * kIN2 + rgb * kBKG2) / max(a, a_min);
				
				kIN3 = masked_color3.a;
				kBKG3 = a * (1 - kIN3);
				a = kIN3 + kBKG3;
				rgb = (masked_color3.rgb * kIN3 + rgb * kBKG3) / max(a, a_min);
				
				return fixed4(rgb, a);
			}
			ENDCG
		}
	}
	FallBack "Unlit/Transparent"
}
