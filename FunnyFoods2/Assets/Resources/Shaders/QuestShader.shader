﻿Shader "Andrew/QuestShader" {
	Properties {
		_MainTex ("LineTex", 2D) = "white" {}
		scale_Center ("LineTexWidth", vector) = (0.2, 0.2, 0.5, 0.5)
	}
 	Category 
    {
		SubShader 
		{	
		Tags { "Queue"="Transparent" "IgnoreProjector"="True" "RenderType"="Transparent" }
		Pass
		{
		Blend SrcAlpha OneMinusSrcAlpha
		CGPROGRAM
			#pragma vertex vert
			#pragma fragment frag
			#include "UnityCG.cginc"
		sampler2D _MainTex;
		float4 _MainTex_ST;
		float4 scale_Center;

		struct v2f
			{
				float4  pos : SV_POSITION;
				float2  uv : TEXCOORD0;
			};
 		
 		v2f vert (appdata_base v)
			{
				v2f o;
				o.pos = mul (UNITY_MATRIX_MVP, v.vertex);
				o.uv = TRANSFORM_TEX (v.texcoord, _MainTex);
				fixed x = (o.uv.x - scale_Center.z) / scale_Center.x + 0.5;
				fixed y = (o.uv.y - scale_Center.w) / scale_Center.y + 0.5;
				o.uv = fixed2(x,y);
				return o;
			}
		
		half4 frag (v2f i) : COLOR 
		{
		half4 c = tex2D (_MainTex, i.uv);
		c = lerp(fixed4(0,0,0,1), fixed4(0,0,0,0), c.a);
		return c;
		}
		ENDCG
	}
	}
	}
}
