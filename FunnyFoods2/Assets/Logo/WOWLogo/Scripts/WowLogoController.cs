﻿using UnityEngine;
using System.Collections;

/// <summary>
/// Скрипт для управления WOWLogo
/// </summary>
[RequireComponent(typeof(Animator))]
[DisallowMultipleComponent]
public class WowLogoController : MonoBehaviour 
{
    #region Fields

    const string animStateEnd = "End";
	const string animStop = "Stop";
	const string animStart = "Start";
	
	private Animator _thisAnimator;
	private AudioSource _thisSound;
	private Collider _thisCollader;
	private Renderer[] _renderers;
	private TextMesh _errorMessage;

    #endregion

    #region Async load level

    private IEnumerator AsyncStartAnim()
    {
        if (_thisAnimator != null) yield break;

        yield return new WaitForSeconds(1.0f);

        _thisAnimator = GetComponent<Animator>();
        _thisSound = GetComponent<AudioSource>();
        if (_renderers != null)
            for (int i = 0; i < _renderers.Length; i++) _renderers[i].enabled = true;

        if (_thisAnimator != null) _thisAnimator.SetTrigger(animStart);
        if (_thisSound != null) _thisSound.Play();
        if (_thisCollader != null) _thisCollader.enabled = true;
        yield break;
    }

	private IEnumerator AsyncLoadLevel()
	{
		if (_thisAnimator == null) yield break;
		_thisAnimator = null;	

		// неявная инициализация сервисов MageApp
		var appState= AppState.instance;				
		yield return new WaitForSeconds(AppConfig.timeWaitRunServices);

        //=====================================================
        // ЭТО БОЛЬШОЙ КОСТЫЛЬ Из-ЗА БАГА в WWW  -- 2015.05.30
#if MAGE_DEBUG
		Debug.Log("AsyncLoadLevel: cross state: ".AddTime() + CPStateController.UpdateState.ToString());
#endif
        float t = 5.0f;		
		while (CPStateController.UpdateState != DLCLoader.LoadingState.Ready &&
			CPStateController.UpdateState != DLCLoader.LoadingState.Stop && t > 0.0f)
		{
			yield return new WaitForSeconds(0.5f);
            t -= 0.5f;
#if MAGE_DEBUG
			Debug.Log("AsyncLoadLevel: cross loading: cross state: ".AddTime() + CPStateController.UpdateState.ToString() + " time: " + t);
#endif

        }        
        //=====================================================
        appState.LoadScene(AppSettings.instance.Scene.MainIndex);
#if MAGE_DEBUG
		Debug.Log("AsyncLoadLevel: end load : cross state: ".AddTime() + CPStateController.UpdateState.ToString());
#endif
        yield break;
	}

    #endregion

    #region OBB Downlod

#if MAGE_OBB && UNITY_ANDROID && !UNITY_EDITOR

    private void OnApplicationPause(bool pauseStatus)
	{
		if(_loadLevelAfterObbDownload && !pauseStatus)
		{
			_loadLevelAfterObbDownload = false;
			enabled = true;
	 		StartCoroutine(AsyncStartAnim());
		}
	}

	bool _loadLevelAfterObbDownload = false;
	private void CheckOBB()
	{
		string expPath = GooglePlayDownloader.GetExpansionFilePath();
		if (expPath == null)
		{
			enabled = false;			
			StartCoroutine(WaitQuit(5));
		}
		else
		{
			string mainPath = GooglePlayDownloader.GetMainOBBPath(expPath);
			if (mainPath == null)
			{
				enabled = false;				
				_loadLevelAfterObbDownload = true;
				GooglePlayDownloader.FetchOBB();
				return;
			}
		}
	}   
    
    private IEnumerator WaitQuit(int timeToWait)
	{
        string msg = "Cannot connect to Store!\r\nPlease check your internet connection\r\nand try again!\r\nClose application over {0} sec.";
		if( Application.systemLanguage ==  SystemLanguage.Russian || Application.systemLanguage ==  SystemLanguage.Ukrainian)
		{
			msg = "Невозможно загрузить ресурсы приложения.\r\n" +
				  "Проверьте подключение к интернету \r\nи повторите попытку позже.\n\n" +
				  "Выход из приложения будет осуществлен\r\nчерез {0} сек.";
		}

		while (timeToWait > 0)
		{
			yield return new WaitForSeconds(1.0f);
			timeToWait--;
			if (_errorMessage != null) _errorMessage.text = string.Format(msg, timeToWait);
		}
		Application.Quit();
        yield break;
	}

#endif

    #endregion

    #region Unity event functions
    void Awake()
	{
		_thisAnimator = null;
		_errorMessage = null;
		_thisCollader = GetComponent<Collider>();
		if (_thisCollader != null) _thisCollader.enabled = false;

		_renderers = GetComponentsInChildren<Renderer>();
		if (_renderers != null)
			for (int i = 0; i < _renderers.Length; i++) _renderers[i].enabled = false;

		var tmpGo = GameObject.Find("errorMessage");
		if (tmpGo != null) _errorMessage = tmpGo.GetComponent<TextMesh>();		

#if MAGE_OBB && UNITY_ANDROID && !UNITY_EDITOR
		if(AppSettings.instance.appInfo.splitBinary) CheckOBB();
#else
        StartCoroutine(AsyncStartAnim());
#endif
    }

    void OnMouseDown()
	{        
        if (_thisAnimator != null ) _thisAnimator.SetTrigger(animStop);
		if (_thisSound != null) _thisSound.Stop();
	}

	void Update()
	{		
		if(_thisAnimator == null) return;
		if (_thisAnimator.GetCurrentAnimatorStateInfo(0).IsName(animStateEnd))
			StartCoroutine(AsyncLoadLevel());
	}

	#endregion
}
