﻿using UnityEngine;
using System.Collections;
using HutongGames.PlayMaker;
public class RM_AMBAR_SwitchShootMode : MonoBehaviour 
{
	public float continuance = 10;
	public GameObject bullet;
	bool enabled = false;
	Vector3 targetPos;
	GameObject bulletInstance;
    float time;

    void Start()
    {
        time = 0;
    }

	void OnMouseDownMT()
	{
        RM_StrangeMovement strangeMovment = transform.GetComponent<RM_StrangeMovement>();
		Object.Destroy(strangeMovment);
		transform.GetComponent<RM_AMBAR_SplashEffect>().enabled = true;
        FsmVariables.GlobalVariables.GetFsmBool("RM/AMB/LEVELS/isShootModeEnabled").Value = true;
        enabled = true;
    }

	void Update()
	{
		if(enabled)
		{
            time += Time.deltaTime;
			if(InputMT.GetMouseButtonDown(0))
			{
				targetPos = Camera.main.ScreenPointToRay(Input.mousePosition).origin;
				targetPos.z = 1;
				FsmVariables.GlobalVariables.GetFsmVector3("RM/AMB/LEVELS/targetPomidorkaPos").Value = targetPos;
				bulletInstance = GameObject.Instantiate(bullet);
				bulletInstance.transform.position = new Vector3(0,-1300,1);
				bulletInstance.SetActive(true);
			}
            if(time >= continuance)
            {
                enabled = false;
                FsmVariables.GlobalVariables.GetFsmBool("RM/AMB/LEVELS/isShootModeEnabled").Value = false;
                GameObject.Destroy(transform.gameObject);
            }
		}
	}
}
