﻿using UnityEngine;
using System.Collections;
namespace HutongGames.PlayMaker.Actions
{
	public class RM_AMBAR_ChooseParentForHidePoint : FsmStateAction 
	{
		public FsmString rightName;
		public FsmGameObject rightParent, wrongParent;

		public override void OnEnter ()
		{
			if (!rightParent.IsNone && !wrongParent.IsNone && rightParent.Value && wrongParent.Value && rightName.Value.Length > 0) 
			{
				if (rightName.Value == Owner.name)
					Owner.transform.parent.parent = rightParent.Value.transform;
				else
					Owner.transform.parent.parent = wrongParent.Value.transform;
			}
			Finish ();
		}
	}
}