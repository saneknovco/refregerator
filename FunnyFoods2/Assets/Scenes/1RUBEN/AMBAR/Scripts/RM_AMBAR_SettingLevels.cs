﻿using UnityEngine;
using System.Collections;

namespace HutongGames.PlayMaker.Actions
{
    [ActionCategory("RM_AmbarGame")]
    public class RM_AMBAR_SettingLevels : FsmStateAction
    {
        [ActionSection("Right food settings")]
        public FsmInt rightFoodTypesCount = new FsmInt() { UseVariable = false };
		public FsmInt rightFoodCopiesFrom = new FsmInt() { UseVariable = false };
		public FsmInt rightFoodCopiesTo = new FsmInt() { UseVariable = false };
        public FsmBool isSameColorForRightFood = new FsmBool() { UseVariable = false };
        [ActionSection("Secondary food settings")]
        public FsmInt secondaryFoodTypesCount = new FsmInt() { UseVariable = false };
		public FsmInt wrongFoodCopiesFrom = new FsmInt() { UseVariable = false };
		public FsmInt wrongFoodCopiesTo = new FsmInt() { UseVariable = false };
        public FsmBool isSimilarColorForSecondaryFood = new FsmBool() { UseVariable = false };
        [ActionSection("Fogger")]
        public FsmGameObject fogger;
        public FsmBool foggerIsActive = new FsmBool() { UseVariable = false };
        [ActionSection("Other settings")]
        public FsmBool isChangePositionAfterHide = new FsmBool() { UseVariable = false };
		public FsmFloat waitingBeforeHide = new FsmFloat() { UseVariable = false };
		public FsmFloat waitingBeforeLookOutFROM = new FsmFloat() { UseVariable = false };
		public FsmFloat waitingBeforeLookOutTO = new FsmFloat() { UseVariable = false };
		public FsmFloat lookHideAnimSpeed = new FsmFloat() { UseVariable = false };

        public override void Reset()
        {
            rightFoodTypesCount.Value = 1;
            secondaryFoodTypesCount.Value = 1;
            isSameColorForRightFood.Value = false;
            isSimilarColorForSecondaryFood.Value = false;
            foggerIsActive.Value = false;
            isChangePositionAfterHide.Value = false;
			waitingBeforeHide.Value = 1.5f;
			waitingBeforeLookOutFROM.Value = 3f;			
			waitingBeforeLookOutTO.Value = 4.5f;
			lookHideAnimSpeed.Value = 0.85f;
        }

        public override void OnEnter()
        {
            FsmVariables.GlobalVariables.GetFsmInt("RM/AMB/LEVELS/NeedGamesCount").Value = rightFoodTypesCount.Value;
			FsmVariables.GlobalVariables.GetFsmInt("RM/AMB/LEVELS/TotalRightCopiesCount/From").Value = rightFoodCopiesFrom.Value;
			FsmVariables.GlobalVariables.GetFsmInt("RM/AMB/LEVELS/TotalRightCopiesCount/To").Value = rightFoodCopiesTo.Value;

            FsmVariables.GlobalVariables.GetFsmInt("RM/AMB/LEVELS/WrongFoodCount").Value = secondaryFoodTypesCount.Value;
			FsmVariables.GlobalVariables.GetFsmInt("RM/AMB/LEVELS/TotalWrongCopiesCount/From").Value = wrongFoodCopiesFrom.Value;
			FsmVariables.GlobalVariables.GetFsmInt("RM/AMB/LEVELS/TotalWrongCopiesCount/To").Value = wrongFoodCopiesTo.Value;

            FsmVariables.GlobalVariables.GetFsmBool("RM/AMB/LEVELS/SameRightFoodColor").Value = isSameColorForRightFood.Value;
            FsmVariables.GlobalVariables.GetFsmBool("RM/AMB/LEVELS/SimilarWrongFoodColor").Value = isSimilarColorForSecondaryFood.Value;
            FsmVariables.GlobalVariables.GetFsmBool("RM/AMB/LEVELS/DifferentPos").Value = isChangePositionAfterHide.Value;

			FsmVariables.GlobalVariables.GetFsmFloat("RM/AMB/LEVELS/WaitingBeforeHide").Value = waitingBeforeHide.Value;
			FsmVariables.GlobalVariables.GetFsmFloat("RM/AMB/LEVELS/WaitingBeforeLookOutFROM").Value = waitingBeforeLookOutFROM.Value;
			FsmVariables.GlobalVariables.GetFsmFloat("RM/AMB/LEVELS/WaitingBeforeLookOutTO").Value = waitingBeforeLookOutTO.Value;
			FsmVariables.GlobalVariables.GetFsmFloat("RM/AMB/LEVELS/LookHideAnimSpeed").Value = lookHideAnimSpeed.Value;

            fogger.Value.SetActive(foggerIsActive.Value);

            Finish();
        }
    }
}
