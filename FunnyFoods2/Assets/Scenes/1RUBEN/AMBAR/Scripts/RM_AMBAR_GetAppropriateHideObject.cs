﻿using UnityEngine;
using System.Collections;

public class RM_AMBAR_GetAppropriateHideObject : MonoBehaviour 
{
	public GameObject appropriateHideObject;
    public int zDistance = 5;
    void Awake()
    {
        transform.position = new Vector3(transform.position.x, transform.position.y, appropriateHideObject.transform.position.z + zDistance);
    }
}