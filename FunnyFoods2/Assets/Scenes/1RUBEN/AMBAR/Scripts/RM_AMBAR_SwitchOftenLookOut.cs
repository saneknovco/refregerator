﻿using UnityEngine;
using System.Collections;
using HutongGames.PlayMaker;
public class RM_AMBAR_SwitchOftenLookOut : MonoBehaviour 
{
	public float continuance = 10, frequancyMultiplier= 1.5f;
	void SwitchFrequencyLookOut(float frequancyMultiplier, bool isFaster)
	{
		if(isFaster)
		{
			FsmVariables.GlobalVariables.GetFsmFloat("RM/AMB/LEVELS/WaitingBeforeLookOutFROM").Value /= frequancyMultiplier;
			FsmVariables.GlobalVariables.GetFsmFloat("RM/AMB/LEVELS/WaitingBeforeLookOutTO").Value /= frequancyMultiplier;
		}
		else
		{
			FsmVariables.GlobalVariables.GetFsmFloat("RM/AMB/LEVELS/WaitingBeforeLookOutFROM").Value *= frequancyMultiplier;
			FsmVariables.GlobalVariables.GetFsmFloat("RM/AMB/LEVELS/WaitingBeforeLookOutTO").Value *= frequancyMultiplier;
		}
	}

	IEnumerator MakeOftenLookOutCoroutine()
	{
		SwitchFrequencyLookOut(frequancyMultiplier, true);
		yield return new WaitForSeconds(continuance);
		SwitchFrequencyLookOut(frequancyMultiplier, false);
		GameObject.Destroy(transform.gameObject);
	}
	void OnMouseDownMT()
	{
		StartCoroutine("MakeOftenLookOutCoroutine");
		RM_StrangeMovement strangeMovment = transform.GetComponent<RM_StrangeMovement>();
		Object.Destroy(strangeMovment);
		transform.GetComponent<RM_AMBAR_SplashEffect>().enabled = true;
	}
}
