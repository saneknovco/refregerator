﻿using UnityEngine;
using System.Collections;

namespace HutongGames.PlayMaker.Actions
{
    [ActionCategory("RM_AmbarGame")]
    public class RM_AMBAR_SetDeltaPosToMove : FsmStateAction 
	{
		public FsmVector3 finishPos = new FsmVector3() { UseVariable = true };
		public FsmFloat dX,dY;
		public FsmString sideToLook;
		public FsmVector3 storeNewFinishPos = new FsmVector3() { UseVariable = true };
		public override void OnEnter ()
		{
			if(sideToLook.Value == "RS") storeNewFinishPos.Value = new Vector3(finishPos.Value.x + dX.Value, finishPos.Value.y, finishPos.Value.z);
			if(sideToLook.Value == "LS") storeNewFinishPos.Value = new Vector3(finishPos.Value.x - dX.Value, finishPos.Value.y, finishPos.Value.z);
			if(sideToLook.Value == "TS") storeNewFinishPos.Value = new Vector3(finishPos.Value.x, finishPos.Value.y + dY.Value, finishPos.Value.z);
			if(sideToLook.Value == "BS") storeNewFinishPos.Value = new Vector3(finishPos.Value.x, finishPos.Value.y - dY.Value, finishPos.Value.z);
			if(sideToLook.Value == "TR") storeNewFinishPos.Value = new Vector3(finishPos.Value.x + dX.Value, finishPos.Value.y + dY.Value, finishPos.Value.z);
			if(sideToLook.Value == "TL") storeNewFinishPos.Value = new Vector3(finishPos.Value.x - dX.Value, finishPos.Value.y + dY.Value, finishPos.Value.z);
			if(sideToLook.Value == "BR") storeNewFinishPos.Value = new Vector3(finishPos.Value.x + dX.Value, finishPos.Value.y - dY.Value, finishPos.Value.z);
			if(sideToLook.Value == "BL") storeNewFinishPos.Value = new Vector3(finishPos.Value.x - dX.Value, finishPos.Value.y - dY.Value, finishPos.Value.z);

			Finish ();
		}
	}
}