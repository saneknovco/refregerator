﻿using UnityEngine;
using System.Collections;

namespace HutongGames.PlayMaker.Actions
{
    public class RM_AMBAR_LevelChooser : FsmStateAction
    {
        public FsmGameObject fogger, bonusGenerator, light;
        
        void SettingLevel(int rightFoodTypesCount = 1, int rightFoodCopiesFrom = 3, int rightFoodCopiesTo = 3, int secondaryFoodTypesCount = 0, int wrongFoodCopiesFrom = 0, int wrongFoodCopiesTo = 0, 
			bool isSameColorForRightFood = false, bool isSimilarColorForSecondaryFood = false, bool isChangePositionAfterHide = false, bool foggerIsActive = false, bool bonusIsActive = false, bool lightIsActive = false,
                          float waitingBeforeHide = 1.5f, float waitingBeforeLookOutFROM = 1.7f, float waitingBeforeLookOutTO = 2f, float lookHideAnimSpeed = 1f)
        {
            //количество типов правильной еды за игру (сколько раз приедет телега)
            FsmVariables.GlobalVariables.GetFsmInt("RM/AMB/LEVELS/NeedGamesCount").Value = rightFoodTypesCount;
            //количество правильной еды каждого типа
            FsmVariables.GlobalVariables.GetFsmInt("RM/AMB/LEVELS/TotalRightCopiesCount/From").Value = rightFoodCopiesFrom;
            FsmVariables.GlobalVariables.GetFsmInt("RM/AMB/LEVELS/TotalRightCopiesCount/To").Value = rightFoodCopiesTo;
            //количество типов побочной еды (она только мешает)
            FsmVariables.GlobalVariables.GetFsmInt("RM/AMB/LEVELS/WrongFoodCount").Value = secondaryFoodTypesCount;
            //количество побочной еды каждого типа
            FsmVariables.GlobalVariables.GetFsmInt("RM/AMB/LEVELS/TotalWrongCopiesCount/From").Value = wrongFoodCopiesFrom;
            FsmVariables.GlobalVariables.GetFsmInt("RM/AMB/LEVELS/TotalWrongCopiesCount/To").Value = wrongFoodCopiesTo;
            //вся правельная еда будет одного цвета (могут быть проблемы)
            FsmVariables.GlobalVariables.GetFsmBool("RM/AMB/LEVELS/SameRightFoodColor").Value = isSameColorForRightFood;
            //для каждой правильной еды будет подоброна побочная еда такого же цвета
            FsmVariables.GlobalVariables.GetFsmBool("RM/AMB/LEVELS/SimilarWrongFoodColor").Value = isSimilarColorForSecondaryFood;
            //после прятания появится из другой позиции
            FsmVariables.GlobalVariables.GetFsmBool("RM/AMB/LEVELS/DifferentPos").Value = isChangePositionAfterHide;
            //фонарик
            FsmVariables.GlobalVariables.GetFsmBool("RM/AMB/LEVELS/isLightEnabled").Value = lightIsActive;
            //выглянул и ждет столько то времени
            FsmVariables.GlobalVariables.GetFsmFloat("RM/AMB/LEVELS/WaitingBeforeHide").Value = waitingBeforeHide;
            //спрятался и выглянет через такое то время
            FsmVariables.GlobalVariables.GetFsmFloat("RM/AMB/LEVELS/WaitingBeforeLookOutFROM").Value = waitingBeforeLookOutFROM;
            FsmVariables.GlobalVariables.GetFsmFloat("RM/AMB/LEVELS/WaitingBeforeLookOutTO").Value = waitingBeforeLookOutTO;
            //скорость анимации выглядывания/заглядывания
            FsmVariables.GlobalVariables.GetFsmFloat("RM/AMB/LEVELS/LookHideAnimSpeed").Value = lookHideAnimSpeed;

            fogger.Value.SetActive(foggerIsActive);
			bonusGenerator.Value.SetActive(bonusIsActive);
			light.Value.SetActive (lightIsActive);
        }
        public override void OnEnter()
        {
            int level = FsmVariables.GlobalVariables.GetFsmInt("RM/AMB/LevelNum").Value;
            level = (level == 0 || level > 15) ? 1 : level;
            FsmVariables.GlobalVariables.GetFsmInt("RM/AMB/LevelNum").Value = level;
            Debug.LogWarning(System.DateTime.Now.ToString() + ": Now playing - " + level.ToString());
            switch (level.ToString())
            {
                case "1":
                    {
                        SettingLevel();
                    }
                    break;
                case "2":
                    {
                        SettingLevel(1, 3, 5, 1, 2, 4);
                    }
                    break;
                case "3":
                    {
                        SettingLevel(2, 6, 9);
                    }
                    break;
                case "4":
                    {
						SettingLevel(2, 6, 9, 2, 2, 4, false, false, false, false, true);
					}
                    break;
                case "5":
                    {
						SettingLevel(2, 6, 9, 2, 2, 4, true, false, false, false, true);
                    }
                    break;
                case "6":
                    {
						SettingLevel(3, 8, 12, 0, 0, 0, false, false, false, false, true, false, 1.2f, 1.5f, 1.7f, 0.9f);
                    }
                    break;
                case "7":
                    {
						SettingLevel(3, 8, 12, 2, 2, 4, false, true, false, false, true, false, 1.2f, 1.5f, 1.7f, 0.9f);
                    }
                    break;
                case "8":
                    {
						if (!light.IsNone && light.Value)
                        	SettingLevel(2, 6, 9, 0, 0, 0, true, false, false, false, true, true, 1.2f, 1.5f, 1.7f, 0.9f);
                    }
                    break;
                case "9":
                    {
                        if (!fogger.IsNone && fogger.Value)
                            SettingLevel(3, 8, 12, 0, 0, 0, false, false, false, true, true, false, 1.2f, 1.5f, 1.7f, 0.9f);
                    }
                    break;
                case "10":
                    {
						SettingLevel(2, 6, 9, 2, 2, 4, false, true, true, false, true, false, 1.2f, 1.5f, 1.7f, 0.9f);
                    }
                    break;
                case "11":
                    {
						SettingLevel(3, 8, 12, 2, 2, 4, false, true, true, false, true, false, 1.1f, 1.3f, 1.5f, 0.85f);
                    }
                    break;
                case "12":
                    {
						if (!light.IsNone && light.Value)
							SettingLevel(3, 8, 12, 2, 2, 4, false, true, true, false, true, true, 1.1f, 1.3f, 1.5f, 0.85f);
                    }
                    break;
                case "13":
                    {
                        if (!fogger.IsNone && fogger.Value)
							SettingLevel(3, 8, 12, 2, 2, 4, false, true, true, true, true, false, 1.1f, 1.3f, 1.5f, 0.85f);
                    }
                    break;
                case "14":
                    {
						SettingLevel(3, 10, 13, 2, 2, 4, true, true, true, false, true, false, 1.1f, 1.3f, 1.5f, 0.85f);
                    }
                    break;
                case "15":
                    {
						if (!fogger.IsNone && fogger.Value && !light.IsNone && light.Value)
							SettingLevel(3, 10, 13, 2, 2, 4, true, true, true, true, true, true, 1.1f, 1.3f, 1.5f, 0.85f);
                    }
                    break;
                default:
                    {
                        SettingLevel();
                    }
                    break;
            }
        }
    }
}