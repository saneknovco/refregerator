﻿using UnityEngine;
using System.Collections;

public class RM_AMBAR_SplashEffect : MonoBehaviour {

	public float unscaleTime = 0.5f;
	float timeForBig, timeForSmall;
	float startScale, bigScale, currentScale;
	void Start () 
	{
		timeForBig = unscaleTime * 0.1f;
		timeForSmall = unscaleTime * 0.9f;
		startScale = transform.localScale.x;
		bigScale = startScale * 1.15f;
	}
	float timer = 0, timerForBig = 0, timerForSmall = 0;
	void Update () 
	{
		if((1 - timerForBig / timeForBig) > Time.deltaTime)
		{
			currentScale = Mathf.Lerp(startScale, bigScale, timerForBig / timeForBig);
			transform.localScale = new Vector3(currentScale,currentScale,transform.localScale.z);
			timerForBig += Time.deltaTime;
		}
		else
		{
			if((1 - timerForSmall / timeForSmall) > Time.deltaTime)
			{
				currentScale = Mathf.Lerp(bigScale, 0f, timerForSmall / timeForSmall);
				transform.localScale = new Vector3(currentScale,currentScale,transform.localScale.z);
				timerForSmall += Time.deltaTime;
			}
			else
			{
				transform.localScale = new Vector3(0, 0, transform.localScale.z);
				GameObject splash = transform.gameObject.FindChild("PS_Splash");
				splash.SetActive(true);
				if(timer >= 1)
				{
					GameObject.Destroy(splash);
					GameObject.Destroy(transform.GetComponent<RM_AMBAR_SplashEffect>());
					GameObject.Destroy(transform.gameObject.FindChild("PS_Tail"));
				}
				timer += Time.deltaTime;

			}
		}
	}
}
