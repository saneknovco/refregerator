﻿using UnityEngine;
using System.Collections;

namespace HutongGames.PlayMaker.Actions
{
	public class RM_AMBAR_CreateFoodForGame : FsmStateAction
	{
		public FsmGameObject rightBucket = new FsmGameObject() { UseVariable = false };
		public FsmGameObject rightReadyBucket = new FsmGameObject() { UseVariable = false };
		public FsmGameObject wrongBucket = new FsmGameObject() { UseVariable = false };
		public FsmGameObject wrongReadyBucket = new FsmGameObject() { UseVariable = false };
		public FsmGameObject trash = new FsmGameObject() { UseVariable = false };
		public FsmGameObject telegaNeedsFood = new FsmGameObject() { UseVariable = false };
		public FsmInt storeRightFoodCount = new FsmInt() { UseVariable = true }, storeWrongFoodCount = new FsmInt() { UseVariable = true };
		int rightTypes, totalRightNeedCount, totalRightNeedCountFrom, totalRightNeedCountTo, wrongTypes, totalWrongNeedCount, totalWrongNeedCountFrom, totalWrongNeedCountTo;
		int [] rightFoodCounts;
		bool sameRightColor, similarWrongColor, itIsOkay = false;
		GameObject colorBucket, currentFood, newFood;
		GameObject GetRandomChild(GameObject parent)
		{
			int childCount = parent.transform.childCount;
			if(childCount > 0)
			{
				if(childCount == 1) return parent.transform.GetChild(0).gameObject;
				else return parent.transform.GetChild(Random.Range(0, childCount)).gameObject;
			}
			return null;
		}
		void InitVariablesFromGlobal()
		{
			rightTypes = FsmVariables.GlobalVariables.GetFsmInt("RM/AMB/LEVELS/NeedGamesCount").Value;
			totalRightNeedCountFrom = FsmVariables.GlobalVariables.GetFsmInt("RM/AMB/LEVELS/TotalRightCopiesCount/From").Value;
			totalRightNeedCountTo = FsmVariables.GlobalVariables.GetFsmInt("RM/AMB/LEVELS/TotalRightCopiesCount/To").Value;
			
			wrongTypes = FsmVariables.GlobalVariables.GetFsmInt("RM/AMB/LEVELS/WrongFoodCount").Value;
			totalWrongNeedCountFrom = FsmVariables.GlobalVariables.GetFsmInt("RM/AMB/LEVELS/TotalWrongCopiesCount/From").Value;
			totalWrongNeedCountTo = FsmVariables.GlobalVariables.GetFsmInt("RM/AMB/LEVELS/TotalWrongCopiesCount/To").Value;
			
			sameRightColor = FsmVariables.GlobalVariables.GetFsmBool("RM/AMB/LEVELS/SameRightFoodColor").Value;
			similarWrongColor = FsmVariables.GlobalVariables.GetFsmBool("RM/AMB/LEVELS/SimilarWrongFoodColor").Value;
		}
		public override void OnEnter ()
		{
			InitVariablesFromGlobal();
			
			rightFoodCounts = new int[rightTypes];
			totalRightNeedCount = Random.Range(totalRightNeedCountFrom, totalRightNeedCountTo + 1);
			totalWrongNeedCount = Random.Range(totalWrongNeedCountFrom, totalWrongNeedCountTo + 1);

			totalRightNeedCount = totalRightNeedCount > (5 * rightTypes) ? 5 * rightTypes : totalRightNeedCount;
			
			if(!storeRightFoodCount.IsNone) storeRightFoodCount.Value = totalRightNeedCount;
			if(!storeWrongFoodCount.IsNone) storeWrongFoodCount.Value = totalWrongNeedCount;

			if(sameRightColor)
				colorBucket = GetRandomChild(Owner);
			int k = 0;

			for (int i = 0; i < rightTypes; i++)
			{
				if(!sameRightColor)
					colorBucket = GetRandomChild(Owner);

				currentFood = GetRandomChild(colorBucket);
				int currentCopiesCount = 0;
				int weHaveFood;
				int weHaveTypes;
				if(totalRightNeedCount > 5)
				{
					do
					{
						currentCopiesCount = Random.Range(1, 6);
						weHaveFood = totalRightNeedCount - currentCopiesCount;
						weHaveTypes = rightTypes - (i+1);
					}
					while((weHaveFood/weHaveTypes) > 5 || (weHaveFood/weHaveTypes) < 1);
				}
				else
				{
					if(rightTypes == (i+1))
					{
						currentCopiesCount = totalRightNeedCount;
					}
					else
					{
						do
						{
							currentCopiesCount = Random.Range(1, totalRightNeedCount+1);
							weHaveFood = totalRightNeedCount - currentCopiesCount;
							weHaveTypes = rightTypes - (i+1);
						}
						while((weHaveFood/weHaveTypes) < 1);
					}
				}
				totalRightNeedCount -= currentCopiesCount;
				rightFoodCounts[i] = currentCopiesCount;
				for (int j = 0; j <= currentCopiesCount; j++) 
				{
					newFood = GameObject.Instantiate(currentFood) as GameObject;
                    //setting food for telega
                    if (j == currentCopiesCount)
                    {
                        newFood.transform.GetChild(0).name = string.Format("{0}_{1}", currentCopiesCount, newFood.transform.GetChild(0).name);
                        foreach (PlayMakerFSM fsm in newFood.GetComponents<PlayMakerFSM>())
                            Object.Destroy(fsm);
                        Object.Destroy(newFood.GetComponent<Shadow2DCaster>());
                        Object.Destroy(newFood.GetComponent<Animation>());
                        newFood.SetActive(false);
                        newFood.transform.parent = telegaNeedsFood.Value.transform;
                    }
                    else
                    {
                        newFood.transform.parent = rightBucket.Value.transform;
                    }
                    newFood.name = currentFood.name;
				}
				currentFood.transform.parent = trash.Value.transform;
				if(wrongTypes > 0)
				{
					wrongTypes = sameRightColor && similarWrongColor ? 1 : wrongTypes;
					if(k < wrongTypes)
					{
						if(similarWrongColor)
						{
							if(wrongTypes == (k+1))
							{
								currentCopiesCount = totalWrongNeedCount;
							}
							else
							{
								do
								{
									currentCopiesCount = Random.Range(1, totalWrongNeedCount + 1);
									weHaveFood = totalWrongNeedCount - currentCopiesCount;
									weHaveTypes = wrongTypes - (k+1);
								}
								while(weHaveFood/weHaveTypes < 1);
							}
							currentFood = GetRandomChild(colorBucket);
							for (int j = 0; j < currentCopiesCount; j++) 
							{
								newFood = GameObject.Instantiate(currentFood) as GameObject;
								newFood.transform.parent = wrongBucket.Value.transform;
								newFood.name = currentFood.name;
							}
							totalWrongNeedCount -= currentCopiesCount;
							k++;
							currentFood.transform.parent = trash.Value.transform;
						}
					}
				}
				if(!sameRightColor)
					colorBucket.transform.parent = trash.Value.transform;

			}
			if(sameRightColor)
				colorBucket.transform.parent = trash.Value.transform;

			if(wrongTypes > 0 && !similarWrongColor)
			{
				for (k = 0; k < wrongTypes; k++) 
				{
					int currentCopiesCount = 0;
					int weHaveFood;
					int weHaveTypes;
					
					colorBucket = GetRandomChild(Owner);
					currentFood = GetRandomChild(colorBucket);
					if(wrongTypes == (k+1))
					{
						currentCopiesCount = totalWrongNeedCount;
					}
					else
					{
						do
						{
							currentCopiesCount = Random.Range(1, totalWrongNeedCount);
							weHaveFood = totalWrongNeedCount - currentCopiesCount;
							weHaveTypes = wrongTypes - (k+1);
						}
						while(weHaveFood/weHaveTypes < 1);
					}
					totalWrongNeedCount -= currentCopiesCount;
					for (int j = 0; j < currentCopiesCount; j++) 
					{
						newFood = GameObject.Instantiate(currentFood) as GameObject;
						newFood.transform.parent = wrongBucket.Value.transform;
						newFood.name = currentFood.name;
					}
					colorBucket.transform.parent = trash.Value.transform;
				}
			}
			Finish ();
		}
	}
}