﻿using UnityEngine;
using System.Collections;

public class RZ_SetVector4OnShader : MonoBehaviour 
{
	void Start () 
	{
		float size = Camera.main.orthographicSize;
		size /= (transform.GetComponent<MeshFilter> ().mesh.bounds.size.y / 2);
		transform.GetComponent<MeshRenderer>().material.SetVector("LineTexWidth", new Vector4(size, size, 0,0));
	}
}
