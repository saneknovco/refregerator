﻿using UnityEngine;
using System.Collections;

public class RM_VOD_TypeOfElementPositioning : MonoBehaviour 
{
	public TypeOfPositioning typeOfPositioning;
	public string TypeOfPositioning
	{
		get
		{
			return typeOfPositioning.ToString();
		}
	}
}
public enum TypeOfPositioning
{
	Side,
	Front
}
