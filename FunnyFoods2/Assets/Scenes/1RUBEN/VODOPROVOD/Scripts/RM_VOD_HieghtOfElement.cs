﻿using UnityEngine;
using System.Collections;

public class RM_VOD_HieghtOfElement : MonoBehaviour 
{
	public HeightOfElement heightOfElement;
	public string HeightOfElement
	{
		get
		{
			return heightOfElement.ToString();
		}
	}
}
public enum HeightOfElement
{
	Tall,
	Low
}
