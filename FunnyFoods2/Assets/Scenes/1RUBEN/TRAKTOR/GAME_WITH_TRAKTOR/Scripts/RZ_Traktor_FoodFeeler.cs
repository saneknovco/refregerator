﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

namespace HutongGames.PlayMaker.Actions
{
	public class RZ_Traktor_FoodFeeler : FsmStateAction 
	{
		public FsmGameObject traktor;
		public FsmGameObject foodKeeper;
		public FsmGameObject foodBucket;
		public FsmString code;
		public FsmInt mysteryVagonNumber;
		public bool randomMysteryVagon = true;

		List<GameObject> foodList;
		List<GameObject> positions;
		int codeLenght;
		string nameForDestroy;
		public override void OnEnter ()
		{
			codeLenght = code.Value.Length;
			//lists initealize
			foodList = new List<GameObject>();
			positions = new List<GameObject>();
			
			GameObject tempGO;

			//get positions
			for (int i = 0; i < 2; i++) 
			{
				tempGO = GameObject.Find(string.Format("PRICEP_{0}_CONTROL", (i+1)));
				tempGO = tempGO.FindChild("EDA");
				tempGO = tempGO.FindChild(codeLenght.ToString());
				tempGO.SetActive(true);
				for (int j = 0; j < tempGO.transform.childCount; j++) 
				{
					positions.Add(tempGO.transform.GetChild(j).gameObject);
				}
			}

			//select mystery position
			int mysteryNum = 0;
			if(randomMysteryVagon)
			{
				mysteryNum = Random.Range(0, positions.Count);
			}
			else
			{
				switch(mysteryVagonNumber.Value)
				{
					case 1:
					{
						mysteryNum = Random.Range(0, positions.Count / 2);
					} break;
					case 2: 
					{
						mysteryNum = Random.Range(positions.Count / 2, positions.Count);
					} break;
					default: { }break;
				}
			}
			positions[mysteryNum].tag = "MysteryPos";

			//get food
			for (int i = 0; i < foodKeeper.Value.transform.childCount; i++) 
			{
				foodList.Add(foodKeeper.Value.transform.GetChild(i).gameObject);
			}

			//positioning food
			for (int i = 0; i < positions.Count; i++) 
			{
				//get code's child and it params
				GameObject tempChild = foodList[int.Parse(code.Value[i].ToString())-1];
				string currentChildName = tempChild.name;
				//create new child
				tempChild.transform.GetComponent<PlayMakerFSM>().enabled = false;
				GameObject newChild = GameObject.Instantiate(tempChild);
				tempChild.transform.GetComponent<PlayMakerFSM>().enabled = true;
				//set params to new child
				newChild.name = currentChildName;
				if(i == mysteryNum) 
				{
					nameForDestroy = currentChildName;
				}
                else
                    newChild.SetActive(true);
                newChild.transform.parent = positions[i].transform;
				newChild.transform.localPosition = Vector3.zero;
				newChild.transform.localScale = Vector3.one;
			}
			//reparent all fod in foodKeeper
			int childCount = foodKeeper.Value.transform.childCount;
			if(childCount>0)
			{
				for (int i = childCount - 1; i >= 0; i--)
				{
					if(foodKeeper.Value.transform.GetChild(i).name != nameForDestroy)
						foodKeeper.Value.transform.GetChild(i).parent = foodBucket.Value.transform;
					else 
						GameObject.Destroy(foodKeeper.Value.transform.GetChild(i).gameObject);
				}
			}
			
			foodList = new List<GameObject>();
			positions = new List<GameObject>();
			Finish();
		}
	}
}