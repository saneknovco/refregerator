﻿using UnityEngine;
using System.Collections;

namespace HutongGames.PlayMaker.Actions
{
	public class AN_WSH_nameComparator : FsmStateAction{

		public FsmOwnerDefault owner;

		public FsmEvent hasGryaz;

		public FsmEvent withoutGryaz;

		private GameObject _head;

		public override void OnEnter(){
			GameObject go = Fsm.GetOwnerDefaultTarget(owner);
			_head = go.transform.GetChild (0).GetChild (0).gameObject;
			if (_head.transform.childCount == 0) {
				Debug.Log ("whyyyy");
				if (withoutGryaz != null)
					Fsm.Event (withoutGryaz);
				Finish ();
		} else {
			string namePart = go.name.Split ('_')[0];
				for (int i = 0; i < _head.transform.childCount; i++) {
					if (_head.transform.GetChild (i).name.Contains ("_GRYAZ")) {
						Debug.Log (_head.transform.GetChild (i).name);
						if (hasGryaz != null)
							Fsm.Event (hasGryaz);
						Fsm.Event(hasGryaz);
						Finish ();
					}
				}
				if (withoutGryaz != null)
					Fsm.Event(withoutGryaz);
				Finish ();
			}
		}
	}
}
