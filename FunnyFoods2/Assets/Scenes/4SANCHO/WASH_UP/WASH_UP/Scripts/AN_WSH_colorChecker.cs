﻿using UnityEngine;
using System.Collections;

namespace HutongGames.PlayMaker.Actions
{
	public class AN_WSH_colorChecker : FsmStateAction
	{
		[RequiredField]
		public FsmOwnerDefault gameObject;

		public FsmGameObject color;

		private Material material;

		public override void OnEnter()
		{
			GameObject go = Fsm.GetOwnerDefaultTarget(gameObject);
			material = go.transform.GetChild(0).GetChild(0).GetChild(1).GetChild(0).GetComponent<Renderer>().material;
			switch (color.Value.name) {
			case "RED":
				material.SetFloat ("_H", 0.4f);
				material.SetFloat ("_S", 0.25f);
				material.SetFloat ("_V", 0);
				break;
			case "VIO":
				material.SetFloat ("_H", 1.16f);
				material.SetFloat ("_S", 0.11f);
				material.SetFloat ("_V", 0);
				break;
			case "BRO":
				material.SetFloat ("_H", 1.49f);
				material.SetFloat ("_S", 0);
				material.SetFloat ("_V", -0.39f);
				break;
			case "ORA":
				material.SetFloat ("_H", 0.49f);
				material.SetFloat ("_S", 0.3f);
				material.SetFloat ("_V", 0);
				break;
			case "BLU":
				material.SetFloat ("_H", 0);
				material.SetFloat ("_S", 0);
				material.SetFloat ("_V", 0);
				break;
			case "GRE":
				material.SetFloat ("_H", -0.24f);
				material.SetFloat ("_S", 0.24f);
				material.SetFloat ("_V", 0);
				break;
			case "YEL":
				material.SetFloat ("_H", -0.37f);
				material.SetFloat ("_S", 0.24f);
				material.SetFloat ("_V", 0);
				break;
			default:
				Debug.Log ("defaulf");	
				break;	
			}
			go.tag = color.Value.name;
			Finish();
		}
	}
}
