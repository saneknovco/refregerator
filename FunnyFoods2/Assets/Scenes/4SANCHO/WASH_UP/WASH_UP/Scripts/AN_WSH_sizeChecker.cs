﻿using UnityEngine;
using System.Collections;

namespace HutongGames.PlayMaker.Actions
{
    public class AN_WSH_sizeChecker : FsmStateAction
    {
        public FsmOwnerDefault owner;
        public FsmFloat maxSize;

        public FsmEvent lessSize;
        public FsmEvent greaterSize;

        private float _size;
        private GameObject _go;

        public override void OnEnter()
        {
            _go = Fsm.GetOwnerDefaultTarget(owner);

            _size = _go.GetComponent<Renderer>().bounds.size.x;
            Debug.Log(_size + " " + _go.name);
            if (_size < maxSize.Value)
            {
                if (lessSize != null)
                    Fsm.Event(lessSize);
                Finish();
            }
            else
            {
                if (greaterSize != null)
                    Fsm.Event(greaterSize);
                Finish();
            }
        }
    }
}
