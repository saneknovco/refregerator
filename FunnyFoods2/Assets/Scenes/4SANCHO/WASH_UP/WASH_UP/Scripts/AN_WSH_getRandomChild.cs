﻿using UnityEngine;
using System.Collections;


	namespace HutongGames.PlayMaker.Actions
	{
		[ActionCategory(ActionCategory.GameObject)]
		[Tooltip("Gets a Random Child of a Game Object.")]
		public class AN_WSH_getRandomChild : FsmStateAction
		{
			[RequiredField]
			public FsmOwnerDefault newParent;
			[RequiredField]
			[UIHint(UIHint.Variable)]
			public FsmGameObject changeParentGO;

			public override void Reset()
			{
			newParent = null;
			}

			public override void OnEnter()
			{
				DoGetRandomChild();
				Finish();
			}

			void DoGetRandomChild()
			{
			GameObject go = Fsm.GetOwnerDefaultTarget(newParent);
				if (go == null) return;

			for (int i = 0; i < go.transform.childCount; i++) {
				if (go.transform.GetChild (i).childCount == 0) {
					changeParentGO.Value.transform.SetParent (go.transform.GetChild (i));
					Finish ();
					return;
				}
			}
			}
		}
	}
