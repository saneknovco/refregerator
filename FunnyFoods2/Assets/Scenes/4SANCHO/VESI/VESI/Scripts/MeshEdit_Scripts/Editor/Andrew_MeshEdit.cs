﻿using UnityEngine;
using UnityEditor;
//using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.IO;

public class Andrew_MeshEdit : EditorWindow {

	GameObject go;
	bool editMesh = false;
	Mesh_Handler mh;
	Object[] texs, pc2d;
	Transform[] selectedTr;
	Vector2 size;
	string pathForMesh;
	GUIStyle gs;
	bool advansedMode;
	int pointsNum;
	bool killExtra, killMuchExtra;
	bool addMesh, addMeshCollider, addPolygon;
	[MenuItem("Mage/Mesh_Editor")]
	static void ShowWindow()
	{
		var w = GetWindow<Andrew_MeshEdit>();
		w.OnSelectionChange();
	}

	void OnGUI()
	{
		if (Application.isPlaying) 
		{
			EditorGUILayout.LabelField(new GUIContent("NOT TO USE IN PLAY MODE"), GUILayout.Width(250));
			return;
		}
		EditorGUILayout.BeginHorizontal();
		if (GUILayout.Button((advancedMeshEdit) ? "Go to base mesh editor" : "Go to advansed mesh editor"))
		{
			advancedMeshEdit = !advancedMeshEdit;
		}
		EditorGUILayout.EndHorizontal();
		if(advancedMeshEdit)
			guiAdvancedMesh();
		else
		{
			EditorGUILayout.BeginHorizontal();
			guiTextures();
			guiEditorAlpha();
			EditorGUILayout.BeginVertical();
			baseMesh();
			editorMesh();
			EditorGUILayout.EndVertical();
			EditorGUILayout.EndHorizontal();
		}
	}

	void guiTextures()
	{
		EditorGUILayout.BeginVertical();

		EditorGUILayout.LabelField("Create mesh & mat ", gs);
		EditorGUILayout.LabelField("from selected Textures ", gs);
		EditorGUILayout.LabelField("Number of Textures select: " + texs.Length);
		if(texs.Length > 0)
		if(GUILayout.Button("CreateMat & Mesh"))
		{
			createMat_Mesh(texs);
		}
		for(int i = 0; i < Mathf.Min(10,texs.Length); i++)
			EditorGUILayout.LabelField(((Texture)texs[i]).name);
		if(texs.Length > 10)
			EditorGUILayout.LabelField("...etc");

		EditorGUILayout.EndVertical();
	}

//	void guiPolygons()
//	{
//		EditorGUILayout.BeginVertical();
//		EditorGUILayout.LabelField("Create mesh from Polygon Collider ", gs);
//		EditorGUILayout.LabelField("Number of Polygon Colliders select: " + pc2d.Length);
//		if(pc2d.Length > 0)
//		if(GUILayout.Button("Create Mesh by PolygonCollider"))
//		{
//			GenerateMeshFromPC2D(pc2d);
//		}
//		for(int i = 0; i < Mathf.Min(10,pc2d.Length); i++)
//			EditorGUILayout.LabelField((pc2d[i] != null) ? (((PolygonCollider2D)pc2d[i]).name) : "someBroken PC2D");
//		if(pc2d.Length > 10)
//			EditorGUILayout.LabelField("...etc");
//
//		EditorGUILayout.EndVertical();
//	}
	void guiEditorAlpha()
	{
		EditorGUILayout.BeginVertical();
		EditorGUILayout.LabelField("Analyze texture/s alpha", gs);

		if(selectedTr.Length == 0) 
		{
			EditorGUILayout.EndVertical();
			return;
		}
		EditorGUILayout.BeginHorizontal();

		addMesh = EditorGUILayout.ToggleLeft("Edit MeshFilter", addMesh);
		addMeshCollider = EditorGUILayout.ToggleLeft("Edit MeshCollider", addMeshCollider && !addPolygon);
		addPolygon = EditorGUILayout.ToggleLeft("Edit PolygonCollider", !addMeshCollider && addPolygon);

		EditorGUILayout.EndHorizontal();
		EditorGUILayout.BeginHorizontal();
		killExtra = EditorGUILayout.ToggleLeft("Kill extra points", killExtra);
		if(killExtra)
			killMuchExtra = EditorGUILayout.ToggleLeft("Kill more extra points", killMuchExtra);
		EditorGUILayout.EndHorizontal();
		EditorGUILayout.BeginHorizontal();
		pointsNum = EditorGUILayout.IntField(pointsNum);
		if (addMesh||addMeshCollider||addPolygon){
			if(GUILayout.Button("Analyze Texture alpha"))
			{
				multiTexAnalizator(false);
			}
			if(!addMesh)
			if(GUILayout.Button("Analyze all GO rendered"))
			{
				multiTexAnalizator(true);
			}}
		EditorGUILayout.EndHorizontal();
		EditorGUILayout.LabelField("Num of GO selected:" + selectedTr.Length);
		for(int i = 0; i < Mathf.Min(10, selectedTr.Length); i++)
			EditorGUILayout.LabelField(selectedTr[i].name);
		if(selectedTr.Length>10)
			EditorGUILayout.LabelField("...etc");
		EditorGUILayout.EndVertical();
	}
	void baseMesh()
	{
		EditorGUILayout.BeginVertical();

		EditorGUILayout.LabelField("Creating Base Mesh", gs);
		size = EditorGUILayout.Vector2Field("Mesh size", size);
		EditorGUILayout.LabelField("Path like *Resources/MeshName*");
		pathForMesh = EditorGUILayout.TextArea(pathForMesh);
		if(size.x * size.y != 0 && pathForMesh != "")
		if(GUILayout.Button("Create"))
		{
			AssetDatabase.DeleteAsset("Assets/" + pathForMesh + ".asset");
			AssetDatabase.CreateAsset(newMesh(size), "Assets/" + pathForMesh + ".asset");
			AssetDatabase.SaveAssets();
			AssetDatabase.Refresh();
		}

		EditorGUILayout.EndVertical();
	}
	void editorMesh()
	{
		EditorGUILayout.BeginVertical();

		EditorGUILayout.LabelField("EditingCurrent_GO_Mesh", gs);
		if(go != null)
			EditorGUILayout.LabelField("Currend GO: " + go.name);
		else
		{
			EditorGUILayout.LabelField("No GO Selected"); 
			return;
		}
		if(go.GetComponent<MeshFilter>() != null)
		if(GUILayout.Button((editMesh) ? "End editing" : "Start editing"))
		{
			if (editMesh) {editMesh = false; DestroyImmediate(mh);}
			else
				if(go != null) {editMesh = true; mh = go.AddComponent<Mesh_Handler>();}
		}
		if(go.GetComponent<PolygonCollider2D>() != null && go.GetComponent<MeshRenderer>()!=null)
		if(GUILayout.Button("Generate mesh from Polygon2D"))
		{
			creMegaMesh(go.GetComponent<PolygonCollider2D>());
		}
		if(go.GetComponent<MeshFilter>()!=null)
		if(GUILayout.Button("Rebuild Polygon2D from mesh"))
		{
			reBuildPolygon(go);
		}

		EditorGUILayout.EndVertical();
	}
	public void OnFocus()
	{
		OnSelectionChange();
	}
	public void OnSelectionChange()
	{
		var w = GetWindow<Andrew_MeshEdit>();
		if(w.mh != null) DestroyImmediate(w.mh);
		w.editMesh = false;
		w.go = Selection.activeGameObject;
		w.pc2d = Selection.GetFiltered(typeof(PolygonCollider2D), SelectionMode.ExcludePrefab);
		w.texs = Selection.GetFiltered(typeof(Texture), SelectionMode.DeepAssets);
		w.gs = new GUIStyle();
		w.gs.fontStyle = FontStyle.Bold;
		w.gs.normal.textColor = Color.white;
		var M = Selection.GetFiltered(typeof(Mesh), SelectionMode.Unfiltered);
		if(M.Length > 0) 
		{
			w.pathForMesh = AssetDatabase.GetAssetPath(M[0]).Substring(7);
			w.pathForMesh = w.pathForMesh.Substring(0, w.pathForMesh.Length - 6);
		}
		w.selectedTr = Selection.GetTransforms(SelectionMode.ExcludePrefab);
	}
	#region AdvancedMesh
	bool uvB, uv2B, colorsB, normalsB, advancedMeshEdit;
	int vertCount, triaCount;
	Vector3[] vert = new Vector3[0], normals = new Vector3[0];
	Vector2[] uv = new Vector2[0], uv2 = new Vector2[0];
	Color[] colors = new Color[0];
	int[] tria = new int[0];

	void guiAdvancedMesh()
	{
		EditorGUILayout.LabelField("Advanced mesh Editor", gs);
		EditorGUILayout.BeginHorizontal();
		EditorGUILayout.LabelField("Verticles count:");
		vertCount = EditorGUILayout.IntField(vertCount);
		EditorGUILayout.LabelField("Really triangles count:");
		triaCount = EditorGUILayout.IntField(triaCount);
		EditorGUILayout.EndHorizontal();

		EditorGUILayout.BeginHorizontal();
		EditorGUILayout.LabelField("Mesh will contain:");

		uvB = EditorGUILayout.ToggleLeft("uv", uvB, gs);

		uv2B = EditorGUILayout.ToggleLeft("uv2", uv2B, gs);

		colorsB = EditorGUILayout.ToggleLeft("colors", colorsB, gs);

		normalsB = EditorGUILayout.ToggleLeft("normals", normalsB, gs);

		EditorGUILayout.EndHorizontal();
		if (GUI.changed)
		{
			if (advancedMeshEdit) resizeArrays();
		}

		pathForMesh = EditorGUILayout.TextArea(pathForMesh);

		EditorGUILayout.BeginHorizontal();

		EditorGUILayout.BeginVertical();
		EditorGUILayout.LabelField("Verticles", gs);
		guiArrayVector3(ref vert);
		EditorGUILayout.EndVertical();

		if(uvB)
		{
			EditorGUILayout.BeginVertical();
			EditorGUILayout.LabelField("UV", gs);
			guiArrayVector2(ref uv);
			EditorGUILayout.EndVertical();
		}

		if(uv2B)
		{
			EditorGUILayout.BeginVertical();
			EditorGUILayout.LabelField("UV2", gs);
			guiArrayVector2(ref uv2);
			EditorGUILayout.EndVertical();
		}

		if(colorsB)
		{
			EditorGUILayout.BeginVertical();
			EditorGUILayout.LabelField("Colors", gs);
			guiArrayColor(ref colors);
			EditorGUILayout.EndVertical();
		}

		if(normalsB)
		{
			EditorGUILayout.BeginVertical();
			EditorGUILayout.LabelField("Normals", gs);
			guiArrayVector3(ref normals);
			EditorGUILayout.EndVertical();
		}

		EditorGUILayout.BeginVertical();
		EditorGUILayout.LabelField("Triangles", gs);
		guiArrayInt(ref tria);
		EditorGUILayout.EndVertical();

		EditorGUILayout.EndHorizontal();

		if (GUILayout.Button("Create Mesh"))
			CreateMegaMesh();
	}
	void guiArrayVector3(ref Vector3[] arr)
	{
		for (int i = 0; i < arr.Length; i++)
			arr[i] = EditorGUILayout.Vector3Field("", arr[i]);
	}
	void guiArrayVector2(ref Vector2[] arr)
	{
		for (int i = 0; i < arr.Length; i++)
			arr[i] = EditorGUILayout.Vector2Field("", arr[i]);
	}
	void guiArrayInt(ref int[] arr)
	{
		for (int i = 0; i < arr.Length / 3; i++)
		{
			EditorGUILayout.BeginHorizontal();
//			EditorGUILayout.LabelField(i.ToString());
			arr[i * 3] = EditorGUILayout.IntField(arr[i * 3]);
			arr[i * 3 + 1] = EditorGUILayout.IntField(arr[i * 3 + 1]);
			arr[i * 3 + 2] = EditorGUILayout.IntField(arr[i * 3 + 2]);
			EditorGUILayout.EndHorizontal();
		}
	}
	void guiArrayColor(ref Color[] arr)
	{
		for (int i = 0; i < arr.Length; i++)
			arr[i] = EditorGUILayout.ColorField("", arr[i]);
	}
	void resizeArrays()
	{
		if (vert.Length != vertCount) System.Array.Resize<Vector3>(ref vert, vertCount);
		if (tria.Length != triaCount * 3) System.Array.Resize<int>(ref tria, triaCount * 3);
		if(uvB && uv.Length != vertCount) System.Array.Resize<Vector2>(ref uv, vertCount);
		if(uv2B && uv2.Length != vertCount) System.Array.Resize<Vector2>(ref uv2, vertCount);
		if(colorsB && colors.Length != vertCount) System.Array.Resize<Color>(ref colors, vertCount);
		if (normalsB && normals.Length != vertCount) System.Array.Resize<Vector3>(ref normals, vertCount);
	}
	void CreateMegaMesh()
	{
		Mesh m = new Mesh();
		m.vertices = vert;
		m.triangles = tria;
		if(uvB) m.uv = uv;
		if(uv2B) m.uv2 = uv2;
		if(colorsB) m.colors = colors;
		if (normalsB) m.normals = normals;
		m.RecalculateBounds();
		AssetDatabase.DeleteAsset("Assets/" + pathForMesh + ".asset");
		AssetDatabase.CreateAsset(m, "Assets/" + pathForMesh + ".asset");
		AssetDatabase.SaveAssets();
		AssetDatabase.Refresh();
	}
	#endregion
	#region create_Mat_&_Mesh
	static void createMat_Mesh(Object[] o)
	{
		var T = new Texture[o.Length];
		for(int i = 0; i < o.Length; i++)
			T[i] = o[i] as Texture;
		for(int i = 0; i < T.Length; i++)
		{
			Texture t = T[i];
			if(t==null) continue;
			string dataPath = Application.dataPath,
			path = UnityEditor.AssetDatabase.GetAssetPath(t).Substring(6);
			string unipath = path.Substring(0, path.Length - t.name.Length - 5);
			if (unipath.Length > 9 && unipath.Substring(unipath.Length-9) == "/Textures") unipath = unipath.Substring(0, unipath.Length - 9);
			string meshPath = "Assets" + unipath + "/Meshes/" + t.name + ".asset",
			matPath = "Assets" + unipath + "/Material/" + t.name + ".mat";
			float w,h;
			GetPNGTextureSize(dataPath + path, out w, out h);
			Mesh m = newMesh(new Vector2(w,h));
			if(!AssetDatabase.IsValidFolder("Assets" + unipath + "/Meshes"))
				AssetDatabase.CreateFolder("Assets" + unipath, "Meshes");
			if(!AssetDatabase.IsValidFolder("Assets" + unipath + "/Material"))
				AssetDatabase.CreateFolder("Assets" + unipath, "Material");
			AssetDatabase.DeleteAsset(meshPath);
			AssetDatabase.CreateAsset(m , meshPath);
			Material mat = new Material(Shader.Find("Unlit/Transparent"));
			mat.mainTexture = t;
			AssetDatabase.DeleteAsset(matPath);
			AssetDatabase.CreateAsset(mat, matPath);
			GameObject go = new GameObject();
			go.name = t.name;
			go.transform.position = Vector3.zero;
			var mr = go.AddComponent<MeshRenderer>();
			mr.material = mat;
			var mf = go.AddComponent<MeshFilter>();
			mf.mesh = m;
		}
		AssetDatabase.SaveAssets();
		AssetDatabase.Refresh();
	}

	static Mesh newMesh(Vector2 size)
	{
		Vector3[] verticles;
		Vector2[] uv;
		int[] triangles;

		uv = new Vector2[] { new Vector2(0, 0), new Vector2(0, 1), new Vector2(1, 1), new Vector2(1, 0) };
		verticles = new Vector3[] { -size / 2, new Vector2(-size.x / 2, size.y / 2), size / 2, new Vector2(size.x / 2, -size.y / 2) };
		triangles = new int[] { 0, 1, 2, 0, 2, 3 };
		Mesh m = new Mesh();
		m.vertices = verticles;
		m.uv = uv;
		m.triangles = triangles;
		return m;
	}

	static bool GetPNGTextureSize(string pathToPNG, out float width, out float height)
	{
		byte[] bytes = new byte[24];
		width = -1.0f;
		height = -1.0f;
		try
		{
			FileStream fs = new FileStream(pathToPNG, FileMode.Open, FileAccess.Read);
			fs.Read(bytes, 0, 24);

			byte[] png_signature = { 137, 80, 78, 71, 13, 10, 26, 10 };

			const int cMinDownloadedBytes = 23;
			byte[] buf = bytes;
			if (buf.Length > cMinDownloadedBytes)
			{
				for (int i = 0; i < png_signature.Length; i++)
				{
					if (buf[i] != png_signature[i])
					{
						Debug.LogWarning("Error! Texture os NOT png format!");
						return false;
					}
				}
				width = buf[16] << 24 | buf[17] << 16 | buf[18] << 8 | buf[19];
				height = buf[20] << 24 | buf[21] << 16 | buf[22] << 8 | buf[23];
				return true;
			}
			return false;
		}
		catch
		{
			return false;
		}
	}
	#endregion
	#region MeshFromPC2D
	static void GenerateMeshFromPC2D(Object[] PC)
	{
		for(int i = 0; i < PC.Length; i++)
		{
			creMegaMesh((PolygonCollider2D)PC[i]);
		}
		AssetDatabase.SaveAssets();
		AssetDatabase.Refresh();
	}

	static void creMegaMesh(PolygonCollider2D pc)
	{
		Transform t = pc.transform;
		var mr = t.GetComponent<MeshRenderer>();
		if (!mr) return;
		var mat = mr.sharedMaterial;// material;
		if (!mat) return;
		var tex = mat.mainTexture;
		if (!tex) return;
		float w,h;

		string dataPath = Application.dataPath,
		path = UnityEditor.AssetDatabase.GetAssetPath(tex).Substring(6);
		string unipath = path.Substring(0, path.Length - tex.name.Length - 5);
		if (unipath.Substring(unipath.Length - 9) == "/Textures") unipath = unipath.Substring(0, unipath.Length - 9);
		string meshPath = "Assets" + unipath + "/Mesh/" + tex.name + ".asset";

		GetPNGTextureSize(dataPath + path, out w, out h);
		var points = pc.points;

		for(int i = 0; i < points.Length; i++)
		{
			points[i] = new Vector2(Mathf.Clamp(points[i].x, -w / 2, w / 2),Mathf.Clamp(points[i].y, -h / 2, h / 2));
		}

		Triangulator tr = new Triangulator(points);
		int[] tria = tr.Triangulate();
		Vector3[] vert = new Vector3[points.Length];
		Vector2[] uv = new Vector2[points.Length];

		for (int i = 0; i < points.Length; i++) {
			vert[i] = new Vector3(points[i].x, points[i].y, 0);
			uv[i] = uvFromPoint(points[i], w, h);
		}

		Mesh mesh = new Mesh();
		mesh.vertices = vert;
		mesh.triangles = tria;
		mesh.uv = uv;

		var mf = (t.GetComponent<MeshFilter>() != null) ? t.GetComponent<MeshFilter>() : t.gameObject.AddComponent<MeshFilter>();
		mf.mesh = mesh;
		if(!AssetDatabase.IsValidFolder("Assets" + unipath + "/Mesh"))
			AssetDatabase.CreateFolder("Assets" + unipath, "Mesh");
		AssetDatabase.DeleteAsset(meshPath);
		AssetDatabase.CreateAsset(mesh, meshPath);
	}

	static Vector2 uvFromPoint(Vector2 p, float x, float y)
	{
		return new Vector3(0.5f + p.x / x, 0.5f + p.y / y);
	}
	#endregion
	#region triangulator
	public class Triangulator
	{
		private List<Vector2> m_points = new List<Vector2>();

		public Triangulator (Vector2[] points) {
			m_points = new List<Vector2>(points);
		}

		public int[] Triangulate() {
			List<int> indices = new List<int>();

			int n = m_points.Count;
			if (n < 3)
				return indices.ToArray();

			int[] V = new int[n];
			if (Area() > 0) {
				for (int v = 0; v < n; v++)
					V[v] = v;
			}
			else {
				for (int v = 0; v < n; v++)
					V[v] = (n - 1) - v;
			}

			int nv = n;
			int count = 2 * nv;
			for (int m = 0, v = nv - 1; nv > 2; ) {
				if ((count--) <= 0)
					return indices.ToArray();

				int u = v;
				if (nv <= u)
					u = 0;
				v = u + 1;
				if (nv <= v)
					v = 0;
				int w = v + 1;
				if (nv <= w)
					w = 0;

				if (Snip(u, v, w, nv, V)) {
					int a, b, c, s, t;
					a = V[u];
					b = V[v];
					c = V[w];
					indices.Add(a);
					indices.Add(b);
					indices.Add(c);
					m++;
					for (s = v, t = v + 1; t < nv; s++, t++)
						V[s] = V[t];
					nv--;
					count = 2 * nv;
				}
			}

			indices.Reverse();
			return indices.ToArray();
		}

		private float Area () {
			int n = m_points.Count;
			float A = 0.0f;
			for (int p = n - 1, q = 0; q < n; p = q++) {
				Vector2 pval = m_points[p];
				Vector2 qval = m_points[q];
				A += pval.x * qval.y - qval.x * pval.y;
			}
			return (A * 0.5f);
		}

		private bool Snip (int u, int v, int w, int n, int[] V) {
			int p;
			Vector2 A = m_points[V[u]];
			Vector2 B = m_points[V[v]];
			Vector2 C = m_points[V[w]];
			if (Mathf.Epsilon > (((B.x - A.x) * (C.y - A.y)) - ((B.y - A.y) * (C.x - A.x))))
				return false;
			for (p = 0; p < n; p++) {
				if ((p == u) || (p == v) || (p == w))
					continue;
				Vector2 P = m_points[V[p]];
				if (InsideTriangle(A, B, C, P))
					return false;
			}
			return true;
		}

		private bool InsideTriangle (Vector2 A, Vector2 B, Vector2 C, Vector2 P) {
			float ax, ay, bx, by, cx, cy, apx, apy, bpx, bpy, cpx, cpy;
			float cCROSSap, bCROSScp, aCROSSbp;

			ax = C.x - B.x; ay = C.y - B.y;
			bx = A.x - C.x; by = A.y - C.y;
			cx = B.x - A.x; cy = B.y - A.y;
			apx = P.x - A.x; apy = P.y - A.y;
			bpx = P.x - B.x; bpy = P.y - B.y;
			cpx = P.x - C.x; cpy = P.y - C.y;

			aCROSSbp = ax * bpy - ay * bpx;
			cCROSSap = cx * apy - cy * apx;
			bCROSScp = bx * cpy - by * cpx;

			return ((aCROSSbp >= 0.0f) && (bCROSScp >= 0.0f) && (cCROSSap >= 0.0f));
		}
	}
	#endregion
	#region polygonator
	void reBuildPolygon(GameObject go)
	{
		var mf = go.GetComponent<MeshFilter>();
		if(mf==null) return;
		var m = mf.sharedMesh;
		if(m==null) return;
		var pc = (go.GetComponent<PolygonCollider2D>()!= null) ? go.GetComponent<PolygonCollider2D>() : go.AddComponent<PolygonCollider2D>();
		var tria = m.triangles;
		var vert = m.vertices;
		var pairs = new Vector2[tria.Length].ToList();
		for(int i  = 0; i < tria.Length / 3; i++)
		{
			pairs[i * 3] = new Vector2(tria[i * 3], tria[i * 3 + 1]);
			pairs[i * 3 + 1] = new Vector2(tria[i * 3 + 1], tria[i * 3 + 2]);
			pairs[i * 3 + 2] = new Vector2(tria[i * 3 + 2], tria[i * 3]);
		}
		deletePairs(ref pairs);
		for(int i = 0; i < pairs.Count; i++)
		{
			int a = (int)pairs[i].x;
			int b = (int)pairs[i].y;
			pairs[i] = new Vector2(Mathf.Min(a,b), Mathf.Max(a,b));
			if((a == 0 && b == vert.Length - 1) || (a == vert.Length - 1 && b == 0))
				pairs[i] = new Vector2(vert.Length - 1, 0);
		}
		for(int i = 0; i < pairs.Count-1; i++)
			for(int j = i + 1; j < pairs.Count; j++)
			{
				if(pairs[i].y == pairs[j].x)
				{
					Vector2 temp = pairs[i+1];
					pairs[i+1] = pairs[j];
					pairs[j] = temp;
				}
			}
		var points = new Vector2[pairs.Count];
		for(int i = 0; i < points.Length; i++)
		{
			points[i] = vert[(int)pairs[i].x];
		}
		pc.points = points;
	}
	void deletePairs(ref List<Vector2> pairs)
	{
		for(int i = 0; i < pairs.Count - 1; i++)
			for(int j = i + 1; j < pairs.Count; j++)
			{
				{
					bool b = ((int)pairs[i].x == (int)pairs[j].x && (int)pairs[i].y == (int)pairs[j].y) ||
						((int)pairs[i].x == (int)pairs[j].y && (int)pairs[i].y == (int)pairs[j].x);
					if(b)
					{
						//						Debug.Log(string.Format("{0}  {1}  {2}", i, j, pairs.Count-2));
						pairs.RemoveAt(j);
						pairs.RemoveAt(i);
						j=i + 1;
					}
				}
			}
	}
	#endregion
	#region texAnalizator
	void multiTexAnalizator(bool rend)
	{
		foreach(Transform t in selectedTr)
		{
			texAnalizator(t.gameObject, pointsNum, rend);
		}
	}
	void texAnalizator(GameObject go, int num, bool rend)
	{
		float w, h;
		Texture2D t;
		Vector2 deltaB = Vector2.zero;
		if (!rend)
		{
			var mr = go.GetComponent<MeshRenderer>();
		if(mr == null) return;
		var mat = mr.sharedMaterial;
		if(mat == null) return;
		var tex = mat.mainTexture;
		if(tex == null) return;
		string dataPath = Application.dataPath,
		path = UnityEditor.AssetDatabase.GetAssetPath(tex).Substring(6);
		GetPNGTextureSize(dataPath + path, out w, out h);
		t = new Texture2D((int)w,(int)h, TextureFormat.Alpha8,false);
		t.LoadImage(File.ReadAllBytes(dataPath + path));
		}
		else
		{
			t = rendGOInTex(go.transform, out deltaB);
			w = t.width;
			h = t.height;
		}
		Vector2[] points = new Vector2[num];
		Vector2 center = new Vector2(w / 2, h / 2);
		for (int i = 0; i < points.Length; i++)
		{
			Vector2 angleVector = RotateV2(Vector2.up, 360f / num * i);
			float s = Mathf.Min(Mathf.Abs(w / angleVector.x), Mathf.Abs(h / angleVector.y)) / 2;
			Vector2 p;
			Color c;
			for(int j = 1; j < s; j++)
			{
				p = center + angleVector * j;
				c = t.GetPixel((int)Mathf.Clamp(p.x, 0, w), (int)Mathf.Clamp(p.y, 0, h));
				if(c.a != 0)
				{
					points[i] = p + angleVector * 2;
//					j = (int)s;
				}
				if(points[i] == Vector2.zero) 
				{
					points[i] = center + angleVector * s;
				}
			}
			points[i] -= center;
			points[i] /= Mathf.Sin(Mathf.PI * (90f - 360f / num) / 180f);
			points[i] = new Vector2(Mathf.Clamp(points[i].x, -(float)w / 2, (float)w / 2), Mathf.Clamp(points[i].y, -(float)h / 2, (float)h / 2)) + deltaB;
		}
		if(killExtra)
		{
			var listP = points.ToList();
			killExtraPoints(ref listP);
			killExtraPoints(ref listP);
			killExtraPoints(ref listP);
			if(killMuchExtra)
			{
				killExtraPoints(ref listP);
				killExtraPoints(ref listP);
				killExtraPoints(ref listP);
			}
			points = listP.ToArray();
		}
		if(addMesh || addPolygon)
		{
			DestroyImmediate(go.GetComponent<MeshCollider>());
			var pc = (go.GetComponent<PolygonCollider2D>()!=null) ? go.GetComponent<PolygonCollider2D>() : go.AddComponent<PolygonCollider2D>();
			pc.points = points;
			if(addMesh)
			creMegaMesh(pc);
			if(!addPolygon) DestroyImmediate(pc);
		}
		if(addMeshCollider)
		{
			DestroyImmediate(go.GetComponent<PolygonCollider2D>());
			var mc = (go.GetComponent<MeshCollider>() != null) ? go.GetComponent<MeshCollider>() : go.AddComponent<MeshCollider>();
			if(addMesh)
			{				
				mc.sharedMesh = go.GetComponent<MeshFilter>().sharedMesh;
				return;
			}
			Triangulator tr = new Triangulator(points);
			int[] tria = tr.Triangulate();
			Vector3[] vert = new Vector3[points.Length];
			for(int i = 0; i < points.Length; i++)
			{
				vert[i] = (Vector3)points[i];
			}
			Mesh m = new Mesh();
			m.vertices = vert;
			m.triangles = tria;
			mc.sharedMesh = m;
			string pathFolderMC = "Assets/Resources";
			string folder = "MeshColliders";
			string pathMC = pathFolderMC + "/" + folder + "/" + go.name + "_Mesh_For_Collider.asset";
			if(!AssetDatabase.IsValidFolder(pathFolderMC + "/" + folder)) 
				AssetDatabase.CreateFolder(pathFolderMC, folder);
			AssetDatabase.DeleteAsset(pathMC);
			AssetDatabase.CreateAsset(m, pathMC);
		}
	}

	void killExtraPoints(ref List<Vector2> listP)
	{		
		for(int i = 0; i < listP.Count; i++)
		{
			if(squareTria(listP[i], listP[(i+2)%listP.Count], Vector2.zero) > 
				squareTria(listP[i], listP[(i+1)%listP.Count], Vector2.zero) + 
				squareTria(listP[(i+1)%listP.Count], listP[(i+2)%listP.Count], Vector2.zero))
			{
				listP.RemoveAt((i+1)%listP.Count);
				if(killMuchExtra)
				i--;
			}
		}
	}

	float squareTria(Vector2 a, Vector2 b, Vector2 c)
	{
		float A = (b-a).magnitude;
		float B = (c-b).magnitude;
		float C = (a-c).magnitude;
		float P = (A+B+C)/2;
		return Mathf.Sqrt(P*(P-A)*(P-B)*(P-C));
	}
	public static Texture2D rendGOInTex(Transform t, out Vector2 deltaB)
	{
		var e1 = t.gameObject.FindChild("GLAZ_R");
		var e2 = t.gameObject.FindChild("GLAZ_L");
		var e3 = t.gameObject.FindChild("ROT");
		if(e1!=null&&e2!=null&&e3!=null)
		{
			e1.SetActive(false);
			e2.SetActive(false);
			e3.SetActive(false);
		}
		Vector3 r = t.rotation.eulerAngles;
		t.rotation = new Quaternion();
		Vector3 size = t.localScale;
		Vector3 s1 = (t.parent != null) ? t.parent.lossyScale : Vector3.one;
		t.localScale = new Vector3(1f / s1.x, 1f / s1.y, 1f);
		Bounds b = getBounds(t);
		float x = b.size.x, y = b.size.y;
		int layer = t.gameObject.layer;
		setLayer(t, 10);

		GameObject camGO = new GameObject();
		camGO.layer = 10;
		camGO.transform.position = b.center + Vector3.back * 100f;
		Camera cam = camGO.AddComponent<Camera>();
		cam.orthographic = true;
		cam.orthographicSize = y / 2;
		cam.aspect = x / y;
		cam.backgroundColor = new Color(0, 0, 0, 0);
		cam.cullingMask = 1024;
		cam.clearFlags = CameraClearFlags.Color;
		cam.nearClipPlane = 1;
		cam.farClipPlane = 10000;

		RenderTexture rt = new RenderTexture((int)x, (int)y, 16, RenderTextureFormat.Default);
		rt.name = t.name + "_tex";
		cam.targetTexture = rt;
		cam.Render();
		rt.Create();
		RenderTexture.active = rt;
		Texture2D tex2D = new Texture2D((int)x, (int)y, TextureFormat.ARGB32, false);
		tex2D.ReadPixels(new Rect(0, 0, (int)x,(int)y),0,0);
		tex2D.Apply();
		deltaB = b.center - t.position;
		cam.targetTexture = null;
		DestroyImmediate(camGO);


		t.rotation = Quaternion.Euler(r);
		t.localScale = size;
		setLayer(t,layer);
		if(e1!=null&&e2!=null&&e3!=null)
		{
			e1.SetActive(true);
			e2.SetActive(true);
			e3.SetActive(true);
		}
		return tex2D;
	}
	public static void setLayer(Transform t, int layer)
	{
		t.gameObject.layer = layer;
		foreach (Transform T in t) setLayer(T,layer);
	}

	//RotateToRight
	public static Vector2 RotateV2(Vector2 p, float angle)
	{
		float sin = Mathf.Sin(angle * Mathf.PI / 180f), cos = Mathf.Cos (angle * Mathf.PI / 180f);
		return new Vector2(p.x * cos + p.y * sin,- p.x * sin + p.y * cos);
	}
	public static Bounds getBounds(Transform t)
	{
		var rend = t.GetComponentsInChildren<Renderer>();
		Bounds b = (rend.Length > 0) ? rend[0].bounds : new Bounds(t.position, Vector3.zero);
		if (rend.Length < 1) return b;
		foreach (Renderer r in rend)
		{
			b = add(b, r.bounds);
		}
		return b;
	}
	public static Bounds add(Bounds b1, Bounds b2)
	{
		Vector3 min = new Vector3(Mathf.Min(b1.min.x, b2.min.x), Mathf.Min(b1.min.y, b2.min.y), Mathf.Min(b1.min.z, b2.min.z)),
		max = new Vector3(Mathf.Max(b1.max.x, b2.max.x), Mathf.Max(b1.max.y, b2.max.y), Mathf.Max(b1.max.z, b2.max.z));
		Bounds b = new Bounds((max + min) / 2f, (max - min));
		return b;
	}
	#endregion
}