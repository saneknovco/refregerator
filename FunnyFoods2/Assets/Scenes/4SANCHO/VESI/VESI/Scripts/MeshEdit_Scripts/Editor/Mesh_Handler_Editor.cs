﻿using UnityEngine;
using UnityEditor;

[CustomEditor(typeof(Mesh_Handler))]
public class Mesh_Handler_Editor : Editor {

	Mesh m;
	private void OnSceneGUI()
	{
		var go = ((Mesh_Handler)target).transform;
		if (m == null)
		{
			var mf = go.GetComponent<MeshFilter>();
			if(mf) m = mf.sharedMesh;
			if (m == null) return;
		}
		Vector3 pos = go.transform.position;
		float size = HandleUtility.GetHandleSize(pos) * 0.2f;
		Vector3[] v = m.vertices;
		for (int i = 0; i < v.Length; i++)
		{
			v[i] =  Handles.FreeMoveHandle(v[i] + go.position, Quaternion.identity, size / 2, Vector3.zero, Handles.SphereCap) - go.position;
		}
		Vector3 newPos = Handles.FreeMoveHandle(pos, Quaternion.identity, size, Vector3.zero, Handles.SphereCap);
		if(GUI.changed)
		{
			Vector3 delta = newPos - pos;
			go.transform.position += delta;
			for(int i = 0; i < v.Length; i++)
			{
				v[i] -= delta;
			}
			m.vertices = v;
		}
	}
}