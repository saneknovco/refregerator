﻿using UnityEngine;
using System.Collections;

namespace HutongGames.PlayMaker.Actions
{
    public class AN_VES_AddPhis : FsmStateAction
    {

        public FsmOwnerDefault owner;


        GameObject go;

        public override void OnEnter()
        {
            go = Fsm.GetOwnerDefaultTarget(owner);
            go.GetComponent<Rigidbody2D>().isKinematic = false;
            go.GetComponent<ConstantForce2D>().enabled = true;
            Finish();
        }
    }
}
