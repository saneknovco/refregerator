﻿using UnityEngine;
using System.Collections;
using System;

namespace HutongGames.PlayMaker.Actions
{
public class AN_VES_getTrueCount : FsmStateAction {

	public FsmOwnerDefault owner;
		public FsmBool hasMore;
		public FsmBool hasLess;
		public FsmString trueNumber;
		int result;
		public FsmInt count;
	GameObject go;

	public override void OnEnter () {
		go = Fsm.GetOwnerDefaultTarget (owner);
			if (count.Value < 5&&!hasMore.Value) { 
				result = UnityEngine.Random.Range (count.Value + 1, 5);
				trueNumber.Value = result.ToString();
				hasMore.Value = true;
				if (count.Value == 1)
					hasMore = false; 
				Finish ();
				return;
				}

			if (count.Value > 1&&!hasLess.Value) {
				result = UnityEngine.Random.Range (1, count.Value);

				Debug.Log (go.name + result);
				trueNumber.Value = result.ToString();

			}
		Finish();
	}
}
}