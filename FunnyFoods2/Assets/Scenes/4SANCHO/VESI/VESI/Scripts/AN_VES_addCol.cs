﻿using UnityEngine;
using System.Collections;
namespace HutongGames.PlayMaker.Actions
{


	public class AN_VES_addCol : FsmStateAction {

	public FsmOwnerDefault owner;
	GameObject go;
	private float _firstChildColPos;
	private float _lastChildColPos;
	private float _distance;
	private float _height;
		private Vector3 vec;
		private float SizeParent;
	public override void OnEnter () {
		go = Fsm.GetOwnerDefaultTarget (owner);
			vec = go.transform.position;
			SizeParent = go.transform.parent.GetComponent<MeshRenderer> ().bounds.size.y / 2 * go.transform.parent.lossyScale.y;
			_firstChildColPos = go.transform.GetChild (0).transform.position.y - go.transform.position.y -(go.transform.GetChild (0).lossyScale.y * go.transform.GetChild(0).GetComponent<BoxCollider2D>().size.y/2);

		_height = go.transform.GetChild (0).GetChild (0).GetChild (0).GetComponent<MeshRenderer>().bounds.size.y*1.5f;
		BoxCollider2D bc = go.GetComponent<BoxCollider2D> ();
			if (go.transform.childCount > 2) {
				bc.size = new Vector2 (400, Mathf.Abs( _firstChildColPos) * 4);
			} else {
                bc.size = new Vector2(400, Mathf.Abs(_firstChildColPos) * 2.5f);
                //bc.center = new Vector3 (0, 0, 0);
			}	
			vec.y -= _firstChildColPos - SizeParent; 
			go.transform.position = vec;
			Finish();
	}
}
}