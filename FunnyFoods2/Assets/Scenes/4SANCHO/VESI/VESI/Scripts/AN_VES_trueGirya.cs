﻿using UnityEngine;
using System.Collections;

namespace HutongGames.PlayMaker.Actions
{
	public class AN_VES_trueGirya : FsmStateAction {

		public FsmOwnerDefault owner;
		public FsmInt trueGirya;
		GameObject go;

		public override void OnEnter () {
			go = Fsm.GetOwnerDefaultTarget (owner);
			if (go.transform.childCount == 1) {
				trueGirya = 1;
			}
			Finish();
		}
}
}
