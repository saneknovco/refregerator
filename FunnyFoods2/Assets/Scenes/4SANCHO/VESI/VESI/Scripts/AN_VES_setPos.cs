﻿using UnityEngine;
using System.Collections;
namespace HutongGames.PlayMaker.Actions
{
public class AN_VES_setPos : FsmStateAction {

	public FsmOwnerDefault owner;
	GameObject go;
		int childCount;
		Vector3 vec;
		float positionX = 150;
		float positionY = 110;
		float delta = 70;
		float sizeCol;
	public override void OnEnter () {
			go = Fsm.GetOwnerDefaultTarget (owner);
			sizeCol = go.transform.GetChild (0).GetComponent<BoxCollider2D> ().size.x * go.transform.GetChild (0).lossyScale.x;
			childCount = go.transform.childCount;
//			for (int i = 0; i < childCount; i++) {
//				setPos (i);
//			}
			setPOs();
		Finish();
	}
		void setPOs(){
			int delta;
			if (go.transform.childCount == 5) {
				for(int i = 0; i < 3; i++){
					delta = (int)((i + 1)/ 2);
					vec.x = Mathf.Pow (-1, i + 1) * ( sizeCol * delta); 
					vec.z = 0;
					go.transform.GetChild(i).localPosition = vec;
				}
				for(int i = 3; i < 5; i++){
					vec.x = Mathf.Pow (-1, i + 1) * (sizeCol / 2); 
					vec.y = positionY;
					vec.z = 30;
					go.transform.GetChild(i).localPosition = vec;
				}
				return;
			}

			if (go.transform.childCount % 2 == 0) {
				for(int i = 0; i < go.transform.childCount; i++){
					delta = (int)((i)/ 2);
					vec.x = Mathf.Pow (-1, i + 1) * (sizeCol / 2 + sizeCol * delta); 
					vec.z = 0;
					go.transform.GetChild(i).localPosition = vec;
				}
			} else {
				for(int i = 0; i < go.transform.childCount; i++){
					delta = (int)((i + 1)/ 2);
					vec.x = Mathf.Pow (-1, i + 1) * ( sizeCol * delta); 
					vec.z = 0;
					go.transform.GetChild(i).localPosition = vec;
				}
			}
		}
		void setPos( int i){
			vec = go.transform.GetChild (i).localPosition;
			if (go.transform.childCount == 1) {
				return;
			}
			if (go.transform.childCount == 2) {
				vec.x = -go.transform.GetChild (0).GetComponent<BoxCollider> ().size.x * go.transform.GetChild (0).lossyScale.x / 2 +
				go.transform.GetChild (0).GetComponent<BoxCollider> ().size.x * go.transform.GetChild (0).lossyScale.x; //i
				vec.z = 0;
				go.transform.GetChild(0).localPosition = vec;

				vec.x = go.transform.GetChild (0).GetComponent<BoxCollider> ().size.x * go.transform.GetChild (0).lossyScale.x/2;
				vec.z = 50;
				go.transform.GetChild(1).localPosition = vec;
				return;
			}
			if (go.transform.childCount == 4) {
				vec.x = -positionX;
				vec.z = 0;
				go.transform.GetChild(0).localPosition = vec;

				vec.x = -positionX/3;
				vec.z = 50;
				go.transform.GetChild(1).localPosition = vec;

				vec.x = positionX/3;
				vec.z = 0;
				go.transform.GetChild(2).localPosition = vec;

				vec.x = positionX;
				vec.z = 50;
				go.transform.GetChild(3).localPosition = vec;
				return;
			}
			switch (i) {
			case 0:
				vec.x = -positionX;
				break;
			case 1:
				vec.x = 0;
				vec.z -= 50;
				break;
			case 2:
				vec.x = positionX;
				break;
			case 3:
				vec.x = -positionX + delta;
				vec.y = positionY;
				vec.z = 50;
				break;
			case 4:
				vec.x = positionX - delta;
				vec.y = positionY;
				vec.z = 50;
				break;
			}
			go.transform.GetChild(i).localPosition = vec;
		}
}
}