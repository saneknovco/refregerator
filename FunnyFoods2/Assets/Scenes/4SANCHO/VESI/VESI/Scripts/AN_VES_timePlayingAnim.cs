﻿using UnityEngine;
using System.Collections;

namespace HutongGames.PlayMaker.Actions
{
    public class AN_VES_timePlayingAnim : FsmStateAction
    {



        public FsmOwnerDefault owner;
        public FsmString animName;
        public FsmFloat wait;
        
        GameObject go;
       
        public override void OnEnter()
        {
            go = Fsm.GetOwnerDefaultTarget(owner);
            switch (animName.Value) {
                case "VESI_MNOGO_MNOGO":
                    wait.Value = 1.15f;
                    break;
                case "VESI_MALO_MALO":
                    wait.Value = 1.05f;
                    break;
                case "VESI_MNOGO":
                    wait.Value = 0.45f;
                    break;
                case "VESI_MNOGO_1":
                    wait.Value = 1.25f;
                    break;
                case "VESI_MALO":
                    wait.Value = 0.2f;
                    break;
                case "VESI_MALO_1":
                    wait.Value = 1.2f;
                    break;
            }
           
            Finish();
        }
    }
}

