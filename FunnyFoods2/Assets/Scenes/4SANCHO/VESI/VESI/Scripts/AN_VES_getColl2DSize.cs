﻿using UnityEngine;
using System.Collections;

namespace HutongGames.PlayMaker.Actions
{
    [ActionCategory("FF2_Ruben")]
    public class AN_VES_getColl2DSize : FsmStateAction
    {
        public FsmOwnerDefault obj;
        public FsmBool considerScale = new FsmBool() { UseVariable = false };
        public FsmVector2 storeSize = new FsmVector2() { UseVariable = true };
        public FsmFloat storeXSize = new FsmFloat() { UseVariable = true };
        public FsmFloat storeYSize = new FsmFloat() { UseVariable = true };
        public FsmFloat storeZSize = new FsmFloat() { UseVariable = true };

        private GameObject _go;
        private BoxCollider2D _bc;
        public override void Reset()
        {
            considerScale = new FsmBool() { UseVariable = false };
            considerScale.Value = true;
        }

        public override void OnEnter()
        {
            _go = Fsm.GetOwnerDefaultTarget(obj);
            if (obj != null)
            {
                Vector2 tempSize = new Vector2();

                _bc = _go.GetComponent<BoxCollider2D>();
                if (_bc != null)
                {
                    tempSize = _bc.size;
                    if (considerScale.Value)
                    {
                        Vector3 tempScale = new Vector3();
                        tempScale = _go.transform.localScale;
                        tempSize = new Vector2(tempSize.x * tempScale.x, tempSize.y * tempScale.y);
                    }
                    if (storeSize != null) storeSize.Value = tempSize;
                    if (storeXSize != null) storeXSize.Value = tempSize.x;
                    if (storeYSize != null) storeYSize.Value = tempSize.y;
                }
            }
            Finish();
        }
    }
}