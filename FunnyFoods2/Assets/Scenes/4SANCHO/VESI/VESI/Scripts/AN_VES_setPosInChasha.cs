﻿using UnityEngine;
using System.Collections;

namespace HutongGames.PlayMaker.Actions
{


	public class AN_VES_setPosInChasha : FsmStateAction {

	public FsmOwnerDefault owner;
		public FsmVector3 _vec; 
	GameObject go;
		private Vector3 vec;
	private float _firstChildColPos;
	private float _lastChildColPos;
	private float _distance;
	private float _height;
	private float SizeParent;
	public override void OnEnter () {
		go = Fsm.GetOwnerDefaultTarget (owner);

			vec = go.transform.parent.position;
			SizeParent = go.transform.parent.GetComponent<MeshRenderer> ().bounds.size.y / 2 - 10;
            Debug.Log(go.transform.parent.name);
			_firstChildColPos = go.transform.GetChild (0).lossyScale.y * go.transform.GetChild(0).GetComponent<BoxCollider2D>().size.y/2;
			vec.y = go.transform.parent.position.y + SizeParent + _firstChildColPos;
			vec.z = go.transform.parent.position.z - 1;;
			_vec.Value = vec;
		Finish();
	}
}
}
