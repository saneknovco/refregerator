﻿using UnityEngine;
using System.Collections;

namespace HutongGames.PlayMaker.Actions
{
public class AN_HOL_BIGGER : FsmStateAction {

		public FsmOwnerDefault owner;
		public FsmFloat size;
		private Vector3 _scale;
		GameObject go;
		private float _delta;
		public override void OnEnter () {
			go = Fsm.GetOwnerDefaultTarget (owner);
			_scale = go.transform.localScale;
		}

		public override void  OnUpdate () {
			if (_scale.x < size.Value) {
				_scale.x += Time.deltaTime * _delta;
				_scale.y += Time.deltaTime * _delta;
				go.transform.localScale = _scale;
			} else
				Finish ();
		}
}
}
