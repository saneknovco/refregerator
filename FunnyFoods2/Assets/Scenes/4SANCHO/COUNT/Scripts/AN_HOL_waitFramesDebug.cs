﻿using UnityEngine;
using System.Collections;

namespace HutongGames.PlayMaker.Actions
    {
        [ActionCategory("FF2_Ruben")]
        public class AN_HOL_waitFramesDebug : FsmStateAction
        {
            [RequiredField]
            public FsmInt frames;
            public FsmEvent finishEvent;

            public override void Reset()
            {
                frames = 1;
                finishEvent = null;
            }
            int counter;
            public override void OnEnter()
            {
                counter = 1;
                if (frames.Value <= 0)
                {
                    Fsm.Event(finishEvent);
                    Finish();
                    return;
                }
            }

            public override void OnUpdate()
            {
                counter++;
                Debug.Log(counter);
                if (counter > frames.Value)
                {
                    Finish();
                    if (finishEvent != null)
                    {
                        Fsm.Event(finishEvent);
                    }
                }
            }

        }
    }