﻿using UnityEngine;
using System.Collections;
namespace HutongGames.PlayMaker.Actions
{
	public class AN_HOL_OnTarelka : FsmStateAction {
		public FsmOwnerDefault owner;

		private GameObject _tarelka;
		private GameObject _parent;
		private MeshFilter _mf;
		private float _size;
		private int _countOfChild;
		private float _delta;
		private Vector3 position;
		private float _leftPoint;
		private int tilt = 92;

	public override void OnEnter () {
			_tarelka = Fsm.GetOwnerDefaultTarget (owner).transform.parent.GetChild(0).gameObject;
			_parent = _tarelka.transform.parent.GetChild (1).gameObject;
			position = _tarelka.transform.position;

			_mf = _tarelka.GetComponent<MeshFilter> ();
			_size = _mf.mesh.bounds.size.x;
			_leftPoint = position.x - _size / 2;

			_countOfChild = _parent.transform.GetChildCount ();
			_delta = _size / _countOfChild;

			for (int i = 0; i < _countOfChild; i++) {
				if (_countOfChild == 1) {
					_parent.transform.GetChild (0).transform.localPosition = new Vector3 (0,50, -500);
				
					break;
				}
				if (_countOfChild == 2) {
					_parent.transform.GetChild (0).transform.localPosition = new Vector3 (-_parent.transform.GetChild (0).GetComponent<BoxCollider>().size.x *
						_parent.transform.GetChild (0).transform.localScale.x/2, 50, -500);
					_parent.transform.GetChild (1).transform.localPosition = new Vector3 (_parent.transform.GetChild (0).GetComponent<BoxCollider>().size.x *
						_parent.transform.GetChild (0).transform.localScale.x/2, 50, -500);
					break;
				}
				if (_countOfChild == 3) {
					_parent.transform.GetChild (0).transform.localPosition = new Vector3 (-_parent.transform.GetChild (0).GetComponent<BoxCollider>().size.x *
						_parent.transform.GetChild (0).transform.localScale.x, 50, -500);
					_parent.transform.GetChild (1).transform.localPosition = new Vector3 (0,50, -500);
					_parent.transform.GetChild (2).transform.localPosition = new Vector3 (_parent.transform.GetChild (0).GetComponent<BoxCollider>().size.x *
						_parent.transform.GetChild (0).transform.localScale.x, 50, -500);
					break;
				}
				if(_parent.transform.GetChild (0).GetComponent<BoxCollider>().size.y > 560)
					_parent.transform.GetChild (i).transform.position = new Vector3 (_leftPoint + _delta * i + tilt, position.y + Mathf.Pow(-1, i)* 30 + 100, position.z - 8 * Mathf.Pow(-1, i+1) - 50);
				else
					_parent.transform.GetChild (i).transform.position = new Vector3 (_leftPoint + _delta * i + tilt, position.y + Mathf.Pow(-1, i)* 30 + 60, position.z - 8 * Mathf.Pow(-1, i+1) - 50);
			}
			Finish ();
		}
}
}