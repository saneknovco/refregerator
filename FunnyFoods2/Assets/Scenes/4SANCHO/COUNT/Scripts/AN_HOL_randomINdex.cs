﻿using UnityEngine;
using System.Collections;

namespace HutongGames.PlayMaker.Actions
{

	public class AN_HOL_randomINdex :FsmStateAction {
		public FsmOwnerDefault owner;
		public FsmArray arr;
		public FsmArray storeArr;
		public FsmInt storeIndex;
		private int arrIndex;

		GameObject go;
		public override void OnEnter () {
			int[] ar = new int[arr.Values.Length];
			go = Fsm.GetOwnerDefaultTarget (owner);

			for (int i = 0; i < arr.Values.Length; i++) {
				ar [i] = (int)arr.Values [i];
			}
			arrIndex = Random.Range (0, arr.Values.Length);
			storeIndex.Value = (int)ar.GetValue (arrIndex);
			Debug.Log (ar.Length);
			ar [arrIndex] = -1;

			for (int i = 0; i < ar.Length; i++) {
				storeArr.Values[i] = ar [i];
			}
			Finish ();
		}

	}

}