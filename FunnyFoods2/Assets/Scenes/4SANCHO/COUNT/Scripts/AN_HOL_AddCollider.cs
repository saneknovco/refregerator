﻿using UnityEngine;
using System.Collections;

namespace HutongGames.PlayMaker.Actions
{
public class AN_HOL_AddCollider : FsmStateAction {

		public FsmOwnerDefault owner;
		GameObject go;
		private float _firstChildColPos;
		private float _lastChildColPos;
		private float _distance;
		private float _height;

		public override void OnEnter () {
			go = Fsm.GetOwnerDefaultTarget (owner);
			_firstChildColPos = go.transform.GetChild (0).transform.position.x - go.transform.GetChild (0).GetChild(0).GetChild(0).GetComponent<MeshRenderer>().bounds.size.x/2;
			_lastChildColPos = go.transform.GetChild (go.transform.childCount - 1).transform.position.x + go.transform.GetChild (go.transform.childCount - 1).GetChild (0).GetChild (0)
				.GetComponent<MeshRenderer> ().bounds.size.x/2;
			_distance = (_lastChildColPos - _firstChildColPos);
			_height = go.transform.GetChild (0).GetChild (0).GetChild (0).GetComponent<MeshRenderer>().bounds.size.y*1.5f;
			BoxCollider bc = go.GetComponent<BoxCollider> ();
			bc.size = new Vector3 (_distance*1.5f, _height, 1);
			bc.center = new Vector3 (0, 60, 0);
			Finish();
		}
}
}
