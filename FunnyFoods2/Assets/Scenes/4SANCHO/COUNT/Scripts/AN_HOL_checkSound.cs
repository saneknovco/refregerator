﻿using UnityEngine;
using System.Collections;


namespace HutongGames.PlayMaker.Actions
{

	public class AN_HOL_checkSound :FsmStateAction  {
		public FsmOwnerDefault owner;
		private GameObject _obj;
		private int _childCount;
		private GameObject _go;
		public override void OnEnter(){
			_go = Fsm.GetOwnerDefaultTarget (owner);
			_obj = Camera.main.gameObject;
			_childCount = _obj.transform.childCount;

			for (int i = 0; i < _childCount; i++) {
				for (int j = 1; j < 6; j++) {
					if (_obj.transform.GetChild (i).name.Equals ("Audio:" + j)) {
						Object.Destroy (_obj.transform.GetChild (i).gameObject);
						break;
					}
				}
			}
			Finish ();
	}
	
	
}
}