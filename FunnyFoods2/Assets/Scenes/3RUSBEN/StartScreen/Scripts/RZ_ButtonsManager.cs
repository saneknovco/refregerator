﻿using UnityEngine;
using UnityEngine.SceneManagement;
using System.Collections;

public enum ButtonsNames
{
	backButton,
	repeatButton,
    settingsButton,
    infoButton
}
public class RZ_ButtonsManager : MonoBehaviour 
{
	[Header("Buttons")]
	public GameObject buttons;
	[Header("Root scene")]
	public GameObject root;
	void DoSwitchButtons(GameObject source)
	{
		RZ_SceneSetting scSettings;
		if ((scSettings = source.GetComponent<RZ_SceneSetting> ()) != null) 
		{
			for (int i = 0; i < buttons.transform.childCount; i++) 
			{
				buttons.transform.GetChild (i).gameObject.SetActive (false);
			}
			for (int i = 0; i < scSettings.buttons.Length; i++) 
			{
				GameObject currentButton;
				if ((currentButton = buttons.FindChild (scSettings.buttons [i].ToString ())) != null)
					currentButton.SetActive (true);
			}
		}
	}
	void SwitchButtons()
	{
		string sceneName = SceneManager.GetActiveScene ().name;
		if (sceneName == root.name) 
		{
			DoSwitchButtons (root);
		} 
		else 
		{
			GameObject currentScene;
			if ((currentScene = root.FindChild (sceneName)) != null) 
			{
				DoSwitchButtons (currentScene);
			}
		}
	}

	void Start () 
	{
		SwitchButtons ();
	}

	void OnLevelWasLoaded(int level)
	{
		SwitchButtons ();
	}
}
