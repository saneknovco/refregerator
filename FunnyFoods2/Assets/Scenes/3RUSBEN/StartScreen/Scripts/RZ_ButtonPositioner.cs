﻿using UnityEngine;
using System.Collections;

public class RZ_ButtonPositioner : MonoBehaviour 
{
	public Positions position;
	public float indent;


	void Start () 
	{
		DoPositioning ();
	}

	void DoPositioning()
	{
		float height = Camera.main.orthographicSize;
		float aspect = Camera.main.aspect;
		float width = height * aspect;

		Vector2 colliderSize = transform.GetComponent<BoxCollider>().size * transform.localScale.x;

		switch (position) 
		{
		case Positions.leftTop:
			{
				transform.localPosition = new Vector2(-width + colliderSize.x / 2 + indent, height - colliderSize.y / 2 - indent);
			}
			break;
		case Positions.rightTop:
			{
				transform.localPosition = new Vector2(width - colliderSize.x / 2 - indent, height - colliderSize.y / 2 - indent);
			}
			break;
		case Positions.leftBottom:
			{
				transform.localPosition = new Vector2(-width + colliderSize.x / 2 + indent, -height + colliderSize.y / 2 + indent);
			}
			break;
		case Positions.rightBottom:
			{
				transform.localPosition = new Vector2(width - colliderSize.x / 2 - indent, -height + colliderSize.y / 2 + indent);
			}
			break;
		}
	}

	public enum Positions
	{
		leftTop, rightTop, leftBottom, rightBottom
	}
}
