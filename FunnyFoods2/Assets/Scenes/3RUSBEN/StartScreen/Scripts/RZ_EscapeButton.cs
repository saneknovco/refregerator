﻿using UnityEngine;
using UnityEngine.SceneManagement;
using HutongGames.PlayMaker;

public class RZ_EscapeButton : MonoBehaviour 
{
    public GameObject GUIContainer;
    public GameObject root;
    GameObject _GUIObject;
	void Start () 
	{
		if(Application.platform == RuntimePlatform.IPhonePlayer) Object.Destroy(transform.GetComponent<RZ_EscapeButton>());
	}

	void Update () 
	{
		if (Input.GetKeyUp(KeyCode.Escape))
		{
            if (GUIContainer != null && GUIContainer.transform.childCount > 0)
            {
                _GUIObject = GUIContainer.transform.GetChild(0).gameObject;
                RZ_GUIObjectSetting GUISettings = _GUIObject.GetComponent<RZ_GUIObjectSetting>();
                if(GUISettings.closeGUIObject != null)
                {
                    PlayMakerFSM closeFSM;
                    if ((closeFSM = GUISettings.closeGUIObject.GetComponent<PlayMakerFSM>()) != null)
                        closeFSM.SendEvent(GUISettings.closeGUIEvent);
                }
            }
            else
            {
                string sceneName = SceneManager.GetActiveScene().name;
                if (sceneName != root.name)
                {
                    GameObject currentScene;
                    if ((currentScene = root.FindChild(sceneName)) != null)
                    {
                        GameObject needScene;
                        if ((needScene = currentScene.transform.parent.gameObject) != null)
                            SceneManager.LoadScene(needScene.name);
                    }
                }
                else
                {
                    //TO DO: Event to quit form
                    Application.Quit();
                }
            }
		}
	}
}
