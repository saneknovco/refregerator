﻿using UnityEngine;
using UnityEngine.SceneManagement;

public class RZ_BackButton : MonoBehaviour 
{
	public GameObject root;
	public float bounceDelta = 1.05f; 
	float scale;
	bool isExit = false;
	void Awake()
	{
		scale = transform.localScale.x;
	}
	void OnMouseDownMT()
	{
		isExit = false;
		transform.localScale = new Vector3 (scale * bounceDelta, scale * bounceDelta, transform.localScale.z);
	}
	void OnMouseUpAsButtonMT()
	{
		if (!isExit) 
		{
			transform.localScale = new Vector3 (scale, scale, transform.localScale.z);
			string sceneName = SceneManager.GetActiveScene ().name;
			if (sceneName != root.name) 
			{
				GameObject currentScene;
				if ((currentScene = root.FindChild (sceneName)) != null) 
				{
					GameObject needScene;
					if(( needScene = currentScene.transform.parent.gameObject)!= null)
                    {
                        Time.timeScale = 1;
                        PlayMakerFSM.BroadcastEvent("FF2/StartLoadScene");
						SceneManager.LoadScene (needScene.name);
                    }
				}
			}
		}
	}
	void OnMouseUpMT()
	{
		isExit = true;
		transform.localScale = new Vector3 (scale, scale, transform.localScale.z);
	}
	void OnMouseExitMT()
	{
		isExit = true;
		transform.localScale = new Vector3 (scale, scale, transform.localScale.z);
	}
}
