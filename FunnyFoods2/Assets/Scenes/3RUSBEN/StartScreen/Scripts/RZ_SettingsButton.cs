﻿using UnityEngine;
using System.Collections;

public class RZ_SettingsButton : MonoBehaviour 
{
	public GameObject GUIContainer;
	public string SettingsPrefabPath;
	public string ClickSound;
	public float bounceDelta = 1.05f; 
	float scale;
	bool isExit = false;
	void Awake()
	{
		scale = transform.localScale.x;
	}
	void OnMouseDownMT()
	{
		isExit = false;
		transform.localScale = new Vector3 (scale * bounceDelta, scale * bounceDelta, transform.localScale.z);
	}
	void OnMouseUpAsButtonMT()
	{
		if (!isExit) 
		{
			transform.localScale = new Vector3 (scale, scale, transform.localScale.z);
			ScenarioUtils.PlaySound (ClickSound, HutongGames.PlayMaker.FsmVariables.GlobalVariables.GetFsmFloat ("FunnyFoods/SystemVolume").Value);
//			AudioClip clip = Resources.Load<AudioClip> ("Sounds/" + ClickSound);
//			if(clip != null)
//			{
//				GameObject audioGO = new GameObject (clip.name + "_SOUND");
//				audioGO.transform.parent = Camera.main.transform;
//				AudioSource audioSource = audioGO.AddComponent<AudioSource> ();
//				if(audioSource != null)
//				{
//					audioSource.clip = clip;
//					audioSource.volume = HutongGames.PlayMaker.FsmVariables.GlobalVariables.GetFsmFloat ("FunnyFoods/SystemVolume").Value;
//					GameObject.Destroy(audioGO, clip.length + 0.5f);
//					audioSource.Play ();
//				}
//			}
			GameObject settings;
			if ((settings = Resources.Load<GameObject> (SettingsPrefabPath)) != null) 
			{
				GameObject settingsInstance = GameObject.Instantiate (settings);
				settingsInstance.name = settings.name;
				settingsInstance.transform.parent = GUIContainer.transform;
				settingsInstance.transform.localPosition = Vector3.zero;
			}
		}
	}
	void OnMouseUpMT()
	{
		isExit = true;
		transform.localScale = new Vector3 (scale, scale, transform.localScale.z);
	}
	void OnMouseExitMT()
	{
		isExit = true;
		transform.localScale = new Vector3 (scale, scale, transform.localScale.z);
	}
}
