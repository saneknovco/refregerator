﻿using UnityEngine;
using UnityEngine.SceneManagement;
using System.Collections;

public class RZ_RepeatButton : MonoBehaviour 
{
	public float bounceDelta = 1.05f; 
	float scale;
	bool isExit = false;
	void Awake()
	{
		scale = transform.localScale.x;
	}
	void OnMouseDownMT()
	{
		isExit = false;
		transform.localScale = new Vector3 (scale * bounceDelta, scale * bounceDelta, transform.localScale.z);
	}
	void OnMouseUpAsButtonMT()
	{
		if (!isExit) 
		{
			transform.localScale = new Vector3 (scale, scale, transform.localScale.z);
            Time.timeScale = 1;
            SceneManager.LoadScene (SceneManager.GetActiveScene ().name);
		}
	}
	void OnMouseUpMT()
	{
		isExit = true;
		transform.localScale = new Vector3 (scale, scale, transform.localScale.z);
	}
	void OnMouseExitMT()
	{
		isExit = true;
		transform.localScale = new Vector3 (scale, scale, transform.localScale.z);
	}
}
