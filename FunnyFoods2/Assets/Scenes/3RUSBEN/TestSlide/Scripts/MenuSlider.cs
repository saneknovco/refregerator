﻿using UnityEngine;
using UnityEngine.Events;
using System.Collections;

namespace FF2
{
	public enum SlideStates
	{
		Moving,
		Attraction,
		Closed,
		Opened,
		Attracting, 
		Hide,
		Reset
	}
	public class MenuSlider : MonoBehaviour 
	{
		/// <summary>
		/// PUBLIC VARIABLES
		/// </summary>
		public Vector3 startPosition;
		public Vector3 openedPosition;
		public Vector3 hidePosition;
		public BoolXYZ blockCoordinats;
		public float noReturnPercentToOpen;
		public float noReturnPercentToClose;
		public float attractionSpeed, slowAttractionSpeed;
		public float maxTapDuration = 0.3f;
		public float moveTolerance;
		public Vector3 startScale;
		public Vector3 destinationScale;
		public float timeForScale;
		public EaseCurvesFunctions.EaseType easeType;
		public GameObject border;
		public UnityEvent blockEvent, unblockEvent;

		/// <summary>
		/// PRIVATE VARIABLES
		/// </summary>
		private SlideStates _currentState, _nextState, _previousState;
		private SlideStates _lastFiniteState = SlideStates.Closed;
		private bool _isTap, _mouseWasPressed, _isBlocked;
		private float _tappingTime, _fullDistance;
		private Vector3 _delta, _min, _max;

		/// <summary>
		/// VARIABLES FOR EDITOR
		/// </summary>
		[HideInInspector]
		public bool _eventsFoldout = true, _scaleFoldout = true, _movementFoldout = true, _noReturnPointsFoldout = true, _speedsFoldout = true;

		void InitParameters()
		{
			_tappingTime = 0f;

			_currentState = SlideStates.Closed;

			_min = new Vector3 (Mathf.Min (startPosition.x, openedPosition.x), Mathf.Min (startPosition.y, openedPosition.y), Mathf.Min (startPosition.z, openedPosition.z));
			_max = new Vector3 (Mathf.Max (startPosition.x, openedPosition.x), Mathf.Max (startPosition.y, openedPosition.y), Mathf.Max (startPosition.z, openedPosition.z));

			transform.position = startPosition;
			transform.localScale = startScale;
		}
		void Start () 
		{
			InitParameters ();
		}
		void OnEnable()
		{
			InitParameters ();
		}
		void OnMouseDown()
		{
			if (_currentState != SlideStates.Attracting) {
				_mouseWasPressed = true;
				_delta = Camera.main.ScreenPointToRay (Input.mousePosition).origin - transform.position;
				_previousState = _currentState;
				_currentState = SlideStates.Moving;
			}
		}
		void OnMouseDrag()
		{
			if (_currentState != SlideStates.Attracting) {
				_isTap = false;
				_tappingTime += Time.deltaTime;
			}
		}
		void OnMouseUp()
		{
			if (_currentState != SlideStates.Attracting) {
				_mouseWasPressed = false;
				_isTap = _tappingTime <= maxTapDuration;
				_tappingTime = 0f;
				_previousState = _currentState;
				_currentState = SlideStates.Attraction;
			}
		}
		void Update ()
		{
			Slide ();
		}
		
		void Slide()
		{
			switch (_currentState) 
			{
				case SlideStates.Closed:
				{
					if (_lastFiniteState != _currentState) 
					{
						_lastFiniteState = SlideStates.Closed;
					}
					if (_previousState != _currentState) 
					{
						if (unblockEvent != null && _isBlocked) 
						{
							unblockEvent.Invoke ();
							_isBlocked = false;
						}
					}
					break;
				}
				case SlideStates.Opened:
				{
					if (_lastFiniteState != _currentState) 
					{
						_lastFiniteState = SlideStates.Opened;

						ScaleToByTime scaler = transform.gameObject.AddComponent<ScaleToByTime> ();
						scaler.destinationScale = destinationScale;
						scaler.time = timeForScale;
						scaler.easeType = easeType;
						InterpolateAlpha interpolator = border.AddComponent<InterpolateAlpha> ();
						interpolator.isRecursive = true;
						interpolator.toAlpha = 1f;
						interpolator.time = 0f;
					}
					break;
				}
				case SlideStates.Moving:
				{
					if (_previousState != _currentState) 
					{
						if (blockEvent != null && !_isBlocked) 
						{
							blockEvent.Invoke ();
							_isBlocked = true;
						}
					}
					Vector3 newPosition = Camera.main.ScreenPointToRay (Input.mousePosition).origin - _delta;

					if (!blockCoordinats.x) {
						if (newPosition.x < _min.x - moveTolerance)
							newPosition.x = _min.x - moveTolerance;
						else if (newPosition.x > _max.x + moveTolerance)
							newPosition.x = _max.x + moveTolerance;
					}
					else
						newPosition.x = transform.position.x;
					
					if (!blockCoordinats.y) {
						if (newPosition.y < _min.y - moveTolerance)
							newPosition.y = _min.y - moveTolerance;
						else if (newPosition.y > _max.y + moveTolerance)
							newPosition.y = _max.y + moveTolerance;
					}
					else
						newPosition.y = transform.position.y;
					
					if (!blockCoordinats.z) {
						if (newPosition.z < _min.z - moveTolerance)
							newPosition.z = _min.z - moveTolerance;
						else if (newPosition.z > _max.z + moveTolerance)
							newPosition.z = _max.z + moveTolerance;
					}
					else
						newPosition.z = transform.position.z;
					
					transform.position = new Vector3(newPosition.x, newPosition.y, newPosition.z);
					break;
				}
				case SlideStates.Attraction:
				{
					RM_MoveToBySpeed mover = transform.gameObject.AddComponent<RM_MoveToBySpeed> ();
					if (_isTap) 
					{
						if (_lastFiniteState == SlideStates.Closed) 
						{
							mover.destinationPosition = openedPosition;
							_nextState = SlideStates.Opened;
						}
						if (_lastFiniteState == SlideStates.Opened) 
						{
							mover.destinationPosition = hidePosition;
							_nextState = SlideStates.Reset;
						}
					} 
					else 
					{
						if (_lastFiniteState == SlideStates.Closed) 
						{
							_fullDistance = Vector3.Distance (startPosition, openedPosition);
							if (Vector3.Distance (startPosition, transform.position) / _fullDistance >= noReturnPercentToOpen) 
							{
								mover.destinationPosition = openedPosition;
								_nextState = SlideStates.Opened;
							} 
							else 
							{
								mover.destinationPosition = startPosition;
								_nextState = SlideStates.Closed;
							}
						} 
						if (_lastFiniteState == SlideStates.Opened)
						{
							_fullDistance = Vector3.Distance (hidePosition, openedPosition);
							if (Vector3.Distance (openedPosition, transform.position) / _fullDistance < noReturnPercentToClose) 
							{
								mover.destinationPosition = openedPosition;
								_nextState = SlideStates.Opened;
							} 
							else 
							{
								mover.destinationPosition = hidePosition;
								_nextState = SlideStates.Reset;
							}
						}
					}
					mover.speed = ((!blockCoordinats.x && (transform.position.x < _min.x || transform.position.x > _max.x)) || 
								   (!blockCoordinats.y && (transform.position.y < _min.y || transform.position.y > _max.y)) ||
								   (!blockCoordinats.z && (transform.position.z < _min.z || transform.position.z > _max.z)))&&
								   !_isTap ? slowAttractionSpeed : attractionSpeed;
					_previousState = _currentState;
					_currentState = SlideStates.Attracting;
					break;
				}
				case SlideStates.Attracting:
				{
					if (transform.GetComponent<RM_MoveToBySpeed> () == null) 
					{
						_previousState = _currentState;
						_currentState = _nextState;
					}
					break;
				}
				case SlideStates.Hide:
				{
					if (transform.GetComponent<RM_MoveToBySpeed> () == null) 
					{
						RM_MoveToBySpeed mover = transform.gameObject.AddComponent<RM_MoveToBySpeed> ();
						mover.destinationPosition = startPosition;
						mover.speed = attractionSpeed;
						_nextState = SlideStates.Closed;
						_previousState = _currentState;
						_currentState = SlideStates.Attracting;
					}
					break;
				}
				case SlideStates.Reset:
				{
					transform.localScale = startScale;
					border.RecursiveSetAlpha (0f);
					_previousState = _currentState;
					_currentState = SlideStates.Hide;
					break;
				}
				
			}
		}
	}
}