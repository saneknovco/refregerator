﻿using UnityEngine;
using System.Collections;

namespace FF2
{
	public class ScaleToByTime : MonoBehaviour 
	{
		public Vector3 destinationScale;
		public float time;
		public EaseCurvesFunctions.EaseType easeType;
		private float _currentTime;
		private Vector3 _startScale;

		void Start () 
		{
			_currentTime = 0;
			_startScale = transform.localScale;
		}
		void OnEnable()
		{
			_currentTime = 0;
			_startScale = transform.localScale;
		}
		void Update () 
		{
			if (_currentTime <= time) {
				transform.localScale = EaseCurvesFunctions.EaseForVector (_startScale, destinationScale, _currentTime / time, EaseCurvesFunctions.GetMethod(easeType));
			} else {
				transform.localScale = destinationScale;
				Object.Destroy (this);
			}
			_currentTime += Time.deltaTime;
		}
	}
}