﻿using UnityEngine;
using System.Collections;
using HutongGames.PlayMaker.Actions;
namespace FF2
{
	public class InterpolateAlpha : MonoBehaviour 
	{
		[Range(0f,1f)]
		public float fromAlpha;
		[Range(0f,1f)]
		public float toAlpha;
		public float time;
		public bool isRecursive;
		private float _currentAlpha, _currentTime;

		void Start () 
		{
			_currentTime = 0f;
		}
		void OnEnable()
		{
			_currentTime = 0f;
		}
		void Update ()
		{
			if (_currentTime <= time) 
			{
				_currentAlpha = Mathf.Lerp (fromAlpha, toAlpha, _currentTime / time);
				if (isRecursive)
					transform.gameObject.RecursiveSetAlpha (_currentAlpha);
				else
					transform.gameObject.SetAlpha (_currentAlpha);
			} 
			else 
			{
				if (isRecursive)
					transform.gameObject.RecursiveSetAlpha (toAlpha);
				else
					transform.gameObject.SetAlpha (toAlpha);
				Object.Destroy (this);
			}
			_currentTime += Time.deltaTime;
			
		}
	}
}