﻿using UnityEngine;
using UnityEditor;

namespace FF2
{
	[CustomEditor(typeof(MenuSlider))]
//	[ExecuteInEditMode]
	[CanEditMultipleObjects]
	public class MenuSliderEditor : Editor
	{
		SerializedProperty startPositionProp;
		SerializedProperty openedPositionProp;
		SerializedProperty hidePositionProp;
		SerializedProperty blockCoordinatsProp;
		SerializedProperty noReturnPercentToOpenProp;
		SerializedProperty noReturnPercentToCloseProp;
		SerializedProperty attractionSpeedProp;
		SerializedProperty slowAttractionSpeedProp;
		SerializedProperty maxTapDurationProp;
		SerializedProperty moveToleranceProp;
		SerializedProperty startScaleProp;
		SerializedProperty destinationScaleProp;
		SerializedProperty timeForScaleProp;
		SerializedProperty easeTypeProp;
		SerializedProperty borderProp;
		SerializedProperty blockEventProp;
		SerializedProperty unblockEventProp;
		MenuSlider ms;
		float margin = 15f, labelWidth = 140f;

		void OnEnable()
		{
			ms = (MenuSlider)target;
			startPositionProp = serializedObject.FindProperty ("startPosition");
			openedPositionProp = serializedObject.FindProperty ("openedPosition");
			hidePositionProp = serializedObject.FindProperty ("hidePosition");
			blockCoordinatsProp = serializedObject.FindProperty ("blockCoordinats");
			noReturnPercentToOpenProp = serializedObject.FindProperty ("noReturnPercentToOpen");
			noReturnPercentToCloseProp = serializedObject.FindProperty ("noReturnPercentToClose");
			attractionSpeedProp = serializedObject.FindProperty ("attractionSpeed");
			slowAttractionSpeedProp = serializedObject.FindProperty ("slowAttractionSpeed");
			maxTapDurationProp = serializedObject.FindProperty ("maxTapDuration");
			moveToleranceProp = serializedObject.FindProperty ("moveTolerance");
			startScaleProp = serializedObject.FindProperty ("startScale");
			destinationScaleProp = serializedObject.FindProperty ("destinationScale");
			timeForScaleProp = serializedObject.FindProperty ("timeForScale");
			easeTypeProp = serializedObject.FindProperty ("easeType");
			borderProp = serializedObject.FindProperty ("border");
			blockEventProp = serializedObject.FindProperty ("blockEvent");
			unblockEventProp = serializedObject.FindProperty ("unblockEvent");
		}

		public override void OnInspectorGUI ()
		{
			serializedObject.Update ();
			EditorGUILayout.BeginVertical();

			ms._movementFoldout = EditorGUILayout.Foldout (ms._movementFoldout, "Movement & positions settings");
			if (ms._movementFoldout) 
			{
				EditorGUILayout.BeginHorizontal ();
				GUILayout.Space (margin);
				EditorGUILayout.BeginVertical ();

					startPositionProp.vector3Value = EditorGUILayout.Vector3Field (new GUIContent ("Start position"), startPositionProp.vector3Value);
					
					openedPositionProp.vector3Value = EditorGUILayout.Vector3Field (new GUIContent ("Opened position"), openedPositionProp.vector3Value);
					
					hidePositionProp.vector3Value = EditorGUILayout.Vector3Field (new GUIContent ("Hide position"), hidePositionProp.vector3Value);
					
					EditorGUILayout.BeginHorizontal ();
						EditorGUILayout.LabelField ("Blocked coordinates", GUILayout.Width (150f));
						EditorGUILayout.PropertyField (blockCoordinatsProp);
					EditorGUILayout.EndHorizontal ();
					EditorGUILayout.Space ();
					
					EditorGUILayout.BeginHorizontal ();
						EditorGUILayout.LabelField ("Move tolerance", GUILayout.Width (labelWidth));
						moveToleranceProp.floatValue = EditorGUILayout.FloatField (moveToleranceProp.floatValue);
					EditorGUILayout.EndHorizontal ();
					
					EditorGUILayout.BeginHorizontal ();
						EditorGUILayout.LabelField ("Max tap duartion", GUILayout.Width (labelWidth));
						maxTapDurationProp.floatValue = EditorGUILayout.FloatField (maxTapDurationProp.floatValue);
					EditorGUILayout.EndHorizontal ();
					EditorGUILayout.Space ();
					
					ms._noReturnPointsFoldout = EditorGUILayout.Foldout (ms._noReturnPointsFoldout, "No return points");
					if (ms._noReturnPointsFoldout) 
					{
							EditorGUILayout.Slider (noReturnPercentToOpenProp, 0f, 1f, new GUIContent ("To open"));

							EditorGUILayout.Slider (noReturnPercentToCloseProp, 0f, 1f, new GUIContent ("To close"));
					}
					EditorGUILayout.Space ();
					
					ms._speedsFoldout = EditorGUILayout.Foldout (ms._speedsFoldout, "Attraction speeds");
					if (ms._speedsFoldout) 
					{
						EditorGUILayout.BeginHorizontal ();
							EditorGUILayout.LabelField ("Standart", GUILayout.Width (labelWidth));
							attractionSpeedProp.floatValue = EditorGUILayout.FloatField (attractionSpeedProp.floatValue);
						EditorGUILayout.EndHorizontal ();
						EditorGUILayout.BeginHorizontal ();
							EditorGUILayout.LabelField ("Slow", GUILayout.Width (labelWidth));
							slowAttractionSpeedProp.floatValue = EditorGUILayout.FloatField (slowAttractionSpeedProp.floatValue);
						EditorGUILayout.EndHorizontal ();
					}
				EditorGUILayout.EndVertical();
				EditorGUILayout.EndHorizontal ();
			}
			EditorGUILayout.Space ();

			ms._scaleFoldout = EditorGUILayout.Foldout (ms._scaleFoldout, "Scaling settings");
			if (ms._scaleFoldout) 
			{
				EditorGUILayout.BeginHorizontal ();
				GUILayout.Space (margin);
				EditorGUILayout.BeginVertical ();
				startScaleProp.vector3Value = EditorGUILayout.Vector3Field (new GUIContent ("Start scale"), startScaleProp.vector3Value);

				destinationScaleProp.vector3Value = EditorGUILayout.Vector3Field (new GUIContent ("Destination scale"), destinationScaleProp.vector3Value);

				EditorGUILayout.BeginHorizontal ();
					EditorGUILayout.LabelField ("Scale time", GUILayout.Width (labelWidth));
					timeForScaleProp.floatValue = EditorGUILayout.FloatField (timeForScaleProp.floatValue);
				EditorGUILayout.EndHorizontal ();

				ms.easeType = (EaseCurvesFunctions.EaseType)EditorGUILayout.EnumPopup ("Ease type", ms.easeType);
				EditorGUILayout.EndVertical();
				EditorGUILayout.EndHorizontal ();
			}
			EditorGUILayout.Space ();

			ms._eventsFoldout = EditorGUILayout.Foldout (ms._eventsFoldout, "Events");
			if (ms._eventsFoldout)
			{
				EditorGUILayout.BeginHorizontal ();
				GUILayout.Space (margin);
				EditorGUILayout.BeginVertical ();

				EditorGUILayout.PropertyField (blockEventProp);

				EditorGUILayout.PropertyField (unblockEventProp);

				EditorGUILayout.EndVertical();
				EditorGUILayout.EndHorizontal ();
			}
			EditorGUILayout.Space ();

			EditorGUILayout.BeginHorizontal ();
				EditorGUILayout.LabelField ("Border object", GUILayout.Width (labelWidth));
				borderProp.objectReferenceValue = EditorGUILayout.ObjectField (borderProp.objectReferenceValue, typeof(GameObject), true);
			EditorGUILayout.EndHorizontal();

			EditorGUILayout.EndVertical();

			serializedObject.ApplyModifiedProperties ();
		}
	}
}
