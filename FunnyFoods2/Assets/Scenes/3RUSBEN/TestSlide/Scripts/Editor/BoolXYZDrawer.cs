﻿using UnityEngine;
using UnityEditor;

namespace FF2
{
	[CustomPropertyDrawer(typeof(BoolXYZ))]
	public class BoolXYZDrawer : PropertyDrawer
	{
		public override void OnGUI(Rect position, SerializedProperty property, GUIContent label)
		{
			EditorGUI.BeginProperty (position, label, property);
	
			SerializedProperty x = property.FindPropertyRelative ("x");
			SerializedProperty y = property.FindPropertyRelative ("y");
			SerializedProperty z = property.FindPropertyRelative ("z");
	
			Rect xLabelRect = new Rect (position.x-15, position.y, 10, position.height);
			Rect xRect = new Rect (position.x + -5, position.y, 30, position.height);
			Rect yLabelRect = new Rect (position.x + 25, position.y, 10, position.height);
			Rect yRect = new Rect (position.x + 35, position.y, 30, position.height);
			Rect zLabelRect = new Rect (position.x + 65, position.y, 10, position.height);
			Rect zRect = new Rect (position.x + 75, position.y, 30, position.height);
	
			EditorGUI.LabelField(xLabelRect, "X");
			EditorGUI.PropertyField (xRect, x, GUIContent.none);
			EditorGUI.LabelField(yLabelRect, "Y");
			EditorGUI.PropertyField (yRect, y, GUIContent.none);
			EditorGUI.LabelField(zLabelRect, "Z");
			EditorGUI.PropertyField (zRect, z, GUIContent.none); 
	
			EditorGUI.EndProperty ();
		}
	}
}