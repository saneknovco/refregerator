﻿using UnityEngine;
using UnityEngine.Events;
using System.Collections;
using HutongGames;
namespace FF2
{
	public class TestEvents : MonoBehaviour 
	{
		public void Fade(float alpha)
		{
			transform.gameObject.SetAlpha (alpha);
			BoxCollider coll = transform.GetComponent<BoxCollider> ();
			if (coll != null) coll.enabled = !coll.enabled;
		}
	}
//	[System.Serializable]
//	public class PlayMakerFSMEvent : UnityEvent<PlayMakerFSMEventArgs>
//	{
//		
//	}
//	public class PlayMakerFSMEventArgs
//	{
//		PlayMakerFSM stateMachine;
//		string eventName;
//		bool toAllMachinesOnObject;
//		public PlayMakerFSMEventArgs ()
//		{
//			
//		}
//		public PlayMakerFSMEventArgs (PlayMakerFSM stateMachine, string eventName, bool toAllMachinesOnObject)
//		{
//			this.stateMachine = stateMachine;
//			this.eventName = eventName;
//			this.toAllMachinesOnObject = toAllMachinesOnObject;
//		}
//	}
}