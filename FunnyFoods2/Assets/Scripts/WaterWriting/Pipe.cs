﻿#if UNITY_EDITOR
using UnityEditor;
#endif
using UnityEngine;
using System.Collections.Generic;
using Bini.Utils;

[ExecuteInEditMode]
public class Pipe : MonoBehaviour {
	[System.Serializable]
	public class PipeEnd {
		public int id = 0;
		public Transform tfm = null;
		public float dropScale, dropSize;
		
		public Pipe pipe = null;
		public int side = -1;
		public bool free = true;
		public float inflation = 0f;
		
		public PipeEnd GetEnd() {
			return (pipe == null) ? null : pipe.GetEnd(side);
		}
		
		public void Reset(int id) {
			this.id = id;
			tfm = null;
			dropScale = 0;
			dropSize = 0;
			
			pipe = null;
			side = 0;
			free = true;
		}
		public static PipeEnd Reset(PipeEnd pipe_end, int id) {
			if (pipe_end == null) pipe_end = new PipeEnd();
			pipe_end.Reset(id);
			return pipe_end;
		}
	}
	
	public PipeEnd end0, end1; // connectivity information
	public PipeEnd GetEnd(int id) {
		return (id == 0) ? end0 : ((id == 1) ? end1 : null);
	}
	
	void Connect(int id, Pipe pipe, int side, bool disconnect_prev) {
		if (pipe == null) side = -1;
		
		var dst_end = this.GetEnd(id);
		if (dst_end == null) return; // safeguard
		if ((dst_end.pipe == pipe) && (dst_end.side == side)) return;
		
		if (disconnect_prev && (dst_end.pipe != null)) {
			dst_end.pipe.Connect(dst_end.side, null, -1, false);
		}
		
		#if UNITY_EDITOR
		PrefabUtility.DisconnectPrefabInstance(this.gameObject);
		#endif
		dst_end.pipe = pipe;
		dst_end.side = side;
	}
	public void Connect(int id, Pipe pipe, int side) {
		this.Connect(id, pipe, side, true);
		pipe.Connect(side, this, id, true);
	}
	int NearestSide(PipeEnd src, Pipe dst_pipe, int dst_side=-1) {
		if ((dst_side < 0) || (dst_side > 1)) {
			var dist0 = (this.GetEndPosition(src.id, true) - dst_pipe.GetEndPosition(0, true)).magnitude;
			var dist1 = (this.GetEndPosition(src.id, true) - dst_pipe.GetEndPosition(1, true)).magnitude;
			dst_side = ((dist0 < dist1) ? 0 : 1);
		}
		return dst_side;
	}
	public int Connect(PipeEnd src, Pipe dst_pipe, int dst_side=-1, bool apply_src=true, bool apply_dst=true) {
		if (dst_pipe == null) dst_side = -1;
		if ((src.pipe == dst_pipe) && (src.side == dst_side)) return -1;
		
		if (dst_pipe != null) {
			dst_side = NearestSide(src, dst_pipe, dst_side);
			Debug.Log("Connected: "+this.name+"["+src.id+"] to "+dst_pipe.name+"["+dst_side+"]");
			if (apply_dst) dst_pipe.Connect(dst_side, this, src.id, (src.pipe != dst_pipe));
		} else if (src.pipe != null) {
			Debug.Log("Disconnected: "+this.name+"["+src.id+"] from "+src.pipe.name+"["+src.side+"]");
			if (apply_dst) src.pipe.Connect(src.side, null, -1, false);
		}
		
		if (apply_src) this.Connect(src.id, dst_pipe, dst_side, true);
		
		return dst_side;
	}
	public int Connect(Pipe dst_pipe, int dst_side=-1) {
		if (dst_pipe == null) return -1;
		
		int side0 = NearestSide(end0, dst_pipe, dst_side);
		int side1 = NearestSide(end1, dst_pipe, dst_side);
		
		var dist0 = (this.GetEndPosition(0, true) - dst_pipe.GetEndPosition(side0, true)).magnitude;
		var dist1 = (this.GetEndPosition(1, true) - dst_pipe.GetEndPosition(side1, true)).magnitude;
		
		var src = ((dist0 < dist1) ? end0 : end1);
		var dst = dst_pipe.GetEnd((dist0 < dist1) ? side0 : side1);
		if ((src.pipe == dst_pipe) && (src.side == dst.id)) return -1;
		
		Connect(src.id, dst_pipe, dst.id);
		return src.id;
	}
	public void DisconnectAll() {
		this.Connect(0, null, -1, true);
		this.Connect(1, null, -1, true);
	}
	
	public Vector3 GetEndPosition(int id, bool toWorld=false) {
		Vector3 pos;
		if (isBallon) {
			var drop_data = drops_data[(id == 0) ? 0 : drops_data.Length-1];
			pos = new Vector3(drop_data.x, drop_data.y, 0);
		} else {
			pos = GetEnd(id).tfm.localPosition;
		}
		if (toWorld) pos = transform.TransformPoint(pos);
		return pos;
	}
	
	public void ResetEndTransform(int id) {
		if (isBallon) return;
		var skin = GetComponent<SkinnedMeshRenderer>();
		var mesh = skin.sharedMesh;
		var bindposes = mesh.bindposes;
		var m = bindposes[(id == 0) ? 0 : bindposes.Length-1].inverse;
		var bone = GetEnd(id).tfm;
		bone.localPosition = m.ExtractPosition();
		bone.localRotation = m.ExtractRotation();
		bone.localScale = m.ExtractScale();
	}
	
	public PipeEnd hangingEnd {
		get { return GetEnd(hangingEndID); }
	}
	public int hangingEndID {
		get {
			bool is0 = (end0.pipe == null);
			bool is1 = (end1.pipe == null);
			return (is0 == is1) ? -1 : (is0 ? 0 : 1);
		}
	}
	
	public PipeEnd linkedEnd {
		get { return GetEnd(linkedEndID); }
	}
	public int linkedEndID {
		get {
			bool is0 = (end0.pipe != null);
			bool is1 = (end1.pipe != null);
			return (is0 == is1) ? -1 : (is0 ? 0 : 1);
		}
	}
	
	public PipeEnd flowEnd0 {
		get {
			if (flow_direction == 0) return null;
			return (flow_direction > 0) ? end0 : end1;
		}
	}
	public PipeEnd flowEnd1 {
		get {
			if (flow_direction == 0) return null;
			return (flow_direction > 0) ? end1 : end0;
		}
	}
	
	public bool isBallon = false;
	public bool isLinear = false;
	public Vector3[] drops_data = null;
	
	public bool BakeVertices = true;
	
	public float InflationScale = 1f;
	public Material WaterMaterial = null;
	public float WaterZOffset = 1f;
	
	public bool AutoManualSegment = true;
	public float ManualSegmentStart = -1f;
	public float ManualSegmentFinish = -1f;
	
	public float ManualStartOffset = 0f;
	public float ManualFinishOffset = 0f;
	
	public int flow_direction = 0;
	public float final_distance = 0f;
	public List<Vector4> path_points = null;
	bool exclude_first_point = false;
	bool exclude_last_point = false;
	List<Vector4> simplified_points = null;
	
	public float ProgressMin {
		get { return path_points[0].w; }
	}
	public float ProgressMax {
		get { return path_points[path_points.Count-1].w; }
	}
	public float ProgressRelative {
		get {
			var mat = ActiveMaterial;
			if (mat == null) return 0f;
			return mat.GetFloat("_FillAmount");
		}
	}
	public float Progress {
		get { return Mathf.Lerp(ProgressMin, ProgressMax, ProgressRelative); }
	}
	
	GameObject water_drops_obj = null;
	
	void OnDrawGizmos() {
		if (isBallon) {
			DrawEndGizmo_Ballon(end0, Color.black);
			DrawEndGizmo_Ballon(end1, Color.white);
			Gizmos.matrix = Matrix4x4.identity;
		} else {
			DrawEndGizmo_Pipe(end0, Color.black);
			DrawEndGizmo_Pipe(end1, Color.white);
			Gizmos.matrix = Matrix4x4.identity;
		}
	}
	void DrawEndGizmo_Ballon(PipeEnd endI, Color color) {
		if (endI == null) return;
		var drop_data = drops_data[(endI.id == 0) ? 0 : drops_data.Length-1];
		var pos = new Vector3(drop_data.x, drop_data.y, 0);
		var scale = drop_data.z;
		Gizmos.matrix = transform.localToWorldMatrix;
		color.a = (endI.free ? 0.25f : 0.125f);
		Gizmos.color = color;
		Gizmos.DrawSphere(pos, scale);
		color.a = (endI.free ? 0.5f : 0.25f);
		Gizmos.color = color;
		Gizmos.DrawWireSphere(pos, scale);
	}
	void DrawEndGizmo_Pipe(PipeEnd endI, Color color) {
		if ((endI == null) || (endI.tfm == null)) return;
		Gizmos.matrix = endI.tfm.localToWorldMatrix;
		color.a = (endI.free ? 0.25f : 0.125f);
		Gizmos.color = color;
		Gizmos.DrawCube(Vector3.zero, Vector3.one * endI.dropSize);
		color.a = (endI.free ? 0.5f : 0.25f);
		Gizmos.color = color;
		Gizmos.DrawWireCube(Vector3.zero, Vector3.one * endI.dropSize);
	}
	
	void UpdateEnd(PipeEnd endI) {
		if ((endI == null) || (endI.tfm == null)) return;
		if (endI.free) return;
		if ((endI.pipe != null) && endI.pipe.isBallon) return;
		
		var dst_end = endI.GetEnd();
		if (dst_end == null) return;
		if (!dst_end.free) return;
		
		var dst_tfm = dst_end.tfm;
		if (dst_tfm == null) return;
		
		var new_position = transform.InverseTransformPoint(dst_tfm.position);
		
		var sign_dst = ((endI.side == 0) ? -1f : 1f);
		var signI = ((endI.id == 0) ? 1f : -1f);
		var new_direction = transform.InverseTransformDirection(dst_tfm.up * sign_dst) * signI;
		var new_rotation = Quaternion.LookRotation(Vector3.forward, new_direction);
		
		var new_scale_x = CalcOtherDropSize(dst_end) / endI.dropSize;
		var new_scale = new Vector3(new_scale_x, endI.tfm.localScale.y, endI.tfm.localScale.z);
		
		if (new_position != endI.tfm.localPosition) endI.tfm.localPosition = new_position;
		if (new_rotation != endI.tfm.localRotation) endI.tfm.localRotation = new_rotation;
		if (new_scale != endI.tfm.localScale) endI.tfm.localScale = new_scale;
	}
	
	float CalcOtherDropSize(PipeEnd dst_end) {
		var dropLB = dst_end.tfm.TransformPoint(Vector3.left * dst_end.dropSize * 0.5f);
		var dropRB = dst_end.tfm.TransformPoint(Vector3.right * dst_end.dropSize * 0.5f);
		var dropL = transform.InverseTransformPoint(dropLB);
		var dropR = transform.InverseTransformPoint(dropRB);
		return (dropR - dropL).magnitude;
	}
	
	#if UNITY_EDITOR
	void Update() {
		if (!Application.isPlaying) {
			if (isBallon) {
			} else {
				UpdateEnd(end0);
				UpdateEnd(end1);
			}
			return;
		}
	}
	#endif
	
	public void Build(double spatial_resolution, ref double accum_dist, ref Vector3 accum_last_p) {
		DetectDirection();
		Initialize();
		CreatePathPoints(spatial_resolution, ref accum_dist, ref accum_last_p);
		CreateWaterDrops(spatial_resolution);
	}
	
	void DetectDirection() {
		if (flow_direction != 0) return;
		if ((end0.pipe != null) && (end0.pipe.flow_direction != 0)) {
			flow_direction = 1;
		} else if ((end1.pipe != null) && (end1.pipe.flow_direction != 0)) {
			flow_direction = -1;
		} else if (hangingEndID != -1) {
			flow_direction = ((hangingEndID == 0) ? 1 : -1);
		}
	}
	
	bool AddDropData(List<Vector3> drops_data_ext, PipeEnd endI) {
		if (endI.pipe == null) return false;
		if (endI.pipe.isBallon) return false;
		var dst_end = endI.GetEnd();
		var p = dst_end.tfm.position;
		p = transform.InverseTransformPoint(p);
		p.z = CalcOtherDropSize(dst_end)*0.5f; // radius = size/2
		drops_data_ext.Add(p);
		return true;
	}
	
	public void Initialize() {
		if (isBallon) {
			var drops_data_ext = new List<Vector3>();
			exclude_first_point = AddDropData(drops_data_ext, end0);
			foreach (var p in drops_data) {
				drops_data_ext.Add(p);
			}
			exclude_last_point = AddDropData(drops_data_ext, end1);
			drops_data = drops_data_ext.ToArray();
		} else {
			var skin = this.GetComponent<SkinnedMeshRenderer>();
			var skin_mesh = skin.sharedMesh;
			var skin_verts = MeshUtils.SkinnedVertices(skin, true);
			
			int VertsPerSegment = 7;
			int nSegments = skin_verts.Length/VertsPerSegment;
			drops_data = new Vector3[nSegments];
			
			final_distance = 0f;
			var distances = new float[skin_verts.Length];
			var pM0 = Vector3.zero;
			for (int i = 0; i < skin_verts.Length; i += VertsPerSegment) {
				var pL = skin_verts[i];
				var pR = skin_verts[i+(VertsPerSegment-1)];
				var pM = (pL + pR)*0.5f;
				if (i > 0) {
					var distance = (pM - pM0).magnitude;
					final_distance += distance;
				}
				for (int j = 0; j < VertsPerSegment; j++) {
					distances[i+j] = final_distance;
				}
				var drop_data = transform.InverseTransformPoint(pM);
				drop_data.z = (transform.InverseTransformPoint(pL) - pM).magnitude;
				drops_data[i/VertsPerSegment] = drop_data;
				pM0 = pM;
			}
			
			var skin_colors = skin_mesh.colors;
			var colors = new Color[skin_colors.Length];
			for (int i = 0; i < distances.Length; i++) {
				float rel_distance = distances[i]/final_distance;
				if (flow_direction < 0) rel_distance = 1f - rel_distance;
				var rgba = (Color)Bini.Utils.MathUtils.EncodeFloatRGBA(rel_distance);
				rgba.a = skin_colors[i].a;
				colors[i] = rgba;
			}
			
			var mesh = MeshUtils.CopyMesh(skin_mesh);
			mesh.colors = colors;
			
			var mat = new Material(skin.sharedMaterial);
			mat.SetFloat("_InflationScale", InflationScale);
			mat.SetFloat("_InflateEnd0", flowEnd0.inflation);
			mat.SetFloat("_InflateEnd1", flowEnd1.inflation);
			
			if (BakeVertices) {
				var matrix = transform.worldToLocalMatrix;
				for (int i = 0; i < skin_verts.Length; i++) {
					skin_verts[i] = matrix.MultiplyPoint3x4(skin_verts[i]);
				}
				mesh.vertices = skin_verts;
				mesh.RecalculateBounds();
				
				UnityEngine.Object.Destroy(skin);
				
				var mesh_filter = this.gameObject.AddComponent<MeshFilter>();
				mesh_filter.sharedMesh = mesh;
				var mesh_renderer = this.gameObject.AddComponent<MeshRenderer>();
				mesh_renderer.sharedMaterial = mat;
			} else {
				skin.sharedMesh = mesh;
				skin.sharedMaterial = mat;
			}
			
			ActiveMaterial = mat;
		}
	}
	
	public void CreatePathPoints(double spatial_resolution, ref double accum_dist, ref Vector3 accum_last_p) {
		var tfm = transform;
		var keypoints = drops_data;
		if (keypoints.Length == 0) return;
		
		path_points = new List<Vector4>();
		
		var splin = new SplineInterpolator(isLinear ? SplineType.Linear : SplineType.Hermite);
		
		bool first_point = (accum_dist <= float.Epsilon);
		
		int iStart = 0, iEnd = keypoints.Length-1, iAdd = 1;
		int di0 = -1, di1 = 0, di2 = 1, di3 = 2;
		if (flow_direction < 0) {
			iEnd = 0; iStart = keypoints.Length-1; iAdd = -1;
			di0 = 1; di1 = 0; di2 = -1; di3 = -2;
		}
		
		// first/last points are marked as excluded if they were added for ballon<->pipe connection
		// (ballon<->pipe segments are invisible but their length must be taken into account
		// to avoid the "teleportation" of water between pipe end and ballon end)
		int iFirst = (exclude_first_point ? iStart+iAdd : iStart);
		int iLast = (exclude_last_point ? iEnd-iAdd : iEnd);
		
		// fallback, just in case
		ManualSegmentStart = (float)(accum_dist);
		ManualSegmentFinish = (float)(accum_dist + spatial_resolution*0.25f);
		
		var kp_indices = new List<int>(); // keypoint indices in path_points
		kp_indices.Add(0);

		Vector3 p0 = Vector3.zero, p0_abs = Vector3.zero;
		Vector3 p1 = Vector3.zero, p1_abs = Vector3.zero;
		Vector3 p2 = Vector3.zero, p2_abs = Vector3.zero;
		Vector3 p3 = Vector3.zero, p3_abs = Vector3.zero;

		int imax = keypoints.Length-1;
		for (int i = iStart; i != iEnd; i += iAdd) {
			if (i == iStart) {
				p0 = keypoints[Mathf.Clamp(i+di0, 0, imax)];
				p1 = keypoints[Mathf.Clamp(i+di1, 0, imax)];
				p2 = keypoints[Mathf.Clamp(i+di2, 0, imax)];
				p3 = keypoints[Mathf.Clamp(i+di3, 0, imax)];
				p0_abs = tfm.TransformPoint(p0.To2D().To3D());
				p1_abs = tfm.TransformPoint(p1.To2D().To3D());
				p2_abs = tfm.TransformPoint(p2.To2D().To3D());
				p3_abs = tfm.TransformPoint(p3.To2D().To3D());
			} else {
				p0 = p1;
				p1 = p2;
				p2 = p3;
				p3 = keypoints[Mathf.Clamp(i+di3, 0, imax)];
				p0_abs = p1_abs;
				p1_abs = p2_abs;
				p2_abs = p3_abs;
				p3_abs = tfm.TransformPoint(p3.To2D().To3D());
			}

			if (i == iFirst) ManualSegmentStart = (float)accum_dist;
			if (i == iLast) ManualSegmentFinish = (float)accum_dist;
			
			float len_estimate = (p1_abs - p0_abs).magnitude + (p2_abs - p1_abs).magnitude + (p3_abs - p2_abs).magnitude;
			int npoints = Mathf.CeilToInt(len_estimate / (float)(spatial_resolution*0.95));
			npoints = Mathf.Max(npoints, 4+1);
			int si_last = (i == iLast ? 1 : 0);
			for (int si = 0; si < npoints+si_last; si++) {
				float t = si / (float)npoints;
				var q = splin[t];
				
				var sp = p0*q.x + p1*q.y + p2*q.z + p3*q.w;
				var sp_abs = p0_abs*q.x + p1_abs*q.y + p2_abs*q.z + p3_abs*q.w;
				
				if (first_point) {
					accum_last_p = sp_abs;
					first_point = false;
				}
				
				accum_dist += ((Vector2)sp_abs - (Vector2)accum_last_p).magnitude;
				path_points.Add(new Vector4(sp.x, sp.y, sp.z, (float)accum_dist));
				accum_last_p = sp_abs;
			}
			
			kp_indices.Add(path_points.Count-1);
		}

		if (isBallon) {
			simplified_points = new List<Vector4>();
			float epsilon = ((float)spatial_resolution) * 0.5f;
			// We cannot simplify over keypoint boundaries, because keypoints might have different waterdrop radius
			for (int kpi = 0; kpi < kp_indices.Count-1; kpi++) {
				SimplifyPath(path_points, simplified_points, epsilon, kp_indices[kpi], kp_indices[kpi+1]);
			}
		}
	}
	
	static void SimplifyPath(List<Vector4> points, List<Vector4> kept_points, float epsilon, int i0=0, int i1=-1) {
		if (i1 < 0) i1 = points.Count-1;
		// Ramer-Douglas-Peucker algorithm
		float dmax = 0;
		int index = -1;
		var p0 = points[i0].To3D();
		p0.z = 0;
		var p1 = points[i1].To3D();
		p1.z = 0;
		var n = (p1 - p0).normalized;
		for (int i = i0+1; i < i1; i++) {
			var p = points[i].To3D();
			p.z = 0;
			// http://onlinemschool.com/math/library/analytic_geometry/p_line/
			var d = Vector3.Cross(p - p0, n).magnitude; // point->line distance
			if (d > dmax) {
				index = i;
				dmax = d;
			}
		}
		// If max distance is greater than epsilon, recursively simplify
		if ((dmax > epsilon) && (index != -1)) {
			SimplifyPath(points, kept_points, epsilon, i0, index);
			SimplifyPath(points, kept_points, epsilon, index, i1);
		} else {
			if (kept_points.Count == 0) kept_points.Add(points[i0]);
			kept_points.Add(points[i1]);
		}
	}

	public void CreateWaterDrops(double spatial_resolution) {
		if (isBallon) {
			water_drops_obj = new GameObject("water_drops");
			
			var water_drops_tfm = water_drops_obj.transform;
			water_drops_tfm.ResetToParent(transform);
			water_drops_tfm.localPosition += Vector3.forward * WaterZOffset;
			
			var mesh_filter = water_drops_obj.EnsureComponent<MeshFilter>();
			var mesh_renderer = water_drops_obj.EnsureComponent<MeshRenderer>();
			
			var vertices = new List<Vector3>();
			var uv = new List<Vector2>();
			var colors = new List<Color>();
			var triangles = new List<int>();
			
			float progress_min = ProgressMin;
			float progress_max = ProgressMax;
			float progress_dInv = (progress_max - progress_min);
			progress_dInv = ((progress_dInv > Mathf.Epsilon) ? 1f/progress_dInv : 1f);
			
			var points = simplified_points;

			var drop_points = new List<Vector4>();

			final_distance = 0f;
			int imax = points.Count-1;
			var pM0 = Vector3.zero;
			for (int i = 0; i < imax; i++) {
				int i_0 = i, i_1 = i+1;
				var p_0 = points[i_0];
				var p_1 = points[i_1];

				float drop_radius = Mathf.Min(Mathf.Abs(p_0.z), Mathf.Abs(p_1.z)); // abs() just in case
				float step_size = Mathf.Max(drop_radius*0.2f, (float)spatial_resolution); // minimal drop_radius can be 0!
				int subdivs = Mathf.CeilToInt((p_1 - p_0).To2D().magnitude / step_size);
				subdivs = Mathf.Max(subdivs, 1);
				int jmax = (i_1 < imax ? subdivs-1 : subdivs);
				for (int j = 0; j <= jmax; j++) {
					float t = j / (float)subdivs;
					var p = Vector4.Lerp(p_0, p_1, t);
					
					var pM = transform.TransformPoint(new Vector3(p.x, p.y, 0));
					if (drop_points.Count > 0) final_distance += (pM - pM0).magnitude;

					p.w = final_distance;
					drop_points.Add(p);

					pM0 = pM;
				}
			}

			float final_distance_inv = (final_distance > float.Epsilon ? 1f / final_distance : 1f);
			for (int i = 0; i < drop_points.Count; i++) {
				var p = drop_points[i];
				float L = p.w * final_distance_inv;

				int i0 = vertices.Count;
				var p0 = new Vector3(p.x - p.z, p.y - p.z, L);
				var p1 = new Vector3(p.x - p.z, p.y + p.z, L);
				var p2 = new Vector3(p.x + p.z, p.y + p.z, L);
				var p3 = new Vector3(p.x + p.z, p.y - p.z, L);
				var t0 = new Vector2(0, 0);
				var t1 = new Vector2(0, 1);
				var t2 = new Vector2(1, 1);
				var t3 = new Vector2(1, 0);
				var w = new Color(Random.Range(0f, 1f), Random.Range(0f, 1f),
				                  Random.Range(0f, 1f), Random.Range(0f, 1f));
				vertices.Add(p0); vertices.Add(p1); vertices.Add(p2); vertices.Add(p3);
				uv.Add(t0); uv.Add(t1); uv.Add(t2); uv.Add(t3);
				colors.Add(w); colors.Add(w); colors.Add(w); colors.Add(w);
				triangles.Add(i0); triangles.Add(i0+1); triangles.Add(i0+2);
				triangles.Add(i0); triangles.Add(i0+2); triangles.Add(i0+3);
			}

			var mesh = new Mesh();
			mesh.vertices = vertices.ToArray();
			mesh.uv = uv.ToArray();
			mesh.colors = colors.ToArray();
			mesh.triangles = triangles.ToArray();
			mesh.RecalculateBounds();
			
			mesh_filter.sharedMesh = mesh;
			
			var material = new Material(WaterMaterial);
			
			mesh_renderer.sharedMaterial = material;
			
			ActiveMaterial = material;
		} else {
		}
	}
	
	Material ActiveMaterial = null;
	public void SetWaterParams(Texture water_frame, float scale, Color color) {
		var mat = ActiveMaterial;
		if (mat == null) return;
		mat.SetTexture("_WaterTex", water_frame);
		mat.SetTextureScale("_WaterTex", Vector2.one * (1f/scale));
		mat.color = color;
	}
	public void SetAbsoluteProgress(float progress, float InflationLength) {
		var mat = ActiveMaterial;
		if (mat == null) return;
		float progress_min = ProgressMin;
		float progress_max = ProgressMax;
		float progress_dInv = (progress_max - progress_min);
		progress_dInv = ((progress_dInv > Mathf.Epsilon) ? 1f/progress_dInv : 1f);
		float progress_rel = (progress - progress_min) * progress_dInv;
		float fill_fade = InflationLength / final_distance;
		mat.SetFloat("_FillAmount", progress_rel);
		mat.SetFloat("_FillFade", fill_fade);
		if ((progress_rel <= 0) || (progress_rel >= 1f+fill_fade)) {
			mat.DisableKeyword("PARTIALLY_FILLED");
			mat.EnableKeyword("NOT_PARTIALLY_FILLED");
		} else {
			mat.EnableKeyword("PARTIALLY_FILLED");
			mat.DisableKeyword("NOT_PARTIALLY_FILLED");
		}
	}
	public void SetPipeColorization(float colorization) {
		if (isBallon) return;
		var mat = ActiveMaterial;
		if (mat == null) return;
		mat.SetFloat("_Colorization", colorization);
	}
	public void SetWave(float ampl, float phase) {
		if (!isBallon) return;
		var mat = ActiveMaterial;
		if (mat == null) return;
		mat.SetFloat("_WaveAmpl", ampl);
		mat.SetFloat("_WavePhase", phase);
		if (ampl <= 0f) {
			mat.DisableKeyword("WAVE_ON");
			mat.EnableKeyword("WAVE_OFF");
		} else {
			mat.EnableKeyword("WAVE_ON");
			mat.DisableKeyword("WAVE_OFF");
		}
	}
}
