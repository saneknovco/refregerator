﻿#if UNITY_EDITOR
using UnityEditor;
#endif
using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using Bini.Utils;

public class PipeCreator : MonoBehaviour {
	public float Thickness = 10f;
	public float Inflation = 0f;
	public bool isBallon = false;
	public bool isLinear = false;

	public struct Keypoint {
		public Transform child;

		public bool isFirst, isLast, isBone;

		public Vector3 pL, pR;
		public float kL, kR;

		public float dropScale, dropScaleK, dropSize;
		public Vector3 dropDir;

		public float distance;
		
		public float weight;
	}

	public List<Keypoint> GatherKeypoints() {
		float half_thickness = Thickness*0.5f;

		var tfm = transform;

		var keypoints = new List<Keypoint>();

		for (int i = 0; i < tfm.childCount; i++) {
			var child = tfm.GetChild(i);
			if (!child.name.StartsWith("_")) continue;
			var keypoint = new Keypoint();
			keypoint.child = child;
			keypoints.Add(keypoint);
		}

		float distance = 0f;
		for (int i = 0; i < keypoints.Count; i++) {
			var keypoint = keypoints[i];
			var child = keypoint.child;

			var pL = tfm.InverseTransformPoint(child.TransformPoint(Vector3.left*half_thickness));
			var pR = tfm.InverseTransformPoint(child.TransformPoint(Vector3.right*half_thickness));

			// Must be sufficiently greater than zero, or normal will be normalized to Infinity
			float local_inflation = Mathf.Max(Inflation * child.localScale.z, 0.01f);

			var pM = (pL + pR) * 0.5f;
			var dL = pL - pM;
			var dR = pR - pM;
			dL = dL.normalized * (dL.magnitude + local_inflation);
			dR = dR.normalized * (dR.magnitude + local_inflation);
			pL = pM + dL;
			pR = pM + dR;

			var kL = local_inflation / dL.magnitude;
			var kR = local_inflation / dR.magnitude;
			
			bool isFirst = (i == 0);
			bool isLast = (i == keypoints.Count-1);

			keypoint.isFirst = isFirst;
			keypoint.isLast = isLast;
			keypoint.isBone = ((isFirst || isLast) || child.name.EndsWith("!"));

			keypoint.pL = pL;
			keypoint.pR = pR;
			keypoint.kL = kL;
			keypoint.kR = kR;

			keypoint.dropScale = Mathf.Clamp01(child.localScale.y);
			keypoint.dropScaleK = keypoint.dropScale * (((1f - kL) + (1f - kR))*0.5f);
			keypoint.dropSize = (dL.magnitude + dR.magnitude) * keypoint.dropScaleK;

			var kp_prev = keypoints[Mathf.Max(i-1, 0)];
			var kp_curr = keypoint;
			var kp_next = keypoints[Mathf.Min(i+1, keypoints.Count-1)];
			var delta_prev = kp_prev.child.localPosition - kp_curr.child.localPosition;
			var delta_next = kp_next.child.localPosition - kp_curr.child.localPosition;

			if (keypoints.Count > 1) {
				if (isBallon) {
					keypoint.dropDir = delta_next - delta_prev;
					var dirNorm = Vector3.Cross(keypoint.dropDir, Vector3.forward).normalized;
					keypoint.pL = pM + dirNorm * dL.magnitude;
					keypoint.pR = pM - dirNorm * dR.magnitude;
				} else {
					if (isFirst) {
						keypoint.dropDir = Vector3.Cross(Vector3.Cross(delta_next, dR), dR).normalized;
					} else if (isLast) {
						keypoint.dropDir = Vector3.Cross(Vector3.Cross(delta_prev, dR), dR).normalized;
					} else {
						keypoint.dropDir = Vector3.zero;
					}
				}
			}

			distance += delta_prev.magnitude;
			keypoint.distance = distance;
			
			var repl = child.name.Replace("_", "").Replace("!", "").Replace(" ", "");
			float weight;
			if (float.TryParse(repl, out weight)) {
				if (weight < 0f) weight = 1f + weight;
				keypoint.weight = Mathf.Clamp01(weight);
			} else {
				keypoint.weight = -1f;
			}

			keypoints[i] = keypoint;
		}

		return keypoints;
	}

#if UNITY_EDITOR
	SplineInterpolator splin;
	void DrawGizmos_Ballon() {
		if (splin == null) splin = new SplineInterpolator(isLinear ? SplineType.Linear : SplineType.Hermite);

		var tfm = transform;
		var keypoints = GatherKeypoints();
		int imax = keypoints.Count-1;
		for (int i = 0; i < keypoints.Count; i++) {
			var keypoint = keypoints[i];

			var pL = keypoint.pL;
			var pR = keypoint.pR;
			var radius = (pR - pL).magnitude * 0.5f;

			if (!keypoint.isFirst) {
				Gizmos.color = new Color(0, 0, 0, 1);
				var p0 = keypoints[Mathf.Max(i-2, 0)].child.position;
				var p1 = keypoints[Mathf.Max(i-1, 0)].child.position;
				var p2 = keypoint.child.position;
				var p3 = keypoints[Mathf.Min(i+1, imax)].child.position;
				var sp0 = Vector3.zero;
				for (int si = 0; si <= 16; si++) {
					float t = si/16f;
					var q = splin[t];
					var sp = p0*q.x + p1*q.y + p2*q.z + p3*q.w;
					if (si > 0) Gizmos.DrawLine(sp, sp0);
					sp0 = sp;
				}
			}

			Gizmos.color = new Color(0, 0, 0, 1);
			var pM = (pL + pR) * 0.5f;
			Vector3 cp0 = Vector3.zero;
			for (int ci = 0; ci <= 16; ci++) {
				float ca = (ci / 16f) * (Mathf.PI*2);
				float cx = Mathf.Cos(ca) * radius;
				float cy = Mathf.Sin(ca) * radius;
				Vector3 cp = pM + (Vector3.right * cx) + (Vector3.up * cy);
				cp = tfm.TransformPoint(cp);
				if (ci > 0) Gizmos.DrawLine(cp, cp0);
				cp0 = cp;
			}
		}
	}
	
	void DrawGizmos_Pipe() {
		var tfm = transform;
		Vector3 pL0 = Vector3.zero, pR0 = Vector3.zero;
		Vector3 pL0k = Vector3.zero, pR0k = Vector3.zero;
		Vector3 pL0d = Vector3.zero, pR0d = Vector3.zero;
		foreach (var keypoint in GatherKeypoints()) {
			var pL = tfm.TransformPoint(keypoint.pL);
			var pR = tfm.TransformPoint(keypoint.pR);
			var pM = (pL + pR) * 0.5f;
			var dL = pL - pM;
			var dR = pR - pM;
			var dLk = dL * (1f - keypoint.kL);
			var dRk = dR * (1f - keypoint.kR);
			var pLk = pM + dLk;
			var pRk = pM + dRk;
			var pLd = pM + dLk * keypoint.dropScale;
			var pRd = pM + dRk * keypoint.dropScale;

			if (keypoint.isBone) {
				Gizmos.color = new Color(0, 0, 1, 1);
			} else {
				Gizmos.color = new Color(0, 0, 1, 0.25f);
			}
			Gizmos.DrawLine(pL, pR);
			
			if (!keypoint.isFirst) {
				Gizmos.color = new Color(0, 0.5f, 0.25f, 1);
				Gizmos.DrawLine(pL0d, pLd);
				Gizmos.DrawLine(pR0d, pRd);
				Gizmos.color = new Color(0, 0, 1, 1);
				Gizmos.DrawLine(pL0, pL);
				Gizmos.DrawLine(pR0, pR);
				Gizmos.color = new Color(0, 0.5f, 1, 1);
				Gizmos.DrawLine(pL0k, pLk);
				Gizmos.DrawLine(pR0k, pRk);
			}

			if (keypoint.isFirst || keypoint.isLast) {
				Gizmos.color = new Color(0, 0, 1, 1);
				var dropDir = tfm.TransformDirection(keypoint.dropDir).normalized;
				var dropTangent = dRk.normalized;
				var dropLen = dRk.magnitude * keypoint.dropScale;
				Vector3 cp0 = Vector3.zero;
				for (int ci = 0; ci <= 8; ci++) {
					float ca = (ci / 8f) * Mathf.PI;
					float cx = Mathf.Cos(ca) * dropLen;
					float cy = Mathf.Sin(ca) * dropLen;
					Vector3 cp = pM + (dropTangent * cx) + (dropDir * cy);
					if (ci > 0) Gizmos.DrawLine(cp, cp0);
					cp0 = cp;
				}
			}

			pL0 = pL; pR0 = pR;
			pL0k = pLk; pR0k = pRk;
			pL0d = pLd; pR0d = pRd;
		}
	}

	void OnDrawGizmos() {
		if (!transform.IsSelectedHierarchy()) return;
		if (isBallon) {
			DrawGizmos_Ballon();
		} else {
			DrawGizmos_Pipe();
		}
	}
#endif
}
