﻿using UnityEngine;
using System.Collections;

[System.Serializable]
public class AnimClipItemData
{
    [FullInspector.Comment(FullInspector.CommentType.None, "Аудио клип")]
    [FullInspector.Margin(8)]
    public AudioClip audioClip;  

    [FullInspector.Comment(FullInspector.CommentType.None, "Аним клип")]
    [FullInspector.Margin(8)]
    public AnimationClip animClip;

    [FullInspector.Margin(12)]    
    public GameObject animSource;

    [FullInspector.Margin(16)]
    [FullInspector.Comment(FullInspector.CommentType.None, "Скорость проигывания клипа")]
    public float speed = 1.0f;

    [FullInspector.Comment(FullInspector.CommentType.None, "Колличество повторов клипа")]
    public int repeat = 1;

    [FullInspector.Comment(FullInspector.CommentType.None, "Громкость аудио клип")]
    [FullInspector.Margin(8)]
    public float volume = 1.0f;

    /// <summary>
    /// флаг игры клипа для повторов
    /// </summary>
    [System.NonSerialized]
    [HideInInspector]
    public bool playStart = false;
    /// <summary>
    /// счетчик повторов
    /// </summary>
    [System.NonSerialized]
    [HideInInspector]
    public int playCounter = 1;

    /// <summary>
    /// время прерывания
    /// </summary>
    [System.NonSerialized]
    [HideInInspector]
    public float timeTouch = 0.0f;

}
