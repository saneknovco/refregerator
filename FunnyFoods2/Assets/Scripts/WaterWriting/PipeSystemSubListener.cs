﻿using UnityEngine;
using System.Collections;

public class PipeSystemSubListener : PipeSystemFlowListener 
{
    private bool _autoDisable = true;
    public bool autoDisable
    {
        get { return _autoDisable; }
        set 
        {
            if (_autoDisable != value)
            {
                if (_fsmAutoEnable)
                {
                    _autoDestroy = false;
                    _autoDisable = false;
                }
                else
                {
                    _autoDisable = value;
                    if (_autoDisable) _autoDestroy = false;
                }
            }
        }
    }
    private bool _autoDestroy = false;
    public bool autoDestroy
    {
        get { return _autoDestroy; }
        set
        {
            if (_fsmAutoEnable)
            {
                _autoDestroy = false;
                _autoDisable = false;
            }
            else
            {
                if (_autoDestroy != value)
                {
                    _autoDestroy = value;
                    if (_autoDestroy) _autoDisable = false;
                }
            }
        }
    }

    private bool _fsmAutoEnable = false;
    [FullInspector.Margin(8)]
    public bool fsmAutoEnable
    {
        get { return _fsmAutoEnable; }
        set 
        {
            if (_fsmAutoEnable != value)
            {
                _fsmAutoEnable = value;
                if (_fsmAutoEnable) {
                    _autoDestroy = false;
                    _autoDisable = false;
                }
            }
        }
    }

    private PipeSystem _subSystem;
	
    // Use this for initialization
	void Start () {

        if(fsm == null) this.isDirectlyListner = true;
        _subSystem = GetComponent<PipeSystem>();
	}

    protected override void DoOutProgressEvent(PipeSystem.ProgressEvent evt)
    {
        if (_subSystem != null) {
            _subSystem.enabled = true;

            if (_fsmAutoEnable && fsm != null) {
                if (!fsm.gameObject.activeSelf) fsm.gameObject.SetActive(true);
                if(!fsm.enabled)fsm.enabled = true;            
            }
            else
            {
                if (_autoDisable) enabled = false;
                else if (_autoDestroy) Destroy(this);
            }
        }
    }	
	
}
