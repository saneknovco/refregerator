﻿using UnityEngine;
using System.Collections.Generic;
using Bini.Utils;

public class PipeSystem : MonoBehaviour {
    public static readonly string progressEventType = typeof(PipeSystem).Name + "_" + typeof(ProgressEvent).Name;

	public Pipe StartingPipe = null;
	public float PipeProgress = 0;

	public bool StopFlow = false;

	public bool AutoManualSegment = true;

	List<Pipe> pipes = new List<Pipe>();
	public float TotalLength { get; private set; }

	public class ProgressEvent {
		public string name;
		public float progress;
		public bool isOn, isOff;
		public override string ToString() {
			string arg1 = progress.ToString();
			string arg2 = name;
			if (isOn) arg2 = "<" + arg2;
			if (isOff) arg2 = arg2 + ">";
			return string.Format("({0} {1})", arg1, arg2);
		}
	}
	public List<ProgressEvent> ProgressEvents {get; private set;}
	public string[] ProgressEventsInfo;

	public bool SpatialResolutionInMm = true;
	public float SpatialResolution = 2f;
	public float ActualSpatialResolution {
		get {
			float spatial_resolution = SpatialResolution;
			if (SpatialResolutionInMm) spatial_resolution *= PlatformUtils.PixelsPerCm/10f;
			return spatial_resolution;
		}
	}

	public float InflationLength = 100f;
	public float InflationLengthBallon = 10f;

	public float ManualStartOffset = 10f;
	public float ManualFinishOffset = -10f;

	public float WaveAmpl = 1f;
	public float WaveFreq = 1f;

	public float FlowSpeed = 100f;
	public float InteractionRadius = 50f;
	public float LossRadiusScale = 1.5f;
	public float RetreatDelay = 1f;
	public float RetreatSpeed = 50f;

	public bool forceBrodcastFSM = true;

	bool ManualFlow = false;
	float ManualMin = -1f;
	float ManualMax = -1f;
	bool ManualHold = false;
	float ManualHoldTime = 0f;
	Vector2 PrevMousePos = Vector2.zero;
	bool MousePosInitialized = false;

	float PrevProgress = 0f;
	int NextEventID = 0;
	ProgressEvent GetProgressEvent(int i) {
		if ((i < 0) || (i >= ProgressEvents.Count)) return null;
		return ProgressEvents[i];
	}
	float PrevEventProgress {
		get {
			int i = Mathf.Clamp(NextEventID, 0, ProgressEvents.Count);
			if (i == 0) return -1f;
			return GetProgressEvent(i-1).progress;
		}
	}
	float NextEventProgress {
		get {
			int i = Mathf.Clamp(NextEventID, 0, ProgressEvents.Count);
			if (i == ProgressEvents.Count) return float.MaxValue;
			return GetProgressEvent(i).progress;
		}
	}

	#region Water texture
	public float PipeColorization = 0f;
	public Color WaterColorTint = Color.white;
	public float WaterTexScale = 20f;
	public float WaterTexFPS = 15f;
	public string WaterTexPath = "";
	public Material WaterMaterial = null;

	Texture2D[] water_frames;
	int water_frame_id = 0;
	float water_frame_next_time = -1;
	
	public Texture current_tex {
		get { return water_frames[water_frame_id]; }
	}

	void UpdateWaterFrames() {
		float WavePhase = ((Time.time * WaveFreq) % 1f) * (2f * Mathf.PI);
		foreach (var pipe in pipes) {
			pipe.SetWaterParams(current_tex, WaterTexScale, WaterColorTint);
			pipe.SetPipeColorization(PipeColorization);
			pipe.SetWave(WaveAmpl, WavePhase);
			pipe.SetAbsoluteProgress(PipeProgress, (pipe.isBallon ? InflationLengthBallon : InflationLength));
		}

		if (Time.time > water_frame_next_time) {
			water_frame_id = (water_frame_id + 1) % water_frames.Length;
			water_frame_next_time = Time.time + (1f/WaterTexFPS);
		}
	}
	
	static Texture2D[] LoadFrames(string path, bool compress=false) {
		var pic_objs = Resources.LoadAll(path, typeof(Texture2D));
		var frames = new Texture2D[pic_objs.Length];
		for (int i = 0; i < pic_objs.Length; i++) {
			frames[i] = pic_objs[i] as Texture2D;
		}
		return frames;
	}
	#endregion

	void ParseProgressEvents() {
		ProgressEvents = new List<ProgressEvent>();

		if (ProgressEventsInfo != null) {
			var split_chars = new char[]{' '};
			foreach (var info in ProgressEventsInfo) {
				var toks = info.Trim().Split(split_chars, System.StringSplitOptions.RemoveEmptyEntries);
				if (toks.Length < 2) continue;

				float progress;
				if (!float.TryParse(toks[0], out progress)) continue;

				string name = toks[1];
				if (string.IsNullOrEmpty(name)) continue;

				bool isOn = name.Contains("<");
				bool isOff = name.Contains(">");
				name = name.Replace("<", "").Replace(">", "");

				AddProgressEvent(progress, isOn, isOff, name);
			}
		}
	}

	void AddProgressEvent(float progress, bool isOn, bool isOff, string name=null) {
		if (progress < 0) return;
		if (string.IsNullOrEmpty(name)) name = null;
		if ((name == null) && (isOn == isOff)) return;
		var progress_event = new ProgressEvent();
		progress_event.name = name;
		progress_event.progress = progress;
		progress_event.isOn = isOn;
		progress_event.isOff = isOff;
		ProgressEvents.Add(progress_event);
	}

	void InitPipeLine() {
		if (StartingPipe == null) return;

		double accum_dist = 0;
		Vector3 accum_last_p = Vector3.zero;

		var pipe = StartingPipe;
		while (pipe != null) {
			pipes.Add(pipe);

			if (pipe.WaterMaterial == null) pipe.WaterMaterial = WaterMaterial;

			pipe.Build(ActualSpatialResolution, ref accum_dist, ref accum_last_p);

			if (pipe.isBallon && AutoManualSegment){//pipe.AutoManualSegment) {
				AddProgressEvent(pipe.ManualSegmentStart+pipe.ManualStartOffset+ManualStartOffset, false, true);
				AddProgressEvent(pipe.ManualSegmentFinish+pipe.ManualFinishOffset+ManualFinishOffset, true, false);
			}

			pipe = pipe.flowEnd1.pipe;
		}

		TotalLength = (float)accum_dist;

		ProgressEvents.Sort((a, b) => {return a.progress.CompareTo(b.progress);});
	}

	public Vector3 PointAtProgress(float progress) {
		int i = 0;
		return PointAtProgress(progress, ref i);
	}
	public Vector3 PointAtProgress(float progress, ref int i) {
		i = CollectionsUtils.IndexOfLocalMinimum(pipes, ((Pipe _pipe) => {
			float _minL = _pipe.ProgressMin;
			float _maxL = _pipe.ProgressMax;
			if (progress < _minL) return Mathf.Abs(progress - _minL);
			if (progress > _maxL) return Mathf.Abs(progress - _maxL);
			return 0f;
		}), i);

		var pipe = pipes[i];
		var minL = pipe.ProgressMin;
		var maxL = pipe.ProgressMax;
		var invDL = (maxL - minL);
		invDL = ((invDL > float.Epsilon) ? (1f / invDL) : 1f);
		
		int imax = pipe.path_points.Count-1;
		
		float t = Mathf.Clamp01((progress - minL) * invDL);
		int i_c = Mathf.RoundToInt(t*imax); // some initial estimate
		i_c = CollectionsUtils.IndexOfLocalMinimum(pipe.path_points, (pp => Mathf.Abs(progress-pp.w)), i_c);
		
		int i_0, i_1;
		var pc = pipe.path_points[i_c];
		if (pc.w <= progress) {
			i_0 = i_c;
			i_1 = Mathf.Min(i_c+1, imax);
		} else {
			i_0 = Mathf.Max(i_c-1, 0);
			i_1 = i_c;
		}
		
		var p0 = pipe.path_points[i_0];
		var p1 = pipe.path_points[i_1];
		
		minL = p0.w;
		maxL = p1.w;
		invDL = (maxL - minL);
		invDL = ((invDL > float.Epsilon) ? (1f / invDL) : 1f);
		t = (progress - minL) * invDL;
		
		var p = Vector4.Lerp(p0, p1, t);
		
		var pos = pipe.transform.TransformPoint(new Vector3(p.x, p.y));
		var radius = pipe.transform.TransformDirection(Vector3.right * p.z).magnitude;
		return new Vector3(pos.x, pos.y, radius);
	}

	float ClosestNextProgress(Vector2 mp, Vector2 pp, float progress, float step, ref int pipe_i, out bool moved) {
		float p_neg = Mathf.Clamp(progress-step, ManualMin, ManualMax);
		float p_pos = Mathf.Clamp(progress+step, ManualMin, ManualMax);

		var pp_neg = (Vector2)PointAtProgress(p_neg, ref pipe_i);
		var pp_pos = (Vector2)PointAtProgress(p_pos, ref pipe_i);

		float pp_dist = (mp - pp).sqrMagnitude;
		float pp_neg_dist = (mp - pp_neg).sqrMagnitude;
		float pp_pos_dist = (mp - pp_pos).sqrMagnitude;

		moved = true;
		if ((pp_neg_dist < pp_dist) && (pp_neg_dist <= pp_pos_dist)) {
			return p_neg;
		} else if ((pp_pos_dist < pp_dist) && (pp_pos_dist <= pp_neg_dist)) {
			return p_pos;
		}

		moved = false;
		return progress;
	}

	void UpdateFlow() {
		var mouse = MouseEmulator.Instance;
		var mp = (Vector2)(mouse.GetPos());
		var dm = mp - PrevMousePos;

		if (!StopFlow) {
			float loss_radius = LossRadiusScale * InteractionRadius;

			if (ManualFlow) {
				PipeProgress = Mathf.Clamp(PipeProgress, ManualMin, ManualMax);

				int pipe_i = 0;
				var pp = (Vector2)PointAtProgress(PipeProgress, ref pipe_i);

				if (mouse.GetButtonDown(0)) {
					if (!MousePosInitialized) {
						PrevMousePos = mp;
						dm = Vector2.zero;
						MousePosInitialized = true;
					}
					if ((mp - pp).magnitude <= InteractionRadius) {
						ManualHold = true;
					}
				} else if (mouse.GetButtonUp(0)) {
					ManualHold = false;
					MousePosInitialized = false;
				}

				if (ManualHold) {
					int n = Mathf.CeilToInt(dm.magnitude / ActualSpatialResolution);
					int imax = Mathf.Max(n-1, 1);
					for (int i = 0; i < n; i++) {
						float t = i / (float)imax;
						var mp_t = Vector2.Lerp(PrevMousePos, mp, t);
						//if ((mp_t - pp).magnitude > loss_radius) ManualHold = false;
						//if ((mp_t - pp).magnitude > InteractionRadius) break;

						bool moved = true;
						while (moved) {
							PipeProgress = ClosestNextProgress(mp_t, pp, PipeProgress, ActualSpatialResolution, ref pipe_i, out moved);
							if (moved) pp = (Vector2)PointAtProgress(PipeProgress, ref pipe_i);
							//if ((mp_t - pp).magnitude > loss_radius) ManualHold = false;
						}
					}

					if ((mp - pp).magnitude < loss_radius) {
						ManualHoldTime = Time.time;
					}
				}

				if (Time.time > ManualHoldTime+RetreatDelay) {
					ManualHold = false;
					PipeProgress -= Time.deltaTime * RetreatSpeed * Mathf.Sign(FlowSpeed);
				}

				PipeProgress = Mathf.Clamp(PipeProgress, ManualMin, ManualMax);

				// trigger switch to auto-flow on completion
				float margin = ActualSpatialResolution*0.5f;
				if (FlowSpeed > 0) {
					if (PipeProgress >= ManualMax-margin) {
						PipeProgress = ManualMax+margin;
					}
				} else {
					if (PipeProgress <= ManualMin+margin) {
						PipeProgress = ManualMin-margin;
					}
				}
			} else {
				PipeProgress += Time.deltaTime * FlowSpeed;
				MousePosInitialized = false;
			}

			PipeProgress = Mathf.Clamp(PipeProgress, 0, TotalLength+InflationLength);

			float delta_progress = PipeProgress - PrevProgress;

			while (true) {
				if (delta_progress > 0) {
					if (PipeProgress > NextEventProgress) {
						ProcessEvent(NextEventID, true);
						NextEventID = Mathf.Clamp(NextEventID+1, 0, ProgressEvents.Count);
					} else {
						break;
					}
				} else if (delta_progress < 0) {
					if (PipeProgress < PrevEventProgress) {
						NextEventID = Mathf.Clamp(NextEventID-1, 0, ProgressEvents.Count);
						ProcessEvent(NextEventID, false);
					} else {
						break;
					}
				} else {
					break;
				}
			}

			PrevProgress = PipeProgress;
		}

		PrevMousePos = mp;
	}

	ProgressEvent FindManualFlowEvent(int i, bool forward, bool expected_value) {
		int di = (forward ? 1 : -1);
		for (i = i+di; (i >= 0) && (i < ProgressEvents.Count); i += di) {
			var evt = ProgressEvents[i];
			if (evt.name == null) {
				bool manual_flow = !(forward ? evt.isOn : evt.isOff);
				if (manual_flow == expected_value) return evt;
			}
		}
		return null;
	}

	void ProcessEvent(int i, bool forward) {
		var evt = GetProgressEvent(i);
		if (evt.name == null) {
			bool manual_flow = !(forward ? evt.isOn : evt.isOff);
			var evt2 = FindManualFlowEvent(i, forward, !manual_flow);
			if ((evt2 != null) || !manual_flow) {
				ManualFlow = manual_flow;
				if (ManualFlow) {
					if (forward) {
						ManualMin = evt.progress;
						ManualMax = evt2.progress;
						PipeProgress = ManualMin;
					} else {
						ManualMin = evt2.progress;
						ManualMax = evt.progress;
						PipeProgress = ManualMax;
					}
					PipeProgress = Mathf.Clamp(PipeProgress, ManualMin, ManualMax);
					ManualHold = false;
					ManualHoldTime = Time.time;
				}
				//Debug.Log("ManualFlow: "+ManualFlow);
			}
		} else {
			string dir_name = (forward ? "Forward" : "Backward");
			//Debug.Log(dir_name+": "+evt + " off " + evt.isOff + " on " + evt.isOn);

            if (Messenger.eventTable.ContainsKey(PipeSystem.progressEventType)) {
                Messenger.Broadcast<ProgressEvent>(progressEventType, evt);
				if (forceBrodcastFSM) PlayMakerFSM.BroadcastEvent(evt.name);
			} else {
				PlayMakerFSM.BroadcastEvent(evt.name);
			}
		}
	}

	void Awake() {
		water_frames = LoadFrames(WaterTexPath);
		ParseProgressEvents();
	}

	void Start() {
		InitPipeLine();
		UpdateWaterFrames();
	}

	void Update() {
		UpdateFlow();
		UpdateWaterFrames();
	}
}
