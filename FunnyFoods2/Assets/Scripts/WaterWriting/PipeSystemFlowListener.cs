﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using HutongGames;
using HutongGames.PlayMaker;

/// <summary>
/// базовый класс слушатеь событий приходящих от PipeSystem(трубопровод)
/// </summary>
public class PipeSystemFlowListener :FullInspector.BaseBehavior
{   
    /// <summary>
    /// вспомогательный класс структуры данных для рвссылки событий FSM PlayMaker
    /// </summary>
    [System.Serializable]
    public class FSMGameObjectBridge
    {
        [FullInspector.Margin(12)]        
        public GameObject fsmOwner;
        public string fsmName = "FSM";
    }

    /// <summary>
    /// направление потока
    /// </summary>
    [System.Serializable]
    public enum FlowDirection
    {
        None     = 0,
        Backward = 1,
        Forward  = 2
    }

    [FullInspector.Margin(12)]
    [FullInspector.InspectorOrder(10)]
    [FullInspector.Comment(FullInspector.CommentType.None, " Монопольный режим для событий 'directly','out_','gl_fsm_'")]
    public bool isDirectlyListner = false;

    [FullInspector.InspectorOrder(20)]  
    [FullInspector.Comment(FullInspector.CommentType.Info, " PlayMakerFSM для которой посылается сообщение. Задается если FSM на другом объекте")]
    public PlayMakerFSM fsm;
    
    [FullInspector.InspectorOrder(21)]
    [FullInspector.InspectorDivider()]
    [FullInspector.Comment(FullInspector.CommentType.Info, " Дополнительный список Объектов с именами FSM для рассылки события")] 
    public List<FSMGameObjectBridge> fsmSubscribers;// { get; private set; }


    [FullInspector.InspectorOrder(22)]
    [FullInspector.Margin(16)]
    [FullInspector.Comment(FullInspector.CommentType.None, " Префикс для имен глобальных событий в системе PlayMaker")]
    public string prefixGlobalFsmEvent = "GL_PSF_";
    
    [FullInspector.InspectorOrder(22.1)]
    [FullInspector.Margin(16)]
    [FullInspector.Comment(FullInspector.CommentType.None, " Префикс для имен событий в системе PlayMaker")]    
    public string prefixFsmEvent = "PSF_";

    #region virtual methods for override custom action
    
    protected virtual void DoDirectlyProgressEvent() { }
    protected virtual void DoOutProgressEvent(PipeSystem.ProgressEvent evt) { }

    #endregion

    /// <summary>
    /// events dispatcher
    /// </summary>
    /// <param name="evt"></param>
    private void OnProgressEvent(PipeSystem.ProgressEvent evt)
    {
        // TO DO: Draft model

        if(evt.name == name ) {
            if (Debug.isDebugBuild) Debug.Log("PipeSystemFlowListner:OnProgressEvent: direct object");
            DoDirectlyProgressEvent();
            return;
        }

        if (evt.name.StartsWith("out_")) {
            if (Debug.isDebugBuild) Debug.Log("PipeSystemFlowListner:OnProgressEvent:  other system event");
            string evtName = evt.name.Substring(4);
            if(isDirectlyListner ) {
                if (name == evtName) DoOutProgressEvent(evt);
            }
            else DoOutProgressEvent(evt);
            return;
        }

        if (fsm != null && evt.name.StartsWith("gl_fsm_")) {
            if (Debug.isDebugBuild) Debug.Log("PipeSystemFlowListner:OnProgressEvent:fsm: global event");
            string fsmEvent = evt.name.Substring(7);
            if (isDirectlyListner && fsmEvent != name) return;

            fsmEvent = prefixGlobalFsmEvent + fsmEvent;
            if (!FsmEvent.IsEventGlobal(fsmEvent)) FsmEvent.GlobalsComponent.AddEvent(fsmEvent);

            fsm.Fsm.Event(fsmEvent);
            SendEventToSubscribers(fsmEvent);          
            return;
        }        

        if (isDirectlyListner) return;

        if (fsm != null && evt.name.StartsWith("fsm_")) {
            if (Debug.isDebugBuild) Debug.Log("PipeSystemFlowListner:OnProgressEvent:fsm: event");
            string fsmEvent = prefixFsmEvent + evt.name.Substring(4);

            if (fsm.enabled == false) fsm.enabled = true;//!!!

            //fsm.Fsm.Event(HutongGames.PlayMaker.FsmEventTarget.EventTarget.BroadcastAll.)
            //fsm.Fsm.Event(HutongGames.PlayMaker.FsmEventTarget.EventTarget.Self, fsmEvent);
            fsm.Fsm.Event(fsmEvent);
            SendEventToSubscribers(fsmEvent);           
            return;
        }
    }

    protected void SendEventToSubscribers(string fsmEvent)
    {
        if (fsmSubscribers != null)
        {
            foreach (var item in fsmSubscribers)
            {
                if (item.fsmOwner && !string.IsNullOrEmpty(item.fsmName))
                    fsm.Fsm.SendEventToFsmOnGameObject(item.fsmOwner, item.fsmName, fsmEvent);
            }
        }
    }

    #region UNITY_EVENT

    protected override void Awake()
    {
        base.Awake();

        if (fsm == null) fsm = GetComponent<PlayMakerFSM>();                        
    }

    protected virtual void OnEnable()
    {
        Messenger.AddListener<PipeSystem.ProgressEvent>(PipeSystem.progressEventType, OnProgressEvent);
    }

    protected virtual void OnDisable()
    {
        if (Messenger.eventTable.ContainsKey(PipeSystem.progressEventType))
            Messenger.RemoveListener<PipeSystem.ProgressEvent>(PipeSystem.progressEventType, OnProgressEvent);
    }

    protected virtual void OnDestroy()
    {
        if (Messenger.eventTable.ContainsKey(PipeSystem.progressEventType))
            Messenger.RemoveListener<PipeSystem.ProgressEvent>(PipeSystem.progressEventType,OnProgressEvent);
    }

    #endregion
}
