﻿using UnityEngine;
using System.Collections.Generic;
using System.Collections;

public class PipeSystemAnimListener : PipeSystemFlowListener {

    [FullInspector.InspectorOrder(1)]
    public bool foneAnimFuzzyLogic = true;

    [FullInspector.InspectorOrder(2)]
    [FullInspector.InspectorDivider()]
    [FullInspector.Margin(16)]    
    [FullInspector.Comment(FullInspector.CommentType.Info, "Фоновая анимация, запускаемая в зависимости от состояния потока")]
    public Dictionary<PipeSystemFlowListener.FlowDirection, AnimClipItemData> foneAnim = new Dictionary<PipeSystemFlowListener.FlowDirection, AnimClipItemData>();

    private bool _autoDisable = true;
    [FullInspector.InspectorOrder(5.0)]
    [FullInspector.Margin(12)]    
    public bool autoDisable
    {
        get { return _autoDisable; }
        set
        {
            if (_autoDisable != value)
            {
                if (_fsmAutoEnable)
                {
                    _autoDestroy = false;
                    _autoDisable = false;
                }
                else
                {
                    _autoDisable = value;
                    if (_autoDisable) _autoDestroy = false;
                }
            }
        }
    }
    private bool _autoDestroy = false;
    [FullInspector.InspectorOrder(5.1)]
    public bool autoDestroy
    {
        get { return _autoDestroy; }
        set
        {
            if (_fsmAutoEnable)
            {
                _autoDestroy = false;
                _autoDisable = false;
            }
            else
            {
                if (_autoDestroy != value)
                {
                    _autoDestroy = value;
                    if (_autoDestroy) _autoDisable = false;
                }
            }
        }
    }

    private bool _fsmAutoEnable = false;
    [FullInspector.InspectorOrder(5.2)]
    [FullInspector.Margin(8)]
    public bool fsmAutoEnable
    {
        get { return _fsmAutoEnable; }
        set
        {
            if (_fsmAutoEnable != value)
            {
                _fsmAutoEnable = value;
                if (_fsmAutoEnable)
                {
                    _autoDestroy = false;
                    _autoDisable = false;
                }
            }
        }
    }

    // Use this for initialization
    void Start()
    {
        if (fsm == null) this.isDirectlyListner = true;      
    }

    protected override void DoOutProgressEvent(PipeSystem.ProgressEvent evt)
    {
        FlowDirection dir = FlowDirection.None;
        if (evt.isOn) dir = FlowDirection.Forward;
        else if (evt.isOff) dir = FlowDirection.Backward;

        AnimClipItemData animData = null;
        if (foneAnim.ContainsKey(dir)) animData = foneAnim[dir];
        else if(foneAnimFuzzyLogic) {
            if (foneAnim.ContainsKey(FlowDirection.None)) animData = foneAnim[FlowDirection.None];
        }

        if(animData!=null)
        {
            if (animData.animSource == null) {
                Animation[] animations = MonoBehaviour.FindObjectsOfType(typeof(Animation)) as Animation[];
                if (animations != null) {
                    foreach (var item in animations) {
           
                        foreach (AnimationState animState in item) {                        
                            if (animData.animSource != null) break;
                            if ((animData.animClip != null) && (animData.animClip.name == animState.clip.name))
                                animData.animSource = item.gameObject;
                        }
                        if (animData.animSource != null) break;
                    }
                }
            }
            
            animData.animSource.GetComponent<Animation>()[animData.animClip.name].speed = animData.speed;
            animData.animSource.GetComponent<Animation>().Play(animData.animClip.name, PlayMode.StopSameLayer);
            if (animData.audioClip!=null) MgAudioHelper.Play(animData.audioClip, 
                animData.volume, (animData.animClip.wrapMode == WrapMode.Loop), animData.speed, (animData.animClip.wrapMode == WrapMode.Loop));
        }

        if (fsmAutoEnable && fsm != null)
        {
            if (!fsm.gameObject.activeSelf) fsm.gameObject.SetActive(true);
            if (!fsm.enabled) fsm.enabled = true;
        }
        else
        {
            if (_autoDisable) enabled = false;
            else if (_autoDestroy) Destroy(this);
        }
    }	
	
	
}
