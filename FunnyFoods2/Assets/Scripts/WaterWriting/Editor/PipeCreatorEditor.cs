﻿using UnityEngine;
using UnityEditor;
using System.Collections;
using System.Collections.Generic;
using Bini.Utils;

[CustomEditor(typeof(PipeCreator))]
public class PipeCreatorEditor : Editor {
	PipeCreator pipe_creator;
	
	void OnEnable() {
		pipe_creator = target as PipeCreator;
	}
	
	public override void OnInspectorGUI() {
		base.OnInspectorGUI();

		if (GUILayout.Button("Build")) {
			Build();
		}
	}

	void Build() {
		var pipe = pipe_creator.gameObject.GetComponentInChildren<Pipe>();
		if (pipe == null) {
			var pipe_obj = new GameObject(pipe_creator.name);
			pipe = pipe_obj.AddComponent<Pipe>();
		} else {
			// if object is a prefab, any modifications made in script are ignored!
			PrefabUtility.DisconnectPrefabInstance(pipe.gameObject);
			pipe.name = pipe_creator.name;
		}

		var pipe_creator_tfm = pipe_creator.transform;
		var pipe_tfm = pipe.transform;
		pipe_tfm.ResetToParent(pipe_creator_tfm);
		pipe_tfm.localPosition += Vector3.back;

		pipe.isBallon = pipe_creator.isBallon;
		pipe.isLinear = pipe_creator.isLinear;

		pipe.end0 = Pipe.PipeEnd.Reset(pipe.end0, 0);
		pipe.end1 = Pipe.PipeEnd.Reset(pipe.end1, 1);

		var src_material = pipe_creator.GetComponent<Renderer>().sharedMaterial;
		var src_mesh = pipe_creator.GetComponent<MeshFilter>().sharedMesh;
		var src_bounds = MeshUtils.CalculateBounds(src_mesh);
		var src_bounds_uv = MeshUtils.CalculateUVBounds(src_mesh);
		
		var keypoints = pipe_creator.GatherKeypoints();
		
		if (pipe.isBallon) {
			var drops_data = new Vector3[keypoints.Count];
			for (int i = 0; i < keypoints.Count; i++) {
				var keypoint = keypoints[i];
				var pL = keypoint.pL;
				var pR = keypoint.pR;
				var radius = (pR - pL).magnitude * 0.5f;
				var pM = (pL + pR) * 0.5f;
				var drop_data = new Vector3(pM.x, pM.y, radius);
				drops_data[i] = drop_data;
			}
			pipe.drops_data = drops_data;

			pipe.EnsureComponent<MeshFilter>().sharedMesh = src_mesh;
			pipe.EnsureComponent<MeshRenderer>().sharedMaterial = src_material;
		} else {
			pipe.drops_data = null;

			var kp_first = keypoints[0];
			var kp_last = keypoints[keypoints.Count-1];
			pipe.end0.dropScale = kp_first.dropScaleK;
			pipe.end0.dropSize = kp_first.dropSize;
			pipe.end1.dropScale = kp_last.dropScaleK;
			pipe.end1.dropSize = kp_last.dropSize;

			var bone_names = AssignWeights(keypoints);

			var vertices = new List<Vector3>();
			var uv = new List<Vector2>();
			var uv1 = new List<Vector2>();
			var normals = new List<Vector3>();
			var colors = new List<Color>();
			var triangles = new List<int>();
			var boneWeights = new List<BoneWeight>();
			var bindposes = new List<Matrix4x4>();
			var bones = new List<Transform>();

			foreach (var kv in bone_names) {
				var bone = pipe_tfm.Find(kv.Key);
				if (bone == null) {
					bone = (new GameObject(kv.Key)).transform;
					bone.parent = pipe_tfm;
				}

				var kp = keypoints[kv.Value];

				bone.position = kp.child.position;
				bone.rotation = kp.child.rotation;
				bone.localScale = Vector3.one;

				var bone_matrix = Matrix4x4.TRS(bone.localPosition, bone.localRotation, bone.localScale);
				bone_matrix = bone_matrix.inverse;

				bindposes.Add(bone_matrix);
				bones.Add(bone);
			}

			float[] interp = new float[]{0f, 0.2f, 0.4f, 0.5f, 0.6f, 0.8f, 1f};

			float max_distance = keypoints[keypoints.Count-1].distance;
			int bi = 0; // bone index
			for (int i = 0; i < keypoints.Count; i++) {
				var kp = keypoints[i];

				var vL = kp.pL;
				var vR = kp.pR;

				var uvL = InterpolateUV(src_bounds, src_bounds_uv, vL);
				var uvR = InterpolateUV(src_bounds, src_bounds_uv, vR);
				var uvM = (uvL + uvR) * 0.5f;

				var uvDL = -(uvL - uvM) * kp.kL;
				var uvDR = -(uvR - uvM) * kp.kR;

				var nL = ((Vector3)uvDL).normalized;
				var nR = ((Vector3)uvDR).normalized;

				// ratio = (inflated / uninflated)
				// which one is negative is related to coordinate system
				var hL = 1f/(1f-kp.kL);
				var hR = -1f/(1f-kp.kR);

				float drop_scale = Mathf.Max(kp.dropScale, 0.001f);
				var uv1L = new Vector2(uvDL.magnitude*Mathf.Sign(hL), (hL / drop_scale));
				var uv1R = new Vector2(uvDR.magnitude*Mathf.Sign(hR), (hR / drop_scale));

				float rel_distance = kp.distance/max_distance;

				var bw = new BoneWeight();
				bw.boneIndex0 = bi;
				bw.weight0 = 1f - kp.weight;
				bw.boneIndex1 = bi + 1;
				bw.weight1 = kp.weight;

				// the created pipe is in local system of the template
				vL = pipe_tfm.InverseTransformPoint(pipe_creator_tfm.TransformPoint(vL));
				vR = pipe_tfm.InverseTransformPoint(pipe_creator_tfm.TransformPoint(vR));

				float hL1 = Mathf.Sign(hL);
				float hR1 = Mathf.Sign(hR);
				float hLd = hL1 * kp.dropScale;
				float hRd = hR1 * kp.dropScale;

				float h_min = hL, h_delta = (hR - hL);
				interp[1] = (hL1 - h_min) / h_delta;
				interp[2] = (hLd - h_min) / h_delta;
				interp[3] = (0f - h_min) / h_delta;
				interp[4] = (hRd - h_min) / h_delta;
				interp[5] = (hR1 - h_min) / h_delta;

				for (int j = 0; j < interp.Length; j++) {
					float t = interp[j];
					vertices.Add(Vector3.Lerp(vL, vR, t));
					uv.Add(Vector2.Lerp(uvL, uvR, t));
					uv1.Add(Vector2.Lerp(uv1L, uv1R, t));
					normals.Add(Vector3.Lerp(nL, nR, t).normalized);
					var rgba = (Color)Bini.Utils.MathUtils.EncodeFloatRGBA(rel_distance);
					rgba.a = Mathf.Abs(Mathf.Lerp(kp.kL, kp.kR, t));
					colors.Add(rgba); // careful, it's packed
					boneWeights.Add(bw); // same (no need to interpolate)
				}

				if (i > 0) {
					int nverts = vertices.Count;
					int iv0_curr = nverts - interp.Length;
					int iv0_prev = iv0_curr - interp.Length;
					for (int j = 1; j < interp.Length; j++) {
						int iL1 = iv0_curr+j-1;
						int iR1 = iv0_curr+j;
						int iL0 = iv0_prev+j-1;
						int iR0 = iv0_prev+j;
						triangles.Add(iL0); triangles.Add(iL1); triangles.Add(iR0);
						triangles.Add(iR0); triangles.Add(iL1); triangles.Add(iR1);
					}
				}

				if (kp.isBone && !kp.isFirst) bi++;
			}

			var mainTex = src_material.mainTexture;

			var material = new Material(Shader.Find("WaterWriting/PipeShader"));
			material.mainTexture = mainTex;

			var mesh = new Mesh();
			mesh.vertices = vertices.ToArray();
			mesh.uv = uv.ToArray();
			mesh.uv2 = uv1.ToArray();
			mesh.normals = normals.ToArray();
			mesh.colors = colors.ToArray();
			mesh.triangles = triangles.ToArray();
			mesh.boneWeights = boneWeights.ToArray();
			mesh.bindposes = bindposes.ToArray();
			mesh.RecalculateBounds();

			string assets_dir = System.IO.Path.GetDirectoryName(FileUtils.GetAssetPath(mainTex));
			assets_dir = System.IO.Path.Combine(assets_dir, "PipeAssets");
			FileUtils.EnsureAssetDir(assets_dir);
			string material_path = System.IO.Path.Combine(assets_dir, mainTex.name+".mat");
			string mesh_path = System.IO.Path.Combine(assets_dir, mainTex.name+".asset");

			// Don't preserve links, because Unity seems to have bugs in
			// EditorUtility.CopySerialized (e.g. uv1 does not change)
			material = FileUtils.SaveAsset(material, material_path, false);
			mesh = FileUtils.SaveAsset(mesh, mesh_path, false);

			var skin = pipe.EnsureComponent<SkinnedMeshRenderer>();
			skin.bones = bones.ToArray();
			skin.sharedMesh = mesh;
			skin.sharedMaterial = material;

			pipe.end0.tfm = bones[0];
			pipe.end1.tfm = bones[bones.Count-1];
		}
	}

	Vector2 InterpolateUV(Bounds src_bounds, Rect src_bounds_uv, Vector3 vert) {
		float u = (vert.x - src_bounds.min.x) / src_bounds.size.x;
		float v = (vert.y - src_bounds.min.y) / src_bounds.size.y;
		u = src_bounds_uv.xMin + u * src_bounds_uv.width;
		v = src_bounds_uv.yMin + v * src_bounds_uv.height;
		return new Vector2(u, v);
	}

	Dictionary<string, int> AssignWeights(List<PipeCreator.Keypoint> keypoints) {
		var bone_names = new Dictionary<string, int>();

		float w0 = 0f;
		int i0 = 0;
		bool assign_first = true;
		for (int i = 0; i < keypoints.Count; i++) {
			var keypoint = keypoints[i];
			float w = keypoint.weight;
			if ((i > 0) && keypoint.isBone) w = 1f;
			if (w >= 0f) {
				InterpolateWeights(keypoints, i0, w0, i, w, assign_first);
				w0 = (keypoint.isBone ? 0f : w);
				i0 = i;
				assign_first = false;
			}
			if (keypoint.isFirst) {
				bone_names.Add("end0", i);
			} else if (keypoint.isLast) {
				bone_names.Add("end1", i);
			} else if (keypoint.isBone) {
				bone_names.Add("mid"+bone_names.Count, i);
			}
		}

		return bone_names;
	}

	void InterpolateWeights(List<PipeCreator.Keypoint> keypoints, int i0, float w0, int i1, float w1, bool assign_first) {
		var splin = new SplineInterpolator(pipe_creator.isLinear ? SplineType.Linear : SplineType.Hermite);
		float min_dist = keypoints[i0].distance;
		float max_dist = keypoints[i1].distance;
		for (int i = i0; i <= i1; i++) {
			if ((i == i0) && !assign_first) continue;
			var keypoint = keypoints[i];
			if (i0 != i1) {
				var dist = keypoint.distance;
				float t = (dist - min_dist) / (max_dist - min_dist);
				var q = splin[t];
				keypoint.weight = w0*(q.x + q.y) + w1*(q.z + q.w);
			} else {
				keypoint.weight = w0;
			}
			keypoints[i] = keypoint;
		}
	}
}
