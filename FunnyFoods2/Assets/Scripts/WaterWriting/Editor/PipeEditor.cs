﻿using UnityEngine;
using UnityEditor;
using System.Collections;
using System.Collections.Generic;
using Bini.Utils;

[CustomEditor(typeof(Pipe))]
public class PipeEditor : Editor {
	Pipe pipe;
	Pipe.PipeEnd end0, end1;

	void OnEnable() {
		pipe = target as Pipe;
		end0 = new Pipe.PipeEnd();
		end1 = new Pipe.PipeEnd();
	}

	/*
	[DrawGizmo(GizmoType.Active)]
	static void DrawCustomGizmo(Transform objectTransform, GizmoType gizmoType) {

	}

	void OnSceneGUI() {
		EditorGUI.BeginChangeCheck();
		
		if (EditorGUI.EndChangeCheck()) {
			EditorUtility.SetDirty(target);
		}
	}
	*/

	public override void OnInspectorGUI() {
		//base.OnInspectorGUI();

		if (pipe.isBallon) {
			OnInspectorGUI_Ballon();
		} else {
			OnInspectorGUI_Pipe();
		}

	}
	void OnInspectorGUI_Ballon() {
		EditorGUI.BeginChangeCheck();

		var AutoManualSegment = EditorGUILayout.Toggle("Auto Manual Segment", pipe.AutoManualSegment);
		var BallonStartOffset = EditorGUILayout.FloatField("Start Offset", pipe.ManualStartOffset);
		var BallonFinishOffset = EditorGUILayout.FloatField("Finish Offset", pipe.ManualFinishOffset);
		var WaterMaterial = EditorGUILayout.ObjectField("Water Material", pipe.WaterMaterial, typeof(Material), false) as Material;
		var WaterZOffset = EditorGUILayout.FloatField("Water Z Offset", pipe.WaterZOffset);

		EditPipeEnd(pipe.end0, end0);
		EditPipeEnd(pipe.end1, end1);
		
		if (EditorGUI.EndChangeCheck()) {
			Undo.RecordObject(target, "Edit Pipe");
			pipe.AutoManualSegment = AutoManualSegment;
			pipe.ManualStartOffset = BallonStartOffset;
			pipe.ManualFinishOffset = BallonFinishOffset;
			pipe.WaterMaterial = WaterMaterial;
			pipe.WaterZOffset = WaterZOffset;
			CopyPipeEnd(end0, pipe.end0);
			CopyPipeEnd(end1, pipe.end1);
			EditorUtility.SetDirty(target);
		}
	}
	void OnInspectorGUI_Pipe() {
		EditorGUI.BeginChangeCheck();

		bool BakeVertices = EditorGUILayout.Toggle("Bake Vertices", pipe.BakeVertices);
		float InflationScale = EditorGUILayout.FloatField("Inflation Scale", pipe.InflationScale);

		EditPipeEnd(pipe.end0, end0);
		EditPipeEnd(pipe.end1, end1);
		
		if (EditorGUI.EndChangeCheck()) {
			Undo.RecordObject(target, "Edit Pipe");
			pipe.BakeVertices = BakeVertices;
			pipe.InflationScale = InflationScale;
			CopyPipeEnd(end0, pipe.end0);
			CopyPipeEnd(end1, pipe.end1);
			EditorUtility.SetDirty(target);
		}
	}

	void EditPipeEnd(Pipe.PipeEnd src, Pipe.PipeEnd dst) {
		if (src == null) return;

		dst.pipe = EditorGUILayout.ObjectField("End "+src.id, src.pipe, typeof(Pipe), true) as Pipe;
		if (pipe.isBallon) {
			dst.free = src.free;
			dst.inflation = src.inflation;
		} else {
			dst.free = EditorGUILayout.Toggle("End "+src.id+" independent", src.free);
			dst.inflation = EditorGUILayout.FloatField("End "+src.id+" inflation", src.inflation);
		}

		if (dst.pipe == src.pipe) {
			dst.side = src.side;
		} else {
			PrefabUtility.DisconnectPrefabInstance(pipe.gameObject);
			pipe.Connect(src, dst.pipe, -1, false, true);
		}

		if (dst.free && !src.free) {
			pipe.ResetEndTransform(src.id);
		}
	}

	void CopyPipeEnd(Pipe.PipeEnd src, Pipe.PipeEnd dst) {
		dst.pipe = src.pipe;
		dst.side = src.side;
		dst.free = src.free;
		dst.inflation = src.inflation;
	}
}
