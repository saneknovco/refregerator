﻿using UnityEngine;
using UnityEditor;
using System.Collections;
using System.Collections.Generic;
using Bini.Utils;

[CustomEditor(typeof(PipeSystem))]
public class PipeSystemEditor : Editor {
	PipeSystem pipe_system;
	
	void OnEnable() {
		pipe_system = target as PipeSystem;
	}
	
	public override void OnInspectorGUI() {
		base.OnInspectorGUI();

		EditorGUILayout.BeginHorizontal();
		if (GUILayout.Button("Disconnect all")) Reconnect(false, false);
		if (GUILayout.Button("Connect closest")) Reconnect(true, true);
		if (GUILayout.Button("Connect 1<->0")) Reconnect(true, false);
		EditorGUILayout.EndHorizontal();
	}

	void Reconnect(bool connect, bool closest) {
		int child_count = pipe_system.transform.childCount;

		for (int i = 0; i < child_count; i++) {
			var child = pipe_system.transform.GetChild(i);
			var pipe = child.GetComponent<Pipe>();
			if (pipe == null) continue;

			pipe.DisconnectAll();
		}

		pipe_system.StartingPipe = null;

		if (!connect) return;

		Pipe pipe0 = null;
		for (int i = 0; i < child_count; i++) {
			var child = pipe_system.transform.GetChild(i);
			var pipe = child.GetComponent<Pipe>();
			if (pipe == null) continue;

			if (pipe0 != null) {
				if (closest) {
					pipe0.Connect(pipe);
				} else {
					pipe0.Connect(1, pipe, 0);
				}
			} else {
				pipe_system.StartingPipe = pipe;
			}

			pipe0 = pipe;
		}
	}
}
