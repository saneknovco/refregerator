﻿using UnityEngine;
using System.Collections;

public class RN_MoveToBySpeed : MonoBehaviour 
{
	public Vector3 destinationPosition;
	public float speed;
	public string finishEvent;
	private Vector3 direction;
	private Vector3 lastDirection, newDirection;
	private Component thisComponent;

	void Start () 
	{
        direction = (destinationPosition - transform.localPosition) / (destinationPosition - transform.localPosition).magnitude;
	}

	void Update () 
	{
        if (Time.deltaTime * speed < Vector3.Distance(destinationPosition, transform.localPosition))
			transform.localPosition += direction * speed * Time.deltaTime;
		else
		{
            transform.localPosition = destinationPosition;
            if (finishEvent != null)
                if (finishEvent.Length > 0)
                    PlayMakerFSM.BroadcastEvent(finishEvent);
			thisComponent = transform.GetComponent<RN_MoveToBySpeed>();
			DestroyObject(thisComponent);
		}
	}
}
