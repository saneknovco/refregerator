﻿using UnityEngine;
using System.Collections;

public class RM_MuteAndDestroy : MonoBehaviour {

	public float timeForMute = 0.1f;

	private AudioSource _as;
	private float _currentVolume;
	private float _delta;
	private float _timer;
	void Start () {
		_as = transform.GetComponent<AudioSource>();
		_currentVolume = _as.volume;
		_timer = 0;
	}

	void Update () {
		_timer += Time.deltaTime;
		if(_timer < timeForMute)
		{
			_as.volume -= Time.deltaTime * _currentVolume / timeForMute;
		}
		else
		{
			GameObject.Destroy(transform.gameObject);
		}
	}
}
