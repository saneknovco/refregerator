﻿using UnityEngine;
using System;
using System.Collections;

public class RZ_CheckRateWindowMode : MonoBehaviour 
{
	void Start()
	{
        //Debug.Log(PlayerPrefs.HasKey("rateWindowMode"));
		if (PlayerPrefs.HasKey ("rateWindowMode")) 
		{
            string needName = PlayerPrefs.GetString ("rateWindowMode");
			GameObject modeGO;
			for (int i = 0; i < transform.childCount; i++) 
			{
				modeGO = transform.GetChild (i).gameObject;
				if (modeGO.name == needName)
					modeGO.SetActive (true);
				else
					modeGO.SetActive (false);
			}
		}
	}
}