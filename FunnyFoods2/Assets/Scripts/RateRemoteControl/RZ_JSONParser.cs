﻿using UnityEngine;
using System.Collections.Generic;
public class RWSwithcerData
{
	public string bundleId = string.Empty;
	public bool showRateWindow = false;
	public string mode = string.Empty;
	public int loopCount = 1;
}

public class RZ_JSONParser
{
    public static RWSwithcerData JSONParse(string jsonString)
    {
        RWSwithcerData data = new RWSwithcerData();
        try
        {

            string lang = GetSystemLanguage();
            JSONObject jsonObj = new JSONObject(jsonString);
            if (jsonObj.HasField(@"BundleId")) data.bundleId = jsonObj[@"BundleId"].str;
            if (jsonObj.HasField(lang)) data.showRateWindow = jsonObj[lang].b;
            else if (jsonObj.HasField(@"default")) data.showRateWindow = jsonObj[@"default"].b;
            if (jsonObj.HasField(@"mode")) data.mode = jsonObj[@"mode"].str;
            if (jsonObj.HasField(@"loopCount")) data.loopCount = (int)jsonObj[@"loopCount"].i;


        }
        catch
        {
            return null;
        }
        return data;
    }
    private static string GetSystemLanguage()
    {
        MageAppUILang lang = MageAppUILang.none;
        switch (Application.systemLanguage)
        {
            case SystemLanguage.English: lang = MageAppUILang.en; break;
            case SystemLanguage.Russian: lang = MageAppUILang.ru; break;
            case SystemLanguage.Japanese: lang = MageAppUILang.jp; break;
            case SystemLanguage.French: lang = MageAppUILang.fr; break;
            case SystemLanguage.Spanish: lang = MageAppUILang.sp; break;
            case SystemLanguage.Italian: lang = MageAppUILang.it; break;
            case SystemLanguage.Portuguese: lang = MageAppUILang.pt; break;
            case SystemLanguage.German: lang = MageAppUILang.de; break;
            case SystemLanguage.Chinese: lang = MageAppUILang.cn; break;
            case SystemLanguage.Korean: lang = MageAppUILang.kr; break;
            case SystemLanguage.Turkish: lang = MageAppUILang.tr; break;
            case SystemLanguage.Ukrainian: lang = MageAppUILang.ru; break;
            case SystemLanguage.Belarusian: lang = MageAppUILang.ru; break;
            default: lang = MageAppUILang.en; break;
        }
        if (lang != MageAppUILang.none)
            return System.Enum.GetName(lang.GetType(), lang);
        return "default";
    }
}
