﻿using UnityEngine;
using System.Collections.Generic;

public class RZ_RateLoader : DLCLoader 
{
	private static bool __isLoading = false;
	internal static void Run(string uri)
	{      
		if (__isLoading) return;

		Dictionary<string, string> prms = new Dictionary<string, string>(1);
		prms.Add(@"RWSwitcher", uri);
		DLCLoader.RunLoader<RZ_RateLoader>(prms, AppConfig.timeWaitFinishing);  

		__isLoading = true;
	}

	protected override void DoAfterLoadingItem(string key, WWW www)
	{ 		   
		if (_isBreakPackRequest) return;
		try
		{		
			//  ПРОВЕРЯЕМ НА ОШИБКУ ЗАГРУЗКИ ЧЕРЕЗ WWW
			if( www == null || !string.IsNullOrEmpty(www.error)) {
				_isRetryLoad = true;
				return;
			}

			RWSwithcerData appDataNew = RZ_JSONParser.JSONParse(System.Text.UTF8Encoding.UTF8.GetString(www.bytes));
			if(appDataNew == null)
			{
				_isRetryLoad = true;
				return;
			}

			if(appDataNew.showRateWindow)
			{
				PlayerPrefs.SetInt("isEnableOpenRateWindow", 1);
				PlayerPrefs.SetString("rateWindowMode", appDataNew.mode);
				HutongGames.PlayMaker.FsmVariables.GlobalVariables.GetFsmInt("FF2/RWNeedGamesCounter").Value = appDataNew.loopCount;
			}
			else 
				PlayerPrefs.SetInt("isEnableOpenRateWindow", 0);
			         
		}
		catch
		{
			_isRetryLoad = true;
		}
	}

	protected override void DoAfterLoadingPackage ()
	{
		__isLoading = false;
	}
}
