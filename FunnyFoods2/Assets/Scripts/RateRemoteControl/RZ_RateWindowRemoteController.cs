﻿using UnityEngine;
using System.Collections;

public class RZ_RateWindowRemoteController : MonoBehaviour 
{
	void Awake ()
	{
		PlayerPrefs.SetInt("isEnableOpenRateWindow", 0);
		int isRateButtonWasPressed = PlayerPrefs.GetInt ("isRateButtonWasPressed", 0);
		Debug.Log ("isRateButtonWasPressed: " + isRateButtonWasPressed);
		if (isRateButtonWasPressed != 0) 
		{
			return;
		}
		#if UNITY_IOS
		RZ_RateLoader.Run (@"http://u.mage-app.com/App/IPhonePlayer/FunnyFoods2/RWSwitcher.json");
		#else
		RZ_RateLoader.Run (@"http://u.mage-app.com/App/Android/FunnyFoods2/RWSwitcher.json");
		#endif
	}

	void Start () 
	{
	
	}


}
