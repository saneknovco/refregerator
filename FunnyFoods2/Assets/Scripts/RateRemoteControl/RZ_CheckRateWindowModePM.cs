﻿using UnityEngine;
using System.Collections;
namespace HutongGames.PlayMaker.Actions
{
	public class RZ_CheckRateWindowModePM : FsmStateAction 
	{
		public FsmEvent safeEvent;
		public override void OnEnter ()
		{
			if (PlayerPrefs.HasKey ("rateWindowMode")) 
			{
				if (PlayerPrefs.GetString ("rateWindowMode") != "dangerous") 
				{
					if (!string.IsNullOrEmpty (safeEvent.Name))
						Fsm.Event (safeEvent);
				}
			}
			Finish ();
		}
	}
}