﻿using UnityEngine;
using System.Collections;
using HutongGames.PlayMaker;

public class RM_SwitchLangOnObject : MonoBehaviour 
{
	public bool isTakeFromAppLangVariable = false;

	GameObject FindChild(GameObject parent, string childName)
	{
		for (int i = 0; i < parent.transform.childCount; i++) 
		{
			if(parent.transform.GetChild(i).name.ToLower() == childName.ToLower()) 
			{
				return parent.transform.GetChild(i).gameObject;
			}
		}
		return null;
	}
	
	GameObject needLangObj;

	void Start () 
	{

		if(isTakeFromAppLangVariable){
			needLangObj = FindChild(transform.gameObject, FsmVariables.GlobalVariables.GetFsmString(@"FF2/GameLang").Value);
		}
		else{
			needLangObj = FindChild(transform.gameObject, AppState.Instance.CurrentLangStr);
		}
		if(needLangObj != null)
		{
			for (int i = 0; i < transform.childCount; i++) 
			{
				transform.GetChild(i).gameObject.SetActive(false);
			}
			needLangObj.SetActive(true);
		}
		Component thisComp = transform.GetComponent<RM_SwitchLangOnObject>();
		Object.Destroy(thisComp);
	}
}
