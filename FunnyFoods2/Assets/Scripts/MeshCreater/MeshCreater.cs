﻿using UnityEngine;
#if UNITY_EDITOR
using UnityEditor;
using System.Collections;
using System;
using System.IO;

public class MeshCreater : MonoBehaviour 
{
	static bool GetPNGTextureSize(string pathToPNG, out float width, out float height)
	{
		byte[] bytes = new byte[24];
		width = -1.0f;
		height = -1.0f;
		try
		{
			FileStream fs = new FileStream(pathToPNG, FileMode.Open, FileAccess.Read);
			fs.Read(bytes, 0, 24);

			byte[] png_signature = { 137, 80, 78, 71, 13, 10, 26, 10 };
			
			const int cMinDownloadedBytes = 23;
			byte[] buf = bytes;
			if (buf.Length > cMinDownloadedBytes)
			{
				for (int i = 0; i < png_signature.Length; i++)
				{
					if (buf[i] != png_signature[i])
					{
						Debug.LogWarning("Error! Texture os NOT png format!");
						return false;
					}
				}

				width = buf[16] << 24 | buf[17] << 16 | buf[18] << 8 | buf[19];
				height = buf[20] << 24 | buf[21] << 16 | buf[22] << 8 | buf[23];
				return true;
			}
			return false;
		}
		catch
		{
			return false;
		}
	}
	public void CreateMesh()
	{
		MeshRenderer renderer;
		if((renderer = transform.GetComponent<MeshRenderer>()) != null)
		{
			string pathToPNG = Application.dataPath + UnityEditor.AssetDatabase.GetAssetPath(renderer.sharedMaterial.mainTexture).Substring(6);
			float width;
			float height;
			if(GetPNGTextureSize(pathToPNG, out width, out height))
			{
				width /= 2f;
				height /= 2f;
				Mesh mesh = new Mesh();
				mesh.vertices = new Vector3[]
				{
					new Vector3(-width, -height, 0),
					new Vector3(-width, height, 0),
					new Vector3(width, height, 0),
					new Vector3(width, -height, 0)
				};
				mesh.uv = new Vector2[]
				{
					new Vector2(0, 0),
					new Vector2(0, 1),
					new Vector2(1, 1),
					new Vector2(1, 0)
				};
				mesh.triangles = new int[]{0, 1, 2, 0, 2, 3};
				mesh.name = transform.name + "_mesh";

				#if UNITY_EDITOR
				string path = UnityEditor.AssetDatabase.GetAssetPath(renderer.sharedMaterial);
				path = path.Substring(0, path.Length - renderer.sharedMaterial.name.Length-13);
				if(!UnityEditor.AssetDatabase.IsValidFolder(path + "Meshes"))
				{
					UnityEditor.AssetDatabase.CreateFolder(path.Substring(0,path.Length-1), "Meshes");
				}
				path += ("Meshes/" + transform.name + "_mesh");
				UnityEditor.AssetDatabase.CreateAsset(mesh, path + ".asset");
				UnityEditor.AssetDatabase.SaveAssets();
				UnityEditor.AssetDatabase.Refresh();
				#endif
				MeshFilter mf;
				mf = transform.gameObject.GetComponent<MeshFilter>() != null ? transform.gameObject.GetComponent<MeshFilter>() : transform.gameObject.AddComponent<MeshFilter>();
				mf.mesh = mesh;
			}
			else Debug.Log ("Error: Can`t acces to file on path:\n" + pathToPNG);
		}
		else Debug.Log ("Error: Game object: \"" + transform.name + "\" hasn`t the component Mesh Renderer!");
	}
	public static void CreateMesh(GameObject go)
	{
		MeshRenderer renderer;
		if((renderer = go.transform.GetComponent<MeshRenderer>()) != null)
		{
			string pathToPNG = Application.dataPath + UnityEditor.AssetDatabase.GetAssetPath(renderer.sharedMaterial.mainTexture).Substring(6);
			float width;
			float height;
			if(GetPNGTextureSize(pathToPNG, out width, out height))
			{
				width /= 2f;
				height /= 2f;
				Mesh mesh = new Mesh();
				mesh.vertices = new Vector3[]
				{
					new Vector3(-width, -height, 0),
					new Vector3(-width, height, 0),
					new Vector3(width, height, 0),
					new Vector3(width, -height, 0)
				};
				mesh.uv = new Vector2[]
				{
					new Vector2(0, 0),
					new Vector2(0, 1),
					new Vector2(1, 1),
					new Vector2(1, 0)
				};
				mesh.triangles = new int[]{0, 1, 2, 0, 2, 3};
				mesh.name = go.name + "_mesh";
				
				#if UNITY_EDITOR
				string path = UnityEditor.AssetDatabase.GetAssetPath(renderer.sharedMaterial);
				path = path.Substring(0, path.Length - renderer.sharedMaterial.name.Length-14);
				if(!UnityEditor.AssetDatabase.IsValidFolder(path + "Meshes"))
				{
					UnityEditor.AssetDatabase.CreateFolder(path.Substring(0,path.Length-1), "Meshes");
				}
				path += ("Meshes/" + go.name + "_mesh");
				UnityEditor.AssetDatabase.CreateAsset(mesh, path + ".asset");
				UnityEditor.AssetDatabase.SaveAssets();
				UnityEditor.AssetDatabase.Refresh();
				#endif
				MeshFilter mf;
				mf = go.GetComponent<MeshFilter>() != null ? go.GetComponent<MeshFilter>() : go.AddComponent<MeshFilter>();
				mf.mesh = mesh;
			}
			else Debug.Log ("Error: Can`t acces to file on path:\n" + pathToPNG);
		}		
		else Debug.Log ("Error: Game object: \"" + go.name + "\" hasn`t the component Mesh Renderer!");
	}
}
#endif
