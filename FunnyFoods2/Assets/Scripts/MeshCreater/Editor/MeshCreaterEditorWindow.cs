﻿using UnityEngine;
using UnityEditor;

public class MeshCreaterEditorWindow : EditorWindow 
{
	[MenuItem("Window/Mesh creater")]
	public static void ShowWindow()
	{
		GetWindow<MeshCreaterEditorWindow>("Mesh creater");
	}
	MeshCreater mc;
	void OnGUI()
	{		
		if (GUILayout.Button("Create meshes")) 
		{
			CreateMeshes();
		}
		
	}
	void CreateMeshes()
	{
		GameObject[] go = Selection.gameObjects;
		if(go != null)
		{
			if(go.Length > 0)
			{
				for (int i = 0; i < go.Length; i++) 
				{	
					MeshCreater.CreateMesh(go[i]);
				}
			}
		}
	}
}
