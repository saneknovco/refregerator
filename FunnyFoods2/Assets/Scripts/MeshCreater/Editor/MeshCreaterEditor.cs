﻿using UnityEngine;
using UnityEditor;

[CustomEditor(typeof(MeshCreater))]
public class MeshCreaterEditor : Editor 
{
	MeshCreater mc;
	public override void OnInspectorGUI()
	{
		if(GUILayout.Button("Create mesh"))
		{
			mc = target as MeshCreater;
			mc.CreateMesh();
		}
	}
}
