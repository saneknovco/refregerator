﻿using UnityEngine;
using System.Collections;

public class RM_StrangeMovement : MonoBehaviour 
{
	public Vector3 direction;
	public float speed, rotationSpeed = 50, yDirectionDelta, destroyAfterInvisibleTimer = 3;
	Vector3 lastPos;
	MeshRenderer thisRenderer;
    float halfTime;
	void Start () 
	{
        halfTime = destroyAfterInvisibleTimer / 2;
		lastPos = transform.position;
		direction.Normalize();
		thisRenderer = transform.GetComponent<MeshRenderer>();
		switch(Random.Range(1,5))
		{
			case 1: { transform.position = new Vector3(-1350, -585, 20);  direction = new Vector3 (Mathf.Abs(direction.x), Mathf.Abs(direction.y), 0); rotationSpeed = -Mathf.Abs(rotationSpeed);} break;
			case 2: { transform.position = new Vector3(-1350, 585, 20); direction = new Vector3 (Mathf.Abs(direction.x), -Mathf.Abs(direction.y), 0); rotationSpeed = -Mathf.Abs(rotationSpeed);} break;
			case 3: { transform.position = new Vector3(1350, 585, 20); direction = new Vector3 (-Mathf.Abs(direction.x), -Mathf.Abs(direction.y), 0); rotationSpeed = Mathf.Abs(rotationSpeed);} break;
			case 4: { transform.position = new Vector3(1350, -585, 20); direction = new Vector3 (-Mathf.Abs(direction.x), Mathf.Abs(direction.y), 0); rotationSpeed = Mathf.Abs(rotationSpeed);} break;
		}
		if(direction.y != 0)
		{
			if(direction.y > 0) yDirectionDelta = -Mathf.Abs(yDirectionDelta);
			else yDirectionDelta = Mathf.Abs(yDirectionDelta);
		}
	}
	bool wasVisible = false;
	float angle = Mathf.PI;
	void Update () 
	{
		if(thisRenderer.isVisible || !wasVisible)
		{
			if(thisRenderer.isVisible && !wasVisible) 
			{
				wasVisible = true;
			}
			transform.eulerAngles += new Vector3(0, 0, angle) * Time.deltaTime * rotationSpeed;
			transform.position += direction * speed * Time.deltaTime;

				direction += new Vector3(0, yDirectionDelta, 0);

			direction.Normalize();

			lastPos = transform.position;
		}
		else
		{
			if(destroyAfterInvisibleTimer <= 0) GameObject.Destroy(transform.gameObject);
			destroyAfterInvisibleTimer -= Time.deltaTime;
		}
	}
}
