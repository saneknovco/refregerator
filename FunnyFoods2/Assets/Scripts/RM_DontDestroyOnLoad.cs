﻿using UnityEngine;
using System.Collections;

public class RM_DontDestroyOnLoad : MonoBehaviour {

	public static GameObject checkDuplication;

	void Awake()
	{
		if(checkDuplication == null)
		{
			Object.DontDestroyOnLoad(transform.gameObject);
			checkDuplication = transform.gameObject;
		}
		else Destroy(transform.gameObject);
	}
}
