﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using FunnyFoods;

public class SexTagger : MonoBehaviour 
{
	public GameElements thisGameElement;
	public bool useOwnerName;
	void Start () 
	{
		Sex sex;
		Dictionary<GameElements, Sex> dict = LanguageData.Instance.GetFoodSex (AppState.instance.CurrentLangStr.ToLower());
		if (dict != null) 
		{
			dict.TryGetValue (thisGameElement, out sex);
			if (useOwnerName)
				transform.name = System.Enum.GetName (typeof(Sex), sex);
			else
				transform.tag = System.Enum.GetName (typeof(Sex), sex);
		}
	}
}
