﻿using UnityEngine;
#if UNITY_EDITOR
using UnityEditor;
#endif
using System.Collections;
using System.Collections.Generic;

namespace FunnyFoods
{
	public enum GameElements
	{
		Banana, Apple, Melon, Corn, Cheese, Lemon, Pear, Grapes, Eggplante, Cabbage, Blackberry, Fig, Watermelon, Cucumber, 
		Broccoli, Peas, Mafin, Coconut, Cookie, Ice_cream, Chocolate, Potato, Plum, Jelly, Blueberry, Lollipop, Strawberry,
		Tomato, Radish, Bell_pepper, Chili_pepper, Pomegranate, Pumpkin, Onion, Persimmon, Yogurt, Mushroom, Egg, Garlic, 
		Zephyr, Orange, Carrot, BOAT, TRUCK, PAN, STAR, RECTANGLE, DIAMOND, SQUARE, OVAL, CIRCLE, TRIANGLE, SEMICIRCLE, HEART, HEXAGON, PENTAGON
	}
	public enum Sex
	{
		HE, SHE, IT
	}
	public enum SpecificLanguages
	{
		ru, de, fr, sp, pt, it
		//TO DO: Here need to add new specific language
	}
	
	public class LanguageData : ScriptableObject
	{
		private static LanguageData _instance;
		public static LanguageData Instance
		{
			get
			{
				try
				{
					if (_instance == null) 
					{
						_instance = MgUnityHelper.CreateFromResources<LanguageData>("LangData/", "FunnyFoods.LanguageData");
						_instance.InitializeDictionaries();
					}
	
				}
				catch
				{
					_instance = ScriptableObject.CreateInstance<LanguageData>();
					_instance.InitializeDictionaries();
				}
				return _instance;
			}
		}
	
		public LanguageDataContainer languageDataContainer;
	
		private void InitializeDictionaries ()
		{
			languageDataContainer.RefreshDictionaries ();
		}

		public Dictionary<GameElements, Sex> GetFoodSex(string language)
		{
			switch(language)
			{
			case "ru":
				{
					return languageDataContainer.foodSexRU;
					break;
				}
			case "de":
				{
					return languageDataContainer.foodSexDE;
					break;
				}
			case "fr":
				{
					return languageDataContainer.foodSexFR;
					break;
				}
			case "sp":
				{
					return languageDataContainer.foodSexSP;
					break;
				}

			case "pt":
				{
					return languageDataContainer.foodSexPT;
					break;
				}

			case "it":
				{
					return languageDataContainer.foodSexIT;
					break;
				}
				//TO DO: Here need to add new specific language
			default:
				{
					return null;
					break;
				}
			}
   		}
	
		[System.Serializable]
		public class LanguageDataContainer : FullInspector.BaseObject
		{
			#if UNITY_EDITOR
			[UnityEditor.MenuItem("Assets/Create/LanguageData")]
			public static void CreateAsset()
			{
				ScriptableObjectUtility.CreateAsset<LanguageData>();
			}
			#endif

			[FullInspector.InspectorCategory("RU")]
			[FullInspector.InspectorCategory("DE")]
			[FullInspector.InspectorCategory("FR")]
			[FullInspector.InspectorCategory("SP")]
			[FullInspector.InspectorCategory("PT")]
			[FullInspector.InspectorCategory("IT")]
			[FullInspector.InspectorButton]
			[FullInspector.InspectorName("Refresh")]
			public void RefreshDictionaries ()
			{
				GameElements[] values = System.Enum.GetValues (typeof(GameElements)) as GameElements[];
				if(foodSexRU == null || foodSexRU.Count == 0)
				{
					foodSexRU = new Dictionary<GameElements, Sex> ();
					for (int i = 0; i < values.Length; i++) 
					{
						foodSexRU.Add (values [i], Sex.HE);
					}
				}
				if (foodSexDE == null || foodSexDE.Count == 0) 
				{
					foodSexDE = new Dictionary<GameElements, Sex> ();
					for (int i = 0; i < values.Length; i++) 
					{
						foodSexDE.Add (values [i], Sex.HE);
					}
				}
				if (foodSexFR == null || foodSexFR.Count == 0) 
				{
					foodSexFR = new Dictionary<GameElements, Sex> ();
					for (int i = 0; i < values.Length; i++) 
					{
						foodSexFR.Add (values [i], Sex.HE);
					}
				}
				if (foodSexSP == null || foodSexSP.Count == 0) 
				{
					foodSexSP = new Dictionary<GameElements, Sex> ();
					for (int i = 0; i < values.Length; i++) 
					{
						foodSexSP.Add (values [i], Sex.HE);
					}
				}
				if (foodSexPT == null || foodSexPT.Count == 0) 
				{
					foodSexPT = new Dictionary<GameElements, Sex> ();
					for (int i = 0; i < values.Length; i++) 
					{
						foodSexPT.Add (values [i], Sex.HE);
					}
				}
				if (foodSexIT == null || foodSexIT.Count == 0) 
				{
					foodSexIT = new Dictionary<GameElements, Sex> ();
					for (int i = 0; i < values.Length; i++) 
					{
						foodSexIT.Add (values [i], Sex.HE);
					}
				}
				//TO DO: Here need to add new specific language
			}

			[FullInspector.InspectorCategory("RU")]
			[FullInspector.InspectorName("Food`s sex")]
			[FullInspector.InspectorCollectionPager(500)]
			public Dictionary<GameElements, Sex> foodSexRU;

			[FullInspector.InspectorCategory("DE")]
			[FullInspector.InspectorName("Food`s sex")]
			[FullInspector.InspectorCollectionPager(500)]
			public Dictionary<GameElements, Sex> foodSexDE;
			
			[FullInspector.InspectorCategory("FR")]
			[FullInspector.InspectorName("Food`s sex")]
			[FullInspector.InspectorCollectionPager(500)]
			public Dictionary<GameElements, Sex> foodSexFR;

			[FullInspector.InspectorCategory("SP")]
			[FullInspector.InspectorName("Food`s sex")]
			[FullInspector.InspectorCollectionPager(500)]
			public Dictionary<GameElements, Sex> foodSexSP;

			[FullInspector.InspectorCategory("PT")]
			[FullInspector.InspectorName("Food`s sex")]
			[FullInspector.InspectorCollectionPager(500)]
			public Dictionary<GameElements, Sex> foodSexPT;

			[FullInspector.InspectorCategory("IT")]
			[FullInspector.InspectorName("Food`s sex")]
			[FullInspector.InspectorCollectionPager(500)]
			public Dictionary<GameElements, Sex> foodSexIT;

			//TO DO: Here need to add new specific language
		}
	
	}
}