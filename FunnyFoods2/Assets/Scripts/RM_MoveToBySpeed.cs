﻿using UnityEngine;
using System.Collections;

public class RM_MoveToBySpeed : MonoBehaviour 
{
	public Vector3 destinationPosition;
	public bool useIntermediatePositions;
	public Vector3[] intermediatePositions;
	public float speed;
	public bool resetZAtTheEnd;
	public float zForReset;
	public string finishEvent;
	private Vector3 direction;
	private Vector3 lastDirection, newDirection;
	private Component thisComponent;
	private int counter;
	private Vector3[] intermediateDirections;
	void Start () 
	{
		if(useIntermediatePositions)
		{
			if(intermediatePositions != null && intermediatePositions.Length > 0)
			{
				counter = 0;
				intermediateDirections = new Vector3[intermediatePositions.Length];
				intermediateDirections[0] = (intermediatePositions[0] - transform.position) / (intermediatePositions[0] - transform.position).magnitude;
				for (int i = 1; i < intermediateDirections.Length; i++) 
				{
					intermediateDirections[i] = (intermediatePositions[i] - intermediatePositions[i-1]) / (intermediatePositions[i] - intermediatePositions[i-1]).magnitude;
				}
				direction = (destinationPosition - intermediatePositions[intermediatePositions.Length - 1]) / (destinationPosition - intermediatePositions[intermediatePositions.Length - 1]).magnitude;
			}
		}
		else
		{
			intermediatePositions = new Vector3[0];
			intermediateDirections = new Vector3[0];
			direction = (destinationPosition - transform.position) / (destinationPosition - transform.position).magnitude;
		}
	}

	void Update () 
	{
		if(useIntermediatePositions && counter < intermediatePositions.Length)
		{
			if(Time.deltaTime * speed < Vector3.Distance(intermediatePositions[counter], transform.position))
				transform.position += intermediateDirections[counter] * speed * Time.deltaTime;
			else
			{
				transform.position = intermediatePositions[counter];
				counter ++;
			}
		}
		else
		{
			if(Time.deltaTime * speed < Vector3.Distance(destinationPosition, transform.position))
				transform.position += direction * speed * Time.deltaTime;
			else
			{
				if(resetZAtTheEnd)
					transform.position = new Vector3(destinationPosition.x, destinationPosition.y, zForReset);
				else
					transform.position = destinationPosition;

	            if (finishEvent != null)
	                if (finishEvent.Length > 0)
						PlayMakerFSM.BroadcastEvent(finishEvent);
				thisComponent = transform.GetComponent<RM_MoveToBySpeed>();
				DestroyObject(thisComponent);
			}
		}
	}
}
