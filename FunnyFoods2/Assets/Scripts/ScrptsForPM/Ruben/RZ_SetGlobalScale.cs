﻿using UnityEngine;

namespace HutongGames.PlayMaker.Actions
{
    [ActionCategory("FF2_Ruben")]
    public class RZ_SetGlobalScale : FsmStateAction 
	{
		[RequiredField]
		public FsmOwnerDefault obj;

		[UIHint(UIHint.Variable)]
		public FsmVector3 vector = new FsmVector3() { UseVariable = true };
		public FsmFloat x = new FsmFloat() { UseVariable = true };
		public FsmFloat y = new FsmFloat() { UseVariable = true };
		public FsmFloat z = new FsmFloat() { UseVariable = true };
		
		public bool everyFrame;
		GameObject _go;

		public override void Reset()
		{
			obj = null;
			vector = null;

			x = new FsmFloat { UseVariable = true };
			y = new FsmFloat { UseVariable = true };
			z = new FsmFloat { UseVariable = true };

			everyFrame = false;
		}

		public override void OnEnter()
		{
			_go = Fsm.GetOwnerDefaultTarget(obj);

			DoSetScale();
			
			if (!everyFrame)
			{
				Finish();
			}
		}

		public override void OnUpdate()
		{
			DoSetScale();
		}

		Vector3 scale;
		void DoSetScale()
		{

			if (_go == null)
			{
				return;
			}
			
			scale = vector.IsNone ? _go.transform.localScale : vector.Value;
			
			if (!x.IsNone) scale.x = x.Value;
			if (!y.IsNone) scale.y = y.Value;
			if (!z.IsNone) scale.z = z.Value;
			
			CalcScale(_go.transform);
		}

		void CalcScale(Transform go)
		{
			if(go.parent != null)
			{
				_go.transform.localScale = DevideVectors(scale, go.parent.localScale);
				scale = _go.transform.localScale;
				CalcScale(go.parent);
			}
		}
		
		Vector3 DevideVectors (Vector3 a, Vector3 b)
		{
			return new Vector3(a.x / (b.x != 0 ? b.x : 1), a.y / (b.y != 0 ? b.y : 1), a.z / (b.z != 0 ? b.z : 1));
		}
	}
}