﻿using UnityEngine;

namespace HutongGames.PlayMaker.Actions
{
    [ActionCategory("FF2_Ruben")]
    public class RZ_SetPlayingAnimationTime : FsmStateAction 
	{
		public FsmOwnerDefault gameObject;
		public FsmFloat animTime = new FsmFloat { UseVariable = false };
		public FsmBool normalize = true;
		public FsmBool everyFrame = false;

		GameObject go;
		bool useOwner;
		public override void Reset()
		{
			animTime.Value = 0f;
            normalize.Value = true;
			everyFrame.Value = false;
		}

		public override void OnEnter ()
		{
			go = Fsm.GetOwnerDefaultTarget(gameObject);
			useOwner = gameObject.OwnerOption == OwnerDefaultOption.UseOwner;
			DoResetAnimations();
			if(!everyFrame.Value) Finish ();
		}
		public override void OnUpdate ()
		{
			DoResetAnimations();
		}

		void DoResetAnimations()
		{
			if(animTime.Value < 0) animTime = 0f;

			Animation animation = go.GetComponent<Animation>();
			string name;
			float time; 

			foreach (AnimationState anim in animation) 
			{
				name = anim.name;
				if(animation.IsPlaying(name) || !useOwner)
				{
					AnimationClip clip = animation.GetClip(name);
					
					if(normalize.Value)
					{
						time = animTime.Value > 1 ? 1f : animTime.Value;
						time = time * clip.length;
					}
					else
					{
						time = animTime.Value > clip.length ? clip.length : animTime.Value;
					}
					
					animation[name].time = time;
					animation[name].enabled = true;
					animation.Sample();
					animation[name].enabled = false;
				}
			}
		}
	}
}	