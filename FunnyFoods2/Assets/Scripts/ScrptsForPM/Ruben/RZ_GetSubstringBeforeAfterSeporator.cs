﻿using UnityEngine;
using System.Collections;

namespace HutongGames.PlayMaker.Actions
{
    [ActionCategory("FF2_Ruben")]
    public class RZ_GetSubstringBeforeAfterSeporator : FsmStateAction 
	{
		public FsmString inputStr = new FsmString() { UseVariable = true };
		public FsmString outputStr = new FsmString() { UseVariable = true };
		public FsmString seporator = new FsmString();
		public FsmBool before = new FsmBool();


		public override void OnEnter ()
		{
			if (inputStr.Value.Contains(seporator.Value))
			{
				int seporatorIndex = inputStr.Value.IndexOf(seporator.Value, 0);
				if(before.Value)
				{
					outputStr.Value = inputStr.Value.Substring(0,seporatorIndex);
				}
				else
				{
					outputStr.Value = inputStr.Value.Substring(seporatorIndex+1, inputStr.Value.Length - (seporatorIndex+1));
				}
			}
			else
			{
				outputStr.Value = inputStr.Value;
			}
			Finish();
		}
	}
}