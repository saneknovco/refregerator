﻿using UnityEngine;

namespace HutongGames.PlayMaker.Actions
{
    [ActionCategory("FF2_Ruben")]
    public class RZ_GameObjectCompareName : FsmStateAction 
	{
		public FsmOwnerDefault object1;
		public FsmOwnerDefault object2;
		public FsmEvent ifFalse;
		public FsmEvent ifTrue;
		public FsmBool storeResult;
		public bool everyFrame = false;

		GameObject o1,o2;
		void DoCompare()
		{
			if(o1.name == o2.name)
			{
				if(!storeResult.IsNone) storeResult.Value = true;
				Fsm.Event(ifTrue);
			}
			else
			{
				if(!storeResult.IsNone) storeResult.Value = false;
				Fsm.Event(ifFalse);
			}
		}
		public override void OnEnter ()
		{
			o1 = Fsm.GetOwnerDefaultTarget(object1);
			o2 = Fsm.GetOwnerDefaultTarget(object2);
			DoCompare();
			if(!everyFrame) Finish ();
		}
		public override void OnUpdate ()
		{
			DoCompare();
		}
	}
}