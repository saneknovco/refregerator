﻿using UnityEngine;

namespace HutongGames.PlayMaker.Actions
{
    [ActionCategory("FF2_Ruben")]
    public class RZ_SetStringValue : FsmStateAction
	{
		[RequiredField]
		[UIHint(UIHint.Variable)]
		public FsmString stringVariable;
		public FsmString stringValue;
		public bool everyFrame;
		
		public override void Reset()
		{
			stringVariable = null;
			stringValue = null;
			everyFrame = false;
		}
		
		public override void OnEnter()
		{
			DoSetStringValue();
			
			if (!everyFrame)
				Finish();
		}
		
		public override void OnUpdate()
		{
			DoSetStringValue();
		}
		
		void DoSetStringValue()
		{
			if (stringVariable == null) return;
			if (stringValue == null || stringValue.IsNone || string.IsNullOrEmpty(stringValue.Value)) stringVariable.Value = "";
			
			stringVariable.Value = stringValue.Value;
		}
		
	}
}
