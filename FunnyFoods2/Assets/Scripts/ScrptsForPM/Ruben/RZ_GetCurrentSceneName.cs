﻿using UnityEngine;
using System.Collections;

namespace HutongGames.PlayMaker.Actions
{
	public class RZ_GetCurrentSceneName : FsmStateAction 
	{
		public FsmString storeResult;

		public override void OnEnter ()
		{
			if(!storeResult.IsNone) 
                storeResult.Value = Application.loadedLevelName;
            Finish();
		}
	}
}