﻿using UnityEngine;
using Bini.Utils.Audio;

namespace HutongGames.PlayMaker.Actions
{
    [ActionCategory("FF2_Ruben")]
    [Tooltip("Plays an Audio Clip at a position defined by a Game Object or Vector3. If a position is defined, it takes priority over the game object. This action doesn't require an Audio Source component, but offers less control than Audio actions.")]
    public class RZ_PlaySoundByName : FsmStateAction
    {
        [RequiredField]
        [Title("Audio Clip")]
        public FsmString clipName;

        [HasFloatSlider(0, 1)]
        public FsmFloat volume = 1f;
        public FsmFloat speed = 1f;
        public FsmBool loop = false;
        public FsmBool waitForEnd = false;

        public override void Reset()
        {
            volume = 1;
            speed = 1;
            loop = false;
            waitForEnd = false;
        }

        public override void OnEnter()
        {
            DoPlaySound();
            if(!waitForEnd.Value) Finish();
        }

        AudioSource audioSource;
        void DoPlaySound()
        {
			AudioClip audioClip = Resources.Load("Sounds/"+clipName.Value) as AudioClip;

            if (audioClip == null)
            {
                LogWarning("Missing Audio Clip!");
                return;
            }
            
            audioSource = Audio.PlaySound(audioClip, volume.Value, speed.Value, false, loop.Value, null);
            audioSource.transform.position = Camera.main.transform.position;
        }
        public override void OnUpdate()
        {
            if (waitForEnd.Value)
                if (!audioSource.isPlaying) Finish();
        }


    }
}
