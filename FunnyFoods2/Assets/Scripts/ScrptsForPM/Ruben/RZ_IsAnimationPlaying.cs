﻿using UnityEngine;

namespace HutongGames.PlayMaker.Actions
{
    [ActionCategory("FF2_Ruben")]
    public class RZ_IsAnimationPlaying : FsmStateAction 
	{
		[RequiredField]
		public FsmOwnerDefault animObj;
		public FsmEvent isTrue, isFalse;
		public FsmBool isRecursive;
		public FsmBool storeResult = new FsmBool() { UseVariable = true };
		public FsmBool everyFrame;

		private GameObject _go;
		private Animation _animation;

		public void DoCheckIsAnimationPlaying()
		{
			bool isPlaying = _go.IsAnimationPlaying (isRecursive: isRecursive.Value);
			if (isPlaying) 
			{
				if (!storeResult.IsNone)
					storeResult.Value = true;
				if (isTrue != null)
					Fsm.Event (isTrue);
			} else 
			{
				if(!storeResult.IsNone)
					storeResult.Value = false;
				if(isFalse != null)
					Fsm.Event(isFalse);
			}
//			if(_animation != null)
//			{
//				if(_animation.isPlaying)
//				{
//					if(!storeResult.IsNone)
//						storeResult.Value = true;
//					if(isTrue != null)
//						Fsm.Event(isTrue);
//				}
//				else
//				{
//					if(!storeResult.IsNone)
//						storeResult.Value = false;
//					if(isFalse != null)
//						Fsm.Event(isFalse);
//				}
//			}
//			else 
//			{
//				if(!storeResult.IsNone)
//					storeResult.Value = false;
//				if(isFalse != null)
//					Fsm.Event(isFalse);
//			}
		}
		public override void OnEnter ()
		{
			if(animObj!=null)
			{
				_go = Fsm.GetOwnerDefaultTarget(animObj);
//                try { _animation = _go.GetComponent<Animation>(); }
//                catch { _animation = null; }
			}
			else 
			{
				if(!storeResult.IsNone)
					storeResult.Value = false;
				if(isFalse != null)
					Fsm.Event(isFalse);
				Finish();
			}
			DoCheckIsAnimationPlaying();

			if(!everyFrame.Value) Finish ();
		}
		public override void OnUpdate ()
		{
			DoCheckIsAnimationPlaying();
		}
	}
}