﻿using UnityEngine;
using System.Collections;

namespace HutongGames.PlayMaker.Actions
{
    [ActionCategory("FF2_Ruben")]
    public class RZ_CheckDistanceToAllChildren : FsmStateAction 
	{
		public FsmOwnerDefault fromObj;
		public FsmGameObject parent;
		public FsmFloat permissibleDistance;
		public FsmBool storeResult;
        public FsmBool everyFrame = false;
		Transform _go, _parent;

		public override void OnEnter ()
		{
			_go = Fsm.GetOwnerDefaultTarget (fromObj).transform;
			_parent = parent.Value.transform;
            CheckDistanceToAll();
            if(!everyFrame.Value) Finish ();
		}

        public override void OnUpdate()
        {
            CheckDistanceToAll();
        }

        void CheckDistanceToAll()
        {
            storeResult.Value = true;
            for (int i = 0; i < _parent.childCount; i++)
            {
                if (!CheckDistance(_go.position, _parent.GetChild(i).position))
                {
                    storeResult.Value = false;
                    break;
                }
            }
        }
		bool CheckDistance(Vector3 from, Vector3 to)
		{
			float dist = Mathf.Sqrt (Mathf.Pow (to.x - from.x, 2) + Mathf.Pow (to.y - from.y, 2));
			if (dist < permissibleDistance.Value) 
				return false;
			else 
				return true;
		}
	}
}