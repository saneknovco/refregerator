﻿using UnityEngine;
using System.Collections;
using System.Text.RegularExpressions;

namespace HutongGames.PlayMaker.Actions
{
    [ActionCategory("FF2_Ruben")]
    public class RZ_OpenWebPage : FsmStateAction 
	{
		public FsmString URL = new FsmString();

		public override void OnEnter ()
		{
			string pattern = @"(http|https):\/\/[\w\-_]+(\.[\w\-_]+)+([\w\-\.,@?^=%&amp;:/~\+#]*[\w\-\@?^=%&amp;/~\+#])?";
			Regex reg = new Regex(pattern);

			if(!URL.IsNone && URL.Value.Length > 0)
			{
				MatchCollection matches = reg.Matches(URL.Value);
				if(matches.Count > 0)
					Application.OpenURL(URL.Value);
			}
			Finish ();
		}
	}
}