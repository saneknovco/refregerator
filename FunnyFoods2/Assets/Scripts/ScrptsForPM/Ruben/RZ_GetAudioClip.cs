﻿using UnityEngine;
using System.Collections;
namespace HutongGames.PlayMaker.Actions
{
    [ActionCategory("FF2_Ruben")]
    public class RZ_GetAudioClip : FsmStateAction
    {
        public FsmString clipName = new FsmString() { UseVariable = true };
        public FsmObject storeClip = new FsmObject() { UseVariable = true };

        public override void OnEnter()
        {
            AudioClip ac = Resources.Load("Sounds/" + clipName) as AudioClip; ;
            storeClip.Value = ac != null ? ac : new AudioClip();
            Finish();
        }
    }
}