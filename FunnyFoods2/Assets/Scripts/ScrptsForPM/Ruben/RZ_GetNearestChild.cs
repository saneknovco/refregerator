﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
namespace HutongGames.PlayMaker.Actions
{
    [ActionCategory("FF2_Ruben")]
    public class RZ_GetNearestChild : FsmStateAction
    {
        public FsmOwnerDefault fromObj;
        public FsmGameObject parent;
        public FsmFloat maxNeedDistance;
        public FsmGameObject storeGameObject;
        public FsmBool everyFrame = false;
        Transform _go, _parent;


        public override void OnEnter()
        {
            _go = Fsm.GetOwnerDefaultTarget(fromObj).transform;
            _parent = parent.Value.transform;
            storeGameObject.Value = CheckDistanceToAll();
            if (!everyFrame.Value) Finish();
        }

        public override void OnUpdate()
        {
            storeGameObject.Value = CheckDistanceToAll();
        }

        GameObject CheckDistanceToAll()
        {
            float minDist = float.MaxValue, currentDist;
            GameObject go = null;
            for (int i = 0; i < _parent.childCount; i++)
            {
                currentDist = GetDistance(_go.position, _parent.GetChild(i).position);
                if (currentDist < minDist && currentDist <= maxNeedDistance.Value)
                {
                    minDist = currentDist;
                    go = _parent.GetChild(i).gameObject;
                }
            }
            return go;
        }
        float GetDistance(Vector3 from, Vector3 to)
        {
            return Mathf.Sqrt((to.x - from.x) * (to.x - from.x) + (to.y - from.y) * (to.y - from.y));
        }
    }
}