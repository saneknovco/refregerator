﻿using UnityEngine;

namespace HutongGames.PlayMaker.Actions
{
    [ActionCategory("FF2_Ruben")]
    public class RZ_SetAlpha : FsmStateAction
    {
        public FsmOwnerDefault owner;
        public FsmFloat alpha;
        public FsmBool isRecursive = true;
        public FsmBool everyFrame;

        Transform tr;
        void SetAlpha(Transform trans)
        {
            Renderer rend = trans.GetComponent<MeshRenderer>();
            if (rend != null)
            {
                if (!rend.material.HasProperty("_Color"))
					rend.material.shader = Shader.Find("Unlit/AlphaSelfIllumWithFade");

                Color color = rend.material.color;
                color.a = alpha.Value;
                rend.material.color = color;
            }
        }
        void RecursiveSetAlpha(Transform trans)
        {
            SetAlpha(trans);
            for (int i = 0; i < trans.childCount; i++)
            {
                SetAlpha(trans.GetChild(i));
                RecursiveSetAlpha(trans.GetChild(i));
            }
        }

        public override void OnEnter()
        {
            tr = Fsm.GetOwnerDefaultTarget(owner).transform;

            if (alpha.Value > 1) alpha.Value = 1;
            if (alpha.Value < 0) alpha.Value = 0;

            if (isRecursive.Value) RecursiveSetAlpha(tr);
            else SetAlpha(tr);

            if (!everyFrame.Value) Finish();
        }

        public override void OnUpdate()
        {
            if (isRecursive.Value) RecursiveSetAlpha(tr);
            else SetAlpha(tr);
        }
    }
}