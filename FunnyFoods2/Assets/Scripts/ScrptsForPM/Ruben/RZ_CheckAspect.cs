﻿using UnityEngine;


namespace HutongGames.PlayMaker.Actions
{
    /*public class AspectFsmEventPair
	{
		[RequiredField]
		public FsmFloat first;
		[RequiredField]
		public FsmFloat second;
		public FsmEvent EventVar;
		public AspectFsmEventPair ()
		{
			
		}
		public float GetAspect()
		{
			return first.Value / second.Value;
		}
	}*/
    [ActionCategory("FF2_Ruben")]
    public class RZ_CheckAspect : FsmStateAction 
	{
		public FsmEvent _16on9, _4on3, _3on2;
		public FsmString saveAspectName = new FsmString() { UseVariable = true };
		public override void OnEnter ()
		{
			float aspect = Camera.main.aspect;
			if (aspect >= 1.7f)
			{
				saveAspectName.Value = "16on9";
				if(_16on9 != null)
					Fsm.Event (_16on9); 
			} 
			else if(aspect >= 1.49f)
			{
				saveAspectName.Value = "3on2";
				if(_3on2 != null)
					Fsm.Event (_3on2);
			} 
			else
			{
				saveAspectName.Value = "4on3";
				if(_4on3 != null)
					Fsm.Event (_4on3);
			} 
			Finish ();
		}
		/*[RequiredField]
		public AspectFsmEventPair[] aspects;

		public override void OnEnter ()
		{
			float aspect = Camera.main.aspect;
			for (int i = 0; i < aspects.Length; i++) 
			{
				if(Mathf.Approximately(aspect, aspects[i].GetAspect())) Fsm.Event(aspects[i].EventVar);
			}
		}*/


	}
}