﻿using UnityEngine;

namespace HutongGames.PlayMaker.Actions
{
    [ActionCategory("FF2_Ruben")]
    public class RZ_PositionGUIElements : FsmStateAction
    {
        [ActionSection("Left top element")]
        public FsmOwnerDefault leftTop = new FsmOwnerDefault() { OwnerOption = OwnerDefaultOption.UseOwner };
        public FsmFloat leftTopIndent = new FsmFloat() { UseVariable = true};

        [ActionSection("Right top element")]
        public FsmOwnerDefault rightTop = new FsmOwnerDefault() { OwnerOption = OwnerDefaultOption.SpecifyGameObject };
        public FsmFloat rightTopIndent = new FsmFloat() { UseVariable = true };

        [ActionSection("Left bottom element")]
        public FsmOwnerDefault leftBottom = new FsmOwnerDefault() { OwnerOption = OwnerDefaultOption.SpecifyGameObject };
        public FsmFloat leftBottomIndent = new FsmFloat() { UseVariable = true };

        [ActionSection("Right bottom element")]
        public FsmOwnerDefault rightBottom = new FsmOwnerDefault() { OwnerOption = OwnerDefaultOption.SpecifyGameObject };
        public FsmFloat rightBottomIndent = new FsmFloat() { UseVariable = true };

        GameObject _leftTop, _rightTop, _leftBottom, _rightBottom;
        float _ortSize, _height, _aspect, _width;
        Vector3 _leftTopColliderSize, _rightTopColliderSize, _leftBottomColliderSize, _rightBottomColliderSize;

        public override void Reset()
        {
            leftTopIndent = new FsmFloat() { UseVariable = true };
            rightTopIndent = new FsmFloat() { UseVariable = true };
            leftBottomIndent = new FsmFloat() { UseVariable = true };
            rightBottomIndent = new FsmFloat() { UseVariable = true };
            leftTop = new FsmOwnerDefault() { OwnerOption = OwnerDefaultOption.UseOwner };
            rightTop = new FsmOwnerDefault() { OwnerOption = OwnerDefaultOption.SpecifyGameObject };
            leftBottom = new FsmOwnerDefault() { OwnerOption = OwnerDefaultOption.SpecifyGameObject };
            rightBottom = new FsmOwnerDefault() { OwnerOption = OwnerDefaultOption.SpecifyGameObject };
        }

        public override void OnEnter()
        {
            //CALCULATE MAIN PARAMS
            _height = Camera.main.orthographicSize;
            _aspect = Camera.main.aspect;
            _width = _height * _aspect;

            //POSITIONING LEFT TOP ELEMENT
            if (leftTop != null)
            {
                _leftTop = Fsm.GetOwnerDefaultTarget(leftTop);
                if (_leftTop != null)
                {
                    _leftTopColliderSize = _leftTop.GetComponent<BoxCollider>().size * _leftTop.transform.localScale.x;
                    _leftTop.transform.localPosition = new Vector3(-_width + _leftTopColliderSize.x / 2 + leftTopIndent.Value, _height - _leftTopColliderSize.y / 2 - leftTopIndent.Value, 0);
                }
            }
            //POSITIONING RIGHT TOP ELEMENT
            if (rightTop != null)
            {
                _rightTop = Fsm.GetOwnerDefaultTarget(rightTop);
                if (_rightTop != null)
                {
                    _rightTopColliderSize = _rightTop.GetComponent<BoxCollider>().size * _rightTop.transform.localScale.x;
                    _rightTop.transform.localPosition = new Vector3(_width - _rightTopColliderSize.x / 2 - rightTopIndent.Value, _height - _rightTopColliderSize.y / 2 - rightTopIndent.Value, 0);
                }
            }
            //POSITIONING LEFT BOTTOM ELEMENT
            if (leftBottom != null)
            {
                _leftBottom = Fsm.GetOwnerDefaultTarget(leftBottom);
                if (_leftBottom != null)
                {
                    _leftBottomColliderSize = _leftBottom.GetComponent<BoxCollider>().size * _leftBottom.transform.localScale.x;
                    _leftBottom.transform.localPosition = new Vector3(-_width + _leftBottomColliderSize.x / 2 + leftBottomIndent.Value, -_height + _leftBottomColliderSize.y / 2 + leftBottomIndent.Value, 0);
                }
            }
            //POSITIONING RIGHT BOTTOM ELEMENT
            if (rightBottom != null)
            {
                _rightBottom = Fsm.GetOwnerDefaultTarget(rightBottom);
                if (_rightBottom != null)
                {
                    _rightBottomColliderSize = _rightBottom.GetComponent<BoxCollider>().size * _rightBottom.transform.localScale.x;
                    _rightBottom.transform.localPosition = new Vector3(_width - _rightBottomColliderSize.x / 2 - rightBottomIndent.Value, -_height + _rightBottomColliderSize.y / 2 + rightBottomIndent.Value, 0);
                }
            }

            Finish();
        }
    }
}