﻿using UnityEngine;

namespace HutongGames.PlayMaker.Actions
{
    [ActionCategory("FF2_Ruben")]
    public class RZ_SelectRandomEvent : FsmStateAction
	{
		[CompoundArray("Events", "Event", "Weight")]
		public FsmEvent[] events;
		[HasFloatSlider(0, 1)]
		public FsmFloat[] weights;
		public FsmBool everyFrame;
		
		public override void Reset()
		{
			events = new FsmEvent[2];
			weights = new FsmFloat[] {1,1};
		}
		
		public override void OnEnter()
		{
			DoSelectRandomEvent();
			if (!everyFrame.Value)
				Finish();
		}
		public override void OnUpdate()
		{
			DoSelectRandomEvent();
		}
		void DoSelectRandomEvent()
		{
			if (events == null) return;
			if (events.Length == 0) return;
			
			int randomIndex = ActionHelpers.GetRandomWeightedIndex(weights);
			
			if (randomIndex != -1)
			{
				Fsm.Event(events[randomIndex]);
			}
		}
	}
}
