﻿using UnityEngine;
using System.Collections;

namespace HutongGames.PlayMaker.Actions
{
	[ActionCategory("FF2_Ruben")]
	public class RZ_MoveToBySpeed : FsmStateAction 
	{
		public FsmOwnerDefault obj;
		public FsmVector3 destinationPosition;
		public FsmFloat speed;
		public FsmEventTarget eventTarget;
		public FsmEvent finishEvent;
		private Vector3 direction;
		private Vector3 lastDirection, newDirection;
		private Component thisComponent;
		private Transform tr;

		public override void OnEnter ()
		{
			tr = Fsm.GetOwnerDefaultTarget(obj).transform;
			direction = (destinationPosition.Value - tr.position) / (destinationPosition.Value - tr.position).magnitude;
		}

		public override void OnUpdate () 
		{
			if(Time.deltaTime * speed.Value < Vector3.Distance(destinationPosition.Value, tr.position))
				tr.position += direction * speed.Value * Time.deltaTime;
			else
			{
				tr.position = destinationPosition.Value;
				if(finishEvent != null)
					Fsm.Event(eventTarget, finishEvent);

				Finish ();
			}
		}
	}
}