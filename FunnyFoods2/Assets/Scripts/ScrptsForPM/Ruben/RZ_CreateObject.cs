﻿// (c) Copyright HutongGames, LLC 2010-2013. All rights reserved.

using UnityEngine;

namespace HutongGames.PlayMaker.Actions
{
    [ActionCategory(ActionCategory.GameObject)]
    [Tooltip("Creates a Game Object, usually from a Prefab.")]
    public class RZ_CreateObject : FsmStateAction
    {
        [RequiredField]
        [Tooltip("GameObject to create. Usually a Prefab.")]
        public FsmGameObject gameObject;

        [UIHint(UIHint.Tag)]
        public FsmString tag;


        [Tooltip("Position. If a Spawn Point is defined, this is used as a local offset from the Spawn Point position.")]
        public FsmVector3 position;

        [Tooltip("Rotation. NOTE: Overrides the rotation of the Spawn Point.")]
        public FsmVector3 rotation;

        [UIHint(UIHint.Variable)]
        [Tooltip("Optionally store the created object.")]
        public FsmGameObject storeObject;

        [Tooltip("Optionally parent object for the created object.")]
        public FsmOwnerDefault parentObject;

        [Tooltip("Set the local position to 0,0,0 after parenting.")]
        public FsmBool resetLocalPosition;

        [Tooltip("Set the local position to 0,0,0 after parenting.")]
        public FsmBool resetLocalRotation;

        [Tooltip("Optionally new game object get name of source game object.")]
        public FsmBool getSourceObjectName;

        public FsmBool LocalPosition;

        public FsmBool getSourceObjectRotation;

        public FsmBool useUnloadAssets;

        public FsmInt newObjectCount;

        public bool DontUseFinish;

        public override void Reset()
        {
            gameObject = null;
            position = new FsmVector3 { UseVariable = true };
            rotation = new FsmVector3 { UseVariable = true };
            storeObject = null;
            getSourceObjectName = false;
            newObjectCount = 1;
        }

        public override void OnEnter()
        {
            var go = gameObject.Value;

            if (go != null)
            {
                var spawnPosition = go.transform.position;
                var spawnRotation = go.transform.eulerAngles;

                if (position != null && !position.IsNone)
                {
                    spawnPosition = position.Value;
                }
                if (rotation != null && !rotation.IsNone)
                {
                    spawnRotation = rotation.Value;
                }

                for (int i = 0; i < newObjectCount.Value; i++)
                {
#if !(UNITY_FLASH || UNITY_NACL || UNITY_METRO || UNITY_WP8 || UNITY_WIIU || UNITY_PSM || UNITY_WEBGL)
                    GameObject newObject;
                    newObject = (GameObject)Object.Instantiate(go, spawnPosition, Quaternion.Euler(getSourceObjectRotation.Value ? go.transform.eulerAngles : spawnRotation));
#else
                    var newObject = (GameObject)Object.Instantiate(go, spawnPosition, Quaternion.Euler(spawnRotation));
#endif
                    if (getSourceObjectName.Value)
                        newObject.name = go.name;
                    if (parentObject != null && !parentObject.GameObject.IsNone)
                        newObject.transform.parent = Fsm.GetOwnerDefaultTarget(parentObject).transform;

                    storeObject.Value = newObject;

                    if (LocalPosition.Value == true)
                        newObject.transform.localPosition = position.Value;
                    if (resetLocalPosition.Value)
                        newObject.transform.localPosition = Vector3.zero;
                    if (resetLocalRotation.Value)
                        newObject.transform.localEulerAngles = Vector3.zero;
                    if (!tag.IsNone && !string.IsNullOrEmpty(tag.Value))
                        newObject.tag = tag.Value;
                    else
                        newObject.tag = go.tag;

                }
            }
            if (useUnloadAssets.Value)
                Resources.UnloadUnusedAssets();
            if(!DontUseFinish)
                Finish();
        }

    }
}