﻿using UnityEngine;
using System.Collections;

namespace HutongGames.PlayMaker.Actions
{
    [ActionCategory("FF2_Ruben")]
    [Tooltip("Get or set main camera params.")]
    public class RZ_GetSetMainCameraParam : FsmStateAction
    {
        [Tooltip("Is true: get main camera params, else: set camera params (orthographic size only).")]
        public FsmBool Get = true;
        [ActionSection("Params")]
        public FsmFloat OrthographicSize = new FsmFloat() { UseVariable = true };
        public FsmFloat Aspect = new FsmFloat() { UseVariable = true };

        public override void OnEnter()
        {
            if (Get.Value)
            {
                OrthographicSize.Value = Camera.main.orthographicSize;
                Aspect.Value = Camera.main.aspect;
            }
            else
            {
                Camera.main.orthographicSize = OrthographicSize.Value;
            }
            Finish();
        }
    }
}