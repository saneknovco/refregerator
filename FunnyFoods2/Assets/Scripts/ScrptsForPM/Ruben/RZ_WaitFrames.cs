﻿using UnityEngine;

namespace HutongGames.PlayMaker.Actions
{
    [ActionCategory("FF2_Ruben")]
    public class RZ_WaitFrames : FsmStateAction
	{
		[RequiredField]
		public FsmInt frames;
		public FsmEvent finishEvent;
				
		public override void Reset()
		{
			frames = 1;
			finishEvent = null;
		}
		int counter;
		public override void OnEnter()
		{
			counter = 1;
			if (frames.Value <= 0)
			{
				Fsm.Event(finishEvent);
				Finish();
				return;
			}
		}
		
		public override void OnUpdate()
		{
            counter++;
			if (counter > frames.Value)
			{
				Finish();
				if (finishEvent != null)
				{
					Fsm.Event(finishEvent);
				}
			}
		}
		
	}
}
