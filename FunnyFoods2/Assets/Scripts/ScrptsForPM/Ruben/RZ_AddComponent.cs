﻿
using System;
using UnityEngine;
using Object = UnityEngine.Object;

namespace HutongGames.PlayMaker.Actions
{
    [ActionCategory("FF2_Ruben")]
    public class RZ_AddComponent : FsmStateAction
	{
		[RequiredField]
		[Tooltip("The GameObject to add the Component to.")]
		public FsmOwnerDefault gameObject;
		
		[RequiredField]
		[UIHint(UIHint.ScriptComponent)]
		[Title("Component Type"), Tooltip("The type of Component to add to the Game Object.")]
		public FsmString component;


		[UIHint(UIHint.Variable)]
		[ObjectType(typeof(Component))]
		[Tooltip("Store the component in an Object variable. E.g., to use with Set Property.")]
		public FsmObject storeComponent;

		public FsmProperty property = new FsmProperty() { setProperty = true };

		[Tooltip("Remove the Component when this State is exited.")]
		public FsmBool removeOnExit;
		
		Component addedComponent;

		public override void Reset()
		{
			gameObject = null;
			component = null;
			storeComponent = null;
			property = new FsmProperty() { setProperty = true };
		}
		
		public override void OnEnter()
		{
			DoAddComponent();
            if ((!property.TargetObject.IsNone) && (property.TargetObject != null))
            {
                property.SetValue();
            }
			Finish();
		}
		
		public override void OnExit()
		{
			if (removeOnExit.Value && addedComponent != null)
			{
				Object.Destroy(addedComponent);
			}
		}
		
		void DoAddComponent()
		{
			var go = Fsm.GetOwnerDefaultTarget(gameObject);
			if(go != null)
			{
                if (!component.IsNone && component != null)
                {
                    addedComponent = go.AddComponent(GetType(component.Value));
                    if(!storeComponent.IsNone && storeComponent != null)
                        storeComponent.Value = addedComponent;
                }
				
				if (addedComponent == null)
				{
					LogError("Can't add component: " + component.Value);
				}
			}
		}

        static Type GetType(string name)
        {
            var type = ReflectionUtils.GetGlobalType(name);
            if (type != null) return type;

            type = ReflectionUtils.GetGlobalType("UnityEngine." + name);
            if (type != null) return type;

            type = ReflectionUtils.GetGlobalType("HutongGames.PlayMaker." + name);

            return type;
        }
	}
}