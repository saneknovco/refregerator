﻿using UnityEngine;
using System.Collections;
namespace HutongGames.PlayMaker.Actions
{
	public class Extr
	{
		public FsmFloat min;
		public FsmFloat max;
	}
    [ActionCategory("FF2_Ruben")]
    public class RZ_SwitchSound : FsmStateAction 
	{
		[RequiredField]
		public FsmOwnerDefault movementGO;
		[RequiredField]
		public FsmEvent onMouseUp;
		public FsmEvent onTap;
		public Extr extrX, extrY;

		[ActionSection("Block AXIS")]
		[UIHintAttribute(UIHint.FsmBool)]
		public FsmBool axisX = false;
		[UIHintAttribute(UIHint.FsmBool)]
		public FsmBool axisY = false;

		public Space space = Space.Self;

        Transform _transfGO;
		Vector3 _delta;
		Vector3 _moveVect;
		Vector3 _currentPos;
		int framesCount;
		public override void OnEnter()
		{
			framesCount = 1;
			_transfGO = Fsm.GetOwnerDefaultTarget(movementGO).transform;
			if(space == Space.Self)
			{
				_currentPos = _transfGO.localPosition;
				_delta = Camera.main.ScreenPointToRay(Input.mousePosition).origin - _transfGO.localPosition;
			}
			else
			{
				_currentPos = _transfGO.position;
				_delta = Camera.main.ScreenPointToRay(Input.mousePosition).origin - _transfGO.position;
			}

			_delta.z = 0;
		}

		public override void OnUpdate()
		{			
			framesCount ++;
			if (Input.GetMouseButton(0))
			{
				if(space == Space.Self)
					_currentPos = _transfGO.localPosition;
				else 
					_currentPos = _transfGO.position;

				if(!axisX.Value && ! axisY.Value)
				{
					_moveVect = Camera.main.ScreenPointToRay(Input.mousePosition).origin - _delta;
					_currentPos = new Vector3(_moveVect.x, _moveVect.y, _currentPos.z);
					
					if(_currentPos.x < extrX.min.Value) _currentPos = new Vector3(extrX.min.Value, _currentPos.y, _currentPos.z);
					if(_currentPos.x > extrX.max.Value) _currentPos = new Vector3(extrX.max.Value, _currentPos.y, _currentPos.z);
					if(_currentPos.y < extrY.min.Value) _currentPos = new Vector3(_currentPos.x, extrY.min.Value, _currentPos.z);
					if(_currentPos.y > extrY.max.Value) _currentPos = new Vector3(_currentPos.x, extrY.max.Value, _currentPos.z);
				}
				else if(axisX.Value)
				{
					_moveVect = Camera.main.ScreenPointToRay(Input.mousePosition).origin - _delta;
					_currentPos = new Vector3(_currentPos.x, _moveVect.y, _currentPos.z);

					if(_currentPos.y < extrY.min.Value) _currentPos = new Vector3(_currentPos.x, extrY.min.Value, _currentPos.z);
					if(_currentPos.y > extrY.max.Value) _currentPos = new Vector3(_currentPos.x, extrY.max.Value, _currentPos.z);
				}
				else if(axisY.Value)
				{
					_moveVect = Camera.main.ScreenPointToRay(Input.mousePosition).origin - _delta;
					_currentPos = new Vector3(_moveVect.x, _currentPos.y, _currentPos.z);
					
					if(_currentPos.x < extrX.min.Value) _currentPos = new Vector3(extrX.min.Value, _currentPos.y, _currentPos.z);
					if(_currentPos.x > extrX.max.Value) _currentPos = new Vector3(extrX.max.Value, _currentPos.y, _currentPos.z);
				}
				if(space == Space.Self)
					_transfGO.localPosition = _currentPos;
				else
					_transfGO.position = _currentPos;
			}
			if(Input.GetMouseButtonUp(0))
			{
				if(framesCount <= 10) Fsm.Event(onTap);
				else Fsm.Event(onMouseUp);
			}
		}
	}
}