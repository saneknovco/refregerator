﻿using UnityEngine;
using System.Collections;
namespace HutongGames.PlayMaker.Actions
{
	[ActionCategory("FF2_Ruben")]
    public class RZ_CreateShadowObject : FsmStateAction
    {
        public FsmOwnerDefault obj;
        public FsmColor color;
        public FsmBool isRecursive = true;
		public FsmBool everyFrame = false;
		[UIHint(UIHint.Variable)]
		public FsmArray namesForDestroy;

        Transform tr;
        public override void OnEnter()
        {
			
            tr = Fsm.GetOwnerDefaultTarget(obj).transform;
			for (int i = 0; i < namesForDestroy.Length; i++) 
			{
				GameObject.Destroy(tr.gameObject.FindChild(namesForDestroy.Get(i).ToString()));
			}
            if (isRecursive.Value) RecursiveCreateShadowObject(tr);
            else CreateShadowObject(tr);

            if (!everyFrame.Value) Finish();
        }

        public override void OnUpdate()
        {
            if (isRecursive.Value) RecursiveCreateShadowObject(tr);
            else CreateShadowObject(tr);
        }

        private void CreateShadowObject(Transform trans)
        {
            Material oldMat, newMat;
            MeshRenderer thisRenderer = trans.GetComponent<MeshRenderer>();
            if (thisRenderer != null)
            {
                trans.name = "SHADOW";
                oldMat = thisRenderer.material;
				if(oldMat.HasProperty("_MainTex_Alpha"))
				{
					newMat = new Material(Shader.Find("BiniUber/RGB+A/TC/Color: Silhouette Alpha"));
					newMat.SetTexture("_MainTex_Alpha", oldMat.GetTexture("_MainTex_Alpha"));
				}
				else
					newMat = new Material(Shader.Find("BiniUber/RGBA/TC/Color: Silhouette Alpha"));
				newMat.name = string.Format("{0}_ID{1}_mat", trans.name, Random.Range(0, short.MaxValue + Random.Range(-50, 378)));
                newMat.color = color.Value;
                newMat.mainTexture = oldMat.mainTexture;
				thisRenderer.material = newMat;
            }
        }
        private void RecursiveCreateShadowObject(Transform trans)
        {
            CreateShadowObject(trans);
            for (int i = 0; i < trans.childCount; i++)
            {
                CreateShadowObject(trans.GetChild(i));
                RecursiveCreateShadowObject(trans.GetChild(i));
            }
        }
    }
}