﻿using UnityEngine;
using UnityEngine;

namespace HutongGames.PlayMaker.Actions
{
	[ActionCategory("FF2_Ruben")]
	public class RZ_OnMouseButtonSpy : FsmStateAction
	{
		[RequiredField]
		public MouseButton button;		
		public FsmEvent OnMouseButtonDown;
		public FsmEvent OnMouseButtonUp;

		public override void Reset()
		{
			button = MouseButton.Left;
		}
		public override void OnEnter ()
		{
			DoSpy();
		}
		public override void OnUpdate()
		{
			DoSpy();
		}
		void DoSpy()
		{
			if(Input.GetMouseButtonDown((int)button))
				Fsm.Event (OnMouseButtonDown);
			if(Input.GetMouseButtonUp((int)button))
				Fsm.Event (OnMouseButtonUp);
		}
	}
}

