﻿using UnityEngine;

namespace HutongGames.PlayMaker.Actions
{
    [ActionCategory("FF2_Ruben")]
    public class RZ_GetBoxColliderSize : FsmStateAction 
	{
		public FsmOwnerDefault obj;
		public FsmBool considerScale = new FsmBool() { UseVariable = false };
		public FsmVector3 storeSize = new FsmVector3() { UseVariable = true };
		public FsmFloat storeXSize = new FsmFloat() { UseVariable = true };
		public FsmFloat storeYSize = new FsmFloat() { UseVariable = true };
		public FsmFloat storeZSize = new FsmFloat() { UseVariable = true };

		private GameObject _go;
		private BoxCollider _bc;
		public override void Reset ()
		{
			considerScale = new FsmBool() { UseVariable = false };
			considerScale.Value = true;
		}

		public override void OnEnter ()
		{
			_go = Fsm.GetOwnerDefaultTarget(obj);
			if(obj != null)
			{
				Vector3 tempSize = new Vector3();

				_bc = _go.GetComponent<BoxCollider>();
				if(_bc!= null)
				{
					tempSize = _bc.size;
					if(considerScale.Value)
					{
						Vector3 tempScale = new Vector3();
						tempScale = _go.transform.localScale;
						tempSize = new Vector3(tempSize.x * tempScale.x, tempSize.y * tempScale.y, tempSize.z * tempScale.z);
					}
					if(storeSize!=null) storeSize.Value = tempSize;
					if(storeXSize!=null) storeXSize.Value = tempSize.x;
					if(storeYSize!=null) storeYSize.Value = tempSize.y;
					if(storeZSize!=null) storeZSize.Value = tempSize.z;
				}
			}
			Finish();
		}
	}
}