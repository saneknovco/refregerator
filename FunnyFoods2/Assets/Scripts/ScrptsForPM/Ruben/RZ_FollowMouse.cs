﻿using UnityEngine;
using System.Collections;
using System;

namespace HutongGames.PlayMaker.Actions
{
	[ActionCategory("FF2_Ruben")]
	public class RZ_FollowMouse : FsmStateAction
    {
        [RequiredField]
        public FsmOwnerDefault movementGO;
        public FsmFloat scaleMultiplier;
        public FsmFloat ScaleTime;
        public FsmEvent onMouseUp;
        public FsmFloat z = new FsmFloat() { UseVariable = true };
        public FsmBool resetZ;
        public FsmBool StopX, StopY;
        public FsmBool ResetScale = true;
		[ActionSection("Retun to start position")]
		[Title("Is return to the start position")]
		public FsmBool moveToStart;
		public FsmBool useOtherStartPosition = false;
		public FsmVector3 otherStartPositions;
		public FsmBool useIntermediatePositions = false;
		public FsmVector3[] intermediatePositions;
		public FsmFloat speed;
		[Tooltip("Warning, broadcast all only")]
		public FsmEventTarget target;
		public FsmEvent FinishMoveToStartEvent;

        Transform _transfGO;
        Vector3 _colliderCenter, scale, _delta, _startPos;
        float currentTime, storeResult, lerpTime;
        
        public override void OnEnter()
        {
            storeResult = 0;
            lerpTime = 0;
            currentTime = 0;
            ScaleTime.Value = ScaleTime.Value < 0 ? 0 : ScaleTime.Value;
			_transfGO = Fsm.GetOwnerDefaultTarget(movementGO).transform;
            scale = _transfGO.localScale;
			_startPos = useOtherStartPosition.Value && !otherStartPositions.IsNone ? otherStartPositions.Value : _transfGO.position;

			// Проверка на перехват во время возврата
			if(moveToStart.Value)
			{
				RM_MoveToBySpeed mover = _transfGO.GetComponent<RM_MoveToBySpeed>();
				if(mover != null)
				{
					_startPos = mover.destinationPosition;
					UnityEngine.Object.DestroyObject(mover);
				}
			}

            if (scaleMultiplier.Value < 1) scaleMultiplier.Value = 1;

			_delta = Camera.main.ScreenPointToRay(InputMT.mousePosition).origin - _transfGO.position;

			_delta.z = 0;
        }
        public override void OnUpdate()
        {
			//Шкалирование при "взятии" объекта
            if (InputMT.GetMouseButtonDown(0))
			{
                scale = _transfGO.localScale;
			}

            if (InputMT.GetMouseButton(0))
            {
                currentTime += Time.deltaTime;
                lerpTime = currentTime / ScaleTime.Value;
                storeResult = Mathf.Lerp(scale.x, scale.x * scaleMultiplier.Value, lerpTime);
                _transfGO.localScale = new Vector3(storeResult, storeResult, scale.z);

                if (StopX.Value)
                {
                    _transfGO.position = new Vector3(_transfGO.position.x, Camera.main.ScreenPointToRay(InputMT.mousePosition).origin.y - _delta.y, z.IsNone ? _startPos.z : z.Value);
                }
                else if(StopY.Value)
                {
                    _transfGO.position = new Vector3(Camera.main.ScreenPointToRay(InputMT.mousePosition).origin.x - _delta.x, _transfGO.position.y, z.IsNone ? _startPos.z : z.Value);
                }
                else
                {
                    _transfGO.position = Camera.main.ScreenPointToRay(InputMT.mousePosition).origin - _delta;
                    _transfGO.position = new Vector3(_transfGO.position.x, _transfGO.position.y, z.IsNone ? _startPos.z : z.Value);
                }
            }
			//При отпускании объекта
            if(InputMT.GetMouseButtonUp(0))
            {
				//Движение к начальной позиции
				if(moveToStart.Value)
				{
					_transfGO.localScale = new Vector3(scale.x, scale.y, scale.z);
					RM_MoveToBySpeed mover = _transfGO.gameObject.AddComponent<RM_MoveToBySpeed>();
					if(resetZ.Value)
					{
						mover.zForReset = _startPos.z;
						mover.destinationPosition = new Vector3(_startPos.x, _startPos.y, z.Value);
					}
					else
					{
						mover.destinationPosition = _startPos;
					}
					if(useIntermediatePositions.Value)
					{
						mover.useIntermediatePositions = true;
						mover.intermediatePositions = new Vector3[intermediatePositions.Length];
						for (int i = 0; i < intermediatePositions.Length; i++)
						{
							mover.intermediatePositions[i] = intermediatePositions[i].Value;
						}
					}
					mover.speed = speed.Value;
					mover.resetZAtTheEnd = resetZ.Value;
					mover.finishEvent = FinishMoveToStartEvent != null? (FinishMoveToStartEvent.Name.Length > 0 ? FinishMoveToStartEvent.Name : "") : "";
				}
				else
                	if(resetZ.Value)
						_transfGO.position = new Vector3(_transfGO.position.x, _transfGO.position.y, _startPos.z);

                if (onMouseUp.Name.Length > 0)
                    Fsm.Event(onMouseUp);
            }
        }
		public override void OnExit ()
		{
            if(ResetScale.Value == true)
			    _transfGO.localScale = new Vector3(scale.x, scale.y, scale.z);
		}
    }
}