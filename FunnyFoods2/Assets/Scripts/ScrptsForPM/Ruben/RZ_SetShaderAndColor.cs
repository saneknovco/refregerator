﻿using UnityEngine;

namespace HutongGames.PlayMaker.Actions
{
    [ActionCategory("FF2_Ruben")]
    public class RZ_SetShaderAndColor : FsmStateAction
    {
        public FsmOwnerDefault owner;
        public FsmString shaderName;
        public FsmFloat R, G, B, A;
        public FsmBool isRecursive = true;
		public FsmBool everyFrame = false;
        Transform tr;
        void SetShaderAndColor(Transform trans)
        {
            Renderer rend = trans.GetComponent<MeshRenderer>();
            if (rend != null)
            {
                if (rend.material.color != null)
                {
					if(!shaderName.IsNone && shaderName.Value != null && shaderName.Value.Length > 0)
                    	rend.material.shader = Shader.Find(shaderName.Value);
                    rend.material.color = new Color(R.Value, G.Value, B.Value, A.Value);
                }
            }
        }
        void RecursiveSetShaderAndColor(Transform trans)
        {
            SetShaderAndColor(trans);
            for (int i = 0; i < trans.transform.childCount; i++)
            {
                SetShaderAndColor(trans.GetChild(i));
                RecursiveSetShaderAndColor(trans.GetChild(i));
            }
        }
        public override void OnEnter()
        {
            tr = Fsm.GetOwnerDefaultTarget(owner).transform;
            if (R.Value > 1) R.Value = 1f;
            if (R.Value < 0) R.Value = 0f;
            if (G.Value > 1) G.Value = 1f;
            if (G.Value < 0) G.Value = 0f;
            if (B.Value > 1) B.Value = 1f;
            if (B.Value < 0) B.Value = 0f;
            if (A.Value > 1) A.Value = 1f;
            if (A.Value < 0) A.Value = 0f;
            if(isRecursive.Value) RecursiveSetShaderAndColor(tr);
            else SetShaderAndColor(tr);
            if(!everyFrame.Value) Finish();
        }

		public override void OnUpdate ()
		{
			if(isRecursive.Value) RecursiveSetShaderAndColor(tr);
			else SetShaderAndColor(tr);
		}
    }
}