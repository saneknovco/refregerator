﻿using UnityEngine;

namespace HutongGames.PlayMaker.Actions
{
    [ActionCategory("FF2_Ruben")]
    [Tooltip("Delays a State from finishing by the random time, beetwen two numbers. NOTE: Other actions continue, but FINISHED can't happen before Time.")]
	public class RZ_RandomWait : FsmStateAction
	{
		[RequiredField]
		public FsmFloat fromTime;
		[RequiredField]
		public FsmFloat toTime;
		public FsmEvent finishEvent;
		public bool realTime;
		
		private float startTime;
		private float timer;
		private float randTime;
		public override void Reset()
		{
			fromTime = 0f;
			toTime = 1f;
			finishEvent = null;
			realTime = false;
		}
		
		public override void OnEnter()
		{
			if (fromTime.Value < 0 || fromTime.Value > toTime.Value)
			{
				Fsm.Event(finishEvent);
				Finish();
				return;
			}
			
			startTime = FsmTime.RealtimeSinceStartup;
			randTime = Random.Range(fromTime.Value, toTime.Value);
            if (randTime == 0)
            {
                Fsm.Event(finishEvent);
                Finish();
                return;
            }
			timer = 0f;
		}
		
		public override void OnUpdate()
		{
			// update time
			
			if (realTime)
			{
				timer = FsmTime.RealtimeSinceStartup - startTime;
			}
			else
			{
				timer += Time.deltaTime;
			}
			
			if (timer >= randTime)
			{
				Finish();
				if (finishEvent != null)
				{
					Fsm.Event(finishEvent);
				}
			}
		}
		
	}
}
