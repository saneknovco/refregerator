﻿using UnityEngine;
using System.Collections;
namespace HutongGames.PlayMaker.Actions
{
	public class SetColor : FsmStateAction 
	{
		public FsmOwnerDefault obj;
		public FsmColor color;
		public FsmBool everyFrame;

		GameObject _go;

		void DoSetColor(GameObject go, Color color)
		{
			MeshRenderer renderer;
			if ((renderer = go.GetComponent<MeshRenderer> ()) != null) 
			{
				if (renderer.material.HasProperty ("_Color"))
				{
					renderer.material.color = color;
				}
			}
		}

		public override void OnEnter ()
		{
			_go = Fsm.GetOwnerDefaultTarget (obj);
			DoSetColor (_go, color.Value);
			if (!everyFrame.Value)
				Finish ();
		}

		public override void OnUpdate ()
		{
			DoSetColor (_go, color.Value);
		}
	}
}