﻿using UnityEngine;
using System.Collections;

namespace HutongGames.PlayMaker.Actions
{
	public class RZ_ConvertColorToFloat : FsmStateAction 
	{
		[ActionSection("Input color")]
		public FsmColor Color;
		[ActionSection("Output float")]
		public FsmFloat R = new FsmFloat() { UseVariable = true };
		public FsmFloat G = new FsmFloat() { UseVariable = true };
		public FsmFloat B = new FsmFloat() { UseVariable = true };
		public FsmFloat A = new FsmFloat() { UseVariable = true };

		public override void OnEnter ()
		{
			R.Value = Color.Value.r;
			G.Value = Color.Value.g;
			B.Value = Color.Value.b;
			A.Value = Color.Value.a;
			Finish();
		}
	}
}