﻿using UnityEngine;

namespace HutongGames.PlayMaker.Actions
{
    [ActionCategory("FF2_Ruslan")]
    public class RZ_SetGOTemplate : FsmStateAction
    {
        public FsmOwnerDefault GameObject;
        public FsmTemplate template;

        public override void OnEnter()
        {
            GameObject go = Fsm.GetOwnerDefaultTarget(GameObject);
            try
            {
                var fsm = go.GetComponent<PlayMakerFSM>();
                fsm.SetFsmTemplate(template);
                Finish();
            }
            catch
            {
                Debug.LogError("Нету преймейкера или что то другое");
            }
        }
    }
}