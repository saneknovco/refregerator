﻿using UnityEngine;

namespace HutongGames.PlayMaker.Actions
{
    [ActionCategory("FF2_Ruslan")]
    public class RZ_ResetPlayingAnimation : FsmStateAction
    {
        public FsmOwnerDefault [] gameObject;
        public override void OnEnter()
        {
            for (int i = 0; i < gameObject.Length; i++)
            {
                GameObject go = Fsm.GetOwnerDefaultTarget(gameObject[i]);
                ResetAnimation(go);
            }
            Finish();
        }

        public void ResetAnimation(GameObject go)
        {
            Animation animation;
            if((animation = go.GetComponent<Animation>()) != null)
            foreach (AnimationState anim in animation)
            {
                string name = anim.name;
                if (animation.IsPlaying(name))
                {
                    animation[name].time = 0;
                    animation[name].enabled = true;
                    animation.Sample();
                    animation[name].enabled = false;
                }
            }
        }
    }
}
