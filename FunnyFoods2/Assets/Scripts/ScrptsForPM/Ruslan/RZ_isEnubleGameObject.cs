﻿using UnityEngine;
using System.Collections;

namespace HutongGames.PlayMaker.Actions
{
    [ActionCategory("FF2_Ruslan")]
    public class RZ_isEnubleGameObject : FsmStateAction
    {
        [RequiredField]
        public FsmOwnerDefault gameObject;
        public FsmBool everyFrame, isEnable;
        GameObject go;

        public override void OnEnter()
        {
            go = Fsm.GetOwnerDefaultTarget(gameObject);
            isEnable.Value = go.activeSelf;
            if (!everyFrame.Value)
                Finish();
        }
        public override void OnUpdate()
        {
            isEnable.Value = go.activeSelf;
        }
    }
}
