﻿using UnityEngine;
using System.Collections;

namespace HutongGames.PlayMaker.Actions
{
    public class RZ_SendEventToParent : FsmStateAction
    {
        public FsmOwnerDefault gameObject;
        public FsmEvent sendEvent;
        public FsmInt parentLevel;
        public FsmBool recursive;

        GameObject GO, parentGO;

        public override void OnEnter()
        {
            GO = Fsm.GetOwnerDefaultTarget(gameObject);
            if (parentLevel.Value > 1)
                for (int i = 0; i < parentLevel.Value; i++)
                {
                    if (GO.transform.parent)
                    {
                        parentGO = GO.transform.parent.gameObject;
                        GO = parentGO;
                    }
                    if (recursive.Value)
                        if(GO.GetComponent<PlayMakerFSM>())
                            GO.GetComponent<PlayMakerFSM>().SendEvent(sendEvent.Name);
                }
            else
            {
                if (GO.GetComponent<PlayMakerFSM>())
                    GO.transform.parent.GetComponent<PlayMakerFSM>().SendEvent(sendEvent.Name);
                Finish();
            }
            if (!recursive.Value)
                if (GO.GetComponent<PlayMakerFSM>())
                    GO.GetComponent<PlayMakerFSM>().SendEvent(sendEvent.Name);
            Finish();
        }
    }
}