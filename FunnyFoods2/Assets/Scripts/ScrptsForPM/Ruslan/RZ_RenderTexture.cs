﻿using UnityEngine;
using System.Collections;


namespace HutongGames.PlayMaker.Actions
{

    public class RZ_RenderTexture : FsmStateAction
    {
        public FsmOwnerDefault gameObject;
        public FsmGameObject TexParent;
        public FsmMaterial mat;
        GameObject go, GO;
        Bounds b;

        void GetBounds(GameObject obj)
        {
            if (obj.GetComponent<Renderer>())
            {
                Bounds bound = obj.GetComponent<Renderer>().bounds;

                if (b.max.x < bound.max.x)
                    b.SetMinMax(b.min, new Vector3(bound.max.x, b.max.y, b.max.z));

                if (b.min.x > bound.min.x)
                    b.SetMinMax(new Vector3(bound.min.x, b.min.y, b.min.z), b.max);

                if (b.max.y < bound.max.y)
                    b.SetMinMax(b.min, new Vector3(b.max.x, bound.max.y, b.max.z));

                if (b.min.y > bound.min.y)
                    b.SetMinMax(new Vector3(b.min.x, bound.min.y, b.min.z), b.max);
            }
        }
        void RecursiveGetBounds(GameObject obj)
        {
            GetBounds(obj);
            for (int i = 0; i < obj.transform.childCount; i++)
                RecursiveGetBounds(obj.transform.GetChild(i).gameObject);
        }

        public override void OnEnter()
        {
            GO = Fsm.GetOwnerDefaultTarget(gameObject);
            b = new Bounds();
            b.min = GO.transform.position;
            b.max = GO.transform.position;
            RecursiveGetBounds(GO.transform.gameObject);
            go = new GameObject();
            go.name = "rendCamFor" + GO.transform.name + Random.Range(-9000, 9000).ToString();
            go.AddComponent<Camera>();
            go.layer = 10;
            go.transform.position = b.center + new Vector3(0, 0, -500);
            float x = (b.max - b.min).x;
            float y = (b.max - b.min).y;
            Camera rendCam = go.GetComponent<Camera>();
            rendCam.orthographic = true;
            rendCam.orthographicSize = y / 2;
            rendCam.aspect = x / y;
            rendCam.backgroundColor = new Color(0, 0, 0, 0);
            rendCam.cullingMask = 1024;
            rendCam.clearFlags = CameraClearFlags.Color;
            rendCam.nearClipPlane = 300;
            rendCam.farClipPlane = 100000;

            RenderTexture rt = new RenderTexture((int)x, (int)y, 16, RenderTextureFormat.Default);
            rt.name = "NewRenTex" + GO.transform.name + Random.Range(-9000, 9000).ToString();
            rendCam.targetTexture = rt;
            rendCam.Render();
            rt.Create();

            Texture tex = rt;

            GameObject goWithTex = new GameObject();
            goWithTex.AddComponent<MeshFilter>();
            goWithTex.AddComponent<MeshRenderer>();
            goWithTex.transform.parent = GO.transform;
            goWithTex.transform.localPosition = new Vector3(0, 0, -50);
            goWithTex.transform.parent = TexParent.Value.transform;
            goWithTex.name = GO.transform.name + "_REND";
            goWithTex.SetActive(true);
            MeshRenderer mr = goWithTex.GetComponent<MeshRenderer>();

            mr.material = new Material(mat.Value);
            mr.material.mainTexture = tex;

            MeshFilter mf = goWithTex.GetComponent<MeshFilter>();
            Mesh m = new Mesh();
            m.name = tex.name + "_Mesh";

            Vector3[] verticles;
            Vector2[] uv;
            Vector2 meshSize = new Vector2(x, y);
            int[] triangles;
            uv = new Vector2[] { new Vector2(0, 0), new Vector2(0, 1), new Vector2(1, 1), new Vector2(1, 0) };
            verticles = new Vector3[] { -meshSize / 2, new Vector2(-meshSize.x / 2, meshSize.y / 2), meshSize / 2, new Vector2(meshSize.x / 2, -meshSize.y / 2) };
            for (int i = 0; i < 4; i++)
                verticles[i] += b.center - GO.transform.position;
            triangles = new int[] { 0, 1, 2, 0, 2, 3 };

            m.vertices = verticles;
            m.uv = uv;
            m.triangles = triangles;
            mf.mesh = m;
            go.SetActive(false);
            go.transform.parent = Camera.main.transform;
            Finish();
        }
    }
}
