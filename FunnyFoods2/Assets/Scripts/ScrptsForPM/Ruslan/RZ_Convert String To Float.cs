﻿// (c) Copyright HutongGames, LLC 2010-2013. All rights reserved.

using UnityEngine;

namespace HutongGames.PlayMaker.Actions
{
    [ActionCategory("FF2_Ruslan")]
	[Tooltip("Converts an String value to an Float value.")]
	public class RZ_ConvertStringToFloat : FsmStateAction
	{
		[RequiredField]
		[UIHint(UIHint.Variable)]
		[Tooltip("The String variable to convert to an float.")]
		public FsmString stringVariable;
		
		[RequiredField]
		[UIHint(UIHint.Variable)]
		[Tooltip("Store the result in an Float variable.")]
		public FsmFloat floatVariable;
		
		[Tooltip("Repeat every frame. Useful if the String variable is changing.")]
		public bool everyFrame;
		
		public override void Reset()
		{
			floatVariable = null;
			stringVariable = null;
			everyFrame = false;
		}
		
		public override void OnEnter()
		{
			DoConvertStringToFloat();
			
			if (!everyFrame)
				Finish();
		}
		
		public override void OnUpdate()
		{
			DoConvertStringToFloat();
		}
		
		void DoConvertStringToFloat()
		{
			floatVariable.Value = float.Parse(stringVariable.Value);
		}
	}
}