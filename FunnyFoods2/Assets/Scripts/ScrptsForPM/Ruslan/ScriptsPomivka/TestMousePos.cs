﻿using UnityEngine;
using System.Collections;

namespace HutongGames.PlayMaker.Actions
{
    public class TestMousePos : FsmStateAction
    {
        public FsmOwnerDefault gameObject;
        public FsmFloat ContX, MouseX, MouseY;
        public FsmVector3 ResPos;
        float gusakRad, deltaYLittle, deltaYBig, StartY, X, rad, resY;
        Vector3 center;

        public override void OnEnter()
        {
 //           rad = Fsm.GameObject.gameObject.GetComponent<SphereCollider>().radius;
            center = GameObject.Find("GUSAK").transform.TransformPoint(GameObject.Find("GUSAK").GetComponent<SphereCollider>().center);
            gusakRad = GameObject.Find("GUSAK").GetComponent<SphereCollider>().radius;
        }

        public override void OnUpdate()
        {
            Vector3 posMouse = new Vector3(MouseX.Value, MouseY.Value, 580);
            Vector2 vecMouse_Gusac = new Vector2(-posMouse.x + center.x, -posMouse.y + center.y);
            float pointInRad = gusakRad / vecMouse_Gusac.magnitude;
            if (pointInRad >= 1)
            {
                ResPos.Value = new Vector3(-vecMouse_Gusac.x * pointInRad + center.x, -vecMouse_Gusac.y* pointInRad + center.y, 580);
            }
            else
                ResPos.Value = new Vector3(MouseX.Value, MouseY.Value, 580);
                    //Vector3 centerRes = new Vector3(center.x, center.y, 0);
            //Vector3 thisCenterRes = new Vector3(MouseX.Value, MouseY.Value, 0);
            //deltaYBig = Mathf.Sqrt(Mathf.Pow(gusakRad / 1.5f + rad, 2) - Mathf.Pow((Mathf.Abs(MouseX.Value) - Mathf.Abs(center.x)) * 1.7f, 2));
            //deltaYLittle = Mathf.Sqrt(Mathf.Pow(Vector3.Distance(centerRes, thisCenterRes), 2) - Mathf.Pow((Mathf.Abs(MouseX.Value) - Mathf.Abs(center.x)), 2));

            //if (deltaYLittle < deltaYBig)
            //{
            //    resY = MouseY.Value - (deltaYBig - deltaYLittle);
            //    ResPos.Value = new Vector3(MouseX.Value, resY, 580);
            //    Debug.Log("Y+ " + deltaYBig);
            //    Debug.Log("Y- " + deltaYLittle);
            //}
            //else
            //{
            //    ResPos.Value = new Vector3(MouseX.Value, MouseY.Value, 580);
            //}
            }
    }
}
