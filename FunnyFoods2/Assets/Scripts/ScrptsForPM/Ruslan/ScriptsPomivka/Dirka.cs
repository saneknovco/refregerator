﻿using UnityEngine;
using System.Collections;

public class Dirka : MonoBehaviour
{
    public GameObject PS;
    ParticleSystem part;
    ParticleSystem.Particle[] p;
    GameObject col = null;

    void Start()
    {
            part = PS.GetComponent<ParticleSystem>();
            p = new ParticleSystem.Particle[part.particleCount];
    }
        

    void OnParticleCollision(GameObject collider)
    {
        col = collider;
    }

    void Update()
    {
        if (col)
        {
            part.GetParticles(p);
            for (int i = 0; i < p.Length; i++)
                if (Mathf.Abs(PS.transform.TransformPoint(p[i].position).y) - Mathf.Abs(gameObject.transform.position.y) < 0.0001)
                    part.SetParticles(p, p.Length);
        }
        col = null;
    }
}
