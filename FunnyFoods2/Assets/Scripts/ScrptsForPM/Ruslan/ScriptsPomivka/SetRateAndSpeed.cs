﻿using UnityEngine;
using System.Collections;

namespace HutongGames.PlayMaker.Actions
{
    public class SetRateAndSpeed : FsmStateAction
    {
        public FsmOwnerDefault gameObject;
        public FsmFloat emissionRate, size;
        ParticleSystem part;

        public override void OnEnter()
        {
            part = Fsm.GetOwnerDefaultTarget(gameObject).GetComponent<ParticleSystem>();
        }

        public override void OnUpdate()
        {
            part.emissionRate = emissionRate.Value;
            part.startSize = size.Value;
        }
    }
}