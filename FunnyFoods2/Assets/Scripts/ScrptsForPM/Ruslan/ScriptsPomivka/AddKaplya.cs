﻿using UnityEngine;
using System.Collections;
using System;

public class AddKaplya : MonoBehaviour
{
    int Counter;
    GameObject coll;
    AudioClip moet;
   // public GameObject ventil; 
    AudioSource VentilSource;
//    public GameObject MoetGO;
    public GameObject counter;
    int prevCounter = 0;
    GameObject gryaz;
    
    void Start()
    {
  //      VentilSource = ventil.GetComponent<AudioSource>();
        moet = Resources.Load("Sounds/VODA_MOET") as AudioClip;
    }

    void OnParticleCollision(GameObject collider)
    {

        if (collider.name != "Plane")
        {
            Debug.Log(collider.name);
			coll = collider;
            Counter++;
//            VentilSource.enabled = false;
//            if (!MoetGO.GetComponent<AudioSource>().enabled)
//                MoetGO.GetComponent<AudioSource>().enabled = true;
        }
        else
        {

        }

    }
    void Update()
    {
        if (prevCounter == Counter)
        {
//            MoetGO.GetComponent<AudioSource>().enabled = false;
//            VentilSource.enabled = true;
			return;
        }
        prevCounter = Counter;
        if (coll)
        {
            float CounterF = Counter;

			try{
            if (coll.gameObject.transform.GetChild(0).GetChild(0).GetChild(0).gameObject)
            	{
                	gryaz = coll.gameObject.transform.GetChild(0).GetChild(0).GetChild(0).gameObject;
                	Color color = gryaz.GetComponent<Renderer>().material.color;
					color.a = (color.a - 0.01f);
					if (color.a <= 0.2f) {
						coll.gameObject.transform.GetChild(0).gameObject.GetComponent<PlayMakerFSM>().SendEvent("AN/WSH/POMIVKAFINISH");
						PlayMakerFSM.BroadcastEvent("AN/WSH/CLEAN");
						Destroy (gryaz);
					}
					gryaz.GetComponent<Renderer>().material.color = color;
                	coll = null;
            	}
			}
			catch(Exception e){
                Debug.Log("pechkkk");
				return;
			}
        }
    }
}
