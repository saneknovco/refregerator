﻿using UnityEngine;
using System.Collections;

namespace HutongGames.PlayMaker.Actions
{
    public class RZ_SetVector2 : FsmStateAction
    {

        public FsmVector2 Vector2Variable;
        public FsmVector2 Vector2Value;
        public FsmFloat x, y;
        public override void OnEnter()
        {
            Vector2Variable.Value = Vector2Value.Value;
            if(x.Value != null && !x.IsNone)
                Vector2Variable.Value = new Vector2(x.Value, Vector2Variable.Value.y);
            if (y.Value != null && !y.IsNone)
                Vector2Variable.Value = new Vector2(Vector2Variable.Value.x, y.Value);

            Finish();
        }
    }
}