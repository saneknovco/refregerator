﻿// (c) Copyright HutongGames, LLC 2010-2013. All rights reserved.

using UnityEngine;

namespace HutongGames.PlayMaker.Actions
{
    [ActionCategory("FF2_Ruslan")]
    [Tooltip("Sets a Float variable to its degree value.")]
    public class RZ_FloatRound : FsmStateAction
    {
        [RequiredField]
        [UIHint(UIHint.Variable)]
        [Tooltip("The Float variable.")]
        public FsmFloat floatVariable;

        [Tooltip("Знак после запятой, до которого нужно округлить.")]
        public FsmFloat degreeVariable;

        [Tooltip("Repeat every frame. Useful if the Float variable is changing.")]
        public bool everyFrame;

        public override void Reset()
        {
            floatVariable = null;
            everyFrame = false;
        }

        public override void OnEnter()
        {
            DoFloatAbs();

            if (!everyFrame)
            {
                Finish();
            }
        }

        public override void OnUpdate()
        {
            DoFloatAbs();
        }

        void DoFloatAbs()
        {
            int temp = 1;
            if (degreeVariable.Value == 0)
                floatVariable.Value = Mathf.Round(floatVariable.Value);
            else
            {
                for (int i = 0; i < degreeVariable.Value; i++)
                {
                    floatVariable.Value *= 10;
                    temp *= 10;
                }
                floatVariable.Value = Mathf.Round(floatVariable.Value)/temp;
            }
                
        }
    }
}