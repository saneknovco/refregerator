﻿using UnityEngine;

namespace HutongGames.PlayMaker.Actions
{
    [ActionCategory("FF2_Ruslan")]
    public class RZ_AppareDissappareGO : FsmStateAction
    {
        public FsmOwnerDefault [] objs;
        public FsmFloat timeToFinish;
        public FsmBool appare, dissappare, recursive, alpha;
        public FsmEvent finishEvent;
        float i;
        float currentI;
        int childCount;
        int updateCount;
        GameObject child;

        public override void OnEnter()
        {
            
            i = 0; currentI = 0;
            if ((appare.Value && dissappare.Value) || (!appare.Value && !dissappare.Value))
                Debug.LogError("Ты уже как то определись!");
        }
        public override void OnUpdate()
        {
            updateCount++;
            for (int i = 0; i < objs.Length; i++)
            {
                if (recursive.Value)
                    RecursiveSetAlpha(Fsm.GetOwnerDefaultTarget(objs[i]));
                else
                    SetAlpha(Fsm.GetOwnerDefaultTarget(objs[i]));
            }
        }

        public void RecursiveSetAlpha(GameObject gameObj)
        {
            SetAlpha(gameObj);
            for (int j = 0; j < gameObj.transform.childCount; j++)
            {
                SetAlpha(gameObj.transform.GetChild(j).gameObject);
                RecursiveSetAlpha(gameObj.transform.GetChild(j).gameObject);
            }
        }

        private void SetAlpha(GameObject GameObj)
        {
            Renderer rend = GameObj.GetComponent<MeshRenderer>();
            if (rend != null)
            {
                if (rend.material.color != null)
                {
                    if (updateCount == 1)
                        childCount++;
                    else
                    {
                        GameObj.GetComponent<Renderer>().material.shader = Shader.Find(string.Format("BiniUber/RGB{0}A/TC/Blend: Alpha", alpha.Value ? "+" : ""));
                        if (dissappare.Value == true)
                        {
                            i += Time.deltaTime;
                            currentI = 1 - i / (timeToFinish.Value * childCount);
                            if (currentI < 0) currentI = 0;
                            if (currentI <= 0)
                            {
                                GameObj.GetComponent<Renderer>().material.color = new Color(1, 1, 1, 0);
                                Finish();
                                if (finishEvent != null)
                                    Fsm.Event(finishEvent);
                            }
                            Color color = GameObj.GetComponent<Renderer>().material.color;
                            color.a = currentI;
                            GameObj.GetComponent<Renderer>().material.color = color;
                        }
                        if (appare.Value == true)
                        {
                            i += Time.deltaTime;
                            currentI = i / (timeToFinish.Value * childCount);
                            if (currentI >= 1)
                            {
                                GameObj.GetComponent<Renderer>().material.color = new Color(1, 1, 1, 1);
                                Finish();
                                if (finishEvent != null)
                                    Fsm.Event(finishEvent);
                            }
                            Color color = GameObj.GetComponent<Renderer>().material.color;
                            color.a = currentI;
                            GameObj.GetComponent<Renderer>().material.color = color;
                        }
                    }
                }
            }
        }
    }
}
