﻿
using UnityEngine;
using System.Collections;

namespace HutongGames.PlayMaker.Actions
{
    public class RN_PAR_Animation : FsmStateAction
    {
        public FsmGameObject gameObject;
        public FsmString animName;
        
        public override void OnEnter()
        {
            animName.Value = gameObject.Value.GetComponent<Animation>().clip.name;
            Finish();
        }
    }
}
