﻿
using UnityEngine;
using System.Collections;

namespace HutongGames.PlayMaker.Actions
{
    public class RN_PAR_BoxControl : FsmStateAction
    {
        public FsmGameObject food;
        public FsmFloat index;

        public override void OnEnter()
        {
            BoxCollider box = food.Value.GetComponent<BoxCollider>();
            if (box.size.x > box.size.y)
                food.Value.transform.localScale = new Vector3(index.Value / box.size.x, index.Value / box.size.x, 0.1f);
            else
                food.Value.transform.localScale = new Vector3(index.Value / box.size.y, index.Value / box.size.y, 0.1f);
            food.Value.transform.localPosition = new Vector3(0, (box.size.y * food.Value.transform.localScale.y) / 2, 0);
            Object.Destroy(box);
            Finish();
        }
    }
}
