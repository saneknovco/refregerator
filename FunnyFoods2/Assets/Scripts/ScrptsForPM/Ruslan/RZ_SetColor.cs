﻿using UnityEngine;
using System.Collections;

namespace HutongGames.PlayMaker.Actions
{
    [ActionCategory("FF2_Ruslan")]
    public class RZ_SetColor : FsmStateAction
    {
        public FsmOwnerDefault GO;
        public bool FromSelfColor;
        public FsmColor FromColor;
        public FsmColor ToColor;
        [HasFloatSlider(0, 1)]
        public FsmFloat TextureInfluence = 1;
        public FsmFloat time;

        float fromR, fromG, fromB, fromA, toR, toG, toB, toA, currentTime, R, G, B, A;
        Material material;
        Renderer renderer;
        public FsmEvent finishEvent;

        
        public override void OnEnter()
        {
            currentTime = 0;
            GameObject gameObject = Fsm.GetOwnerDefaultTarget(GO);
            renderer = gameObject.GetComponent<MeshRenderer>();
            if (renderer)
            {
                material = renderer.material;
                if (material)
                {
                    if (!material.HasProperty("_Color"))
                    {
                        if (TextureInfluence.Value >= 1)
                        {
                            if (material.HasProperty("_MainTex_Alpha"))
                                material = new Material(Shader.Find("BiniUber/RGB+A/TC/Color: Silhouette Alpha"));
                            else
                                material = new Material(Shader.Find("BiniUber/RGBA/TC/Color: Silhouette Alpha"));
                        }
                        else
                        {
                            if (material.HasProperty("_MainTex_Alpha"))
                                material = new Material(Shader.Find("BiniUber/RGB+A/TC/Color: Alpha"));
                            else
                                material = new Material(Shader.Find("BiniUber/RGBA/TC/Color: Alpha"));
                        }
                    }
                    if (TextureInfluence.Value > 1)
                        TextureInfluence = 1;
                    gameObject.GetComponent<Renderer>().material.SetFloat("_OutlineSize", 0);
                    gameObject.GetComponent<Renderer>().material.SetFloat("_TexInfluence", TextureInfluence.Value);
                    gameObject.GetComponent<Renderer>().material.SetVector("_OutlineColor", new Vector4(0, 0, 0, 0));

                    if (FromSelfColor)
                    {
                        fromR = material.color.r;
                        fromG = material.color.g;
                        fromB = material.color.b;
                        fromA = material.color.a;
                    }
                    else
                    {
                        fromR = FromColor.Value.r;
                        fromG = FromColor.Value.g;
                        fromB = FromColor.Value.b;
                        fromA = FromColor.Value.a;
                        material.color = new Color(fromR, fromG, fromB, fromA);
                    }

                    toR = ToColor.Value.r;
                    toG = ToColor.Value.g;
                    toB = ToColor.Value.b;
                    toA = ToColor.Value.a;
                }
                else
                {
                    Debug.LogWarning("No Material!!!");
                    if (finishEvent != null)
                    {
                        Fsm.Event(finishEvent);
                    }
                    Finish();
                }
            }
            else
            {
                Debug.LogWarning("No MeshRenderer!!!");
                if (finishEvent != null)
                {
                    Fsm.Event(finishEvent);
                }
                Finish();
            }

        }

        public override void OnUpdate()
        {
            currentTime += Time.deltaTime;
            float lerpTime = currentTime / time.Value;
            R = Mathf.Lerp(fromR, toR, lerpTime);
            G = Mathf.Lerp(fromG, toG, lerpTime);
            B = Mathf.Lerp(fromB, toB, lerpTime);
            A = Mathf.Lerp(fromA, toA, lerpTime);

            material.color = new Color(R, G, B, A);


            if (lerpTime > 1)
            {
                if (finishEvent != null)
                {
                    Fsm.Event(finishEvent);
                }

                Finish();
            }
        }
        public override void OnExit()
        {
            currentTime = 0;
            time.Value = time.Value;
        }
    }
}