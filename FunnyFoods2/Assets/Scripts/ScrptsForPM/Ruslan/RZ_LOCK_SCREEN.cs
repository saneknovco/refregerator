﻿using UnityEngine;
using System.Collections;


namespace HutongGames.PlayMaker.Actions
{
    [ActionCategory("FF2_Ruslan")]
    public class RZ_LOCK_SCREEN : FsmStateAction
    {
        public StatusEnum Status;
        public override void OnEnter()
        {
            switch (Status)
            {
                case StatusEnum.Lock:
                    {
                        Camera.main.GetComponent<BoxCollider>().enabled = true;
                        break;
                    }
                case StatusEnum.Unlock:
                    {
                        Camera.main.GetComponent<BoxCollider>().enabled = false;
                        break;
                    }
            }
            Finish();
        }
        public enum StatusEnum { Lock, Unlock };
    }
}
