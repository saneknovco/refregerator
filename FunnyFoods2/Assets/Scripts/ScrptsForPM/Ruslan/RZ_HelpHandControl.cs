﻿using UnityEngine;
using System.Collections;

namespace HutongGames.PlayMaker.Actions
{
    [ActionCategory("Rusben")]
    public class RZ_HelpHandControl : FsmStateAction
    {
        [ActionSection("Game Objects")]
        [RequiredField]
        public FsmOwnerDefault handControl;
        public FsmGameObject OriginalGO;
        [RequiredField]
        public FsmGameObject EffectGO;
        [ActionSection("Transform")]
        public FsmVector3 moveTo;
        public FsmVector3 rotateTo;
        public FsmVector3 scaleTo;
        [ActionSection("Appearance Settings")]
        [RequiredField]
        public FsmFloat appearanceTime;
        [RequiredField]
        public FsmBool recursive;
        public FsmBool slowTime;
        [ActionSection("Move Settings")]
        public FsmFloat moveSpeed;
        public FsmBool onlyTap;
        [ActionSection("Other Settings")]
        [RequiredField]
        public FsmBool isAnimationPlayingTest;
        public FsmFloat startWait;
        public FsmFloat loopWait;
        [UIHint(UIHint.ScriptComponent)]
        [Tooltip("The name of the Components to destroy.")]
        public FsmString[] destroyComponents;
        public FsmEvent wakeUpEvent;
        public FsmEvent finishEvent;

        GameObject CloneGO;
        float i, currentI, currentWait;
        int childCount, updateCount;
        Vector3 startScale, startRotation, cloneStartScale, cloneScale, startPosition, direction, rotationForPickups;
        GameObject GO;
        string Action, thisSceneName;

        public override void OnEnter()
        {
            i = currentI = currentWait = 0;
            childCount = 1;
            if (currentWait != startWait.Value && currentWait != loopWait.Value)
                currentWait = startWait.Value;
            else
                if (currentWait == startWait.Value)
                currentWait = loopWait.Value;
            Action = "Wait";
            GO = Fsm.GetOwnerDefaultTarget(handControl);
            thisSceneName = UnityEngine.SceneManagement.SceneManager.GetActiveScene().name;
        }

        public override void OnUpdate()
        {
            if (OriginalGO.Value == null && appearanceTime.Value != 0)
                CustomFinish();

            updateCount++;

            if (recursive.Value && Action != "Move" && Action != "WaitEffect" && Action != "Wait" && Action != "isAnimationPlaying")
                RecursiveSetAlphaScaleRotation(GO);
            else
                SetAlphaScaleRotation(GO);

            if (thisSceneName != "PHOTOROBOT" && thisSceneName != "PAROCHKI")
                if (Action == "FinishAppearanceScaleRotation")
                    OriginalGO.Value.RecursiveSetAlpha(1 - currentI);
        }

        public void CustomFinish()
        {
            Finish();
            if (finishEvent != null)
                Fsm.Event(finishEvent);
        }

        public void RecursiveSetAlphaScaleRotation(GameObject go)
        {
            SetAlphaScaleRotation(go);
            for (int j = 0; j < go.transform.childCount; j++)
                RecursiveSetAlphaScaleRotation(go.transform.GetChild(j).gameObject);
        }

        //for vodoprovod
        Vector3 globalPos = new Vector3();
        private void SetAlphaScaleRotation(GameObject go)
        {
            switch (Action)
            {
                #region Wait
                case "Wait":
                    {
                        if (slowTime.Value == true)
                            Time.timeScale = 1;
                        if (loopWait.Value != 0)
                        {
                            i += Time.deltaTime;
                            if (i >= currentWait)
                            {
                                Action = "isAnimationPlaying";
                                i = 0;
                            }
                        }
                        else
                            Action = "isAnimationPlaying";
                        break;
                    }
                #endregion 
                #region IsAnimationPlaying
                case "isAnimationPlaying":
                    {
                        if (thisSceneName != "MainScene")
                        {
                            if (appearanceTime.Value != 0)
                                GO.transform.position = new Vector3(OriginalGO.Value.transform.position.x, OriginalGO.Value.transform.position.y, moveTo.Value.z);
                            if (thisSceneName == "VODOPROVOD_SC")
                            {
                                GameObject center = OriginalGO.Value.FindChild("center");
                                if (appearanceTime.Value != 0)
                                    GO.transform.position = new Vector3(center.transform.position.x, center.transform.position.y, moveTo.Value.z);
                            }
                            startScale = GO.transform.localScale;
                            startRotation = GO.transform.localEulerAngles;
                            startPosition = GO.transform.position;
                            direction = (moveTo.Value - startPosition) / (moveTo.Value - startPosition).magnitude;
                        }

                        if (isAnimationPlayingTest.Value)
                        {
                            if (thisSceneName != "VODOPROVOD_SC")
                            {
                                if (!OriginalGO.Value.IsAnimationPlaying(isRecursive: true))
                                    Action = !onlyTap.Value ? "StartParams" : "StartAppearanceScaleRotation";
                            }
                            else
                                if (!OriginalGO.Value.transform.parent.gameObject.IsAnimationPlaying(isRecursive: false))
                                Action = !onlyTap.Value ? "StartParams" : "StartAppearanceScaleRotation";
                        }
                        else
                            Action = !onlyTap.Value ? "StartParams" : "StartAppearanceScaleRotation";
                        break;
                    }
                #endregion
                #region StartParams

                case "StartParams":
                    {
                        if (appearanceTime.Value > 0 && !onlyTap.Value)
                        {
                            RZ_CreateObject create = new RZ_CreateObject();
                            create.DontUseFinish = true;
                            create.storeObject = CloneGO;
                            create.newObjectCount = 1;
                            create.parentObject = null;
                            create.position = null;
                            create.LocalPosition = false;
                            create.resetLocalPosition = false;
                            create.resetLocalRotation = false;
                            create.tag = "Untagged";
                            create.useUnloadAssets = false;
                            create.getSourceObjectName = true;
                            create.getSourceObjectRotation = true;
                            create.gameObject = OriginalGO.Value;
                            create.OnEnter();
                            CloneGO = create.storeObject.Value;
                            CloneGO.transform.parent = GO.transform;
                            ///RUBASYA`S KOSYAK
							if (thisSceneName != "PryatkiScene" && thisSceneName != "VODOPROVOD_SC")
                                CloneGO.transform.localPosition = new Vector3(0, 0, 50);
                            if (thisSceneName == "VODOPROVOD_SC")
                            {
                                CloneGO.transform.localPosition = new Vector3(CloneGO.transform.localPosition.x, CloneGO.transform.localPosition.y, 50);
                                globalPos = CloneGO.transform.position;
                            }
                            if (thisSceneName == "PICKUPS")
                                CloneGO.GetComponent<Rigidbody2D>().isKinematic = true;
                            DestroyComponents d = new DestroyComponents();
                            d.component = destroyComponents;
                            d.RecursiveDoDestroyComponent(CloneGO);
                            CloneGO.transform.localScale = new Vector3(OriginalGO.Value.transform.lossyScale.x / GO.transform.localScale.x, OriginalGO.Value.transform.lossyScale.y / GO.transform.localScale.y, OriginalGO.Value.transform.lossyScale.z / GO.transform.localScale.z);
                            cloneStartScale = CloneGO.transform.lossyScale;
                            if (thisSceneName == "MainScene" || thisSceneName == "Pomyvka" || thisSceneName == "PomyvkaKontur")
                                Object.Destroy(CloneGO.transform.GetChild(1).gameObject);
                            ///RUBASYA`S KOSYAK START
							if (thisSceneName == "AMBAR_SC")
                                if (CloneGO)
                                    CloneGO.SetGlobalScale(0.48f, 0.48f, 0.1f);
                            ///RUBASYA`S KOSYAK END
                        }
                        if (appearanceTime.Value == 0 && OriginalGO.Value)
                        {
                            if (thisSceneName != "PHOTOROBOT")
                                OriginalGO.Value.RecursiveSetAlpha(1);
                            OriginalGO.Value.SetActive(true);
                            if (wakeUpEvent != null)
                                OriginalGO.Value.RecursiveSendEventToGameObject(wakeUpEvent.Name);
                        }
                        else if (OriginalGO.Value && CloneGO)
                        {
                            OriginalGO.Value.RecursiveResetAnimation();
                            if (thisSceneName != "PHOTOROBOT")
                             OriginalGO.Value.RecursiveSetAlpha(0);
                            if (thisSceneName != "PHOTOROBOT")
                                OriginalGO.Value.SetActive(false);
                            CloneGO.RecursiveSetAlpha(1);
                        }
                        Action = "StartAppearanceScaleRotation";
                        updateCount = 1;
                        if (appearanceTime.Value > 0 && !onlyTap.Value)
                            rotationForPickups = OriginalGO.Value.transform.eulerAngles;
                        if (slowTime.Value == true)
                            Time.timeScale = 0.1f;
                        break;
                    }
                #endregion
                #region StartAppearanceScaleRotation
                case "StartAppearanceScaleRotation":
                    {
                        MeshRenderer rend = go.GetComponent<MeshRenderer>();
                        if (rend != null)
                            if (rend.material.HasProperty("_Color"))
                            {
                                if (updateCount == 1)
                                    childCount++;
                                else
                                {
                                    i += Time.deltaTime / Time.timeScale;
                                    currentI = i / (appearanceTime.Value * childCount);
                                    if (appearanceTime.Value != 0)
                                    {
                                        Color color = rend.material.color;
                                        if (color.a != 1)
                                            color.a = currentI;
                                        rend.material.color = color;

                                        if (thisSceneName == "AMBAR_SC")
                                            CloneGO.SetGlobalScale(0.48f, 0.48f, 0.1f);
                                        GO.transform.localScale = startScale + (i / (appearanceTime.Value * childCount)) * (scaleTo.Value - startScale);
                                        GO.transform.localEulerAngles = startRotation + (i / (appearanceTime.Value * childCount)) * (rotateTo.Value - startRotation);
                                    }
                                    if (CloneGO)
                                    {
                                        if (thisSceneName == "PICKUPS")
                                            CloneGO.transform.eulerAngles = rotationForPickups;
                                        else if (thisSceneName == "PICCA" && GO.name == "HAND_BUTILKA")
                                            CloneGO.transform.eulerAngles = OriginalGO.Value.transform.eulerAngles;
                                        else if (thisSceneName == "PICCA" && GO.name == "HAND_INGRID")
                                            CloneGO.transform.eulerAngles = OriginalGO.Value.transform.eulerAngles;
                                        else
                                            CloneGO.transform.eulerAngles = Vector3.zero;
                                        cloneScale.x = cloneStartScale.x / GO.transform.localScale.x;
                                        cloneScale.y = cloneStartScale.y / GO.transform.localScale.y;
                                        cloneScale.z = cloneStartScale.z;
                                        CloneGO.transform.localScale = cloneScale;
                                        if (thisSceneName == "AMBAR_SC" && CloneGO)
                                            CloneGO.SetGlobalScale(0.48f, 0.48f, 0.1f);
                                        if (thisSceneName == "VODOPROVOD_SC" && CloneGO)
                                            CloneGO.transform.position = globalPos;
                                    }
                                    if (currentI >= 1 || appearanceTime.Value == 0)
                                    {
                                        Action = "WaitEffect";
                                            rend.material.color = new Color(rend.material.color.r, rend.material.color.g, rend.material.color.b, 1);
                                        i = 0;
                                        GO.transform.localScale = scaleTo.Value;
                                        if (thisSceneName == "AMBAR_SC" && CloneGO) CloneGO.SetGlobalScale(0.48f, 0.48f, 0.1f);
                                        GO.transform.localEulerAngles = rotateTo.Value;
                                        if (appearanceTime.Value == 0)
                                        {
                                            Action = "";
                                                rend.material.color = new Color(rend.material.color.r, rend.material.color.g, rend.material.color.b, 0);

                                            if (EffectGO.Value)
                                                EffectGO.Value.SetActive(false);

                                            for (int j = 0; j < GO.transform.childCount; j++)
                                                if (GO.transform.GetChild(j).name != "HAND" && GO.transform.GetChild(j).name != "PS_Wave")
                                                    Object.Destroy(GO.transform.GetChild(j).gameObject);

                                            CustomFinish();
                                        }
                                    }
                                }
                            }
                        break;
                    }
                #endregion
                #region WaitEffect
                case "WaitEffect":
                    {

                        if (EffectGO.Value)
                        {
                            if (appearanceTime.Value != 0)
                                EffectGO.Value.SetActive(true);
                            if (!EffectGO.Value.GetComponent<ParticleSystem>().IsAlive(false))
                            {
                                EffectGO.Value.SetActive(false);
                                Action = "Move";
                                if (onlyTap.Value)
                                    Action = "FinishAppearanceScaleRotation";
                            }
                        }
                        else
                            Action = "Move";
                        break;
                    }
                #endregion
                #region Move
                case "Move":
                    {
                        if (thisSceneName == "PICCA" && GO.name == "HAND_BUTILKA")
                            CloneGO.transform.eulerAngles = OriginalGO.Value.transform.eulerAngles;
                        else if (thisSceneName == "PICCA" && GO.name == "HAND_INGRID")
                        {
                            CloneGO.SetGlobalScale(0.8f,0.8f,0.1f);
                            CloneGO.transform.eulerAngles = OriginalGO.Value.transform.eulerAngles;
                        }
                        else
                            if (CloneGO) CloneGO.transform.eulerAngles = Vector3.zero;

                        GO.transform.position += direction * moveSpeed.Value * Time.deltaTime;
                        if (Time.deltaTime * moveSpeed.Value >= Vector3.Distance(GO.transform.position, moveTo.Value))
                        {
                            Action = "FinishAppearanceScaleRotation";
                            GO.transform.position = moveTo.Value;
                            if (thisSceneName == "VODOPROVOD_SC")
                                globalPos = CloneGO.transform.position;

                            OriginalGO.Value.SetActive(true);
                            if (wakeUpEvent != null)
                                OriginalGO.Value.RecursiveSendEventToGameObject(wakeUpEvent.Name);
                        }
                        break;
                    }
                #endregion
                #region FinishAppearanceScaleRotation
                case "FinishAppearanceScaleRotation":
                    {
                        MeshRenderer rend = go.GetComponent<MeshRenderer>();
                        if (rend != null)
                            if (rend.material.HasProperty("_Color"))
                            {
                                i += Time.deltaTime / Time.timeScale;
                                currentI = 1 - i / (appearanceTime.Value * childCount);
                                Color color = rend.material.color;
                                color.a = currentI;
                                rend.material.color = color;
                                GO.transform.localScale = scaleTo.Value + (i / (appearanceTime.Value * childCount)) * (startScale - scaleTo.Value);
                                if (CloneGO)
                                {
                                    cloneScale.x = cloneStartScale.x / GO.transform.localScale.x;
                                    cloneScale.y = cloneStartScale.y / GO.transform.localScale.y;
                                    cloneScale.z = cloneStartScale.z / GO.transform.localScale.z;
                                    if (thisSceneName == "PICCA" && GO.name == "HAND_BUTILKA")
                                    {
                                        CloneGO.transform.localScale = cloneScale;
                                        CloneGO.transform.eulerAngles = OriginalGO.Value.transform.eulerAngles;
                                    }
                                    else if (thisSceneName == "PICCA" && GO.name == "HAND_INGRID")
                                    {
                                        CloneGO.SetGlobalScale(0.8f, 0.8f, 0.1f);
                                        CloneGO.transform.eulerAngles = OriginalGO.Value.transform.eulerAngles;
                                    }
                                    else
                                        CloneGO.transform.localScale = cloneScale;

                                    if (thisSceneName == "AMBAR_SC" && CloneGO)
                                        CloneGO.SetGlobalScale(0.48f, 0.48f, 0.1f);
                                    if (thisSceneName == "VODOPROVOD_SC" && CloneGO)
                                        CloneGO.transform.position = globalPos;
                                }
                                if (currentI <= 0)
                                {
                                    rend.material.color = new Color(rend.material.color.r, rend.material.color.g, rend.material.color.b, 0);
                                    GO.transform.localScale = startScale;
                                    GO.transform.localEulerAngles = startRotation;
                                    if (CloneGO)
                                    {
                                        CloneGO.transform.parent = null;
                                        if (thisSceneName == "AMBAR_SC" && CloneGO) CloneGO.SetGlobalScale(0.48f, 0.48f, 0.1f);
                                        Object.Destroy(CloneGO);
                                    }
                                    RM_SetAlpha al = new RM_SetAlpha();
                                    al.alpha = 0;
                                    al.RecursiveSetAlpha(GO.transform);
                                    Time.timeScale = 1f;
                                    CustomFinish();
                                }
                            }
                        break;
                    }
                    #endregion
            }
        }
    }
}