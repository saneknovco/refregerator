﻿using UnityEngine;
//using System.Collections;
namespace HutongGames.PlayMaker.Actions
{
    [ActionCategory("FF2_Ruslan")]
	public class RZ_SetShadowPlane : FsmStateAction {
		public FsmOwnerDefault gameObject;
		public FsmGameObject shadowPlane;
		public override void OnEnter() 
		{
			var go = Fsm.GetOwnerDefaultTarget(gameObject);
			var go1 = shadowPlane.Value;
            if (go && go1)
			{
                Shadow2DCaster cast = go.GetComponent<Shadow2DCaster>();
                Shadow2DPlane plane = go1.GetComponent<Shadow2DPlane>();
				Shadow2DPlane planeOld = cast.ShadowPlane;
				if(cast) 
				{
					if(planeOld)
						planeOld.RemoveCaster(cast);
					cast.ShadowPlane = plane;
					plane.AddCaster(cast);
				}
			}
			Finish();
		}
	}	
}  