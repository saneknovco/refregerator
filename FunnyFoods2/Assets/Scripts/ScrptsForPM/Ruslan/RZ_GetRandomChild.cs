﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

namespace HutongGames.PlayMaker.Actions
{
    public class RZ_GetRandomChild : FsmStateAction
    {
        public FsmOwnerDefault gameObject;
        public FsmBool noRepeat;
        public FsmGameObject storeResult;
        GameObject tempGO;
        List<int> tempInt = new List<int>();
        int index;

        public override void OnEnter()
        {
            DoGetRandomChild();
            Finish();
        }

        void DoGetRandomChild()
        {
            
            GameObject go = Fsm.GetOwnerDefaultTarget(gameObject);
            if (go == null) return;
            
            if (go.transform.childCount == 0) return;

            if (noRepeat.Value == true)
            {
                do
                {
                    index = Random.Range(0, go.transform.childCount);
                    tempGO = go.transform.GetChild(index).gameObject;
                }
                while (tempInt.Contains(index));

                storeResult.Value = tempGO;
            }
            else
                tempGO = storeResult.Value = go.transform.GetChild(Random.Range(0, go.transform.childCount)).gameObject;

            tempInt.Add(index);
        }
    }

}

