﻿using UnityEngine;
using System.Collections;

namespace HutongGames.PlayMaker.Actions
{
    public class RZ_SetScreenOrientation : FsmStateAction
    {
        public ScreenOrientation orientation;
        public bool fixOrientation;
        public bool autorotateToLandscape;
        public bool autorotateToPortrait;

        public override void OnEnter()
        {
            if (fixOrientation == true)
                Screen.autorotateToLandscapeLeft = Screen.autorotateToLandscapeRight = Screen.autorotateToPortrait = Screen.autorotateToPortraitUpsideDown = false;

            else if (autorotateToLandscape == true || autorotateToPortrait == true)
            {
                if (autorotateToLandscape == true)
                    Screen.autorotateToLandscapeLeft = Screen.autorotateToLandscapeRight = true;
                if (autorotateToPortrait == true)
                    Screen.autorotateToPortrait = Screen.autorotateToPortraitUpsideDown = true;
            }
            else
                Screen.orientation = orientation;

            Finish();
        }
    }
}