﻿// (c) Copyright HutongGames, LLC 2010-2013. All rights reserved.

using UnityEngine;

namespace HutongGames.PlayMaker.Actions
{
    [ActionCategory("FF2_Ruslan")]
    [Tooltip("Gets a Random Child of a Game Object and his index")]
    public class RZ_GetRandomChildAndIndex : FsmStateAction
    {
        [RequiredField]
        public FsmOwnerDefault gameObject;
        [RequiredField]
        [UIHint(UIHint.Variable)]
        public FsmGameObject storeResult;
        [UIHint(UIHint.Variable)]
        public FsmInt storeResultIndex;

        public override void Reset()
        {
            gameObject = null;
            storeResult = null;
        }

        public override void OnEnter()
        {
            DoGetRandomChild();
            Finish();
        }

        void DoGetRandomChild()
        {
            GameObject go = Fsm.GetOwnerDefaultTarget(gameObject);
            if (go == null) return;

            int childCount = go.transform.childCount;
            if (childCount == 0) return;
            storeResultIndex.Value = Random.Range(0, childCount);
            storeResult.Value = go.transform.GetChild(storeResultIndex.Value).gameObject;
        }
    }
}