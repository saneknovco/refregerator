﻿using UnityEngine;
using System.Collections;

namespace HutongGames.PlayMaker.Actions
{
    public class RN_PHOTOROBOT_BoxControl : FsmStateAction
    {
        public FsmFloat finishX;
        public FsmFloat startX;
        BoxCollider box;
        public override void OnEnter()
        {
            if (box = Owner.GetComponent<BoxCollider>())
            {
                if (startX.Value < finishX.Value)
                    box.center = new Vector3(-Owner.transform.localPosition.x, box.center.y, box.center.z);
                else
                    box.center = new Vector3(-Owner.transform.localPosition.x, box.center.y, box.center.z);
            }
            Finish();
        }
    }
}
