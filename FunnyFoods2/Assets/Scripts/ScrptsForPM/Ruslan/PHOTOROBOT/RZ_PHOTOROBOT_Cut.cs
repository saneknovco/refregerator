﻿using UnityEngine;

namespace HutongGames.PlayMaker.Actions
{
    public class RZ_PHOTOROBOT_Cut : FsmStateAction
    {
        public FsmOwnerDefault gameObject;
        FsmInt piecesCount = 3;
        public FsmFloat divideIndex = 50;
        public FsmTemplate pieceTemplate;
        public FsmTemplate placeTemplate;
        public FsmGameObject trashParent;
        public FsmBool createTrash;
        public FsmBool addCollider;


        public override void OnEnter()
        {
            GameObject GO, pieceFolder, trashGO, trashPlace, go;
            Vector3 scale, pos;
            PlayMakerFSM pm, pmPlace;
            BoxCollider bc;
            Mesh m;
            float index1, index2 , correctCenterY;
            GO = Fsm.GetOwnerDefaultTarget(gameObject);
            scale = GO.transform.parent.parent.localScale;

            for (int i = 1; i <= piecesCount.Value; i++)
            {
                #region old version
                //name = GO.transform.name;
                //index1 = 1 - (i / (float)piecesCount.Value);
                //index2 = ((piecesCount.Value + 1) - i) / (float)piecesCount.Value;
                //GameObject go = Object.Instantiate(GO, Vector3.zero, Quaternion.identity) as GameObject;
                //if (go.GetComponent<MeshFilter>() == null || go.GetComponent<MeshRenderer>() == null)
                //    Finish();

                //MeshFilter mf = go.GetComponent<MeshFilter>();
                //MeshRenderer mr = go.GetComponent<MeshRenderer>();
                //float correctCenterY = ((mf.mesh.bounds.min.y + mf.mesh.bounds.size.y * index1) + (mf.mesh.bounds.min.y + mf.mesh.bounds.size.y * index2)) / 2;

                //mf.mesh.vertices = new Vector3[] {new Vector3(mf.mesh.bounds.min.x*scale.x, scale.y * (mf.mesh.bounds.min.y + mf.mesh.bounds.size.y * index1-correctCenterY), 0),
                //                                  new Vector3(mf.mesh.bounds.min.x*scale.x, scale.y * (mf.mesh.bounds.min.y + mf.mesh.bounds.size.y * index2-correctCenterY), 0),
                //                                  new Vector3(mf.mesh.bounds.max.x*scale.x, scale.y * (mf.mesh.bounds.min.y + mf.mesh.bounds.size.y * index2-correctCenterY), 0),
                //                                  new Vector3(mf.mesh.bounds.max.x*scale.x, scale.y * (mf.mesh.bounds.min.y + mf.mesh.bounds.size.y * index1-correctCenterY), 0)};
                //mf.mesh.uv =       new Vector2[] {new Vector2(0 , index1),
                //                                  new Vector2(0 , index2),
                //                                  new Vector2(1 , index2),
                //                                  new Vector2(1 , index1)};
                //mf.mesh.triangles = new int[] { 0, 1, 2, 0, 2, 3 };

                //go.transform.parent = piecesParent.Value ? piecesParent.Value.transform : Fsm.GetOwnerDefaultTarget(gameObject).transform.parent;
                //go.transform.localPosition = new Vector3(0,correctCenterY/scale.y,0);
                //go.transform.name = name + i;
                //PlayMakerFSM pm = go.AddComponent<PlayMakerFSM>();
                //pm.SetFsmTemplate(pieceTemplate);
                //BoxCollider bc = go.AddComponent<BoxCollider>();
                //bc.size = new Vector3(mf.mesh.bounds.size.x*scale.x, (mf.mesh.bounds.size.y/piecesCount.Value) * scale.y, 1);

                //GameObject pieceFolder = new GameObject();
                //pieceFolder.transform.parent = go.transform.parent;
                //pieceFolder.name = i.ToString();
                //pieceFolder.transform.position = go.transform.position;
                //go.transform.parent = pieceFolder.transform;
                //go.transform.localPosition = Vector3.zero;
                //PlayMakerFSM pmPlace = pieceFolder.AddComponent<PlayMakerFSM>();
                //pmPlace.SetFsmTemplate(placeTemplate);

                //GameObject temp = Object.Instantiate(go, Vector3.zero, Quaternion.identity) as GameObject;
                //temp.transform.name = go.transform.name;
                //GameObject trashPlace;

                //if (trashParent.Value.transform.childCount < piecesCount.Value)
                //{
                //    trashPlace = new GameObject();
                //    trashPlace.transform.name = i.ToString();
                //    trashPlace.transform.parent = trashParent.Value.transform;
                //    trashPlace.transform.localPosition = Vector3.zero;
                //    temp.transform.parent = trashPlace.transform;
                //    temp.transform.localPosition = Vector3.zero;
                //}
                //else
                //{
                //    temp.transform.parent = trashParent.Value.transform.GetChild(i - 1);
                //    temp.transform.localPosition = Vector3.zero;
                //}
                #endregion


                //get parameters
                go = Object.Instantiate(GO, Vector3.zero, Quaternion.identity) as GameObject;
                pos = Vector3.zero;
                m = go.GetComponent<MeshFilter>().mesh;
                GetIndexes(m, out index1, out index2, i);
                correctCenterY = ((m.bounds.min.y + m.bounds.size.y * index1) + (m.bounds.min.y + m.bounds.size.y * index2)) / 2;

                //cut texture
                m.vertices = new Vector3[] {new Vector3(m.bounds.min.x*scale.x, scale.y * (m.bounds.min.y + m.bounds.size.y * index1-correctCenterY), 0),
                                                  new Vector3(m.bounds.min.x*scale.x, scale.y * (m.bounds.min.y + m.bounds.size.y * index2-correctCenterY), 0),
                                                  new Vector3(m.bounds.max.x*scale.x, scale.y * (m.bounds.min.y + m.bounds.size.y * index2-correctCenterY), 0),
                                                  new Vector3(m.bounds.max.x*scale.x, scale.y * (m.bounds.min.y + m.bounds.size.y * index1-correctCenterY), 0)};
                m.uv = new Vector2[] {new Vector2(0 , index1),
                                                  new Vector2(0 , index2),
                                                  new Vector2(1 , index2),
                                                  new Vector2(1 , index1)};
                m.triangles = new int[] { 0, 1, 2, 0, 2, 3 };
                m.RecalculateBounds();

               

                //reparent and replace
                go.transform.parent = Fsm.GetOwnerDefaultTarget(gameObject).transform.parent;
                go.transform.localPosition = new Vector3(0, correctCenterY / scale.y, 0);
                go.transform.name = GO.transform.name + "_" + i;

                if (addCollider.Value == true)
                {
                    bc = go.AddComponent<BoxCollider>();
                    bc.size = new Vector3(m.bounds.size.x, m.bounds.size.y, 1);
                    bc.enabled = false;
                }
                pieceFolder = new GameObject();
                pieceFolder.transform.parent = go.transform.parent;
                pieceFolder.name = i.ToString();
                for (int j = 0; j < piecesCount.Value; j++)
                {
                    GameObject g = new GameObject();
                    g.name = (j+1)+"_PLACE";
                    g.transform.parent = pieceFolder.transform;
                    g.transform.localPosition = Vector3.zero;
                    pm = g.AddComponent<PlayMakerFSM>();
                    pm.SetFsmTemplate(pieceTemplate);
                }
                if (i == 1)
                    pieceFolder.transform.position = new Vector3(go.transform.position.x, go.transform.position.y - m.bounds.size.y / 2 * go.transform.lossyScale.y, go.transform.position.z);
                if (i == 2)
                    pieceFolder.transform.position = go.transform.position;
                if (i == 3)
                    pieceFolder.transform.position = new Vector3(go.transform.position.x, go.transform.position.y + m.bounds.size.y / 2 * go.transform.lossyScale.y, go.transform.position.z);
                go.transform.parent = pieceFolder.transform.GetChild(0);
                pmPlace = pieceFolder.AddComponent<PlayMakerFSM>();
                pmPlace.SetFsmTemplate(placeTemplate);
                if (createTrash.Value == true)
                {
                    trashGO = Object.Instantiate(go, Vector3.zero, Quaternion.identity) as GameObject;
                    Color c = new Color(1, 1, 1, 0);
                    trashGO.GetComponent<MeshRenderer>().material.color = c;
                    trashGO.transform.name = go.transform.name;
                    if (trashParent.Value.transform.childCount < piecesCount.Value)
                    {
                        trashPlace = new GameObject();
                        trashPlace.transform.name = i.ToString();
                        trashPlace.transform.parent = trashParent.Value.transform;
                        trashPlace.transform.localPosition = Vector3.zero;
                        trashGO.transform.parent = trashPlace.transform;
                        trashGO.transform.localPosition = Vector3.zero;
                    }
                    else
                    {
                        trashGO.transform.parent = trashParent.Value.transform.GetChild(i - 1);
                        trashGO.transform.localPosition = Vector3.zero;
                    }
                }
            }

            Finish();
        }

        private void GetIndexes(Mesh m, out float index1, out float index2, int i)
        {
            switch (i)
            {
                case 1:
                    {
                        index1 = 0.5f + divideIndex.Value / m.bounds.size.y;
                        index2 = ((piecesCount.Value + 1) - i) / (float)piecesCount.Value;
                        break;
                    }
                case 2:
                    {
                        index1 = 0.5f - divideIndex.Value / m.bounds.size.y;
                        index2 = 0.5f + divideIndex.Value / m.bounds.size.y;
                        break;
                    }
                default:
                    {
                        index1 = 1 - (i / (float)piecesCount.Value);
                        index2 = 0.5f - divideIndex.Value / m.bounds.size.y;
                        break;
                    }
            }
        }
    }
}
