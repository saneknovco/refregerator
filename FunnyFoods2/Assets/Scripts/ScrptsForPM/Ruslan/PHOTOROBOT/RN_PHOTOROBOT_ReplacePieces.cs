﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

namespace HutongGames.PlayMaker.Actions
{
    public class RN_PHOTOROBOT_ReplacePieces : FsmStateAction
    {
        GameObject trashStorage, trashPiece, piece, place, ownParent;
        int counter;
        bool repeatTest;
        string namesString = "";
        public FsmFloat Xdistance;
        int[] indexes = new int[] {1, 2, 4, 5, 7, 8 };
        bool left;
        Texture2D tex;
        RenderTexture rt;
        Vector2 leftUpPoint, rightUpPoint, leftDownPoint, rightDownPoint;
        Vector2[] mainPoints, points;
        Vector3 vert;


        public override void OnEnter()
        {
            mainPoints = GetAlphaPoints(Owner.transform.parent.GetChild(5).GetChild(0).GetChild(0).gameObject, piecePos.CENTER);
            for (int i = 0; i < 2; i++)
            {
                trashStorage = FsmVariables.GlobalVariables.GetFsmGameObject("RN/PHO/Temp").Value.GetChildByName(Owner.name);
                do
                {
                    repeatTest = false;
                    ownParent = Owner.transform.parent.gameObject;
                    trashPiece = trashStorage.transform.GetChild(Random.Range(0, trashStorage.transform.childCount)).gameObject;
                    string namePart = trashPiece.name.Substring(0, trashPiece.name.Length - 1);
                    if (ownParent.GetChildByName(namePart + 1) != null || ownParent.GetChildByName(namePart + 2) != null || ownParent.GetChildByName(namePart + 3) != null)
                        repeatTest = true;
                }
                while (trashPiece.name == Owner.transform.GetChild(0).name || repeatTest == true);
                trashPiece.transform.parent = Owner.transform.GetChild(i+1);
                trashPiece.transform.localPosition = Vector3.zero;
                repeatTest = false;
                counter = 0;
            }

            switch (Owner.name)
            {
                case "1":
                    {
                        BoxCollider box = Owner.AddComponent<BoxCollider>();
                        box.size = new Vector3(Xdistance.Value, 300, 1);
                        box.center = new Vector3(0, 150, 0);
                        int randindex = indexes[Random.Range(0, indexes.Length)];
                        for (int i = 0; i < 3; i++)
                        {
                            place = Owner.transform.GetChild(i).gameObject;
                            piece = place.transform.GetChild(0).gameObject;
                            if (i > 0)
                            {
                                points = GetAlphaPoints(piece, piecePos.UP);
                                piece.transform.localScale = new Vector3(Vector2.Distance(mainPoints[0], mainPoints[1]) / Vector2.Distance(points[2], points[3]), piece.transform.localScale.y, piece.transform.localScale.z);
                                piece.transform.localPosition = new Vector3(mainPoints[0].x - (((points[2].x - piece.transform.position.x) * piece.transform.lossyScale.x) + piece.transform.position.x), 0, 0);
                                place.transform.localPosition = new Vector3(-(Xdistance.Value * 3) + i * Xdistance.Value * 2, (piece.GetComponent<MeshFilter>().mesh.bounds.size.y / 2 / piece.transform.localScale.y), 0);

                            }
                            FsmVariables.GlobalVariables.GetFsmInt("RN/PHO/ChangeCount").Value = randindex;
                        }
                        break;
                    }
                case "2":
                    {
                        BoxCollider box = Owner.AddComponent<BoxCollider>();
                        box.size = new Vector3(Xdistance.Value, 200, 1);
                        box.center = new Vector3(0, 0, 0);
                        box.enabled = false;
                        for (int i = 0; i < 3; i++)
                        {
                            place = Owner.transform.GetChild(i).gameObject;
                            piece = place.transform.GetChild(0).gameObject;
                            if (i > 0)
                                place.transform.localPosition = new Vector3(-(Xdistance.Value * 3) + i * Xdistance.Value * 2, 0, 0);
                            FsmVariables.GlobalVariables.GetFsmInt("RN/PHO/ChangeCount").Value = 0;
                        }
                        break;
                    }
                case "3":
                    {
                        BoxCollider box = Owner.AddComponent<BoxCollider>();
                        box.size = new Vector3(Xdistance.Value, 300, 1);
                        box.center = new Vector3(0, -150, 0);
                        int randindex = indexes[Random.Range(0, indexes.Length)];
                        for (int i = 0; i < 3; i++)
                        {
                            place = Owner.transform.GetChild(i).gameObject;
                            piece = place.transform.GetChild(0).gameObject;
                            if (i > 0)
                            {
                                points = GetAlphaPoints(piece, piecePos.DOWN);
                                piece.transform.localScale = new Vector3(Vector2.Distance(mainPoints[2], mainPoints[3]) / Vector2.Distance(points[0], points[1]), piece.transform.localScale.y, piece.transform.localScale.z);
                                piece.transform.localPosition = new Vector3(mainPoints[2].x - (((points[0].x - piece.transform.position.x) * piece.transform.lossyScale.x) + piece.transform.position.x), 0, 0);
                                place.transform.localPosition = new Vector3((-(Xdistance.Value * 3) + i * Xdistance.Value * 2), -(piece.GetComponent<MeshFilter>().mesh.bounds.size.y / 2 / piece.transform.localScale.y), 0);
                            }
                            else
                                if (piece.GetComponent<BoxCollider>())
                                    piece.GetComponent<BoxCollider>().enabled = true;

                            FsmVariables.GlobalVariables.GetFsmInt("RN/PHO/ChangeCount").Value = randindex;
                        }
                        break;
                    }
            }
            Finish();
        }

        private Vector2[] GetAlphaPoints(GameObject go, piecePos p)
        {
            left = false;
            leftDownPoint = leftUpPoint = rightDownPoint = rightUpPoint = Vector2.zero;
            vert = go.transform.GetComponent<MeshFilter>().mesh.vertices[1];
            rt = (RenderTexture)go.GetComponent<MeshRenderer>().material.mainTexture;
            RenderTexture.active = rt;
            tex = new Texture2D(rt.width, rt.height);
            tex.ReadPixels(new Rect(0, 0, rt.width, rt.height), 0, 0);
            tex.Apply();

            switch (p)
            {
                case piecePos.UP:
                    {
                        for (int k = 0; k <= tex.width; k= k + 3)
                        {
                            if (tex.GetPixel(k, (int)(Mathf.Round(rt.height - vert.y * 2))).a >= 0.7f && left == false)
                            {
                                leftDownPoint = new Vector2(go.transform.position.x + vert.x * go.transform.lossyScale.x + k * go.transform.lossyScale.x, go.transform.position.y - vert.y);
                                left = true;
                            }
                            if (tex.GetPixel(k, (int)(Mathf.Round(rt.height - vert.y * 2))).a <= 0.7f && left == true)
                            {
                                rightDownPoint = new Vector2(go.transform.position.x + vert.x * go.transform.lossyScale.x + k * go.transform.lossyScale.x, go.transform.position.y - vert.y);
                                left = false;
                                break;
                            }
                        }
                       // Debug.DrawLine(go.transform.position, leftDownPoint, Color.green, 110);
                        //Debug.DrawLine(go.transform.position, rightDownPoint, Color.green, 110);
                        break;
                    }
                case piecePos.CENTER:
                    {
                        for (int k = 0; k <= tex.width; k = k + 3)
                        {
                            if (tex.GetPixel(k, (int)(Mathf.Round(rt.height / 2 + vert.y))).a >= 0.7f && left == false)
                            {
                                leftUpPoint = new Vector2(go.transform.position.x + vert.x + k, go.transform.position.y + vert.y);
                                left = true;
                            }
                            if (tex.GetPixel(k, (int)(Mathf.Round(rt.height / 2 + vert.y))).a <= 0.7f && left == true)
                            {
                                rightUpPoint = new Vector2(go.transform.position.x + vert.x + k, go.transform.position.y + vert.y);
                                left = false;
                                break;
                            }
                        }
                        for (int k = 0; k <= tex.width; k = k + 3)
                        {
                            if (tex.GetPixel(k, (int)(Mathf.Round(rt.height / 2 - vert.y))).a >= 0.7f && left == false)
                            {
                                leftDownPoint = new Vector2(go.transform.position.x + vert.x + k, go.transform.position.y - vert.y);
                                left = true;
                                
                            }
                            if (tex.GetPixel(k, (int)(Mathf.Round(rt.height / 2 - vert.y))).a <= 0.7f && left == true)
                            {
                                rightDownPoint = new Vector2(go.transform.position.x + vert.x + k, go.transform.position.y - vert.y);
                                left = false;
                                break;
                            }
                        }

                       // Debug.DrawLine(go.transform.position, leftUpPoint, Color.red, 110);
                        //Debug.DrawLine(go.transform.position, rightUpPoint, Color.red, 110);
                        //Debug.DrawLine(go.transform.position, leftDownPoint, Color.red, 110);
                        //Debug.DrawLine(go.transform.position, rightDownPoint, Color.red, 110);
                        break;
                    }
                case piecePos.DOWN:
                    {
                        for (int k = 0; k <= tex.width; k=k+3)
                        {
                            if (tex.GetPixel(k, (int)(Mathf.Round(vert.y * 2))).a >= 0.7f && left == false)
                            {
                                leftUpPoint = new Vector2(go.transform.position.x + vert.x * go.transform.lossyScale.x + k * go.transform.lossyScale.x, go.transform.position.y + vert.y);
                                left = true;
                            }
                            if (tex.GetPixel(k, (int)(Mathf.Round(vert.y * 2))).a <= 0.7f && left == true)
                            {
                                rightUpPoint = new Vector2(go.transform.position.x + vert.x * go.transform.lossyScale.x + k * go.transform.lossyScale.x, go.transform.position.y + vert.y);
                                left = false;
                                break;
                            }
                        }
                        //Debug.DrawLine(go.transform.position, leftUpPoint, Color.blue, 110);
                        //Debug.DrawLine(go.transform.position, rightUpPoint, Color.blue, 110);
                        break;
                    }
            }
            return new Vector2[] { leftUpPoint, rightUpPoint, leftDownPoint, rightDownPoint };
        }

        void GetRandChild()
        {
            do
            {
                piece = Owner.transform.GetChild(Random.Range(0, Owner.transform.childCount)).gameObject;
            }
            while (namesString.Contains(piece.name));
            namesString += piece.name;
        }
        enum piecePos
        {
            UP,CENTER,DOWN
        }
    }
}
