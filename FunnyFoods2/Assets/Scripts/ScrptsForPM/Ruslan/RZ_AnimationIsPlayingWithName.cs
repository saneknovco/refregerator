﻿using UnityEngine;

namespace HutongGames.PlayMaker.Actions
{
    [ActionCategory("FF2_Ruslan")]
    [Tooltip("Is animation is playing by name?")]
    public class RZ_AnimationIsPlayingWithName : FsmStateAction 
	{
		[RequiredField]
		public FsmOwnerDefault animObj;
        public FsmEvent isPlaying;
        public FsmEvent isNotPlaying;
        public FsmString AnimName;
		public FsmBool everyFrame;

		private GameObject _go;
		private Animation _animation;

		public void DoCheckIsAnimationPlaying()
		{
			if(_animation != null)
            {
                if (AnimName.Value != "")
                {
                    if (_animation.IsPlaying(AnimName.Value) == true)
                        Fsm.Event(isPlaying);
                    else
                        Fsm.Event(isNotPlaying);
                }
                else
                    Finish();
            }
            Finish();
		}
		public override void OnEnter ()
		{
            if (animObj != null)
            {
                _go = Fsm.GetOwnerDefaultTarget(animObj);
                _animation = _go.GetComponent<Animation>();
            }
            else
                Finish();
			
            DoCheckIsAnimationPlaying();

			if(!everyFrame.Value) Finish ();
		}
		public override void OnUpdate ()
		{
			DoCheckIsAnimationPlaying();
		}
	}
}
