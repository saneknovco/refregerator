﻿using UnityEngine;
using Bini.Utils.Audio;


namespace HutongGames.PlayMaker.Actions
{
    [ActionCategory("FF2_Ruslan")]
    public class RZ_GoodBadSound : FsmStateAction
    {
        public GoodOrBad GoodOrBadValue;
        public FsmString Language;
        public FsmEvent finishEvent;
        public FsmFloat volume = 1f, speed = 1f;
        public FsmBool ClearGoodBadSounds;
        float length, time;
        string Sound;
        string[] good, bad;
        bool dontPlay;

        public override void OnEnter()
        {
            dontPlay = false;
            good = new string[10] { "R_011", "R_022", "R_033", "R_044", "R_055", "R_066", "R_077", "R_088", "R_099", "R_100" };
            bad = new string[8] { "W_011", "W_022", "W_033", "W_044", "W_055", "W_066", "W_077", "W_088" };
            if (ClearGoodBadSounds.Value == true)
            {
                ClearOldAudio();
            }
            else
            {
                switch (GoodOrBadValue)
                {
                    case GoodOrBad.good:
                        {
                            int index = Random.Range(0, good.Length - 1);
                            Sound = good[index];
                            break;
                        }
                    case GoodOrBad.bad:
                        {
                            int index = Random.Range(0, bad.Length - 1);
                            Sound = bad[index];
                            break;
                        }
                }
                ClearOldAudio();
                if (dontPlay == false)
                {

                    AudioClip clip = Resources.Load("Sounds/" + Language.Value + "/" + Sound) as AudioClip;
                    if (clip == null)
                        Finish();
                    else
                    {
                        length = clip.length;
                        time = 0;
                        Audio.PlaySound(clip, volume.Value, speed.Value);
                    }
                    if (finishEvent == null)
                        Finish();
                }
                else
                    Finish();
            }
        }

        private void ClearOldAudio()
        {
            foreach (string sound in good)
                foreach (Transform child in Camera.main.gameObject.transform)
                    if (child.name == "Audio:" + sound)
                        dontPlay = true;
                        //GameObject.Destroy(child.gameObject);
            foreach (string sound in bad)
                foreach (Transform child in Camera.main.gameObject.transform)
                    if (child.name == "Audio:"+sound)
                        dontPlay = true;
                        //GameObject.Destroy(child.gameObject);
        }
        public override void OnUpdate()
        {
            time += Time.deltaTime;
            if (time > length)
            {
                if (finishEvent != null)
                    Fsm.Event(finishEvent);
                Finish();
            }
        }
    }
    public enum GoodOrBad { good, bad }
}
