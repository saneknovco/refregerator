﻿using UnityEngine;

namespace HutongGames.PlayMaker.Actions
{
    [ActionCategory("FF2_Ruslan")]
    public class RZ_SetSousCount : FsmStateAction
    {
        public FsmInt ColorCount;
        public FsmGameObject Parent;

        public override void OnEnter()
        {
            GameObject parent = Parent.Value;
            float x = parent.transform.GetChild(0).GetComponent<MeshRenderer>().bounds.size.x / 2;
            float y = parent.transform.GetChild(0).GetComponent<MeshRenderer>().bounds.size.y / 2;
            switch (ColorCount.Value)
            {
                case 0: break;
                case 1: break;
                case 2:
                    {
                        CutSous(parent.transform.GetChild(0).gameObject.GetComponent<MeshFilter>().mesh,
                            parent.transform.GetChild(0).transform.GetChild(0).gameObject.GetComponent<MeshFilter>().mesh,
                            new Vector2(x, y),
                             new int[] { 0, 1, 2 }, new int[] { 0, 1, 2 }, parent.transform.GetChild(0).gameObject);

                        CutSous(parent.transform.GetChild(1).gameObject.GetComponent<MeshFilter>().mesh,
                            parent.transform.GetChild(1).transform.GetChild(0).gameObject.GetComponent<MeshFilter>().mesh,
                            new Vector2(x, y),
                            new int[] { 3, 0, 2 }, new int[] { 0, 3, 2 }, parent.transform.GetChild(1).gameObject);

                        Finish();
                        break;
                    }
                case 3:
                    {
                        CutSous(parent.transform.GetChild(0).gameObject.GetComponent<MeshFilter>().mesh,
                            parent.transform.GetChild(0).transform.GetChild(0).gameObject.GetComponent<MeshFilter>().mesh,
                            new Vector2(x, y),
                             new int[] { 0, 7, 6, 0, 4, 7 }, new int[] { 0, 4, 6, 7 }, parent.transform.GetChild(0).gameObject);

                        CutSous(parent.transform.GetChild(1).gameObject.GetComponent<MeshFilter>().mesh,
                            parent.transform.GetChild(1).transform.GetChild(0).gameObject.GetComponent<MeshFilter>().mesh,
                            new Vector2(x, y),
                            new int[] { 3, 6, 7, 3, 7, 5 }, new int[] { 3, 5, 6, 7 }, parent.transform.GetChild(1).gameObject);

                        CutSous(parent.transform.GetChild(2).gameObject.GetComponent<MeshFilter>().mesh,
                            parent.transform.GetChild(2).transform.GetChild(0).gameObject.GetComponent<MeshFilter>().mesh,
                            new Vector2(x, y),
                             new int[] { 5, 7, 4, 5, 4, 1, 5, 1, 2 }, new int[] { 1, 2, 4, 5, 7 }, parent.transform.GetChild(2).gameObject);

                        Finish();
                        break;
                    }
                default: break;
            }
        }

        void CutSous(Mesh go, Mesh blick, Vector2 bounds, int[] tr, int[]index, GameObject piece)
        {
            Vector3 sum = new Vector3();
            go.vertices = new Vector3[] { new Vector2(-bounds.x, bounds.y), new Vector2(bounds.x, bounds.y), new Vector2(bounds.x, -bounds.y), new Vector2(-bounds.x, -bounds.y), new Vector2((bounds.x) * 0.7f, bounds.y), new Vector2((bounds.x) * 0.7f, -bounds.y), new Vector2(-bounds.x, 0), new Vector2(0, 0) };
            foreach (int item in index)
                sum += go.vertices[item];
            Vector3 center = new Vector3(sum.x / index.Length, sum.y / index.Length, 0);
            go.triangles = tr;
            go.uv = new Vector2[] { new Vector2(0, 1), new Vector2(1, 1), new Vector2(1, 0), new Vector2(0, 0), new Vector2(0.85f, 1), new Vector2(0.85f, 0), new Vector2(0, 0.5f), new Vector2(0.5f, 0.5f) };
            go.vertices = new Vector3[] { new Vector2(-bounds.x - center.x, bounds.y - center.y), new Vector2(bounds.x - center.x, bounds.y - center.y), new Vector2(bounds.x - center.x, -bounds.y - center.y), new Vector2(-bounds.x - center.x, -bounds.y - center.y), new Vector2((bounds.x * 0.7f - center.x), bounds.y - center.y), new Vector2((bounds.x * 0.7f - center.x), -bounds.y - center.y), new Vector2(-bounds.x - center.x, 0 - center.y), new Vector2(0 - center.x, 0 - center.y) };
            piece.transform.position += new Vector3(center.x / piece.transform.lossyScale.x, center.y / piece.transform.lossyScale.y, 0);

            sum = new Vector3();
            blick.vertices = new Vector3[] { new Vector2(-bounds.x, bounds.y), new Vector2(bounds.x, bounds.y), new Vector2(bounds.x, -bounds.y), new Vector2(-bounds.x, -bounds.y), new Vector2((bounds.x) * 0.7f, bounds.y), new Vector2((bounds.x) * 0.7f, -bounds.y), new Vector2(-bounds.x, 0), new Vector2(0, 0) };
            foreach (int item in index)
                sum += blick.vertices[item];
            center = new Vector3(sum.x / index.Length, sum.y / index.Length, 0);
            blick.vertices = new Vector3[] { new Vector2(-bounds.x, bounds.y), new Vector2(bounds.x, bounds.y), new Vector2(bounds.x, -bounds.y), new Vector2(-bounds.x, -bounds.y), new Vector2((bounds.x) * 0.7f, bounds.y), new Vector2((bounds.x) * 0.7f, -bounds.y), new Vector2(-bounds.x, 0), new Vector2(0, 0) };
            blick.triangles = tr;
            blick.uv = new Vector2[] { new Vector2(0, 1), new Vector2(1, 1), new Vector2(1, 0), new Vector2(0, 0), new Vector2(0.85f, 1), new Vector2(0.85f, 0), new Vector2(0, 0.5f), new Vector2(0.5f, 0.5f) };
            blick.vertices = new Vector3[] { new Vector2(-bounds.x - center.x, bounds.y - center.y), new Vector2(bounds.x - center.x, bounds.y - center.y), new Vector2(bounds.x - center.x, -bounds.y - center.y), new Vector2(-bounds.x - center.x, -bounds.y - center.y), new Vector2((bounds.x * 0.7f - center.x), bounds.y - center.y), new Vector2((bounds.x * 0.7f - center.x), -bounds.y - center.y), new Vector2(-bounds.x - center.x, 0 - center.y), new Vector2(0 - center.x, 0 - center.y) };
            piece.transform.GetChild(0).transform.position += new Vector3(center.x / piece.transform.GetChild(0).transform.lossyScale.x, center.y / piece.transform.GetChild(0).transform.lossyScale.y, 0);
        }
    }
}
