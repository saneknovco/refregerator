﻿using UnityEngine;
using System.Collections;
using Bini.Utils.Audio;
using System;

namespace HutongGames.PlayMaker.Actions
{
    public class RZ_PlayFoodName : FsmStateAction
    {
        public AudioGO ActionPlace;
        public MuteTypes MuteType;
        public FsmString Default;
        GameObject NameGO;
        string Name, FullName;
        public FsmString Language;
        public FsmEvent finishEvent;
        public FsmFloat volume = 1f, speed = 1f;
        float length, time;

        public override void OnEnter()
        {
            if (String.IsNullOrEmpty(Default.Value))
                switch (MuteType)
                {
                    #region FoodName
                    case MuteTypes.FoodName:
                        {
                            switch (ActionPlace)
                            {
                                case AudioGO.Golova:
                                    {
                                        Name = Owner.transform.parent.name.Substring(0, Owner.transform.parent.name.Length - 5);
                                        break;
                                    }
                                case AudioGO.Pers:
                                    {
                                        Name = Owner.name.Substring(0, Owner.name.Length - 5);
                                        break;
                                    }
                                case AudioGO.PersControl:
                                    {
                                        Name = Owner.transform.GetChild(0).name.Substring(0, Owner.transform.GetChild(0).name.Length - 5);
                                        break;
                                    }
                                default: break;
                            } break;
                        }
                    #endregion
                    #region PersName
                    case MuteTypes.PersName:
                        {

                            switch (ActionPlace)
                            {
                                case AudioGO.Golova:
                                    {
                                        Name = Owner.transform.parent.parent.parent.parent.tag;
                                        break;
                                    }
                                case AudioGO.Pers:
                                    {
                                        Name = Owner.transform.parent.tag;
                                        break;
                                    }
                                case AudioGO.PersControl:
                                    {
                                        Name = Owner.tag;
                                        break;
                                    }
                                case AudioGO.Head:
                                    {
                                        Name = Owner.transform.parent.parent.parent.tag;
                                        break;
                                    }
                                case AudioGO.HeadControl:
                                    {
                                        Name = Owner.transform.parent.parent.tag;
                                        break;
                                    }
                                default: break;
                            }
                        } break;
                    #endregion
                    #region PersHeadName
                    case MuteTypes.PersHeadName:
                        {

                            switch (ActionPlace)
                            {
                                case AudioGO.Golova:
                                    {
                                        Name = Owner.transform.parent.parent.tag;
                                        break;
                                    }
                                case AudioGO.PersControl:
                                    {
                                        Name = Owner.tag;
                                        break;
                                    }
                                case AudioGO.Head:
                                    {
                                        Name = Owner.transform.parent.tag;
                                        break;
                                    }
                                default: break;
                            }
                        } break;
                    #endregion
                    #region SizeName
                    case MuteTypes.SizeName:
                        {
                            switch (ActionPlace)
                            {
                                case AudioGO.Golova:
                                    {
                                        if(Language.Value == "ru")
                                            Name = Owner.tag + Owner.transform.parent.parent.tag.Substring(0, Owner.transform.parent.parent.tag.Length - 5);
                                        else
                                            Name = Owner.transform.parent.parent.tag.Substring(0, Owner.transform.parent.parent.tag.Length - 5);
                                        break;
                                    }
                                case AudioGO.Pers:
                                    {
                                        if (Language.Value == "ru")
                                            Name = Owner.transform.GetChild(0).tag + Owner.transform.parent.tag.Substring(0, Owner.transform.parent.tag.Length - 5);
                                        else
                                            Name = Owner.transform.parent.tag.Substring(0, Owner.transform.parent.tag.Length - 5);
                                        break;
                                    }
                                case AudioGO.PersControl:
                                    {
                                        if (Language.Value == "ru")
                                            Name = Owner.transform.GetChild(0).GetChild(0).tag + Owner.tag.Substring(0, Owner.tag.Length - 5);
                                        else
                                            Name = Owner.tag.Substring(0, Owner.tag.Length - 5);
                                        break;
                                    }
                                default: break;
                            }
                        } break;
                    #endregion
                }
            else
                Name = Default.Value;
            FullName = Language.Value + "/" + Name;
            AudioClip clip = Resources.Load("Sounds/" + FullName) as AudioClip;
            if (clip == null)
                Finish();
            else
            {
                length = clip.length;
                time = 0;
                Audio.PlaySound(clip, volume.Value, speed.Value);
            }
            if (finishEvent == null)
                Finish();
        }
        public override void OnUpdate()
        {
            time += Time.deltaTime;
            if (time > length)
            {
                if (finishEvent != null)
                    Fsm.Event(finishEvent);
                Finish();
            }
        }

    }
    public enum AudioGO
    {
        Golova, 
        Pers, 
        PersControl,
        Head,
        HeadControl
    }
}
