﻿using UnityEngine;

namespace HutongGames.PlayMaker.Actions
{
    [ActionCategory("FF2_Ruslan")]
    public class RZ_GetParentNumNum : FsmStateAction
    {
        [RequiredField]
        [Tooltip("The GameObject to search.")]
        public FsmOwnerDefault gameObject;

        [RequiredField]
        [Tooltip("The index of the parent to find.")]
        public FsmInt parentLevel;

        [RequiredField]
        [UIHint(UIHint.Variable)]
        [Tooltip("Store the parent in a GameObject variable.")]
        public FsmGameObject storeResult;

        public override void OnEnter()
        {
            storeResult.Value = DoGetParentNum(Fsm.GetOwnerDefaultTarget(gameObject));
            Finish();
        }

        GameObject DoGetParentNum(GameObject go)
        {
                if (go != null)
                    storeResult.Value = go.transform.parent == null ? null : go.transform.parent.gameObject;
                else
                    storeResult.Value = null;

                for (int i = 0; i < parentLevel.Value - 1; i++)
                    storeResult.Value = storeResult.Value.transform.parent == null ? null : storeResult.Value.transform.parent.gameObject;

                return storeResult.Value;
        }
    }
}
