﻿using UnityEngine;
using System.Collections;

namespace HutongGames.PlayMaker.Actions
{
    [ActionCategory("FF2_Ruben")]
    public class RZ_ShadowController : FsmStateAction
    {
        public FsmOwnerDefault GameObject;
        public FsmFloat time;
        public FsmFloat GoalDistance;
        public FsmEvent finishEvent;
        public FsmBool OFF;

        Component storeComponent;
        float currentTime, storeResult, startDistance;
        GameObject go;

        public override void OnEnter()
        {
            currentTime = 0; storeResult = 0; startDistance = 0; go = null; storeComponent = null;
            go = Fsm.GetOwnerDefaultTarget(GameObject);
            storeComponent = go.GetComponent<Shadow2DCaster>();
            if (!storeComponent)
                Finish();
            startDistance = go.GetComponent<Shadow2DCaster>().DistanceToPlane;
            go.GetComponent<Shadow2DCaster>().enabled = true;
        }

        public override void OnUpdate()
        {
            currentTime += Time.deltaTime;
            var lerpTime = currentTime / time.Value;
            storeResult = Mathf.Lerp(startDistance, GoalDistance.Value, lerpTime);
            go.GetComponent<Shadow2DCaster>().DistanceToPlane = storeResult;
            if (lerpTime > 1)
            {
                if(OFF.Value == true)
                    go.GetComponent<Shadow2DCaster>().enabled = false;
                if (finishEvent != null)
                    Fsm.Event(finishEvent);
                Finish();
            }
        }
    }
}