﻿// (c) Copyright HutongGames, LLC 2010-2013. All rights reserved.

using UnityEngine;

namespace HutongGames.PlayMaker.Actions
{
    [Tooltip("Recursive sets a Game Object's Layer.")]
    public class RZ_SetLayer : FsmStateAction
    {
        [RequiredField]
        public FsmOwnerDefault[] gameObject;
        [UIHint(UIHint.Layer)]
        public int layer;
        public FsmBool isRecursive;

        public override void OnEnter()
        {
            for (int i = 0; i < gameObject.Length; i++)
            {
                GameObject GO = Fsm.GetOwnerDefaultTarget(gameObject[i]);
                DoSetLayer(GO);
                if (isRecursive.Value == true)
                    //for (int j = 0; j < GO.transform.childCount; j++)
                        RecursiveDoSetLayer(GO);
            }
            Finish();
        }

        void DoSetLayer(GameObject go)
        {
            go.layer = layer;
        }

        void RecursiveDoSetLayer(GameObject go)
        {
            DoSetLayer(go);
            for (int j = 0; j < go.transform.childCount; j++)
                RecursiveDoSetLayer(go.transform.GetChild(j).gameObject);
        }
    }
}