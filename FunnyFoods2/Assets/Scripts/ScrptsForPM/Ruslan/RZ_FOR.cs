﻿// (c) Copyright HutongGames, LLC 2010-2013. All rights reserved.

namespace HutongGames.PlayMaker.Actions
{
    [ActionCategory("FF2_Ruslan")]
    public class RZ_FOR : FsmStateAction
    {
        public FsmInt From;
        public FsmInt To;
        public FsmInt Index;
        public FsmBool ResetIndexOnExit;

        public FsmEvent loop;
        public FsmEvent exit;


        public override void OnEnter()
        {
            DoIntCompare();
        }

        void DoIntCompare()
        {
            if (Index.Value >= To.Value-1)
            {
                if (ResetIndexOnExit.Value == true)
                    Index.Value = 0;
                
                if (exit == null)
                    Finish();

                Fsm.Event(exit);
                return;
            }
            else
            {
                Index.Value++;
                
                if (loop == null)
                    Finish();
                
                Fsm.Event(loop);
                return;
            }
            

        }

        public override string ErrorCheck()
        {
            if (FsmEvent.IsNullOrEmpty(loop) &&
                FsmEvent.IsNullOrEmpty(exit))
                return "Action sends no events!";
            return "";
        }
    }
}