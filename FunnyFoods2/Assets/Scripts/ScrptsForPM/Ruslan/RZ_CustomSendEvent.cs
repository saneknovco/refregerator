﻿// (c) Copyright HutongGames, LLC 2010-2013. All rights reserved.

using System;
using UnityEngine;

namespace HutongGames.PlayMaker.Actions
{
    [ActionCategory("FF2_Ruslan")]
    [Tooltip("Sends an Event after an optional delay. NOTE: To send events between FSMs they must be marked as Global in the Events Browser.")]
    public class RZ_CustomSendEvent : FsmStateAction
    {
        [Tooltip("Where to send the event.")]
        public FsmEventTarget eventTarget;

        [Tooltip("Check to send event to parent of this GO. NOTE: If this parameter is checked then parameter 'eventTarget' is off")]
        public FsmBool SendToParent;

        [Tooltip("Check to send event to num of child of this GO. NOTE: If this parameter is checked then parameter 'eventTarget' is off")]
        public FsmInt SendToChildNum = -1;

        [Tooltip("Check to send event to name of child of this GO. NOTE: If this parameter is checked then parameter 'eventTarget' is off")]
        public FsmString SendToChildName;

        [RequiredField]
        [Tooltip("The event to send. NOTE: Events must be marked Global to send between FSMs.")]
        public FsmEvent sendEvent;

        [HasFloatSlider(0, 10)]
        [Tooltip("Optional delay in seconds.")]
        public FsmFloat delay;

        [Tooltip("Repeat every frame. Rarely needed.")]
        public bool everyFrame;

        private DelayedEvent delayedEvent;

        public override void Reset()
        {
            eventTarget = null;
            sendEvent = null;
            delay = null;
            everyFrame = false;
        }

        public override void OnEnter()
        {
            if (delay.Value < 0.001f)
            {
                if (SendToParent.Value && SendToChildNum.Value == -1 && SendToChildName.Value == "")
                {

                    //eventTarget.gameObject.OwnerOption = OwnerDefaultOption.UseOwner;
                    FsmEventTarget parentCust = new FsmEventTarget();
                    //Debug.Log(eventTarget.gameObject.GameObject.Value.transform.parent.gameObject.name + " 1");
                    parentCust.gameObject.GameObject.Value = eventTarget.gameObject.GameObject.Value.transform.parent.gameObject;
                    Debug.Log(parentCust.gameObject.ToString() + " 2");
                    Fsm.Event(parentCust, sendEvent);
                    Debug.Log(sendEvent.Name + " 3");
                    
                }
                if (!SendToParent.Value && SendToChildNum.Value >= 0 && SendToChildName.Value == "")
                {
                    eventTarget.gameObject.OwnerOption = OwnerDefaultOption.UseOwner;
                    FsmEventTarget child = new FsmEventTarget();
                    child.gameObject.GameObject.Value = eventTarget.gameObject.GameObject.Value.transform.GetChild(SendToChildNum.Value).gameObject;
                    Fsm.Event(child, sendEvent);
                }
                if (!SendToParent.Value && SendToChildNum.Value == -1 && SendToChildName.Value != "")
                {
                    eventTarget.gameObject.OwnerOption = OwnerDefaultOption.UseOwner;
                    FsmEventTarget child = new FsmEventTarget();
                    child.gameObject.GameObject.Value = eventTarget.gameObject.GameObject.Value.transform.FindChild(SendToChildName.Value).gameObject;
                    Fsm.Event(child, sendEvent);
                }
                if (SendToParent.Value && SendToChildNum.Value >= 0 || SendToChildNum.Value >= 0 && SendToChildName.Value != "" || SendToParent.Value && SendToChildName.Value != "" || SendToParent.Value && SendToChildNum.Value >= 0 && SendToChildName.Value != "")
                    LogWarning("Chose SendToParent or SendToChildNum or SendToChildName");
                
                Fsm.Event(eventTarget, sendEvent);
                Finish();
            }
            else
            {
                delayedEvent = Fsm.DelayedEvent(eventTarget, sendEvent, delay.Value);
            }
        }

        public override void OnUpdate()
        {
            if (!everyFrame)
            {
                if (DelayedEvent.WasSent(delayedEvent))
                {
                    Finish();
                }
            }
        }
    }
}