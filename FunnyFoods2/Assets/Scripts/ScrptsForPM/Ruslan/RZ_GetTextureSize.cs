﻿
using UnityEngine;
using System.Collections;

namespace HutongGames.PlayMaker.Actions
{

public class RZ_GetTextureSize : FsmStateAction {

        public FsmOwnerDefault gameObject;
        public FsmFloat X;
        public FsmFloat Y;

        public override void OnEnter()
        {
            GameObject GO = Fsm.GetOwnerDefaultTarget(gameObject);
            MeshFilter mf;
            if(GO)
            {
                if(mf = GO.GetComponent<MeshFilter>())
                {
                    if (X != null)
                        X.Value = mf.mesh.bounds.size.x;
                    if (Y != null)
                        Y.Value = mf.mesh.bounds.size.y;
                }
            }
            Finish();
        }
    }
}
