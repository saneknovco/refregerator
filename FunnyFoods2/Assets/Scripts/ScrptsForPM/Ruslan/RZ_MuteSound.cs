﻿using System;
using UnityEngine;
using Object = UnityEngine.Object;

namespace HutongGames.PlayMaker.Actions
{
    public class RZ_MuteSound : FsmStateAction
    {
        public AudioGO ActionPlace;
        string SoundName;
        public MuteTypes MuteType;
        public FsmString Default;
        
        public override void OnEnter()
        {
            if (String.IsNullOrEmpty(Default.Value))
                switch (MuteType)
                {
                    #region FoodName
                    case MuteTypes.FoodName:
                        {
                            switch (ActionPlace)
                            {
                                case AudioGO.Golova:
                                    {
                                        SoundName = Owner.transform.parent.name.Substring(0, Owner.transform.parent.name.Length - 5);
                                        break;
                                    }
                                case AudioGO.Pers:
                                    {
                                        SoundName = Owner.name.Substring(0, Owner.name.Length - 5);
                                        break;
                                    }
                                case AudioGO.PersControl:
                                    {
                                        SoundName = Owner.transform.GetChild(0).name.Substring(0, Owner.transform.GetChild(0).name.Length - 5);
                                        break;
                                    }
                                default: break;
                            } break;
                        }
                    #endregion
                    #region PersName
                    case MuteTypes.PersName:
                        {

                            switch (ActionPlace)
                            {
                                case AudioGO.Golova:
                                    {
                                        SoundName = Owner.transform.parent.parent.parent.parent.tag;
                                        break;
                                    }
                                case AudioGO.Pers:
                                    {
                                        SoundName = Owner.transform.parent.tag;
                                        break;
                                    }
                                case AudioGO.PersControl:
                                    {
                                        SoundName = Owner.tag;
                                        break;
                                    }
                                case AudioGO.Head:
                                    {
                                        SoundName = Owner.transform.parent.parent.parent.tag;
                                        break;
                                    }
                                case AudioGO.HeadControl:
                                    {
                                        SoundName = Owner.transform.parent.parent.tag;
                                        break;
                                    }
                                default: break;
                            }
                        } break;
                    #endregion
                    #region PersHeadName
                    case MuteTypes.PersHeadName:
                        {

                            switch (ActionPlace)
                            {
                                case AudioGO.Golova:
                                    {
                                        SoundName = Owner.transform.parent.parent.tag;
                                        break;
                                    }
                                case AudioGO.PersControl:
                                    {
                                        SoundName = Owner.tag;
                                        break;
                                    }
                                case AudioGO.Head:
                                    {
                                        SoundName = Owner.transform.parent.tag;
                                        break;
                                    }
                                default: break;
                            }
                        } break;
                    #endregion
                    #region SizeName
                    case MuteTypes.SizeName:
                        {
					try{
                            switch (ActionPlace)
                            {
                                case AudioGO.Golova:
                                    {
                                        SoundName = Owner.tag + Owner.transform.parent.parent.tag.Substring(0, Owner.transform.parent.parent.tag.Length - 5);
                                        break;
                                    }
                                case AudioGO.Pers:
                                    {
                                        SoundName = Owner.transform.GetChild(0).tag + Owner.transform.parent.tag.Substring(0, Owner.transform.parent.tag.Length - 5);
                                        break;
                                    }
                                case AudioGO.PersControl:
                                    {
                                        SoundName = Owner.transform.GetChild(0).GetChild(0).tag + Owner.tag.Substring(0, Owner.tag.Length - 5);
                                        break;
                                    }
                                default: break;
						}

                        }
					catch{}
					break;
				}

                    #endregion
                }
            else
                SoundName = Default.Value;
            GameObject camera = Camera.main.gameObject;
            GameObject sound = DoGetChildByName(camera, "Audio:" + SoundName);
            if (sound)
                sound.AddComponent(GetType("RM_MuteAndDestroy"));
            Finish();
        }
        static Type GetType(string name)
        {
            var type = ReflectionUtils.GetGlobalType(name);
            if (type != null) return type;

            type = ReflectionUtils.GetGlobalType("UnityEngine." + name);
            if (type != null) return type;

            type = ReflectionUtils.GetGlobalType("HutongGames.PlayMaker." + name);

            return type;
        }
        static GameObject DoGetChildByName(GameObject root, string name)
        {
            if (root == null)
            {
                return null;
            }

            foreach (Transform child in root.transform)
            {
                if (!string.IsNullOrEmpty(name))
                {
                    if (child.name == name)
                        return child.gameObject;
                }
                var returnObject = DoGetChildByName(child.gameObject, name);
                if (returnObject != null)
                {
                    return returnObject;
                }
            }

            return null;
        }
    }
    public enum MuteTypes
    {
        FoodName,
        PersName,
        PersHeadName,
        ColorName,
        SizeName
    }
}