﻿using UnityEngine;
using System.Collections;
namespace HutongGames.PlayMaker.Actions
{
    [ActionCategory("FF2_Ruslan")]
    [Tooltip("Gets the last Child of a GameObject")]
    public class RZ_GetLastChild : FsmStateAction
    {
        [RequiredField]
        [Tooltip("The GameObject to search.")]
        public FsmOwnerDefault gameObject;

        public FsmInt lastChildIndex;

        [RequiredField]
        [UIHint(UIHint.Variable)]
        [Tooltip("Store the child in a GameObject variable.")]
        public FsmGameObject store;

        public override void Reset()
        {
            gameObject = null;
            lastChildIndex = 0;
            store = null;
        }

        public override void OnEnter()
        {
            store.Value = DoGetLastChild(Fsm.GetOwnerDefaultTarget(gameObject));
            Finish();
        }

        GameObject DoGetLastChild(GameObject go)
        {
            lastChildIndex.Value = go.transform.childCount-1;
            return go == null ? null : go.transform.GetChild(lastChildIndex.Value % go.transform.childCount).gameObject;
        }
            
    }
}
