﻿// (c) Copyright HutongGames, LLC 2010-2013. All rights reserved.

using System;

namespace HutongGames.PlayMaker.Actions
{
    [ActionCategory("FF2_Ruslan")]
	[Tooltip("Sends an Event after an optional delay to many GOs. NOTE: To send events between FSMs they must be marked as Global in the Events Browser.")]
	public class RZ_SendEventToManyGOs : FsmStateAction
	{
		[Tooltip("Where to send the event.")]
		public FsmEventTarget[] eventTarget;
		
		[RequiredField]
		[Tooltip("The event to send. NOTE: Events must be marked Global to send between FSMs.")]
		public FsmEvent sendEvent;
		
		[HasFloatSlider(0, 10)]
		[Tooltip("Optional delay in seconds.")]
		public FsmFloat delay;

		[Tooltip("Repeat every frame. Rarely needed.")]
		public bool everyFrame;

		private DelayedEvent delayedEvent;

		public override void Reset()
		{
			eventTarget = null;
			sendEvent = null;
			delay = null;
			everyFrame = false;
		}

		public override void OnEnter()
		{
			if (delay.Value < 0.001f)
			{
                foreach (FsmEventTarget go in eventTarget)
                {
                    Fsm.Event(go, sendEvent);
                }
				Finish();
			}
			else
			{
                foreach (FsmEventTarget go in eventTarget)
                {
                    delayedEvent = Fsm.DelayedEvent(go, sendEvent, delay.Value);
                }
			}
		}

		public override void OnUpdate()
		{
			if (!everyFrame)
			{
				if (DelayedEvent.WasSent(delayedEvent))
				{
					Finish();
				}
			}
		}
	}
}
