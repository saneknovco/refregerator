﻿using UnityEngine;

namespace HutongGames.PlayMaker.Actions
{
    [ActionCategory("FF2_Ruslan")]
    public class RZ_SetOutlineShaderParametersColorMode : FsmStateAction
    {
        public FsmGameObject[] objs;
		public FsmColor MainColor;
        public FsmFloat OutlineSize, TextureInfluence;
		public FsmColor OutlineColor;
        public FsmBool everyFrame;
        public FsmBool recursive;

        
        public override void OnEnter()
        {
            for (int i = 0; i < objs.Length; i++)
            {
                if (recursive.Value)
                    RecursiveSetParam(objs[i].Value.gameObject);
                else
                    SetParam(objs[i].Value.gameObject);
            }
            if (!everyFrame.Value) 
                Finish();
        }

        public override void OnUpdate()
        {
            for (int i = 0; i < objs.Length; i++)
            {
                RecursiveSetParam(objs[i].Value.gameObject);
            }
        }

        public void RecursiveSetParam(GameObject gameObj)
        {
            SetParam(gameObj);
            for (int j = 0; j < gameObj.transform.childCount; j++)
            {
                SetParam(gameObj.transform.GetChild(j).gameObject);
                RecursiveSetParam(gameObj.transform.GetChild(j).gameObject);
            }
        }

        public void SetParam(GameObject go)
        {
            Renderer rend = go.GetComponent<MeshRenderer>();
            if (rend != null)
            {
                if (rend.material.color != null)
                {
					if(!MainColor.IsNone)
						go.GetComponent<Renderer>().material.color = MainColor.Value;
					if(!OutlineSize.IsNone)
                    	go.GetComponent<Renderer>().material.SetFloat("_OutlineSize", OutlineSize.Value);
					if(!TextureInfluence.IsNone)
                    	go.GetComponent<Renderer>().material.SetFloat("_TexInfluence", TextureInfluence.Value);
					if(!OutlineColor.IsNone)
						go.GetComponent<Renderer>().material.SetVector("_OutlineColor", new Vector4(OutlineColor.Value.r, OutlineColor.Value.g, OutlineColor.Value.b, OutlineColor.Value.a));
				}
            }
            
        }
    }
}
