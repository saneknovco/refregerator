﻿using UnityEngine;
using System.Collections;

namespace HutongGames.PlayMaker.Actions
{
    [ActionCategory("FF2_Ruslan")]
    public class RZ_RemoveStringPart : FsmStateAction
    {
        public FsmString inputStr;
        public FsmString deleteStr;
        public FsmString outputStr;

        public override void OnEnter()
        {
            if (inputStr.Value.Contains(deleteStr.Value))
            {
                int index = inputStr.Value.IndexOf(deleteStr.Value);
                outputStr.Value = inputStr.Value.Remove(index, deleteStr.Value.Length);
                Finish();
            }
        }
    }
}