﻿using UnityEngine;

namespace HutongGames.PlayMaker.Actions
{
    [ActionCategory("FF2_Ruslan")]
    public class RZ_SetOutlineShaderParameters : FsmStateAction
    {
        public FsmGameObject[] objs;
        public FsmFloat A, R, G, B, OutlineSize, TextureInfluence, OutlineR, OutlineG, OutlineB, OutlineA;
        public FsmBool everyFrame;
        public FsmBool recursive;

        
        public override void OnEnter()
        {
            for (int i = 0; i < objs.Length; i++)
            {
                if (recursive.Value)
                    RecursiveSetParam(objs[i].Value.gameObject);
                else
                    SetParam(objs[i].Value.gameObject);
            }
            if (!everyFrame.Value) 
                Finish();
        }

        public override void OnUpdate()
        {
            for (int i = 0; i < objs.Length; i++)
            {
                RecursiveSetParam(objs[i].Value.gameObject);
            }
        }

        public void RecursiveSetParam(GameObject gameObj)
        {
            SetParam(gameObj);
            for (int j = 0; j < gameObj.transform.childCount; j++)
            {
                SetParam(gameObj.transform.GetChild(j).gameObject);
                RecursiveSetParam(gameObj.transform.GetChild(j).gameObject);
            }
        }

        public void SetParam(GameObject go)
        {
            Renderer rend = go.GetComponent<MeshRenderer>();
            if (rend != null)
            {
                if (rend.material.color != null)
                {
                    if (R.Value > 1) R.Value = 1f;
                    if (R.Value < 0) R.Value = 0f;
                    if (G.Value > 1) G.Value = 1f;
                    if (G.Value < 0) G.Value = 0f;
                    if (B.Value > 1) B.Value = 1f;
                    if (B.Value < 0) B.Value = 0f;
                    if (A.Value > 1) A.Value = 1f;
                    if (A.Value < 0) A.Value = 0f;

                    if (OutlineR.Value > 1) OutlineR.Value = 1f;
                    if (OutlineR.Value < 0) OutlineR.Value = 0f;
                    if (OutlineG.Value > 1) OutlineG.Value = 1f;
                    if (OutlineG.Value < 0) OutlineG.Value = 0f;
                    if (OutlineB.Value > 1) OutlineB.Value = 1f;
                    if (OutlineB.Value < 0) OutlineB.Value = 0f;
                    if (OutlineA.Value > 1) OutlineA.Value = 1f;
                    if (OutlineA.Value < 0) OutlineA.Value = 0f;
					if(!R.IsNone && !G.IsNone && !B.IsNone && !A.IsNone)
                    	go.GetComponent<Renderer>().material.color = new Color(R.Value, G.Value, B.Value, A.Value);
					if(!OutlineSize.IsNone)
                    	go.GetComponent<Renderer>().material.SetFloat("_OutlineSize", OutlineSize.Value);
					if(!TextureInfluence.IsNone)
                    	go.GetComponent<Renderer>().material.SetFloat("_TexInfluence", TextureInfluence.Value);
					if(!OutlineR.IsNone && !OutlineG.IsNone && !OutlineB.IsNone && !OutlineA.IsNone)
                    	go.GetComponent<Renderer>().material.SetVector("_OutlineColor", new Vector4(OutlineR.Value, OutlineG.Value, OutlineB.Value, OutlineA.Value));
                }
            }
            
        }
    }
}
