﻿using UnityEngine;
using System.Collections;

public class RN_PICCA_Skalka : MonoBehaviour
{
    Vector3 ownPos, lastOwnPos;
    AudioSource audioSource;

    void Start()
    {
        audioSource = gameObject.GetComponent<AudioSource>();
        ownPos = lastOwnPos = transform.position;
    }

    void Update()
    {
        ownPos = transform.position;

        if (Vector3.Distance(ownPos, lastOwnPos) > 5)
            if (!audioSource.isPlaying)
                audioSource.Play();

        lastOwnPos = transform.position;
    }
}
