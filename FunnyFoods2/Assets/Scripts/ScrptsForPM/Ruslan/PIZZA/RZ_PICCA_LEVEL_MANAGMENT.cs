﻿using UnityEngine;
using System.Collections;

namespace HutongGames.PlayMaker.Actions
{
    [ActionCategory("FF2_Ruslan")]
    public class RZ_PICCA_LEVEL_MANAGMENT : FsmStateAction
    {
        public FsmInt LEVEL;
        public FsmString LevelValue;
        public FsmFloat StartAngle;
        public FsmFloat AddAngle;
        public FsmInt FoodCount;
        public FsmInt ClonesCount;
        public FsmInt ButilkiCount;
        public FsmInt PersCount;
        public FsmInt SousCount;
        public FsmInt BludoCount;
        public FsmInt PiecesCount;
        public FsmFloat PiecesAngle;
        public FsmInt StartBludoPlace;
        public FsmInt TotalFoodCount;
        public FsmInt UnnecessaryFood;
        public FsmFloat PieceScale;
        [UIHint(UIHint.Variable)]
        public FsmEnum BludoMovingValue;
        public FsmBool isNeedBlink;
        public FsmBool isNeedZakaz;
        [UIHint(UIHint.Variable)]
        public FsmEnum BludoControlValue;
        [UIHint(UIHint.Variable)]
        public FsmEnum SkalkaMoveValue;


        int index;
        public override void OnEnter()
        {
            switch (LEVEL.Value)
            {
                case 0:
                    {
                        index = Random.Range(0, 7);
                        switch (index)
                        {
                            case 5: { FoodCount.Value = 5; ClonesCount.Value = 1; break; }
                            case 0: { FoodCount.Value = 5; ClonesCount.Value = 2; break; }
                            case 1: { FoodCount.Value = 5; ClonesCount.Value = 3; break; }
                            case 2: { FoodCount.Value = 4; ClonesCount.Value = 3; break; }
                            case 4: { FoodCount.Value = 3; ClonesCount.Value = 4; break; }
                            case 6: { FoodCount.Value = 3; ClonesCount.Value = 5; break; }
                            default: { FoodCount.Value = 3; ClonesCount.Value = 4; break; }
                        }
                        LevelValue.Value = "1";
                        StartAngle.Value = Random.Range(0, 360);
                        AddAngle.Value = 360/ClonesCount.Value;
                        ButilkiCount.Value = 3;
                        PersCount.Value = 4;
                        SousCount.Value = 0;
                        BludoCount.Value = FoodCount.Value;
                        PiecesCount.Value = PersCount.Value;
                        PiecesAngle.Value = 360 / PersCount.Value;
                        StartBludoPlace.Value = 7;
                        TotalFoodCount.Value = FoodCount.Value * ClonesCount.Value;
                        PieceScale.Value = 0.2f;
                        BludoMovingValue.Value = BludoMovingType.Successively;
                        isNeedBlink.Value = false;
                        isNeedZakaz.Value = false;
                        BludoControlValue.Value = BludoControlType.First;

                        if (SystemInfo.supportsAccelerometer)
                            SkalkaMoveValue.Value = SkalkaMoveType.Accelerometer;
                        else
                            SkalkaMoveValue.Value = SkalkaMoveType.Simple;

                        break;
                    }
                case 1:
                    {
                        LevelValue.Value = "1";
                        StartAngle.Value = 0;
                        AddAngle.Value = 0;
                        FoodCount.Value = 5;
                        ClonesCount.Value = 1;
                        ButilkiCount.Value = 1;
                        PersCount.Value = 2;
                        SousCount.Value = 0;
                        BludoCount.Value = 5;
                        PiecesCount.Value = 2;
                        PiecesAngle.Value = 0;
                        StartBludoPlace.Value = 7;
                        TotalFoodCount.Value = 5;
                        PieceScale.Value = 0.2f;
                        BludoMovingValue.Value = BludoMovingType.Successively;
                        isNeedBlink.Value = false;
                        isNeedZakaz.Value = false;
                        BludoControlValue.Value = BludoControlType.First;
                        SkalkaMoveValue.Value = SkalkaMoveType.Simple;
                        break;
                    }
                case 2:
                    {
                        LevelValue.Value = "2";
                        StartAngle.Value = 0;
                        AddAngle.Value = 180;
                        FoodCount.Value = 5;
                        ClonesCount.Value = 2;
                        ButilkiCount.Value = 2;
                        PersCount.Value = 4;
                        SousCount.Value = 1;
                        BludoCount.Value = 5;
                        PiecesCount.Value = 4;
                        PiecesAngle.Value = 90;
                        StartBludoPlace.Value = 7;
                        TotalFoodCount.Value = 10;
                        PieceScale.Value = 0.3f;
                        BludoMovingValue.Value = BludoMovingType.LoopMove;
                        isNeedBlink.Value = false;
                        isNeedZakaz.Value = false;
                        BludoControlValue.Value = BludoControlType.Second;
                        SkalkaMoveValue.Value = SkalkaMoveType.Simple;
                        break;
                    }
                case 3:
                    {
                        LevelValue.Value = "3";
                        StartAngle.Value = 315;
                        AddAngle.Value = 45;
                        FoodCount.Value = 5;
                        ClonesCount.Value = 3;
                        ButilkiCount.Value = 3;
                        PersCount.Value = 6;
                        SousCount.Value = 3;
                        BludoCount.Value = 5;
                        PiecesCount.Value = 6;
                        PiecesAngle.Value = 60;
                        StartBludoPlace.Value = 7;
                        TotalFoodCount.Value = 15;
                        PieceScale.Value = 0.3f;
                        BludoMovingValue.Value = BludoMovingType.Successively;
                        isNeedBlink.Value = false;
                        isNeedZakaz.Value = false;
                        BludoControlValue.Value = BludoControlType.First;
                        SkalkaMoveValue.Value = SkalkaMoveType.Simple;
                        break;
                    }
                case 4:
                    {
                        LevelValue.Value = "4";
                        StartAngle.Value = Random.Range(0, 360);
                        AddAngle.Value = 120;
                        FoodCount.Value = 5;
                        ClonesCount.Value = 3;
                        ButilkiCount.Value = 3;
                        PersCount.Value = 5;
                        SousCount.Value = 1;
                        BludoCount.Value = 5;
                        PiecesCount.Value = 4;
                        PiecesAngle.Value = 90;
                        StartBludoPlace.Value = 7;
                        TotalFoodCount.Value = 15;
                        PieceScale.Value = 0.3f;
                        BludoMovingValue.Value = BludoMovingType.LoopMove;
                        isNeedBlink.Value = false;
                        isNeedZakaz.Value = true;
                        BludoControlValue.Value = BludoControlType.Second;
                        SkalkaMoveValue.Value = SkalkaMoveType.Simple;
                        break;
                    }
                case 5:
                    {
                        LevelValue.Value = "5";
                        StartAngle.Value = 45;
                        AddAngle.Value = 90;
                        FoodCount.Value = 3;
                        ClonesCount.Value = 4;
                        ButilkiCount.Value = 1;
                        PersCount.Value = 2;
                        SousCount.Value = 1;
                        BludoCount.Value = 3;
                        PiecesCount.Value = 2;
                        PiecesAngle.Value = 0;
                        StartBludoPlace.Value = 5;
                        TotalFoodCount.Value = 12;
                        PieceScale.Value = 0.2f;
                        BludoMovingValue.Value = BludoMovingType.Successively;
                        isNeedBlink.Value = false;
                        isNeedZakaz.Value = false;
                        BludoControlValue.Value = BludoControlType.First;
                        SkalkaMoveValue.Value = SkalkaMoveType.Simple;
                        break;
                    }
                case 6:
                    {
                        LevelValue.Value = "6";
                        StartAngle.Value = 0;
                        AddAngle.Value = 180;
                        FoodCount.Value = 5;
                        ClonesCount.Value = 2;
                        ButilkiCount.Value = 3;
                        PersCount.Value = 5;
                        SousCount.Value = 2;
                        BludoCount.Value = 1;
                        PiecesCount.Value = 4;
                        PiecesAngle.Value = 90;
                        StartBludoPlace.Value = 0;
                        TotalFoodCount.Value = 10;
                        PieceScale.Value = 0.3f;
                        BludoMovingValue.Value = BludoMovingType.Static;
                        isNeedBlink.Value = true;
                        isNeedZakaz.Value = true;
                        BludoControlValue.Value = BludoControlType.Third;
                        SkalkaMoveValue.Value = SkalkaMoveType.TwoTouches;
                        break;
                    }
                case 7:
                    {
                        LevelValue.Value = "7";
                        StartAngle.Value = 315;
                        AddAngle.Value = 45;
                        FoodCount.Value = 5;
                        ClonesCount.Value = 3;
                        ButilkiCount.Value = 3;
                        PersCount.Value = 7;
                        SousCount.Value = 1;
                        BludoCount.Value = 5;
                        PiecesCount.Value = 6;
                        PiecesAngle.Value = 60;
                        StartBludoPlace.Value = 7;
                        TotalFoodCount.Value = 15;
                        PieceScale.Value = 0.3f;
                        BludoMovingValue.Value = BludoMovingType.Successively;
                        isNeedBlink.Value = false;
                        isNeedZakaz.Value = true;
                        BludoControlValue.Value = BludoControlType.First;
                        SkalkaMoveValue.Value = SkalkaMoveType.TwoTouches;
                        break;
                    }
                case 8:
                    {
                        LevelValue.Value = "8";
                        StartAngle.Value = Random.Range(0, 360);
                        AddAngle.Value = 90;
                        FoodCount.Value = 3;
                        ClonesCount.Value = 4;
                        ButilkiCount.Value = 3;
                        PersCount.Value = 5;
                        SousCount.Value = 3;
                        BludoCount.Value = 3;
                        PiecesCount.Value = 4;
                        PiecesAngle.Value = 90;
                        StartBludoPlace.Value = 7;
                        TotalFoodCount.Value = 12;
                        PieceScale.Value = 0.3f;
                        BludoMovingValue.Value = BludoMovingType.LoopMove;
                        isNeedBlink.Value = false;
                        isNeedZakaz.Value = true;
                        BludoControlValue.Value = BludoControlType.Second;
                        SkalkaMoveValue.Value = SkalkaMoveType.TwoTouches;
                        break;
                    }
                case 9:
                    {
                        LevelValue.Value = "9";
                        StartAngle.Value = Random.Range(0, 360);
                        AddAngle.Value = 120;
                        FoodCount.Value = 5;
                        ClonesCount.Value = 3;
                        ButilkiCount.Value = 3;
                        PersCount.Value = 3;
                        SousCount.Value = 2;
                        BludoCount.Value = 1;
                        PiecesCount.Value = 2;
                        PiecesAngle.Value = 0;
                        StartBludoPlace.Value = 0;
                        TotalFoodCount.Value = 15;
                        //UnnecessaryFoodCount.Value = 3;
                        PieceScale.Value = 0.2f;
                        BludoMovingValue.Value = BludoMovingType.Static;
                        isNeedBlink.Value = true;
                        isNeedZakaz.Value = true;
                        BludoControlValue.Value = BludoControlType.Third;
                        SkalkaMoveValue.Value = SkalkaMoveType.TwoTouches;
                        break;
                    }
                case 10:
                    {
                        LevelValue.Value = "10";
                        StartAngle.Value = Random.Range(0, 360);
                        AddAngle.Value = 72;
                        FoodCount.Value = 3;
                        ClonesCount.Value = 5;
                        ButilkiCount.Value = 3;
                        PersCount.Value = 6;
                        SousCount.Value = 1;
                        BludoCount.Value = 3;
                        PiecesCount.Value = 6;
                        PiecesAngle.Value = 60;
                        StartBludoPlace.Value = 7;
                        TotalFoodCount.Value = 15;
                        UnnecessaryFood.Value = 3;
                        PieceScale.Value = 0.3f;
                        BludoMovingValue.Value = BludoMovingType.LoopMove;
                        isNeedBlink.Value = false;
                        isNeedZakaz.Value = false;
                        BludoControlValue.Value = BludoControlType.Second;
                        SkalkaMoveValue.Value = SkalkaMoveType.TwoTouches;
                        break;
                    }
                case 11:
                    {
                        LevelValue.Value = "11";
                        StartAngle.Value = 45;
                        AddAngle.Value = 90;
                        FoodCount.Value = 5;
                        ClonesCount.Value = 2;
                        ButilkiCount.Value = 3;
                        PersCount.Value = 3;
                        SousCount.Value = 1;
                        BludoCount.Value = 5;
                        PiecesCount.Value = 2;
                        PiecesAngle.Value = 0;
                        StartBludoPlace.Value = 7;
                        TotalFoodCount.Value = 10;
                        PieceScale.Value = 0.2f;
                        BludoMovingValue.Value = BludoMovingType.Successively;
                        isNeedBlink.Value = false;
                        isNeedZakaz.Value = true;
                        BludoControlValue.Value = BludoControlType.First;
                        SkalkaMoveValue.Value = SkalkaMoveType.Accelerometer;
                        break;
                    }
                case 12:
                    {
                        LevelValue.Value = "12";
                        StartAngle.Value = Random.Range(0, 360);
                        AddAngle.Value = 90;
                        FoodCount.Value = 3;
                        ClonesCount.Value = 4;
                        ButilkiCount.Value = 3;
                        PersCount.Value = 5;
                        SousCount.Value = 3;
                        BludoCount.Value = 3;
                        PiecesCount.Value = 4;
                        PiecesAngle.Value = 90;
                        StartBludoPlace.Value = 7;
                        TotalFoodCount.Value = 12;
                        UnnecessaryFood.Value = 3;
                        PieceScale.Value = 0.3f;
                        BludoMovingValue.Value = BludoMovingType.LoopMove;
                        isNeedBlink.Value = false;
                        isNeedZakaz.Value = true;
                        BludoControlValue.Value = BludoControlType.Second;
                        SkalkaMoveValue.Value = SkalkaMoveType.Accelerometer;
                        break;
                    }
                case 13:
                    {
                        LevelValue.Value = "13";
                        StartAngle.Value = Random.Range(0, 360);
                        AddAngle.Value = 90;
                        FoodCount.Value = 3;
                        ClonesCount.Value = 4;
                        ButilkiCount.Value = 3;
                        PersCount.Value = 7;
                        SousCount.Value = 2;
                        BludoCount.Value = 2;
                        PiecesCount.Value = 6;
                        PiecesAngle.Value = 60;
                        StartBludoPlace.Value = 0;
                        TotalFoodCount.Value = 12;
                        UnnecessaryFood.Value = 12;
                        PieceScale.Value = 0.3f;
                        BludoMovingValue.Value = BludoMovingType.LoopMove;
                        isNeedBlink.Value = true;
                        isNeedZakaz.Value = true;
                        BludoControlValue.Value = BludoControlType.Fourth;
                        SkalkaMoveValue.Value = SkalkaMoveType.Accelerometer;
                        break;
                    }
                case 14:
                    {
                        LevelValue.Value = "14";
                        StartAngle.Value = Random.Range(0, 360);
                        AddAngle.Value = 90;
                        FoodCount.Value = 3;
                        ClonesCount.Value = 4;
                        ButilkiCount.Value = 3;
                        PersCount.Value = 4;
                        SousCount.Value = 1;
                        BludoCount.Value = 1;
                        PiecesCount.Value = 4;
                        PiecesAngle.Value = 90;
                        StartBludoPlace.Value = 0;
                        TotalFoodCount.Value = 12;
                        PieceScale.Value = 0.3f;
                        BludoMovingValue.Value = BludoMovingType.Static;
                        isNeedBlink.Value = true;
                        isNeedZakaz.Value = false;
                        BludoControlValue.Value = BludoControlType.Third;
                        SkalkaMoveValue.Value = SkalkaMoveType.Accelerometer;
                        break;
                    }
                case 15:
                    {
                        LevelValue.Value = "15";
                        StartAngle.Value = Random.Range(0, 360);
                        AddAngle.Value = 72;
                        FoodCount.Value = 3;
                        ClonesCount.Value = 5;
                        ButilkiCount.Value = 3;
                        PersCount.Value = 7;
                        SousCount.Value = 1;
                        BludoCount.Value = 3;
                        PiecesCount.Value = 6;
                        PiecesAngle.Value = 60;
                        StartBludoPlace.Value = 0;
                        TotalFoodCount.Value = 15;
                        UnnecessaryFood.Value = 20;
                        PieceScale.Value = 0.3f;
                        BludoMovingValue.Value = BludoMovingType.LoopMove;
                        isNeedBlink.Value = true;
                        isNeedZakaz.Value = true;
                        BludoControlValue.Value = BludoControlType.Fifth;
                        SkalkaMoveValue.Value = SkalkaMoveType.Accelerometer;
                        break;
                    }
            }
            Finish();
        }

       public enum BludoMovingType
        {
            Successively,
            LoopMove,
            Static
        }

       public enum BludoControlType
       {
           First,
           Second,
           Third,
           Fourth,
           Fifth
       }

        public enum SkalkaMoveType
        {
            Simple,
            TwoTouches,
            Accelerometer
        }
    }
}