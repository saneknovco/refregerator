﻿using UnityEngine;

namespace HutongGames.PlayMaker.Actions
{
    [ActionCategory("FF2_Ruslan")]
    public class RZ_Ruslan_CutAlpha : FsmStateAction
    {
        [RequiredField]
        [Tooltip("The GameObject to set.")]
        public FsmOwnerDefault gameObject;
        public FsmInt Iteration;
        string Counter;
        public FsmFloat Angle, Wscale, RValue, Alpha;
        public FsmColor Color;
        public FsmBool newPunktir_offAllParams, everyFrame;
        public FsmTexture punktir;
        GameObject go;

        public override void OnEnter()
        {
            switch (Iteration.Value)
            {
                case 0: Counter = ""; break;
                case 1: { Counter = "2"; break; }
                case 2: { Counter = "3"; break; }
            }
            go = Fsm.GetOwnerDefaultTarget(gameObject);
            SetAlphaCut();
            if (!everyFrame.Value) Finish();
        }
        public override void OnUpdate()
        {
            SetAlphaCut();
        }
        public void SetAlphaCut()
        {

            if (newPunktir_offAllParams.Value == true)
            {
                go.GetComponent<Renderer>().material.SetTexture("_MaskedTex" + Counter, punktir.Value);
                go.GetComponent<Renderer>().material.SetColor("_MaskedColor" + Counter, new Color(1, 1, 1, 1));
            }
            else
            {
                go.GetComponent<Renderer>().material.SetFloat("_Angle" + Counter, Angle.Value);

                if (Wscale.Value != null)
                    go.GetComponent<Renderer>().material.SetVector("_PosScale" + Counter, new Vector4(0, 0, 1, Wscale.Value));

                if (Color.Value != null)
                {
                    if (Alpha.Value != null && !Alpha.IsNone)
                        go.GetComponent<Renderer>().material.SetColor("_MaskedColor" + Counter, new Color(Color.Value.r, Color.Value.g, Color.Value.b, Alpha.Value));
                    else
                        go.GetComponent<Renderer>().material.SetColor("_MaskedColor" + Counter, Color.Value);
                }

                if (RValue.Value != null && !RValue.IsNone)
                    go.GetComponent<Renderer>().material.SetVector("_MaskedColor" + Counter, new Vector4(RValue.Value, 0, 0, 1));
            }
        }
    }
}
