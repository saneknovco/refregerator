﻿using UnityEngine;
using System.Collections;

namespace HutongGames.PlayMaker.Actions
{
    public class RZ_SendEventToChild : FsmStateAction
    {
        public FsmOwnerDefault gameObject;
        public FsmEvent sendEvent;
        public FsmInt[] childLevel;
        public FsmBool recursive;

        GameObject GO, childGO;
        PlayMakerFSM pm;

        public override void OnEnter()
        {
            GO = Fsm.GetOwnerDefaultTarget(gameObject);
            if (childLevel.Length > 1)
            {
                for (int i = 0; i < childLevel.Length; i++)
                {
                    if (GO.transform.childCount - 1 >= childLevel[i].Value)
                    {
                        GO = GO.transform.GetChild(childLevel[i].Value).gameObject;
                        if (recursive.Value)
                            if (pm = GO.GetComponent<PlayMakerFSM>())
                                pm.SendEvent(sendEvent.Name);
                    }
                    else
                    {
                        Debug.LogError("No child level, index " + i);
                        break;
                    }
                }
                if (pm = GO.GetComponent<PlayMakerFSM>())
                    pm.SendEvent(sendEvent.Name);
            }
            else
            {
                if (GO.transform.childCount > 0)
                    if (GO.transform.GetChild(childLevel[0].Value).GetComponent<PlayMakerFSM>())
                        GO.transform.GetChild(childLevel[0].Value).GetComponent<PlayMakerFSM>().SendEvent(sendEvent.Name);
            }
            Finish();
        }
    }
}