﻿using UnityEngine;
using Bini.Utils.Audio;


namespace HutongGames.PlayMaker.Actions
{
    [ActionCategory("FF2_Ruslan")]
    public class RZ_PlayClipByName : FsmStateAction
    {
        public FsmString clipName;
        public FsmEvent finishEvent;
        public FsmFloat volume = 1f, speed = 1f;
        float length, time;

        public override void OnEnter()
        {
            AudioClip clip = Resources.Load("Sounds/" + clipName.Value) as AudioClip;
            if (clip == null)
                Finish();
            else
            {
                length = clip.length;
                time = 0;
                Audio.PlaySound(clip, volume.Value, speed.Value);
            }
            if (finishEvent == null)
                Finish();
        }
        public override void OnUpdate()
        {
            time += Time.deltaTime;
            if (time > length)
            {
                if (finishEvent != null)
                    Fsm.Event(finishEvent);
                Finish();
            }
        }
    }
}