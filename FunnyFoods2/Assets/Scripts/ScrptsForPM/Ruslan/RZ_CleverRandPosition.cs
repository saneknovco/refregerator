﻿using UnityEngine;
using System.Collections;

namespace HutongGames.PlayMaker.Actions
{
    public class RZ_CleverRandPosition : FsmStateAction
    {
        public FsmGameObject go;
        public FsmGameObject eda;

        float xPos, yPos, SizeXR, SizeYU, SizeXL, SizeYD;

        public override void OnEnter()
        {
            float distance = 0;
            float Angle = 0;
            float randX;
            float randY;
            xPos = go.Value.transform.position.x;
            yPos = go.Value.transform.position.y;
            SizeXR = xPos + go.Value.GetComponent<BoxCollider>().size.x / 2 - 100;
            SizeYU = yPos + go.Value.GetComponent<BoxCollider>().size.y / 2 - 100;
            SizeXL = xPos - go.Value.GetComponent<BoxCollider>().size.x / 2 + 100;
            SizeYD = yPos - go.Value.GetComponent<BoxCollider>().size.y / 2 + 100;
            GameObject food;
            for (int i = 0; i < eda.Value.transform.childCount; i++)
            {
                Angle += 72;
                food = eda.Value.transform.GetChild(i % eda.Value.transform.childCount).gameObject;
                randX = Random.Range(SizeXL, SizeXR);
                randY = Random.Range(SizeYD, SizeYU);
                for (int j = 0; j < go.Value.transform.childCount; j++)
                {
                    Vector3 foodInBlyudo = go.Value.transform.GetChild(j % go.Value.transform.childCount).gameObject.transform.position;
                    foodInBlyudo = new Vector3(foodInBlyudo.x, foodInBlyudo.y, 100);
                    distance = Vector3.Distance(new Vector3(randX, randY, 100), foodInBlyudo);
                    if (distance < 10) break;
                }
                food.transform.position = new Vector3(randX, randY, 100);
                food.transform.eulerAngles = new Vector3(0, 0, Angle);
                food.transform.parent = go.Value.gameObject.transform;
            }
        }
    }
}
