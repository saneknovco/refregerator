﻿// (c) Copyright HutongGames, LLC 2010-2013. All rights reserved.

using UnityEngine;

namespace HutongGames.PlayMaker.Actions
{
    [ActionCategory("FF2_Ruslan")]
    public class RZ_GetChildNum : FsmStateAction
    {
        [RequiredField]
        [Tooltip("The GameObject to search.")]
        public FsmOwnerDefault gameObject;

        [RequiredField]
        [Tooltip("The index of the child to find.")]
        public FsmInt childIndex;
        public FsmEvent NoChildren;
        [RequiredField]
        [UIHint(UIHint.Variable)]
        [Tooltip("Store the child in a GameObject variable.")]
        public FsmGameObject store;
        public FsmBool WaitAnyFrames;
        int i = 0;


        public override void Reset()
        {
            gameObject = null;
            childIndex = 0;
            store = null;
        }

        public override void OnEnter()
        {
            store.Value = DoGetChildNum(Fsm.GetOwnerDefaultTarget(gameObject));

            Finish();
        }

        GameObject DoGetChildNum(GameObject go)
        {
            if (go.transform.childCount != 0)
                return go == null ? null : go.transform.GetChild(childIndex.Value % go.transform.childCount).gameObject;
            else
            {
                if(NoChildren.Name != "")
                {
                    Fsm.Event(NoChildren);
                    return null;
                }
                else
                {
                    Finish();
                    return null;
                }
            }
        }
        
        public override void OnUpdate()
        {
            if(WaitAnyFrames.Value == true)
            {
                i++;
                if(i==5)
                    store.Value = DoGetChildNum(Fsm.GetOwnerDefaultTarget(gameObject));
            }
        }
    }
}