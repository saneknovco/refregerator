﻿// (c) Copyright HutongGames, LLC 2010-2013. All rights reserved.

using UnityEngine;

namespace HutongGames.PlayMaker.Actions
{
    [ActionCategory("FF2_Ruslan")]
    [Tooltip("Destroys a Game Object in 1 frame")]
    public class RZ_FastDestroyObject : FsmStateAction
    {
        [RequiredField]
        [Tooltip("The GameObject to destroy.")]
        public FsmGameObject gameObject;

        [Tooltip("Detach children before destroying the Game Object.")]
        public FsmBool detachChildren;


        public override void Reset()
        {
            gameObject = null;
        }

        public override void OnEnter()
        {
            var go = gameObject.Value;

            if (go != null)
            {
                go.transform.parent = null;
                Object.Destroy(go);

                if (detachChildren.Value)
                    go.transform.DetachChildren();
            }
            Finish();
        }
    }
}