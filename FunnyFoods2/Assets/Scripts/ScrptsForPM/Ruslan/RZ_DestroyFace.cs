﻿// (c) Copyright HutongGames, LLC 2010-2013. All rights reserved.

using UnityEngine;

namespace HutongGames.PlayMaker.Actions
{
    [ActionCategory("FF2_Ruslan")]
    public class RZ_DestroyFace : FsmStateAction
    {
        [RequiredField]
        [Tooltip("The GameObject to search.")]
        public FsmOwnerDefault gameObject;
        public FsmBool DisactiveFace;

        public override void OnEnter()
        {
            DoGetChildByName(Fsm.GetOwnerDefaultTarget(gameObject));
        }

        void DoGetChildByName(GameObject root)
        {
            foreach (Transform child in root.transform)
            {
                if (child.name == "ROT_CONTROL" || child.name == "GLAZ_R_CONTROL" || child.name == "GLAZ_L_CONTROL")
                {
                    if (DisactiveFace.Value == true)
                        child.gameObject.SetActive(false);
                    else
                        GameObject.Destroy(child.gameObject);
                }

                DoGetChildByName(child.gameObject);
            }
            Finish();
        }
    }
}