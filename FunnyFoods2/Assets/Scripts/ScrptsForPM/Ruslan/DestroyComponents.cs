﻿// (c) Copyright HutongGames, LLC 2010-2013. All rights reserved.

using UnityEngine;

namespace HutongGames.PlayMaker.Actions
{
    [ActionCategory("Ruslan/FunnyFoods2+")]
    public class DestroyComponents : FsmStateAction
    {
        [Tooltip("This parameter off your settings, gets GameObjects that owns the Components and recursive destroy Animation, PM, Collider and Looker")]
        public FsmBool LightVersion = true;
        
        [RequiredField]
        [Tooltip("The GameObjects that owns the Components.")]
        public FsmOwnerDefault[] gameObject;

        [RequiredField]
        [UIHint(UIHint.ScriptComponent)]
        [Tooltip("The name of the Components to destroy.")]
        public FsmString[] component;

        public FsmBool Recursive;

        Component aComponent;

        public override void Reset()
        {
            aComponent = null;
            gameObject = null;
            component = null;
        }

        public override void OnEnter()
        {
            if (LightVersion.Value)
                Recursive.Value = true;
            for (int i = 0; i < gameObject.Length; i++)
            {
                if(!Recursive.Value)
                    DoDestroyComponent(gameObject[i].OwnerOption == OwnerDefaultOption.UseOwner ? Owner : gameObject[i].GameObject.Value);
                else
                    RecursiveDoDestroyComponent(gameObject[i].OwnerOption == OwnerDefaultOption.UseOwner ? Owner : gameObject[i].GameObject.Value);
            }
            Finish();
        }

        public void RecursiveDoDestroyComponent(GameObject gameObj)
        {
            DoDestroyComponent(gameObj);
            for (int j = 0; j < gameObj.transform.childCount; j++)
            {
                DoDestroyComponent(gameObj.transform.GetChild(j).gameObject);
                RecursiveDoDestroyComponent(gameObj.transform.GetChild(j).gameObject);
            }
        }

        public void DoDestroyComponent(GameObject go)
        {
            if (LightVersion.Value)
            {
                aComponent = go.GetComponent<BoxCollider>();
                if (aComponent != null)
                    Object.Destroy(aComponent);
                aComponent = go.GetComponent<Looker>();
                if (aComponent != null)
                    Object.Destroy(aComponent);
                aComponent = go.GetComponent<PlayMakerFSM>();
                if (aComponent != null)
                    Object.Destroy(aComponent);
                aComponent = go.GetComponent<Animation>();
                if (aComponent != null)
                    Object.Destroy(aComponent);
                aComponent = go.GetComponent<SphereCollider>();
                if (aComponent != null)
                    Object.Destroy(aComponent);
                aComponent = go.GetComponent<CapsuleCollider>();
                if (aComponent != null)
                    Object.Destroy(aComponent);
                aComponent = go.GetComponent<PolygonCollider2D>();
                if (aComponent != null)
                    Object.Destroy(aComponent);
                aComponent = go.GetComponent<ConstantForce2D>();
                if (aComponent != null)
                    Object.Destroy(aComponent);
                aComponent = go.GetComponent<Rigidbody2D>();
                if (aComponent != null)
                    Object.Destroy(aComponent);
                aComponent = go.GetComponent<Rigidbody>();
                if (aComponent != null)
                    Object.Destroy(aComponent);
                //aComponent = go.GetComponent<SexTagger>();
                //if (aComponent != null)
                //    Object.Destroy(aComponent);
				aComponent = go.GetComponent<MeshCollider>();
				if (aComponent != null)
					Object.Destroy(aComponent);
                aComponent = go.GetComponent<RN_PICCA_Skalka>();
                if (aComponent != null)
                    Object.Destroy(aComponent);
                aComponent = go.GetComponent<AudioSource>();
                if (aComponent != null)
                    Object.Destroy(aComponent);
            }
            else
                for (int j = 0; j < component.Length; j++)
                {
                    aComponent = go.GetComponent(component[j].Value);
                    if (aComponent != null)
                        Object.Destroy(aComponent);
                }

        }
    }
}