﻿using UnityEngine;
using System.Collections;

namespace HutongGames.PlayMaker.Actions
{
    public class RZ_GetDistance : FsmStateAction
    {
        public FsmOwnerDefault GOfrom;
        public FsmOwnerDefault GOto;
        public FsmFloat targetDistance;
        public FsmEvent Exit;
        public Distance GreaterLess;
        GameObject from, to;
        Vector2 fromPos, toPos;
        float distance;

        public override void OnEnter()
        {
            from = Fsm.GetOwnerDefaultTarget(GOfrom);
            to = Fsm.GetOwnerDefaultTarget(GOto);
        }
        public override void OnUpdate()
        {
            if (!from || !to)
                Finish();
            fromPos = from.transform.position;
            toPos = to.transform.position;
            distance = Vector2.Distance(fromPos, toPos);
            switch (GreaterLess)
            {
                case Distance.Greater:
                    {
                        if (distance >= targetDistance.Value)
                            Fsm.Event(Exit);
                        break;
                    }
                case Distance.Less:
                    {
                        if (distance <= targetDistance.Value)
                            Fsm.Event(Exit);
                        break;
                    }
            }
        }
    }
    public enum Distance
    {
        Greater, Less
    }
}
