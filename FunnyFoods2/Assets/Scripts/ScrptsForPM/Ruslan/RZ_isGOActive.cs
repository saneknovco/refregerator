﻿using UnityEngine;
using System.Collections;

namespace HutongGames.PlayMaker.Actions
{
    public class RZ_isGOActive : FsmStateAction {

        public FsmOwnerDefault gameObject;
        public FsmEvent goIsActive, goIsNotActive;

        public override void OnEnter()
        {
            if (Fsm.GetOwnerDefaultTarget(gameObject).activeInHierarchy)
            {
                if (goIsActive != null)
                    Fsm.Event(goIsActive);
                else
                    Finish();
            }
            else
            {
                if (goIsNotActive != null)
                    Fsm.Event(goIsNotActive);
                else
                    Finish();
            }
        }
    }
}