﻿
using UnityEngine;

namespace HutongGames.PlayMaker.Actions
{
    [ActionCategory("FF2_Ruslan")]
    [Tooltip("Sets an Integer Variable to a random value between Min/Max except some value.")]
    public class RZ_RandIntWithParams : FsmStateAction
    {
		[RequiredField]
		public FsmInt min;
		[RequiredField]
		public FsmInt max;
		[RequiredField]
		[UIHint(UIHint.Variable)]
		public FsmInt storeResult;
        public FsmInt except;
        public FsmBool on;

        [Tooltip("Should the Max value be included in the possible results?")]
        public bool inclusiveMax;

		public override void Reset()
		{
			min = 0;
			max = 100;
			storeResult = null;
			// make default false to not break old behavior.
		    inclusiveMax = false;
		}

		public override void OnEnter()
		{

            if (on.Value)
            {
                do
                    storeResult.Value = (inclusiveMax) ? Random.Range(min.Value, max.Value + 1) : Random.Range(min.Value, max.Value);
                while (storeResult.Value == except.Value);
            } 
            Finish();
		}
    }
}