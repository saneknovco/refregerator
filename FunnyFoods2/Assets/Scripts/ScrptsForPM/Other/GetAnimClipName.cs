﻿using System;
using UnityEngine;

namespace HutongGames.PlayMaker.Actions
{
	[Tooltip("Not Work With -1 Speed!!!")]
	public class GetAnimClipName : FsmStateAction
	{
		[RequiredField]
		[Tooltip("Game Object to play the animation on.")]
		public FsmOwnerDefault gameObject;
		
		[UIHint(UIHint.Animation)]
		[Tooltip("The name of the animation to play.")]
		public FsmString animName;
		public FsmString Store;
			
		public override void OnEnter()
		{
		Store.Value = animName.Value;
			Finish();
		}
	}
}  