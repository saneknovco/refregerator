﻿using UnityEngine;
using Bini.Utils.Audio;

namespace HutongGames.PlayMaker.Actions
{
    public class PlayAudioClip : FsmStateAction
    {
        public FsmString clipName;
        public FsmEvent startEvent, finishEvent;
        public FsmFloat volume = 1f, speed = 1f;
        float length, time;
        public override void OnEnter()
        {
            AudioClip clip = Resources.Load("Sounds/" + clipName.Value) as AudioClip;
            if (clip == null) 
                Finish();
            else
            {
                length = clip.length;
                time = 0;
                Audio.PlaySound(clip, volume.Value, speed.Value);
                Fsm.Event(startEvent);
                if (finishEvent != null)
                    Finish();
            }
        }
        public override void OnUpdate()
        {
            time += Time.deltaTime;
            if (time > length)
            {
                if (finishEvent != null)
                    Fsm.Event(finishEvent);
                Finish();
            }
        }
    }
}