﻿using UnityEngine;

namespace HutongGames.PlayMaker.Actions
{
	[Tooltip("Verification is anim plaing")]
	public class SetVector2 : FsmStateAction 
	{
		public FsmVector2 Vector;
		public FsmFloat x,y;
		public override void OnEnter() 
		{
			try
			{
				Vector.Value = new Vector2(x.Value,y.Value);
			}
			catch { }
			Finish();
		}
		
	}
} 