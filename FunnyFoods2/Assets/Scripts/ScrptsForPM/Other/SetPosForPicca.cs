﻿// (c) Copyright HutongGames, LLC 2010-2013. All rights reserved.

using UnityEngine;

namespace HutongGames.PlayMaker.Actions
{
    public class SetPosForPicca : FsmStateAction
    {
        public FsmOwnerDefault gameObject;
        public FsmInt Iteration;

        public override void OnEnter()
        {
            var go = Fsm.GetOwnerDefaultTarget(gameObject);
            
            if (go == null)
                return;

            if (Iteration.Value == 0)
                go.transform.localPosition = new Vector3(-33, -11, 0);
            if (Iteration.Value == 1)
                go.transform.localPosition = new Vector3(115, -130, 0);
            if (Iteration.Value == 2)
                go.transform.localPosition = new Vector3(70, -25, 0);
                Finish();
        }
    }
}