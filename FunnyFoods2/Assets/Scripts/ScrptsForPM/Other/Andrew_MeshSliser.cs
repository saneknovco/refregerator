﻿using UnityEngine;

namespace HutongGames.PlayMaker.Actions
{
    [ActionCategory("Andrew_UniAction")]
    public class Andrew_MeshSliser : FsmStateAction
    {
        public FsmOwnerDefault fromGameObject;
        public FsmGameObject GOWithFSMTemplate;
        public FsmVector2[] points = new FsmVector2[3];
        public FsmInt completedVersion = 0;
        public FsmVector2 center;
        public FsmString NameStartForGO, varName, siluetteShName;
        public FsmFloat NewPosKoef;
        public FsmFloat NewPosKoefSiluet;
        public FsmFloat Speed;
        public FsmBool OffSiluet, SetAlphaMapTexture, ForPicca;
        public FsmTexture AlphaMap;
        //		float xmin,xmax,ymin,ymax;
        float x, y;
        Vector2[] Points;
        Vector2 min, max;
        Texture tex;
        Shader siluette;// = Shader.Find();
        Vector2 zero = Vector2.zero, up = Vector2.up, one = Vector2.one, right = Vector2.right;
        //		Vector3[] triangles,verticles,uv;
        public override void OnEnter()
        {
            GameObject go = Fsm.GetOwnerDefaultTarget(fromGameObject);
            GameObject detalsParent = new GameObject();
            GameObject shadowsParent = new GameObject();
            detalsParent.name = NameStartForGO.Value + "Parent";
            shadowsParent.name = NameStartForGO.Value + "Siluette_Parent";
            detalsParent.transform.parent = go.transform;
            shadowsParent.transform.parent = go.transform;
            detalsParent.transform.localPosition = Vector3.zero;
            shadowsParent.transform.localPosition = Vector3.zero;
            if (OffSiluet.Value == true)
                shadowsParent.SetActive(false);
            detalsParent.transform.localScale = Vector3.one;
            shadowsParent.transform.localScale = Vector3.one;
            FsmTemplate fsmTemp = GOWithFSMTemplate.Value.GetComponent<PlayMakerFSM>().FsmTemplate;
            MeshFilter M = go.GetComponent<MeshFilter>();
            tex = go.GetComponent<Renderer>().material.mainTexture;
            //			transparent = Shader.Find(goodShName.Value);
            if(SetAlphaMapTexture.Value == true)
                siluette = Shader.Find("Unlit/AlphaSelfIllumWithFade");
            else
                siluette = Shader.Find(siluetteShName.Value);
            Bounds b = M.mesh.bounds;
            min = b.min;
            max = b.max;
            ArrayCreator();
            //			ymin = b.min.y;
            //			ymax = b.max.y;
            for (int i = 0; i < Points.Length; i++)
            {
                GameObject good = new GameObject();
                good.transform.parent = detalsParent.transform;
                GameObject shad = new GameObject();
                
                if (OffSiluet.Value == false)
                    shad.transform.parent = shadowsParent.transform;
                
                good.name = NameStartForGO.Value + (i + 1);
                good.transform.position = go.transform.position + Vector3.back;
                
                var fsm = good.AddComponent<PlayMakerFSM>();
                
                fsm.SetFsmTemplate(fsmTemp);
                if (OffSiluet.Value == false)
                    fsm.FsmVariables.GetFsmGameObject(varName.Value).Value = shad;
                if (OffSiluet.Value == false)
                    shad.name = NameStartForGO.Value + (i + 1) + "_siluette";
                if (OffSiluet.Value == false)
                    shad.transform.position = go.transform.position;
                good.AddComponent<MeshRenderer>();
                if (OffSiluet.Value == false)
                    shad.AddComponent<MeshRenderer>();
                Material mTr = new Material(Shader.Find("Unlit/AlphaSelfIllumWithFade"));
                mTr.mainTexture = go.GetComponent<Renderer>().material.mainTexture;
                good.GetComponent<Renderer>().material = mTr;
                if(SetAlphaMapTexture.Value == true)
                    good.GetComponent<Renderer>().material.SetTexture("_AlphaMap", AlphaMap.Value);
                good.GetComponent<Renderer>().useLightProbes = false;
                good.GetComponent<Renderer>().receiveShadows = false;
                good.GetComponent<Renderer>().shadowCastingMode = UnityEngine.Rendering.ShadowCastingMode.Off;
                Material mat = new Material(siluette);
                mat.mainTexture = go.GetComponent<Renderer>().material.mainTexture;
                //				mat.shader = siluette;
                if (OffSiluet.Value == false)
                    shad.GetComponent<Renderer>().material = mat;
                if (OffSiluet.Value == false)
                    if (SetAlphaMapTexture.Value == true)
                        shad.GetComponent<Renderer>().material.SetTexture("_AlphaMap", AlphaMap.Value);
                Creator(Points[i], Points[(i + 1) % Points.Length], center.Value, good, shad);
            }
            go.GetComponent<Renderer>().enabled = false;
            Finish();
        }
        public void ArrayCreator()
        {
            if (points.Length < 2)
            {
                int n = completedVersion.Value;
                if (n == 0)
                    n = Random.Range(1, 5);
                if (n > 0)
                    switch (n)
                    {
                        case 1: Points = new Vector2[] { zero, up, one, right }; break;
                        case 2: Points = new Vector2[] { up / 2, new Vector2(0.5f, 1), new Vector2(1, 0.5f), right / 2 }; break;
                        case 3: Points = new Vector2[] { up / 1.33f, new Vector2(0.75f, 1), new Vector2(1, 0.75f), right / 1.33f }; break;
                        case 4: Points = new Vector2[] { up / 3, new Vector2(0.33f, 1), new Vector2(1, 0.33f), right / 3 }; break;
                    }
                else
                {
                    float f1 = 0, f2 = 0, f3 = 0, f4 = 5f;
                    while (f4 > 4)
                    {
                        f1 = Random.Range(0.65f, 1.2f);
                        f2 = f1 + Random.Range(0.65f, 1.2f);
                        f3 = f2 + Random.Range(0.65f, 1.2f);
                        f4 = f3 + Random.Range(0.65f, 1.2f);
                    }
                    Points = new Vector2[] { P(f1), P(f2), P(f3), P(f4) };
                }
            }
            else
            {
                Points = new Vector2[points.Length];
                for (int i = 0; i < points.Length; i++)
                    Points[i] = points[i].Value;
            }
            float[] pointsFloat = new float[Points.Length];
            for (int i = 0; i < pointsFloat.Length; i++)
            {
                pointsFloat[i] = F(Points[i]);
            }
            for (int i = 0; i < pointsFloat.Length - 1; i++)
            {
                for (int j = 0; j < pointsFloat.Length - 1 - i; j++)
                    if (pointsFloat[j] > pointsFloat[j + 1])
                    {
                        float pf = pointsFloat[j];
                        pointsFloat[j] = pointsFloat[j + 1];
                        pointsFloat[j + 1] = pf;
                    }
            }
            for (int i = 0; i < pointsFloat.Length; i++)
            {
                Points[i] = P(pointsFloat[i]);
                //				Debug.Log(Points[i]);
            }
        }
        public float F(Vector2 point)
        {
            if (point == Vector2.zero) return 0;
            if (point == Vector2.one) return 2;
            if (point == Vector2.up) return 1;
            if (point == Vector2.right) return 3;
            if (point.x == 0) return point.y;
            if (point.x == 1) return 3f - point.y;
            if (point.y == 0) return 4f - point.x;
            if (point.y == 1) return 1f + point.x;
            return 0;
        }
        public Vector2 P(float f)
        {
            while (f > 4) f -= 4;
            while (f < 0) f += 4;
            if (f < 1) return new Vector2(0, f);
            else if (f < 2) return new Vector2(f - 1, 1);
            else if (f < 3) return new Vector2(1, 3f - f);
            else return new Vector2(4f - f, 0);
        }
        Vector3 vert(Vector2 point)
        {
            return new Vector3(min.x + (max - min).x * point.x, min.y + (max - min).y * point.y, 0f);
        }
        public void Creator(Vector2 one, Vector2 two, Vector2 center, GameObject go, GameObject sil)
        {
            Vector3[] verticles;
            Vector2[] uv;
            int[] triangles;
            float F1 = F(one), F2 = F(two);
            int n = (int)F2 - (int)F1;
            if (n == 0)
            {
                uv = new Vector2[] { center, one, two };
                verticles = new Vector3[] { vert(center), vert(one), vert(two) };
                if (ForPicca.Value == true)
                {
                    x = (vert(center).x + vert(one).x + vert(two).x) / 3;
                    y = (vert(center).y + vert(one).y + vert(two).y) / 3;
                }
                triangles = new int[] { 0, 1, 2 };
            }
            else if (n == 1 || n == -3)
            {
                float f = (int)F(two);
                Vector2 p = P(f);
                uv = new Vector2[] { center, one, p, two };
                verticles = new Vector3[] { vert(center), vert(one), vert(p), vert(two) };
                if (ForPicca.Value == true)
                {
                    x = (vert(center).x + vert(one).x + vert(two).x) / 3;
                    y = (vert(center).y + vert(one).y + vert(two).y) / 3;
                    if (Mathf.Approximately(x, 0))
                    {
                        x = (vert(center).x + vert(one).x + vert(two).x + vert(p).x) / 4;
                        y = (vert(center).y + vert(one).y + vert(two).y + vert(p).y) / 4;
                    }
                }
                triangles = new int[] { 0, 1, 2, 0, 2, 3 };
            }
            else if (n == 2 || n == -2)
            {
                float f1 = (int)F(one) + 1;
                Vector2 p1 = P(f1);
                float f2 = (int)F(two);
                Vector2 p2 = P(f2);
                uv = new Vector2[] { center, one, p1, p2, two };
                verticles = new Vector3[] { vert(center), vert(one), vert(p1), vert(p2), vert(two) };
                if (ForPicca.Value == true)
                {
                    x = (vert(center).x + vert(one).x + vert(two).x) / 3;
                    y = (vert(center).y + vert(one).y + vert(two).y) / 3;
                    if (Mathf.Approximately(x, 0))
                    {
                        x = (vert(center).x + vert(one).x + vert(two).x + vert(p1).x + vert(p2).x) / 5;
                        y = (vert(center).y + vert(one).y + vert(two).y + vert(p1).y + vert(p2).y) / 5;
                    }

                }
                triangles = new int[] { 0, 1, 2, 0, 2, 3, 0, 3, 4 };
            }
            else
            {
                float f1 = (int)F(one) + 1;
                Vector2 p1 = P(f1);
                float f2 = (int)F(one) + 2;
                Vector2 p2 = P(f2);
                float f3 = (int)F(two);
                Vector2 p3 = P(f3);
                uv = new Vector2[] { center, one, p1, p2, p3, two };
                verticles = new Vector3[] { vert(center), vert(one), vert(p1), vert(p2), vert(p3), vert(two) };
                if (ForPicca.Value == true)
                {
                    x = (vert(center).x + vert(one).x + vert(two).x) / 3;
                    y = (vert(center).y + vert(one).y + vert(two).y) / 3;

                }
                triangles = new int[] { 0, 1, 2, 0, 2, 3, 0, 3, 4, 0, 4, 5 };
            }
            float xmin = verticles[0].x, xmax = verticles[0].x, ymin = verticles[0].y, ymax = verticles[0].y;
            for (int i = 0; i < verticles.Length; i++)
            {
                if (xmin > verticles[i].x) xmin = verticles[i].x;
                if (xmax < verticles[i].x) xmax = verticles[i].x;
                if (ymin > verticles[i].y) ymin = verticles[i].y;
                if (ymax < verticles[i].y) ymax = verticles[i].y;
            }
            if (ForPicca.Value != true)
            {
                x = (xmax + xmin) / 2; 
                y = (ymax + ymin) / 2;
            }
            for (int i = 0; i < verticles.Length; i++)
            {
                verticles[i] -= new Vector3(x, y, 0);
            }

            Vector2 sred = new Vector2(((one.x + two.x) / 2) - 0.5f, ((one.y + two.y) / 2) - 0.5f);
            sred *= NewPosKoef.Value;
            go.transform.position += new Vector3(x, y, 0);
            go.AddComponent<RN_MoveToBySpeed>();
            go.GetComponent<RN_MoveToBySpeed>().destinationPosition = new Vector3(x + sred.x, y + sred.y, 0);
            if (Mathf.Approximately(sred.x, 0))
                go.GetComponent<RN_MoveToBySpeed>().destinationPosition = new Vector3(x * 1.1f, y * 1.1f, 0);
            go.GetComponent<RN_MoveToBySpeed>().speed = Speed.Value;
            if (OffSiluet.Value == false)
                sil.transform.position += new Vector3(x * (NewPosKoefSiluet.Value + 0.1f), y * (NewPosKoefSiluet.Value + 0.1f), 0);
            Mesh m = new Mesh();
            m.name = "" + go.name[go.name.Length - 1];
            m.vertices = verticles;
            m.uv = uv;
            m.triangles = triangles;
            MeshFilter mf1 = go.AddComponent<MeshFilter>();
            MeshFilter mf2 = new MeshFilter();
            if (OffSiluet.Value == false)
                mf2 = sil.AddComponent<MeshFilter>();
            mf1.mesh = m;
            if (OffSiluet.Value == false)
                mf2.mesh = m;
            PolygonCollider2D pc = go.AddComponent<PolygonCollider2D>();
            Vector2[] vert2 = new Vector2[verticles.Length];
            for (int i = 0; i < vert2.Length; i++)
            {
                vert2[i] = (Vector2)verticles[i];
            }
            pc.points = vert2;
        }
    }
}