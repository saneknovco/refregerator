﻿using UnityEngine;
using System.Collections;

namespace HutongGames.PlayMaker.Actions
{
	[ActionCategory("FF2_Ruben")]
	public class RZ_GetActiveAppParams : FsmStateAction 
	{
		[UIHint(UIHint.Variable)]
		public FsmString activeLang;
		[UIHint(UIHint.Variable)]
		public FsmBool activeAppType;

		public override void OnEnter ()
		{
			if(!activeLang.IsNone) 
			{
				if(AppState.Instance.CurrentLangStr == "ru")
					activeLang.Value = AppState.Instance.CurrentLangStr;
				else 
					activeLang.Value = "en";
			}
			if(!activeAppType.IsNone) activeAppType.Value = AppSettings.Instance.appInfo.ActiveAppType == MageAppType.Full;
			Finish();
		}

	}
}