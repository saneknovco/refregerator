﻿using UnityEngine;

namespace HutongGames.PlayMaker.Actions
{
    public class PiccaDistTuPunktir : FsmStateAction
    {
        public FsmFloat mouseX, mouseY, needDistance;
        public FsmVector3 point1, point2;
        public FsmEvent BadDistance, GoodDistance;
        public FsmBool Good;
        public override void OnUpdate()
        {
            float i1, i2, i3;
            i1 = point1.Value.x - point2.Value.x;
            i2 = -(point1.Value.y - point2.Value.y);
            i3 = -((-point2.Value.x * (point1.Value.y - point2.Value.y)) - (-point2.Value.y * (point1.Value.x - point2.Value.x)));
            float distance = (Mathf.Abs(i1 * mouseY.Value + i2 * mouseX.Value + i3))/ (Mathf.Sqrt(i1 * i1 + i2 * i2));
            if (distance < needDistance.Value)
                if (Good.Value == true)
                    Fsm.Event(GoodDistance);
            if (distance > needDistance.Value)
                if (Good.Value == false)
                    Fsm.Event(BadDistance);
        }
    }
}
