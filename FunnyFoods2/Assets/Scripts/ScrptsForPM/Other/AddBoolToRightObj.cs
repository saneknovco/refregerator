﻿using UnityEngine;
using System.Collections;
namespace HutongGames.PlayMaker.Actions
{
	[Tooltip("Script that assign right/unright bool to obj")]
	public class AddBoolToRightObj : FsmStateAction 
	{
		[RequiredField]
		[Tooltip("The GameObject Clicked")]
		public FsmOwnerDefault gameObject;
		[RequiredField]
		[Tooltip("The Correct Number")]
		public FsmInt CorrectNum;
		[RequiredField]
		[Tooltip("The Correct GObj Parent")]
		public FsmGameObject CorrectGObjParent;
		[RequiredField]
		[Tooltip("The InCorrect GObj Parent")]
		public FsmGameObject InCorrectGObjParent;
		public override void OnEnter() 
		{
			var go = Fsm.GetOwnerDefaultTarget(gameObject);
			int a = int.Parse(""+go.name[0]);
			if (go.name[1] == '0') a = 10;
			bool IsThitCorrectNum= a==CorrectNum.Value;
//			Debug.Log(go.name+"  "+a+"  "+CorrectNum.Value+"  "+IsThitCorrectNum);
			if(IsThitCorrectNum) 
			{
				go.name = "R"+go.name;
				go.transform.parent = CorrectGObjParent.Value.transform;
			}
			else 
			{
				go.name = "N"+go.name;
				go.transform.parent = InCorrectGObjParent.Value.transform;
			}
			Finish();
		}
	}
}