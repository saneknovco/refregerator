﻿using UnityEngine;
namespace HutongGames.PlayMaker.Actions
{
	[ActionCategory("Puzzle")]
	public class Puzzle_LOL_Quest : FsmStateAction 
	{
		public FsmOwnerDefault go;
		public FsmOwnerDefault positioner;
		public FsmBool useMousePosition;
		Vector2 texSize;
		Vector2 pos;
		Material mat;
		Transform t, posTransform;
		public override void OnEnter() 
		{
			t = Fsm.GetOwnerDefaultTarget(go).transform;
			posTransform = Fsm.GetOwnerDefaultTarget (positioner).transform;
			texSize = (Vector2)t.GetComponent<MeshFilter>().mesh.bounds.size;
			mat = t.GetComponent<MeshRenderer>().material;
		}
		public override void OnUpdate()
		{
			if (useMousePosition.Value) 
			{
				pos = (Vector2)Camera.main.ScreenPointToRay (InputMT.mousePosition).origin;
			} 
			else 
			{
				pos = (Vector2)posTransform.position;
			}

			pos.x += texSize.x/2;
			pos.y += texSize.y/2;
			pos.x/=texSize.x;
			pos.y/=texSize.y;
			Vector4 v = mat.GetVector("scale_Center");
			v.z = pos.x;
			v.w = pos.y;
			mat.SetVector("scale_Center", v);
		}
	}
}