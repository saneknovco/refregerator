﻿using UnityEngine;

namespace HutongGames.PlayMaker.Actions
{
	[Tooltip("Verification is anim plaing")]
	public class SetActiveGO : FsmStateAction 
	{
		[RequiredField]
		[Tooltip("The GameObject")]
		public FsmOwnerDefault gameObject;
		[RequiredField]
//		[Tooltip("bool for store result")]
		public FsmBool IsActive;
//		public FsmBool EveryFrame;
		GameObject go;
		
		public override void OnEnter() 
		{
			go = Fsm.GetOwnerDefaultTarget(gameObject);
			if(go)
			{
			go.SetActive(IsActive.Value);
			}
			Finish();
		}
	}
} 