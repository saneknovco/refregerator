﻿using UnityEngine;
using System.Collections;
using System;

namespace HutongGames.PlayMaker.Actions
{
    [ActionCategory("FF2_Ruslan")]
    public class Pizza_FollowMouse : FsmStateAction
    {
        [RequiredField]
        public FsmOwnerDefault movementGO;
        public FsmGameObject animation;
        public FsmInt Iteration, distancePoint;
        public FsmFloat angle, needDistance, speedAnim, scaleMultiplier, ScaleTime, divideKoef, z = new FsmFloat() { UseVariable = true };
        public FsmVector3 point1, point2, StartPos;
        public FsmBool TriggetEnter, resetZ, OnMouseDown;
        public FsmEvent WrongEnterPoint, Cut, onMouseUp;

        Transform _transfGO;
        Vector3 _colliderCenter, scale, _delta, _startPos, InPos, OutPos;
        float currentTime, storeResult, lerpTime, storeResultY, storeResultX, storeResultY1, storeResultX1, mouseX, mouseY,
            divideKoeftemp, distance, tempX, tempY, tempC, i1, i2, i3, ii1, ii2, ii3, radius;
        float[] formulaParams, tempForformula;
        int frame, deepEnterIndex;
        bool firstFrame;

        public override void OnEnter()
        {
            currentTime = storeResult = lerpTime = storeResultY = storeResultX = storeResultY1 = storeResultX1 = mouseX = mouseY = divideKoeftemp = distance = tempX = tempY = tempC = i1 = i2 = i3 = ii1 = ii2 = ii3 = radius = 0;
            
            firstFrame = false;
            _delta = Vector3.zero;
            if (Iteration.Value == 1 || Iteration.Value == 2)
                divideKoeftemp = -divideKoef.Value;
            else
                divideKoeftemp = divideKoef.Value;

            radius = Owner.GetComponent<MeshRenderer>().bounds.size.x / 2;
            float tempInPosY = radius * Mathf.Sin(angle.Value * (Mathf.PI / 180));
            float tempInPosX = radius * Mathf.Cos(angle.Value * (Mathf.PI / 180));
            float tempInPosY2 = radius * Mathf.Sin((angle.Value + 180) * (Mathf.PI / 180));
            float tempInPosX2 = radius * Mathf.Cos((angle.Value + 180) * (Mathf.PI / 180));
            mouseX = Camera.main.ScreenToWorldPoint(new Vector3(InputMT.mousePosition.x, 0, 0)).x;
            mouseY = Camera.main.ScreenToWorldPoint(new Vector3(0, InputMT.mousePosition.y, 0)).y;
            Vector3 mousePos = new Vector3(mouseX, mouseY, 0);
            InPos = new Vector3(tempInPosX, tempInPosY, 0);
            OutPos = new Vector3(tempInPosX2, tempInPosY2, 0);
            if (OnMouseDown.Value == false)
                if (Vector3.Distance(mousePos, InPos) > needDistance.Value && Vector3.Distance(mousePos, OutPos) > needDistance.Value)
                    Fsm.Event(WrongEnterPoint);
            OnMouseDown.Value = false;

            storeResult = 0;
            lerpTime = 0;
            currentTime = 0;
            ScaleTime.Value = ScaleTime.Value < 0 ? 0 : ScaleTime.Value;
            _transfGO = Fsm.GetOwnerDefaultTarget(movementGO).transform;
            scale = _transfGO.localScale;
            _startPos = _transfGO.position;

            if (scaleMultiplier.Value < 1)
                scaleMultiplier.Value = 1;

        }
        public override void OnUpdate()
        {
            
            if (InputMT.GetMouseButtonDown(0))
            {
                firstFrame = true;
                scale = _transfGO.localScale;
            }

            if (InputMT.GetMouseButton(0))
            {
                if (deepEnterIndex == 5)
                    if (_transfGO.position != new Vector3(-1000, -1000, 0))
                    {
                        if (firstFrame == true)
                        {
                            formulaParams = UrovnenieIndex();
                            _delta = new Vector3(formulaParams[3], formulaParams[4], _transfGO.position.z) - _transfGO.position;
                            firstFrame = false;
                        }
                    }
                    else
                        _delta = Vector3.zero;

                if (frame == 0)
                {
                    float xpos = InputMT.mousePosition.x;
                    float ypos = InputMT.mousePosition.y;
                    storeResultY = Camera.main.ScreenToWorldPoint(new Vector3(0, ypos, 0)).y;
                    storeResultX = Camera.main.ScreenToWorldPoint(new Vector3(xpos, 0, 0)).x;
                    float distance1 = Vector3.Distance(new Vector3(storeResultX, storeResultY, 0), point1.Value * 1.5f);
                    float distance2 = Vector3.Distance(new Vector3(storeResultX1, storeResultY1, 0), point1.Value * 1.5f);
                    distance = -distance1 + distance2;
                    speedAnim.Value = distance / divideKoeftemp;
                    mouseX = storeResultX;
                    mouseY = storeResultY;
                }
                else
                {
                    float xpos = InputMT.mousePosition.x;
                    float ypos = InputMT.mousePosition.y;
                    storeResultY1 = Camera.main.ScreenToWorldPoint(new Vector3(0, ypos, 0)).y;
                    storeResultX1 = Camera.main.ScreenToWorldPoint(new Vector3(xpos, 0, 0)).x;
                    float distance1 = Vector3.Distance(new Vector3(storeResultX, storeResultY, 0), point1.Value*1.5f);
                    float distance2 = Vector3.Distance(new Vector3(storeResultX1, storeResultY1, 0), point1.Value * 1.5f);
                    distance = distance1 - distance2;
                    speedAnim.Value = distance / divideKoeftemp;
                    mouseX = storeResultX1;
                    mouseY = storeResultY1;
                }

                if (TriggetEnter.Value == false)
                {
                    speedAnim.Value = 0;
                    firstFrame = true;
                    deepEnterIndex = 0;
                }

                currentTime += Time.deltaTime;
                lerpTime = currentTime / ScaleTime.Value;
                storeResult = Mathf.Lerp(scale.x, scale.x * scaleMultiplier.Value, lerpTime);
                _transfGO.localScale = new Vector3(storeResult, storeResult, scale.z);

                formulaParams = UrovnenieIndex();
                distance = (Mathf.Abs(formulaParams[0] * mouseX + formulaParams[1] * mouseY + formulaParams[2])) / (Mathf.Sqrt(Mathf.Pow(formulaParams[0], 2) + Mathf.Pow(formulaParams[1], 2)));

                if ((distance < needDistance.Value && TriggetEnter.Value == true) || TriggetEnter.Value == true)
                {
                    animation.Value.GetComponent<Animation>().enabled = true;

                    if (deepEnterIndex == 5)
                    {
                        if (Vector3.Distance(new Vector3(StartPos.Value.x, StartPos.Value.y, 0), OutPos) < 150 && distancePoint.Value == 0)
                            distancePoint.Value = 1;
                        if (Vector3.Distance(new Vector3(StartPos.Value.x, StartPos.Value.y, 0), InPos) < 150 && distancePoint.Value == 0)
                            distancePoint.Value = 2;

                        formulaParams = UrovnenieIndex();
                        if (Vector3.Distance((new Vector3(formulaParams[3], formulaParams[4], z.IsNone ? _startPos.z : z.Value) - _delta), new Vector3(Owner.transform.position.x, Owner.transform.position.y, z.IsNone ? _startPos.z : z.Value)) <= radius * 1.05f)
                            _transfGO.position = new Vector3(formulaParams[3], formulaParams[4], z.IsNone ? _startPos.z : z.Value) - _delta;
                        else
                            speedAnim.Value = 0;

                        switch (distancePoint.Value)
                        {
                            case 0: break;
                            case 1: if (Vector3.Distance(new Vector3(_transfGO.position.x, _transfGO.position.y, 0), InPos) < 50) { Fsm.Event(Cut); distancePoint.Value = 0; } break;
                            case 2: if (Vector3.Distance(new Vector3(_transfGO.position.x, _transfGO.position.y, 0), OutPos) < 50) { Fsm.Event(Cut); distancePoint.Value = 0; } break;
                        }
                    }
                    TriggetEnter.Value = true;
                }
                else
                    animation.Value.GetComponent<Animation>().enabled = false;

            }

            if (InputMT.GetMouseButtonUp(0))
            {
                if (resetZ.Value)
                    _transfGO.position = new Vector3(_transfGO.position.x, _transfGO.position.y, _startPos.z);

                if (onMouseUp != null)
                    Fsm.Event(onMouseUp);
                else
                    _transfGO.localScale = new Vector3(scale.x, scale.y, scale.z);
            }
            if (frame == 0)
                frame++;
            else
                frame = 0;
            if (deepEnterIndex < 5)
                deepEnterIndex++;
        }

        private float[] UrovnenieIndex()
        {
            i2 = point1.Value.x - point2.Value.x;
            i1 = -(point1.Value.y - point2.Value.y);
            i3 = -((-point2.Value.x * (point1.Value.y - point2.Value.y)) - (-point2.Value.y * (point1.Value.x - point2.Value.x)));
            ii1 = i2;
            ii2 = -i1;
            ii3 = -mouseX * i2 + i1 * mouseY;
            tempX = i1 * (-ii2 / ii1) + i2;
            tempC = -(i3 + i1 * (-ii3 / ii1));
            tempY = tempC / tempX;
            tempX = (-(ii3 + (ii2 * tempY))) / ii1;
            return new float[] { i1, i2, i3, tempX, tempY };
        }
        public override void OnExit()
        {
            _transfGO.localScale = new Vector3(scale.x, scale.y, scale.z);
        }
    }
}