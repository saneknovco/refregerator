﻿using UnityEngine;
using System.Collections;

namespace HutongGames.PlayMaker.Actions
{
    public class Tile : FsmStateAction
    {
        public FsmOwnerDefault gameObject;
        public FsmFloat speed;
        GameObject go;

        public override void OnEnter()
        {
            go = Fsm.GetOwnerDefaultTarget(gameObject);
            go.transform.GetComponent<MeshRenderer>().material.mainTextureOffset -= new Vector2(0, speed.Value);
            Finish();
        }
    }
}