﻿using UnityEngine;

namespace HutongGames.PlayMaker.Actions
{
	public class Bubbles_FlyingBubble : FsmStateAction
	{ 
		public FsmOwnerDefault owner;
		public FsmFloat xspd,yspd,xa,ya,xt,yt;
		GameObject go; 
		Vector3 pos,dPos,sinPos=Vector3.zero;
		float t=0;
		public override void OnEnter()
		{
			go = Fsm.GetOwnerDefaultTarget(owner);
			pos = go.transform.position;
		}
		public override void OnUpdate()
		{
			pos = go.transform.position;
			float dt = Time.deltaTime;
			t+= dt;
			pos -= sinPos;
			dPos = new Vector3(xspd.Value*dt,yspd.Value*dt,0);
			sinPos = new Vector3(Mathf.Sin(t/xt.Value)*xa.Value,Mathf.Cos (t/yt.Value)*ya.Value,0);
			pos+=dPos+sinPos;
			go.transform.position = pos;
		}
	}
} 