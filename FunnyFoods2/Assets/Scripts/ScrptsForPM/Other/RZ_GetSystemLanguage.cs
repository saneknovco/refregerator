﻿using UnityEngine;
using System.Collections;

namespace HutongGames.PlayMaker.Actions
{
    public class RZ_GetSystemLanguage : FsmStateAction
    {
        public FsmString[] GameLanguages;
        public FsmString Language;
        public override void OnEnter()
        {
            if (string.IsNullOrEmpty(Language.Value))
                switch (Application.systemLanguage)
                {
                    case SystemLanguage.English: { Language.Value = "en"; TestLang(); break; }
                    case SystemLanguage.Russian: { Language.Value = "ru"; TestLang(); break; }
                    case SystemLanguage.Japanese: { Language.Value = "jp"; TestLang(); break; }
                    case SystemLanguage.French: { Language.Value = "fr"; TestLang(); break; }
                    case SystemLanguage.Spanish: { Language.Value = "sp"; TestLang(); break; }
                    case SystemLanguage.Italian: { Language.Value = "it"; TestLang(); break; }
                    case SystemLanguage.Portuguese: { Language.Value = "pt"; TestLang(); break; }
                    case SystemLanguage.German: { Language.Value = "de"; TestLang(); break; }
                    case SystemLanguage.Chinese: { Language.Value = "cn"; TestLang(); break; }
                    case SystemLanguage.Korean: { Language.Value = "kr"; TestLang(); break; }
                    case SystemLanguage.Turkish: { Language.Value = "tr"; TestLang(); break; }
                    case SystemLanguage.Ukrainian: { Language.Value = "ru"; TestLang(); break; }
                    case SystemLanguage.Belarusian: { Language.Value = "ru"; TestLang(); break; }
                    default: { Language.Value = "en"; break; }
                }
            Finish();
        }
        void TestLang()
        {
            foreach (FsmString s in GameLanguages)
            {
                if (Language.Value == s.Value)
                    break;
                else
                    Language.Value = "en";
            }
        }
    }
}
