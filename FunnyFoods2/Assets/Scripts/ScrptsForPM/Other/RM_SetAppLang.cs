﻿using UnityEngine;
using System;
using System.Collections;

public enum ReadyLanguages
{
	en = 0,
	ru = 1,
	cn = 2,
	tr = 3,
    sp = 4,
	de = 5
}
namespace HutongGames.PlayMaker.Actions
{
	public class RM_SetAppLang : FsmStateAction 
	{
		[UIHint(UIHint.Variable)]
		public FsmString activeLang;
		public FsmString activeLangForSound;
		public FsmString needLang;

		public override void OnEnter ()
		{
			string[] names = Enum.GetNames (typeof(ReadyLanguages));
			string tempNeedLang = needLang.Value.ToLower ();
			activeLang.Value = names.Contains(tempNeedLang) ? tempNeedLang : "en";
//			bool langWasFound = false;
//			for (int i = 0; i < names.Length; i++) 
//			{
//				if (tempNeedLang == names [i]) 
//				{
//					activeLang.Value = tempNeedLang;
//					langWasFound = true;
//				}
//			}
//			if (!langWasFound)
//				activeLang.Value = "en";
			activeLangForSound.Value = needLang.Value.ToLower();
			AppState.Instance.CurrentLangStr = needLang.Value.ToLower();
			RZ_OrthoboxCountriesAnalyzer.CheckIsEnableOrthobox ();
			Finish();
		}
	}
}