﻿using UnityEngine;

namespace HutongGames.PlayMaker.Actions
{
	public class Andrew_SetAlphaVar : FsmStateAction
	{ 
		public FsmGameObject[] objs;
		public FsmFloat alpha;
		public FsmBool everyFrame;
		public override void OnEnter()
		{
			SetAlpha();
			if(!everyFrame.Value) Finish();
		}
		public override void OnUpdate()
		{
			SetAlpha ();
		}
		public void SetAlpha()
		{
			if (alpha.Value > 1) alpha.Value = 1f;
			if (alpha.Value < 0) alpha.Value = 0f;
			for(int i = 0; i < objs.Length; i++)
			{
				var go = objs[i].Value;
				Color color = go.GetComponent<Renderer>().material.color;
				color.a = alpha.Value;
				go.GetComponent<Renderer>().material.color = color;
			}
		}
	}
}