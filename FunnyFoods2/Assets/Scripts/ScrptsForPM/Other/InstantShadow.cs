﻿using UnityEngine;
namespace HutongGames.PlayMaker.Actions
{
	[Tooltip("Verification Correct Number Click")]
	public class InstantShadow : FsmStateAction {
		[RequiredField]
		[Tooltip("The GameObject With All Other GObjs")]
		public FsmOwnerDefault gameObject;
		public FsmGameObject GOForInstantiate;
		public FsmInt NeededNum;
		public FsmInt Counter;
		public FsmVector3 StartPosition;
		public FsmVector3 scale;
		public FsmFloat Distance;
		public FsmBool isSdvig = false;
//		public FsmEvent LastInstantiate;
		public override void OnEnter() 
		{
			var go = GOForInstantiate.Value;
			var gogo = Fsm.GetOwnerDefaultTarget(gameObject);
			var newObject = (GameObject)Object.Instantiate(go); // (GameObject)Object.Instantiate(ParentGObj.Value.FindChild(NeededNum.Value+"thGameObjShadow"));
			newObject.transform.position = StartPosition.Value;
			//newObject.transform.rotation = go.transform.rotation;
			newObject.name = ""+(NeededNum.Value-Counter.Value)+"thShadow";
			newObject.transform.parent = gogo.transform;
			newObject.transform.localScale = scale.Value;
			newObject.SetActive(true);
			StartPosition.Value += new Vector3(Distance.Value,0,0);
			if(NeededNum.Value>3 && Counter.Value<(NeededNum.Value/2+1)&&!isSdvig.Value)
			{
				StartPosition.Value -= new Vector3(Distance.Value*(((NeededNum.Value-1) / 2)+1),Distance.Value,0);
				isSdvig.Value = true;
			}
			Finish();
		}
	}	
}
