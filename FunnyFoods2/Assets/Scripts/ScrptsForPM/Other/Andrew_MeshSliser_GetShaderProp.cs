﻿using UnityEngine;

namespace HutongGames.PlayMaker.Actions
{
	[ActionCategory("Mesh_Sliser")]
	public class Andrew_MeshSliser_GetShaderProp : FsmStateAction
	{ 
		public FsmOwnerDefault owner;
        public FsmVector2 point1, point2, point3, point4, point5, point6;
		public FsmVector3 p1,p2;
		public FsmFloat alpha;
        public FsmInt Iteration;
        string Counter;
        Vector2 p111, p222;
        
		public override void OnEnter()
		{
            switch (Iteration.Value)
            {
                case 0: Counter = ""; break;
                case 1: { Counter = "2"; break; }
                case 2: { Counter = "3"; break; }
            }
			GameObject go = Fsm.GetOwnerDefaultTarget (owner);
			Material mat = go.GetComponent<Renderer>().material;
            var vec = mat.GetVector("_PosScale" + Counter);
            var angle = mat.GetFloat("_Angle" + Counter) % 180;
			alpha.Value = angle;
			Mesh mesh = go.GetComponent<MeshFilter>().mesh;
			Vector2 min = mesh.bounds.min,max = mesh.bounds.max;
			if (angle == 0) 
			{
                p111 = new Vector2(0, 0.5f + vec.y / (max.y - min.y));
                p222 = new Vector2(1, 0.5f + vec.y / (max.y - min.y));
				Finish();
			}
			if (angle == 90) 
			{
                p111 = new Vector2(0.5f + vec.x / (max.x - min.x), 1);
                p222 = new Vector2(0.5f + vec.x / (max.x - min.x), 0);
				Finish();
			}
			float k = Mathf.Tan (angle/180f*Mathf.PI);
			float x1 = 0, x2 = 0, y1 = 0, y2 = 0;
			float x = max.x - min.x, y = max.y - min.y;
			x1 = (vec.x+x/2f-(vec.y+y/2f)/k)/x;
			x2 = (vec.x+x/2f+(y/2f-vec.y)/k)/x;
			y1 = (vec.y+y/2f-(vec.x+x/2f)*k)/y;
			y2 = (vec.y+y/2f+(x/2f-vec.x)*k)/y;
			if(k>0)
			{
                if (x1 > 0 && x1 < 1) p111 = new Vector2(x1, 0);
                else p111 = new Vector2(0, y1);
                if (x2 > 0 && x2 < 1) p222 = new Vector2(x2, 1);
                else p222 = new Vector2(1, y2);
			}
			else
			{
                if (x1 > 0 && x1 < 1) p111 = new Vector2(x1, 1);
                else p111 = new Vector2(0, y1);
                if (x2 > 0 && x2 < 1) p222 = new Vector2(x2, 0);
                else p222 = new Vector2(1, y2);
			}

			if (angle > 90 && angle < 180)
			{
				p111.y = 1f - p111.y;
				p222.y = 1f - p222.y;
			}

			if(angle > 135f && angle < 180f)
			{
				p111.x = 1f - p111.x;
				p222.x = 1f - p222.x;
			}

			Vector2 P1 = min;
            P1.x += (max.x - min.x) * p111.x;
            P1.y += (max.y - min.y) * p111.y;
			P1.x*=go.transform.lossyScale.x;
			P1.y*=go.transform.lossyScale.y;

			Vector2 P2 = min;
            P2.x += (max.x - min.x) * p222.x;
            P2.y += (max.y - min.y) * p222.y;
			P2.x*=go.transform.lossyScale.x;
			P2.y*=go.transform.lossyScale.y;

			p1.Value = go.transform.position+(Vector3)P1;
			p2.Value = go.transform.position+(Vector3)P2;


            switch (Iteration.Value)
            {
                case 0: { point1.Value = p111; point2.Value = p222; break; }
                case 1: { point3.Value = p111; point4.Value = p222; break; }
                case 2: { point5.Value = p111; point6.Value = p222; break; }
            }
            //Debug.Log(vec + " "+ angle + " "+ k + " ");
            //Debug.Log(string.Format("angle {0} P1  {1}   P2  {2}", angle, P1, P2));	
//			GameObject g = new GameObject();
//			for (int i = 0; i < 360; i+=1)
//			{
//				math(go, 0.1f + i, g);
//			}
			Finish();
		}
		void math (GameObject go, float angle, GameObject g)
		{
			Material mat = go.GetComponent<Renderer>().material;
			var vec = mat.GetVector("_PosScale" + Counter);
//			var angle = mat.GetFloat("_Angle" + Counter) % 180;
			Mesh mesh = go.GetComponent<MeshFilter>().mesh;
			Vector2 min = mesh.bounds.min,max = mesh.bounds.max;
			if (angle == 0) 
			{
				p111 = new Vector2(0, 0.5f + vec.y / (max.y - min.y));
				p222 = new Vector2(1, 0.5f + vec.y / (max.y - min.y));
				Finish();
			}
			if (angle == 90) 
			{
				p111 = new Vector2(0.5f + vec.x / (max.x - min.x), 1);
				p222 = new Vector2(0.5f + vec.x / (max.x - min.x), 0);
				Finish();
			}
			float k = Mathf.Tan (angle / 180f * Mathf.PI);
			float x1 = 0, x2 = 0, y1 = 0, y2 = 0;
			float x = max.x - min.x, y = max.y - min.y;
			x1 = (x / 2f - (y / 2f) / k) / x;
			x2 = (x / 2f + (y / 2f) / k) / x;
			y1 = (y / 2f - (x / 2f) * k) / y;
			y2 = (y / 2f + (x / 2f) * k) / y;
			if(k>0)
			{
				if (x1 > 0 && x1 < 1) p111 = new Vector2(x1, 0);
				else p111 = new Vector2(0, y1);
				if (x2 > 0 && x2 < 1) p222 = new Vector2(x2, 1);
				else p222 = new Vector2(1, y2);
			}
			else
			{
				if (x1 > 0 && x1 < 1) p111 = new Vector2(x1, 1);
				else p111 = new Vector2(0, y1);
				if (x2 > 0 && x2 < 1) p222 = new Vector2(x2, 0);
				else p222 = new Vector2(1, y2);
			}
			if (angle > 90 && angle < 180)
			{
				p111.y = 1f - p111.y;
				p222.y = 1f - p222.y;
			}
			if(angle > 135f && angle < 180f)
			{
				p111.x = 1f - p111.x;
				p222.x = 1f - p222.x;
			}
			Vector2 P1 = min;
			P1.x += (max.x - min.x) * p111.x;
			P1.y += (max.y - min.y) * p111.y;
			P1.x*=go.transform.lossyScale.x;
			P1.y*=go.transform.lossyScale.y;

			Vector2 P2 = min;
			P2.x += (max.x - min.x) * p222.x;
			P2.y += (max.y - min.y) * p222.y;
			P2.x*=go.transform.lossyScale.x;
			P2.y*=go.transform.lossyScale.y;

			P1 = go.transform.position+(Vector3)P1;
			P2 = go.transform.position+(Vector3)P2;
			GameObject gg = new GameObject();
			gg.transform.position = (Vector3)P1;
			gg.transform.parent = g.transform;
			gg.name = "new" + angle;
			//Debug.Log(string.Format("angle {0} P1  {1}   P2  {2}  p111 {3}  p222 {4}", angle, P1, P2, p111, p222));				
		}
	}
} 