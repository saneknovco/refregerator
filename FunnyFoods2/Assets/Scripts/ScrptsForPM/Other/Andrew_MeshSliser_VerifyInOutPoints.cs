﻿using UnityEngine;

namespace HutongGames.PlayMaker.Actions
{
	[ActionCategory("Mesh_Sliser")]
	public class Andrew_MeshSliser_VerifyInOutPoints : FsmStateAction
	{ 
		public FsmOwnerDefault owner;
        public FsmInt Iteration;
		public FsmVector2 point1,point2,point3,point4,point5,point6;
		public FsmFloat inX,inY,outX,outY,maxNormalizzedDistance, mouseX,mouseY;
		public FsmVector2 storeIn,storeOut,storeCenter;
        public FsmBool MouseEnter;
		public FsmEvent goodResult,badResult,noResult;
		Vector2 center,pIn,pOut,p1,p2;


		public override void OnEnter()
		{
            switch(Iteration.Value)
            {
                case 0: { p1 = point1.Value; p2 = point2.Value; break; }
                case 1: { p1 = point3.Value; p2 = point4.Value; break; }
                case 2: { p1 = point5.Value; p2 = point6.Value; break; }
            }
			GameObject go = Fsm.GetOwnerDefaultTarget (owner);
            center = (p1 + p2) / 2;
			Bounds b = go.GetComponent<MeshRenderer>().bounds;
			Vector3 min = b.min,max = b.max;
			float y = Camera.main.orthographicSize;
			float x = y*Camera.main.aspect;
			float xx = inX.Value*x*2-x;
			xx = (xx-min.x)/(max.x-min.x);
			float yy = inY.Value*y*2-y;
			yy = (yy-min.y)/(max.y-min.y);
			pIn = new Vector2(xx,yy);
			xx = outX.Value*x*2-x;
			xx = (xx-min.x)/(max.x-min.x);
			yy = outY.Value*y*2-y;
			yy = (yy-min.y)/(max.y-min.y);
			pOut = new Vector2(xx,yy);
			x = pOut.x-pIn.x;
			y = pOut.y-pIn.y;
			if (x == 0) 
			{
				pIn = new Vector2(pIn.x,0);
				pOut = new Vector2(pOut.x,1);
				Finish();
			}
			else
			if(y==0)
			{
				pIn = new Vector2(0,pIn.y);
				pOut = new Vector2(1,pOut.y);
			}
			else
			{
				float k = y/x;
				float x1 = 0, x2 = 0, y1 = 0, y2 = 0;
				x = 1f;
				y = 1f;
				Vector2 vec = ((pIn+pOut)-Vector2.one)/2;
				x1 = (vec.x+x/2f-(vec.y+y/2f)/k)/x;
				x2 = (vec.x+x/2f+(y/2f-vec.y)/k)/x;
				y1 = (vec.y+y/2f-(vec.x+x/2f)*k)/y;
				y2 = (vec.y+y/2f+(x/2f-vec.x)*k)/y;
				//Debug.Log(vec+" "+ x1+" "+y1+" "+x2+" "+y2);
				if(k>0)
				{
					if(x1 > 0 && x1 < 1) pIn = new Vector2(x1,0);
					else pIn = new Vector2(0,y1);
					if(x2 > 0 && x2 < 1) pOut = new Vector2(x2,1);
					else pOut = new Vector2(1,y2);
				}
				else
				{
					if(x1 > 0 && x1 < 1) pIn = new Vector2(x1,1);
					else pIn = new Vector2(0,y1);
					if(x2 > 0 && x2 < 1) pOut = new Vector2(x2,0);
					else pOut = new Vector2(1,y2);
				}
			}
			storeIn.Value = pIn;
			storeOut.Value = pOut;
			storeCenter.Value = (pIn+pOut)/2;
			float d = maxNormalizzedDistance.Value;

            if (((pIn - p1).magnitude < d && (pOut - p2).magnitude < d) || ((pIn - p2).magnitude < d && (pOut - p1).magnitude < d)) 
                if(MouseEnter.Value == true)
                    Fsm.Event(goodResult);
                else
                    Fsm.Event(noResult);
            else
                if (MouseEnter.Value == true)
                    Fsm.Event(badResult);
                else
                    Fsm.Event(noResult);
            Finish();
		}
	}
} 
