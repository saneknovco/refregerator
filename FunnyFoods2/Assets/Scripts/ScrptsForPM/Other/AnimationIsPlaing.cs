using UnityEngine;

namespace HutongGames.PlayMaker.Actions
{
	[Tooltip("Verification is anim plaing")]
	public class AnimationIsPlaing : FsmStateAction 
	{
		[RequiredField]
		[Tooltip("The GameObject Clicked")]
		public FsmOwnerDefault gameObject;
		[RequiredField]
		[Tooltip("bool for store result")]
		public FsmBool isPlaying;
		public FsmBool EveryFrame;
		GameObject go;
		
		public override void OnEnter() 
		{
			go = Fsm.GetOwnerDefaultTarget(gameObject);
			isPlaying.Value = go.GetComponent<Animation>().isPlaying;
			if(!EveryFrame.Value)
			{Finish();}
		}
		public override void OnUpdate() 
		{
			if(EveryFrame.Value)
				isPlaying.Value = go.GetComponent<Animation>().isPlaying;
		}
	}
}