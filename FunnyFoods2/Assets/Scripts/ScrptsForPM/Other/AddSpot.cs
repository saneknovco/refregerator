﻿using UnityEngine;

namespace HutongGames.PlayMaker.Actions
{
	[Tooltip("Verification Correct Number Click")]
	public class AddSpot : FsmStateAction 
	{
		[RequiredField]
		[Tooltip("The GameObject Clicked")]
		public FsmOwnerDefault gameObject;

		public override void OnEnter() 
		{
			var go = Fsm.GetOwnerDefaultTarget(gameObject);
			//neew to start animations/sounds/etc
			Finish();
		}
	}
}