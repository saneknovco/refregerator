﻿using UnityEngine;

namespace HutongGames.PlayMaker.Actions
{
	[Tooltip("Gets the Child of a GameObject by Index.\nE.g., O to get the first child. HINT: Use this with an integer variable to iterate through children.")]
	public class GetChildNumNum : FsmStateAction
	{
		[RequiredField]
		[Tooltip("The GameObject to search.")]
		public FsmOwnerDefault gameObject;
		
		[RequiredField]
		[Tooltip("The index of the child to find.")]
		public FsmInt[] childIndexes;
		
		[RequiredField]
		[UIHint(UIHint.Variable)]
		[Tooltip("Store the child in a GameObject variable.")]
		public FsmGameObject store;
		
		/*public override void Reset()
		{
			gameObject = null;
			childIndex = 0;
			store = null;
		}*/
		
		public override void OnEnter()
		{
			store.Value = DoGetChildNum(Fsm.GetOwnerDefaultTarget(gameObject));
			
			Finish();
		}
		
		GameObject DoGetChildNum(GameObject go)
		{
			foreach(var childNum in childIndexes)
			{
				go = go.transform.GetChild(childNum.Value % go.transform.childCount).gameObject;
//				Debug.Log(go.name);
			}
			return go;
		}
	}
}
