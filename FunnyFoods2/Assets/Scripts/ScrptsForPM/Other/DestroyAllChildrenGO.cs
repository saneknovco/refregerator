﻿using UnityEngine;

namespace HutongGames.PlayMaker.Actions
{
	[Tooltip("Verification is anim plaing")]
	public class DestroyAllChildrenGO : FsmStateAction 
	{
		public FsmOwnerDefault gameObject; 

		public override void OnEnter() 
		{
			var go = Fsm.GetOwnerDefaultTarget(gameObject);
			int n = go.transform.childCount;
			if(n>0)
				for(int i = n-1; i>=0;i--)
			{
				var go1 = go.transform.GetChild(i).gameObject;
				GameObject.Destroy(go1);
			}
			Finish();
		}
		
	}
} 