﻿using UnityEngine;
using System;
using System.Collections;

public enum OrthoboxCountriesNames
{
	Bulgarian,
	Belarusian,
	Greek,
	SerboCroatian,
	Russian,
	Ukrainian
}
public static class RZ_OrthoboxCountriesAnalyzer 
{
	public static void CheckIsEnableOrthobox()
	{
		string[] orthoboxNames = Enum.GetNames (typeof(OrthoboxCountriesNames));

		if (orthoboxNames.Contains (Enum.GetName (typeof(SystemLanguage), Application.systemLanguage)) || orthoboxNames.Contains (Enum.GetName (typeof(SystemLanguage), MageAppToSystemLanguage (AppState.Instance.CurrentLang))))
		{
			PlayerPrefs.SetInt ("EasterEnabled", 1);
		} 
		else 
		{
			PlayerPrefs.SetInt ("EasterEnabled", 0);
		}
		Debug.Log("Easter " + (PlayerPrefs.GetInt("EasterEnabled") > 0 ? "enabled" : "disabled"));
	}
	static SystemLanguage MageAppToSystemLanguage(MageAppUILang lang)
	{
		switch (lang)
		{
			case MageAppUILang.en: return SystemLanguage.English; break;
			case MageAppUILang.ru: return SystemLanguage.Russian; break;
			case MageAppUILang.jp: return SystemLanguage.Japanese; break;
			case MageAppUILang.fr: return SystemLanguage.French; break;
			case MageAppUILang.sp: return SystemLanguage.Spanish; break;
			case MageAppUILang.it: return SystemLanguage.Italian; break;
			case MageAppUILang.pt: return SystemLanguage.Portuguese; break;
			case MageAppUILang.de: return SystemLanguage.German; break;
			case MageAppUILang.cn: return SystemLanguage.Chinese; break;
			case MageAppUILang.kr: return SystemLanguage.Korean; break;
			case MageAppUILang.tr: return SystemLanguage.Turkish; break;
			default: return SystemLanguage.English; break;
		}
	}
}
