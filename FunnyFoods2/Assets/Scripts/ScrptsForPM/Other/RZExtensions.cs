﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public static class RZExtensions
{
    public static bool Contains<T>(this T[] array, T element)
    {
        for (int i = 0; i < array.Length; i++)
        {
            if (array[i].Equals(element))
                return true;
        }
        return false;
    }

    public static void SetGlobalScale(this GameObject go, float x, float y, float z)
    {
        if (go != null)
        {
            if (go.transform.parent != null)
            {
                Vector3 lossy = go.transform.parent.lossyScale;
                go.transform.localScale = new Vector3(x / lossy.x, y / lossy.y, z / lossy.z);
            }
            else
                go.transform.localScale = new Vector3(x, y, z);
        }

    }

    public static void SetGlobalScale(this GameObject go, Vector3 scale)
    {
        if (go.transform.parent != null)
        {
            Vector3 lossy = go.transform.parent.lossyScale;
            go.transform.localScale = new Vector3(scale.x / lossy.x, scale.y / lossy.y, scale.z / lossy.z);
        }
        else
            go.transform.localScale = new Vector3(scale.x, scale.y, scale.z);

    }

    public static bool IsAnimationPlaying(this GameObject go, string name = "", bool isRecursive = false)
    {
        bool isPlay = false;
        isPlay = IsAnimationPlay(go, name);
        if (isPlay)
            return isPlay;
        if (isRecursive)
        {
            for (int i = 0; i < go.transform.childCount; i++)
            {
                isPlay = IsAnimationPlaying(go.transform.GetChild(i).gameObject, name, true);
                if (isPlay)
                    return isPlay;
            }
        }
        return false;
    }

    private static bool IsAnimationPlay(GameObject go, string name = "")
    {
        Animation _animation;
        if ((_animation = go.GetComponent<Animation>()) != null)
        {
            if (!string.IsNullOrEmpty(name))
                return _animation.IsPlaying(name);
            return _animation.isPlaying;
        }
        return false;
    }

    public static void SetAlpha(this GameObject go, float alpha)
    {
        if (go)
        {
            MeshRenderer rend = go.GetComponent<MeshRenderer>();
            if (rend != null)
            {
                if (!rend.material.HasProperty("_Color"))
                    rend.material.shader = Shader.Find("Unlit/AlphaSelfIllumWithFade");

                Color color = rend.material.color;
                color.a = alpha;
                rend.material.color = color;
            }
        }
    }

    public static void RecursiveSetAlpha(this GameObject go, float alpha)
    {
        if (go)
        {
            go.SetAlpha(alpha);
            for (int i = 0; i < go.transform.childCount; i++)
            {
                SetAlpha(go.transform.GetChild(i).gameObject, alpha);
                RecursiveSetAlpha(go.transform.GetChild(i).gameObject, alpha);
            }
        }
    }

    public static void ResetAnimation(this GameObject go)
    {
        Animation animation;
        if (go != null && (animation = go.GetComponent<Animation>()) != null)
            foreach (AnimationState anim in animation)
            {
                string name = anim.name;
                if (animation.IsPlaying(name))
                {
                    animation[name].time = 0;
                    animation[name].enabled = true;
                    animation.Sample();
                    animation[name].enabled = false;
                }
            }
    }

    public static void RecursiveResetAnimation(this GameObject go)
    {
        if (go)
        {
            go.ResetAnimation();
            for (int i = 0; i < go.transform.childCount; i++)
                go.transform.GetChild(i).gameObject.ResetAnimation();
        }
    }

    public static void SendEventToGameObject(this GameObject go, string eventName)
    {
        PlayMakerFSM pm;
        if (go && !string.IsNullOrEmpty(eventName))
            if (pm = go.GetComponent<PlayMakerFSM>())
                pm.SendEvent(eventName);
    }

    public static void RecursiveSendEventToGameObject(this GameObject go, string eventName)
    {
        if (go && !string.IsNullOrEmpty(eventName))
        {
            go.SendEventToGameObject(eventName);
            for (int i = 0; i < go.transform.childCount; i++)
                SendEventToGameObject(go.transform.GetChild(i).gameObject, eventName);
        }
    }

    public static GameObject GetChildByName(this GameObject go, string name)
    {
        GameObject returnObject;
        if (go == null)
            return null;

        foreach (Transform child in go.transform)
        {
            if (!string.IsNullOrEmpty(name))
                if (child.name == name)
                        return child.gameObject;

            returnObject = GetChildByName(child.gameObject, name);
            if (returnObject != null)
                return returnObject;
        }
        return null;
    }

}
