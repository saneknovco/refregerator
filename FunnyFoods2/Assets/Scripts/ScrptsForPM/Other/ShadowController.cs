﻿using UnityEngine;
namespace HutongGames.PlayMaker.Actions
{
	[Tooltip("Verification Correct Number Click")]
	public class ShadowController : FsmStateAction {
		[RequiredField]
		[Tooltip("The GameObject With All Other GObjs")]
		public FsmOwnerDefault ParentShadowGObj;
		public FsmInt NeededNum;
		public FsmInt Counter;
//		public FsmVector3 StartPosition;
		GameObject go;
		public FsmGameObject TargetShadow;
		public FsmGameObject StolShadow;
		//		public FsmEvent LastInstantiate;
		public override void OnEnter() 
		{
			go = Fsm.GetOwnerDefaultTarget(ParentShadowGObj);;
			Counter.Value++;
			TargetShadow.Value = go.transform.FindChild(Counter.Value+"thShadow").gameObject;
			StolShadow.Value.transform.position = TargetShadow.Value.transform.position;
			Finish();
		}
	}	
}
