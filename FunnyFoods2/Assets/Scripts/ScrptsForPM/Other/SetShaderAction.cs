﻿using UnityEngine;

namespace HutongGames.PlayMaker.Actions
{
	public class SetShaderAction : FsmStateAction 
	{
		public FsmOwnerDefault gameObject;
		public FsmString shaderName;

		public override void OnEnter() 
		{
			GameObject go = Fsm.GetOwnerDefaultTarget(gameObject);
			go.GetComponent<Renderer>().material.shader = Shader.Find(shaderName.Value);
			Finish ();
		}

	}
}