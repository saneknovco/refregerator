﻿using UnityEngine;
using System.Collections;

namespace HutongGames.PlayMaker.Actions
{
    public class Picca_accelerometer : FsmStateAction
    {
        public FsmFloat Y;
        public FsmBool Stop;
        public override void OnUpdate()
        {
            Y.Value = Input.acceleration.y;
            if (Stop.Value == false)
                Owner.transform.Translate(new Vector3(0, Input.acceleration.y * 30, 0));
        }
    }
}
