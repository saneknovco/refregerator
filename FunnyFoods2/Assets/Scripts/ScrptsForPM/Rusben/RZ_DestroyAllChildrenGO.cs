﻿using UnityEngine;

namespace HutongGames.PlayMaker.Actions
{
    [ActionCategory("FF2_Rusben")]
    public class RZ_DestroyAllChildrenGO : FsmStateAction
    {
        public FsmOwnerDefault gameObject;

        public override void OnEnter()
        {
            GameObject go = Fsm.GetOwnerDefaultTarget(gameObject);
            int n = go.transform.childCount;
            if (n > 0)
                for (int i = n - 1; i >= 0; i--)
                {
                    var child = go.transform.GetChild(i).gameObject;
                    GameObject.Destroy(child);
                }
            Finish();
        }

    }
}