﻿// (c) Copyright HutongGames, LLC 2010-2013. All rights reserved.

using UnityEngine;

namespace HutongGames.PlayMaker.Actions
{
    [ActionCategory("FF2_Rusben")]
    [Tooltip("Destroys a Game Object.")]
    public class RZ_DestroyObject : FsmStateAction
    {
        [RequiredField]
        [Tooltip("The GameObject to destroy.")]
        public FsmGameObject[] gameObject;

        [HasFloatSlider(0, 5)]
        [Tooltip("Optional delay before destroying the Game Object.")]
        public FsmFloat delay;

        [Tooltip("Detach children before destroying the Game Object.")]
        public FsmBool detachChildren;

        public FsmBool DestroySelf;

        public override void Reset()
        {
            gameObject = null;
            delay = 0;
        }

        public override void OnEnter()
        {
            foreach (var go in gameObject)
            {
                if (go.Value != null)
                {
                    if (delay.Value <= 0)
                    {
                        Object.Destroy(go.Value);
                    }
                    else
                    {
                        Object.Destroy(go.Value, delay.Value);
                    }

                    if (detachChildren.Value)
                        go.Value.transform.DetachChildren();
                }
            }
            if (DestroySelf.Value == true)
            {
                Object.Destroy(Owner);
            }
            Finish();
        }
    }
}
