﻿using UnityEngine;
using System.Collections;

namespace HutongGames.PlayMaker.Actions
{
    public class RZ_FoodsLoad : FsmStateAction
    {
        public FsmOwnerDefault NamesParent;
        public FsmInt NamesCount;
        public FsmOwnerDefault GameObjectsParent;

        GameObject newGO, _namesParent, _gameObjectsParent;
        

        public override void OnEnter()
        {
            if (NamesCount.Value <= 0)
            {
                Finish();
                return;
            }
            
            _namesParent = Fsm.GetOwnerDefaultTarget(NamesParent);
            _gameObjectsParent = Fsm.GetOwnerDefaultTarget(GameObjectsParent);

            if (_namesParent == null || _gameObjectsParent == null)
            {
                Finish();
                return;
            }

            if (_namesParent.transform.childCount < NamesCount.Value)
            {
                Finish();
                return;
            }

            DoGetRandomChildren();
            Finish();
            return;
        }

        void DoGetRandomChildren()
        {
            for (int i = 0; i < NamesCount.Value; i++)
                DoGetRandomChild();
        }

        void DoGetRandomChild()
        {
            int childCount = _namesParent.transform.childCount;
            Transform ContainerFood = _namesParent.transform.GetChild(Random.Range(0, childCount));
            Vector3 scale = ContainerFood.localScale;
            ContainerFood.localScale = Vector3.one;
            GameObject obj = Resources.Load(string.Format("Prefabs/{0}", ContainerFood.name)) as GameObject;
            Transform ContainerFoodPers = ContainerFood.GetChild(0);
            if (obj == null) return;
            obj = obj.transform.GetChild(0).gameObject;
            //mesh and material to golova
            Material mat = obj.transform.GetChild(0).GetComponent<MeshRenderer>().sharedMaterial;
            Mesh mesh = obj.transform.GetChild(0).GetComponent<MeshFilter>().sharedMesh;
            MeshRenderer mr = ContainerFoodPers.GetChild(0).gameObject.AddComponent<MeshRenderer>();
            mr.material = mat;
            MeshFilter mf = ContainerFoodPers.GetChild(0).gameObject.AddComponent<MeshFilter>();
            mf.mesh = mesh;
            //reparent food parts
            for (int i = 1; i < obj.transform.childCount; i++)
            {
                string name = obj.transform.GetChild(i).name;
                newGO = GameObject.Instantiate(obj.transform.GetChild(i).gameObject);
                newGO.transform.parent = ContainerFoodPers;
                newGO.name = name;
            }
            ContainerFood.localScale = scale;
            ContainerFood.parent = _gameObjectsParent.transform;

        }
    }
}
