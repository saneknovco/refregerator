﻿using UnityEngine;
using System.Collections;

namespace HutongGames.PlayMaker.Actions
{
    public class RZ_PersLoad : FsmStateAction
    {
        public FsmOwnerDefault NamesParent;
        public FsmInt NamesCount;
        public FsmOwnerDefault GameObjectsParent;

        public FsmBool isHeadOnly;
        public FsmBool Helicopter;

        GameObject newGO, _namesParent, _gameObjectsParent;
        

        public override void OnEnter()
        {
            if (NamesCount.Value <= 0)
            {
                Finish();
                return;
            }

            _namesParent = Fsm.GetOwnerDefaultTarget(NamesParent);
            _gameObjectsParent = Fsm.GetOwnerDefaultTarget(GameObjectsParent);

            if (_namesParent == null || _gameObjectsParent == null)
            {
                Finish();
                return;
            }

            if (_namesParent.transform.childCount < NamesCount.Value)
            {
                Finish();
                return;
            }

            DoGetRandomChildren();
            Finish();
            return;
        }

        void DoGetRandomChildren()
        {
            for (int i = 0; i < NamesCount.Value; i++)
                DoGetRandomChild();
        }

        void recursiveCopyComp(GameObject from, GameObject to)
        {
            MeshRenderer fromMeshRenderer;
            MeshFilter fromMeshFilter;
            if ((fromMeshRenderer = from.transform.GetComponent<MeshRenderer>()) != null && (fromMeshFilter = from.transform.GetComponent<MeshFilter>()) != null)
            {
				MeshRenderer toMeshRenderer;
				MeshFilter toMeshFilter;
				if((toMeshRenderer = to.AddComponent<MeshRenderer>())!=null && (toMeshFilter = to.AddComponent<MeshFilter>())!= null)
				{
	                toMeshRenderer.material = fromMeshRenderer.sharedMaterial;
	                toMeshFilter.mesh = fromMeshFilter.sharedMesh;
				}
            }
			if(from.transform.childCount == to.transform.childCount)
            	for (int i = 0; i < from.transform.childCount; i++)
                	recursiveCopyComp(from.transform.GetChild(i).gameObject, to.transform.GetChild(i).gameObject);
        }

        void DoGetRandomChild()
        {
            int childCount = _namesParent.transform.childCount;
            GameObject containerObj = _namesParent.transform.GetChild(Random.Range(0, childCount)).gameObject;
            Vector3 scale = containerObj.transform.localScale;
            containerObj.transform.localScale = Vector3.one;
            GameObject loadedObj = Resources.Load(string.Format("Prefabs/{0}", containerObj.name)) as GameObject;
            GameObject containerPers = containerObj.transform.GetChild(0).gameObject;
            if (loadedObj == null) return;
            GameObject loadedPers = loadedObj.transform.GetChild(0).gameObject;

            if (isHeadOnly.Value)
            {
                recursiveCopyComp(loadedPers.transform.GetChild(0).GetChild(0).gameObject, containerPers);
                containerObj.transform.parent = _gameObjectsParent.transform;
            }
            else
            {
                recursiveCopyComp(loadedObj, containerObj);
                containerObj.transform.parent = _gameObjectsParent.transform;
            }
        }
    }
}
