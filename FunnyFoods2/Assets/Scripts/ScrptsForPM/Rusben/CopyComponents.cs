﻿using UnityEngine;
using System.Collections;
using System.Reflection;

namespace HutongGames.PlayMaker.Actions
{
    public class CopyComponents : FsmStateAction
    {
        public FsmOwnerDefault From;
        public FsmOwnerDefault To;
        GameObject _from;
        GameObject _to;
        Component tempComp;
        
        public override void OnEnter()
        {
            _from = Fsm.GetOwnerDefaultTarget(From);
            _to = Fsm.GetOwnerDefaultTarget(To);
            
            //Copy<Collider>();
            //Copy<Collider2D>();
            //Copy<Animation>();
            //Copy<Animator>();
            //Copy<Rigidbody>();
            //Copy<ConstantForce>();
            //Copy<Rigidbody2D>();
            //Copy<ConstantForce2D>();
            Copy<MonoBehaviour>();
            //Copy<GetMouseDown>();
            
            Finish();
        }
        void Copy<T>() where T : Component
        {
            T[] components = _from.GetComponents<T>();
            Debug.Log(components.Length);
            
            if (components.Length > 0)
            for (int i = 0; i < components.Length; i++)
            {
                if (components[i] != null)
                    if (!(components[i] is PlayMakerFSM))
                        CopyComponent<T>(components[i] as T, _to);
                Debug.Log(components[i]);
            }
        }

        T CopyComponent<T>(T original, GameObject destination) where T : Component
        {
            System.Type type = original.GetType();
            T copy = destination.AddComponent(type) as T;
            BindingFlags flags = BindingFlags.Public | BindingFlags.NonPublic | BindingFlags.Instance | BindingFlags.Default | BindingFlags.DeclaredOnly;
            System.Reflection.PropertyInfo[] fields = type.GetProperties(flags);
            foreach (System.Reflection.PropertyInfo field in fields)
            {
                field.SetValue(copy, field.GetValue(original, null), null);
            }
            return copy;
        }
    }
}