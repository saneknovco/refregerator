﻿using UnityEngine;
namespace HutongGames.PlayMaker.Actions
{
    [ActionCategory("FF2_Rusben")]
    public class RZ_GetMainCameraParam : FsmStateAction
    {
        public FsmFloat XSize, YAndOrtSize, Aspect, CamPosX, CamPosY, CamPosZ;
        public FsmVector3 CamPosition;
        public FsmBool SetParams;

        public override void OnEnter()
        {
            Camera cam = Camera.main;
            if (SetParams.Value)
            {
                cam.orthographicSize = YAndOrtSize.Value;
                cam.aspect = Aspect.Value;
                cam.gameObject.transform.position = CamPosition.Value;
                cam.gameObject.transform.position = new Vector3(CamPosX.Value, CamPosY.Value, CamPosZ.Value);
            }
            else
            {
                XSize.Value = cam.orthographicSize * cam.aspect;
                YAndOrtSize.Value = cam.orthographicSize;
                Aspect.Value = cam.aspect;
                CamPosition.Value = cam.gameObject.transform.position;
                CamPosX.Value = cam.gameObject.transform.position.x;
                CamPosY.Value = cam.gameObject.transform.position.y;
                CamPosZ.Value = cam.gameObject.transform.position.z;
            }
            Finish();
        }
    }
}
