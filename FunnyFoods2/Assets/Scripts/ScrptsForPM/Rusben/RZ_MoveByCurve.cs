﻿using UnityEngine;

namespace HutongGames.PlayMaker.Actions
{
    [ActionCategory("FF2_Rusben")]
    public class RZ_MoveByCurve : FsmStateAction
    {
        public FsmOwnerDefault gameObject;
        public FsmVector3 TargetPosition;
        public FsmFloat Gravity, storeTime;
        public FsmEvent FinishEvent;
        public FsmFloat Height;

        GameObject go;
        float x1, x2, xm, y1, y2, ym, a, b, c, h, dh, vx, vy1, vy2, t, t1, t2, g, z, time = 0f;
        public override void OnEnter()
        {
            time = 0f;
            go = Fsm.GetOwnerDefaultTarget(gameObject);
            Vector3 p1 = go.transform.position;
            Vector3 p2 = TargetPosition.Value;
            x1 = p1.x;
            x2 = p2.x;
            y1 = p1.y;
            y2 = p2.y;
            z = p1.z;
            float camY = Camera.main.orthographicSize + Camera.main.transform.position.y;
            //ym = (y2 + camY) / 2.4f;
            ym = Mathf.Max(y1, y2) + Height.Value; //(y2 + 100);
            h = Mathf.Abs(ym - y1);
            dh = Mathf.Abs(ym - y2);
            g = Gravity.Value;
            vy1 = Mathf.Sqrt(h * (2f * g));
            vy2 = Mathf.Sqrt(dh * (2f * g));
            t1 = vy1 / g;
            t2 = vy2 / g;
            t = t1 + t2;
            vx = (x2 - x1) / t;
            xm = x1 + vx * t1;
            storeTime.Value = t;
        }
        bool once = true;
        public override void OnUpdate()
        {
            time += Time.deltaTime;
            if (time > t)
            {
                go.transform.position = TargetPosition.Value;
                if (FinishEvent != null) Fsm.Event(FinishEvent);
                Finish();
            }
            else
            {
                if (time > t / 2 && once)
                {
                    z = TargetPosition.Value.z;
                    once = false;
                }
                float x = x1 + vx * time;
                float y = y1 + vy1 * time - g * time * time / 2f;
                go.transform.position = new Vector3(x, y, z);
            }
        }
    }
}
