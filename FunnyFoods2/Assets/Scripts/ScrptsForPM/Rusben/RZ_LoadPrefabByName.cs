﻿using UnityEngine;
using System.Collections;
namespace HutongGames.PlayMaker.Actions
{
	public class RZ_LoadPrefabByName : FsmStateAction 
	{
		public FsmString prefabName;
		public FsmGameObject storeResult;
		public FsmBool isInstantiate;

		public override void OnEnter ()
		{
			GameObject go = Resources.Load(prefabName.Value) as GameObject;
			if(isInstantiate.Value)
			{
				storeResult.Value = GameObject.Instantiate(go);
				storeResult.Value.name = go.name;
			}
			else storeResult.Value = go;
			Finish();
		}
	}
}