﻿// (c) Copyright HutongGames, LLC 2010-2013. All rights reserved.

using UnityEngine;

namespace HutongGames.PlayMaker.Actions
{
	[ActionCategory("FF2_Rusben")]
	[Tooltip("Gets the X,Y Position of the mouse and stores it in a Float Variables.")]
    public class RZ_GetMouseXY : FsmStateAction
	{
		public FsmFloat storeResultX;
        public FsmFloat storeResultY;
		public bool normalize;
        public FsmBool everyFrame;
        public FsmBool ScreenToWorldPoint;

		
		public override void Reset()
		{
			storeResultY = null;
            storeResultX = null;
			normalize = true;
		}

		public override void OnEnter()
		{
            DoGetMouseX();
			DoGetMouseY();
            if (!everyFrame.Value)
                Finish();
		}

		public override void OnUpdate()
		{
			DoGetMouseY();
            DoGetMouseX();
		}
		
		void DoGetMouseY()
		{
			if (storeResultY != null)
			{
				float ypos = Input.mousePosition.y;
				
				if (normalize)
					ypos /= Screen.height;
                if (ScreenToWorldPoint.Value)
                    storeResultY.Value = Camera.main.ScreenToWorldPoint(new Vector3(0, ypos, 0)).y;
                else
                    storeResultY.Value = ypos;
			}
		}

        void DoGetMouseX()
        {
            if (storeResultX != null)
            {
                float xpos = Input.mousePosition.x;

                if (normalize)
                    xpos /= Screen.width;
                if (ScreenToWorldPoint.Value)
                    storeResultX.Value = Camera.main.ScreenToWorldPoint(new Vector3(xpos, 0, 0)).x;
                else
                    storeResultX.Value = xpos;
            }
        }
	}
}


