﻿using System;using Bini.Utils.Anim;using Bini.Utils.Audio;
using UnityEngine;

namespace HutongGames.PlayMaker.Actions
{
	[ActionCategory("FF2_Rusben")]
	[Tooltip("Not Work With -1 Speed!!!")]
    public class RZ_PlayWithSound : FsmStateAction
	{
		Bini.Utils.Anim.AnimBasic LastAnim = null; 
		void StartClip(GameObject Obj, string ClipName, float Speed,WrapMode wrapmode)
		{
			LastAnim = Anim.PlayWithSound(Obj, ClipName, Speed, false, wrapmode,1f, null, LoopMode.Value);
			animstarted = true;
			if(Camera.main.gameObject.transform.FindChild("Audio:"+animName.Value))
			{
				audioSourceGO = Camera.main.transform.GetChild(Camera.main.transform.childCount-1).gameObject;
				audioSourceGO.GetComponent<AudioSource>().volume = audioVolume.Value;
			}
			else
			{
				if(highLowMedium!=null&&highLowMedium.Value != "")
				{
					AudioClip clip = Resources.Load("Sounds/"+highLowMedium.Value+ClipName)as AudioClip;
					if (clip)
                        audioSourceGO = Audio.PlaySound(clip, audioVolume.Value, 1, false, wrapmode == WrapMode.Loop).transform.gameObject;
				}
			}
//			Debug.Log("Started Clip " + ClipName.AddTime());
		}
		[RequiredField]
		[Tooltip("Game Object to play the animation on.")]
		public FsmOwnerDefault gameObject;
		
		[UIHint(UIHint.Animation)]
		[Tooltip("The name of the animation to play.")]
		public FsmString animName;
		public FsmFloat Speed=1f,audioVolume = 1f;

		[Tooltip("Event to send when the animation is finished playing. NOTE: Not sent with Loop or PingPong wrap modes!")]
		public FsmEvent finishEvent;
		
		[Tooltip("Event to send when the animation loops. If you want to send this event to another FSM use Set Event Target. NOTE: This event is only sent with Loop and PingPong wrap modes.")]
		public FsmEvent StartedEvent;
		[Tooltip("true == loop;false == onse")] 
		public FsmBool LoopMode;
		public FsmBool Identifier;
		public FsmBool NeedToWaitForEndPrevAnim = false;
		public FsmString highLowMedium=null;
		bool animstarted = false;
		[Tooltip("Stop playing the animation when this state is exited.")]
		public bool stopOnExit;
		GameObject go;
		bool storeNeedToWait;
		private AnimationState anim;
		private float prevAnimtTime;
		GameObject audioSourceGO;
		
		/*public override void Reset()
		{
			gameObject = null;
			animName = null;
			finishEvent = null;
			StartedEvent = null;
			stopOnExit = false;
		}*/
		
		public override void OnEnter()
		{
			animstarted = false;
			storeNeedToWait = NeedToWaitForEndPrevAnim.Value;
			DoPlayAnimation();
		}
		
		void DoPlayAnimation()
		{
			go = Fsm.GetOwnerDefaultTarget(gameObject);
			if(go.GetComponent<Animation>() || go.GetComponent<Animator>())
			{
			go.GetComponent<Animation>().wrapMode = WrapMode.Once;
			if (go == null || string.IsNullOrEmpty(animName.Value))
			{
				Finish();
				return;
			}
			
			if (string.IsNullOrEmpty(animName.Value))
			{
				LogWarning("Missing animName!");
				Finish();
				return;
			}
			
			if (go.GetComponent<Animation>() == null)
			{
				LogWarning("Missing animation component!");
				Finish();
				return;
			}
			
			anim = go.GetComponent<Animation>()[animName.Value];
			
			if (anim == null)
			{
				LogWarning("Missing animation: " + animName.Value);
				Finish();
				return;
			}
			WrapMode WM;
			if(!LoopMode.Value) WM = WrapMode.Once; else WM = WrapMode.Loop;
			if(!NeedToWaitForEndPrevAnim.Value||!go.GetComponent<Animation>().isPlaying) 
			{
				StartClip(go,animName.Value,Speed.Value,WM);
				prevAnimtTime = anim.time/Mathf.Abs(Speed.Value);
				Fsm.Event(StartedEvent);
				if (StartedEvent == null && finishEvent == null)
					Finish();
			}
			}
			else
			{
				Debug.LogError("Havent Animation/Animator Component! obj.name = "+ go.name+ "animClipName = "+animName.Value+"fsmOwnerName = "+ Fsm.OwnerName);
				Finish();
			}
		}
		
		public override void OnUpdate()
		{
//			Debug.Log("UpdateAnimWait  "+Time.time);
//			var go = Fsm.GetOwnerDefaultTarget(gameObject);
			if (go == null || anim == null)
			{
//				Debug.Log("anim or go == null".AddTime());
				return;
			}
			
			if (!go.GetComponent<Animation>().isPlaying&&animstarted)
			{
				Fsm.Event(finishEvent);
				Finish();
//				Debug.Log("finishLol".AddTime());
				return;
			//	Finish();
			}
			if(NeedToWaitForEndPrevAnim.Value && !go.GetComponent<Animation>().isPlaying)
			{
				WrapMode WM;
				if(!LoopMode.Value) WM = WrapMode.Once; else WM = WrapMode.Loop;
				StartClip(go,animName.Value,Speed.Value,WM);
				NeedToWaitForEndPrevAnim.Value = !NeedToWaitForEndPrevAnim.Value;
				prevAnimtTime = anim.time/Mathf.Abs(Speed.Value);
				Fsm.Event(StartedEvent);
				if(finishEvent == null)
					Finish();
			}
//			if (anim.wrapMode != WrapMode.ClampForever && anim.time > anim.length && prevAnimtTime < anim.length)
//			{
//				Fsm.Event(loopEvent);
//			}
		}
		
		public override void OnExit()
		{
			NeedToWaitForEndPrevAnim.Value=storeNeedToWait;
			if (stopOnExit)
			{
				StopAnimation();
			}
			if(LoopMode.Value&&audioSourceGO)
			{
				GameObject.Destroy(audioSourceGO);//Camera.main.gameObject.FindChild("Audio:" + animName.Value));
			}
			/*if(ChangeToOnceOnExit.Value)
			{
				var go = Fsm.GetOwnerDefaultTarget(gameObject);
				go.animation.wrapMode = WrapMode.Once;
			}*/
		}
		
		void StopAnimation()
		{
//			var go = Fsm.GetOwnerDefaultTarget(gameObject);
			if (go != null && go.GetComponent<Animation>() != null)
			{
				go.GetComponent<Animation>().Stop(animName.Value);
			}
		}
	}
}
