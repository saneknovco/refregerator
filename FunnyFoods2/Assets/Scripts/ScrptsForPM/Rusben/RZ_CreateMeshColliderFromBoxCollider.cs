﻿using UnityEngine;

namespace HutongGames.PlayMaker.Actions
{
	[ActionCategory("FF2_Rusben")]
	public class RZ_CreateMeshColliderFromBoxCollider : FsmStateAction
	{ 
		public FsmOwnerDefault obj;

		BoxCollider _box;
		Vector3 _size,_center;

		public override void OnEnter()
		{
			GameObject go = Fsm.GetOwnerDefaultTarget(obj);
			if((_box = go.GetComponent<BoxCollider>()) != null)
			{
				_size = _box.size/2f;
				_center = _box.center;

				Object.Destroy(_box);

				Mesh mesh = new Mesh();
				mesh.name = "New_Mesh_For_Collider";
				mesh.vertices = new Vector3[4] 
				{
					new Vector3(-_size.x+_center.x, -_size.y+_center.y, 0.01f), 
					new Vector3(_size.x+_center.x, -_size.y+_center.y, 0.01f), 
					new Vector3(_size.x+_center.x, _size.y+_center.y, 0.01f), 
					new Vector3(-_size.x+_center.x, _size.y+_center.y, 0.01f) 
				};
				mesh.uv = new Vector2[4] {new Vector2 (0, 0), new Vector2 (0, 1), new Vector2(1, 1), new Vector2 (1, 0)};
				mesh.triangles = new int[6] {0, 2, 1, 0, 3, 2};
				mesh.RecalculateNormals();

				MeshCollider meshColider = go.AddComponent<MeshCollider>();
				meshColider.sharedMesh = mesh;
			}
			Finish();
		}

	}
}   