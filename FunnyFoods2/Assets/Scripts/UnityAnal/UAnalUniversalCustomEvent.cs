﻿
using UnityEngine;
using System.Collections;
using System.Collections.Generic;

#if MAGE_ANALYTICS
using UnityEngine.Analytics;
#endif


namespace HutongGames.PlayMaker.Actions
{
	public enum UAnalParameters
	{
		gameName,
		action
	}
	public class UAnalUniversalCustomEvent : FsmStateAction 
	{
		[RequiredField]
		public FsmString eventName;
		[RequiredField]
		public UAnalParameters parametr;
		[RequiredField]
		public FsmString parametrValue;

		public override void OnEnter ()
		{
			string compositEventName = string.Format("{0}_{1}", System.Enum.GetName(typeof(RuntimePlatform), Application.platform), eventName.Value);
			#if MAGE_ANALYTICS
			Analytics.CustomEvent(compositEventName , new Dictionary<string, object>
				{
					{ System.Enum.GetName(typeof(UAnalParameters), parametr), parametrValue.Value }
				});
			#endif
			Finish();
		}
	}
}