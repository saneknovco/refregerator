﻿using UnityEngine;
using System.Collections;

#if MAGE_ANALYTICS
using UnityEngine.Analytics;
#endif

public class UAnal_PlayButton : MonoBehaviour {

	void OnMouseDown()
	{
		#if MAGE_ANALYTICS
		Analytics.CustomEvent("PlayButton", null);
		#endif
	}
}
