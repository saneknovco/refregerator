﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

#if MAGE_ANALYTICS
using UnityEngine.Analytics;
#endif


namespace HutongGames.PlayMaker.Actions
{
	public class UAnalCustomEvent : FsmStateAction 
	{
		public FsmString eventName;

		public override void OnEnter ()
        {
            #if MAGE_ANALYTICS
                string fullName = eventName.Value;
                if (fullName.Contains("_"))
                {
                    int seporatorIndex = fullName.IndexOf("_", 0);
                    string nameOfEvent = fullName.Substring(0, seporatorIndex);
                    string parametrValue = fullName.Substring(seporatorIndex + 1);
                    Analytics.CustomEvent(nameOfEvent , new Dictionary<string, object>
                    {
                        { "gameName", parametrValue }
                    });	
                }
                else
                {
                    Analytics.CustomEvent(fullName, null);
                }

            #endif
			Finish();
        }
	}
}