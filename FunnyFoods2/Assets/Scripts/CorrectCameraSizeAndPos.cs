﻿using UnityEngine;

public class CorrectCameraSizeAndPos : MonoBehaviour 
{
	public float CameraXShift = 0, CameraYShift = 0;
	public bool CorrectThisCam = false;
	public GameObject[] SetActiveGameObjects;
	void CorrectCameraSizeAndPosFunc(Camera cam)
	{
		float aspect = cam.aspect;
		//16on9
		if (aspect >= 1.7f)
		{
			cam.transform.position += new Vector3(CameraXShift, CameraYShift, 0);
			cam.orthographicSize = 681f;
			
		} 
		//3on2
		else if(aspect >= 1.49f)
		{
			cam.transform.position += new Vector3(CameraXShift, CameraYShift, 0);
			cam.orthographicSize = 683f;
		}
		//4on3
		else
		{
			cam.transform.position = new Vector3(0, 0, cam.transform.position.z);
			cam.orthographicSize = 768;
		}
	}
	void Awake () 
	{

		if(CorrectThisCam)
			CorrectCameraSizeAndPosFunc(transform.GetComponent<Camera>());
		else
			CorrectCameraSizeAndPosFunc(Camera.main);

		GameObject _GUIElements = transform.gameObject.FindChild("GUIElements");
        if(_GUIElements != null)
		    _GUIElements.SetActive(true);

		if(SetActiveGameObjects != null)
			if(SetActiveGameObjects.Length > 0)
				for (int i = 0; i < SetActiveGameObjects.Length; i++) 
				{
					SetActiveGameObjects[i].SetActive(true);
				}
	}
}