﻿using UnityEngine;
using System.Collections.Generic;

public class AquariumWater : MonoBehaviour {
	public float AntialiasSize = 5f; // in pixels

	public int WaveResolution = 256; // number of slices

	public float VelocityLimit = 40f;

	public float TimeScale = 20f;
	public int IterationsPerFrame = 1;

	public float Damping = 0.05f;
	public Vector2 Tension = new Vector2(0.75f, 0.25f);

	public float InfluenceAttenuationLength = 50f;
	public Vector2 CollisionVelocityScale = new Vector2(0.1f, 0.25f);

	public float SplashSizeMin = 10f, SplashSizeMax = 20f;
	public float SplashEnergyMin = 1f, SplashEnergyMax = 2f;
	public float SplashMinVelocity = 10f, SplashSpeedScale = 1f;

	//public float WaterTexScale = 20f;
	public float WaterTexFPS = 15f;
	public string WaterTexPath = "";

	public string WaterLayerPrefix = "Water";

	ParticleEmitter splash_emitter = null;

	BoxCollider2D box_collider = null;
	Rect collider_bounds;

	Material water_material = null;
	Material splash_material = null;

	struct WaterPoint {
		public float pos, vel, acc;
	}

	Mesh water_mesh = null;
	Vector3[] water_verts = null;
	Vector2[] water_uvs = null;
	WaterPoint[] water_state = null;

	Texture2D[] water_frames;
	int water_frame_id = 0;
	float water_frame_next_time = -1;
	
	public Texture current_tex {
		get { return water_frames[water_frame_id]; }
	}
	
	void UpdateWaterFrames() {
		if (water_material == null) return;
		water_material.SetTexture("_WaterTex", current_tex);
		//water_material.SetTextureScale("_WaterTex", Vector2.one * (1f/WaterTexScale));

		if (splash_material != null) {
			splash_material.SetTexture("_WaterTex", current_tex);
			splash_material.SetTextureScale("_WaterTex", water_material.GetTextureScale("_WaterTex"));
		}

		if (Time.time > water_frame_next_time) {
			water_frame_id = (water_frame_id + 1) % water_frames.Length;
			water_frame_next_time = Time.time + (1f/WaterTexFPS);
		}
	}
	
	static Texture2D[] LoadFrames(string path, bool compress=false) {
		var pic_objs = Resources.LoadAll(path, typeof(Texture2D));
		var frames = new Texture2D[pic_objs.Length];
		for (int i = 0; i < pic_objs.Length; i++) {
			frames[i] = pic_objs[i] as Texture2D;
		}
		return frames;
	}

	Mesh MakeWaveMesh() {
		int nColumns = Mathf.Max(WaveResolution, 1);
		int rows_per_column = 2;
		int verts_per_point = rows_per_column+1;
		int nVerts = (nColumns+1)*verts_per_point;
		int nPoints = nColumns+1;

		water_state = new WaterPoint[nPoints];
		water_verts = new Vector3[nVerts];
		water_uvs = new Vector2[nVerts];

		var colors = new Color32[nVerts];
		var color_full = Color.white;
		var color_transparent = Color.white;
		color_transparent.a = 0f;
		for (int i = 0; i < nPoints; i++) {
			int vi = i*verts_per_point;
			colors[vi] = color_full;
			colors[vi+1] = color_full;
			colors[vi+2] = color_transparent;
		}

		var triangles = new List<int>();
		for (int ci = 0; ci < nColumns; ci++) {
			int vA = ci*verts_per_point;
			int vB = vA + verts_per_point;
			for (int ri = 0; ri < rows_per_column; ri++) {
				triangles.Add(vA); triangles.Add(vA+1); triangles.Add(vB);
				triangles.Add(vB); triangles.Add(vA+1); triangles.Add(vB+1);
				vA++; vB++;
			}
		}

		water_mesh = new Mesh();
		water_mesh.vertices = water_verts;
		water_mesh.uv = water_uvs;
		water_mesh.triangles = triangles.ToArray();
		water_mesh.colors32 = colors;
		return water_mesh;
	}

	void UpdateWave(bool recalculate_bounds=false) {
		int nPoints = water_state.Length;

		// update velocities
		int nPoints1 = nPoints-1;
		float damping = Mathf.Clamp01(Damping);
		float tension_x = Mathf.Clamp01(Tension.x);
		float tension_y = Mathf.Clamp01(Tension.y);
		float dt = (TimeScale * Time.deltaTime) / IterationsPerFrame;
		for (int iteration = 0; iteration < IterationsPerFrame; iteration++) {
			for (int i = 0; i < nPoints; i++) {
				var ps = water_state[i];
				float dy_neighbors = (i > 0 ? (water_state[i-1].pos - ps.pos) : 0f);
				if (i < nPoints1) dy_neighbors += (water_state[i+1].pos - ps.pos);
				float acceleration = (tension_x * dy_neighbors) - (tension_y * ps.pos) - (ps.vel * damping);
				ps.vel += acceleration * dt;
				ps.vel = Mathf.Clamp(ps.vel, -VelocityLimit, VelocityLimit);
				water_state[i] = ps;
			}
			for (int i = 0; i < nPoints; i++) {
				var ps = water_state[i];
				ps.pos += ps.vel * dt;
				water_state[i] = ps;
			}
		}

		var scale = transform.lossyScale;
		float y_scale_inv = 1f / scale.y;

		// Bounds are in global coordinates, but here we need local ones
		float x_min = box_collider.offset.x - box_collider.size.x*0.5f;
		float y_min = box_collider.offset.y - box_collider.size.y*0.5f;
		float x_max = box_collider.offset.x + box_collider.size.x*0.5f;
		float y_max = box_collider.offset.y + box_collider.size.y*0.5f;
		collider_bounds = Rect.MinMaxRect(x_min, y_min, x_max, y_max);

		var main_cam = Camera.main;
		float antialias_abs_size = (main_cam.orthographicSize*2) * (AntialiasSize / main_cam.pixelHeight);
		float antialias_local_size = antialias_abs_size * y_scale_inv;

		float x_step = (x_max - x_min) / (nPoints-1);
		float u_step = 1f / (nPoints-1);

		float y_size_inv = 1f / (y_max - y_min);

		int vi = 0;
		Vector3 vp = new Vector3(x_min, y_min, 0f);
		Vector2 uv = new Vector2();

		// update positions and vertices
		for (int i = 0; i < nPoints; i++) {
			// base
			vp.y = y_min;
			uv.y = 0f;
			water_verts[vi] = vp;
			water_uvs[vi] = uv;
			vi++;

			// surface
			vp.y = y_max + water_state[i].pos * y_scale_inv;
			uv.y = (vp.y - y_min) * y_size_inv;
			water_verts[vi] = vp;
			water_uvs[vi] = uv;
			vi++;

			// antialias
			vp.y = vp.y + antialias_local_size;
			uv.y = (vp.y - y_min) * y_size_inv;
			water_verts[vi] = vp;
			water_uvs[vi] = uv;
			vi++;

			vp.x += x_step;
			uv.x += u_step;
		}

		water_mesh.vertices = water_verts;
		water_mesh.uv = water_uvs;
		if (recalculate_bounds) water_mesh.RecalculateBounds();
	}

	// Note: triggered only if other_collider has Rigidbody2D
	void OnTriggerEnter2D(Collider2D other_collider) {
		ProcessCollision(other_collider, true);
	}
	void OnTriggerStay2D(Collider2D other_collider) {
		ProcessCollision(other_collider, false);
	}
	void OnTriggerExit2D(Collider2D other_collider) {
		ProcessCollision(other_collider, true);
	}

	void ProcessCollision(Collider2D other_collider, bool surface_broken) {
		var tfm = transform;

		var pos_abs = other_collider.transform.position;
		var pos = tfm.InverseTransformPoint(pos_abs);
		
		var pos_proj = pos;
		pos_proj.y = collider_bounds.yMax;
		pos_proj = tfm.TransformPoint(pos_proj);
		float y_delta = Vector3.Dot((pos_abs - pos_proj), tfm.up);
		float radius = other_collider.bounds.extents.magnitude;

		int nPoints = water_state.Length;
		int nPoints1 = nPoints-1;
		float nPoints1_inv = 1f / nPoints1;
		float radius_u = radius / (tfm.lossyScale.x * collider_bounds.width);
		float radius_u_inv = 1f / radius_u;
		float pos_u = (pos.x - collider_bounds.x) / collider_bounds.width;
		int i0 = Mathf.FloorToInt(Mathf.Clamp01(pos_u - radius_u)*nPoints1);
		int i1 = Mathf.CeilToInt(Mathf.Clamp01(pos_u + radius_u)*nPoints1);

		Vector3 rb_velocity = Vector3.zero;

		var kvt = other_collider.GetComponent<KinematicVelocityTracker>();
		if (kvt != null) rb_velocity += kvt.velocity;

		float speed_y = Vector3.Dot(rb_velocity, tfm.up)*CollisionVelocityScale.y;
		float speed_x = Vector3.Dot(rb_velocity, tfm.right)*CollisionVelocityScale.x;
		if (Mathf.Max(Mathf.Abs(speed_x), Mathf.Abs(speed_y)) <= Mathf.Epsilon) return;

		y_delta = Mathf.Abs(y_delta);
		float influence = Mathf.Exp(-Mathf.Max(y_delta-radius, 0f)/InfluenceAttenuationLength);
		if (influence < 0.01f) return;

		float speed_y_abs = Mathf.Abs(speed_y);
		float splash_vel = rb_velocity.magnitude;
		float vel_radius = radius+speed_y_abs;
		bool touches_surface = (y_delta < vel_radius) || surface_broken;
		float splash_influence = 1f - Mathf.Clamp01((y_delta-Mathf.Abs(speed_y))/vel_radius);

		for (int i = i0; i <= i1; i++) {
			float du_r = Mathf.Clamp(((i * nPoints1_inv) - pos_u) * radius_u_inv, -1f, 1f);
			float kx = Mathf.Sin(du_r * Mathf.PI);
			float ky = Mathf.Cos(du_r * Mathf.PI * 0.5f);
			float ps_acceleration = (speed_x * kx + speed_y * ky) * influence;
			var ps = water_state[i];
			ps.vel += ps_acceleration;
			water_state[i] = ps;

			if (touches_surface && (speed_y_abs > SplashMinVelocity)) {
				float emission_intensity = (1f - du_r) * splash_influence;
				//var point_pos_abs = pos_abs;
				//var point_pos_abs = pos_proj + tfm.up * ps.pos;
				var point_pos_abs = tfm.TransformPoint(water_verts[i*3+1]);
				point_pos_abs.z -= 0.5f;
				//var emission_vel = ((Vector3)Random.insideUnitCircle) * ps.vel;// * emission_intensity;
				var emission_vel = Random.onUnitSphere;
				emission_vel.z = 0;
				emission_vel = emission_vel.normalized * splash_vel;
				emission_vel.y += Mathf.Abs(emission_vel.y);
				emission_vel.y += speed_y_abs * SplashSpeedScale;
				var splash_size = Random.Range(SplashSizeMin, SplashSizeMax);// * emission_intensity;
				var splash_energy = Random.Range(SplashEnergyMin, SplashEnergyMax) * emission_intensity;
				splash_emitter.Emit(point_pos_abs, emission_vel, splash_size, splash_energy, Color.white);
			}
		}
	}

	void Awake() {
		water_frames = LoadFrames(WaterTexPath);

		var rndr = GetComponent<Renderer>();
		var mesh_filter = GetComponent<MeshFilter>();

		box_collider = GetComponent<BoxCollider2D>();
		if (box_collider == null) {
			box_collider = gameObject.AddComponent<BoxCollider2D>();
			box_collider.offset = (Vector2)mesh_filter.sharedMesh.bounds.center;
			box_collider.size = (Vector2)mesh_filter.sharedMesh.bounds.size;
		}
		box_collider.isTrigger = true;

		water_material = rndr.material; // make copy

		mesh_filter.sharedMesh = MakeWaveMesh(); // AFTER initializing box_collider

		var splash_emitter_tfm = transform.FindChild("SplashEmitter");
		if (splash_emitter_tfm != null) {
			splash_emitter = splash_emitter_tfm.GetComponent<ParticleEmitter>();
			var splash_renderer = splash_emitter.GetComponent<Renderer>();
			splash_material = splash_renderer.material; // make copy
		}

		if (!string.IsNullOrEmpty(WaterLayerPrefix)) {
			string prefix_invariant = WaterLayerPrefix.ToLowerInvariant();
			foreach (Transform child in transform) {
				if (child.name.ToLowerInvariant() == prefix_invariant) {
					var child_mesh_filter = child.GetComponent<MeshFilter>();
					if (child_mesh_filter != null) child_mesh_filter.sharedMesh = water_mesh;
				}
			}
		}
	}
	
	void Start() {
		UpdateWave(true);
		UpdateWaterFrames();
	}
	
	void Update() {
		UpdateWave();
		UpdateWaterFrames();
	}
}
