﻿using UnityEngine;
using System.Collections.Generic;

public class KinematicVelocityTracker : MonoBehaviour {
	public Vector3 velocity;
	public Vector3 acceleration;

	Vector3 pos0, vel0;

	void Start() {
		pos0 = transform.position;
	}
	
	void Update() {
		var pos = transform.position;
		velocity = pos - pos0;
		acceleration = velocity - vel0;
		pos0 = pos;
		vel0 = velocity;
	}
}
