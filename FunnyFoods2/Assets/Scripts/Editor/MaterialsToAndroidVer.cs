﻿using UnityEngine;
using UnityEditor;
using System.Collections;
using System.Collections;
using System.Collections.Generic;

public class MaterialsToAndroidVer : ScriptableWizard {

	static Material[] m;

	static void OnWizardCreate()
	{
		Object[] o = Selection.GetFiltered(typeof(Material), SelectionMode.DeepAssets);
		if(o.Length==0) 
		{
			Debug.Log ("zeroLength");
			return;
		}
		else
			Debug.Log(o.Length);
		m = new Material[o.Length];
		for(int i = 0; i < o.Length; i++)
			m[i] = o[i] as Material;
		for(int i = 0; i < m.Length; i++)
		{
			Material M = m[i];
			Texture t = M.mainTexture;
			if(t==null) continue;
			string s = AssetDatabase.GetAssetPath(t);
			string sNew = s.Insert(s.Length-4, "_Alpha");
			if(AssetImporter.GetAtPath(sNew) == null)
			{
				TextureImporter ti = AssetImporter.GetAtPath(s) as TextureImporter;
				ti.SetPlatformTextureSettings("Android", 2048, TextureImporterFormat.ETC_RGB4, 100, false);
				ti.npotScale = TextureImporterNPOTScale.ToNearest;
				ti.SaveAndReimport();
				AssetDatabase.CopyAsset(s, sNew);
				TextureImporter tiNew = AssetImporter.GetAtPath(sNew) as TextureImporter;
				tiNew.SetPlatformTextureSettings("Android", pot(Mathf.Max(t.height,t.width)/2), TextureImporterFormat.Alpha8);
				tiNew.npotScale = TextureImporterNPOTScale.None;
//				tiNew.maxTextureSize = pot(Mathf.Max(t.height,t.width)/2);
			}
			M.shader = Shader.Find("Unlit/AndroidTransparentAndrew");
			M.SetTexture("_AlphaMap", (Texture)AssetDatabase.LoadAssetAtPath(sNew, typeof(Texture)));
		}
		AssetDatabase.SaveAssets();
		AssetDatabase.Refresh();
		for(int i = 0; i < m.Length; i++)
		{
			Material M = m[i];
			Texture t = M.mainTexture;
			if(t==null) continue;
			string s = AssetDatabase.GetAssetPath(t);
			string sNew = s.Insert(s.Length-4, "_Alpha");
			M.SetTexture("_AlphaMap", (Texture)AssetDatabase.LoadAssetAtPath(sNew, typeof(Texture)));
		}
	}
	static int pot(int n)
	{
		if(n>=2048) return 2048;
		if(n>=1024) return 1024;
		if(n>=512) return 512;
		if(n>=256) return 256;
		if(n>=128) return 128;
		if(n>=64) return 64;
		return 32;
	}
	[MenuItem("Mage/AndroidMats", priority = 20006)]
//	[ContextMenuItem("MatsToAndroid", "OnWizardCreate")]
	static void DoMatReDo()
	{
		OnWizardCreate();
	}
}

