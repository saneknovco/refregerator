﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class TouchEffect : MonoBehaviour {

	public GameObject effectTouch;
	private GameObject _touchGO;

	void Update()
	{
#if UNITY_EDITOR
		if(Input.GetMouseButtonDown(0))
		{
			if(_touchGO)
			{
				Vector3 mousePosition=this.gameObject.GetComponent<Camera>().ScreenToWorldPoint(Input.mousePosition);
				mousePosition= new Vector3(mousePosition.x,mousePosition.y,5);
				_touchGO.transform.position=mousePosition;
			}
			else
			{
				CreateTouchGO ();
			}
		}
		if(Input.GetMouseButton(0))
		{
			if(_touchGO)
			{
				Vector3 mousePosition=this.gameObject.GetComponent<Camera>().ScreenToWorldPoint(Input.mousePosition);
				mousePosition= new Vector3(mousePosition.x,mousePosition.y,5);
				_touchGO.transform.position=mousePosition;
			}
		}

		if(Input.GetMouseButtonUp(0))
		{
			if(_touchGO!=null)
				StartCoroutine (DestroyTouchGO (_touchGO, 0.5f));
		}
#else
		if(Input.touchCount>0)
		{
			if(_touchGO)
			{
				Vector3 mousePosition=this.gameObject.GetComponent<Camera>().ScreenToWorldPoint(Input.mousePosition);
				//Vector3 mousePosition=this.gameObject.camera.ScreenToWorldPoint(Input.mousePosition);
				mousePosition= new Vector3(mousePosition.x,mousePosition.y,5);
				_touchGO.transform.position=mousePosition;
			}
			else
			{
				CreateTouchGO ();
			}
		}

		if(Input.touchCount<=0)
		{
			if(_touchGO!=null)
				StartCoroutine (DestroyTouchGO (_touchGO, 0.5f));
		}
#endif
	}

	void CreateTouchGO()
	{
		this.gameObject.GetComponent<Camera>().orthographicSize = Camera.main.orthographicSize;
		this.gameObject.GetComponent<Camera>().depth = Camera.main.depth + 10;
		Camera.main.cullingMask &= ~(1 << LayerMask.NameToLayer("TransparentFX"));
		Vector3 position = this.gameObject.GetComponent<Camera>().ScreenToWorldPoint(Input.mousePosition);
		position = new Vector3 (position.x, position.y, 5);
		effectTouch.transform.position = position;
		_touchGO= Instantiate(effectTouch)as GameObject;
		_touchGO.transform.parent = this.gameObject.transform;
		_touchGO.transform.position=position;
		_touchGO.SetActive(true);
	}

	IEnumerator DestroyTouchGO(GameObject go, float waitTime)
	{
		yield return new WaitForSeconds(waitTime);
		Destroy (go);
		go = null;
	}



}
