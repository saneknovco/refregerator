﻿using UnityEngine;
using System.Collections;
using Mage.Effects;

/// <summary>
/// Mage app user interface event proxy.
/// пример интегрции компонентов MageApp с Приложением
/// </summary>
public class MageAppUIEventProxy : MonoBehaviour {


	public bool hideStampWhenIAP = true;
	public GameObject parentUIView = null;

	public float fadingOutTime = 0.5f;
	public float fadingInTime = 0.5f;


	/// <summary>
	/// Locks the game UI - реакция при работе с кроссом & другими модулями MageApp
	/// </summary>
	/// <param name="isLock">If set to <c>true</c> is lock.</param>
	private void LockGameUI(bool isLock)
	{       
		// TO DO
		Debug.Log ("Customize logic: Lock flag: ".AddTime() + isLock + " appstate:uiview: " + AppState.Instance.UIViewMode);
		///// ДЛЯ ПЛЕЙМЕЙКЕРА!!!
		//if(isLock) PlayMakerFSM.BroadcastEvent("lockUI_sample_event");
		//else PlayMakerFSM.BroadcastEvent("unlockUI_sample_event");
		////
	}

	/// <summary>
	/// показать\спрятать марку(банер) кросс-промо.
	/// </summary>
	/// <param name="enabled">If set to <c>true</c> enabled.</param>
	private void EnabledStamp(bool enabled)
	{        
		// прячем\показываем марку
		if(CPStateController.Instance.IsEmptyAppListForCurrentLang) enabled = false;
		if (Messenger.eventTable.ContainsKey(AppState.EventTypes.cpStampAction))
			Messenger.Broadcast<CPStampAction, bool>(AppState.EventTypes.cpStampAction, CPStampAction.Show, enabled);
	}

	/// <summary>
	/// Locks the user interface widgets.
	/// </summary>
	/// <param name="isLock">If set to <c>true</c> is lock.</param>
	private void LockUIWidgets(bool isLock)
	{
		// блокируем нажатия на кнопки (если нужно)
		AppState.Instance.lockWidgets = isLock;
	}

    #region Кросс



	private void DoCreateCPView()
	{
		if(AppState.Instance.IsShowUIView || AppState.Instance.lockWidgets || ButtonWidget.countTouchInProcess>0)
			return;

		MgScreenFaderController.Run(MageAppFadingMode.OutContinued, fadingOutTime);		
		StartCoroutine(MgCoroutineHelper.WaitThenCallback(fadingOutTime + 0.25f, () =>
		{	
			if (Messenger.eventTable.ContainsKey(AppState.EventTypes.cpCreateUIView))
				Messenger.Broadcast<GameObject>(AppState.EventTypes.cpCreateUIView, parentUIView);
				MgScreenFaderController.Run(MageAppFadingMode.In, fadingInTime);
		}));
	}



    // ОТКРЫТИЕ кроса
    private void OnBeforeCreateUIView_CP()
    {
		LockGameUI(true);
    }

    // ОТМЕНА PARENTAL GATE при открытии кроса
    private void OnCancelCreateUIView_CP()
    {
        if (Debug.isDebugBuild) Debug.Log("Cross-promotion: parental gate rejected".AddTime());
		LockGameUI(false);
    }

    /// <summary>
    /// обработка  сообщения отображения префаба кросса
    /// </summary>
    /// <param name="isShow">If set to <c>true</c> is show.</param>
	private void OnShowUIView_CP(bool isShow)
    {
        if (Debug.isDebugBuild) Debug.Log("Cross-promotion: load status...".AddTime() + isShow);
        //  ЕСЛИ НЕ СМОГЛИ ЗАГРУЗИТЬ ПРЕФАБ
		if(isShow == false)
		{
			LockGameUI(false);
		}
		else
		{
            if (Debug.isDebugBuild) Debug.Log("префаб кросса загружен и отображается");
		}
    }
    
    // ЗАКРЫТИЕ 
    private void OnAfterCloseUIView_CP()
    {	
		LockGameUI(false);
		StartCoroutine(MgCoroutineHelper.WaitThenCallback(60, ()=>{ Debug.Log(" appstate:uiview: " + AppState.Instance.UIViewMode);}));
	}

    #endregion

    #region IAP (InApp)

    // ОТКРЫТИЕ IAP
	void OnBeforeCreateUIView_IAP ()
	{
		LockGameUI(true);

		if(hideStampWhenIAP ){
			// прячем марку
			EnabledStamp(false);
		}
	}

    // ОТМЕНА PARENTAL GATE при открытии IAP
    private void OnCancelCreateUIView_IAP()
    {
        if (Debug.isDebugBuild) Debug.Log("IAP: parental gate rejected".AddTime());
        LockGameUI(false);
        if (hideStampWhenIAP) EnabledStamp(true);
    }
	
	void OnShowUIView_IAP (bool isShow)
	{
        if (Debug.isDebugBuild) Debug.Log("IAP: load status...".AddTime() + isShow);
        //показываем марку
		if(isShow == false)
		{
			LockGameUI(false);
            if (hideStampWhenIAP) EnabledStamp(true);
		}
	}

    private void OnCloseUIView_IAP(bool refreshProduct)
    {
		// Показываем марку 
		StartCoroutine(MgCoroutineHelper.WaitThenCallback(AppConfig.timeWaitFinishing, ()=> { 
			LockGameUI(false);
            if (hideStampWhenIAP) EnabledStamp(true);
		}));
    }

    private void OnCloseUIView_GFV()
    {
        // Показываем марку 
        StartCoroutine(MgCoroutineHelper.WaitThenCallback(AppConfig.timeWaitFinishing, () =>
        {
            LockGameUI(false);
            if (hideStampWhenIAP) EnabledStamp(true);
        }));
    }


    #endregion

    void Awake()
    {
		#region Подпись на события MageApp

		// Cross-promotion
		Messenger.AddListener(AppState.EventTypes.cpBeforeCreateUIView, OnBeforeCreateUIView_CP);
        Messenger.AddListener(AppState.EventTypes.cpCancelCreateUIView, OnCancelCreateUIView_CP);
        Messenger.AddListener<bool>(AppState.EventTypes.cpShowUIView, OnShowUIView_CP);        
        Messenger.AddListener(AppState.EventTypes.cpAfterCloseUIView, OnAfterCloseUIView_CP);

        // Подписываемся если не полная версия
        if (AppSettings.Instance.appInfo.ActiveAppType != MageAppType.Full)
        {
            // IAP (In-App)
            if (AppSettings.Instance.IAP.enabled)
            {
                Messenger.AddListener(AppState.EventTypes.iapBeforeCreateUIView, OnBeforeCreateUIView_IAP);
                Messenger.AddListener(AppState.EventTypes.iapCancelCreateUIView, OnCancelCreateUIView_IAP);
                Messenger.AddListener<bool>(AppState.EventTypes.iapShowUIView, OnShowUIView_IAP);
                Messenger.AddListener<bool>(AppState.EventTypes.iapBeforeCloseUIView, OnCloseUIView_IAP);
            }
            else // Get Full version
            {
                Messenger.AddListener(AppState.EventTypes.gfvBeforeCreateUIView, OnBeforeCreateUIView_IAP);
                Messenger.AddListener(AppState.EventTypes.gfvCancelCreateUIView, OnCancelCreateUIView_IAP);
                Messenger.AddListener<bool>(AppState.EventTypes.gfvShowUIView, OnShowUIView_IAP);
                Messenger.AddListener(AppState.EventTypes.gfvAfterCloseUIView, OnCloseUIView_GFV);
            }
		}
		#endregion

		// разблокировка виджетов Info & Stamp
        AppState.Instance.lockWidgets = false;// явно сбрасываем

        // автоматически показывать марку при возврате из игр!
        // явно посылаем сообщения для показа марки!
        if (Messenger.eventTable.ContainsKey(AppState.EventTypes.cpStampAction))
            Messenger.Broadcast<CPStampAction, bool>(AppState.EventTypes.cpStampAction, CPStampAction.Show, true);
        // задержка в 1 sec - менее стабильно зависит от логики
        //StartCoroutine(MgCoroutineHelper.WaitThenCallback(1.0f, () =>
        //{
        //    if (Messenger.eventTable.ContainsKey(AppState.EventTypes.cpStampAction))
        //        Messenger.Broadcast<CPStampAction, bool>(AppState.EventTypes.cpStampAction, CPStampAction.Show, true);
        //}));

		if (parentUIView == null) parentUIView = AppState.Instance.UIContainer;   
    }

    void OnDestroy()
	{
		#region Отписываемся от событий MageApp

		if (Messenger.eventTable.ContainsKey(AppState.EventTypes.cpBeforeCreateUIView))
            Messenger.RemoveListener(AppState.EventTypes.cpBeforeCreateUIView, OnBeforeCreateUIView_CP);
        if (Messenger.eventTable.ContainsKey(AppState.EventTypes.cpCancelCreateUIView))
            Messenger.RemoveListener(AppState.EventTypes.cpCancelCreateUIView, OnCancelCreateUIView_CP);
        if (Messenger.eventTable.ContainsKey(AppState.EventTypes.cpShowUIView))
            Messenger.RemoveListener<bool>(AppState.EventTypes.cpShowUIView, OnShowUIView_CP);
        if (Messenger.eventTable.ContainsKey(AppState.EventTypes.cpAfterCloseUIView))
            Messenger.RemoveListener(AppState.EventTypes.cpAfterCloseUIView, OnAfterCloseUIView_CP);

        if (AppSettings.Instance.appInfo.ActiveAppType != MageAppType.Full)
        {
            if (AppSettings.Instance.IAP.enabled)
            {
                if (Messenger.eventTable.ContainsKey(AppState.EventTypes.iapBeforeCreateUIView))
                    Messenger.RemoveListener(AppState.EventTypes.iapBeforeCreateUIView, OnBeforeCreateUIView_IAP);
                if (Messenger.eventTable.ContainsKey(AppState.EventTypes.iapCancelCreateUIView))
                    Messenger.RemoveListener(AppState.EventTypes.iapCancelCreateUIView, OnCancelCreateUIView_IAP);
                if (Messenger.eventTable.ContainsKey(AppState.EventTypes.iapShowUIView))
                    Messenger.RemoveListener<bool>(AppState.EventTypes.iapShowUIView, OnShowUIView_IAP);
                if (Messenger.eventTable.ContainsKey(AppState.EventTypes.iapBeforeCloseUIView))
                    Messenger.RemoveListener<bool>(AppState.EventTypes.iapBeforeCloseUIView, OnCloseUIView_IAP);
            }
            else
            {
                if (Messenger.eventTable.ContainsKey(AppState.EventTypes.gfvBeforeCreateUIView))
                    Messenger.RemoveListener(AppState.EventTypes.gfvBeforeCreateUIView, OnBeforeCreateUIView_IAP);
                if (Messenger.eventTable.ContainsKey(AppState.EventTypes.gfvCancelCreateUIView))
                    Messenger.RemoveListener(AppState.EventTypes.gfvCancelCreateUIView, OnCancelCreateUIView_IAP);
                if (Messenger.eventTable.ContainsKey(AppState.EventTypes.gfvShowUIView))
                    Messenger.RemoveListener<bool>(AppState.EventTypes.gfvShowUIView, OnShowUIView_IAP);
                if (Messenger.eventTable.ContainsKey(AppState.EventTypes.gfvAfterCloseUIView))
                    Messenger.RemoveListener(AppState.EventTypes.gfvAfterCloseUIView, OnCloseUIView_GFV);
            }
		}

		#endregion
	}
}