﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

namespace Bini.Utils.Audio {
	public static class Audio {
		static public float SoundVolume_Sounds = 1, SoundVolume_Text = 1, SoundVolume_Music = 1;

		public static void SetGlobalVolume(float Vol) {
			AudioListener.volume = Vol;
		}

		public static void AbortAllSounds() {
			foreach(Transform child in Camera.main.transform) {
				if (child.name.StartsWith("Audio:")) Object.Destroy(child.gameObject);
			}
		}

		public static void AbortSound(AudioClip audio_clip) {
			AbortSound(audio_clip.name);
		}
		public static void AbortSound(string ClipName) {
			string name = "Audio:"+ClipName;
			var parent = Camera.main.transform;
			for (int i = parent.childCount; i >= 0; i--) {
				var child = parent.GetChild(i);
				if (child.name == name) Object.Destroy(child.gameObject);
			}
		}

		public static AudioSource FindSound(AudioClip audio_clip) {
			return FindSound(audio_clip.name);
		}
		public static AudioSource FindSound(string ClipName) {
			var child = Camera.main.transform.Find("Audio:"+ClipName);
			return (child != null) ? child.GetComponent<AudioSource>() : null;
		}

		public static AudioSource PlaySound(string audio_path, float volume=1f,
		                                    float speed=1f, bool speed_is_time=false,
		                                    bool loop=false, float? time_start=null) {
			if (volume <= 0) return null;
			var audio_clip = Resources.Load("Sounds/"+audio_path) as AudioClip;
			return PlaySound(audio_clip, volume, speed, speed_is_time, loop, time_start);
		}
		public static AudioSource PlaySound(AudioClip audio_clip, float volume=1f,
		                                    float speed=1f, bool speed_is_time=false,
		                                    bool loop=false, float? time_start=null) {
			if (volume <= 0) return null;
			
			if (audio_clip == null) return null;
			
			var cam = Camera.main;
			if (cam == null) return null;
			
			var gameObj = new GameObject("Audio:"+audio_clip.name);
			gameObj.transform.parent = cam.transform;
			gameObj.transform.localPosition = Vector3.zero;

			var source = gameObj.AddComponent<AudioSource>();
			source.clip = audio_clip;
			source.volume = volume;
			if (time_start != null) source.time = (float)time_start;
			
			float duration = audio_clip.length / speed;
			if (speed_is_time) {
				float wrk = speed; speed = duration; duration = wrk;
			}
			
			source.loop = loop;
			
			source.pitch = speed;
			if (!loop) GameObject.Destroy(gameObj, duration + 0.5f);
			
			source.Play();
			
			return source;
		}

	}
}
