﻿using UnityEngine;
using System.Collections.Generic;
using Bini.Utils.Audio;

namespace Bini.Utils.Anim {
	public static class Anim {
		public static void SetAnimTime(GameObject obj, string anim_name, float time, bool is_normalized=true) {
			var anim = AnimBasic.Create(obj, anim_name);
			if (anim == null) return;
			if (is_normalized) {
				anim.normalizedTime = time;
			} else {
				anim.time = time;
			}
			anim.Update();
		}

		public static AnimBasic PlayWithSound(
			GameObject obj, string anim_name,
			float anim_speed=1f, bool speed_is_time=false,
			WrapMode wrap_mode=WrapMode.Default, float sound_volume=1f,
			float? time_start=null, bool align_sound=true)
		{
			AudioSource source;
			return PlayWithSound(out source, obj,  anim_name, anim_speed,  speed_is_time,
			                     wrap_mode,  sound_volume, time_start, align_sound);
		}

		public static AnimBasic PlayWithSound(out AudioSource source,
			GameObject obj, string anim_name,
			float anim_speed=1f, bool speed_is_time=false,
			WrapMode wrap_mode=WrapMode.Default, float sound_volume=1f,
			float? time_start=null, bool align_sound=true)
		{
			source = null;
			var anim = AnimBasic.Create(obj, anim_name);
			if (anim == null) return null;
			
			anim.enabled = true;

			anim.wrapMode = wrap_mode;
			
			if (speed_is_time) {
				anim_speed = 1f / anim_speed;
			}

			anim.Play(anim_speed, speed_is_time);
			if (time_start != null) anim.time = (float)time_start;
			
			float anim_length = 0;
			if (speed_is_time) {
				anim_length = Mathf.Abs(1f / anim_speed);
			} else {
				anim_length = anim.length / Mathf.Abs(anim_speed);
			}
			anim_length *= Mathf.Sign(anim_speed);

			if (align_sound) {
				source = Audio.Audio.PlaySound(anim_name, sound_volume, anim_length, true);
			} else {
				source = Audio.Audio.PlaySound(anim_name, sound_volume);
			}
			
			return anim;
		}
	}

	public abstract class AnimBasic {
		public bool anim_was_specified = false;
		public string anim_name;
		public static AnimBasic Create(GameObject obj, string anim_name) {
			if (obj == null) {
				Debug.LogError("Attempt to get \""+anim_name+"\" animation state of null object");
				return null;
			}

			var animation = obj.GetComponent<Animation>();
			if (animation != null) {
				var anim = new AnimOld(animation, anim_name);
				if (string.IsNullOrEmpty(anim.anim_name)) return null;
				return anim;
			}
			
			var animator = obj.GetComponent<Animator>();
			if (animator != null) {
				var anim = new AnimNew(animator, anim_name);
				if (string.IsNullOrEmpty(anim.anim_name)) return null;
				return anim;
			}
			
			if (Debug.isDebugBuild) Debug.LogError(string.Format(
				"anim {0}: object {1} has no Animation or Animator component!",
				anim_name, obj.name));
			return null;
		}
		public float WrapAnimTime() {
			float anim_time = time;
			float anim_len = length;
			var wrap_mode = wrapMode;
			if (wrap_mode == WrapMode.Default) {
				wrap_mode = clip.wrapMode;
			}
			switch (wrap_mode) {
			case WrapMode.Loop:
				anim_time = anim_time % anim_len;
				if (anim_time < 0) anim_time += anim_len;
				break;
			case WrapMode.PingPong:
				float anim_len2 = 2f*anim_len;
				anim_time = anim_time % anim_len2;
				if (anim_time < 0) anim_time += anim_len2;
				if (anim_time > anim_len) anim_time = anim_len2 - anim_time;
				break;
			default:
				anim_time = Mathf.Clamp(anim_time, 0f, anim_len);
				break;
			}
			return anim_time;
		}
		public abstract float length {get;}
		public abstract float speed {get; set;}
		public abstract float normalizedSpeed {get; set;}
		public abstract float time {get; set;}
		public abstract float normalizedTime {get; set;}
		public abstract WrapMode wrapMode {get; set;}
		public abstract AnimationClip clip {get;}
		public abstract Transform transform {get;}
		public abstract GameObject gameObject {get;}
		public abstract bool enabled {get; set;}
		public abstract bool isPlaying {get;}
		public abstract void Play(float anim_speed, bool speed_is_time);
		public abstract void Stop();
		public abstract void Update();
	}
	public class AnimOld : AnimBasic {
		public Animation animation;
		public AnimationState state;
		public AnimOld(Animation animation, string anim_name) {
			anim_was_specified = !string.IsNullOrEmpty(anim_name);
			if (!anim_was_specified) {
				foreach (AnimationState _state in animation) {
					if (animation.IsPlaying(_state.name)) {
						anim_name = _state.name;
						break;
					}
				}
			}
			this.anim_name = anim_name;
			this.animation = animation;
			if (!string.IsNullOrEmpty(anim_name)) {
				state = animation[anim_name];
				if (state == null) {
					var clip = Resources.Load<AnimationClip>("Animations/"+anim_name);
					if (clip != null) {
						animation.AddClip(clip, anim_name);
						state = animation[anim_name];
					}
				}
				if (state == null) this.anim_name = "";
			}
		}
		public override float length {get{return state.length;}}
		public override float speed {get{return state.speed;} set{state.speed = value;}}
		public override float normalizedSpeed {get{return state.normalizedSpeed;} set{state.normalizedSpeed = value;}}
		public override float time {get{return state.time;} set{state.time = value;}}
		public override float normalizedTime {get{return state.normalizedTime;} set{state.normalizedTime = value;}}
		public override WrapMode wrapMode {get{return state.wrapMode;}
			set{float anim_time = WrapAnimTime(); state.wrapMode = value; time = anim_time;}}
		public override AnimationClip clip {get{return state.clip;}}
		public override Transform transform {get{return animation.transform;}}
		public override GameObject gameObject {get{return animation.gameObject;}}
		public override bool enabled {get{return animation.enabled;} set{animation.enabled = value;}}
		public override bool isPlaying {get{return animation.isPlaying;}}
		public override void Play(float anim_speed, bool speed_is_time) {
			if (speed_is_time) anim_speed *= length;
			
			speed = anim_speed;
			if (anim_speed < 0) time = length;
			
			animation.Play(anim_name, PlayMode.StopAll);
		}
		public override void Stop() {
			animation.Stop(anim_name);
		}
		public override void Update() {
			animation.Sample();
		}
	}
	public class AnimNew : AnimBasic {
		public Animator animator;
		public AnimatorStateInfo asi;
		int _hash = 0;
		AnimationClip _clip;
		WrapMode _wrap_mode = WrapMode.Default;
		public AnimNew(Animator animator, string anim_name) {
			anim_was_specified = !string.IsNullOrEmpty(anim_name);
			if (!anim_was_specified) {
#if UNITY_5
				var _states = animator.GetCurrentAnimatorClipInfo(0);
#else
				var _states = animator.GetCurrentAnimationClipState(0);
#endif
				if (_states.Length != 0) {
					var clip_ = _states[0].clip;
					if (clip_ != null) anim_name = clip_.name;
				}
			}
			this.anim_name = anim_name;
			this.animator = animator;
			_hash = Animator.StringToHash("Base Layer."+anim_name);
			Asi();
			Clip();
			if (_clip != null) _wrap_mode = _clip.wrapMode;
		}
		AnimatorStateInfo asiC {get{return animator.GetCurrentAnimatorStateInfo(0);}}
		AnimatorStateInfo asiN {get{return animator.GetNextAnimatorStateInfo(0);}}	
#if UNITY_5
		AnimatorStateInfo Asi() {
			var tmp_asi = asiC;
			asi = (tmp_asi.fullPathHash == _hash) ? tmp_asi : asiN;
			return asi;
		}
		AnimationClip GetClip(AnimatorClipInfo[] ai) {return (ai.Length == 0) ? null : ai[0].clip;}
		AnimationClip clipC {get{return GetClip(animator.GetCurrentAnimatorClipInfo(0));}}
		AnimationClip clipN {get{return GetClip(animator.GetNextAnimatorClipInfo(0));}}
#else
		AnimatorStateInfo Asi()
		{
			var tmp_asi = asiC;
			asi = (tmp_asi.nameHash == _hash) ? tmp_asi : asiN;
			return asi;
		}
		AnimationClip GetClip(AnimationInfo[] ai) { return (ai.Length == 0) ? null : ai[0].clip; }
		AnimationClip clipC { get { return GetClip(animator.GetCurrentAnimationClipState(0)); } }
		AnimationClip clipN { get { return GetClip(animator.GetNextAnimationClipState(0)); } }
#endif
		
		AnimationClip Clip() {
			var tmp_clip = clipC;
			_clip = ((tmp_clip != null) && (tmp_clip.name == anim_name)) ? tmp_clip : clipN;
			return _clip;
		}
		public override float length {get{return asi.length;}}
		public override float speed {get{return animator.speed;} set{animator.speed = value;}}
		public override float normalizedSpeed {get{return speed / length;} set{speed = value * length;}}
		public override float time {get{return normalizedTime * length;} set{normalizedTime = value / length;}}

#if UNITY_5
		public override float normalizedTime {get{return Asi().normalizedTime;} set{animator.Play(asi.fullPathHash, 0, value);}}
#else
		public override float normalizedTime { get { return Asi().normalizedTime; } set { animator.Play(asi.nameHash, 0, value); } }
#endif

		public override WrapMode wrapMode {
			get{return (clip == null) ? _wrap_mode : clip.wrapMode;}
			set{
				if (clip == null) {_wrap_mode = value; return;}
				float anim_time = WrapAnimTime(); clip.wrapMode = value; time = anim_time;
			}}
		public override AnimationClip clip {get{return _clip;}}
		public override Transform transform {get{return animator.transform;}}
		public override GameObject gameObject {get{return animator.gameObject;}}
		public override bool enabled {get{return animator.enabled;} set{animator.enabled = value;}}
		public override bool isPlaying {get{
				Asi();
				if (!asi.IsName(anim_name)) return false; // Doesn't become False when animation ends
				if (speed >= 0) return (asi.normalizedTime < 1);
				return (asi.normalizedTime > 0);
			}}
		public override void Play(float anim_speed, bool speed_is_time) {
			try {
				animator.Play(anim_name, 0, 0f);
				animator.Update(0f); // nesessary to get anim info
			} catch {
				if (Debug.isDebugBuild) Debug.LogError(
					"Animation state '"+anim_speed+"' not found (object: '"+animator.name+"')");
				return;
			}
			
			Asi();
			Clip();
			
			wrapMode = _wrap_mode;
			
			if (speed_is_time) anim_speed *= length;
			
			speed = anim_speed;
			if (anim_speed < 0) time = length;
		}
		public override void Stop() {
			animator.speed = 0;
		}
		public override void Update() {
			animator.Update(0f);
		}
	}
}