﻿using UnityEngine;
using System.Runtime.InteropServices;

/// <summary>
/// Вызов натийных функций из UnityScript
/// </summary>
static public class SysUtilsProxy 
{

#if UNITY_IOS || UNITY_TVOS

	[DllImport("__Internal")]
	private static extern System.Int64 _GetFreeDiskspace();

    [DllImport("__Internal")]
	private static extern bool _ValidateUrlApp(string urlApp);

    [DllImport ("__Internal")]
    private static extern void iOSAudio_setupAudioSession();
	
#endif

    /// <summary>
    /// ДОСТУПНОЕ СВОБОДНОЕ МЕСТО НА УСТРОЙСТВЕ
    /// </summary>
    public static System.Int64 FreeDiskSpace
	{
		get
		{
			System.Int64 freeDiskSpace = 100*1024*1024;//STUB 100MB;
#if UNITY_EDITOR
			return freeDiskSpace;
#else
#if UNITY_IOS
			freeDiskSpace = _GetFreeDiskspace();
#elif UNITY_ANDROID
			AndroidJavaObject statFs = new AndroidJavaObject("android.os.StatFs", Application.persistentDataPath);
			freeDiskSpace = (System.Int64)statFs.Call<int>("getBlockSize") * (System.Int64)statFs.Call<int>("getAvailableBlocks");
#endif

#if MAGE_DEBUG
			Debug.Log ("SysUtilsProxy.FreeDiskSpace: " + freeDiskSpace);
#endif
			return freeDiskSpace;
#endif
        }
	}

#if UNITY_ANDROID
	private static AndroidJavaClass _unityPlayer = null;
	private static AndroidJavaObject _currentActivity = null;
	private static AndroidJavaObject _packageManager = null;
	/// <summary>
	/// инициализация экземпляра PackageManager для вызова других приложений на Android устройствах
	/// </summary>
	/// <returns></returns>
	private static bool __InitAndroidPackageManager()
	{
		if (_packageManager != null) return true;
		
		if (_unityPlayer == null) _unityPlayer = new AndroidJavaClass("com.unity3d.player.UnityPlayer");
		if (_unityPlayer == null) return false;
		if (_currentActivity == null) _currentActivity = _unityPlayer.GetStatic<AndroidJavaObject>("currentActivity");
		if (_currentActivity == null) return false;
		if (_packageManager == null) _packageManager = _currentActivity.Call<AndroidJavaObject>("getPackageManager");
		return (_packageManager != null);
	}
#endif

    /// <summary>
    /// проверка установлено ли приложение
    /// </summary>
    /// <param name="urlApp"></param>
    /// <returns></returns>
    public static bool AppIsInstalled(string urlApp)
    {
        bool isInstalled = false;
        // SystemInfo.operatingSystem returns something like iPhone OS 6.1
        //float osVersion = -1f;
        //string versionString = SystemInfo.operatingSystem.Replace("iPhone OS ", "");
        //float.TryParse(versionString.Substring(0, 1), out osVersion);
#if !UNITY_EDITOR
#if UNITY_IOS
        // REGRESION FOR iOS 9+ - not correctly work!
		isInstalled =  _ValidateUrlApp ( urlApp);
        //
#elif UNITY_ANDROID
		if ( _packageManager == null )
		{
			if ( !__InitAndroidPackageManager() ) return false;
		}	
		try
		{			
			using (AndroidJavaObject launchIntent = _packageManager.Call<AndroidJavaObject>("getLaunchIntentForPackage", urlApp))
			{
				isInstalled = (launchIntent != null);
			}
		}
		catch
		{
			isInstalled = false;
		}
#elif UNITY_METRO
        Debug.LogError("AppIsInstalled: not implementation to ".AddTime() + Application.platform);
#endif
#endif
        return isInstalled;
	}

    /// <summary>
    /// запуск приложения
    /// </summary>
    /// <param name="urlApp"></param>
    /// <param name="checkIsInstalled"></param>
    public static void OpenApp(string urlApp, bool checkIsInstalled = true)
    {	
		
#if UNITY_ANDROID && !UNITY_EDITOR

		if ( !__InitAndroidPackageManager() ) return;
		try
		{	
			//-------------------------------------------
			//FLAG_ACTIVITY_NEW_TASK			0x10000000
			//FLAG_ACTIVITY_CLEAR_TOP			0x04000000			
			//FLAG_ACTIVITY_RESET_TASK_IF_NEEDE 0x00200000
			//FLAG_ACTIVITY_CLEAR_TASK			0x00008000
			//-------------------------------------------

			using (AndroidJavaObject launchIntent = _packageManager.Call<AndroidJavaObject>("getLaunchIntentForPackage", urlApp))
			{
				try
				{
					var newLaunchIntent = launchIntent.Call<AndroidJavaObject>("addFlags", 0x14208000);
					_currentActivity.Call("startActivity", newLaunchIntent);
				}
				catch
				{
					_currentActivity.Call("startActivity", launchIntent);
				}
			}	
		}
		catch { }
#else
		if ( checkIsInstalled && AppIsInstalled(urlApp) ) Application.OpenURL(urlApp);		
		else Application.OpenURL(urlApp);
#endif
    }

    private static int? _apiLevel = null;
    /// <summary>
    /// API Level (SDK version )
    /// </summary>
	public static int apiLevel      
	{
        get
        {
            if(!_apiLevel.HasValue)
            {
#if UNITY_ANDROID && !UNITY_EDITOR
                using (var version = new AndroidJavaClass("android.os.Build$VERSION"))
                {
                   _apiLevel = version.GetStatic<int>("SDK_INT");
                }
#else                
                _apiLevel = 0;
#endif
            }
            return _apiLevel.Value;
        }
	}

    private static float? _screenSize = null;
    /// <summary>
    /// Screen Size inch 
    /// </summary>
    public static float screenSize
    {
        get
        {
            if (!_screenSize.HasValue)
            {               
                float w = (float)Screen.width;
                float h = (float)Screen.height;
                _screenSize = Mathf.Sqrt((w * w) + (h * h)) / dpi;
            }
            return _screenSize.Value;
        }
    }

    private static float? _dpi = null;
    /// <summary>
    /// The exact physical pixels per inch of the screen
    /// </summary>    
    public static float dpi
    {
        get
        {
            if (!_dpi.HasValue) CalcDisplayMetrics();
            return _dpi.Value;
        }
    }

    private static void CalcDisplayMetrics()
    {
        // WARNING - NOT FOR INDIA & TV  
#if UNITY_ANDROID && !UNITY_EDITOR
        if (__InitAndroidPackageManager())
        {
            //... which is pretty much equivalent to the code on this page:
            //http://developer.android.com/reference/android/util/DisplayMetrics.html
            using
            (
                AndroidJavaObject metricsInstance = new AndroidJavaObject("android.util.DisplayMetrics"),             
                windowManagerInstance = _currentActivity.Call<AndroidJavaObject>("getWindowManager"),
                displayInstance = windowManagerInstance.Call<AndroidJavaObject>("getDefaultDisplay")
            )
            {
                displayInstance.Call("getMetrics", metricsInstance);
                _dpi = 0.5f * (metricsInstance.Get<float>("xdpi") + metricsInstance.Get<float>("ydpi"));
                if (_dpi.Value < 1.0f) _dpi = 160f;
            }
        }
#else
        _dpi = Screen.dpi;
        if (_dpi.Value < 160f) _dpi = 160f;
#endif

    }

    public static void SetupAudioSession()
    {
#if !UNITY_EDITOR && (UNITY_IOS || UNITY_TVOS)
        iOSAudio_setupAudioSession();
#endif
    }
}
