﻿using UnityEngine;
using System.Runtime.InteropServices;

public static class BundleInfoBindings 
{

#if UNITY_IOS || UNITY_TVOS
    [DllImport("__Internal")]
    private static extern string _GetCFBundleVersion();

	[DllImport("__Internal")]
    private static extern string _GetCFBundleDisplayName();

    [DllImport("__Internal")]
    private static extern string _GetCFBundleIdentifier();
#endif

    private static string  GetBundleIdentifier()
    {
#if UNITY_5
        return Application.bundleIdentifier;
#else
        string bundleId = string.Empty;
#if UNITY_EDITOR
        bundleId = UnityEditor.PlayerSettings.bundleIdentifier;
#elif UNITY_IOS || UNITY_TVOS
        bundleId = _GetCFBundleIdentifier();
#elif UNITY_ANDROID
        AndroidJavaClass jc = new AndroidJavaClass("com.unity3d.player.UnityPlayer");
	    AndroidJavaObject jo = jc.GetStatic<AndroidJavaObject>("currentActivity");	
	    string strBundleID = jo.Call<string>("getPackageName");
		displayName = strBundleID;
#endif
       return bundleId;
#endif
    }

    private static string GetVersionInfo()
	{
#if UNITY_5
        return Application.version;
#else
        string ver = Application.unityVersion;
#if !UNITY_EDITOR		
#if UNITY_IOS || UNITY_TVOS
		ver = _GetCFBundleVersion();       
#endif
#endif
        return ver;
#endif
    }

    private static string GetDisplayNameInfo()
	{
#if UNITY_5
        string displayName = Application.productName;
#else
        string displayName = UnityEngine.SceneManagement.SceneManager.GetActiveScene().name;
#endif

#if !UNITY_EDITOR 

#if UNITY_IOS || UNITY_TVOS
        displayName = _GetCFBundleDisplayName();
#elif UNITY_ANDROID
#if !UNITY_5
         //WARNING RETURN PACKAGE NAME (BUNDLE IDENTIFIER)!!!
        AndroidJavaClass jc = new AndroidJavaClass("com.unity3d.player.UnityPlayer");
        AndroidJavaObject jo = jc.GetStatic<AndroidJavaObject>("currentActivity");
        string strBundleID = jo.Call<string>("getPackageName");
        displayName = strBundleID;       
#endif
#endif
#endif
        return displayName;

    }

    private static string _bundleIdentifier;
    public static string bundleIdentifier
    {
        get
        {
            if (string.IsNullOrEmpty(_bundleIdentifier))
            {
                _bundleIdentifier = GetBundleIdentifier();
            }
            return _bundleIdentifier;
        }
    }

	private static string _bundleVersion;
	public static string bundleVersion
	{
		get
		{
			if (string.IsNullOrEmpty(_bundleVersion))
			{
                _bundleVersion = GetVersionInfo();
			}
			return _bundleVersion;
		}
	}

	private  static string _bundleDisplayName;
	public static string bundleDisplayName
	{
		get
		{
			if (string.IsNullOrEmpty(_bundleDisplayName))
			{
                _bundleDisplayName = GetDisplayNameInfo();
			}
			return _bundleDisplayName;
		}
	}

	public static string bundleProductName
	{
		get
		{
			return bundleDisplayName + @" " + bundleVersion;
		}
	}

	
    
}