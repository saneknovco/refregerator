﻿using UnityEngine;
using System;
using System.IO;

#if MAGE_FILE_LOG
public class MageFileLogHandler : ILogHandler
{

    private FileStream m_FileStream;
    private StreamWriter m_StreamWriter;
    private ILogHandler m_DefaultLogHandler = Debug.logger.logHandler;

    public MageFileLogHandler()
    {
        string filePath = Application.persistentDataPath + "/AppLogs.txt";
        if(File.Exists(filePath)) m_FileStream = new FileStream(filePath, FileMode.Append, FileAccess.Write);
        else m_FileStream = new FileStream(filePath, FileMode.CreateNew, FileAccess.Write);
        m_StreamWriter = new StreamWriter(m_FileStream);
        // Replace the default debug log handler
        Debug.logger.logHandler = this;
    }

    public void LogFormat(LogType logType, UnityEngine.Object context, string format, params object[] args)
    {
        m_StreamWriter.WriteLine(String.Format(format, args));
        m_StreamWriter.Flush();        
        m_DefaultLogHandler.LogFormat(logType, context, format, args);
    }

    public void LogException(Exception exception, UnityEngine.Object context)
    {
        m_DefaultLogHandler.LogException(exception, context);
    }
}
#endif
