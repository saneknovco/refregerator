﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public static class MgUnityExtension
{
	#region GameObject

	/// <summary>
	/// поиск по имени объекта в дереве с корнем rootObject
	/// </summary>
	/// <param name="rootObject">родительский объект с которого начинается поиск по имени</param>
	/// <param name="nameObject"> имя объекта</param>
	/// <returns></returns>
	public static GameObject FindChild(this GameObject rootObject, string nameObject)
	{
		if (nameObject == rootObject.name) return rootObject;
		foreach (Transform child in rootObject.transform)
		{
			if (child != null)
			{
				var find = child.gameObject.FindChild(nameObject);
				if (find != null) return find;
			}
		}
		return null;
	}

	/// <summary>
	/// получить полное имя объекта - включаю всю иерархию объектов
	/// позволяет найти объекты с одинаковыми именами но в разной ветки иерархии.
	/// </summary>
	/// <param name="gameObject"></param>
	/// <returns></returns>
	public static string GetFullName(this GameObject gameObject)
	{		
		System.Text.StringBuilder sb = new System.Text.StringBuilder(gameObject.name);
		var parent = gameObject.transform.parent;
		while (parent != null)
		{
			sb.Insert(0, parent.name + @"/");
			parent = parent.parent;
		}
		return sb.ToString();
	}

    /// <summary>
    /// получить имя корневого объекта
    /// </summary>
    /// <param name="gameObject"></param>
    /// <returns></returns>
    public static string GetRootName (this GameObject gameObject)
    {
        string rootName = gameObject.name;
        var parent = gameObject.transform.parent;
        while (parent != null)
        {
            rootName = parent.name;
            parent = parent.parent;
        }
        return rootName;
    }

    /// <summary>
    /// получить корневой объект
    /// </summary>
    /// <param name="gameObject"></param>
    /// <returns></returns>
    public static GameObject GetRootInstance(this GameObject gameObject)
    {
        var rootGO = gameObject;
        var parent = gameObject.transform.parent;
        while (parent != null)
        {
            rootGO = parent.gameObject;
            parent = parent.parent;
        }
        return rootGO;
    }

    #endregion

    #region String (for Debug Log)

    /// <summary>
    /// служебная функция расширения сообщений
    /// используется в Debug.Log
    /// </summary>
    /// <param name="message"></param>
    /// <returns></returns>
    public static string AddTime(this string message)
	{
		System.DateTime dt = System.DateTime.Now;
		return string.Format("{0}:{1}:{2}.{3}: {4}", dt.Hour, dt.Minute, dt.Second, dt.Millisecond, message);
	}

	#endregion

	#region String for path

	public static string GetUnityStylePath(this string path)
	{
		return path.Replace(System.Convert.ToString(System.IO.Path.DirectorySeparatorChar),
					System.Convert.ToString(System.IO.Path.AltDirectorySeparatorChar));
	}

	#endregion

	#region Dictionary

	public static void SafeSet<TKey, TValue>(this Dictionary<TKey,TValue> dic, TKey key, TValue val)
    {
        if (dic.ContainsKey(key)) dic[key] = val;
        else dic.Add(key, val);
    }

    public static TValue SafeGet<TKey, TValue>(this Dictionary<TKey, TValue> dic, TKey key, TValue defVal)
    {        
		if (!dic.ContainsKey(key)) dic.Add(key, defVal);
        return dic[key];
    }

    public static TValue SafeGetByDefKey<TKey, TValue>(this Dictionary<TKey, TValue> dic, TKey key, TKey defKey)
    {
        if (dic.ContainsKey(key)) return dic[key];
        else return dic[defKey];
	}

	#endregion
}
