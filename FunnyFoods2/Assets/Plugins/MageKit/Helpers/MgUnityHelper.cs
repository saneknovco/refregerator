﻿using UnityEngine;
using System.Collections.Generic;

#if UNITY_EDITOR
using UnityEditor;
#endif

public class MgUnityHelper
{
    #region Build settings

    public static RuntimePlatform GetRuntimePlatform()
    {
#if UNITY_EDITOR        
        return  GetRuntimePlatformByActiveBuildTarget();
#else
        return Application.platform;
#endif
    }

#if UNITY_EDITOR
    public static BuildTargetGroup GetActiveBuildTargetGroup(BuildTargetGroup defaultTarget = BuildTargetGroup.iOS)
	{
		var activeTarget = EditorUserBuildSettings.activeBuildTarget;
		switch (activeTarget)
        {
            case BuildTarget.Android: return BuildTargetGroup.Android;
            case BuildTarget.iOS: return BuildTargetGroup.iOS;
            case BuildTarget.tvOS: return BuildTargetGroup.tvOS;
            //feature platforms
            case BuildTarget.WSAPlayer: return BuildTargetGroup.WSA;
            case BuildTarget.SamsungTV: return BuildTargetGroup.SamsungTV;
            // standalone desctop platform
            case BuildTarget.StandaloneWindows:
            case BuildTarget.StandaloneWindows64:
            case BuildTarget.StandaloneOSXIntel:
            case BuildTarget.StandaloneOSXIntel64:
            case BuildTarget.StandaloneOSXUniversal: return BuildTargetGroup.Standalone;
            
            default: return defaultTarget;
        }
    }
	
	public static RuntimePlatform GetRuntimePlatformByActiveBuildTarget()
	{
		var activeTarget = EditorUserBuildSettings.activeBuildTarget;		
		switch (activeTarget)
		{
			case BuildTarget.Android: return RuntimePlatform.Android;
            case BuildTarget.iOS: return RuntimePlatform.IPhonePlayer;
            case BuildTarget.tvOS: return RuntimePlatform.tvOS;

            // featuring platforms:
            case BuildTarget.SamsungTV: return RuntimePlatform.SamsungTVPlayer;
            case BuildTarget.Tizen: return RuntimePlatform.TizenPlayer;           
            case BuildTarget.WSAPlayer: return RuntimePlatform.WSAPlayerX86;

            //default:
#if UNITY_EDITOR_OSX
			default: return RuntimePlatform.OSXEditor;
#elif UNITY_EDITOR_WIN            
            default: return RuntimePlatform.WindowsEditor;
#else
            default: return RuntimePlatform.WindowsEditor;
#endif
        }
    }

	public static void AddScriptingDefineSymbols(string defineSymbol, BuildTargetGroup targetGroup)
	{
		var defines = PlayerSettings.GetScriptingDefineSymbolsForGroup(targetGroup);
        List<string> defKeys = new List<string>(defines.Split(';'));      
        if (!defKeys.Exists(el => el == defineSymbol))
		{
			defines = defines + ";" + defineSymbol;
			PlayerSettings.SetScriptingDefineSymbolsForGroup(targetGroup, defines);
		}
        defKeys.Clear();
    }

    public static void RemoveScriptingDefineSymbols(string defineSymbol, BuildTargetGroup targetGroup)
    {
        var defines = PlayerSettings.GetScriptingDefineSymbolsForGroup(targetGroup);
        List<string> defKeys = new List<string>(defines.Split(';'));
        int idx = defKeys.FindIndex(el => el == defineSymbol);
        if(idx>=0)
        {
            System.Text.StringBuilder builder = new System.Text.StringBuilder();
            for (int i = 0; i < idx; i++) builder.Append(defKeys[i]).Append(";");
            for(int i = idx+1; i< defKeys.Count; i++) builder.Append(defKeys[i]).Append(";");
            defines = builder.ToString().TrimEnd(';');            
            PlayerSettings.SetScriptingDefineSymbolsForGroup(targetGroup, defines);            
        }    
        defKeys.Clear();
    }

    public static void UpdateScriptingDefineSymbols( string defineSymbol, bool addDefine )
	{
		if(addDefine)
		{
			AddScriptingDefineSymbols(defineSymbol, GetActiveBuildTargetGroup());
		}
		else
		{
			RemoveScriptingDefineSymbols(defineSymbol, GetActiveBuildTargetGroup());
		}
	}

	public static void AddScriptingDefineSymbols(string defineSymbol)
	{
		AddScriptingDefineSymbols(defineSymbol, GetActiveBuildTargetGroup());
	}

	public static void RemoveScriptingDefineSymbols(string defineSymbol)
	{
		RemoveScriptingDefineSymbols(defineSymbol, GetActiveBuildTargetGroup());
	}
#endif   

    #endregion

    #region Resources & DLC
    public static T CreateFromResources<T>(string relativePath, string assetName, string assetExtension = ".asset") where T : ScriptableObject
    {
        T instance = Resources.Load(relativePath + assetName) as T;
        if (instance == null)
        {
            // If not found, autocreate the asset object.
            instance = ScriptableObject.CreateInstance<T>();
#if UNITY_EDITOR
            string properPath = System.IO.Path.Combine(Application.dataPath, @"Resources").GetUnityStylePath();

            properPath = System.IO.Path.Combine(properPath, relativePath).GetUnityStylePath();
            if (!System.IO.Directory.Exists(properPath))
            {

                Debug.LogWarning("CreateFromResources<" + typeof(T).Name + ">: Create Folder: " + properPath);
                System.IO.Directory.CreateDirectory(properPath);
                UnityEditor.AssetDatabase.Refresh(UnityEditor.ImportAssetOptions.Default);
            }
            string assetPath = System.IO.Path.Combine(@"Assets/Resources", relativePath).GetUnityStylePath();
            string uriAsset = System.IO.Path.Combine(assetPath,
                                           assetName + assetExtension).GetUnityStylePath();

            Debug.LogWarning("CreateFromResources<" + typeof(T).Name + ">: " + uriAsset + " is Auto Create");
            UnityEditor.AssetDatabase.CreateAsset(instance, uriAsset);
#endif
        }
        return instance;
    }
#endregion

}
