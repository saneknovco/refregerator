﻿using UnityEngine;
using System.Collections;

public static class MgCoroutineHelper
{
    /// <summary>
    /// 
    /// </summary>
    /// <param name="time"></param>
    /// <param name="callback"></param>
    /// <returns></returns>
    /// <example> 
    ///  StartCoroutine(waitThenCallback(5, () => 
    ///     { Debug.Log("Five seconds have passed!"); }));
    /// </example>
    static public  IEnumerator WaitThenCallback(float time, System.Action callback)
    {
        yield return new WaitForSeconds(time);
        if (callback != null) callback();
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="frame"></param>
    /// <param name="callback"></param>
    /// <returns></returns>
    static public IEnumerator WaitThenCallback(int frame, System.Action callback)
    {
        for (int i = 0; i < frame; i++) yield return null;
        if(callback != null) callback();
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="callback"></param>
    /// <returns></returns>
    static public IEnumerator WaitEndOfFrameThenCallback(System.Action callback)
    {
        yield return new WaitForEndOfFrame();
        if (callback != null) callback();
    }
}
