﻿using UnityEngine;
using System.Collections;

namespace MageKit.Anim
{
    [ExecuteInEditMode]
    [DisallowMultipleComponent]
    public class AlphaController : MonoBehaviour
    {
        [Range(0, 1)]
        public float alpha;


        float _lastAlpha = -1.0f;
        Material _mat;
        Color _color;

        void Awake()
        {
            var go = GetComponent<Renderer>();
            if (go != null)
#if UNITY_EDITOR
            _mat = go.sharedMaterial;
#else
			_mat = go.material;
#endif
        }

        void LateUpdate()
        {
            if (_mat == null) return;

            if (alpha != _lastAlpha)
            {
                _color = _mat.color;
                _color.a = alpha;
                _mat.color = _color;
                _lastAlpha = alpha;
            }
        }
    }
}

