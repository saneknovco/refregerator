﻿using UnityEngine;
using System.Collections;

namespace Bini.Anim
{
	[AddComponentMenu("Bini/AnimReplaceTextures")]
	[RequireComponent(typeof(MeshFilter))]
	[RequireComponent(typeof(MeshRenderer))]
	[ExecuteInEditMode]
	public class BnAnimReplaceTextures : MonoBehaviour
	{
		public float activeTextureIndex = -1;
		
		[SerializeField]
		public Texture[] textures = new Texture[0];

		[System.NonSerialized]
		private Texture _origTexture;

		private bool _firstReplace = true;
		private bool _rendererEnabled = true;

		private int _cached_activeIndex = -1;
		private int _restore_activeIndex = -1;

		#region Unity event functions

		void Awake()
		{
			_origTexture = null;
			var rnd = GetComponent<Renderer>();
			if(rnd != null)
#if UNITY_EDITOR
				if(rnd.sharedMaterial != null)
					_origTexture = rnd.sharedMaterial.mainTexture;
#else
			if(rnd.material != null)	
				_origTexture =  rnd.material.mainTexture;			
#endif
		}

		void OnEnable()
		{
			if (_origTexture != null)
			{
				if (_restore_activeIndex != -1)
					_ReplaceTexture(_restore_activeIndex);
			}
			
			if (Application.isPlaying) {
				Messenger.AddListener<int, GameObject>(@"AE_ReplaceTexture", AE_ReplaceTexture);
			}
		}
	
		// Use this for initialization
		void Start()
		{
			if ( _cached_activeIndex != (int)activeTextureIndex )
			{
				_cached_activeIndex = (int)activeTextureIndex;
				ReplaceTexture(_cached_activeIndex);
			}
		}		

		void Update()
		{
			if ( _cached_activeIndex != (int)activeTextureIndex )
				_ReplaceTexture((int)activeTextureIndex);
		}		

		void OnDisable()
		{
			if (_origTexture != null)
			{
				_restore_activeIndex = _cached_activeIndex;
				_ReplaceTexture(-1);
			}
		}

#if UNITY_EDITOR
        void OnDestroy()
        {
            if (Application.isPlaying)
            {
                if (_origTexture != null)
                    _ResetTexture();           
            }
        }
#endif

        #endregion

        private void _ResetTexture()
        {
            if (_origTexture == null) return;
            Material mat = null;
            var rnd = GetComponent<Renderer>();
#if UNITY_EDITOR            
            if (rnd == null)
            {
                Debug.LogError((this.name + @" ReplaceTexture say: internal error: not access to renderer!").AddTime());
                return;
            }
            mat = rnd.sharedMaterial;
#else
			if(rnd == null) return ;
			mat = rnd.material;
#endif            
            if(mat != null)	mat.mainTexture = _origTexture;

        }

		private bool _ReplaceTexture(int index)
		{
			if (!this.enabled) return false;

			if (_cached_activeIndex == index) return false;

			_cached_activeIndex = index;
			Material mat = null;
			var rnd = GetComponent<Renderer>();
#if UNITY_EDITOR
			//Debug.Log((name + " BnReplaceTextures:SetTexture: " + index).AddTime());
			if(rnd == null)
			{
				Debug.LogError((this.name + @" ReplaceTexture say: internal error: not access to renderer!").AddTime());
				return false;
			}
			mat = rnd.sharedMaterial;			
#else
			if(rnd == null) return false;
			mat = rnd.material;
#endif
			if (mat == null)
			{
				Debug.LogError((this.name + @" ReplaceTexture say: internal error: not access to material!").AddTime());
				return false;
			}
			rnd.enabled = true;

			if ( (textures != null) && (index <= -1 || index >= textures.Length) )
			{
				if( _origTexture != null) mat.mainTexture = _origTexture;
				_firstReplace = true;
				rnd.enabled = _rendererEnabled;
			}
			else
			{
				if (_firstReplace)
				{
					_firstReplace = false;					
					_rendererEnabled = rnd.enabled;
				}				
				rnd.enabled = true;
				if(textures[index] != null ) mat.mainTexture = textures[index];
			}
			return true;
		}

		public bool ReplaceTexture(int index)
		{
			if (!this.enabled) return false;
			activeTextureIndex = index;
			return _ReplaceTexture(index);
		}

		void AE_ReplaceTexture(int index, GameObject obj) {
			if (obj != this.gameObject) return;
			ReplaceTexture(index);
		}
	}
}
