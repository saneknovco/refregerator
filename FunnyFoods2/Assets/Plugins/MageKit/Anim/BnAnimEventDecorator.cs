﻿using UnityEngine;
using System.Collections;

namespace Bini.Anim
{
	[AddComponentMenu("Bini/AnimEventDecorator")]
	//[RequireComponent(typeof(Animation))]
	public class BnAnimEventDecorator : MonoBehaviour {

		public bool isTrace = false;
		
		private Animation _anim;

		void Awake()
		{
			_anim = GetComponent<Animation>();
		}
		
		GameObject FindObject(string nameObj, GameObject obj)
		{			
			if(nameObj == obj.name)
				return obj;				
			foreach (Transform child in obj.transform) 
			{
				if(child!=null)
				{
					var find = FindObject(nameObj, child.gameObject );
					if(find != null)
						return find;
				}
			}				
			return null;
		}

		void ReplaceTexture(AnimationEvent animEvent)
		{
			//if (!this.enabled) return;

			var fobj = FindObject( animEvent.stringParameter,_anim.transform.gameObject);
			if(fobj)
			{							
				Messenger.Broadcast<int, GameObject>(@"AE_ReplaceTexture", animEvent.intParameter, fobj);
				/*
				BnAnimReplaceTextures bhv = fobj.GetComponent<BnAnimReplaceTextures>();
				if (bhv)
				{
					#if UNITY_EDITOR
					if (!bhv.ReplaceTexture(animEvent.intParameter))
					{
						if (isTrace) 
							Debug.LogWarning(System.DateTime.Now + " " + 
								fobj.name + " say: sprite corrupted! index texture: " + animEvent.intParameter);
					}
					#else
					bhv.ReplaceTexture(animEvent.intParameter);	
					#endif
				}				
				*/
			}
		}

		void TransparentEffect(AnimationEvent animEvent)
		{	
		
			if(isTrace) 
				Debug.Log ((@"AE_TransparentEffect: object: " + animEvent.stringParameter + 
				" alpha: " + animEvent.floatParameter + " framecount: " +  animEvent.intParameter).AddTime());

			Messenger.Broadcast<AnimationEvent>(@"AE_TransparentEffect", animEvent);
		}

	}
}
