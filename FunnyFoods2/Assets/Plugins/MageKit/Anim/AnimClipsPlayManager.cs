﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;


namespace Mage.Anim
{
	[AddComponentMenu("Mage/AnimClipsPlayManager")]
	public class AnimClipsPlayManager : MonoBehaviour
	{
		private const float FRAME_PER_SECOND = 60.0f;

		#region services classes for AnimClipsStartManager

		[System.Serializable]
		public class AnimationClipItem
		{
			/// <summary>
			/// анимационный клип
			/// </summary>
			public AnimationClip clip;
			/// <summary>
			/// 
			/// </summary>
			public float frameStart;
			/// <summary>
			/// скорость воспроизведения
			/// </summary>
			public float speed = 1.0f;
			/// <summary>
			/// количество повторов
			/// </summary>
			public int repeat = 1;

			[System.NonSerialized]
			[HideInInspector]
			public Animation animation;
		
			/// <summary>
			/// флаг игры клипа для повторов
			/// </summary>
			[System.NonSerialized]
			[HideInInspector]
			public bool playStart=false;

			/// <summary>
			/// счетчик повторов
			/// </summary>
			[System.NonSerialized]
			[HideInInspector]
			public int playCounter = 1;
			
		}

		#endregion

		public bool playIncludeInactive = true;
		public bool playAutomatic = true;
		public bool useScaleSpeed = false;
		public float scaleSpeed =1.0f;

		public AnimationClipItem[] animClipItems = new AnimationClipItem[1];

		private bool __isPlayMode = false;

		private List<AnimationClipItem> _repeatClipItems = new List<AnimationClipItem>();				
		private List<AnimationClipItem> __tmpItems = new List<AnimationClipItem>();

		private IEnumerator StartPlayAnimClip(AnimationClipItem animClipItem)
		{		
			if (useScaleSpeed)
			{
				yield return new WaitForSeconds(animClipItem.frameStart / (FRAME_PER_SECOND * Mathf.Abs(scaleSpeed)) );
				animClipItem.animation[animClipItem.clip.name].speed = scaleSpeed * animClipItem.speed;
			}
			else
			{
				yield return new WaitForSeconds(animClipItem.frameStart / (FRAME_PER_SECOND));
				animClipItem.animation[animClipItem.clip.name].speed = animClipItem.speed;
			}

#if UNITY_EDITOR
			Debug.Log(("Start play animation at " + animClipItem.animation.gameObject.name +
				" animation clip" + animClipItem.clip.name +
				" absolutely time start: " + animClipItem.frameStart / FRAME_PER_SECOND +
				" scale time start:" + animClipItem.frameStart / FRAME_PER_SECOND * Mathf.Abs(scaleSpeed)).AddTime()
			);
#endif

			animClipItem.playStart = true;
			animClipItem.playCounter = 1;
			animClipItem.animation.Play(animClipItem.clip.name);
			if(animClipItem.repeat > 1)
				_repeatClipItems.Add(animClipItem);
			yield break;
		}

		public void StartPlayAnim()
		{
			if (__isPlayMode) return;
			__isPlayMode = true;
			_repeatClipItems.Clear();
			foreach (var item in animClipItems)
			{
				if (item != null && item.clip != null && item.animation != null)
					StartCoroutine(StartPlayAnimClip(item));				
			}
		}
		
		public float GetTotalAnimationLength()
		{
			float res = 0;
			foreach (var item in animClipItems)
			{
				if(item!=null && item.clip!= null && item.animation!= null)
				{
					float finish = useScaleSpeed ? (item.frameStart / FRAME_PER_SECOND + item.clip.length)/Mathf.Abs(scaleSpeed):
						item.frameStart / FRAME_PER_SECOND + item.clip.length;
					if(res<finish) res = finish;
				}
			}	
			return res;
		}
		
		public void StopAllAnimations()
		{
			foreach (var item in animClipItems)
			{
				if(item!=null && item.clip!= null && item.animation!= null)
				{
					item.playStart = false;
					item.playCounter = 1;
					item.animation.Stop();
				}
			}
		}
		
		public void ForbidPlayMode()
		{
			__isPlayMode = true;
		}
		
		public void ResetPlayMode()
		{
			__isPlayMode = false;
		}

		#region UNITY EVENT

		void Awake()
		{
			Animation[] animations;
			if (playIncludeInactive)
				animations = Resources.FindObjectsOfTypeAll(typeof(Animation)) as Animation[];
			else
				animations = MonoBehaviour.FindObjectsOfType(typeof(Animation)) as Animation[];
			
			if (animations != null && animClipItems != null)
			{
				foreach (var item in animations)
				{
					foreach (AnimationState animState in item)
					{
						foreach (var clipItem in animClipItems)
						{
							if ((clipItem.clip != null) && (clipItem.clip.name == animState.clip.name))
								clipItem.animation = item;
						}
					}
				}
			}
		}

		// Use this for initialization
		void Start()
		{
#if UNITY_EDITOR
			Debug.Log("AnimClipsPlayManager: start: config anim:".AddTime());
			foreach (var item in animClipItems)
			{
				if (item.animation)
					Debug.Log((item.animation.name + ": " + item.clip.name + ": " + item.frameStart).AddTime());
			}
#endif	
			if (playAutomatic)
				StartPlayAnim();
		}

	
		void LateUpdate()
		{
			if (__isPlayMode == false) return;
			foreach (var item in _repeatClipItems)
			{
				if (item.playStart && !item.animation.IsPlaying(item.clip.name))
				{
					item.animation.Play(item.clip.name);
					item.playCounter++;
					if (item.playCounter >= item.repeat)
						__tmpItems.Add(item);
				}
			}
			foreach (var item in __tmpItems)
				_repeatClipItems.Remove(item);
			__tmpItems.Clear();
		}

		#endregion

	}
}
