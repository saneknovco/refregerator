﻿using UnityEngine;
using System;

public class MgGameObjectUid : MonoBehaviour {

	[HideInInspector]
	public string  uid = Guid.NewGuid().ToString();

	void Reset()
	{
		if (GetComponents<MgGameObjectUid>().Length > 1)
		{
			Invoke("DestroyThis", 0);
		}
	}

	void DestroyThis()
	{
		DestroyImmediate(this);
	}
}
