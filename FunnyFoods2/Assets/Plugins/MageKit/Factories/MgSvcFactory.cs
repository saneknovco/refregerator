﻿using System;

/// <summary>
/// Light Generic Factory  для создание единичного экзеспляра сервиса
/// ПОДДЕРЖИВАЕТ ТОЛЬКО ОДНУ РЕАЛИЗАЦИЮ СЕРВИСА
/// </summary>
/// <typeparam name="I"> interface of the service </typeparam>
public class MgSvcFactory<I> where I: class
{
	/// <summary>
	/// интерфейс сервиса
	/// </summary>	
	private static I _instance;
	/// <summary>
	/// класс реализующий интерфейс сервиса
	/// </summary>
	private static Type _type;

	/// <summary>
	/// Экземпляр сервися реализующего интерфейс I
	/// </summary>
	public static I instance
	{
		get
		{
			if (_instance == null) _instance = (I)Activator.CreateInstance(_type);			
			return _instance;
		}
	}

	/// <summary>
	/// регистрация класса T реализующего интерфейс I
	/// </summary>
	/// <typeparam name="T"></typeparam>
	public static void RegisterClass<T> () where T:I, new() 
	{
		_type = typeof(T);
		_instance = null;
	}
}

