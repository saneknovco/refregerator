using UnityEngine;
using UnityEditor;
using System.Collections.Generic;
using System.Linq;
using System.IO;
using Mage.Helper;

namespace Mage.Utils
{
    public class MgTextureCompress : ScriptableWizard
	{
        //[System.Serializable]
        //public enum TargetPlatform
        //{
        //    Default,
        //    Web,
        //    Standalone,
        //    iPhone,
        //    Android
        //};
        enum ModeCompress
		{
			TrueColor = 0x1,
			Compress = 0x2
		};

        public string[] pathToTextures = _pathBuffer; //null;

        public BuildTarget platform = BuildTarget.iOS;
		public TextureImporterNPOTScale nPOTScale = TextureImporterNPOTScale.ToNearest;
		public TextureImporterFormat compressFormat = TextureImporterFormat.PVRTC_RGBA2; 

		public int textureMaxSize = 2048;
		public bool includePng = true;
		public bool includePsd = true;
		private  List<string> _textureFileMasks = null;

		private static ModeCompress _mode = ModeCompress.Compress;
		private static string[] _pathBuffer = null;
        
		private bool ValidatePathProcess(string[] path)
		{
			foreach (var item in path)
			{
				if(string.IsNullOrEmpty(item))return false;
				if (!System.IO.Directory.Exists(item))
				{
					EditorUtility.DisplayDialog("Compress Textures",
						string.Format("Path {0} not Exist!", item),"Ok");
					Debug.LogError(string.Format("MgTextureCompress.ValidatePathProcess: Path {0} not Exist!", item));
					return false;
				}
			}
			return true;
		}
		
		private void CompressTexture(string root, ModeCompress mode)
		{
			//based on http://msdn.microsoft.com/ru-ru/library/bb513869.aspx
			//public static void TraverseTree(string root)	
			if (!System.IO.Directory.Exists(root))
			{
				Debug.LogError(string.Format("Compress Textures: Path {0} not Exist!", root));
				return;
			}

			string pathTexture;
			Texture2D texture2D;
			List<string> files = new List<string>();
			Stack<string> dirs = new Stack<string>(100);
			dirs.Push(root);

			AssetDatabase.StartAssetEditing();
			while (dirs.Count > 0)
			{
				string currentDir = dirs.Pop();
				if (currentDir.Contains(Mage.Helper.MgEditorHelper.MATERIAL_FOLDER_NAME))
					continue;				
				string[] subDirs;
				subDirs = System.IO.Directory.GetDirectories(currentDir);
				files.Clear();
				foreach (var item in _textureFileMasks)
					files.AddRange(System.IO.Directory.GetFiles(currentDir, item, SearchOption.TopDirectoryOnly).ToList());

				foreach (string file in files)
				{
					System.IO.FileInfo fi = new System.IO.FileInfo(file);
					texture2D = AssetDatabase.LoadMainAssetAtPath(MgEditorHelper.GetDBAssetName(fi.FullName)) as Texture2D;
					Debug.Log("Texture load :" + texture2D.name);				
					if (texture2D)
					{
						pathTexture = AssetDatabase.GetAssetPath(texture2D);
						TextureImporter textureImporter = AssetImporter.GetAtPath(pathTexture) as TextureImporter;
						TextureImporterSettings st = new TextureImporterSettings();
						switch (mode)
						{
							case ModeCompress.TrueColor:
								{
									textureImporter.textureType = TextureImporterType.Advanced;
#if UNITY_IOS
#if UNITY_5
									textureImporter.SetPlatformTextureSettings(platform.ToString(), 4096,TextureImporterFormat.AutomaticTruecolor, 
								           (int)TextureCompressionQuality.Best, false);
#else
									textureImporter.SetPlatformTextureSettings(platform.ToString(), 4096,TextureImporterFormat.AutomaticTruecolor, 
								           (int)TextureCompressionQuality.Best);
#endif
#endif
									textureImporter.ReadTextureSettings(st);
									st.ApplyTextureType(TextureImporterType.Advanced, false);							
									st.npotScale = TextureImporterNPOTScale.None;
                                    st.textureFormat = TextureImporterFormat.AutomaticTruecolor;
                                    st.aniso = 3;//max 9
									st.wrapMode = TextureWrapMode.Clamp;
									st.filterMode = FilterMode.Trilinear;
									st.textureFormat = TextureImporterFormat.AutomaticTruecolor;
									st.maxTextureSize = 4096;									
								};
								break;
							case ModeCompress.Compress:
								{
									textureImporter.textureType = TextureImporterType.Advanced;
#if UNITY_5
									textureImporter.SetPlatformTextureSettings(platform.ToString(), textureMaxSize,
										compressFormat, (int)TextureCompressionQuality.Best,false);					
#else
									textureImporter.SetPlatformTextureSettings(platform.ToString(), textureMaxSize,
										compressFormat, (int)TextureCompressionQuality.Best);					
#endif
									textureImporter.ReadTextureSettings(st);

									st.ApplyTextureType(TextureImporterType.Advanced, false);
									st.npotScale = nPOTScale;
									st.textureFormat = compressFormat;									
									st.mipmapEnabled = false;
									st.aniso = 3;//max 9
									st.wrapMode = TextureWrapMode.Clamp;
									st.filterMode = FilterMode.Trilinear;
									st.maxTextureSize = textureMaxSize;
									st.compressionQuality = (int)TextureCompressionQuality.Best;	
								};
								break;
						}
						textureImporter.SetTextureSettings(st);
						AssetDatabase.ImportAsset(pathTexture);
					}
				}
				foreach (string str in subDirs)
					dirs.Push(str);
			}
			AssetDatabase.StopAssetEditing();
		}	

		void OnWizardCreate()
		{
			if(pathToTextures == null)
			{
				pathToTextures = new List<string>(_pathBuffer).ToArray();
			}
			else
			{
				if (pathToTextures != null && (pathToTextures.Length > 0))
				{
					_pathBuffer = new List<string>(pathToTextures).ToArray();

					if (!ValidatePathProcess(this.pathToTextures))
						return;
				}
				else
				{
                    _pathBuffer = null;
                    EditorUtility.DisplayDialog("Compress Textures: Path to Textures","The list is Empty!", "Ok");
					return;
				}
			}

			_textureFileMasks = new List<string>();
			if (includePng)
				_textureFileMasks.Add("*.png");
			if (includePsd)
				_textureFileMasks.Add("*.psd");

			foreach (var path in pathToTextures)
				CompressTexture(path, _mode);
		}

        static void DoCompress(ModeCompress mode, string title, string command)
        {
            //if(_pathBuffer == null)
            //{
            //	_pathBuffer = new string[2];
            //	_pathBuffer[0] = @"./Assets/Resources/Textures";
            //};
            _mode = mode;
            if (_pathBuffer == null || _pathBuffer.Length <1)
            {            
                _pathBuffer = new string[2]{
                    MgEditorHelper.GetAssetScenePath() + @"Textures",
                    MgEditorHelper.GetDBAssetPath() + @"Resources"
                };
            }
            ScriptableWizard.DisplayWizard<MgTextureCompress>(title, command);
        }

		[MenuItem("Mage/Utils/Compress textures...", priority = 20000)]
		static void DoCompressTexture()
		{           
            DoCompress(ModeCompress.Compress, "Compress textures for mobile!", "Compress");
		}

		[MenuItem("Mage/Utils/Uncompress textures (TrueColor)...", priority = 20001)]
		static void DoUncopressTexture()
		{
            DoCompress(ModeCompress.TrueColor,"Uncompress textures to true color! ","Uncompress");
		}
	}
}