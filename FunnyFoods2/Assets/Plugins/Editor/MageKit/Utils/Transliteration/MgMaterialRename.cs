using UnityEngine;
using UnityEditor;
using Mage.Helper;

namespace Mage.Utils
{
	public class MgMaterialRename : ScriptableWizard
	{
		public GameObject rootObject;
		public string pathRootObject = MgEditorHelper.GetSceneTexturesPath();

		private string assetPathMaterialObjects;

		void RecursiveRenameMaterial(GameObject src)
		{
			foreach (Transform child_transform in src.transform)
			{
				GameObject child = child_transform.gameObject;				
				string new_name;
				Material mat;
				if(child.GetComponent<Renderer>())
				{
					mat = child.GetComponent<Renderer>().material;
					if (mat != null)
					{											
						//Debug.Log("old2: " + old_name);
						new_name = Mage.Utils.Transliteration.Front(mat.name.Replace(" (Instance)", ""));
						new_name.Replace("(Instance)", "");
						if (new_name != mat.name)
						{
							mat.name = new_name;							
							AssetDatabase.CreateAsset(mat, assetPathMaterialObjects + new_name + ".mat");
							child.GetComponent<Renderer>().material = mat;
						}
					}
				}
				RecursiveRenameMaterial(child);
			}
		}		

		void OnWizardCreate()
		{
			if (!System.IO.Directory.Exists(this.pathRootObject))
			{
				EditorApplication.Beep();
				Debug.LogError("Path to Textures not Exist!");
				return;
			}

			assetPathMaterialObjects = MgEditorHelper.GetFolderMaterialName(this.pathRootObject);			
			if (!System.IO.Directory.Exists(assetPathMaterialObjects))
			{
				AssetDatabase.CreateFolder(MgEditorHelper.GetDBAssetName(this.pathRootObject),
					MgEditorHelper.MATERIAL_FOLDER_NAME);
			}
			Debug.Log(assetPathMaterialObjects);
			RecursiveRenameMaterial(rootObject);
		}

		//if you add more then 10 between two items, an Separator-Line is drawn before the menuitem
		[MenuItem("Mage/Utils/Translit/Rename Materials", priority = 30002)]
		static void DoTranslitRenameMaterial()
		{
			ScriptableWizard.DisplayWizard<MgMaterialRename>(
				"Rename Materials: Russian to Translit!!!",
				"Rename!");
		}
	}

}
