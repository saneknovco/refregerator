﻿using UnityEngine;
using UnityEditor;
using System.Collections.Generic;
using Mage.Helper;

namespace Mage.Utils
{
	public class MgTextureRename : ScriptableWizard
	{
        private static string _rootPath = string.Empty;
        public string rootPathToTextures = MgTextureRename._rootPath;

        private static void TraverseTree(string root)
		{
			// Data structure to hold names of subfolders to be
			// examined for files.
			Stack<string> dirs = new Stack<string>(100);
			dirs.Push(root);

			while (dirs.Count > 0)
			{
				string currentDir = dirs.Pop();
				string[] subDirs;
				string[] files = null;
				string asset_name;
				string new_name;
				string error;
				try
				{
					subDirs = System.IO.Directory.GetDirectories(currentDir);
					files = System.IO.Directory.GetFiles(currentDir);
				}
				catch (System.Exception e)
				{
					Debug.LogError("TraverseTree: " + e.Message);
					continue;
				}
				// Perform the required action on each file here.
				// Modify this block to perform your required task.
				foreach (string file in files)
				{
					try
					{
						System.IO.FileInfo fi = new System.IO.FileInfo(file);
						if (fi.Extension.ToUpper() == ".PSD" || fi.Extension.ToUpper() == ".PNG")
						{
							asset_name = MgEditorHelper.GetDBAssetName(fi.FullName);
							new_name = Transliteration.Front(asset_name);
							if (new_name != asset_name)
							{
								new_name = System.IO.Path.GetFileNameWithoutExtension(new_name);
								error = AssetDatabase.RenameAsset(asset_name, new_name);
								if (!string.IsNullOrEmpty(error))
								{
									Debug.LogError("MgFixName:TraverseTree say: rename error " + error +
										"name original texture: " + asset_name + " new name: " + new_name);
								}
								AssetDatabase.SaveAssets();
								AssetDatabase.Refresh(ImportAssetOptions.Default);
							}
						}
					}
					catch (System.IO.FileNotFoundException e)
					{
						// If file was deleted by a separate application
						//  or thread since the call to TraverseTree()
						// then just continue.
						Debug.LogError("MgFixName:TraverseTree say: " + e.Message);
						continue;
					}
				}
				// Push the subdirectories onto the stack for traversal.
				// This could also be done before handing the files.
				foreach (string str in subDirs)
					dirs.Push(str);
			}
		}

		void OnWizardCreate()
		{
            if (!System.IO.Directory.Exists(rootPathToTextures))
            {
                EditorApplication.Beep();
                Debug.LogError("Path: '" + rootPathToTextures + "'  not Exist!");
                return;
            }
            TraverseTree(MgEditorHelper.GetScenePath());
        }

		//if you add more then 10 between two items, an Separator-Line is drawn before the menuitem
		[MenuItem("Mage/Utils/Translit/Rename Textures", priority = 30001)]
		static void DoTranslitRenameTextures()
		{
            _rootPath = AssetDatabase.GetAssetPath(Selection.activeObject.GetInstanceID());           
            ScriptableWizard.DisplayWizard<MgTextureRename>(
				"WARNING: Rename Russian Texture into Translit!",
				"Rename!");
		}
	}
}
