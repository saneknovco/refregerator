
#if UNITY_EDITOR
using UnityEditor;
#endif
using System.IO;
using UnityEditor.SceneManagement;

namespace Mage.Helper
{
	public static class MgEditorHelper
	{
		#region CONSTANT

		public const string ASSET_FOLDER_NAME = @"Assets";
		public const string MATERIAL_FOLDER_NAME = @"Materials";
		public const string TEXTURE_FOLDER_NAME = @"Textures";
		public const string RESOURCES_FOLDER_NAME = @"Resources";

		public const string ROOT_ANIM_OBJECT_PREFIX = @"Anim";

		#endregion

#if UNITY_EDITOR
		public static string GetShortName(string fullName)
		{
			string correctPath = fullName.Replace(System.Convert.ToString(Path.AltDirectorySeparatorChar),
				System.Convert.ToString(Path.DirectorySeparatorChar));
			string[] path = correctPath.Split(Path.DirectorySeparatorChar);
			int lastIndex = path.Length - 1;
			return path[lastIndex];
		}

		public static string GetParentShortName(string fullName)
		{
			string correctPath = fullName.Replace(System.Convert.ToString(Path.AltDirectorySeparatorChar),
				System.Convert.ToString(Path.DirectorySeparatorChar));
			//Debug.Log(("GetParentShortName: " + correctPath).AddLogTime());
			string[] path = correctPath.Split(Path.DirectorySeparatorChar);
			int lastIndex = path.Length - 2;
			if (lastIndex >= 0)
				return path[lastIndex];
			else
				return string.Empty;
		}

		public static string GetSceneName()
		{
			return Path.GetFileNameWithoutExtension(EditorSceneManager.GetActiveScene().path);
		}

		public static string GetScenePath()
	    {    
			string path = Path.GetFullPath(EditorSceneManager.GetActiveScene().path);
			path = path.Replace(Path.GetFileName(EditorSceneManager.GetActiveScene().path), "");
			return path ;
	    }
		public static string GetSceneTexturesPath()
	    {    
			string path = Path.GetFullPath(EditorSceneManager.GetActiveScene().path);
			path = path.Replace(Path.GetFileName(EditorSceneManager.GetActiveScene().path), "");
			return path + MgEditorHelper.TEXTURE_FOLDER_NAME + System.Convert.ToString(Path.DirectorySeparatorChar);
	    }
		
		public static string GetFolderMaterialName(string uriTextures)
		{			

			return GetDBAssetName(uriTextures).Replace(
					System.Convert.ToString(Path.DirectorySeparatorChar),
					System.Convert.ToString(Path.AltDirectorySeparatorChar)) +  
					"/" + MgEditorHelper.MATERIAL_FOLDER_NAME + "/";
		}

		public static string GetDBAssetPath()
		{
			return  UnityEngine.Application.dataPath + "/";			
		}

        public static string GetAssetScenePath()
        {
            return GetScenePath().Replace(
                    System.Convert.ToString(Path.DirectorySeparatorChar),
                    System.Convert.ToString(Path.AltDirectorySeparatorChar));
        }

        public static string GetDBAssetName(string uriName)
		{
			int idx = uriName.IndexOf(MgEditorHelper.ASSET_FOLDER_NAME);
			if (idx > 0)
				return uriName.Substring(idx);
			else return string.Empty;
		}		

#endif
	}

}
