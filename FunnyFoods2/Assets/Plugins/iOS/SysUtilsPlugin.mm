
/////////////////////////////////////////////////////////////
//>==== поддержка обращения к приложения через custom url scheme ===
//http://idev.by/ios/20540/
// AppDelegate.h/ AppDelegate.mm
@interface UnityAppController
@end

@implementation UnityAppController (delegates)
-(BOOL) application:(UIApplication*)application handleOpenURL:(NSURL *)url
{
    if (!url) {  return NO; }
    
    NSString *URLString = [url absoluteString];
    [[NSUserDefaults standardUserDefaults] setObject:URLString forKey:@"url"];
    [[NSUserDefaults standardUserDefaults] synchronize];
    return YES;
}
@end
//==== поддержка обращения к приложения через custom url scheme <===
/////////////////////////////////////////////////////////////

////////////////////////////////////////////////////////////
// How to Replace the USER UNIQUE ID
//====http://oleb.net/blog/2011/09/how-to-replace-the-udid/
//- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions
//{
//    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
//    if ([defaults objectForKey:@"UUID"] != nil) return;
//    NSString *uuidString = nil;
//    CFUUIDRef uuid = CFUUIDCreate(NULL);
//    if (uuid) {
//        uuidString = (NSString *)CFUUIDCreateString(NULL, uuid);
//        CFRelease(uuid);
//    }
//    [defaults setObject:[NSString uuidString] forKey:@"UUID"];
//    [defaults synchronize];
//}
//====
//////////////////////////////////////////////////////////////

//>=== Native iOS functions for Unity ====
extern "C" 
{
    //convert char* to NSString
    NSString* CreateNSString(const char* string)
    {
        if (string) return [NSString stringWithUTF8String: string];
        else return [NSString stringWithUTF8String: ""];
    }
   
    // проверка установлено ли приложение с custom url scheme
    BOOL _ValidateUrlApp(const char* urlApp)
    {
        NSString *stringURL = CreateNSString(urlApp);
        NSURL *url = [NSURL URLWithString:stringURL];
        if (!url) {  return NO; }
        return  [[UIApplication sharedApplication] canOpenURL: url];
    }  
 
    // the build of application
    const char * _GetCFBuildVersion() 
    {
        NSString *version = [[NSBundle mainBundle] objectForInfoDictionaryKey:@"CFBundleVersion"];            
        return strdup([version UTF8String]);
    }
    // the version of application
    const char * _GetCFBundleVersion() 
    {
        NSString *version = [[NSBundle mainBundle] objectForInfoDictionaryKey:@"CFBundleShortVersionString"];
        return strdup([version UTF8String]);
    }
    // the bundleId of application
    const char * _GetCFBundleIdentifier() 
    {
        NSString *bundleId = [[NSBundle mainBundle] bundleIdentifier];
        return strdup([bundleId UTF8String]);
    }

    //  название приложения
    const char * _GetCFBundleDisplayName() 
    {
        NSString *displayName = [[NSBundle mainBundle] objectForInfoDictionaryKey:@"CFBundleDisplayName"];
        return strdup([displayName UTF8String]);
    }
 
    // доступное место на устройстве
    uint64_t _GetFreeDiskspace ()
    { 
        uint64_t totalFreeSpace = 1024000;            
        NSError *error = nil;            
        NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
        NSDictionary *dictionary = [[NSFileManager defaultManager] attributesOfFileSystemForPath:[paths lastObject] error: &error];
       
        if (dictionary) {
            NSNumber *freeFileSystemSizeInBytes = [dictionary objectForKey:NSFileSystemFreeSize];
            totalFreeSpace = [freeFileSystemSizeInBytes unsignedLongLongValue];
        }
        return totalFreeSpace;
    }
}
//=== Native iOS functions for Unity <====
////////////////////////////////////////////////////////////////////
